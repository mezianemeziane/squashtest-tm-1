/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.api.wizard;

import org.squashtest.tm.api.plugin.ConfigurablePlugin;
import org.squashtest.tm.api.plugin.EntityReference;

/**
 * @author Gregory Fouquet
 */
public interface WizardPlugin extends ConfigurablePlugin {

	/**
	 * @return the name of the javascript module that must be loaded and executed. May return null if none is applicable.
	 * @deprecated : from 2.0.0 each wizard plugin with interface comes with its own SPA to be embedded in a iframe
	 * @see ConfigurablePlugin#getConfigurationPath(EntityReference)
	 */
	@Deprecated
	default String getModule() {
		throw new UnsupportedOperationException("This method is deprecated and will be removed soon.");
	}

}
