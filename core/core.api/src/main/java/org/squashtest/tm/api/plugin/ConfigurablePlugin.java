/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.api.plugin;

import org.squashtest.tm.api.workspace.WorkspaceType;

/**
 * This interface allows to define plugins for Squash TM that will store a configuration in database via the
 * LibraryPluginBinding entity.
 *
 * Notes:
 * - For historic reasons, the plugin configuration is stored per workspace (one per plugin instance).
 * - As of v2.1.0, configurable plugins are only used in the context of projects plugins administration. While
 *   <code>getConfigurationPath</code> allows for different entity kinds, it seems like only Project entities are
 *   dealt with by TM core at the moment.
 * - For project templates that accept configuration, there's a distinct interface {@see TemplateConfigurablePlugin}.
 */
public interface ConfigurablePlugin extends Plugin {

    /**
     * A user-friendly name for this plugin to be displayed in administration pages
     * @return plugin name
     */
    default String getName() { return getId(); }

    /**
     * @return the {@link WorkspaceType} where this wizard is configured. Should not return <code>null</code>
     */
    WorkspaceType getConfiguringWorkspace();

    /**
     * 	Must return the URI path that lead to a configuration page for this plugin. Because the configuration might be context-dependant,
     * 	(a prominent example is per-project dependant), an EntityReference is supplied as this context.
     * 	Returning <code>null</code> indicates that this plugin doesn't come with a configuration page for the given context.
     *
     * @param context an entity reference that serves as a reference for the queried configuration page
     * @return a path (URL segment) for this plugin's configuration page
     */
    String getConfigurationPath(EntityReference context);
}
