/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.testcase;

import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.squashtest.tm.backend.tests.basetest.DatabaseTest;
import org.squashtest.tm.backend.tests.basetest.Users;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static io.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertEquals;

public class TestCaseTest extends DatabaseTest {

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"ADMIN", "PROJECT_MANAGER", "TEST_DESIGNER", "TEST_EDITOR"})
	public void shouldCreateTestCase(Users user) {
		Cookie session = login(user);

		Map<String, Object> formModel = new HashMap<>();
		formModel.put("name", "Test case 1");
		formModel.put("reference", "Ref-001");
		formModel.put("description", "");
		formModel.put("parentEntityReference", "TestCaseLibrary--1");
		formModel.put("customFields", new HashMap<Long, Object>());

		String response = given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
				when()
			.post("backend/test-case-tree/new-test-case").
				then()
			.statusCode(200)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/test-case/create-response-schema.json"))
			.extract().response().asString();

		JsonPath jsonPath = JsonPath.from(response);
		assertEquals("Ref-001 - Test case 1", jsonPath.getString("data.NAME"));
		assertEquals("Ref-001", jsonPath.getString("data.REFERENCE"));
	}

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"VIEWER", "ADVANCE_TESTER", "TEST_RUNNER", "VALIDATOR", "AUTOMATED_TEST_WRITER"})
	public void shouldForbidToCreateTestCase(Users user) {
		Cookie session = login(user);

		Map<String, Object> formModel = new HashMap<>();
		formModel.put("name", "Test case 1");
		formModel.put("reference", "");
		formModel.put("description", "");
		formModel.put("parentEntityReference", "TestCaseLibrary--1");
		formModel.put("customFields", new HashMap<Long, Object>());

		given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
			when()
			.post("backend/test-case-tree/new-test-case").
			then()
			.statusCode(403);
	}

	@Test
	public void shouldForbidTestCaseCreationWithDuplicateName() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = new HashMap<>();
		formModel.put("name", "test case 2");
		formModel.put("reference", "");
		formModel.put("description", "");
		formModel.put("parentEntityReference", "TestCaseLibrary--1");
		formModel.put("customFields", new HashMap<Long, Object>());

		String response = given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
				when()
			.post("backend/test-case-tree/new-test-case").
				then()
			.statusCode(412)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/errors/field-validation-schema.json"))
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("squashTMError.fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("sqtm-core.error.generic.duplicate-name", jsonPath.getString("squashTMError.fieldValidationErrors[0].i18nKey"));
	}

	@Test
	public void shouldForbidTestCaseCreationWithEmptyName() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = new HashMap<>();
		formModel.put("name", "");
		formModel.put("reference", "");
		formModel.put("description", "");
		formModel.put("parentEntityReference", "TestCaseLibrary--1");
		formModel.put("customFields", new HashMap<Long, Object>());

		given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
			when()
			.post("backend/test-case-tree/new-test-case").
			then()
			.statusCode(412)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/errors/empty-field-validation-schema.json"));
	}

	@Override
	protected List<String> getSetupDatasetPath() {
		return Arrays.asList(
			"core-config/core-config.xml",
			"referential/info-list-system.xml",
			"referential/referential-data-test.xml",
			"testcase/test-case-data-test.xml");
	}
}
