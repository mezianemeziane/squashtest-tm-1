/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.research;

import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.squashtest.tm.backend.tests.basetest.DatabaseTest;
import org.squashtest.tm.backend.tests.basetest.Users;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MilestoneModificationTest extends DatabaseTest {

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"ADMIN"})
	public void getMilestoneMassEditForTc(Users user) {
		Cookie session = login(user);


		String response = given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.when()
			.get("backend/search/milestones/test-case/-112")
			.then()
			.statusCode(200)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/search/mass-edit-milestone-schema.json"))
			.extract().response().asString();

		JsonPath jsonPath = JsonPath.from(response);
		assertEquals("true", jsonPath.getString("samePerimeter"));
		assertEquals("[-1]", jsonPath.getString("checkedIds"));

		List<Long> milestoneIds = jsonPath.getList("milestoneIds");
		assertEquals(2, milestoneIds.size());
		assertTrue(milestoneIds.containsAll(Arrays.asList(-1, -2)));
	}

	@Override
	protected List<String> getSetupDatasetPath() {
		return Arrays.asList(
			"core-config/core-config.xml",
			"referential/info-list-system.xml",
			"referential/referential-data-test.xml",
			"search/research-milestone-data-test.xml");
	}
}
