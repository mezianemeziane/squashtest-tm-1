/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.basetest;

import io.restassured.RestAssured;
import io.restassured.http.Cookie;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static io.restassured.RestAssured.given;

public class BackEndTest {

	static {
		RestAssured.baseURI = "http://localhost:8080/squash";
	}

	protected Cookie login(Users user) {
		Map<String, String> credentials = new HashMap<>();
		credentials.put("username", user.getUsername());
		credentials.put("password", user.getPassword());

		Cookie jsessionid = given()
			.formParams(credentials)
			.when()
			.post("/backend/login")
			.thenReturn()
			.getDetailedCookie("JSESSIONID");

		if(Objects.isNull(jsessionid)){
			throw new RuntimeException("Unable to login for user " + user);
		}

		return jsessionid;
	}
}
