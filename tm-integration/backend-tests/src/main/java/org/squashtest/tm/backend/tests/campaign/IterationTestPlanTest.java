/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.campaign;

import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.squashtest.tm.backend.tests.basetest.DatabaseTest;
import org.squashtest.tm.backend.tests.basetest.Users;
import org.squashtest.tm.service.internal.display.grid.GridRequest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static io.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class IterationTestPlanTest extends DatabaseTest {

	@Test
	public void shouldGetIterationTestPlan() {
		Cookie session = login(Users.ADMIN);

		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(10);
		gridRequest.setPage(0);

		String response = given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json").
				when()
			.post("backend/iteration/-1/test-plan").
				then()
			.statusCode(200)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/grid/grid-response-schema.json"))
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		assertEquals(2, dataRows.size());
		assertEquals(-1, jsonPath.getLong("dataRows[0].data.itemTestPlanId"));
		assertEquals("TC1", jsonPath.getString("dataRows[0].data.testCaseName"));

		assertEquals(-2, jsonPath.getLong("dataRows[1].data.itemTestPlanId"));
		assertNull(jsonPath.get("dataRows[1].data.testCaseName"));
	}

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"ADMIN", "PROJECT_MANAGER", "TEST_EDITOR"})
	public void shouldAddTestCasesToTestPlan(Users user) {
		Cookie session = login(user);

		Map<String, List<Long>> body = new HashMap<>();
		body.put("testCaseIds",Arrays.asList(-113L, -114L));

		given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(body).
			when()
			.post("backend/iteration/-1/test-plan-items").
			then()
			.statusCode(200);
	}

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"VIEWER", "ADVANCE_TESTER", "TEST_RUNNER", "VALIDATOR", "AUTOMATED_TEST_WRITER", "TEST_DESIGNER"})
	public void shouldForbidAddTestCasesToTestPlan(Users user) {
		Cookie session = login(user);

		Map<String, List<Long>> body = new HashMap<>();
		body.put("testCaseIds",Arrays.asList(-113L, -114L));

		given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(body).
			when()
			.post("backend/iteration/-1/test-plan-items").
			then()
			.statusCode(403);
	}

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"ADMIN", "PROJECT_MANAGER", "ADVANCE_TESTER", "TEST_RUNNER"})
	public void shouldCreateNewExecution(Users user) {
		Cookie session = login(user);

		String response = given()
			.cookie(session)
			.when()
			.post("backend/iteration/-1/test-plan/-1/executions/new-manual")
			.then()
			.statusCode(200)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		long executionId = jsonPath.getLong("executionId");
		assert executionId > 0L;
	}

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"VIEWER", "VALIDATOR", "AUTOMATED_TEST_WRITER", "TEST_DESIGNER"})
	public void shouldForbidCreateNewExecution(Users user) {
		Cookie session = login(user);
		given()
			.cookie(session)
			.when()
			.post("backend/iteration/-1/test-plan/-1/executions/new-manual")
			.then()
			.statusCode(403);
	}

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"ADMIN", "PROJECT_MANAGER"})
	public void shouldRemoveItemsWithExecutions(Users user) {
		Cookie session = login(user);

		given()
			.cookie(session)
			.when()
			.delete("backend/iteration/-1/test-plan/-1")
			.then()
			.statusCode(200);
	}

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"ADMIN", "PROJECT_MANAGER", "TEST_EDITOR"})
	public void shouldRemoveItemsWithoutExecutions(Users user) {
		Cookie session = login(user);

		given()
			.cookie(session)
			.when()
			.delete("backend/iteration/-1/test-plan/-2")
			.then()
			.statusCode(200);
	}

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"ADVANCE_TESTER", "AUTOMATED_TEST_WRITER", "TEST_DESIGNER", "TEST_RUNNER", "VALIDATOR", "VIEWER"})
	public void canTRemoveItemsWithoutExecutions(Users user) {
		Cookie session = login(user);

		given()
			.cookie(session)
			.when()
			.delete("backend/iteration/-1/test-plan/-2")
			.then()
			.statusCode(403);
	}

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"ADVANCE_TESTER", "AUTOMATED_TEST_WRITER", "TEST_DESIGNER", "TEST_RUNNER", "VALIDATOR", "VIEWER"})
	public void canTRemoveItemsWithExecutions(Users user) {
		Cookie session = login(user);

		given()
			.cookie(session)
			.when()
			.delete("backend/iteration/-1/test-plan/-1")
			.then()
			.statusCode(403);
	}

	@ParameterizedTest
	@EnumSource(value = Users.class, names = {"TEST_EDITOR"})
	public void testEditorCanOnlyRemoveItemsWithoutExecutions(Users user) {
		Cookie session = login(user);

		given()
			.cookie(session)
			.when()
			.delete("backend/iteration/-1/test-plan/-1,-2")
			.then()
			.statusCode(200);

		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(10);
		gridRequest.setPage(0);

		String response = given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json").
				when()
			.post("backend/iteration/-1/test-plan").
				then()
			.statusCode(200)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/grid/grid-response-schema.json"))
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		assertEquals(1, dataRows.size());
		assertEquals(-1, jsonPath.getLong("dataRows[0].data.itemTestPlanId"));
		assertEquals("TC1", jsonPath.getString("dataRows[0].data.testCaseName"));
	}

	@Override
	protected List<String> getSetupDatasetPath() {
		return Arrays.asList(
			"core-config/core-config.xml",
			"referential/info-list-system.xml",
			"referential/referential-data-test.xml",
			"campaign/iteration-test-plan-data-test.xml"
		);
	}
}
