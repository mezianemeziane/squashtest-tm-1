/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation

import org.spockframework.util.NotThreadSafe
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.core.foundation.collection.ColumnFiltering
import org.squashtest.tm.core.foundation.collection.PagedCollectionHolder
import org.squashtest.tm.core.foundation.collection.PagingAndMultiSorting
import org.squashtest.tm.core.foundation.collection.Sorting
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.campaign.TestSuite
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.domain.execution.ExecutionStatus
import org.squashtest.tm.domain.project.GenericProject
import org.squashtest.tm.domain.testautomation.AutomatedSuite
import org.squashtest.tm.service.internal.dto.AutomatedSuiteDto
import org.squashtest.tm.domain.testautomation.TestAutomationServer
import org.squashtest.tm.service.internal.repository.AutomatedExecutionExtenderDao
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao
import org.squashtest.tm.service.internal.repository.AutomatedTestDao
import org.squashtest.tm.service.internal.repository.CustomFieldValueDao
import org.squashtest.tm.service.internal.repository.DenormalizedFieldValueDao
import org.squashtest.tm.service.internal.repository.ExecutionDao
import org.squashtest.tm.service.internal.repository.ExecutionStepDao
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao
import org.squashtest.tm.service.internal.repository.TestSuiteDao
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteWithSquashAutomAutomatedITPIs
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Ignore
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@NotThreadSafe
@UnitilsSupport
@Transactional
class AutomatedSuiteManagerServiceIT extends DbunitServiceSpecification {

	@Inject
	AutomatedSuiteManagerService service

	@Inject
	AutomatedSuiteDao automatedSuiteDao

	@Inject
	AutomatedExecutionExtenderDao automatedExecutionExtenderDao

	@Inject
	IterationTestPlanDao itpiDao

	@Inject
	ExecutionDao executionDao

	@Inject
	ExecutionStepDao executionStepDao

	@Inject
	CustomFieldValueDao customFieldValueDao

	@Inject
	DenormalizedFieldValueDao denormalizedFieldValueDao

	@Inject
	TestSuiteDao suiteDao

	@Inject
	AutomatedTestDao automatedTestDao;

	@DataSet("TestAutomationService.sandbox.xml")
	def "should return executions associated to an automated test suite given its id"(){
		when:
		def res = service.findExecutionsByAutomatedTestSuiteId("suite1")
		then:
		res[0].id == -41L
		res[1].id == -40L
	}

	def getServer(id){
		return getSession().load(TestAutomationServer.class, id)
	}

	def getProject(id){
		return getSession().load(GenericProject.class, id)
	}

	@DataSet("TestAutomationService.TFtrigger.xml")
	def "should return automated test suite associated to an iteration given its id (Squash TF executions only)"() {
		when:
		AutomatedSuite suite = service.createFromIterationTestPlan(-11L)

		then:
		suite.executionExtenders.size() == 3
		suite.executionExtenders[0].id == 1L
		suite.executionExtenders[0].automatedTest.id == -71L
	}

	@DataSet("TestAutomationService.TFtrigger.xml")
	def "should return automated test suite associated to an iteration given its id (Squash TF and Squash AUTOM executions)"() {
		when:
		AutomatedSuiteWithSquashAutomAutomatedITPIs suite = service.createFromIterationTestPlanSquashAutom(-11L)

		then:
		suite.getSuite().executionExtenders.size() == 3
		suite.getSuite().executionExtenders[0].id == 4L
		suite.getSuite().executionExtenders[0].automatedTest.id == -71L

		suite.getSquashAutomAutomatedItems().size() == 2
		suite.getSquashAutomAutomatedItems()[0].id == -204L
	}

	@DataSet("TestAutomationService.findPagedList.xml")
	def "should return paged collection of automated suite given an iteration ID"() {
		given:
		PagingAndMultiSorting paging = new TestPagingMultiSorting()
		ColumnFiltering filtering = ColumnFiltering.UNFILTERED

		when:
		PagedCollectionHolder<List<AutomatedSuiteDto>> pagedSuites = service.getAutomatedSuitesByIterationID(-11L, paging, filtering)

		then:

		pagedSuites.totalNumberOfItems == 2
		pagedSuites.firstItemIndex == 0
		pagedSuites.pagedItems.size() == 2

	}

	@DataSet("TestAutomationService.findPagedList.xml")
	def "should return paged collection of automated suite given a test suite ID"() {
		given:
		PagingAndMultiSorting paging = new TestPagingMultiSorting()
		ColumnFiltering filtering = ColumnFiltering.UNFILTERED

		when:
		PagedCollectionHolder<List<AutomatedSuiteDto>> pagedSuites = service.getAutomatedSuitesByTestSuiteID(-21L, paging, filtering)

		then:

		pagedSuites.totalNumberOfItems == 1
		pagedSuites.firstItemIndex == 0
		pagedSuites.pagedItems.size() == 1
		pagedSuites.pagedItems[0].getSuiteId() == "123"

	}

	@DataSet("TestAutomationService.deleteOldAutomatedSuites.xml")
	@Ignore
	// wait for a fix on 1.22 branch
	def "Should delete all old automated suites according to project configuration"() {
		when: "for each iteration, create one new automated suite with their new automated execution extenders"
			service.createFromIterationTestPlan(-1L)
			service.createFromIterationTestPlan(-2L)
			service.createFromIterationTestPlan(-3L)
		then:
			List<IterationTestPlanItem> itemsBefore = itpiDao.findAll()
			itemsBefore.size() == 12
			itemsBefore.findAll { it.getExecutions().size() == 11 }.size() == 8
			itemsBefore.findAll { it.getExecutions().size() == 10 }.size() == 4
			itemsBefore.each {
				it.setExecutionStatus(ExecutionStatus.BLOCKED)
			}
			def automatedSuites = automatedSuiteDao.findAll()
			automatedSuites.size() == 33
			automatedExecutionExtenderDao.count() == 128
			List<Execution> executionsBefore = executionDao.findAll()
			executionsBefore.size() == 116 + 12
			executionStepDao.findAll().size() == 9
			customFieldValueDao.count() == 9 + 12
			denormalizedFieldValueDao.count() == 6 + 12
			List<TestSuite> suitesBefore = suiteDao.findAll()
			suitesBefore.size() == 3
			suitesBefore.every({
				it.executionStatus == ExecutionStatus.BLOCKED
			})
			automatedTestDao.findAll().size() == 3
		when:
			service.cleanOldSuites()
		then:
			em.flush()
			em.clear()
			// P1 keeps all suites, P2 keeps nothing, P3 only keeps new ones
			List<IterationTestPlanItem> itemsAfter = itpiDao.findAll()
			itemsAfter.size() == 12
			def itemsInProject1 = itemsAfter.findAll({it.project.id == -1L })
			itemsInProject1.every {
				it.executions.size() == 11
			}
			def itemsInProject2 = itemsAfter.findAll({it.project.id == -2L})
			itemsInProject2.every {
				it.executions.size() == 0
			}
			def itemsInProject3 = itemsAfter.findAll({it.project.id == -3L})
			itemsInProject3.every {
				it.executions.size() == 1
			}
			automatedSuiteDao.findAll().size() == 11 + 1
			automatedExecutionExtenderDao.findAll().size() == 44 + 4
			List<Execution> executionsAfter = executionDao.findAll()
			executionsAfter.size() == 44 + 4
			def executionsAfterFromProject1 = executionsAfter.findAll({ it.project.id == -1L })
			executionsAfterFromProject1.size() == 44
			executionsAfterFromProject1.any {
				it.steps.size() == 3
			}
			def executionsAfterFromProject2 = executionsAfter.findAll({ it.project.id == -2L })
			executionsAfterFromProject2.isEmpty()
			def executionsAfterFromProject3 = executionsAfter.findAll({ it.project.id == -3L })
			executionsAfterFromProject3.size() == 4
			executionsAfterFromProject3.every({
				it.steps.size() == 0
			})
			def executionStepsAfter = executionStepDao.findAll()
			executionStepsAfter.size() == 3
			customFieldValueDao.count() == 5 + 8
			denormalizedFieldValueDao.count() == 2 + 8
			List<TestSuite> suitesAfter = suiteDao.findAll()
			suitesAfter.size() == 3
			suitesAfter.every({
				it.executionStatus == ExecutionStatus.READY ||
					it.executionStatus == ExecutionStatus.BLOCKED
			})
			automatedTestDao.findAll().size() == 3
	}

	@DataSet("TestAutomationService.deleteEmptyOldAutomatedSuites.xml")
	def "Should delete all old automated suite even if they have no execution"() {
		when:
			service.cleanOldSuites()
		then:
			automatedSuiteDao.findAll().size() == 0
	}

	@DataSet("TestAutomationService.deleteOldAutomatedSuites.xml")
	@Ignore
	// wait for a fix on 1.22 branch
	def "Should count the old automated suites and the automated execution extenders according to the project configuration"() {
			given: "for each iteration, create one new automated suite with their new automated execution extenders"
				service.createFromIterationTestPlan(-1L)
				service.createFromIterationTestPlan(-2L)
				service.createFromIterationTestPlan(-3L)
			when:
				AutomationDeletionCount resultCount = service.countOldAutomatedSuitesAndExecutions()
			then:
				resultCount.getOldAutomatedSuiteCount() == 21
				resultCount.getOldAutomatedExecutionCount() == 80
	}

	def "Should not throw any Exception if no old automated suites are to delete"() {
		given: "an empty dataset"
		when:
			service.cleanOldSuites()
		then:
			noExceptionThrown()
	}
}

class TestPagingMultiSorting implements PagingAndMultiSorting{

	@Override
	int getFirstItemIndex() {
		return 0
	}

	@Override
	int getPageSize() {
		return 50
	}

	@Override
	boolean shouldDisplayAll() {
		return false
	}

	@Override
	List<Sorting> getSortings() {
		return Collections.emptyList()
	}

}
