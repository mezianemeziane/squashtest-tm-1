/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.collectors.libraries

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.service.internal.repository.display.impl.collectors.libraries.CampaignLibraryCollector
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet()
class CampaignLibraryCollectorIT extends DbunitDaoSpecification {

	@Inject
	CampaignLibraryCollector campaignLibraryCollector

	@Unroll
	def "Should collect campaign libraries"() {
		when:
		def nodes = campaignLibraryCollector.collect(ids)

		then:
		nodes.size() == expectedNodes.size();
		for (id in ids) {
			def expectedNode = expectedNodes.get(id);
			def actualNode = nodes.get(id)

			assert expectedNode.id == actualNode.id;
			Map<String, Object> expectedData = expectedNode.data;
			def actualData = actualNode.data
			assert actualData.size() == expectedData.size()
			expectedData.forEach { key, expectedValue ->
				def actualValue = actualData.get(key)
				assert actualValue == expectedValue;
			}
		}

		where:
		ids   || expectedNodes
		null  || []
		[]    || []
		[-1L,-2L] || [
			(-1L): [id  : "CampaignLibrary--1",
					projectId: -1L,
					data: [
						CL_ID    : -1L,
						NAME       : "project-1",
						CHILD_COUNT: 2,
						projectId: -1L,
						BOUND_TO_BLOCKING_MILESTONE: false
					]
			],
			(-2L): [id  : "CampaignLibrary--2",
					projectId: -2L,
					data: [
						CL_ID    : -2L,
						NAME       : "project-2",
						CHILD_COUNT: 0,
						projectId: -2L,
						BOUND_TO_BLOCKING_MILESTONE: false
					]
			]
		]
	}
}
