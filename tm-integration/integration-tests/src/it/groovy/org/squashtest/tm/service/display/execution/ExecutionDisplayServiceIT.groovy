/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.execution

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@Transactional
@DataSet
@UnitilsSupport
class ExecutionDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	ExecutionDisplayService executionDisplayService

	def "should retrieve executions by test case"() {

		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		when:
		def response = executionDisplayService.findByTestCaseId(-1L, gridRequest)

		then:
		def rows = response.dataRows
		rows.size() == 1
		rows.get(0).data["campaignName"] == "campaign"
		rows.get(0).data["iterationName"] == "iteration"
		rows.get(0).data["projectName"] == "project"

	}

	def "should retrieve executions by test case order by last executed on"() {

		given:
		def gridRequest = new GridRequest()
		gridRequest.sort = Arrays.asList(new GridSort("lastExecutedOn", GridSort.SortDirection.DESC))
		gridRequest.size = 25

		when:
		def response = executionDisplayService.findByTestCaseId(-2L, gridRequest)

		then:
		def rows = response.dataRows
		rows.size() == 3
		def dateRow1 = (Date) rows.get(0).data["lastExecutedOn"]
		def dateRow2 = (Date) rows.get(1).data["lastExecutedOn"]
		def dateRow3 = (Date) rows.get(2).data["lastExecutedOn"]
		dateRow1 > dateRow2
		dateRow2 > dateRow3
	}
}
