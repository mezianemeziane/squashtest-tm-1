/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.collectors

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.service.internal.repository.display.impl.collectors.TestCaseFolderCollector
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet
class TestCaseFolderCollectorIT extends DbunitDaoSpecification {

	@Inject
	TestCaseFolderCollector testCaseFolderCollector

	@Inject
	ActiveMilestoneHolder activeMilestoneHolder

	def setup() {
		activeMilestoneHolder.setActiveMilestone(-1L)
	}

	def cleanup() {
		activeMilestoneHolder.clearContext()
	}

	@Unroll
	def "Should collect test case folders"() {
		when:
		def nodes = testCaseFolderCollector.collect(ids)

		then:
		nodes.size() == expectedNodes.size()
		for (id in ids) {
			def expectedNode = expectedNodes.get(id)
			def actualNode = nodes.get(id)

			assert expectedNode.id == actualNode.id
			Map<String, Object> expectedData = expectedNode.data
			def actualData = actualNode.data
			assert actualData.size() == expectedData.size()
			expectedData.forEach { key, expectedValue ->
				def actualValue = actualData.get(key)
				assert actualValue == expectedValue
			}
		}

		where:
		ids          || expectedNodes
		null         || []
		[]           || []
		[-11L, -12L] || [
			(-11L): [
				id       : "TestCaseFolder--11",
				projectId: -1L,
				data     : [
					TCLN_ID        : -11L,
					NAME           : "test case folder 1",
					projectId      : -1,
					CHILD_COUNT    : 2,
					"CUF_COLUMN_-1": "",
					"CUF_COLUMN_-2": "true",
					"MILESTONES"   : [],
					BOUND_TO_BLOCKING_MILESTONE: false
				]
			],
			(-12L): [
				id       : "TestCaseFolder--12",
				projectId: -1L,
				data     : [
					TCLN_ID        : -12L,
					NAME           : "test case folder 2",
					projectId      : -1,
					CHILD_COUNT    : 0,
					"CUF_COLUMN_-2": "false",
					"CUF_COLUMN_-1": "SEC-2",
					"CUF_COLUMN_-3": "tag0|toto|ahaha|value0",
					"MILESTONES"   : [],
					BOUND_TO_BLOCKING_MILESTONE: false,
				]
			]
		]
	}
}
