/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.tf

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.stub.security.UserContextHelper
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class AutomationTesterRequestDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private AutomationTesterRequestDisplayService service;

	def setup() {
		UserContextHelper.setUsername("JP01")
	}

	def "should search ready to transmit automation requests"() {
		given:
		def gridRequest = new GridRequest()
		def sort = new GridSort()
		sort.property = "tclnId"
		sort.direction = GridSort.SortDirection.DESC
		gridRequest.sort.add(sort)
		gridRequest.setPage(0)
		gridRequest.setSize(25)

		when:
		def gridResponse = service.findReadyForTransmissionRequests(gridRequest)

		then:
		gridResponse.dataRows.size() == 2

		def row1 = gridResponse.dataRows.get(0)
		row1.data['name'] == 'test-case-1'
		def row2 = gridResponse.dataRows.get(1)
		row2.data['name'] == 'test-case-3'
	}

	def "should search to be validated automation requests"() {
		given:
		def gridRequest = new GridRequest()
		def sort = new GridSort()
		sort.property = "tclnId"
		sort.direction = GridSort.SortDirection.DESC
		gridRequest.sort.add(sort)
		gridRequest.setPage(0)
		gridRequest.setSize(25)

		when:
		def gridResponse = service.findToBeValidatedRequests(gridRequest)

		then:
		gridResponse.dataRows.size() == 3

		def row1 = gridResponse.dataRows.get(0)
		row1.data['name'] == 'test-case-6'
		def row2 = gridResponse.dataRows.get(1)
		row2.data['name'] == 'test-case-7'
		def row3 = gridResponse.dataRows.get(2)
		row3.data['name'] == 'test-case-8'
	}

	def "should search global automation requests"() {
		given:
		def gridRequest = new GridRequest()
		def sort = new GridSort()
		sort.property = "tclnId"
		sort.direction = GridSort.SortDirection.DESC
		gridRequest.sort.add(sort)
		gridRequest.setPage(0)
		gridRequest.setSize(25)

		when:
		def gridResponse = service.findGlobalView(gridRequest)

		then:

		gridResponse.dataRows.size() == 6

		def row1 = gridResponse.dataRows.get(0)
		row1.data['name'] == 'test-case-1'
		def row2 = gridResponse.dataRows.get(1)
		row2.data['name'] == 'test-case-3'
		def row3 = gridResponse.dataRows.get(2)
		row3.data['name'] == 'test-case-5'
		def row4 = gridResponse.dataRows.get(3)
		row4.data['name'] == 'test-case-6'
		def row5 = gridResponse.dataRows.get(4)
		row5.data['name'] == 'test-case-7'
		def row6 = gridResponse.dataRows.get(5)
		row6.data['name'] == 'test-case-8'
	}
}
