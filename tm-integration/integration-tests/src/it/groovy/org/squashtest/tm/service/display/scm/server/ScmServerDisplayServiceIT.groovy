/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.scm.server

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject


@UnitilsSupport
@Transactional
@DataSet
class ScmServerDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private ScmServerDisplayService scmServerDisplayService

	def "should fetch scm servers"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		when:
		def gridResponse = scmServerDisplayService.findAll(gridRequest)

		then:
		gridResponse.count == 2
		def dataServer1 = gridResponse.dataRows.get(0).data
		def dataServer2 = gridResponse.dataRows.get(1).data

		dataServer1.serverId == -1L
		dataServer1.name == "Github"
		dataServer1.url == "https://github.com/RecetteSquash"
		dataServer1.kind == "git"
		dataServer1.projectCount == 0

		dataServer2.serverId == -2L
		dataServer2.name == "Gitlab"
		dataServer2.url == "https://gitlab.com/RecetteSquash"
		dataServer2.kind == "git"
		dataServer2.projectCount == 1
	}

	@Unroll("should allow sorting on #column")
	def "should allow sorting"() {
		when:
		def gridRequest = new GridRequest()
		gridRequest.sort.add(new GridSort(column, GridSort.SortDirection.ASC))
		gridRequest.setSize(25)
		this.scmServerDisplayService.findAll(gridRequest)

		then:
		noExceptionThrown()

		where:
		column << ["serverId",
				   "name",
				   "url",
				   "kind",
				   "projectCount"]
	}
}
