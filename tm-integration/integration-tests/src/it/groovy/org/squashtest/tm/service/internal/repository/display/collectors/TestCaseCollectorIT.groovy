/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.collectors

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.domain.testcase.TestCaseKind
import org.squashtest.tm.service.internal.repository.display.impl.collectors.TestCaseCollector
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet
class TestCaseCollectorIT extends DbunitDaoSpecification {

	@Inject
	TestCaseCollector testCaseCollector

	@Unroll
	def "Should collect test cases"() {
		when:
		def nodes = testCaseCollector.collect(ids)

		then:
		nodes.size() == expectedNodes.size();
		for (id in ids) {
			def expectedNode = expectedNodes.get(id);
			def actualNode = nodes.get(id)

			assert expectedNode.id == actualNode.id;
			Map<String, Object> expectedData = expectedNode.data;
			def actualData = actualNode.data
			assert actualData.size() == expectedData.size()
			expectedData.forEach { key, expectedValue ->
				def actualValue = actualData.get(key)
				assert actualValue == expectedValue;
			}
		}

		where:
		ids            || expectedNodes
		null           || []
		[]             || []
		[-111L, -112L] || [
			(-111L): [id       : "TestCase--111",
					  projectId: -1L,
					  data     : [
						  TCLN_ID              : -111L,
						  NAME                 : "REF-001 - test case 1",
						  IMPORTANCE           : "LOW",
						  REFERENCE            : "REF-001",
						  TC_STATUS            : "WORK_IN_PROGRESS",
						  TC_KIND              : TestCaseKind.STANDARD,
						  TC_NATURE_ICON       : "indeterminate_checkbox_empty",
						  TC_NATURE_LABEL      : "test-case.nature.NAT_UNDEFINED",
						  TC_NATURE_TYPE       : "SYS",
						  STEP_COUNT           : 0,
						  COVERAGE_COUNT       : 0,
						  "CUF_COLUMN_-1"      : "",
						  "CUF_COLUMN_-2"      : "true",
						  projectId            : -1,
						  MILESTONES           : [],
						  SCRIPTED_TEST_CASE_ID: null,
						  KEYWORD_TEST_CASE_ID : null,
						  BOUND_TO_BLOCKING_MILESTONE: false
					  ]
			],
			(-112L): [id       : "TestCase--112",
					  projectId: -1L,
					  data     : [
						  TCLN_ID              : -112L,
						  NAME                 : "TC2 - test case 2",
						  IMPORTANCE           : "HIGH",
						  REFERENCE            : "TC2",
						  TC_STATUS            : "WORK_IN_PROGRESS",
						  TC_KIND              : TestCaseKind.STANDARD,
						  TC_NATURE_ICON       : "indeterminate_checkbox_empty",
						  TC_NATURE_LABEL      : "test-case.nature.NAT_UNDEFINED",
						  TC_NATURE_TYPE       : "SYS",
						  STEP_COUNT           : 2,
						  COVERAGE_COUNT       : 0,
						  "CUF_COLUMN_-1"      : "SEC-2",
						  "CUF_COLUMN_-2"      : "false",
						  projectId            : -1,
						  MILESTONES           : [-1],
						  SCRIPTED_TEST_CASE_ID: null,
						  KEYWORD_TEST_CASE_ID : null,
						  BOUND_TO_BLOCKING_MILESTONE: true
					  ]
			]
		]
	}
}
