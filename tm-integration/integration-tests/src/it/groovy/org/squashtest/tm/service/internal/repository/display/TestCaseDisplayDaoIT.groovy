/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display


import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.domain.testcase.TestCaseKind
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
class TestCaseDisplayDaoIT extends DbunitDaoSpecification {

	@Inject
	private TestCaseDisplayDao dao

	@DataSet
	def "should retrieve a TestCase"() {

		given:
		def testCaseId = -21L

		when:
		def dto = dao.getTestCaseDtoById(testCaseId)

		then:

		dto.name == "TestCase2"
		dto.kind == TestCaseKind.STANDARD
		dto.projectId == -1
		dto.createdBy == "me"
	}

	@DataSet
	def "should retrieve calling testCases"() {

		given:
		when:
		def callings = dao.getCallingTestCaseById(testCaseId)

		then:

		callings.size() == nbCalls

		where:
		testCaseId | nbCalls
		-11L       | 0
		-21L       | 1
		-31L       | 0
		-41L       | 1
	}

	@DataSet
	def "should retrieve the most recent execution status"() {
		given:
		when:

		def executionStatus = dao.getLastExecutionStatus(testCaseId)
		then:
		executionStatus == status
		where:
		testCaseId | status
		-11L       | "SUCCESS"
		-21L       | "FAILURE"

	}
}
