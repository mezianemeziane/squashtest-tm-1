/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.tf

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.stub.security.UserContextHelper
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject


@UnitilsSupport
@Transactional
@DataSet
class AutomationWorkspaceDataProviderServiceIT extends DbunitServiceSpecification {

	@Inject
	private AutomationWorkspaceDataProviderService service

	def setup() {
		UserContextHelper.setUsername("JP01")
	}

	def "should fetch data"() {

		when:
		def workspaceData = service.findData()
		then:

		workspaceData.nbAssignedAutomReq == 2
		workspaceData.nbAutomReqToTreat == 1
		workspaceData.nbTotal == 6
		workspaceData.usersWhoModifiedTestCasesAssignView.size() == 1
		workspaceData.usersWhoModifiedTestCasesGlobalView.size() == 1
		workspaceData.usersWhoModifiedTestCasesTreatmentView.size() == 1
	}

	def "should fetch functional tester data"() {

		when:
		def wsTesterData = service.findFunctionalTesterData()
		then:

		wsTesterData.nbGlobalAutomReq == 6
		wsTesterData.nbReadyForTransmissionAutomReq == 1
		wsTesterData.nbToBeValidatedAutomReq == 2
		wsTesterData.usersWhoModifiedTestCasesGlobalView.size() == 1
		wsTesterData.usersWhoModifiedTestCasesReadyView.size()  == 1
		wsTesterData.usersWhoModifiedTestCasesValidateView.size()  == 1
	}
}
