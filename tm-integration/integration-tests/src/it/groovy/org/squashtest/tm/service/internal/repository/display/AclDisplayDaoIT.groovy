/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import com.google.common.collect.Multimap
import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
class AclDisplayDaoIT extends DbunitDaoSpecification {

	public static final List<String> READ = ["READ"]
	public static final int VIEWER_ALL_RIGHT_SIZE = 7

	@Inject
	AclDisplayDao dao

	private static final List<String> PM_PROJECT = ["READ", "MANAGEMENT", "ATTACH"]
	private static final List<String> PM_REQ = ["READ", "WRITE", "CREATE", "DELETE", "EXPORT", "LINK", "IMPORT", "ATTACH"]
	private static final List<String> PM_TEST_CASE = ["READ", "WRITE", "CREATE", "DELETE", "EXPORT", "LINK", "IMPORT", "ATTACH"]
	private static final List<String> PM_CAMPAIGN = ["READ", "WRITE", "CREATE", "DELETE", "EXPORT", "EXECUTE", "LINK", "ATTACH", "EXTENDED_DELETE", "READ_UNASSIGNED"]
	private static final List<String> PM_CUSTOM_REPORT = ["READ", "WRITE", "CREATE", "DELETE", "ATTACH"]
	private static final List<String> PM_AUTOMATION_REQUEST = ["READ", "WRITE", "CREATE", "DELETE", "EXPORT", "LINK", "IMPORT", "ATTACH", "WRITE_AS_FUNCTIONAL", "WRITE_AS_AUTOMATION"]

	private static int PM_ALL_RIGHTS_SIZE = PM_PROJECT.size() + PM_REQ.size() + PM_TEST_CASE.size() + PM_CAMPAIGN.size() + PM_CUSTOM_REPORT.size() + PM_AUTOMATION_REQUEST.size()

	@DataSet()
	def "Should find permissions for one user who can manage one project"() {
		given:

		when:
		def permissions = dao.findPermissions([-1L, -2L, -3L, -4L], [-1L]);

		then:
		permissions.size() == 4;
		permissions.get(-2L).size() == 0
		permissions.get(-3L).size() == 0
		permissions.get(-4L).size() == 0

		def perm = permissions.get(-1L)
		checkProjectManager(perm)
	}

	@DataSet()
	def "Should find permissions for one user who can view one project"() {
		given:

		when:
		def permissions = dao.findPermissions([-1L, -2L, -3L, -4L], [-2L]);

		then:
		permissions.size() == 4;
		permissions.get(-1L).size() == 0
		permissions.get(-2L).size() == 0
		permissions.get(-3L).size() == 0

		def perm = permissions.get(-4L)
		checkProjectViewer(perm)
	}

	@DataSet()
	def "Should find permissions for a team who can see several projects"() {
		given:

		when:
		def permissions = dao.findPermissions([-1L, -2L, -3L, -4L], [-100L]);

		then:
		permissions.size() == 4;

		checkProjectManager(permissions.get(-1L))
		checkProjectViewer(permissions.get(-2L))
		checkProjectManager(permissions.get(-3L))
		permissions.get(-4L).size() == 0
	}



	@DataSet()
	def "Should add permissions when several parties have rights on different objects"() {
		given:

		when:
		def permissions = dao.findPermissions([-1L, -2L, -3L, -4L], [-2L, -100L]);

		then:
		permissions.size() == 4;
		permissions.get(-1L).size() == PM_ALL_RIGHTS_SIZE
		permissions.get(-2L).size() == VIEWER_ALL_RIGHT_SIZE
		permissions.get(-3L).size() == PM_ALL_RIGHTS_SIZE
		permissions.get(-4L).size() == VIEWER_ALL_RIGHT_SIZE

		checkProjectManager(permissions.get(-1L))
		checkProjectViewer(permissions.get(-2L))
		checkProjectManager(permissions.get(-3L))
		checkProjectViewer(permissions.get(-4L))
	}


	@DataSet()
	def "Should merge permissions when several parties have different rights on same objects"() {
		given:

		when:
		def permissions = dao.findPermissions([-1L], [-1L, -100L]);

		then:
		permissions.size() == 1;

		def perm = permissions.get(-1L)
		checkProjectManager(perm)

	}

	private void checkProjectManager(Multimap<String, String> perm) {
		assert perm.size() == PM_ALL_RIGHTS_SIZE
		assert perm.get("PROJECT") == PM_PROJECT
		assert perm.get("REQUIREMENT_LIBRARY") == PM_REQ
		assert perm.get("TEST_CASE_LIBRARY") == PM_TEST_CASE
		assert perm.get("CAMPAIGN_LIBRARY") == PM_CAMPAIGN
		assert perm.get("CUSTOM_REPORT_LIBRARY") == PM_CUSTOM_REPORT
		assert perm.get("AUTOMATION_REQUEST_LIBRARY") == PM_AUTOMATION_REQUEST
	}

	private void checkProjectViewer(Multimap<String, String> perm) {
		assert perm.size() == VIEWER_ALL_RIGHT_SIZE
		assert perm.get("PROJECT") == READ
		assert perm.get("REQUIREMENT_LIBRARY") == READ
		assert perm.get("TEST_CASE_LIBRARY") == READ
		assert perm.get("CAMPAIGN_LIBRARY") == ["READ", "READ_UNASSIGNED"]
		assert perm.get("CUSTOM_REPORT_LIBRARY") == READ
		assert perm.get("AUTOMATION_REQUEST_LIBRARY") == READ
	}


}
