/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.templateplugin

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.api.template.TemplateConfigurablePlugin
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.project.ProjectTemplate
import org.squashtest.tm.domain.project.TemplateConfigurablePluginBinding
import org.squashtest.tm.exception.templateplugin.TemplateConfigurablePluginBindingAlreadyExistsException
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
class TemplateConfigurablePluginBindingServiceIT extends DbunitServiceSpecification {

    @Inject TemplateConfigurablePluginBindingService service

    @DataSet("TemplateConfigurablePluginBindingServiceIT.xml")
    @Unroll
    def "should create a new binding"() {
        given:
        ProjectTemplate template = findEntity(ProjectTemplate.class, -1L) as ProjectTemplate
        Project project = findEntity(Project.class, -2L) as Project
        TemplateConfigurablePlugin plugin = Mock()
        plugin.id >> "my-plugin-id"

        when:
        TemplateConfigurablePluginBinding binding = service.createTemplateConfigurablePluginBinding(template, project, plugin)

        then:
        def persistedBinding = (TemplateConfigurablePluginBinding) findEntity(TemplateConfigurablePluginBinding.class, binding.id)
        persistedBinding.id == binding.id
        persistedBinding.projectTemplate.name == "Template"
        persistedBinding.project.name == "Attached Project"
        persistedBinding.pluginId == "my-plugin-id"
    }

    @DataSet("TemplateConfigurablePluginBindingServiceIT.xml")
    @Unroll
    def "should throw when trying to create existing binding"() {
        given:
        ProjectTemplate template = findEntity(ProjectTemplate.class, -1L) as ProjectTemplate
        Project project = findEntity(Project.class, -2L) as Project
        TemplateConfigurablePlugin plugin = Mock()
        plugin.id >> "my-plugin-id"
        service.createTemplateConfigurablePluginBinding(template, project, plugin)

        when:
        service.createTemplateConfigurablePluginBinding(template, project, plugin)

        then:
        thrown TemplateConfigurablePluginBindingAlreadyExistsException
    }

    @DataSet("TemplateConfigurablePluginBindingServiceIT_bindings.xml")
    @Unroll
    def "should remove all bindings for project"() {
        when:
        service.removeAllForGenericProject(-2L)

        then:
        def bindings = findAll("TemplateConfigurablePluginBinding")
        bindings.size() == 4
    }

    @DataSet("TemplateConfigurablePluginBindingServiceIT_bindings.xml")
    @Unroll
    def "should remove all bindings for template"() {
        when:
        service.removeAllForGenericProject(-1L)

        then:
        def bindings = findAll("TemplateConfigurablePluginBinding")
        bindings.size() == 0
    }

    @DataSet("TemplateConfigurablePluginBindingServiceIT_bindings.xml")
    @Unroll
    def "should determine if binding exists"() {
        expect:
        service.hasBinding(-1L, -2L, "my-plugin")
        service.hasBinding(-1L, -2L, "other-plugin")

        !service.hasBinding(-1L, -2L, "unknown-plugin")
        !service.hasBinding(-2L, -1L, "my-plugin")
        !service.hasBinding(-1L, -42L, "my-plugin")
    }

    @DataSet("TemplateConfigurablePluginBindingServiceIT_bindings.xml")
    @Unroll
    def "should find all bindings for template"() {
        expect:
        service.findAllByTemplateId(-1L).size() == 6
    }

    @DataSet("TemplateConfigurablePluginBindingServiceIT_bindings.xml")
    @Unroll
    def "should find all bindings for template and plugin ID"() {
        expect:
        service.findAllByTemplateIdAndPluginId(-1L, "my-plugin").size() == 3
    }

    @DataSet("TemplateConfigurablePluginBindingServiceIT_bindings.xml")
    @Unroll
    def "should determine if project plugin conf is bound"() {
        expect:
        service.isProjectConfigurationBoundToTemplate(-2L, "my-plugin")

        !service.isProjectConfigurationBoundToTemplate(-2L, "unknown-plugin")
    }

}
