/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.security.domain.objectidentity

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementLibrary
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.domain.testcase.TestCaseLibrary
import org.squashtest.tm.service.security.acls.domain.objectidentity.OptimizedObjectIdentityRetrievalStrategy
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet("OptimizedObjectEntityRetrievers.sandbox.xml")
class OptimizedObjectIdentityRetrieverIT extends DbunitServiceSpecification {

	@Inject
	OptimizedObjectIdentityRetrievalStrategy optimizedObjectIdentityRetrievalStrategy

	@Unroll
	def "should fetch oid from entity "() {

		when:
		def objectIdentity = optimizedObjectIdentityRetrievalStrategy.createObjectIdentity(entityId, entityClass)

		then:
		objectIdentity
		objectIdentity.identifier == expectedLibraryId
		objectIdentity.type == expectedLibraryClass

		where:
		entityClass            | entityId || expectedLibraryClass                       | expectedLibraryId
		TestCase.CLASS_NAME    | -1L      || TestCaseLibrary.CLASS_NAME + ':Unknown'    | 0L
		TestCase.CLASS_NAME    | -2L      || TestCaseLibrary.CLASS_NAME                 | -3L
		TestCase.CLASS_NAME    | -3L      || TestCaseLibrary.CLASS_NAME                 | -13L
		Requirement.CLASS_NAME | -1L      || RequirementLibrary.CLASS_NAME + ':Unknown' | 0L
		Requirement.CLASS_NAME | -2L      || RequirementLibrary.CLASS_NAME              | -2L
		Requirement.CLASS_NAME | -3L      || RequirementLibrary.CLASS_NAME              | -12L

	}
}
