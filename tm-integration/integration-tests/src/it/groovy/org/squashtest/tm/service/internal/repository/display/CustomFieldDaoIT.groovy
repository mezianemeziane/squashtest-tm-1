/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.domain.customfield.BindableEntity
import org.squashtest.tm.domain.customfield.BoundEntity
import org.squashtest.tm.service.internal.display.dto.CufBindingDto
import org.squashtest.tm.service.internal.display.dto.ProjectDto
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
class CustomFieldDaoIT extends DbunitDaoSpecification {

	@Inject
	CustomFieldDao dao;

	@DataSet()
	def "Should append bindings in project DTO"() {
		given:
		ProjectDto project = new ProjectDto()
		project.setId(-1L)
		List<ProjectDto> projects = [project];

		when:
		dao.appendCustomFieldBindings(projects);

		then:
		project.customFieldBindings.size() == 12;
		def bindingsToTestCase = project.customFieldBindings.get(BindableEntity.TEST_CASE)
		bindingsToTestCase.size() == 3
		bindingsToTestCase.collect({ it.id }) == [-131L, -111L, -121L]
		def bindingsToCampaign = project.customFieldBindings.get(BindableEntity.CAMPAIGN)
		bindingsToCampaign.size() == 0;
		def bindingsToIteration = project.customFieldBindings.get(BindableEntity.ITERATION)
		bindingsToIteration.size() == 3
		bindingsToIteration.collect({ it.id }) == [-112L, -132L, -122L]
	}
}
