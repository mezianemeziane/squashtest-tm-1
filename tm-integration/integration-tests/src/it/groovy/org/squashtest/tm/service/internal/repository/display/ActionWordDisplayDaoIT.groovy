/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.service.internal.display.dto.testcase.ActionWordFragmentValueDto
import org.squashtest.tm.service.internal.display.dto.testcase.ActionWordTextValueDto
import org.squashtest.tm.service.internal.display.dto.testcase.ActionWordParameterValueDto
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@DataSet
@UnitilsSupport
class ActionWordDisplayDaoIT extends DbunitDaoSpecification {

	@Inject
	ActionWordDisplayDao actionWordDisplayDao

	def "#findActionWordFragmentValues() should retrieve action word fragments dto"() {
		given:
			def keywordStepIds = [-1L, -2L, -3L, -4L, -5L]
			when:
			List<ActionWordFragmentValueDto> result =
				actionWordDisplayDao.findActionWordFragmentValues(keywordStepIds)
		then:
			result != null
			result.size() == 12
			result.every { it.value != null }
			result.findAll {ActionWordTextValueDto.class.isAssignableFrom(it.class) }.size() == 7
			result.findAll {ActionWordParameterValueDto.class.isAssignableFrom(it.class) }.size() == 5
			result.findAll {it.keywordStepId == -2L }.size() == 4
	}


}
