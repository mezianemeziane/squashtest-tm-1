/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.service.internal.display.grid.DataRow
import org.squashtest.tm.service.internal.display.grid.GridFilterValue
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

/**
 * @author qtran - created on 16/11/2020
 *
 */
@UnitilsSupport
@DataSet
@Transactional
class ModificationHistoryDisplayDaoIT extends DbunitDaoSpecification {
	@Inject
	private ModificationHistoryDisplayDao dao
	private requestParam = new GridRequest()

	def setup() {
		requestParam.setPage(0)
		requestParam.setSize(25)
		requestParam.setSort(new ArrayList<GridSort>())
		requestParam.setFilterValues(new ArrayList<GridFilterValue>())
		requestParam.setScope(new ArrayList<String>())
	}

	def "should find requirement version modification history by id"() {
		when:
		def grid = dao.findGridByRequirementVersionId(-11L, requestParam)

		then:
		grid != null
		grid.count == 1
		grid.dataRows.size() == 1
		DataRow row1 = grid.dataRows.get(0)
		row1.id == "-1"
		def data = row1.data
		data.size() == 8
		data.get("date") != null
		data.get("eventId") == -1
		data.get("event") == "status"
		data.get("user") == "me"
		data.get("oldValue") == "WORK_IN_PROGRESS"
		data.get("newValue") == "UNDER_REVIEW"
	}

	def "should find requirement version modification history by id (description changed)"() {
		when:
		def grid = dao.findGridByRequirementVersionId(-21L, requestParam)

		then:
		grid != null
		grid.count == 1
		grid.dataRows.size() == 1
		DataRow row1 = grid.dataRows.get(0)
		row1.id == "-2"
		def data = row1.data
		data.size() == 8
		data.get("date") != null
		data.get("eventId") == -2
		data.get("event") == "description"
		data.get("user") == "me"
		data.get("oldValue") == "another nice requirement"
		data.get("newValue") == "<p><strong>Hello;</strong> world;</p>"
	}
}
