/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.spockframework.util.NotThreadSafe
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.NodeReference
import org.squashtest.tm.domain.NodeReferences
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

import static org.squashtest.tm.domain.NodeType.*

@UnitilsSupport
@Transactional
@NotThreadSafe
@DataSet("TreeBrowserDaoIT.xml")
class MultipleHierarchyTreeBrowserDaoIT extends DbunitServiceSpecification {

	@Inject
	MultipleHierarchyTreeBrowserDao treeBrowser

	def "should find children reference of a test case library"() {
		when:
		def ids = new HashSet<NodeReference>()
		def reference = new NodeReference(TEST_CASE_LIBRARY, -1L)
		ids.add(reference)

		def children = treeBrowser.findChildrenReference(ids)

		then:
		// expected [[TEST_CASE:-1], [TEST_CASE_FOLDER:-2], [TEST_CASE_FOLDER:-4],[TEST_CASE:-3]]
		children.size() == 4
		children.get(reference).get(0).id == -1L
		children.get(reference).get(0).nodeType == TEST_CASE
		children.get(reference).get(1).id == -2L
		children.get(reference).get(1).nodeType == TEST_CASE_FOLDER
		children.get(reference).get(2).id == -4L
		children.get(reference).get(2).nodeType == TEST_CASE_FOLDER
		children.get(reference).get(3).id == -3L
		children.get(reference).get(3).nodeType == TEST_CASE
	}

	def "should find children reference of a test case folder"() {
		when:
		def ids = new HashSet<NodeReference>()
		def reference = new NodeReference(TEST_CASE_FOLDER, -2L)
		ids.add(reference)

		def children = treeBrowser.findChildrenReference(ids)

		then:
		// expected [[TEST_CASE:-7], [TEST_CASE:-6], [TEST_CASE:-5]]
		children.size() == 3
		children.get(reference).get(0).id == -7L
		children.get(reference).get(0).nodeType == TEST_CASE
		children.get(reference).get(1).id == -6L
		children.get(reference).get(1).nodeType == TEST_CASE
		children.get(reference).get(2).id == -5L
		children.get(reference).get(2).nodeType == TEST_CASE
	}

	def "should find children reference of various test case library nodes"() {
		when:
		def ids = new HashSet<NodeReference>()
		def libReference = new NodeReference(TEST_CASE_LIBRARY, -1L)
		ids.add(libReference)

		def tcf2 = new NodeReference(TEST_CASE_FOLDER, -2L)
		ids.add(tcf2)

		def tcf4 = new NodeReference(TEST_CASE_FOLDER, -4L)
		ids.add(tcf4)

		def tcf8 = new NodeReference(TEST_CASE_FOLDER, -8L)
		ids.add(tcf8)

		def children = treeBrowser.findChildrenReference(ids)

		then:
		// TestCaseLibrary-1
		// expected children reference [[TEST_CASE:-1], [TEST_CASE_FOLDER:-2], [TEST_CASE_FOLDER:-4],[TEST_CASE:-3]]
		children.get(libReference).size() == 4
		children.get(libReference).get(0).id == -1L
		children.get(libReference).get(0).nodeType == TEST_CASE
		children.get(libReference).get(1).id == -2L
		children.get(libReference).get(1).nodeType == TEST_CASE_FOLDER
		children.get(libReference).get(2).id == -4L
		children.get(libReference).get(2).nodeType == TEST_CASE_FOLDER
		children.get(libReference).get(3).id == -3L
		children.get(libReference).get(3).nodeType == TEST_CASE

		// TestCaseFolder-2
		// expected children reference [[TEST_CASE:-7], [TEST_CASE:-6], [TEST_CASE:-5]]
		children.get(tcf2).size() == 3
		children.get(tcf2).get(0).id == -7L
		children.get(tcf2).get(0).nodeType == TEST_CASE
		children.get(tcf2).get(1).id == -6L
		children.get(tcf2).get(1).nodeType == TEST_CASE
		children.get(tcf2).get(2).id == -5L
		children.get(tcf2).get(2).nodeType == TEST_CASE

		// TestCaseFolder-8
		children.get(tcf8).size() == 1
	}

	def "should find children reference of a campaign library"() {
		when:
		def ids = new HashSet<NodeReference>()
		def reference = new NodeReference(CAMPAIGN_LIBRARY, -1L)
		ids.add(reference)

		def children = treeBrowser.findChildrenReference(ids)

		then:
		// expected [[CAMPAIGN_FOLDER:-31], [CAMPAIGN:-33]]
		children.size() == 2
		children.get(reference).get(0).id == -31L
		children.get(reference).get(0).nodeType == CAMPAIGN_FOLDER
		children.get(reference).get(1).id == -33L
		children.get(reference).get(1).nodeType == CAMPAIGN
	}

	def "should find children reference of various campaign workspace nodes"() {
		when:
		def folder1 = new NodeReference(CAMPAIGN_FOLDER, -31L)
		def folder1_1 = new NodeReference(CAMPAIGN_FOLDER, -32L)
		def campaign = new NodeReference(CAMPAIGN, -33L)
		def iteration1 = new NodeReference(ITERATION, -1L)
		def iteration2 = new NodeReference(ITERATION, -2L)
		def iteration3 = new NodeReference(ITERATION, -3L)

		def ids = new HashSet<NodeReference>()
		ids.addAll(folder1, folder1_1, campaign, iteration1, iteration2, iteration3);

		def children = treeBrowser.findChildrenReference(ids)

		then:
		children.get(folder1).size() == 2
		children.get(folder1).get(0).id == -32L
		children.get(folder1).get(0).nodeType == CAMPAIGN_FOLDER
		children.get(folder1).get(1).id == -34L
		children.get(folder1).get(1).nodeType == CAMPAIGN

		children.get(folder1_1).size() == 1
		children.get(folder1_1).get(0).id == -35L
		children.get(folder1_1).get(0).nodeType == CAMPAIGN

		children.get(campaign).size() == 2
		children.get(campaign).get(0).id == -1L
		children.get(campaign).get(0).nodeType == ITERATION
		children.get(campaign).get(1).id == -2L
		children.get(campaign).get(1).nodeType == ITERATION

		children.get(iteration1).size() == 2
		children.get(iteration1).get(0).id == -1L
		children.get(iteration1).get(0).nodeType == TEST_SUITE
		children.get(iteration1).get(1).id == -2L
		children.get(iteration1).get(1).nodeType == TEST_SUITE

		children.get(iteration2).size() == 1
		children.get(iteration2).get(0).id == -3L
		children.get(iteration2).get(0).nodeType == TEST_SUITE

		children.get(iteration3).size() == 0
	}

	@Unroll("#descendantIds should have ancestors : #expectedAncestors")
	def "should find ancestor reference of various workspace nodes"() {
		when:
		def ancestors = treeBrowser.findAncestors(new NodeReferences(descendantIds))

		then:
		ancestors == expectedAncestors as Set

		where:
		descendantIds                                   | expectedAncestors
		[new NodeReference(TEST_CASE_LIBRARY, -1L)]  | []

		[new NodeReference(TEST_CASE, -1L)]          | [new NodeReference(TEST_CASE_LIBRARY, -1L)]

		[new NodeReference(TEST_CASE, -5L),
		 new NodeReference(TEST_CASE, -1L)]          | [new NodeReference(TEST_CASE_LIBRARY, -1L),
														new NodeReference(TEST_CASE_FOLDER, -2L)]

		[new NodeReference(TEST_CASE, -9L)]          | [new NodeReference(TEST_CASE_FOLDER, -8L),
														new NodeReference(TEST_CASE_LIBRARY, -1L),
														new NodeReference(TEST_CASE_FOLDER, -4L)]

		[new NodeReference(TEST_CASE_FOLDER, -8L)]   | [new NodeReference(TEST_CASE_FOLDER, -4L),
														new NodeReference(TEST_CASE_LIBRARY, -1L)]

		[new NodeReference(TEST_CASE, -9L),
		 new NodeReference(TEST_CASE_FOLDER, -8L),
		 new NodeReference(TEST_CASE, -5L)]          | [new NodeReference(TEST_CASE_FOLDER, -8L),
														new NodeReference(TEST_CASE_LIBRARY, -1L),
														new NodeReference(TEST_CASE_FOLDER, -4L),
														new NodeReference(TEST_CASE_FOLDER, -2L)]

		[new NodeReference(TEST_CASE, -1L),
		 new NodeReference(TEST_CASE, -10L)]         | [new NodeReference(TEST_CASE_LIBRARY, -1L),
														new NodeReference(TEST_CASE_LIBRARY, -20L)]

		[new NodeReference(CAMPAIGN, -35L)]          | [new NodeReference(CAMPAIGN_LIBRARY, -1L),
														new NodeReference(CAMPAIGN_FOLDER, -31L),
														new NodeReference(CAMPAIGN_FOLDER, -32L)]

		[new NodeReference(TEST_SUITE, -2L)]         | [new NodeReference(CAMPAIGN_LIBRARY, -1L),
														new NodeReference(CAMPAIGN, -33L),
														new NodeReference(ITERATION, -1L)]

		[new NodeReference(TEST_SUITE, -2L),
		 new NodeReference(ITERATION, -3L),
		 new NodeReference(CAMPAIGN_FOLDER, -36L)]   | [new NodeReference(CAMPAIGN_LIBRARY, -1L),
														new NodeReference(CAMPAIGN_LIBRARY, -2L),
														new NodeReference(CAMPAIGN, -33L),
														new NodeReference(ITERATION, -1L),
														new NodeReference(CAMPAIGN, -34L),
														new NodeReference(CAMPAIGN_FOLDER, -31L)]

		[new NodeReference(REQUIREMENT_FOLDER, -3L)] | [new NodeReference(REQUIREMENT_LIBRARY, -1L),
														new NodeReference(REQUIREMENT_FOLDER, -2L)]

		[new NodeReference(REQUIREMENT, -5L)]        | [new NodeReference(REQUIREMENT_LIBRARY, -1L),
														new NodeReference(REQUIREMENT_FOLDER, -2L),
														new NodeReference(REQUIREMENT_FOLDER, -3L),
														new NodeReference(REQUIREMENT, -4L)]
	}
}
