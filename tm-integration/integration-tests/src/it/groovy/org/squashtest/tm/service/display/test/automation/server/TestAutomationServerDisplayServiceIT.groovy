/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.test.automation.server

import org.jooq.DSLContext
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.squashtest.tm.service.internal.display.test.automation.server.TestAutomationServerDisplayServiceImpl
import org.squashtest.tm.service.internal.repository.TestAutomationServerDao
import org.squashtest.tm.service.internal.repository.display.TestAutomationServerDisplayDao
import org.squashtest.tm.service.servers.StoredCredentialsManager
import org.squashtest.tm.service.testautomation.TestAutomationServerCredentialsService
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.text.SimpleDateFormat

@UnitilsSupport
@Transactional
@DataSet
class TestAutomationServerDisplayServiceIT extends DbunitServiceSpecification {

	private TestAutomationServerDisplayService testAutomationServerDisplayService

	@Inject	DSLContext dslContext
	@Inject	TestAutomationServerDisplayDao testAutomationServerDisplayDao
	@Inject	StoredCredentialsManager storedCredentialsManager
	@Inject	TestAutomationServerDao testAutomationServerDao

	def setup() {
		testAutomationServerDisplayService = new TestAutomationServerDisplayServiceImpl(
				dslContext,
				testAutomationServerDisplayDao,
				storedCredentialsManager,
				Stub(TestAutomationServerCredentialsService),
				testAutomationServerDao
		)
	}

	def "should fetch test automation servers"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		when:
		def gridResponse = testAutomationServerDisplayService.getTestAutomationServerGrid(gridRequest)

		then:
		gridResponse.count == 2
		def dataServer1 = gridResponse.dataRows.get(0).data
		def dataServer2 = gridResponse.dataRows.get(1).data

		dataServer1.serverId == -1L
		dataServer1.name == "testServer1"
		dataServer1.baseUrl == "http:192.168.0.03:9090/testServer1"
		dataServer1.executionCount == 0

		dataServer2.serverId == -2L
		dataServer2.name == "testServer2"
		dataServer2.baseUrl == "http:192.168.0.03:9090/testServer2"
		dataServer2.executionCount == 1
	}

	@Unroll("should allow sorting on #column")
	def "should allow sorting"() {
		when:
		def gridRequest = new GridRequest()
		gridRequest.sort.add(new GridSort(column, GridSort.SortDirection.ASC))
		gridRequest.setSize(25)
		this.testAutomationServerDisplayService.getTestAutomationServerGrid(gridRequest)

		then:
		noExceptionThrown()

		where:
		column << ["serverId",
				   "name",
				   "baseUrl",
				   "executionCount"]
	}

	def "should fetch test automation server view dto for a given test automation server id"() {
		given:
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd")

		when:
		def testAutomationServerViewDto = testAutomationServerDisplayService.getTestAutomationServerView(-1L)

		then:
		testAutomationServerViewDto.name == "testServer1"
		testAutomationServerViewDto.baseUrl == "http:192.168.0.03:9090/testServer1"
		testAutomationServerViewDto.kind == TestAutomationServerKind.jenkins.name()
		!testAutomationServerViewDto.manualSlaveSelection
		testAutomationServerViewDto.createdBy == "admin"
		testAutomationServerViewDto.createdOn == dateFormat.parse("2020-04-22")
	}
}
