/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.custom.field

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject


@UnitilsSupport
@Transactional
@DataSet
class CustomFieldDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private CustomFieldDisplayService customFieldDisplayService

	def "should fetch a custom fields grid"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		when:
		def gridResponse = this.customFieldDisplayService.findAll(gridRequest)

		then:
		gridResponse.count == 13
		def rows = gridResponse.dataRows

		// Rows are sorted by name ASC
		rows.get(0).id == "-13"
		rows.get(0).data.get("cfId") == -13
		rows.get(0).data.get("name") == "opt_check"
		rows.get(0).data.get("label") == "opt_check_label"
		rows.get(0).data.get("code") == "ocheck"
		rows.get(0).data.get("inputType") == "CHECKBOX"
		rows.get(0).data.get("optional") == true

		rows.get(12).id == "-1"
		rows.get(12).data.get("cfId") == -1
		rows.get(12).data.get("name") == "req_text"
		rows.get(12).data.get("label") == "req_text_label"
		rows.get(12).data.get("code") == "rt"
		rows.get(12).data.get("inputType") == "PLAIN_TEXT"
		rows.get(12).data.get("optional") == false
	}

	@Unroll("should allow sorting on #column")
	def "should allow sorting"() {
		when:
		def gridRequest = new GridRequest()
		gridRequest.sort.add(new GridSort(column, GridSort.SortDirection.ASC))
		gridRequest.setSize(25)
		this.customFieldDisplayService.findAll(gridRequest)

		then:
		noExceptionThrown()

		where:
		column << ["name",
				   "label",
				   "code",
				   "inputType",
				   "optional"]
	}
}
