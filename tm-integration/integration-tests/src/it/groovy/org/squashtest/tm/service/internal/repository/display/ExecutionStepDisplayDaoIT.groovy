/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
class ExecutionStepDisplayDaoIT extends DbunitDaoSpecification {

	@Inject
	ExecutionStepDisplayDao executionStepDisplayDao

	@DataSet
	@Unroll
	def "should retrieve executions by test case"() {

		when:
		def stepViews = executionStepDisplayDao.findByExecutionId(executionID)

		then:
		stepViews.size() == expectedSize
		for (int i = 0; i < expectedIds.size(); i++) {
			stepViews.get(i).id == expectedIds.get(i)
		}

		where:
		executionID || expectedSize | expectedIds
		-1L         || 5            | [-1, -2, -3, -5, -4]
		-2L         || 0            | []
	}

	@DataSet
	def "should retrieve steps for one execution"() {

		when:
		def stepViews = executionStepDisplayDao.findByExecutionId(-1L)

		then:
		def firstStep = stepViews.get(0)
		firstStep.executionStatus == 'SUCCESS'
		firstStep.action == "action 1"
		firstStep.expectedResult == "result 1"
		firstStep.comment == "a comment"
		firstStep.attachmentList.id == -11
		firstStep.attachmentList.attachments.size() == 2
		firstStep.customFieldValues.size() == 1
		firstStep.denormalizedCustomFieldValues.size() == 2

		def secondStep = stepViews.get(1)
		secondStep.executionStatus == 'READY'
		secondStep.action == "action 2"
		secondStep.expectedResult == "result 2"
		secondStep.attachmentList.id == -12
		secondStep.attachmentList.attachments.size() == 1

	}

}
