/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.search

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.query.QueryColumnPrototypeReference
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import javax.persistence.EntityManager

@UnitilsSupport
@Transactional
@DataSet
class TestCaseThroughRequirementSearchServiceIT extends DbunitServiceSpecification {

	static boolean isH2() {
		return System.properties['jooq.sql.dialect'] == null || System.properties['jooq.sql.dialect'] == 'H2'
	}

	def setBidirectionalReqReqVersion(Long reqId, Long reqVersionId) {
		def reqVer = entityManager.find(RequirementVersion.class, reqVersionId)
		def req = entityManager.find(Requirement.class, reqId)
		reqVer.setRequirement(req)
	}

	def setup() {
		def ids = [
			[-2L, -2L],
			[-3L, -3L],
			[-3L, -33L],
			[-4L, -4L],
			[-5L, -5L],
			[-5L, -15L],
			[-7L, -7L],
		]
		ids.each {
			setBidirectionalReqReqVersion(it[0], it[1])
		}
	}

	@Inject
	EntityManager entityManager

	@Inject
	TestCaseThroughRequirementSearchService service

	def "should find a search result"() {
		given:
		GridRequest request = new GridRequest()
		request.setPage(0)
		request.setSize(2)
		request.setScope(["Project--1"])

		when:
		ResearchResult result = service.search(request)

		then:
		result != null
		result.ids.size() == 2
		result.count == 4
	}

	def "should gracefully handle empty result"() {
		given:
		GridRequest request = new GridRequest()
		request.setPage(0)
		request.setSize(2)
		request.setScope(["Project--3"])

		when:
		ResearchResult result = service.search(request)

		then:
		result != null
		result.ids.size() == 0
		result.count == 0
	}

	def "should gracefully(!) throw if incorrect perimeter is used"() {
		given:
		GridRequest request = new GridRequest()
		request.setPage(0)
		request.setSize(2)
		request.setScope(["Project--5"])

		when:
		ResearchResult result = service.search(request)

		then:
		thrown IllegalArgumentException
	}

	def "should find a paginated result"() {
		given:
		GridRequest request = new GridRequest()
		request.setPage(1)
		request.setSize(2)
		request.setScope(["Project--1"])

		when:
		ResearchResult result = service.search(request)

		then:
		result != null
		result.ids == [-112L, -111L]
		result.count == 4
	}

	def "should find a sorted result"() {
		given:
		GridRequest request = new GridRequest()
		request.setPage(0)
		request.setSize(4)
		request.setScope(["Project--1"])
		def sort = new GridSort()
		sort.setColumnPrototype(QueryColumnPrototypeReference.TEST_CASE_IMPORTANCE)
		sort.setDirection(GridSort.SortDirection.ASC)
		sort.setProperty("importance")
		request.setSort([sort])

		when:
		ResearchResult result = service.search(request)

		then:
		result != null
		result.ids == [-112L, -114L, -113L, -111L]
		result.count == 4
	}

}
