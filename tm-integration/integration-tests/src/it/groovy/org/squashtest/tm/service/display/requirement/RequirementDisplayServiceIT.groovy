/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.requirement

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.core.foundation.lang.DateUtils
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.squashtest.tm.service.display.requirements.RequirementDisplayService
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionDto
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import javax.persistence.EntityManager

@UnitilsSupport
@Transactional
@DataSet
class RequirementDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	ActiveMilestoneHolder activeMilestoneHolder

	@Inject
	RequirementDisplayService requirementDisplayService

	@Inject
	EntityManager entityManager

	def setBidirectionalReqReqVersion(Long reqId, Long reqVersionId) {
		def reqVer = entityManager.find(RequirementVersion.class, reqVersionId)
		def req = entityManager.find(Requirement.class, reqId)
		reqVer.setRequirement(req)
		// Flush is necessary as the dao use native query through jooq.
		entityManager.flush()
	}

	def setup() {
		activeMilestoneHolder.setActiveMilestone(-1L)
		def ids = [
			[-2L, -2L],
			[-3L, -3L],
			[-3L, -33L],
			[-4L, -4L],
			[-5L, -5L],
			[-5L, -15L],
			[-7L, -7L],
		]
		ids.each {
			setBidirectionalReqReqVersion(it[0], it[1])
		}
	}

	def cleanup() {
		activeMilestoneHolder.clearContext()
	}

	def "should fetch last versions"() {
		when:
		RequirementVersionDto requirementVersion = this.requirementDisplayService.findCurrentVersionView(requirementId)

		then:
		requirementVersion.id == expectedVersionId

		where:
		requirementId || expectedVersionId
		-2L           || -2L
		-3L           || -33L
		-4L           || -4L
		-5L           || -15L // Milestone mode
	}

	def "should refuse not existing requirementIds"() {
		when:
		this.requirementDisplayService.findCurrentVersionView(requirementId)

		then:
		thrown(IllegalArgumentException.class)

		where:
		requirementId << [-1L, -90L]
	}


	def "should fetch on version in details"() {
		when:
		def requirementVersion = this.requirementDisplayService.findCurrentVersionView(-4L)

		then:
		requirementVersion.id == -4L
		requirementVersion.name == "Requirement_3"
		requirementVersion.projectId == -1L
		requirementVersion.attachmentListId == -1L
		requirementVersion.customFieldValues.size() == 1
		requirementVersion.category == 1
		requirementVersion.criticality == "UNDEFINED"
		requirementVersion.status == "WORK_IN_PROGRESS"
		requirementVersion.createdBy == "admin"
		requirementVersion.createdOn == DateUtils.parseIso8601Date("2020-01-01")
		def customFieldValueDto = requirementVersion.customFieldValues.get(0)
		customFieldValueDto.boundEntityId == -4L
		customFieldValueDto.value == "Annyong Haseyo"
		def verifyingTestCases = requirementVersion.verifyingTestCases
		verifyingTestCases.size() == 1
		def verifyingTestCase = verifyingTestCases.get(0)
		verifyingTestCase.name == "test case 1"
		verifyingTestCase.id == -111L
	}

	def "should find versions of requirement" () {

		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		when:

		def requirementVersionGrid = requirementDisplayService.findVersionsByRequirementId(-3 ,gridRequest)

		then:

		requirementVersionGrid.count == 2
		def lastVersion = requirementVersionGrid.dataRows.get(0)
		lastVersion.data.get('versionNumber') == 1
		lastVersion.data.get('coverages') == 2
		lastVersion.data.get('reference') == ""
		lastVersion.data.get('criticality') == "UNDEFINED"
		lastVersion.data.get('resId') == -33L
		lastVersion.data.get('name') == "Requirement_2_v2"
		lastVersion.data.get('category') == 1L
		lastVersion.data.get('requirementStatus') == "WORK_IN_PROGRESS"
		lastVersion.data.get('links') == 0


		def firstVersion = requirementVersionGrid.dataRows.get(1)
		firstVersion.data.get('versionNumber') == 0
		firstVersion.data.get('coverages') == 0
		firstVersion.data.get('reference') == ""
		firstVersion.data.get('criticality') == "UNDEFINED"
		firstVersion.data.get('resId') == -3L
		firstVersion.data.get('name') == "Requirement_2_v1"
		firstVersion.data.get('category') == 1L
		firstVersion.data.get('requirementStatus') == "WORK_IN_PROGRESS"
		firstVersion.data.get('links') == 0
	}
}
