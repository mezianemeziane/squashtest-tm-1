INSERT INTO ATTACHMENT_LIST (ATTACHMENT_LIST_ID) VALUES
(-780),
(-784),
(-788),
(-792),
(-793),
(-794);

INSERT INTO THIRD_PARTY_SERVER (SERVER_ID, NAME, URL, AUTH_POLICY, AUTH_PROTOCOL) VALUES
(-2, 'GitHubBis', 'https://github.com', 'APP_LEVEL', 'BASIC_AUTH'),
(-3, 'GitHub2', 'https://github2.com/', 'APP_LEVEL', 'BASIC_AUTH'),
(-4, 'GitHub3', 'https://github.com/RecetteSquash', 'APP_LEVEL', 'BASIC_AUTH'),
(-5, 'GitHub4', 'https://othergithub.com', 'APP_LEVEL', 'BASIC_AUTH');

INSERT INTO SCM_SERVER (SERVER_ID, KIND) VALUES
(-2, 'git'),
(-3, 'git'),
(-4, 'git'),
(-5, 'git');

INSERT INTO SCM_REPOSITORY (SCM_REPOSITORY_ID, SERVER_ID, NAME, REPOSITORY_PATH, WORKING_BRANCH) VALUES
(-50, -3, 'Repository-1', 'home/test/repo1', 'master'),
(-51, -2, 'Repository-2', 'home/test/repo2', 'master');

-- Update existed test cases with created scm servers, will create repositories if don't exist --
UPDATE TEST_CASE SET SOURCE_CODE_REPOSITORY_URL = 'https://github.com/Repository-1' WHERE TCLN_ID = -200;
UPDATE TEST_CASE SET SOURCE_CODE_REPOSITORY_URL = 'https://github.com/Repository-2' WHERE TCLN_ID = -123;
UPDATE TEST_CASE SET SOURCE_CODE_REPOSITORY_URL = 'https://github.com/Repository-2' WHERE TCLN_ID = -122;
UPDATE TEST_CASE SET SOURCE_CODE_REPOSITORY_URL = 'https://github2.com/Repository-1' WHERE TCLN_ID = -121;
UPDATE TEST_CASE SET SOURCE_CODE_REPOSITORY_URL = 'https://github2.com/Repository-2' WHERE TCLN_ID = -100;
UPDATE TEST_CASE SET SOURCE_CODE_REPOSITORY_URL = 'https://othergithub.com/default' WHERE TCLN_ID = -2;
UPDATE TEST_CASE SET SOURCE_CODE_REPOSITORY_URL = 'https://github.com/RecetteSquash/recette' WHERE TCLN_ID = -1;

UPDATE PROJECT SET SCM_REPOSITORY_ID = -32 WHERE PROJECT_ID = 3;

-- Add some others with non existed scm servers, will create respective servers and its repositories --
INSERT INTO TEST_CASE_LIBRARY_NODE (TCLN_ID, DESCRIPTION, NAME, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, PROJECT_ID, ATTACHMENT_LIST_ID) VALUES
(-216, '<p>The Wheel of Time</p>', 'JDD-TC04', 'yoda', '2012-08-26 00:00:00', NULL, NULL, 4, -780),
(-217, '<p>Lord of the Rings</p>', 'JDD-TC03', 'gandalf', '1965-08-18 00:00:00', NULL, NULL, 4, -784),
(-218, '<p>The Silmarillion</p>', 'JDD-TC02', 'gandalf', '1985-08-17 00:00:00', NULL, NULL, 4, -788),
(-219, '<p>LOTR</p>', 'JDD-TC01', 'gandalf', '1985-08-17 00:00:00', NULL, NULL, 4, -792),
(-220, '<p>LOTR2</p>', 'JDD-TC02', 'gandalf', '1985-08-17 00:00:00', NULL, NULL, 4, -793),

(-221, '<p>LOTR3</p>', 'JDD-TC03', 'gandalf', '1985-08-17 00:00:00', NULL, NULL, 3, -794);

INSERT INTO TEST_CASE (TCLN_ID, VERSION, EXECUTION_MODE, IMPORTANCE, IMPORTANCE_AUTO, PREREQUISITE, REFERENCE, TA_TEST, TC_STATUS, UUID, SOURCE_CODE_REPOSITORY_URL) VALUES
(-216, 1, 'MANUAL', 'MEDIUM', false, '<ol><li>Being an Aes Sedai or an Asha''man</li></ol>', '', NULL, 'WORK_IN_PROGRESS', 'fc8c56d7-7540-4db0-892d-8b774db23ab1', 'https://git.fr/RecetteSquash/recette'),
(-217, 1, 'MANUAL', 'LOW', false, '<ol><li>Knowledge of Middle-Earth</li></ol>', '', NULL, 'WORK_IN_PROGRESS', 'fc8c56d7-7540-4db0-892d-8b774db23ab2', null),
(-218, 1, 'MANUAL', 'VERY_HIGH', false, '<ol><li>Knowledge of Sindarin</li></ol>', '', NULL, 'WORK_IN_PROGRESS', 'fc8c56d7-7540-4db0-892d-8b774db23ab3', 'https://git.fr/Repository-2'),
(-219, 1, 'MANUAL', 'HIGH', false, '<ol><li>Knowledge of test case creation</li></ol>', '', NULL, 'WORK_IN_PROGRESS', 'fc8c56d7-7540-4db0-892d-8b774db23ab4', 'https://git.fr/Repository-2'),
(-220, 1, 'MANUAL', 'HIGH', false, '<ol><li>Knowledge of test case creation2</li></ol>', '', NULL, 'WORK_IN_PROGRESS', 'fc8c56d7-7540-4db0-892d-8b774db23ab5', '     https://git.fr/Repository-2/    '),

(-221, 1, 'MANUAL', 'HIGH', false, '<ol><li>Knowledge of test case creation3</li></ol>', '', NULL, 'WORK_IN_PROGRESS', 'fc8c56d7-7540-4db0-892d-8b774db23ab6', ' https://github.com/Repository-2/');
