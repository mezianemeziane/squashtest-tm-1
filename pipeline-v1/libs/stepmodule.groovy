/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
	Other utilities pertaining to usage in a pipeline


 */


/**
 * Checks whether the current build is a release build. It assumes the existence of the
 * build parameters 'build_type'
 *
 * @param that : an instance of the WorkflowScript (basically, just invoke isRelease(this) from withing the pipeline)
 * @return
 */
/*
    A build is a release when :
    - the user choose 'release',
    - both release and next versions are set
 */
def isRelease(that){

	// guard condition
	if (params.build_type != 'release'){
		that.echo "this build is a regular build - skipping release"
		return false;
	}


	def confOK = (
		params.build_type == 'release' &&
			params.release_version.trim().length() > 0 &&
			params.next_version.trim().length() > 0
	)

	// notify the user in case of configuration error
	if (params.build_type == 'release' && (!confOK)){
		that.error "Cannot release : either the release version or next dev version is blank"
	}

	return confOK
}

/**
 * Mark the build as a release build in the Jenkins history. If possible it attempts to use the usual Maven release icon if available,
 * else it falls back to a default save icon. Please note that the Administrator needs to accept it in the whitelist of
 * the sandbox policy.
 *
 * @param that : the instance of WorkflowScript (just call addReleaseInfo(this))
 * @return
 */

// Note : the fallback in case the Maven plugin isn't available doesn't work : the try catch is totally bypassed
// but I'm not sure I remember what happens exactly. Anyway, just make sure the Maven plugin is available.
def addReleaseInfo(that){

	def msg = "Release - ${params.release_version}"
	def icon = null

	// is the Maven release plugin available ? If so we can use its icon
	try{
		// testing by looking for a specific class
		def m2badge = new org.jvnet.hudson.plugins.m2release.M2ReleaseBadgeAction()
		icon = '/plugin/m2release/img/releasebadge.png'
	}

	// else fallback to default icon
	catch(ClassNotFoundException |
		NoClassDefFoundError |
		org.jenkinsci.plugins.scriptsecurity.sandbox.RejectedAccessException |
		org.jenkinsci.plugins.workflow.cps.CpsCompilationErrorsException ex){

		that.warning """note : Maven Release plugin is not available, or action 'org.jvnet.hudson.plugins.m2release.M2ReleaseBadgeAction' is not approved by the \
        sandbox administrator. Thus we cannot use M2 release badge - using a generic icon instead."""

		icon = '/plugin/badge/images/save.gif'
	}

	addBadge icon: icon,text : msg

}



return this
