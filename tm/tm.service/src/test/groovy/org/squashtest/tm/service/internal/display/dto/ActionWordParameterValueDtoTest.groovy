/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto

import org.squashtest.tm.service.internal.display.dto.testcase.ActionWordParameterValueDto
import spock.lang.Specification

class ActionWordParameterValueDtoTest extends Specification {

	def "#getUnstyledAction() and #getStyledAction()"() {
		given:
			ActionWordParameterValueDto paramValueDto = new ActionWordParameterValueDto(3, value)
		when:
			String result = paramValueDto.getUnstyledAction()
		then:
			result == expectedResult
		where:
			value		| expectedResult
			"hello"		| "\"hello\""
			"2"			| "2"
			"<name>"	| "<name>"
			"\"\""		| "\"\""
	}

	def "#getStyledAction()"() {
		given:
			ActionWordParameterValueDto paramValueDto = new ActionWordParameterValueDto(3, value)
		when:
			String result = paramValueDto.getStyledAction()
		then:
			result == expectedResult
		where:
			value		| expectedResult
			"hello"		| "<span style=\"color: blue;\">hello</span>"
			"2"			| "<span style=\"color: blue;\">2</span>"
			"<name>"	| "<span style=\"color: blue;\">&lt;name&gt;</span>"
			"\"\""		| "<span style=\"color: blue;\">\"\"</span>"
	}
}
