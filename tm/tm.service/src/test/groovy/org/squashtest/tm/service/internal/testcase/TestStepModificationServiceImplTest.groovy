/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testcase


import org.squashtest.tm.domain.testcase.KeywordTestStep
import org.squashtest.tm.service.internal.repository.KeywordTestStepDao
import org.squashtest.tm.service.testcase.TestStepModificationService
import spock.lang.Specification

import static org.squashtest.tm.domain.bdd.Keyword.AND
import static org.squashtest.tm.domain.bdd.Keyword.BUT
import static org.squashtest.tm.domain.bdd.Keyword.GIVEN
import static org.squashtest.tm.domain.bdd.Keyword.THEN
import static org.squashtest.tm.domain.bdd.Keyword.WHEN

class TestStepModificationServiceImplTest extends Specification {

	CustomTestStepModificationServiceImpl testStepModificationService = new CustomTestStepModificationServiceImpl()
	KeywordTestStepDao keywordTestStepDao = Mock()

	def setup() {
		testStepModificationService.keywordTestStepDao = keywordTestStepDao
	}

	def "should update the keyword of a keyword step"() {
		given:
			KeywordTestStep step = new KeywordTestStep()
			step.setKeyword(AND)
		and:
			1 * keywordTestStepDao.findById(10L) >> step
		when:
			testStepModificationService.updateKeywordTestStep(10L, newKeyword)
		then:
			step.keyword == newKeyword
		where:
			newKeyword << [GIVEN, WHEN, THEN, AND, BUT]
	}
}
