/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.administration;


import org.jooq.*;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

import java.util.Arrays;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_LINK_TYPE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_LINK;

public class RequirementsLinksGrid extends AbstractGrid {

	private static final String LINK_COUNT = "LINK_COUNT";

	private static final String LINK_TYPE_ID = "LINK_TYPE_ID";

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(DSL.field("TYPE_ID")),
			new GridColumn(DSL.field("ROLE_1")),
			new GridColumn(DSL.field("ROLE_1_CODE")),
			new GridColumn(DSL.field("ROLE_2")),
			new GridColumn(DSL.field("ROLE_2_CODE")),
			new GridColumn(DSL.field("IS_DEFAULT")),
			new GridColumn(DSL.isnull(DSL.field(LINK_COUNT, Integer.class),0).as(LINK_COUNT))
			);
	}

	@Override
	protected Table<?> getTable() {

		SelectHavingStep<Record2<Long, Integer>> requirementVersionLinkCount = getRequirementVersionLinkCount();

		return DSL.select(
			REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID,
			REQUIREMENT_VERSION_LINK_TYPE.ROLE_1,
			REQUIREMENT_VERSION_LINK_TYPE.ROLE_1_CODE,
			REQUIREMENT_VERSION_LINK_TYPE.ROLE_2,
			REQUIREMENT_VERSION_LINK_TYPE.ROLE_2_CODE,
			REQUIREMENT_VERSION_LINK_TYPE.IS_DEFAULT,
			requirementVersionLinkCount.field(LINK_COUNT)
		).from(REQUIREMENT_VERSION_LINK_TYPE)
			.leftJoin(requirementVersionLinkCount).on(requirementVersionLinkCount.field(LINK_TYPE_ID, Long.class).eq(REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID))
			.groupBy(REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID, requirementVersionLinkCount.field(LINK_COUNT))
			.asTable();
	}

	private SelectHavingStep<Record2<Long, Integer>> getRequirementVersionLinkCount() {
		return DSL.select(
			REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID.as(LINK_TYPE_ID),
				DSL.count(REQUIREMENT_VERSION_LINK.LINK_ID).as(LINK_COUNT))
			.from(REQUIREMENT_VERSION_LINK_TYPE)
			.leftJoin(REQUIREMENT_VERSION_LINK).on(REQUIREMENT_VERSION_LINK.LINK_TYPE_ID.eq(REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID))
			.groupBy(REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID);
	}

	@Override
	protected Field<?> getIdentifier() {
		return DSL.field(REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID);
	}

	@Override
	protected Field<?> getProjectIdentifier() {
		return null;
	}

	@Override
	protected SortField<?> getDefaultOrder() {
		return DSL.upper(DSL.field("ROLE_1", String.class)).asc();
	}
}
