/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search.filter;

import com.google.common.collect.Sets;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.hibernate.HibernateQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.jpql.ExtendedHibernateQuery;
import org.squashtest.tm.domain.requirement.QRequirementVersion;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

import java.util.List;
import java.util.Set;

import static org.squashtest.tm.domain.requirement.QRequirementVersion.requirementVersion;
import static org.squashtest.tm.service.internal.display.search.filter.RequirementVersionHasDescriptionFilterHandler.HasDescriptionFilterValue.NO_DESCRIPTION;

@Component
public class RequirementVersionHasDescriptionFilterHandler implements FilterHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequirementVersionHasDescriptionFilterHandler.class);

	private final Set<String> handledPrototypes = Sets.newHashSet("REQUIREMENT_VERSION_HAS_DESCRIPTION");

	@Override
	public boolean canHandleFilter(GridFilterValue filter) {
		return this.handledPrototypes.contains(filter.getColumnPrototype());
	}

	@Override
	public void handleFilter(ExtendedHibernateQuery<?> query, GridFilterValue filter) {
		HasDescriptionFilterValue filterValue = extractFilterValue(filter);

		QRequirementVersion outerVersion = requirementVersion;
		QRequirementVersion initVersion = new QRequirementVersion("initVersion");

		HibernateQuery<?> subquery;

		if (NO_DESCRIPTION.equals(filterValue)) {
			subquery = new ExtendedHibernateQuery<>()
				.select(Expressions.ONE)
				.from(initVersion)
				.where(initVersion.id.eq(outerVersion.id).and(initVersion.description.isEmpty()));
		} else {
			subquery = new ExtendedHibernateQuery<>()
				.select(Expressions.ONE)
				.from(initVersion)
				.where(initVersion.id.eq(outerVersion.id).and(initVersion.description.isNotEmpty()));
		}

		query.where(subquery.exists());
	}

	private HasDescriptionFilterValue extractFilterValue(GridFilterValue filter) {
		List<String> values = filter.getValues();
		if (values.size() != 1) {
			throw new IllegalArgumentException("Invalid value for filter REQUIREMENT_VERSION_HAS_DESCRIPTION");
		}

		return HasDescriptionFilterValue.valueOf(values.get(0));
	}

	public enum HasDescriptionFilterValue {
		HAS_DESCRIPTION, NO_DESCRIPTION;
	}
}
