/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.LibraryDto;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignDto;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignFolderDto;
import org.squashtest.tm.service.internal.display.dto.campaign.IterationPlanningDto;
import org.squashtest.tm.service.internal.repository.display.CampaignDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

@Repository
public class CampaignDisplayDaoImpl implements CampaignDisplayDao {

	private DSLContext dsl;

	public CampaignDisplayDaoImpl(DSLContext dsl) {
		this.dsl = dsl;
	}

	@Override
	public LibraryDto getCampaignLibraryDtoById(Long id) {
		return dsl
			.select(CAMPAIGN_LIBRARY.CL_ID.as(RequestAliasesConstants.ID), CAMPAIGN_LIBRARY.ATTACHMENT_LIST_ID,
				PROJECT.PROJECT_ID, PROJECT.DESCRIPTION, PROJECT.NAME)
			.from(CAMPAIGN_LIBRARY)
			.innerJoin(PROJECT).on(PROJECT.TCL_ID.eq(CAMPAIGN_LIBRARY.CL_ID))
			.where(CAMPAIGN_LIBRARY.CL_ID.eq(id))
			.fetchOne().into(LibraryDto.class);
	}

	@Override
	public CampaignFolderDto getCampaignFolderDtoById(long campaignFolderId) {
		return dsl.select(
			CAMPAIGN_LIBRARY_NODE.CLN_ID.as(RequestAliasesConstants.ID),
			CAMPAIGN_LIBRARY_NODE.ATTACHMENT_LIST_ID, CAMPAIGN_LIBRARY_NODE.DESCRIPTION, CAMPAIGN_LIBRARY_NODE.NAME, CAMPAIGN_LIBRARY_NODE.PROJECT_ID)
			.from(CAMPAIGN_LIBRARY_NODE)
			.where(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(campaignFolderId))
			.fetchOne().into(CampaignFolderDto.class);
	}

	@Override
	public CampaignDto getCampaignDtoById(long campaignId) {
		return dsl.select(
			CAMPAIGN_LIBRARY_NODE.CLN_ID.as(RequestAliasesConstants.ID),
			CAMPAIGN_LIBRARY_NODE.ATTACHMENT_LIST_ID, CAMPAIGN_LIBRARY_NODE.DESCRIPTION, CAMPAIGN_LIBRARY_NODE.NAME, CAMPAIGN_LIBRARY_NODE.PROJECT_ID,
			CAMPAIGN_LIBRARY_NODE.CREATED_BY, CAMPAIGN_LIBRARY_NODE.CREATED_ON, CAMPAIGN_LIBRARY_NODE.LAST_MODIFIED_BY, CAMPAIGN_LIBRARY_NODE.LAST_MODIFIED_ON,
			CAMPAIGN.REFERENCE, CAMPAIGN.CAMPAIGN_STATUS, CAMPAIGN.ACTUAL_END_AUTO, CAMPAIGN.ACTUAL_END_DATE, CAMPAIGN.ACTUAL_START_AUTO,
			CAMPAIGN.ACTUAL_START_DATE, CAMPAIGN.SCHEDULED_START_DATE, CAMPAIGN.SCHEDULED_END_DATE,
			DSL.field(DSL.count(CAMPAIGN_TEST_PLAN_ITEM.DATASET_ID).gt(0)).as("HAS_DATASETS"))
			.from(CAMPAIGN_LIBRARY_NODE)
			.innerJoin(CAMPAIGN).on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
			.leftJoin(CAMPAIGN_TEST_PLAN_ITEM).on(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
			.where(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(campaignId))
			.groupBy(CAMPAIGN_LIBRARY_NODE.CLN_ID, CAMPAIGN.CLN_ID)
			.fetchOne().into(CampaignDto.class);
	}

	@Override
	public List<IterationPlanningDto> findIterationPlanningByCampaign(Long campaignId) {
		return dsl.select(ITERATION.ITERATION_ID.as(RequestAliasesConstants.ID), ITERATION.SCHEDULED_START_DATE,
			ITERATION.SCHEDULED_END_DATE, ITERATION.NAME)
			.from(ITERATION)
			.innerJoin(CAMPAIGN_ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
			.where(CAMPAIGN.CLN_ID.eq(campaignId))
			.fetchInto(IterationPlanningDto.class);
	}

	@Override
	public int getNbTestPlanItem(Long campaignId, String login) {
		SelectConditionStep<Record1<Integer>> allTestPlanItemsFromCampaign = dsl.selectCount()
			.from(CAMPAIGN)
			.innerJoin(CAMPAIGN_TEST_PLAN_ITEM).on(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
			.leftJoin(CORE_USER).on(CORE_USER.PARTY_ID.eq(CAMPAIGN_TEST_PLAN_ITEM.USER_ID))
			.where(CAMPAIGN.CLN_ID.eq(campaignId));
		if (login != null) {
			allTestPlanItemsFromCampaign = allTestPlanItemsFromCampaign.and(CORE_USER.LOGIN.eq(login));
		}

		return allTestPlanItemsFromCampaign.groupBy(CAMPAIGN.CLN_ID)
			.fetchOne(0, int.class);
	}
}
