/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics.helpers;

import com.google.common.collect.Lists;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * SQUASH-3915
 * In postgresql, there is a technical limitation in "IN" clause, fixed to 32767 elements.
 * I fixed it to 10k because we have 3 "IN" clause in some requests.
 * For now it is only applying for Requirement and Test Case statistics, no need for Campaign and Iteration
 */
public class WorkspaceStatisticsHelper {

    public static final int SQL_IN_CLAUSE_ITEMS_LIMIT = 10000;


    public static Map<Object, Integer> retrieveAllStatisticsResultPartitionsIntoMap(String queryString, boolean isNativeQuery, String parameterName, Collection<Long> entityIds, EntityManager entityManager) {
        List<List<Object[]>> partitionResults = retrieveAllStatisticsResultPartitions(entityManager, entityIds, queryString, isNativeQuery, parameterName);

        return retrieveStatisticsIntoMap(partitionResults, isNativeQuery);
    }

    public static List<List<Object[]>> retrieveAllStatisticsResultPartitions(EntityManager entityManager, Collection<Long> entityIds, String queryString, boolean isNativeQuery, String parameterName) {
        List<List<Long>> partitionsIds = Lists.partition(new ArrayList<>(entityIds), SQL_IN_CLAUSE_ITEMS_LIMIT);
        List<List<Object[]>> partitionResults = new ArrayList<>();
        for (List<Long> partitionIds : partitionsIds) {
            Query query = isNativeQuery ? entityManager.createNativeQuery(queryString) : entityManager.createNamedQuery(queryString);
            query.setParameter(parameterName, partitionIds);
            partitionResults.add(query.getResultList());
        }
        return partitionResults;
    }

    private static Map<Object, Integer> retrieveStatisticsIntoMap(List<List<Object[]>> partitionResults, boolean isValueTypeBigInteger) {
        Map<Object, Integer> statisticsMap = new HashMap<>();
        for (List<Object[]> partitionResult : partitionResults) {
            statisticsMap = doRetrieveStatisticsIntoMap(statisticsMap, partitionResult, isValueTypeBigInteger);
        }
        return statisticsMap;
    }

    private static Map<Object, Integer> doRetrieveStatisticsIntoMap(Map<Object, Integer> statisticsMap, List<Object[]> partitionResult, boolean isValueTypeBigInteger) {
        Map<Object, Integer> partitionResultMap = partitionResult.stream()
                .collect(Collectors.toMap(
                        tuple -> tuple[0],
                        tuple -> isValueTypeBigInteger ?
                                ((BigInteger) tuple[1]).intValue() :
                                ((Long) tuple[1]).intValue()
                        )
                );
        return statisticsMap.isEmpty() ? partitionResultMap : mergeIntoFinalMap(partitionResultMap, statisticsMap);
    }

    private static Map<Object, Integer> mergeIntoFinalMap(Map<Object, Integer> partitionResultMap, Map<Object, Integer> statisticsMap) {
        partitionResultMap.forEach((k, v) -> statisticsMap.merge(k, v, Integer::sum));
        return statisticsMap;
    }

}
