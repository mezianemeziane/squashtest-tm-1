/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import org.jooq.Field;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.service.internal.display.grid.columns.LevelEnumColumn;

import java.util.HashMap;
import java.util.Map;

public class ExecutionStatusColumn extends LevelEnumColumn {
	ExecutionStatusColumn(Field<String> field) {
		super(ExecutionStatus.class, field);
	}

	@Override
	public Map<String, Integer> getAscSortMap() {
		return new HashMap<String, Integer>() {
			{
				put(ExecutionStatus.NOT_FOUND.toString(), 	11);
				put(ExecutionStatus.UNTESTABLE.toString(), 	10);
				put(ExecutionStatus.SETTLED.toString(), 	9);
				put(ExecutionStatus.SUCCESS.toString(), 	8);
				put(ExecutionStatus.WARNING.toString(), 	7);
				put(ExecutionStatus.RUNNING.toString(), 	6);
				put(ExecutionStatus.FAILURE.toString(), 	5);
				put(ExecutionStatus.ERROR.toString(), 		4);
				put(ExecutionStatus.BLOCKED.toString(), 	3);
				put(ExecutionStatus.NOT_RUN.toString(), 	2);
				put(ExecutionStatus.READY.toString(), 		1);
			}
		};
	}
}
