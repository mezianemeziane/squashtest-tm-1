/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.administration;

import org.jooq.*;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

import static org.squashtest.tm.jooq.domain.Tables.CORE_TEAM;
import static org.squashtest.tm.jooq.domain.Tables.CORE_TEAM_MEMBER;

import java.util.Arrays;
import java.util.List;

public class TeamGrid extends AbstractGrid {

	private static final String TEAM_MEMBERS_COUNT = "TEAM_MEMBERS_COUNT";

	private static final String TEAM_ID = "TEAM_ID";

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(CORE_TEAM.PARTY_ID),
			new GridColumn(CORE_TEAM.NAME),
			new GridColumn(CORE_TEAM.DESCRIPTION),
			new GridColumn(DSL.isnull(DSL.field(TEAM_MEMBERS_COUNT, Integer.class),0).as(TEAM_MEMBERS_COUNT)),
			new GridColumn(CORE_TEAM.CREATED_ON),
			new GridColumn(CORE_TEAM.CREATED_BY),
			new GridColumn(CORE_TEAM.LAST_MODIFIED_ON),
			new GridColumn(CORE_TEAM.LAST_MODIFIED_BY)
		);
	}

	@Override
	protected Table<?> getTable() {

		SelectHavingStep<Record2<Long, Integer>> teamMembersCount = getTeamMembersCount();
		return CORE_TEAM
			.leftJoin(teamMembersCount).on(teamMembersCount.field(TEAM_ID, Long.class).eq(CORE_TEAM.PARTY_ID));
	}

	private SelectHavingStep<Record2<Long, Integer>> getTeamMembersCount() {
		return DSL.select(
			CORE_TEAM_MEMBER.TEAM_ID.as(TEAM_ID),
			DSL.count(CORE_TEAM_MEMBER.USER_ID).as(TEAM_MEMBERS_COUNT))
			.from(CORE_TEAM)
			.leftJoin(CORE_TEAM_MEMBER).on(CORE_TEAM_MEMBER.TEAM_ID.eq(CORE_TEAM.PARTY_ID))
			.groupBy(CORE_TEAM.PARTY_ID, CORE_TEAM_MEMBER.TEAM_ID);

	}

	@Override
	protected Field<?> getIdentifier() {
		return CORE_TEAM.PARTY_ID;
	}

	@Override
	protected Field<?> getProjectIdentifier() {
		// We provide a fake project ID as teams don't belong to any project...
		return null; // DSL.field("PROJECT_ID");
	}

	@Override
	protected SortField<?> getDefaultOrder() {
		return DSL.upper(CORE_TEAM.NAME).asc();
	}
}
