/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.administration;

import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Result;
import org.jooq.SelectHavingStep;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

import java.util.Arrays;
import java.util.List;

import static org.squashtest.tm.jooq.domain.tables.AclObjectIdentity.ACL_OBJECT_IDENTITY;
import static org.squashtest.tm.jooq.domain.tables.AclResponsibilityScopeEntry.ACL_RESPONSIBILITY_SCOPE_ENTRY;
import static org.squashtest.tm.jooq.domain.tables.CoreUser.CORE_USER;
import static org.squashtest.tm.jooq.domain.tables.Milestone.MILESTONE;
import static org.squashtest.tm.jooq.domain.tables.MilestoneBinding.MILESTONE_BINDING;
import static org.squashtest.tm.jooq.domain.tables.MilestoneBindingPerimeter.MILESTONE_BINDING_PERIMETER;
import static org.squashtest.tm.jooq.domain.tables.Project.PROJECT;

public class MilestoneGrid extends AbstractGrid {

    // Field 'public' aliases
    private static final String MILESTONE_ID = "MILESTONE_ID";
    private static final String LABEL = "LABEL";
    private static final String STATUS = "STATUS";
    private static final String END_DATE = "END_DATE";
    private static final String PROJECT_COUNT = "PROJECT_COUNT";
    private static final String RANGE = "RANGE";
    private static final String OWNER_FIRST_NAME = "OWNER_FIRST_NAME";
    private static final String OWNER_LAST_NAME = "OWNER_LAST_NAME";
    private static final String OWNER_LOGIN = "OWNER_LOGIN";
    private static final String OWNER_ID = "OWNER_ID";
    private static final String DESCRIPTION = "DESCRIPTION";
    private static final String CREATED_ON = "CREATED_ON";
    private static final String CREATED_BY = "CREATED_BY";
    private static final String USER_SORT_COLUMN = "USER_SORT_COLUMN";

    // Field internal aliases
    private static final String M_RANGE = "M_RANGE";
    private static final String USER_ID = "USER_ID";
    private static final String FILTERED_MILESTONE_ID = "FILTERED_MILESTONE_ID";

    private final Long userId;
    private final boolean isAdmin;

    public MilestoneGrid(Long userId, boolean isAdmin) {
        this.userId = userId;
        this.isAdmin = isAdmin;
    }

    @Override
    protected List<GridColumn> getColumns() {
        return Arrays.asList(
                new GridColumn(DSL.field(MILESTONE_ID)),
                new GridColumn(DSL.field(LABEL)),
                new GridColumn(DSL.field(STATUS)),
                new GridColumn(DSL.field(END_DATE)),
                new GridColumn(DSL.isnull(DSL.field(PROJECT_COUNT), 0).as(PROJECT_COUNT)),
                new GridColumn(DSL.field(M_RANGE).as(RANGE)),
                new GridColumn(DSL.field(OWNER_FIRST_NAME)),
                new GridColumn(DSL.field(OWNER_LAST_NAME)),
                new GridColumn(DSL.field(OWNER_LOGIN)),
                new GridColumn(DSL.field(USER_ID).as(OWNER_ID)),
                new GridColumn(DSL.field(DESCRIPTION)),
                new GridColumn(DSL.field(CREATED_ON)),
                new GridColumn(DSL.field(CREATED_BY)),
                new GridColumn(DSL.field(USER_SORT_COLUMN))
        );
    }

    @Override
    protected Table<?> getTable() {
        Table<?> visibleMilestones = getVisibleMilestonesForUser();
        SelectHavingStep<?> projectCount = getProjectCount();

        return DSL.select(
                visibleMilestones.asterisk(),
                projectCount.field(PROJECT_COUNT).as(PROJECT_COUNT),
                CORE_USER.FIRST_NAME.as(OWNER_FIRST_NAME),
                CORE_USER.LAST_NAME.as(OWNER_LAST_NAME),
                CORE_USER.LOGIN.as(OWNER_LOGIN),
                getUserSortColumn(visibleMilestones))
                .from(visibleMilestones)
                .leftJoin(CORE_USER)
                .on(visibleMilestones.field(USER_ID, Long.class).eq(CORE_USER.PARTY_ID))
                .leftJoin(projectCount)
                .on(visibleMilestones.field(MILESTONE_ID, Long.class).eq(projectCount.field(MILESTONE_ID, Long.class)))
                .asTable();
    }

    private SelectHavingStep<?> getProjectCount() {
        return DSL.select(
                MILESTONE_BINDING.MILESTONE_ID.as(MILESTONE_ID),
                DSL.countDistinct(PROJECT.PROJECT_ID).as(PROJECT_COUNT))
                .from(MILESTONE_BINDING)
                .leftJoin(PROJECT).on(MILESTONE_BINDING.PROJECT_ID.eq(PROJECT.PROJECT_ID))
                .groupBy(MILESTONE_BINDING.MILESTONE_ID);
    }

    public boolean canReadMilestone(Long milestoneId, DSLContext dslContext) {
        Result<?> visibleMilestones = dslContext.selectFrom(getVisibleMilestonesForUser()).fetch();

        return visibleMilestones
                .stream()
                .anyMatch(milestone -> milestoneId.equals(milestone.get(MILESTONE_ID, Long.class)));
    }

    private Table<?> getVisibleMilestonesForUser() {
        if (isAdmin) {
            return DSL.select(MILESTONE.asterisk()).from(MILESTONE).asTable();
        } else {
            SelectHavingStep<Record1<Long>> globalMilestones = getGlobalRangeMilestones();
            SelectHavingStep<Record1<Long>> ownedMilestones = getOwnedMilestones();
            SelectHavingStep<Record1<Long>> milestoneBoundToManagedProject = getMilestoneBoundToManagedProject();

            return DSL.select(MILESTONE.asterisk())
                    .from(MILESTONE)
                    .join(globalMilestones.union(ownedMilestones).union(milestoneBoundToManagedProject))
                    .on(MILESTONE.MILESTONE_ID.eq(DSL.field(FILTERED_MILESTONE_ID, Long.class)))
                    .asTable();
        }
    }

    private Field<?> getUserSortColumn(Table<?> visibleMilestones) {
        final String valueForGlobalRange = "0";
        final Field<String> concatWithoutFirstName = DSL.concat(DSL.val("1"), CORE_USER.LAST_NAME, DSL.val(" ("),
                CORE_USER.LOGIN, DSL.val(")"));
        final Field<String> concatWithFirstName = DSL.concat(DSL.val("1"), CORE_USER.FIRST_NAME, DSL.val(" "),
                CORE_USER.LAST_NAME, DSL.val(" ("), CORE_USER.LOGIN, DSL.val(")"));

        return DSL.when(visibleMilestones.field("M_RANGE", String.class).eq("GLOBAL"), valueForGlobalRange)
                .otherwise(DSL.when(CORE_USER.FIRST_NAME.isNull(), concatWithoutFirstName)
                        .otherwise(concatWithFirstName))
                .as(USER_SORT_COLUMN);
    }

    private SelectHavingStep<Record1<Long>> getGlobalRangeMilestones() {
        return DSL.select(MILESTONE.MILESTONE_ID.as(FILTERED_MILESTONE_ID))
                .from(MILESTONE).where(MILESTONE.M_RANGE.eq("GLOBAL"));
    }

    private SelectHavingStep<Record1<Long>> getOwnedMilestones() {
        return DSL.select(MILESTONE.MILESTONE_ID.as(FILTERED_MILESTONE_ID))
                .from(MILESTONE).where(MILESTONE.USER_ID.eq(this.userId));
    }

    private SelectHavingStep<Record1<Long>> getMilestoneBoundToManagedProject() {
        return DSL.select(MILESTONE_BINDING_PERIMETER.MILESTONE_ID.as(FILTERED_MILESTONE_ID))
                .from(MILESTONE_BINDING_PERIMETER)
                .join(ACL_OBJECT_IDENTITY)
                .on(ACL_OBJECT_IDENTITY.IDENTITY.eq(MILESTONE_BINDING_PERIMETER.PROJECT_ID)
                        .and(ACL_OBJECT_IDENTITY.CLASS_ID.eq(1L)))
                .join(ACL_RESPONSIBILITY_SCOPE_ENTRY)
                .on(ACL_RESPONSIBILITY_SCOPE_ENTRY.OBJECT_IDENTITY_ID.eq(ACL_OBJECT_IDENTITY.ID))
                .where(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.eq(this.userId)
                        .and(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID.eq(5L)));
    }

    @Override
    protected Field<?> getIdentifier() {
        return DSL.field(MILESTONE_ID);
    }

    @Override
    protected Field<?> getProjectIdentifier() {
        return null;
    }

    @Override
    protected SortField<?> getDefaultOrder() {
        return DSL.field(END_DATE).desc();
    }
}
