/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.administration;

import org.jooq.*;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

import java.util.Arrays;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.CORE_GROUP;
import static org.squashtest.tm.jooq.domain.Tables.CORE_GROUP_MEMBER;
import static org.squashtest.tm.jooq.domain.Tables.CORE_TEAM_MEMBER;
import static org.squashtest.tm.jooq.domain.Tables.ACL_RESPONSIBILITY_SCOPE_ENTRY;
import static org.squashtest.tm.jooq.domain.Tables.ACL_OBJECT_IDENTITY;


public class UserGrid extends AbstractGrid {

	private static final String HABILITATION_COUNT = "HABILITATION_COUNT";

	private static final String TEAM_COUNT = "TEAM_COUNT";

	private static final String TEAM_MEMBER_ID = "TEAM_MEMBER_ID";

	private static final String USER_ID = "USER_ID";

	public UserGrid() {
	}

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(CORE_USER.PARTY_ID),
			new GridColumn(CORE_USER.ACTIVE),
			new GridColumn(CORE_USER.LOGIN),
			new GridColumn(CORE_GROUP.QUALIFIED_NAME.as("USER_GROUP")),
			new GridColumn(CORE_USER.FIRST_NAME),
			new GridColumn(CORE_USER.LAST_NAME),
			new GridColumn(CORE_USER.EMAIL),
			new GridColumn(CORE_USER.CREATED_ON),
			new GridColumn(CORE_USER.CREATED_BY),
			new GridColumn(DSL.isnull(DSL.field(HABILITATION_COUNT, Integer.class),0).as(HABILITATION_COUNT)),
			new GridColumn(DSL.isnull(DSL.field(TEAM_COUNT, Integer.class),0).as(TEAM_COUNT)),
			new GridColumn(CORE_USER.LAST_CONNECTED_ON)
		);
	}

	@Override
	protected Table<?> getTable() {
		SelectHavingStep<Record2<Long, Integer>> teamCount = getTeamCount();

		SelectHavingStep<Record2<Long, Integer>> habilitationCount = getHabilitationCount();

		return CORE_USER
			.leftJoin(CORE_GROUP_MEMBER).on(CORE_GROUP_MEMBER.PARTY_ID.eq(CORE_USER.PARTY_ID))
			.leftJoin(CORE_GROUP).on(CORE_GROUP_MEMBER.GROUP_ID.eq(CORE_GROUP.ID))
			.leftJoin(teamCount).on(teamCount.field(TEAM_MEMBER_ID, Long.class).eq(CORE_USER.PARTY_ID))
			.leftJoin(habilitationCount).on(habilitationCount.field(USER_ID,Long.class).eq(CORE_USER.PARTY_ID));
	}

	private SelectHavingStep<Record2<Long, Integer>> getTeamCount() {
		return DSL.select(
			CORE_USER.PARTY_ID.as(TEAM_MEMBER_ID),
				DSL.count(CORE_TEAM_MEMBER.TEAM_ID).as(TEAM_COUNT))
			.from(CORE_USER)
			.leftJoin(CORE_TEAM_MEMBER).on(CORE_TEAM_MEMBER.USER_ID.eq(CORE_USER.PARTY_ID))
			.groupBy(CORE_USER.PARTY_ID);
	}

	private SelectHavingStep<Record2<Long, Integer>> getHabilitationCount() {
		return DSL.select(
			CORE_USER.PARTY_ID.as(USER_ID),
				DSL.count(ACL_RESPONSIBILITY_SCOPE_ENTRY.ID).as(HABILITATION_COUNT))
			.from(CORE_USER)
			.leftJoin(ACL_RESPONSIBILITY_SCOPE_ENTRY).on(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.eq(CORE_USER.PARTY_ID))
			.leftJoin(ACL_OBJECT_IDENTITY).on(ACL_OBJECT_IDENTITY.ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.OBJECT_IDENTITY_ID))
			.where(ACL_OBJECT_IDENTITY.CLASS_ID.eq(1L))
			.groupBy(CORE_USER.PARTY_ID);
	}

	@Override
	protected Field<?> getIdentifier() {
		return CORE_USER.PARTY_ID;
	}

	@Override
	protected Field<?> getProjectIdentifier() {
		return null;
	}

	@Override
	protected SortField<?> getDefaultOrder() {
		return DSL.upper(CORE_USER.LOGIN).asc();
	}
}
