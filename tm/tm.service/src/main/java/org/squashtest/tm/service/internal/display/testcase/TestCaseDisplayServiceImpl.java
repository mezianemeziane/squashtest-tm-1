/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.testcase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.plugin.PluginType;
import org.squashtest.tm.api.workspace.WorkspaceType;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.project.AutomationWorkflowType;
import org.squashtest.tm.domain.project.LibraryPluginBinding;
import org.squashtest.tm.domain.users.PartyPreference;
import org.squashtest.tm.domain.users.preferences.CorePartyPreference;
import org.squashtest.tm.service.actionword.ActionWordService;
import org.squashtest.tm.service.customreport.CustomReportDashboardService;
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService;
import org.squashtest.tm.service.internal.display.dto.AutomationRequestDto;
import org.squashtest.tm.service.internal.display.dto.RequirementVersionCoverageDto;
import org.squashtest.tm.service.internal.display.dto.requirement.DetailedStepViewRequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.testcase.DataSetDto;
import org.squashtest.tm.service.internal.display.dto.testcase.DatasetParamValueDto;
import org.squashtest.tm.service.internal.display.dto.testcase.ParameterDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseFolderDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseLibraryDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseMultiSelectionDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TestStepDto;
import org.squashtest.tm.service.internal.display.testcase.parameter.TestCaseParameterOperationReport;
import org.squashtest.tm.service.internal.repository.ExecutionDao;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.AutomatedTestDisplayDao;
import org.squashtest.tm.service.internal.repository.display.AutomationRequestDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.DatasetDisplayDao;
import org.squashtest.tm.service.internal.repository.display.DatasetParamValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.IssueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ParameterDisplayDao;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionCoverageDisplayDao;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TestCaseDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TestStepDisplayDao;
import org.squashtest.tm.service.internal.testcase.TestCaseCallTreeFinder;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;
import org.squashtest.tm.service.user.PartyPreferenceService;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;
import static org.squashtest.tm.service.security.Authorizations.READ_REQVERSION_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_TCF_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_TC_OR_ROLE_ADMIN;


@Service
@Transactional(readOnly = true)
public class TestCaseDisplayServiceImpl implements TestCaseDisplayService {

	@Inject
	private TestCaseDisplayDao testCaseDisplayDao;

	@Inject
	private ProjectDao projectDao;

	@Inject
	private MilestoneDisplayDao milestoneDisplayDao;

	@Inject
	private TestStepDisplayDao testStepDisplayDao;

	@Inject
	private CustomFieldValueDisplayDao customFieldValueDisplayDao;

	@Inject
	private AutomationRequestDisplayDao automationRequestDisplayDao;

	@Inject
	private ParameterDisplayDao parameterDisplayDao;

	@Inject
	private DatasetDisplayDao datasetDisplayDao;

	@Inject
	private DatasetParamValueDisplayDao datasetParamValueDisplayDao;

	@Inject
	private RequirementVersionCoverageDisplayDao requirementVersionCoverageDisplayDao;

	@Inject
	private RequirementVersionDisplayDao requirementVersionDisplayDao;

	@Inject
	private TestCaseCallTreeFinder testCaseCallTreeFinder;

	@Inject
	private AttachmentDisplayDao attachmentDisplayDao;

	@Inject
	private AutomatedTestDisplayDao automatedTestDisplayDao;

	@Inject
	private IssueDisplayDao issueDao;

	@Inject
	private TestCaseLibraryNavigationService testCaseLibraryNavigationService;

	@Inject
	private ExecutionDao executionDao;

	@Autowired(required = false)
	private ActionWordService actionWordService;

	@Inject
	private CustomReportDashboardService customReportDashboardService;

	@Inject
	private PartyPreferenceService partyPreferenceService;

	@Inject
	private GenericProjectManagerService projectManager;

	private static final String FINAL_STATE = "finalState";

	@Override
	public TestCaseLibraryDto getTestCaseLibraryView(long libraryId) {
		TestCaseLibraryDto libraryDto = this.testCaseDisplayDao.getTestCaseLibraryDtoById(libraryId);
		libraryDto.setAttachmentList(attachmentDisplayDao.findAttachmentListById(libraryDto.getAttachmentListId()));
		appendFavoriteDashboardInformation(libraryDto);
		return libraryDto;
	}

	private void appendFavoriteDashboardInformation(TestCaseLibraryDto libraryDto) {
		libraryDto.setCanShowFavoriteDashboard(customReportDashboardService.canShowDashboardInWorkspace(Workspace.TEST_CASE));
		libraryDto.setShouldShowFavoriteDashboard(customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.TEST_CASE));
		PartyPreference preference = partyPreferenceService
			.findPreferenceForCurrentUser(CorePartyPreference.FAVORITE_DASHBOARD_TEST_CASE.getPreferenceKey());
		if (preference != null) {
			Long dashboardId = Long.valueOf(preference.getPreferenceValue());
			libraryDto.setFavoriteDashboardId(dashboardId);
		}
	}

	@Override
	public TestCaseMultiSelectionDto getTestCaseMultiView() {
		TestCaseMultiSelectionDto testCaseMultiSelectionDto = new TestCaseMultiSelectionDto();
		testCaseMultiSelectionDto.setCanShowFavoriteDashboard(customReportDashboardService.canShowDashboardInWorkspace(Workspace.TEST_CASE));
		testCaseMultiSelectionDto.setShouldShowFavoriteDashboard(customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.TEST_CASE));
		PartyPreference preference = partyPreferenceService
			.findPreferenceForCurrentUser(CorePartyPreference.FAVORITE_DASHBOARD_TEST_CASE.getPreferenceKey());
		if (preference != null) {
			Long dashboardId = Long.valueOf(preference.getPreferenceValue());
			testCaseMultiSelectionDto.setFavoriteDashboardId(dashboardId);
		}
		return testCaseMultiSelectionDto;
	}

	@Override
	@PreAuthorize(READ_TC_OR_ROLE_ADMIN)
	public TestCaseDto getTestCaseView(long testCaseId) {
		TestCaseDto testCase = testCaseDisplayDao.getTestCaseDtoById(testCaseId);
		testCase.setMilestones(milestoneDisplayDao.getMilestonesByTestCaseId(testCaseId));
		testCase.setTestSteps(testStepDisplayDao.getTestStepsByTestCase(testCaseId));
		testCase.setCustomFieldValues(customFieldValueDisplayDao.findCustomFieldValues(BindableEntity.TEST_CASE, testCaseId));

		AutomationRequestDto automationRequestDto = automationRequestDisplayDao.findByTestCaseId(testCaseId);
		testCase.setAutomationRequest(automationRequestDto);
		setParameterDatasetEntities(testCase);
		testCase.setAutomatedTest(automatedTestDisplayDao.findByTestCaseId(testCaseId));
		testCase.setCoverages(doFindCoverages(testCaseId));
		testCase.setAttachmentList(attachmentDisplayDao.findAttachmentListById(testCase.getAttachmentListId()));
		testCase.setNbIssues(issueDao.countIssuesByTestCaseId(testCaseId));
		testCase.setCalledTestCases(testCaseDisplayDao.getCallingTestCaseById(testCaseId));
		testCase.setLastExecutionStatus(testCaseDisplayDao.getLastExecutionStatus(testCaseId));
		testCase.setNbExecutions(executionDao.countByTestCaseId(testCaseId));
		testCase.setActionWordLibraryActive(nonNull(actionWordService));
		testCase.setConfiguredRemoteFinalStatus(getRemoteFinalStatus(testCase.getProjectId()));
		return testCase;
	}

	private String getRemoteFinalStatus(Long projectId) {
		String automationWorkflowTypeKey = projectDao.findAutomationWorkflowTypeByProjectId(projectId);

		if (automationWorkflowTypeKey.equals(AutomationWorkflowType.REMOTE_WORKFLOW.getI18nKey())) {
			LibraryPluginBinding lpb = projectDao.findPluginForProject(projectId, PluginType.AUTOMATION);
			Map<String,String> pluginConfiguration = projectManager.getPluginConfigurationWithoutCheck(projectId, WorkspaceType.TEST_CASE_WORKSPACE, lpb.getPluginId());
			return pluginConfiguration.get(FINAL_STATE);
		}
		return null;
	}

	@Override
	@PreAuthorize(READ_TCF_OR_ROLE_ADMIN)
	public TestCaseFolderDto getTestCaseFolderView(long testCaseFolderId) {
		TestCaseFolderDto testCaseFolder = this.testCaseDisplayDao.getTestCaseFolderDtoById(testCaseFolderId);
		testCaseFolder.setAttachmentList(attachmentDisplayDao.findAttachmentListById(testCaseFolder.getAttachmentListId()));
		testCaseFolder.setCustomFieldValues(customFieldValueDisplayDao.findCustomFieldValues(BindableEntity.TESTCASE_FOLDER, testCaseFolderId));
		appendFavoriteDashboardInformation(testCaseFolder);
		return testCaseFolder;
	}

	private void appendFavoriteDashboardInformation(TestCaseFolderDto requirementFolderDto) {
		requirementFolderDto.setCanShowFavoriteDashboard(customReportDashboardService.canShowDashboardInWorkspace(Workspace.TEST_CASE));
		requirementFolderDto.setShouldShowFavoriteDashboard(customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.TEST_CASE));
		PartyPreference preference = partyPreferenceService
			.findPreferenceForCurrentUser(CorePartyPreference.FAVORITE_DASHBOARD_TEST_CASE.getPreferenceKey());
		if (preference != null) {
			Long dashboardId = Long.valueOf(preference.getPreferenceValue());
			requirementFolderDto.setFavoriteDashboardId(dashboardId);
		}
	}

	private void setParameterDatasetEntities(TestCaseDto testCase) {
		List<ParameterDto> parameters = parameterDisplayDao.findAllByTestCaseId(testCase.getId());
		testCase.setParameters(parameters);
		List<DataSetDto> datasets = datasetDisplayDao.findAllByTestCaseId(testCase.getId());
		testCase.setDatasets(datasets);
		testCase.setDatasetParamValues(findDataSetParamValues(parameters, datasets));
	}

	@Override
	@PreAuthorize(READ_TC_OR_ROLE_ADMIN)
	public List<RequirementVersionCoverageDto> findCoverages(Long testCaseId) {
		return doFindCoverages(testCaseId);
	}

	private List<RequirementVersionCoverageDto> doFindCoverages(Long testCaseId) {
		Set<Long> calledIds = testCaseCallTreeFinder.getTestCaseCallTree(testCaseId);
		return requirementVersionCoverageDisplayDao.findByTestCaseIds(testCaseId, calledIds);
	}

	@Override
	@PreAuthorize(READ_TC_OR_ROLE_ADMIN)
	public TestCaseParameterOperationReport findParametersData(Long testCaseId) {

		TestCaseParameterOperationReport testCaseParametersData = new TestCaseParameterOperationReport();

		List<DataSetDto> dataSets = datasetDisplayDao.findAllByTestCaseId(testCaseId);
		testCaseParametersData.setDataSets(dataSets);
		List<ParameterDto> parameters = parameterDisplayDao.findAllByTestCaseId(testCaseId);
		testCaseParametersData.setParameters(parameters);

		testCaseParametersData.setParamValues(findDataSetParamValues(parameters, dataSets));

		return testCaseParametersData;
	}

	@Override
	public TestCaseParameterOperationReport findParametersDataByTestStepId(Long testStepId) {
		Long testCaseId = testCaseDisplayDao.getTestCaseIdByTestStepId(testStepId);
		return findParametersData(testCaseId);
	}

	@Override
	@PreAuthorize(READ_TC_OR_ROLE_ADMIN)
	public List<TestStepDto> findTestStepsByTestCaseId(Long testCaseId) {
		return testStepDisplayDao.getTestStepsByTestCase(testCaseId);
	}

	@Override
	@PreAuthorize(READ_TC_OR_ROLE_ADMIN)
	public TestCaseDto getDetailedTestStepView(long testCaseId) {
		TestCaseDto testCase = testCaseDisplayDao.getTestCaseDtoById(testCaseId);
		testCase.setMilestones(milestoneDisplayDao.getMilestonesByTestCaseId(testCaseId));
		testCase.setTestSteps(testStepDisplayDao.getTestStepsByTestCase(testCaseId));
		testCase.setAttachmentList(attachmentDisplayDao.findAttachmentListById(testCase.getAttachmentListId()));
		testCase.setCoverages(doFindCoverages(testCaseId));
		setParameterDatasetEntities(testCase);
		return testCase;
	}

	@Override
	@PreAuthorize(READ_REQVERSION_OR_ROLE_ADMIN)
	public DetailedStepViewRequirementVersionDto getDetailedTestStepViewRequirement(long requirementVersionId) {
		return this.requirementVersionDisplayDao.findById(requirementVersionId);
	}

	private List<DatasetParamValueDto> findDataSetParamValues(List<ParameterDto> parameters, List<DataSetDto> dataSets) {
		List<Long> parameterIds = parameters.stream().map(ParameterDto::getId).collect(Collectors.toList());
		List<Long> datasetIds = dataSets.stream().map(DataSetDto::getId).collect(Collectors.toList());
		return datasetParamValueDisplayDao.findAllByParameterAndDataset(parameterIds, datasetIds);
	}
}
