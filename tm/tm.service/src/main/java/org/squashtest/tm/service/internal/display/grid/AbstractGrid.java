/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.OrderField;
import org.jooq.Record;
import org.jooq.Select;
import org.jooq.SelectLimitAfterOffsetStep;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.filters.GridFilterConditionBuilder;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static java.util.Collections.emptySet;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import static org.jooq.impl.DSL.count;

public abstract class AbstractGrid {

	protected final Map<String, GridColumn> aliasToFieldDictionary;

	protected final Map<String, String> fieldToAliasDictionary;

	protected abstract List<GridColumn> getColumns();

	protected abstract Table<?> getTable();

	// return always true by default
	protected Condition craftInvariantFilter() {
		return DSL.val(1).eq(1);
	}

	protected abstract Field<?> getIdentifier();

	protected abstract Field<?> getProjectIdentifier();

	protected SortField<?> getDefaultOrder() {
		return this.getIdentifier().asc();
	}

	public AbstractGrid() {
		this.fieldToAliasDictionary = this.getColumns().stream()
			.collect(Collectors.toMap(
				column -> column.getAliasedField().getName(),
				GridColumn::findRuntimeAlias
			));

		this.aliasToFieldDictionary = this.getColumns().stream()
			.collect(Collectors.toMap(
				GridColumn::findRuntimeAlias,
				column -> column
			));
	}

	private List<Field<?>> getFields() {
		return this.getColumns().stream()
			.map(GridColumn::getAliasedField)
			.collect(Collectors.toList());
	}

	public GridResponse getRows(GridRequest request, DSLContext dslContext) {
		Integer count = countRows(dslContext, request);

		Select<?> baseRequest = getBaseRequest(request, dslContext);

		List<DataRow> dataRows = baseRequest
			.fetch()
			.stream()
			.collect(getCollector());

		GridResponse gridResponse = new GridResponse();
		gridResponse.setCount(count);
		gridResponse.setDataRows(dataRows);
		return gridResponse;
	}

	private Select<Record> getBaseRequest(GridRequest request, DSLContext dslContext) {
		int offset = 0;

		if (request.getPage() != null && request.getSize() != null) {
			offset = request.getPage() * request.getSize();
		}

		SelectLimitAfterOffsetStep<Record> query = dslContext
			.select(getFields())
			.from(getTable())
			.where(craftInvariantFilter()).and(this.craftVariableFilters(request))
			.orderBy(getOrderClause(request))
			.offset(offset);

		if (request.getSize() != null) {
			return query.limit(request.getSize());
		} else {
			return query;
		}
	}

	protected List<OrderField<?>> getOrderClause(GridRequest request) {
		if (request.getSort().isEmpty()) {
			return Collections.singletonList(getDefaultOrder());
		} else {
			return generateOrderClause(request);
		}
	}

	private List<OrderField<?>> generateOrderClause(GridRequest request) {
		return request.getSort().stream()
			.map(gridSort -> {
				GridColumn gridColumn = this.aliasToFieldDictionary.get(gridSort.getProperty());
				return gridColumn.generateOrderClause(gridSort.getDirection());
			}).collect(Collectors.toList());
	}

	private Integer countRows(DSLContext dslContext, GridRequest request) {
		return dslContext
			.select(count())
			.from(getTable())
			.where(craftInvariantFilter()).and(this.craftVariableFilters(request))
			.fetchOne(0, Integer.class);
	}

	protected Collector<Record, ?, List<DataRow>> getCollector() {
		return new AbstractGrid.DefaultCollector(this.getIdentifier(), this.getProjectIdentifier(), this.fieldToAliasDictionary);
	}

	private Condition craftVariableFilters(GridRequest request) {
		if (request.getFilterValues().isEmpty()) {
			return DSL.val(1).eq(1);
		} else {
			return request.getFilterValues().stream().collect(new DefaultFilterCollector(this.aliasToFieldDictionary, request.isSearchOnMultiColumns()));
		}
	}

	static class DefaultCollector implements Collector<Record, List<DataRow>, List<DataRow>> {

		private final Field<?> idField;

		private final Field<?> projectIdField;

		private final Map<String, String> columnNameMap;

		public DefaultCollector(Field<?> idField, Field<?> projectIdField, Map<String, String> columnNameMap) {
			this.idField = idField;
			this.projectIdField = projectIdField;
			this.columnNameMap = columnNameMap;
		}

		@Override
		public Supplier<List<DataRow>> supplier() {
			return ArrayList::new;
		}

		@Override
		public BiConsumer<List<DataRow>, Record> accumulator() {
			return (list, record) -> {
				DataRow dataRow = new DataRow();
				Map<String, Object> rawData = record.intoMap();
				Map<String, Object> data = new HashMap<>();

				// Using 'Collectors.toMap' won't work for entries with null values
				for (Map.Entry<String, Object> entry : rawData.entrySet()) {
					data.put(columnNameMap.get(entry.getKey()), entry.getValue());
				}

				dataRow.setId(record.get(this.idField, String.class));

				if (this.projectIdField != null) {
					dataRow.setProjectId((record.get(this.projectIdField, Long.class)));
				}

				dataRow.setData(data);
				list.add(dataRow);
			};
		}

		@Override
		public BinaryOperator<List<DataRow>> combiner() {
			return (x, y) -> {
				throw new UnsupportedOperationException();
			};
		}

		@Override
		public Function<List<DataRow>, List<DataRow>> finisher() {
			return Function.identity();
		}

		@Override
		public Set<Characteristics> characteristics() {
			Set<Characteristics> characteristics = new HashSet<>();
			characteristics.add(Characteristics.IDENTITY_FINISH);
			return characteristics;
		}
	}

	/**
	 * Basic implementation of a collector that reduce a list of {@link GridFilterValue} to a single chain of {@link Condition}.
	 * Aka it reduce a list of filters to a {@link Condition} like condition1.and(condition2).and(condition3)...
	 */
	public static class DefaultFilterCollector implements Collector<GridFilterValue, List<GridFilterValue>, Condition> {

		protected final Map<String, GridColumn> aliasToFieldDictionary;
		protected final boolean searchingOnMultiColumns;

		public DefaultFilterCollector(Map<String, GridColumn> aliasToFieldDictionary, boolean searchingOnMultiColumns) {
			this.aliasToFieldDictionary = aliasToFieldDictionary;
			this.searchingOnMultiColumns = searchingOnMultiColumns;
		}

		@Override
		public Supplier<List<GridFilterValue>> supplier() {
			return ArrayList::new;
		}

		@Override
		public BiConsumer<List<GridFilterValue>, GridFilterValue> accumulator() {
			return List::add;
		}

		@Override
		public BinaryOperator<List<GridFilterValue>> combiner() {
			return (x, y) -> {
				throw new UnsupportedOperationException();
			};
		}

		@Override
		public Function<List<GridFilterValue>, Condition> finisher() {
			return gridFilterValues -> {
				Queue<GridFilterValue> filterStack = new ArrayDeque<>(gridFilterValues);
				Condition condition = convertFilterToCondition(requireNonNull(filterStack.poll()));
				while (nonNull(filterStack.peek())) {
					if (searchingOnMultiColumns) {
						condition = condition.or(convertFilterToCondition(filterStack.poll()));
					} else {
						condition = condition.and(convertFilterToCondition(filterStack.poll()));
					}
				}
				return condition;
			};
		}

		private Condition convertFilterToCondition(GridFilterValue gridFilterValue) {
			String id = gridFilterValue.getId();
			GridColumn gridColumn = requireNonNull(aliasToFieldDictionary.get(id));
			Field<?> field = gridColumn.getNativeField();
			return GridFilterConditionBuilder.getConditionBuilder(field, gridFilterValue).build();
		}

		@Override
		public Set<Characteristics> characteristics() {
			return emptySet();
		}
	}

}
