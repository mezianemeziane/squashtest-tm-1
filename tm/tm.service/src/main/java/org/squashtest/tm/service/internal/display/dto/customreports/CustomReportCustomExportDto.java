/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.customreports;

import org.squashtest.tm.domain.customreport.CustomReportCustomExportColumn;

import java.util.Date;
import java.util.List;

public class CustomReportCustomExportDto {
    private Long id;
    private Long customReportLibraryNodeId;
    private Long projectId;
    private String name;
    private Date createdOn;
    private String createdBy;
    private Date lastModifiedOn;
    private String lastModifiedBy;
    private String scopeNodeId;
    private String scopeNodeName;
    private Long scopeProjectId;
    private List<CustomReportCustomExportColumnDto> columns;

    public CustomReportCustomExportDto() {
    }

    public static class CustomReportCustomExportColumnDto {
        String entityType;
        String columnName;
        String label;
        Long cufId;

        public static CustomReportCustomExportColumnDto from(CustomReportCustomExportColumn column) {
            String columnName = column.getLabel().name();
            String entityType = column.getLabel().getEntityType().toString();
            return new CustomReportCustomExportColumnDto(entityType, columnName, column.getCufId());
        }

        private CustomReportCustomExportColumnDto(String entityType, String columnName, Long cufId) {
            this.entityType = entityType;
            this.columnName = columnName;
            this.cufId = cufId;
        }

        public String getEntityType() {
            return entityType;
        }

        public String getColumnName() {
            return columnName;
        }

        public String getLabel() {
            return label;
        }

        public Long getCufId() {
            return cufId;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomReportLibraryNodeId() {
        return customReportLibraryNodeId;
    }

    public void setCustomReportLibraryNodeId(Long customReportLibraryNodeId) {
        this.customReportLibraryNodeId = customReportLibraryNodeId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScopeNodeId() {
        return scopeNodeId;
    }

    public void setScopeNodeId(String scopeNodeId) {
        this.scopeNodeId = scopeNodeId;
    }

    public String getScopeNodeName() {
        return scopeNodeName;
    }

    public void setScopeNodeName(String scopeNodeName) {
        this.scopeNodeName = scopeNodeName;
    }

    public List<CustomReportCustomExportColumnDto> getColumns() {
        return columns;
    }

    public void setColumns(List<CustomReportCustomExportColumnDto> columns) {
        this.columns = columns;
    }

    public Long getScopeProjectId() {
        return scopeProjectId;
    }

    public void setScopeProjectId(Long scopeProjectId) {
        this.scopeProjectId = scopeProjectId;
    }

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
}
