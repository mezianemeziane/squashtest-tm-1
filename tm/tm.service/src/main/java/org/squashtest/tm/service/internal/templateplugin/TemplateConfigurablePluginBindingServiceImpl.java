/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.templateplugin;

import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.template.TemplateConfigurablePlugin;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.domain.project.TemplateConfigurablePluginBinding;
import org.squashtest.tm.exception.templateplugin.TemplateConfigurablePluginBindingAlreadyExistsException;
import org.squashtest.tm.service.internal.display.dto.TemplateConfigurablePluginBindingDto;
import org.squashtest.tm.service.internal.repository.GenericProjectDao;
import org.squashtest.tm.service.internal.repository.TemplateConfigurablePluginBindingDao;
import org.squashtest.tm.service.templateplugin.TemplateConfigurablePluginBindingService;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Service("squashtest.tm.service.TemplateConfigurablePluginBindingManagerService")
@Transactional
public class TemplateConfigurablePluginBindingServiceImpl implements TemplateConfigurablePluginBindingService {

    private final TemplateConfigurablePluginBindingDao templateConfigurablePluginBindingDao;
    private final GenericProjectDao genericProjectDao;

    TemplateConfigurablePluginBindingServiceImpl(TemplateConfigurablePluginBindingDao templateConfigurablePluginBindingDao,
                                                 GenericProjectDao genericProjectDao) {
        this.templateConfigurablePluginBindingDao = templateConfigurablePluginBindingDao;
        this.genericProjectDao = genericProjectDao;
    }

    public TemplateConfigurablePluginBinding createTemplateConfigurablePluginBinding(@NonNull ProjectTemplate template,
                                                                                     @NonNull Project project,
                                                                                     @NonNull TemplateConfigurablePlugin plugin) {
        if (hasBinding(template.getId(), project.getId(), plugin.getId())) {
            throw new TemplateConfigurablePluginBindingAlreadyExistsException();
        }

        return templateConfigurablePluginBindingDao.createTemplateConfigurablePluginBinding(template, project, plugin);
    }

    @Override
    public void removeAllForGenericProject(long genericProjectId) {
        if (genericProjectDao.isProjectTemplate(genericProjectId)) {
            templateConfigurablePluginBindingDao.removeAllForTemplate(genericProjectId);
        } else {
            templateConfigurablePluginBindingDao.removeAllForProject(genericProjectId);
        }
    }

    @Override
    public boolean hasBinding(long templateId, long projectId, @NonNull @NotBlank String pluginId) {
        return templateConfigurablePluginBindingDao.findOne(templateId, projectId, pluginId) != null;
    }

    @Override
    public List<TemplateConfigurablePluginBindingDto> findAllByTemplateId(long templateId) {
        return templateConfigurablePluginBindingDao.findAllByTemplateId(templateId);
    }

    @Override
    public List<TemplateConfigurablePluginBindingDto> findAllByTemplateIdAndPluginId(long templateId, String pluginId) {
        return templateConfigurablePluginBindingDao.findAllByTemplateIdAndPluginId(templateId, pluginId);
    }

    @Override
    public boolean isProjectConfigurationBoundToTemplate(long projectId) {
        return templateConfigurablePluginBindingDao.isProjectConfigurationBoundToTemplate(projectId);
    }

    @Override
	public boolean isProjectConfigurationBoundToTemplate(long projectId, String pluginId) {
		return templateConfigurablePluginBindingDao.isProjectConfigurationBoundToTemplate(projectId, pluginId);
	}
}
