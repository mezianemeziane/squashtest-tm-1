/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.infolist;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.infolist.SystemInfoListCode;
import org.squashtest.tm.service.display.infolist.InfoListDisplayService;
import org.squashtest.tm.service.internal.display.dto.InfoListAdminViewDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.administration.InfoListGrid;
import org.squashtest.tm.service.internal.repository.display.InfoListDisplayDao;

import javax.inject.Inject;
import javax.transaction.Transactional;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

@Service
@Transactional
public class InfoListDisplayServiceImpl implements InfoListDisplayService {

	private final DSLContext dsl;
	private InfoListDisplayDao infoListDisplayDao;

	@Inject
	public InfoListDisplayServiceImpl(DSLContext dsl,
									  InfoListDisplayDao infoListDisplayDao) {
		this.dsl = dsl;
		this.infoListDisplayDao = infoListDisplayDao;
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public GridResponse findAll(GridRequest request) {
		GridResponse response = new InfoListGrid().getRows(request, dsl);
		response.sanitizeField("description");
		return response;
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public InfoListAdminViewDto getInfoListView(long infoListId) {
		InfoListAdminViewDto infoList = infoListDisplayDao.getInfoListById(infoListId);
		checkInfoListIsSystem(infoList.getCode());
		return infoList;
	}

	private void checkInfoListIsSystem(String infoListCode) {
		if(SystemInfoListCode.isSystem(infoListCode)) {
			throw new IllegalAccessError("This is a system info list! Access denied");
		}
	}
}
