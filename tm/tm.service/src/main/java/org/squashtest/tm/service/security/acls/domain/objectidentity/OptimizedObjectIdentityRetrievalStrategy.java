/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.security.acls.domain.objectidentity;

import org.springframework.security.acls.model.ObjectIdentityGenerator;

/**
 * {@link ObjectIdentityGenerator} based on optimized sql request.
 * Using direct requests is an order of magnitude more faster that fetching a whole tree of hibernate entities, especially in case of
 * TM2.0 views optimized fetching code, as we try to avoid fetching the complete entities !
 * It's a shame to lose the benefits of that huge performance improvement just for permissions...
 * Some rapid benchmark show an improvement by a factor 5 to 10 in postgresql when fetching requirement view for the WHOLE process.
 * It means that entity fetching for permission check was responsible for 80% to 90% of the TOTAL cost of fetching a requirement view...
 * Even for write process that require entity fetching, doing it directly will probably avoid selection of libraries, project and other related stuff juste for perm checking.
 * However this class only handle fetching permissions by Id and Type. No fetching {@link org.springframework.security.acls.model.ObjectIdentity} by entity as if we may already have paid the fetching cost...
 *
 * Everything come at a cost. Here it's development cost because each optimized class need a dedicated request and maintenance cost, even if jooq protect us from schema evolution.
 */
public interface OptimizedObjectIdentityRetrievalStrategy extends ObjectIdentityGenerator {
	boolean isHandled(String className);
}
