/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display;

import com.google.common.collect.Multimap;

import java.util.List;
import java.util.Map;

public interface AclDisplayDao {
	/**
	 * Fetch all merged permissions for a given list of projects and a given list of partyIds.
	 * <p>
	 * Beware that you must exploit that permission map in conjunction with user profile as ADMIN.
	 *
	 * @param projectIds Ids of looked projects
	 * @param partyIds   ids of parties
	 * @return A {@link Map} with permission byProject.
	 * Key is projectId, Value is a {@link Multimap} of permission by AclClass
	 * Example :
	 * <ul>
	 * <li>
	 * <p>
	 * 1
	 * <ul>
	 * <li>Project: ["READ","MANAGE"]</li>
	 * <li>RequirementLibrary : ["READ", "WRITE"...]</li>
	 * <li>TestCaseLibrary : ["READ", "WRITE"...]</li>
	 * </ul>
	 * </li>
	 * <li>
	 * <p>
	 * 2
	 * <ul>
	 * <li>Project: ["READ"]</li>
	 * <li>RequirementLibrary : ["READ"]</li>
	 * <li>TestCaseLibrary : ["READ"]</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * All projectIds will have their key, even if there is no rights on this projects.
	 */
	Map<Long, Multimap<String, String>> findPermissions(List<Long> projectIds, List<Long> partyIds);
}
