/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search;

import org.jooq.DSLContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.query.QueryColumnPrototypeReference;
import org.squashtest.tm.service.display.search.RequirementSearchService;
import org.squashtest.tm.service.display.search.ResearchResult;
import org.squashtest.tm.service.display.search.TestCaseSearchService;
import org.squashtest.tm.service.display.search.TestCaseThroughRequirementSearchService;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.repository.TestCaseDao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static org.squashtest.tm.service.display.search.ResearchResult.emptyResult;

@Service
@Transactional(readOnly = true)
public class TestCaseThroughRequirementSearchServiceImpl implements TestCaseThroughRequirementSearchService {

	private final TestCaseSearchService testCaseSearchService;

	private final RequirementSearchService requirementSearchService;

	private final DSLContext dslContext;

	private final TestCaseDao testCaseDao;

	public TestCaseThroughRequirementSearchServiceImpl(TestCaseSearchService testCaseSearchService,
													   RequirementSearchService requirementSearchService,
													   DSLContext dslContext,
													   TestCaseDao testCaseDao) {
		this.testCaseSearchService = testCaseSearchService;
		this.requirementSearchService = requirementSearchService;
		this.dslContext = dslContext;
		this.testCaseDao = testCaseDao;
	}

	@Override
	public ResearchResult search(GridRequest request) {
		// 1 - Search requirements with filters and scope WITHOUT pagination or sorts -> List<Long> reqVersionIds
		ResearchResult requirementVersionSearchResult = requirementSearchService.search(request.toNonPaginatedRequest());
		// 2 - Search covering test case and their calling testcase WITHOUT pagination or sorts ->  List<Long> testCaseIds
		Set<Long> testCaseIds = getTestCaseIdsByRequirementCoverages(requirementVersionSearchResult.getIds());
		// 3 - Search test case with : Filter on Ids and request pagination and sorts -> List<Long> testCaseIds + count
		if(testCaseIds.isEmpty()){
			return emptyResult();
		}
		GridRequest testCaseRequest = craftTestCaseRequest(request, testCaseIds);
		return testCaseSearchService.search(testCaseRequest);
	}

	private GridRequest craftTestCaseRequest(GridRequest request, Set<Long> testCaseIds) {
		GridRequest testCaseRequest = request.clone();
		testCaseRequest.setScope(new ArrayList<>());
		GridFilterValue gridFilterValue = new GridFilterValue();
		gridFilterValue.setId("id");
		gridFilterValue.setColumnPrototype(QueryColumnPrototypeReference.TEST_CASE_ID);
		gridFilterValue.setOperation("IN");
		gridFilterValue.setValues(testCaseIds.stream().map(Object::toString).collect(Collectors.toList()));
		testCaseRequest.setFilterValues(singletonList(gridFilterValue));
		return testCaseRequest;
	}

	private Set<Long> getTestCaseIdsByRequirementCoverages(List<Long> requirementVersionIds) {
		Set<Long> testCaseIds = testCaseDao.findAllTestCaseIdsCoveringRequirementVersions(requirementVersionIds);
		testCaseIds.addAll(testCaseDao.findAllTestCasesIdsCallingTestCases(new ArrayList<>(testCaseIds)));
		return testCaseIds;
	}
}
