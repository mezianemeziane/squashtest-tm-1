/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.service.internal.display.dto.requirement.DetailedStepViewRequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionDto;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.field;
import static org.squashtest.tm.jooq.domain.Tables.*;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.STATUS;

@Repository
public class RequirementVersionDisplayDaoImpl implements RequirementVersionDisplayDao {

	private static final String hasExtenderAlias = "HAS_EXTENDER";

	private final DSLContext dsl;
	private final ActiveMilestoneHolder activeMilestoneHolder;

	public RequirementVersionDisplayDaoImpl(DSLContext dsl, ActiveMilestoneHolder activeMilestoneHolder) {
		this.dsl = dsl;
		this.activeMilestoneHolder = activeMilestoneHolder;
	}

	@Override
	public Map<Long, Long> findCurrentRequirementVersions(List<Long> requirementIds) {
		Optional<Milestone> activeMilestoneId = activeMilestoneHolder.getActiveMilestone();
		if (activeMilestoneId.isPresent()) {
			return findRequirementVersionIdsInMilestoneMode(requirementIds, activeMilestoneId.get().getId());
		} else {
			return findRequirementVersionIdsWithoutMilestone(requirementIds);
		}
	}

	@Override
	public Long findCurrentRequirementVersions(Long requirementId) {
		Map<Long, Long> currentRequirementVersions = this.findCurrentRequirementVersions(Collections.singletonList(requirementId));
		return currentRequirementVersions.get(requirementId);
	}

	@Override
	public RequirementVersionDto findRequirementVersion(Long currentVersionId) {
		// @formatter:off
		return dsl.select(
			REQUIREMENT_VERSION.RES_ID.as(RequestAliasesConstants.ID), REQUIREMENT_VERSION.REFERENCE, REQUIREMENT_VERSION.REQUIREMENT_ID,
			REQUIREMENT_VERSION.VERSION_NUMBER, REQUIREMENT_VERSION.CATEGORY, REQUIREMENT_VERSION.CRITICALITY, REQUIREMENT_VERSION.REQUIREMENT_STATUS.as(STATUS),
			RESOURCE.NAME, RESOURCE.ATTACHMENT_LIST_ID, RESOURCE.CREATED_BY, RESOURCE.CREATED_ON, RESOURCE.LAST_MODIFIED_BY, RESOURCE.LAST_MODIFIED_ON,
			RESOURCE.DESCRIPTION,
			REQUIREMENT_LIBRARY_NODE.PROJECT_ID,
			REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID, REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_URL,
			REMOTE_SYNCHRONISATION.SYNC_STATUS, REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_PERIMETER_STATUS,
			field(REQUIREMENT_SYNC_EXTENDER.REQ_SYNC_ID.isNotNull()).as(hasExtenderAlias))
			.from(REQUIREMENT_VERSION)
			.innerJoin(RESOURCE).using(REQUIREMENT_VERSION.RES_ID)
			.innerJoin(REQUIREMENT_LIBRARY_NODE).on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID))
			.leftJoin(REQUIREMENT_SYNC_EXTENDER).on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID))
			.leftJoin(REMOTE_SYNCHRONISATION).on(REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID.eq(REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_ID))
			.where(REQUIREMENT_VERSION.RES_ID.eq(currentVersionId))
			.fetchOneInto(RequirementVersionDto.class);
		// @formatter:on
	}

	@Override
	public DetailedStepViewRequirementVersionDto findById(Long requirementVersionId) {
		return dsl.select(
			REQUIREMENT_VERSION.CATEGORY, REQUIREMENT_VERSION.CRITICALITY, REQUIREMENT_VERSION.REQUIREMENT_STATUS.as(STATUS), REQUIREMENT_VERSION.VERSION_NUMBER
			, RESOURCE.DESCRIPTION)
			.from(REQUIREMENT_VERSION).innerJoin(RESOURCE).using(REQUIREMENT_VERSION.RES_ID)
			.where(REQUIREMENT_VERSION.RES_ID.eq(requirementVersionId))
			.fetchOneInto(DetailedStepViewRequirementVersionDto.class);
	}


	private Map<Long, Long> findRequirementVersionIdsWithoutMilestone(List<Long> requirementIds) {
		return findCurrentVersionByRequirement(requirementIds);
	}

	private Map<Long, Long> findCurrentVersionByRequirement(List<Long> requirementIds) {
		return dsl.select(REQUIREMENT.RLN_ID, REQUIREMENT.CURRENT_VERSION_ID)
			.from(REQUIREMENT)
			.where(REQUIREMENT.RLN_ID.in(requirementIds))
			.fetchMap(REQUIREMENT.RLN_ID, REQUIREMENT.CURRENT_VERSION_ID);
	}

	private Map<Long, Long> findRequirementVersionIdsInMilestoneMode(List<Long> requirementIds, Long activeMilestoneId) {
		Map<Long, Long> requirementToCurrentVersionIds = findCurrentVersionByRequirement(requirementIds);

		Map<Long, Long> milestoneRequirementVersionIds =
			dsl.select(REQUIREMENT_VERSION.REQUIREMENT_ID, REQUIREMENT_VERSION.RES_ID)
				.from(REQUIREMENT_VERSION)
				.innerJoin(MILESTONE_REQ_VERSION).on(REQUIREMENT_VERSION.RES_ID.eq(MILESTONE_REQ_VERSION.REQ_VERSION_ID))
				.where(REQUIREMENT_VERSION.REQUIREMENT_ID.in(requirementIds)).and(MILESTONE_REQ_VERSION.MILESTONE_ID.eq(activeMilestoneId))
				.fetchMap(REQUIREMENT_VERSION.REQUIREMENT_ID, REQUIREMENT_VERSION.RES_ID);

		return requirementIds
			.stream()
			.collect(Collectors.toMap(Function.identity(), requirementId -> {
				return Optional.ofNullable(
					milestoneRequirementVersionIds.getOrDefault(
						requirementId, requirementToCurrentVersionIds.getOrDefault(requirementId, null))
				).orElseThrow(() -> new IllegalArgumentException("No version found for requirement " + requirementId));
			}));
	}

}
