/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.Result;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.testcase.CalledTestCaseDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseFolderDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseLibraryDto;
import org.squashtest.tm.service.internal.repository.display.TestCaseDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.*;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.*;

@Repository
public class TestCaseDisplayDaoImpl implements TestCaseDisplayDao {


	private DSLContext dsl;

	public TestCaseDisplayDaoImpl(DSLContext dsl) {
		this.dsl = dsl;
	}

	@Override
	public TestCaseDto getTestCaseDtoById(Long testCaseId) {
		return dsl.select(
			TEST_CASE_LIBRARY_NODE.TCLN_ID.as(RequestAliasesConstants.ID),
			TEST_CASE_LIBRARY_NODE.NAME,
			TEST_CASE_LIBRARY_NODE.DESCRIPTION,
			TEST_CASE_LIBRARY_NODE.PROJECT_ID,
			TEST_CASE_LIBRARY_NODE.ATTACHMENT_LIST_ID,
			TEST_CASE_LIBRARY_NODE.CREATED_BY,
			TEST_CASE_LIBRARY_NODE.CREATED_ON,
			TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY,
			TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_ON,
			TEST_CASE.REFERENCE, TEST_CASE.IMPORTANCE,
			TEST_CASE.TC_STATUS.as(RequestAliasesConstants.STATUS),
			TEST_CASE.AUTOMATABLE, TEST_CASE.IMPORTANCE_AUTO,
			TEST_CASE.PREREQUISITE, TEST_CASE.TC_NATURE.as(RequestAliasesConstants.NATURE),
			TEST_CASE.TC_TYPE.as(RequestAliasesConstants.TYPE),
			TEST_CASE.UUID,
			TEST_CASE.EXECUTION_MODE,
			SCRIPTED_TEST_CASE.TCLN_ID.as(SCRIPTED_TEST_CASE_ID), SCRIPTED_TEST_CASE.SCRIPT,
			KEYWORD_TEST_CASE.TCLN_ID.as(KEYWORD_TEST_CASE_ID),
			TEST_CASE.AUTOMATED_TEST_REFERENCE,
			TEST_CASE.AUTOMATED_TEST_TECHNOLOGY,
			TEST_CASE.SCM_REPOSITORY_ID
		)
			.from(TEST_CASE_LIBRARY_NODE)
			.innerJoin(TEST_CASE).on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
			.leftJoin(SCRIPTED_TEST_CASE).on(TEST_CASE.TCLN_ID.eq(SCRIPTED_TEST_CASE.TCLN_ID))
			.leftJoin(KEYWORD_TEST_CASE).on(TEST_CASE.TCLN_ID.eq(KEYWORD_TEST_CASE.TCLN_ID))
			.where(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(testCaseId))
			.fetchOne()
			.into(TestCaseDto.class);
	}

	@Override
	public TestCaseLibraryDto getTestCaseLibraryDtoById(Long id) {
		return dsl
			.select(TEST_CASE_LIBRARY.TCL_ID.as(RequestAliasesConstants.ID), TEST_CASE_LIBRARY.ATTACHMENT_LIST_ID,
				PROJECT.PROJECT_ID, PROJECT.DESCRIPTION, PROJECT.NAME)
			.from(TEST_CASE_LIBRARY)
			.innerJoin(PROJECT).on(PROJECT.TCL_ID.eq(TEST_CASE_LIBRARY.TCL_ID))
			.where(TEST_CASE_LIBRARY.TCL_ID.eq(id))
			.fetchOne().into(TestCaseLibraryDto.class);
	}

	@Override
	public List<Long> getCalledTestCaseIdsByTestCaseId(Long id) {
		return dsl.select(CALL_TEST_STEP.CALLED_TEST_CASE_ID)
			.from(CALL_TEST_STEP)
			.innerJoin(TEST_STEP).on(CALL_TEST_STEP.TEST_STEP_ID.eq(TEST_STEP.TEST_STEP_ID))
			.innerJoin(TEST_CASE_STEPS).on(TEST_STEP.TEST_STEP_ID.eq(TEST_CASE_STEPS.STEP_ID))
			.innerJoin(TEST_CASE).on(TEST_CASE_STEPS.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
			.fetch().into(Long.class);
	}

	@Override
	public List<CalledTestCaseDto> getCallingTestCaseById(Long id) {
		return dsl.select(PROJECT.NAME.as(RequestAliasesConstants.PROJECT_NAME), TEST_CASE.REFERENCE, TEST_CASE.TCLN_ID.as(RequestAliasesConstants.ID),
			TEST_CASE_LIBRARY_NODE.NAME, DATASET.NAME.as(RequestAliasesConstants.DATASETS_NAME), TEST_CASE_STEPS.STEP_ORDER)
			.from(TEST_CASE_LIBRARY_NODE)
			.innerJoin(TEST_CASE).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
			.innerJoin(PROJECT).on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.innerJoin(TEST_CASE_STEPS).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE_STEPS.TEST_CASE_ID))
			.innerJoin(CALL_TEST_STEP).on(TEST_CASE_STEPS.STEP_ID.eq(CALL_TEST_STEP.TEST_STEP_ID))
			.leftJoin(DATASET).on(CALL_TEST_STEP.CALLED_DATASET.eq(DATASET.DATASET_ID))
			.where(CALL_TEST_STEP.CALLED_TEST_CASE_ID.eq(id))
			.fetchInto(CalledTestCaseDto.class);
	}

	@Override
	public TestCaseFolderDto getTestCaseFolderDtoById(long testCaseFolderId) {
		return dsl.select(
			TEST_CASE_LIBRARY_NODE.TCLN_ID.as(RequestAliasesConstants.ID),
			TEST_CASE_LIBRARY_NODE.ATTACHMENT_LIST_ID, TEST_CASE_LIBRARY_NODE.DESCRIPTION, TEST_CASE_LIBRARY_NODE.NAME, TEST_CASE_LIBRARY_NODE.PROJECT_ID)
			.from(TEST_CASE_LIBRARY_NODE)
			.where(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(testCaseFolderId))
			.fetchOne().into(TestCaseFolderDto.class);
	}

	@Override
	public Long getTestCaseIdByTestStepId(Long testStepId) {
		return dsl.select(TEST_CASE_LIBRARY_NODE.TCLN_ID)
			.from(TEST_CASE_LIBRARY_NODE)
			.innerJoin(TEST_CASE_STEPS).on(TEST_CASE_STEPS.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
			.where(TEST_CASE_STEPS.STEP_ID.eq(testStepId)).fetchOne(TEST_CASE_LIBRARY_NODE.TCLN_ID);
	}

	@Override
	public String getLastExecutionStatus(Long testCaseId) {

		Result<Record1<String>> resultStatus = dsl.select(EXECUTION.EXECUTION_STATUS)
			.from(EXECUTION)
			.where(EXECUTION.TCLN_ID.eq(testCaseId))
			.and(EXECUTION.LAST_EXECUTED_ON.eq(DSL.select(DSL.max(EXECUTION.LAST_EXECUTED_ON)).from(EXECUTION).where(EXECUTION.TCLN_ID.eq(testCaseId)))).fetch();

		if (resultStatus.isEmpty()) {
			return "";
		} else {
			return resultStatus.get(0).value1();
		}
	}
}
