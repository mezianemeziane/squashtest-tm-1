/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.List;

public class TestAutomationServerAdminViewDto extends TestAutomationServerDto {
    private CredentialsDto credentials;
    private List<String> supportedAuthenticationProtocols;

    /**
     * Creates a TestAutomationServerAdminViewDto from a TestAutomationServerDto.
     * Note: 'credentials' and 'supportedAuthenticationProtocols' fields won't be set.
     * @param taServer dto to build a new admin view dto from
     * @return a fresh new TestAutomationServerAdminViewDto with no credentials
     */
    public static TestAutomationServerAdminViewDto from(TestAutomationServerDto taServer) {
        TestAutomationServerAdminViewDto dto = new TestAutomationServerAdminViewDto();
        dto.setId(taServer.getId());
        dto.setBaseUrl(taServer.getBaseUrl());
        dto.setKind(taServer.getKind());
        dto.setName(taServer.getName());
        dto.setDescription(taServer.getDescription());
        dto.setCreatedBy(taServer.getCreatedBy());
        dto.setCreatedOn(taServer.getCreatedOn());
        dto.setLastModifiedBy(taServer.getLastModifiedBy());
        dto.setLastModifiedOn(taServer.getLastModifiedOn());
        dto.setManualSlaveSelection(taServer.getManualSlaveSelection());
        dto.setAuthProtocol(taServer.getAuthProtocol());
        return dto;
    }

    public CredentialsDto getCredentials() {
        return credentials;
    }

    public void setCredentials(CredentialsDto credentials) {
        this.credentials = credentials;
    }

    public List<String> getSupportedAuthenticationProtocols() {
        return supportedAuthenticationProtocols;
    }

    public void setSupportedAuthenticationProtocols(List<String> supportedAuthenticationProtocols) {
        this.supportedAuthenticationProtocols = supportedAuthenticationProtocols;
    }
}
