/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.campaign;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.users.PartyPreference;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.domain.users.preferences.CorePartyPreference;
import org.squashtest.tm.service.campaign.CampaignTestPlanManagerService;
import org.squashtest.tm.service.customreport.CustomReportDashboardService;
import org.squashtest.tm.service.display.campaign.CampaignDisplayService;
import org.squashtest.tm.service.internal.display.dto.LibraryDto;
import org.squashtest.tm.service.internal.display.dto.UserView;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignDto;
import org.squashtest.tm.service.internal.display.dto.campaign.CampaignFolderDto;
import org.squashtest.tm.service.internal.display.dto.campaign.IterationPlanningDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.campaign.CampaignTestPlanGrid;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CampaignDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.IssueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.hibernate.HibernateCampaignDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.UserContextService;
import org.squashtest.tm.service.user.PartyPreferenceService;

import java.util.List;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

@Service
@Transactional(readOnly = true)
public class CampaignDisplayServiceImpl implements CampaignDisplayService {

	private static final String READ_UNASSIGNED = "READ_UNASSIGNED";

	private static final String CAN_READ_CAMPAIGN = "hasPermission(#campaignId, 'org.squashtest.tm.domain.campaign.Campaign', 'READ')" + OR_HAS_ROLE_ADMIN;

	private final CampaignDisplayDao campaignDisplayDao;

	private final AttachmentDisplayDao attachmentDisplayDao;

	private final CustomFieldValueDisplayDao customFieldValueDisplayDao;

	private final MilestoneDisplayDao milestoneDisplayDao;

	private final HibernateCampaignDao hibernateCampaignDao;

	private final IssueDisplayDao issueDisplayDao;

	private final DSLContext dslContext;

	private final CampaignTestPlanManagerService campaignTestPlanManagerService;

	private final CustomReportDashboardService customReportDashboardService;

	private final PartyPreferenceService partyPreferenceService;

	private final ReadUnassignedTestPlanHelper readUnassignedTestPlanHelper;

	private final AvailableDatasetAppender availableDatasetAppender;

	private final UserContextService userContextService;

	private final PermissionEvaluationService permissionEvaluationService;

	public CampaignDisplayServiceImpl(CampaignDisplayDao campaignDisplayDao,
									  AttachmentDisplayDao attachmentDisplayDao,
									  CustomFieldValueDisplayDao customFieldValueDisplayDao,
									  MilestoneDisplayDao milestoneDisplayDao,
									  HibernateCampaignDao hibernateCampaignDao,
									  DSLContext dslContext,
									  IssueDisplayDao issueDisplayDao,
									  CampaignTestPlanManagerService campaignTestPlanManagerService,
									  CustomReportDashboardService customReportDashboardService,
									  PartyPreferenceService partyPreferenceService,
									  ReadUnassignedTestPlanHelper readUnassignedTestPlanHelper,
									  AvailableDatasetAppender availableDatasetAppender,
									  UserContextService userContextService,
									  PermissionEvaluationService permissionEvaluationService) {
		this.campaignDisplayDao = campaignDisplayDao;
		this.attachmentDisplayDao = attachmentDisplayDao;
		this.customFieldValueDisplayDao = customFieldValueDisplayDao;
		this.milestoneDisplayDao = milestoneDisplayDao;
		this.hibernateCampaignDao = hibernateCampaignDao;
		this.issueDisplayDao = issueDisplayDao;
		this.dslContext = dslContext;
		this.campaignTestPlanManagerService = campaignTestPlanManagerService;
		this.customReportDashboardService = customReportDashboardService;
		this.partyPreferenceService = partyPreferenceService;
		this.readUnassignedTestPlanHelper = readUnassignedTestPlanHelper;
		this.availableDatasetAppender = availableDatasetAppender;
		this.userContextService = userContextService;
		this.permissionEvaluationService = permissionEvaluationService;
	}

	@Override
	public LibraryDto getCampaignLibraryView(long libraryId) {
		LibraryDto libraryDto = this.campaignDisplayDao.getCampaignLibraryDtoById(libraryId);
		libraryDto.setAttachmentList(attachmentDisplayDao.findAttachmentListById(libraryDto.getAttachmentListId()));
		return libraryDto;
	}

	@Override
	public CampaignFolderDto getCampaignFolderView(long campaignFolderId) {
		CampaignFolderDto campaignFolderDto = this.campaignDisplayDao.getCampaignFolderDtoById(campaignFolderId);
		campaignFolderDto.setAttachmentList(attachmentDisplayDao.findAttachmentListById(campaignFolderDto.getAttachmentListId()));
		campaignFolderDto.setCustomFieldValues(customFieldValueDisplayDao.findCustomFieldValues(BindableEntity.CAMPAIGN_FOLDER, campaignFolderId));
		campaignFolderDto.setNbIssues(issueDisplayDao.countIssuesByCampaignFolderId(campaignFolderId));
		campaignFolderDto.setCanShowFavoriteDashboard(customReportDashboardService.canShowDashboardInWorkspace(Workspace.CAMPAIGN));
		campaignFolderDto.setShouldShowFavoriteDashboard(customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.CAMPAIGN));
		PartyPreference preference = partyPreferenceService
			.findPreferenceForCurrentUser(CorePartyPreference.FAVORITE_DASHBOARD_CAMPAIGN.getPreferenceKey());
		if (preference != null) {
			Long dashboardId = Long.valueOf(preference.getPreferenceValue());
			campaignFolderDto.setFavoriteDashboardId(dashboardId);
		}
		return campaignFolderDto;
	}

	@Override
	public CampaignDto getCampaignView(long campaignId) {
		CampaignDto campaignDto = this.campaignDisplayDao.getCampaignDtoById(campaignId);
		campaignDto.setMilestones(milestoneDisplayDao.getMilestonesByCampaignId(campaignId));
		campaignDto.setAttachmentList(attachmentDisplayDao.findAttachmentListById(campaignDto.getAttachmentListId()));
		campaignDto.setTestPlanStatistics(hibernateCampaignDao.findCampaignStatistics(campaignId));
		campaignDto.setCustomFieldValues(customFieldValueDisplayDao.findCustomFieldValues(BindableEntity.CAMPAIGN, campaignId));
		campaignDto.setNbIssues(issueDisplayDao.countIssuesByCampaignId(campaignId));
		appendUsers(campaignDto);
		campaignDto.setShouldShowFavoriteDashboard(customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.CAMPAIGN));
		campaignDto.setCanShowFavoriteDashboard(customReportDashboardService.canShowDashboardInWorkspace(Workspace.CAMPAIGN));
		PartyPreference preference = partyPreferenceService
			.findPreferenceForCurrentUser(CorePartyPreference.FAVORITE_DASHBOARD_CAMPAIGN.getPreferenceKey());
		if (preference != null) {
			Long dashboardId = Long.valueOf(preference.getPreferenceValue());
			campaignDto.setFavoriteDashboardId(dashboardId);
		}
		appendNbTestPlanItems(campaignDto);
		return campaignDto;
	}

	private void appendUsers(CampaignDto campaignDto) {
		List<User> users = this.campaignTestPlanManagerService.findAssignableUserForTestPlan(campaignDto.getId());
		campaignDto.setUsers(UserView.fromEntities(users));
	}

	private void appendNbTestPlanItems(CampaignDto campaignDto) {
		String login = this.userContextService.getUsername();
		int count;

		if(currentUserCanReadUnassigned(campaignDto.getId())){
			count = this.campaignDisplayDao.getNbTestPlanItem(campaignDto.getId(), null);
		} else {
			count = this.campaignDisplayDao.getNbTestPlanItem(campaignDto.getId(), login);
		}
		campaignDto.setNbTestPlanItems(count);
	}

	private boolean currentUserCanReadUnassigned(Long campaignId) {
		return this.permissionEvaluationService.hasRoleOrPermissionOnObject(Roles.ROLE_ADMIN, READ_UNASSIGNED, campaignId, Campaign.SIMPLE_CASS_NAME);
	}

	@Override
	public List<IterationPlanningDto> findIterationsPlanningByCampaign(Long campaignId) {
		return campaignDisplayDao.findIterationPlanningByCampaign(campaignId);
	}

	@Override
	@PreAuthorize(CAN_READ_CAMPAIGN)
	public GridResponse findTestPlan(Long campaignId, GridRequest gridRequest) {
		CampaignTestPlanGrid testPlanGrid = new CampaignTestPlanGrid(campaignId);
		this.readUnassignedTestPlanHelper.appendReadUnassignedFilter(gridRequest, campaignId, Campaign.SIMPLE_CASS_NAME);
		GridResponse gridResponse = testPlanGrid.getRows(gridRequest, this.dslContext);
		availableDatasetAppender.appendAvailableDatasets(gridResponse);
		return gridResponse;
	}
}
