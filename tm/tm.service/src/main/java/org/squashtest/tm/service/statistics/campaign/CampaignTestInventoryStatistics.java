/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics.campaign;

import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.service.statistics.CountOnEnum;

import java.util.*;
import java.util.stream.Collectors;

public class CampaignTestInventoryStatistics {

	private String campaignName;
	private CountOnEnum<ExecutionStatus> statistics;

	public CampaignTestInventoryStatistics(){
		Set<ExecutionStatus> executionStatuses = getHandledStatuses();
		statistics = new CountOnEnum<>(executionStatuses);
	}

	private Set<ExecutionStatus> getHandledStatuses() {
		return EnumSet.allOf(ExecutionStatus.class).stream()
			.map(ExecutionStatus::getCanonicalStatus)
			.collect(Collectors.toSet());
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public LinkedHashMap<ExecutionStatus, Integer> getStatistics() {
		return statistics.getStatistics();
	}

	public void setNumber(int intValue, ExecutionStatus status) {
		this.statistics.add(status.getCanonicalStatus(), intValue);
	}
}
