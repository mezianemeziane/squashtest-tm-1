/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;


import org.jooq.DSLContext;
import org.jooq.Record4;
import org.jooq.SelectHavingStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.requirement.VerifyingTestCaseDto;
import org.squashtest.tm.service.internal.repository.display.VerifyingTestCaseDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import java.sql.Timestamp;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;

@Repository
public class VerifyingTestCaseDisplayDaoImpl implements VerifyingTestCaseDisplayDao {
	private static final String MILESTONE_LABELS = "MILESTONE_LABELS";
	private static final String MILESTONE_MAX_DATE = "MILESTONE_MAX_DATE";
	private static final String MILESTONE_MIN_DATE = "MILESTONE_MIN_DATE";
	private static final String ID = "ID";


	private final DSLContext dsl;

	public VerifyingTestCaseDisplayDaoImpl(DSLContext dsl) {
		this.dsl = dsl;
	}

	@Override
	public List<VerifyingTestCaseDto> findByRequirementVersionId(Long requirementVersionId) {
		SelectHavingStep<?> milestoneDates = getMilestoneDates();
		return dsl.select(TEST_CASE.TCLN_ID.as(ID), TEST_CASE.REFERENCE, TEST_CASE.TC_STATUS.as(RequestAliasesConstants.STATUS), TEST_CASE.IMPORTANCE,
			TEST_CASE_LIBRARY_NODE.NAME, PROJECT.NAME.as(RequestAliasesConstants.PROJECT_NAME),
			DSL.field(MILESTONE_LABELS), DSL.field(MILESTONE_MIN_DATE), DSL.field(MILESTONE_MAX_DATE))
			.from(TEST_CASE)
			.innerJoin(TEST_CASE_LIBRARY_NODE).on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
			.innerJoin(PROJECT).on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.innerJoin(REQUIREMENT_VERSION_COVERAGE).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID))
			.leftJoin(milestoneDates).on(milestoneDates.field(ID, Long.class).eq(TEST_CASE.TCLN_ID))
			.where(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(requirementVersionId))
			.fetchInto(VerifyingTestCaseDto.class);
	}

	private SelectHavingStep<Record4<Long, Timestamp, Timestamp, String>> getMilestoneDates() {
		return DSL.select(
			TEST_CASE.TCLN_ID.as(ID),
			DSL.min(MILESTONE.END_DATE).as(MILESTONE_MIN_DATE),
			DSL.max(MILESTONE.END_DATE).as(MILESTONE_MAX_DATE),
			DSL.listAgg(MILESTONE.LABEL, ", ").withinGroupOrderBy(MILESTONE.END_DATE.asc()).as(MILESTONE_LABELS)
		)
			.from(TEST_CASE)
			.innerJoin(MILESTONE_TEST_CASE)
			.on(TEST_CASE.TCLN_ID.eq(MILESTONE_TEST_CASE.TEST_CASE_ID))
			.innerJoin(MILESTONE).on(MILESTONE.MILESTONE_ID.eq(MILESTONE_TEST_CASE.MILESTONE_ID))
			.groupBy(
				TEST_CASE.TCLN_ID,
				MILESTONE_TEST_CASE.TEST_CASE_ID
			);
	}
}
