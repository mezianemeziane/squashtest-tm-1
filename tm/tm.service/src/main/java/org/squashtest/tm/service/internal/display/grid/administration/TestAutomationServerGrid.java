/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.administration;

import org.jooq.*;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

import java.util.Arrays;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.*;

public class TestAutomationServerGrid extends AbstractGrid {

	private static final String PROJECT_COUNT = "PROJECT_COUNT";
	private static final String EXECUTION_COUNT = "EXECUTION_COUNT";
	private static final String SERVER_ID = "SERVER_ID";

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(TEST_AUTOMATION_SERVER.SERVER_ID),
			new GridColumn(THIRD_PARTY_SERVER.NAME),
			new GridColumn(THIRD_PARTY_SERVER.URL.as("BASE_URL")),
			new GridColumn(TEST_AUTOMATION_SERVER.KIND),
			new GridColumn(TEST_AUTOMATION_SERVER.CREATED_BY),
			new GridColumn(TEST_AUTOMATION_SERVER.CREATED_ON),
			new GridColumn(TEST_AUTOMATION_SERVER.LAST_MODIFIED_BY),
			new GridColumn(TEST_AUTOMATION_SERVER.LAST_MODIFIED_ON),
			new GridColumn(DSL.isnull(DSL.field(EXECUTION_COUNT), 0).as(EXECUTION_COUNT))
		);
	}

	@Override
	protected Table<?> getTable() {
		SelectHavingStep<Record2<Long, Integer>> executionCount = getExecutionCount();

		return TEST_AUTOMATION_SERVER
			.innerJoin(THIRD_PARTY_SERVER).on(THIRD_PARTY_SERVER.SERVER_ID.eq(TEST_AUTOMATION_SERVER.SERVER_ID))
			.leftJoin(executionCount).on(executionCount.field(SERVER_ID, Long.class).eq(TEST_AUTOMATION_SERVER.SERVER_ID));
	}

	private SelectHavingStep<Record2<Long, Integer>> getExecutionCount(){
		return DSL.select(
			TEST_AUTOMATION_SERVER.SERVER_ID.as(SERVER_ID),
			DSL.countDistinct(AUTOMATED_EXECUTION_EXTENDER.EXTENDER_ID).as(EXECUTION_COUNT))
			.from(TEST_AUTOMATION_SERVER)
			.leftJoin(TEST_AUTOMATION_PROJECT).on(TEST_AUTOMATION_PROJECT.SERVER_ID.eq(TEST_AUTOMATION_SERVER.SERVER_ID))
			.leftJoin(AUTOMATED_TEST).on(TEST_AUTOMATION_PROJECT.TA_PROJECT_ID.eq(AUTOMATED_TEST.PROJECT_ID))
			.leftJoin(AUTOMATED_EXECUTION_EXTENDER).on(AUTOMATED_TEST.TEST_ID.eq(AUTOMATED_EXECUTION_EXTENDER.TEST_ID))
			.groupBy(TEST_AUTOMATION_SERVER.SERVER_ID);
	}

	@Override
	protected Field<?> getIdentifier() {
		return TEST_AUTOMATION_SERVER.SERVER_ID;
	}

	@Override
	protected Field<?> getProjectIdentifier() {
		return null;
	}

	@Override
	protected SortField<?> getDefaultOrder() { return DSL.upper(THIRD_PARTY_SERVER.NAME).asc();	}
}
