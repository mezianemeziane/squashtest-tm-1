/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display;

import com.google.common.collect.ListMultimap;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeReferences;
import org.squashtest.tm.domain.NodeWorkspace;

import java.util.Collection;
import java.util.Set;

/**
 * DAO dedicated to browsing different kind of hierarchical trees in SquashTM
 */
public interface TreeBrowserDao {

	/**
	 * Return the {@link NodeReference} for libraries for a given workspace.
	 * @param workspace  The {@link NodeWorkspace}
	 * @param projectIds List of {@link org.squashtest.tm.domain.project.Project} ids.
	 * @return All the libraries {@link NodeReference}
	 */
	Set<NodeReference> findLibraryReferences(NodeWorkspace workspace, Collection<Long> projectIds);

	/**
	 * Find the children {@link NodeReference} for a given set of parent identified by their {@link NodeReference}.
	 * @param parentReferences The set of parents
	 * @return A multi map PARENT-REF - all of this parent children.
	 * ex :
	 * PARENT-REF-1 : [CHILDREN-REF-1, CHILDREN-REF-2],
	 * PARENT-REF-2 : [CHILDREN-REF-3, CHILDREN-REF-4, CHILDREN-REF-5]...
	 */
	ListMultimap<NodeReference, NodeReference> findChildrenReference(Set<NodeReference> parentReferences);

	/**
	 * Find all ancestors for a given set of node reference. All references must came from same tree.
	 * @param nodeReferences a set of {@link NodeReference}
	 * @return
	 */
	Set<NodeReference> findAncestors(NodeReferences nodeReferences);
}
