/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.campaign;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.users.PartyPreference;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.domain.users.preferences.CorePartyPreference;
import org.squashtest.tm.service.campaign.IterationTestPlanFinder;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.customreport.CustomReportDashboardService;
import org.squashtest.tm.service.display.campaign.IterationDisplayService;
import org.squashtest.tm.service.internal.display.dto.UserView;
import org.squashtest.tm.service.internal.display.dto.campaign.IterationDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.campaign.IterationTestPlanGrid;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.AutomatedSuiteDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.IssueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.IterationDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TestSuiteDisplayDao;
import org.squashtest.tm.service.internal.repository.hibernate.HibernateIterationDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.UserContextService;
import org.squashtest.tm.service.user.PartyPreferenceService;

import java.util.List;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

@Service
@Transactional(readOnly = true)
public class IterationDisplayServiceImpl implements IterationDisplayService {

	private static final String READ_UNASSIGNED = "READ_UNASSIGNED";

	private static final String CAN_READ_ITERATION = "hasPermission(#iterationId, 'org.squashtest.tm.domain.campaign.Iteration', 'READ')" + OR_HAS_ROLE_ADMIN;

	private final IterationDisplayDao iterationDisplayDao;

	private final DSLContext dslContext;

	private final AttachmentDisplayDao attachmentDisplayDao;

	private final CustomFieldValueDisplayDao customFieldValueDisplayDao;

	private final IterationTestPlanFinder iterationTestPlanFinder;

	private final MilestoneDisplayDao milestoneDisplayDao;

	private final HibernateIterationDao hibernateIterationDao;

	private final IssueDisplayDao issueDisplayDao;

	private final IterationTestPlanManagerService iterationTestPlanManagerService;

	private final TestSuiteDisplayDao testSuiteDisplayDao;
	private final TestPlanItemSuccessRateCalculator rateCalculator;

	private final AutomatedSuiteDisplayDao automatedSuiteDisplayDao;

	private final CustomReportDashboardService customReportDashboardService;
	private final ReadUnassignedTestPlanHelper readUnassignedTestPlanHelper;

	private final AvailableDatasetAppender availableDatasetAppender;

	private final PartyPreferenceService partyPreferenceService;

	private final UserContextService userContextService;

	private final PermissionEvaluationService permissionEvaluationService;

	public IterationDisplayServiceImpl(IterationDisplayDao iterationDisplayDao,
									   DSLContext dslContext,
									   AttachmentDisplayDao attachmentDisplayDao,
									   CustomFieldValueDisplayDao customFieldValueDisplayDao,
									   IterationTestPlanFinder iterationTestPlanFinder,
									   MilestoneDisplayDao milestoneDisplayDao,
									   HibernateIterationDao hibernateIterationDao,
									   IssueDisplayDao issueDisplayDao,
									   IterationTestPlanManagerService iterationTestPlanManagerService,
									   TestSuiteDisplayDao testSuiteDisplayDao,
									   TestPlanItemSuccessRateCalculator rateCalculator,
									   AutomatedSuiteDisplayDao automatedSuiteDisplayDao,
									   CustomReportDashboardService customReportDashboardService,
									   ReadUnassignedTestPlanHelper readUnassignedTestPlanHelper,
									   AvailableDatasetAppender availableDatasetAppender,
									   PartyPreferenceService partyPreferenceService,
									   UserContextService userContextService,
									   PermissionEvaluationService permissionEvaluationService
									   ) {
		this.iterationDisplayDao = iterationDisplayDao;
		this.dslContext = dslContext;
		this.attachmentDisplayDao = attachmentDisplayDao;
		this.customFieldValueDisplayDao = customFieldValueDisplayDao;
		this.iterationTestPlanFinder = iterationTestPlanFinder;
		this.milestoneDisplayDao = milestoneDisplayDao;
		this.hibernateIterationDao = hibernateIterationDao;
		this.issueDisplayDao = issueDisplayDao;
		this.iterationTestPlanManagerService = iterationTestPlanManagerService;
		this.testSuiteDisplayDao = testSuiteDisplayDao;
		this.rateCalculator = rateCalculator;
		this.automatedSuiteDisplayDao = automatedSuiteDisplayDao;
		this.customReportDashboardService = customReportDashboardService;
		this.readUnassignedTestPlanHelper = readUnassignedTestPlanHelper;
		this.availableDatasetAppender = availableDatasetAppender;
		this.partyPreferenceService = partyPreferenceService;
		this.userContextService = userContextService;
		this.permissionEvaluationService = permissionEvaluationService;
	}

	@PreAuthorize(CAN_READ_ITERATION)
	@Override
	public IterationDto findIterationView(Long iterationId) {
		IterationDto iteration = iterationDisplayDao.findById(iterationId);
		iteration.setAttachmentList(attachmentDisplayDao.findAttachmentListById(iteration.getAttachmentListId()));
		iteration.getCustomFieldValues().addAll(customFieldValueDisplayDao.findCustomFieldValues(BindableEntity.ITERATION, iterationId));
		iteration.setTestPlanStatistics(hibernateIterationDao.getIterationStatistics(iterationId));
		iteration.setNbIssues(issueDisplayDao.countIssuesByIterationId(iterationId));
		iteration.setMilestones(milestoneDisplayDao.getMilestonesByIterationId(iterationId));
		iteration.setTestSuites(testSuiteDisplayDao.findForIteration(iterationId));
		appendUsers(iteration);
		iteration.setExecutionStatusMap(iterationDisplayDao.getExecutionStatusMap(iterationId));
		iteration.setNbAutomatedSuites(automatedSuiteDisplayDao.countAutomatedSuiteByIterationId(iterationId));
		PartyPreference preference = partyPreferenceService
			.findPreferenceForCurrentUser(CorePartyPreference.FAVORITE_DASHBOARD_CAMPAIGN.getPreferenceKey());
		iteration.setCanShowFavoriteDashboard(customReportDashboardService.canShowDashboardInWorkspace(Workspace.CAMPAIGN));
		iteration.setShouldShowFavoriteDashboard(customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.CAMPAIGN));
		if (preference != null) {
			Long dashboardId = Long.valueOf(preference.getPreferenceValue());
			iteration.setFavoriteDashboardId(dashboardId);
		}
		appendNbTestPlanItems(iteration);
		return iteration;
	}

	private void appendUsers(IterationDto iteration) {
		List<User> users = this.iterationTestPlanManagerService.findAssignableUserForTestPlan(iteration.getId());
		iteration.setUsers(UserView.fromEntities(users));
	}

	private void appendNbTestPlanItems(IterationDto iteration) {
		String login = this.userContextService.getUsername();
		int count;

		if(currentUserCanReadUnassigned(iteration.getId())){
			count = this.iterationDisplayDao.getNbTestPlanItem(iteration.getId(), null);
		} else {
			count = this.iterationDisplayDao.getNbTestPlanItem(iteration.getId(), login);
		}
		iteration.setNbTestPlanItems(count);
	}

	private boolean currentUserCanReadUnassigned(Long iterationId) {
		return this.permissionEvaluationService.hasRoleOrPermissionOnObject(Roles.ROLE_ADMIN, READ_UNASSIGNED, iterationId, Iteration.SIMPLE_CASS_NAME);
	}

	@Override
	@PreAuthorize(CAN_READ_ITERATION)
	public GridResponse findTestPlan(Long iterationId, GridRequest gridRequest) {
		IterationTestPlanGrid testPlanGrid = new IterationTestPlanGrid(iterationId);
		this.readUnassignedTestPlanHelper.appendReadUnassignedFilter(gridRequest, iterationId, Iteration.SIMPLE_CASS_NAME);
		GridResponse gridResponse = testPlanGrid.getRows(gridRequest, this.dslContext);
		rateCalculator.appendSuccessRate(gridResponse);
		availableDatasetAppender.appendAvailableDatasets(gridResponse);
		return gridResponse;
	}
}
