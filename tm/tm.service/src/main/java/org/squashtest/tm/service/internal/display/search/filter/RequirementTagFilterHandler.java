/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search.filter;

import com.google.common.collect.Sets;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.hibernate.HibernateQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.QCustomFieldValue;
import org.squashtest.tm.domain.customfield.QCustomFieldValueOption;
import org.squashtest.tm.domain.customfield.QTagsValue;
import org.squashtest.tm.domain.jpql.ExtendedHibernateQuery;
import org.squashtest.tm.domain.query.CufTagOperation;
import org.squashtest.tm.domain.requirement.QRequirementVersion;
import org.squashtest.tm.domain.testcase.QTestCase;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

import java.util.List;
import java.util.Set;

import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.REQUIREMENT_VERSION_CUF_TAG;

@Component
public class RequirementTagFilterHandler implements FilterHandler{

	private static final Logger LOGGER = LoggerFactory.getLogger(RequirementTagFilterHandler.class);

	private final Set<String> handledPrototypes = Sets.newHashSet(REQUIREMENT_VERSION_CUF_TAG);

	@Override
	public boolean canHandleFilter(GridFilterValue filter) {
		return this.handledPrototypes.contains(filter.getColumnPrototype());
	}

	@Override
	public void handleFilter(ExtendedHibernateQuery<?> query, GridFilterValue filter) {
		LOGGER.debug("Begin create filter tags");

		CufTagOperation cufTagOperation = CufTagOperation.valueOf(filter.getOperation());

		List<String> tags = filter.getValues();

		QCustomFieldValue cfv = new QCustomFieldValue("cfv");
		QCustomFieldValueOption cfvo = new QCustomFieldValueOption("cfvo");

		QRequirementVersion outerRequirementVersion = QRequirementVersion.requirementVersion;

		QRequirementVersion initVersion = new QRequirementVersion("initVersion");
		QTagsValue tagsValue = new QTagsValue("tagsValue");

		HibernateQuery<Integer> subquery;

		long size = tags.size();

		LOGGER.debug("Create sub query");

		subquery = new ExtendedHibernateQuery<>().select(Expressions.ONE)
			.from(initVersion)
			.join(cfv).on(initVersion.id.eq(cfv.boundEntityId))
			.join(tagsValue).on(cfv.id.eq(tagsValue._super.id))
			.join(tagsValue.selectedOptions, cfvo)
			.where(cfv.boundEntityType.eq(BindableEntity.REQUIREMENT_VERSION)
				.and(initVersion.id.eq(outerRequirementVersion.id))
				.and(cfvo.label.in(tags)).and(cfv.cufId.eq(filter.getCufId())));

		if (cufTagOperation.equals(CufTagOperation.AND)) {

			subquery = subquery.groupBy(initVersion.id)
				.having(cfvo.label.count().eq(size));

		}
		query.where(subquery.exists());

		LOGGER.debug("End create filter tags");
	}
}
