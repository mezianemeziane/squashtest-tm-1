/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;
import org.squashtest.tm.service.internal.display.dto.TestAutomationServerDto;
import org.squashtest.tm.service.internal.repository.display.TestAutomationServerDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.THIRD_PARTY_SERVER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.BASE_URL;

@Repository
public class TestAutomationServerDisplayDaoImpl implements TestAutomationServerDisplayDao {

	@Autowired(required = false)
	private final Collection<TestAutomationConnector> connectors = Collections.emptyList();

	@Inject
	private DSLContext dslContext;

	@Override
	public List<TestAutomationServerDto> findAll() {
		return dslContext.select(TEST_AUTOMATION_SERVER.SERVER_ID.as(RequestAliasesConstants.ID), TEST_AUTOMATION_SERVER.KIND,
			TEST_AUTOMATION_SERVER.DESCRIPTION, TEST_AUTOMATION_SERVER.CREATED_BY, TEST_AUTOMATION_SERVER.CREATED_ON,
			TEST_AUTOMATION_SERVER.LAST_MODIFIED_BY, TEST_AUTOMATION_SERVER.LAST_MODIFIED_ON,
			TEST_AUTOMATION_SERVER.MANUAL_SLAVE_SELECTION,
			THIRD_PARTY_SERVER.URL.as(BASE_URL), THIRD_PARTY_SERVER.NAME
		)
			.from(TEST_AUTOMATION_SERVER)
			.innerJoin(THIRD_PARTY_SERVER).on(THIRD_PARTY_SERVER.SERVER_ID.eq(TEST_AUTOMATION_SERVER.SERVER_ID))
			.fetchInto(TestAutomationServerDto.class);
	}

	@Override
	public Set<TestAutomationServerKind> findAllAvailableKinds() {
		return connectors.stream()
				.map(TestAutomationConnector::getConnectorKind)
				.collect(Collectors.toSet());
	}

	@Override
	public TestAutomationServerDto getTestAutomationServerById(long testAutomationServerId) {
		return dslContext.select(
			TEST_AUTOMATION_SERVER.SERVER_ID.as(RequestAliasesConstants.ID),
			THIRD_PARTY_SERVER.NAME,
			THIRD_PARTY_SERVER.URL.as(BASE_URL),
			THIRD_PARTY_SERVER.AUTH_PROTOCOL,
			TEST_AUTOMATION_SERVER.KIND,
			TEST_AUTOMATION_SERVER.DESCRIPTION,
			TEST_AUTOMATION_SERVER.MANUAL_SLAVE_SELECTION,
			TEST_AUTOMATION_SERVER.CREATED_BY,
			TEST_AUTOMATION_SERVER.CREATED_ON,
			TEST_AUTOMATION_SERVER.LAST_MODIFIED_BY,
			TEST_AUTOMATION_SERVER.LAST_MODIFIED_ON
		).from(TEST_AUTOMATION_SERVER)
			.innerJoin(THIRD_PARTY_SERVER).on(THIRD_PARTY_SERVER.SERVER_ID.eq(TEST_AUTOMATION_SERVER.SERVER_ID))
			.where(TEST_AUTOMATION_SERVER.SERVER_ID.eq(testAutomationServerId))
			.fetchOne().into(TestAutomationServerDto.class);
	}
}
