/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.testcase;

public class KeywordTestStepDto extends TestStepDto {

	public static final String KEYWORD_STEP = "keyword-step";

	private Long id;
	private String keyword;

	private Long actionWordId;
	private Long actionWordProjectId;
	private String action;
	private String styledAction;

	private String datatable;
	private String docstring;
	private String comment;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getKind() {
		return KEYWORD_STEP;
	}

	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Long getActionWordId() {
		return actionWordId;
	}
	public void setActionWordId(Long actionWordId) {
		this.actionWordId = actionWordId;
	}

	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

	public String getStyledAction() {
		return styledAction;
	}
	public void setStyledAction(String styledAction) {
		this.styledAction = styledAction;
	}

	public String getDatatable() {
		return datatable;
	}
	public void setDatatable(String datatable) {
		this.datatable = datatable;
	}

	public String getDocstring() {
		return docstring;
	}
	public void setDocstring(String docstring) {
		this.docstring = docstring;
	}

	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public Long getActionWordProjectId() {
		return actionWordProjectId;
	}

	public void setActionWordProjectId(Long actionWordProjectId) {
		this.actionWordProjectId = actionWordProjectId;
	}
}
