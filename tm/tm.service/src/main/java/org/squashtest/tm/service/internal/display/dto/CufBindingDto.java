/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.RenderingLocation;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class CufBindingDto {
	private Long id;
	private Long customFieldId;
	private BindableEntity bindableEntity;
	private Set<RenderingLocation> renderingLocations = new HashSet<>(5);
	private Long boundProjectId;
	private int position = 0;

	public CufBindingDto(Long id, Long customFieldId, BindableEntity bindableEntity, Long boundProjectId, int position) {
		this.id = id;
		this.customFieldId = customFieldId;
		this.bindableEntity = bindableEntity;
		this.boundProjectId = boundProjectId;
		this.position = position;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomFieldId() {
		return customFieldId;
	}

	public void setCustomFieldId(Long customFieldId) {
		this.customFieldId = customFieldId;
	}

	public Set<RenderingLocation> getRenderingLocations() {
		return renderingLocations;
	}

	public void setRenderingLocations(Set<RenderingLocation> renderingLocations) {
		this.renderingLocations = renderingLocations;
	}

	public void addRenderingLocation(RenderingLocation renderingLocation) {
		if (renderingLocation != null) {
			this.renderingLocations.add(renderingLocation);
		}
	}

	public void addRenderingLocations(Set<RenderingLocation> renderingLocation) {
		if (renderingLocation != null) {
			this.renderingLocations.addAll(renderingLocation);
		}
	}

	public Long getBoundProjectId() {
		return boundProjectId;
	}

	public void setBoundProjectId(Long boundProjectId) {
		this.boundProjectId = boundProjectId;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public BindableEntity getBindableEntity() {
		return bindableEntity;
	}

	public void setBindableEntity(BindableEntity bindableEntity) {
		this.bindableEntity = bindableEntity;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		CufBindingDto that = (CufBindingDto) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
