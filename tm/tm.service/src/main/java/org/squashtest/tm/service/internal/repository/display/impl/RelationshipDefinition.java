/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.TableField;
import org.squashtest.tm.domain.NodeType;

import java.util.HashMap;
import java.util.Map;

import static org.squashtest.tm.jooq.domain.Tables.*;

/**
 * Declarative enum of relations existing into legacy tree data structure in SquashTM.
 * By legacy i mean Requirement, TestCase and Campaign trees.
 * It's a sort of mapping but a lot simpler, so we can use it in a reusable way with jOOQ.
 */
public enum RelationshipDefinition {
	TEST_CASE_LIBRARY_RELATION(NodeType.TEST_CASE_LIBRARY, TEST_CASE_LIBRARY_CONTENT.LIBRARY_ID, TEST_CASE_LIBRARY_CONTENT.CONTENT_ID, TEST_CASE_LIBRARY_CONTENT.CONTENT_ORDER) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.TEST_CASE, TEST_CASE.TCLN_ID);
			relations.put(NodeType.TEST_CASE_FOLDER, TEST_CASE_FOLDER.TCLN_ID);
			return relations;
		}
	},
	TEST_CASE_FOLDER_RELATION(NodeType.TEST_CASE_FOLDER, TCLN_RELATIONSHIP.ANCESTOR_ID, TCLN_RELATIONSHIP.DESCENDANT_ID, TCLN_RELATIONSHIP.CONTENT_ORDER) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.TEST_CASE, TEST_CASE.TCLN_ID);
			relations.put(NodeType.TEST_CASE_FOLDER, TEST_CASE_FOLDER.TCLN_ID);
			return relations;
		}
	},
	REQUIREMENT_LIBRARY_RELATION(NodeType.REQUIREMENT_LIBRARY, REQUIREMENT_LIBRARY_CONTENT.LIBRARY_ID, REQUIREMENT_LIBRARY_CONTENT.CONTENT_ID, REQUIREMENT_LIBRARY_CONTENT.CONTENT_ORDER) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.REQUIREMENT, REQUIREMENT.RLN_ID);
			relations.put(NodeType.REQUIREMENT_FOLDER, REQUIREMENT_FOLDER.RLN_ID);
			return relations;
		}
	},
	REQUIREMENT_FOLDER_RELATION(NodeType.REQUIREMENT_FOLDER, RLN_RELATIONSHIP.ANCESTOR_ID, RLN_RELATIONSHIP.DESCENDANT_ID, RLN_RELATIONSHIP.CONTENT_ORDER) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.REQUIREMENT, REQUIREMENT.RLN_ID);
			relations.put(NodeType.REQUIREMENT_FOLDER, REQUIREMENT_FOLDER.RLN_ID);
			return relations;
		}
	},
	REQUIREMENT_RELATION(NodeType.REQUIREMENT, RLN_RELATIONSHIP.ANCESTOR_ID, RLN_RELATIONSHIP.DESCENDANT_ID, RLN_RELATIONSHIP.CONTENT_ORDER) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.REQUIREMENT, REQUIREMENT.RLN_ID);
			return relations;
		}
	},
	CAMPAIGN_LIBRARY_RELATION(NodeType.CAMPAIGN_LIBRARY, CAMPAIGN_LIBRARY_CONTENT.LIBRARY_ID, CAMPAIGN_LIBRARY_CONTENT.CONTENT_ID, CAMPAIGN_LIBRARY_CONTENT.CONTENT_ORDER) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.CAMPAIGN_FOLDER, CAMPAIGN_FOLDER.CLN_ID);
			relations.put(NodeType.CAMPAIGN, CAMPAIGN.CLN_ID);
			return relations;
		}
	},
	CAMPAIGN_FOLDER_RELATION(NodeType.CAMPAIGN_FOLDER, CLN_RELATIONSHIP.ANCESTOR_ID, CLN_RELATIONSHIP.DESCENDANT_ID, CLN_RELATIONSHIP.CONTENT_ORDER) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.CAMPAIGN_FOLDER, CAMPAIGN_FOLDER.CLN_ID);
			relations.put(NodeType.CAMPAIGN, CAMPAIGN.CLN_ID);
			return relations;
		}
	},
	CAMPAIGN_RELATION(NodeType.CAMPAIGN, CAMPAIGN_ITERATION.CAMPAIGN_ID, CAMPAIGN_ITERATION.ITERATION_ID, CAMPAIGN_ITERATION.ITERATION_ORDER) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.ITERATION, ITERATION.ITERATION_ID);
			return relations;
		}
	},
	ITERATION_RELATION(NodeType.ITERATION, ITERATION_TEST_SUITE.ITERATION_ID, ITERATION_TEST_SUITE.TEST_SUITE_ID, ITERATION_TEST_SUITE.ITERATION_TEST_SUITE_ORDER) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.TEST_SUITE, TEST_SUITE.ID);
			return relations;
		}
	};

	RelationshipDefinition(NodeType nodeType, TableField<?, Long> ancestorField, TableField<?, Long> descendantField, TableField<?, Integer> orderField) {
		this.nodeType = nodeType;
		this.ancestorField = ancestorField;
		this.descendantField = descendantField;
		this.orderField = orderField;
	}

	private NodeType nodeType;
	private TableField<?, Long> ancestorField;
	private TableField<?, Long> descendantField;
	private TableField<?, Integer> orderField;

	public NodeType getNodeType() {
		return nodeType;
	}

	public TableField<?, Long> getAncestorField() {
		return ancestorField;
	}

	public TableField<?, Long> getDescendantField() {
		return descendantField;
	}

	public TableField<?, Integer> getOrderField() {
		return orderField;
	}

	abstract public Map<NodeType, TableField<?, Long>> getRelationshipIds();
}
