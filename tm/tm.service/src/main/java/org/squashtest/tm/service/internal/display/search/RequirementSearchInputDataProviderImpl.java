/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.service.display.search.RequirementSearchInputData;
import org.squashtest.tm.service.display.search.RequirementSearchInputDataProvider;
import org.squashtest.tm.service.internal.display.dto.UserView;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.UserDao;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.user.UserAccountService;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Transactional(readOnly = true)
public class RequirementSearchInputDataProviderImpl implements RequirementSearchInputDataProvider {

	private final ProjectFinder projectFinder;

	private final ProjectDao projectDao;

	private final UserAccountService userAccountService;

	private final UserDao userDao;

	public RequirementSearchInputDataProviderImpl(
		ProjectFinder projectFinder,
		ProjectDao projectDao,
		UserAccountService userAccountService,
		UserDao userDao) {
		this.projectFinder = projectFinder;
		this.projectDao = projectDao;
		this.userAccountService = userAccountService;
		this.userDao = userDao;
	}

	@Override
	public RequirementSearchInputData provide() {
		UserDto currentUser = userAccountService.findCurrentUserDto();
		List<Long> readableProjectIds = projectFinder.findAllReadableIds(currentUser);
		List<UserView> createdUsers = findCreatedUserViews(readableProjectIds);
		List<UserView> modifiedUsers = findModifiedUserViews(readableProjectIds);
		return new RequirementSearchInputData(createdUsers, modifiedUsers);
	}

	private List<UserView> findCreatedUserViews(List<Long> readableProjectIds) {
		List<String> usersWhoCreatedRequirementVersions = projectDao.findUsersWhoCreatedRequirementVersions(readableProjectIds);
		return findUserViewsByLogin(usersWhoCreatedRequirementVersions);
	}

	private List<UserView> findModifiedUserViews(List<Long> readableProjectIds) {
		List<String> usersWhoModifiedRequirementVersions = projectDao.findUsersWhoModifiedRequirementVersions(readableProjectIds);
		return findUserViewsByLogin(usersWhoModifiedRequirementVersions);
	}

	private List<UserView> findUserViewsByLogin(List<String> usersWhoCreatedTestCases) {
		List<User> users = userDao.findUsersByLoginList(usersWhoCreatedTestCases);
		Map<String, UserView> userViews = users.stream()
			.map(user -> {
				UserView userView = new UserView();
				userView.setId(user.getId());
				userView.setFirstName(user.getFirstName());
				userView.setLastName(user.getLastName());
				userView.setLogin(user.getLogin());
				return userView;
			}).collect(Collectors.toMap(UserView::getLogin, Function.identity()));

		return usersWhoCreatedTestCases.stream()
			.map(login -> userViews.getOrDefault(login, new UserView(login)))
			.collect(Collectors.toList());
	}


}
