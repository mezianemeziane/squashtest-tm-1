/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.templateplugin;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.squashtest.tm.api.template.TemplateConfigurablePlugin;
import org.squashtest.tm.service.templateplugin.TemplateConfigurablePluginService;

import javax.validation.constraints.NotBlank;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class TemplateConfigurablePluginManagerImpl implements TemplateConfigurablePluginService {

    @Autowired(required = false)
    private final Collection<TemplateConfigurablePlugin> plugins = Collections.emptyList();

    @Override
    public Optional<TemplateConfigurablePlugin> findById(@NotBlank @NonNull String id) {
        return plugins.stream().filter(plugin -> id.equals(plugin.getId())).findFirst();
    }

    @Override
    public List<TemplateConfigurablePlugin> findAll() {
        return Lists.newArrayList(plugins);
    }
}
