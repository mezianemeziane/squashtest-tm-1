/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search;

import com.querydsl.core.Tuple;
import com.querydsl.core.types.Order;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.jpql.ExtendedHibernateQuery;
import org.squashtest.tm.domain.query.*;
import org.squashtest.tm.jooq.domain.tables.records.ProjectRecord;
import org.squashtest.tm.service.display.search.ResearchResult;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridSort;
import org.squashtest.tm.service.internal.display.search.filter.FilterHandlers;
import org.squashtest.tm.service.internal.display.search.filter.FilterValueHandlers;
import org.squashtest.tm.service.internal.repository.ColumnPrototypeDao;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.query.ConfiguredQuery;
import org.squashtest.tm.service.query.QueryProcessingService;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

public abstract class AbstractSearchService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractSearchService.class);

	protected Map<String, QueryColumnPrototype> prototypesByLabel;
	protected QueryProcessingService queryService;
	protected ColumnPrototypeDao columnPrototypeDao;
	protected EntityManager entityManager;
	protected ProjectFinder projectFinder;
	protected FilterHandlers filterHandlers;
	protected FilterValueHandlers gridFilterValueHandlers;
	private final DSLContext dslContext;

	public AbstractSearchService(QueryProcessingService queryService,
								 ColumnPrototypeDao columnPrototypeDao,
								 EntityManager entityManager,
								 ProjectFinder projectFinder,
								 FilterHandlers filterHandlers,
								 FilterValueHandlers gridFilterValueHandlers,
								 DSLContext dslContext) {
		this.queryService = queryService;
		this.columnPrototypeDao = columnPrototypeDao;
		this.entityManager = entityManager;
		this.projectFinder = projectFinder;
		this.filterHandlers = filterHandlers;
		this.gridFilterValueHandlers = gridFilterValueHandlers;
		this.dslContext = dslContext;
	}

	@PostConstruct
	public void init() {
		this.prototypesByLabel = columnPrototypeDao.findAll().stream().collect(Collectors.toMap(
			QueryColumnPrototype::getLabel,
			Function.identity()
		));
	}

	public ResearchResult search(GridRequest gridRequest) {
		assertCurrentUserHasPermissionToSearch();
		QueryModel query = prepareBaseQuery(gridRequest);
		List<Long> ids = searchPaginatedEntityIds(gridRequest, query);
		long count = countMatchingEntities(gridRequest, query);
		return new ResearchResult(ids, count);
	}

	// [SQUASH-3654] Prevent user with no project permissions to access search
	private void assertCurrentUserHasPermissionToSearch() {
		if (!projectFinder.canReadAtLeastOneProject()) {
			throw new AccessDeniedException("No permissions to search");
		}
	}

	private long countMatchingEntities(GridRequest request, QueryModel query) {
		ExtendedHibernateQuery<Tuple> countHibernateQuery = prepareHibernateQuery(request, query);
		countHibernateQuery.limit(Long.MAX_VALUE);
		countHibernateQuery.offset(0);
		return countHibernateQuery
			.clone(getSession())
			.fetchCount();
	}

	private List<Long> searchPaginatedEntityIds(GridRequest request, QueryModel query) {
		ExtendedHibernateQuery<Tuple> extendedHibernateQuery = prepareHibernateQuery(request, query);
		return extendedHibernateQuery
			.clone(getSession())
			.fetch()
			.stream()
			.map(tuple -> tuple.get(0, Long.class))
			.collect(Collectors.toList());
	}

	private ExtendedHibernateQuery<Tuple> prepareHibernateQuery(GridRequest request, QueryModel query) {
		ConfiguredQuery configuredQuery = preparePaginatedConfiguredQuery(request, query);
		ExtendedHibernateQuery<Tuple> extendedHibernateQuery = queryService.prepareQuery(configuredQuery);
		this.filterHandlers.handleFiltersOutOfQueryEngine(extendedHibernateQuery, request);
		return extendedHibernateQuery;
	}

	private Session getSession() {
		return entityManager.unwrap(Session.class);
	}

	private ConfiguredQuery preparePaginatedConfiguredQuery(GridRequest request, QueryModel query) {
		Pageable pageable = null;
		if (request.getPage() != null && request.getSize() != null) {
			pageable = PageRequest.of(request.getPage(), request.getSize());
		}
		ConfiguredQuery configuredQuery = new ConfiguredQuery();
		List<EntityReference> scope = computeScope(request);
		configuredQuery.setScope(scope);
		configuredQuery.setQueryModel(query);
		configuredQuery.setPaging(pageable);
		return configuredQuery;
	}

	private List<EntityReference> computeScope(GridRequest request) {
		List<EntityReference> scope;
		if (request.getScope().isEmpty()) {
			scope = computeDefaultScope();
		} else {
			scope = computeCustomScope(request);
		}
		// [SQUASH-3768][SQUASH-3769]
		// We must convert project perimeter into library as the search/custom-chart engine has incorrect perimeter handling
		// as 1.22/2.0 the engine apply the perimeter to all elements in query, projection, filters and sorts.
		// Example :
		// - 2 projects A and B.
		// - 2 test case in each project A1,A2 and B1,B2 with references REF-A1, REF-A2 and REF-B1, REF-2
		// - One iteration in project B with 4 itpi in test plan, one for each test case.
		//  When i do a search with a filter on reference attribute, value searched 'REF' :
		// - If i have the two project in my perimeter A and B, i got the four itpi.
		// - However, if i have only project B, i get only two itpi ib search result, because a project perimeter is applied also on test-case entity.
		// because the reference filter is a test case attribute. The search engine is not able to make difference between the primary entity and entities added into the
		// request because of filters or sorts. Il lead to very dirty behavior, like rows missing from search results when sorting...
		// To avoid this, i got two way :
		// - Fixing the whole engine which could be is very complicated because of the persisted aspect of custom charts. Business is not ready to delay 2.0 version for this...
		// - For search only, converting project perimeters into library perimeters, so the perimeter can be only applied on primary entity. Note that if one
		// day, business choose to invest the required time to fix the engine, this code should become optional, but it should not cause any harm...
		return this.convertProjectIntoLibraries(scope);
	}

	// As 2.0 we assumes that we have no project/entity mixed perimeters, aka no [Project-1, TestCaseFolder-4]
	// Mixed perimeter can come from trees and thus are of form [TestCaseLibrary-1, TestCaseFolder-4]
	private List<EntityReference> convertProjectIntoLibraries(List<EntityReference> entityReferences) {
		boolean hasProject = entityReferences.stream().anyMatch(e -> e.getType() == EntityType.PROJECT);
		if (hasProject) {
			List<Long> projectIds = entityReferences.stream()
				.filter(entityReference -> entityReference.getType() == EntityType.PROJECT)
				.map(EntityReference::getId)
				.collect(Collectors.toList());

			List<EntityReference> libraryIds = findLibraryIds(projectIds).stream()
				.map(id -> new EntityReference(getLibraryEntityType(), id))
				.collect(Collectors.toList());

			if(libraryIds.size() != entityReferences.size()){
				throw new IllegalArgumentException("Incorrect perimeter in that search... " + entityReferences);
			}
			return libraryIds;
		} else {
			return entityReferences;
		}
	}

	abstract protected EntityType getLibraryEntityType();

	private List<Long> findLibraryIds(List<Long> projectIds) {
		return dslContext.select(getLibraryIdField())
			.from(PROJECT)
			.where(PROJECT.PROJECT_ID.in(projectIds))
			.fetch(getLibraryIdField());
	}

	abstract protected TableField<ProjectRecord, Long> getLibraryIdField();

	private List<EntityReference> computeCustomScope(GridRequest request) {
		return request.getScope().stream()
			.map(EntityReference::fromNodeId)
			.collect(Collectors.toList());
	}

	private List<EntityReference> computeDefaultScope() {
		return this.projectFinder.findAllReadableIds().stream()
			.map(id -> new EntityReference(EntityType.PROJECT, id))
			.collect(Collectors.toList());
	}

	protected QueryModel prepareBaseQuery(GridRequest request) {
		QueryModel query = new QueryModel();
		query.setStrategy(QueryStrategy.MAIN);
		query.setJoinStyle(NaturalJoinStyle.INNER_JOIN);
		prepareProjection(request, query);
		prepareFilters(request, query);
		prepareOrders(request, query);
		return query;
	}

	private void prepareOrders(GridRequest request, QueryModel query) {
		List<GridSort> gridSorts = request.getSort();
		List<QueryOrderingColumn> orderingColumns;
		orderingColumns = prepareCustomSorts(gridSorts);
		query.setOrderingColumns(orderingColumns);
	}

	private List<QueryOrderingColumn> prepareCustomSorts(List<GridSort> gridSorts) {
		List<QueryOrderingColumn> orderingColumns = gridSorts.stream()
			.map(this::prepareCustomSort)
			.collect(Collectors.toList());
		orderingColumns.add(prepareDefaultOrderingColumn());
		return orderingColumns;
	}

	private QueryOrderingColumn prepareCustomSort(GridSort gridSort) {
		QueryOrderingColumn queryOrderingColumn = new QueryOrderingColumn();
		QueryColumnPrototype columnPrototype = findColumnPrototype(gridSort.getColumnPrototype());
		queryOrderingColumn.setColumnPrototype(columnPrototype);
		if (gridSort.getDirection().equals(GridSort.SortDirection.ASC)) {
			queryOrderingColumn.setOrder(Order.ASC);
		} else {
			queryOrderingColumn.setOrder(Order.DESC);
		}
		queryOrderingColumn.setOperation(Operation.NONE);
		return queryOrderingColumn;
	}

	private List<QueryOrderingColumn> prepareDefaultSort() {
		return Collections.singletonList(prepareDefaultOrderingColumn());
	}

	private QueryOrderingColumn prepareDefaultOrderingColumn() {
		QueryOrderingColumn queryOrderingColumn = new QueryOrderingColumn();
		queryOrderingColumn.setColumnPrototype(findColumnPrototype(getDefaultSortColumnName()));
		queryOrderingColumn.setOperation(Operation.NONE);
		queryOrderingColumn.setOrder(Order.ASC);
		return queryOrderingColumn;
	}

	private void prepareFilters(GridRequest request, QueryModel query) {
		List<QueryFilterColumn> filters = new ArrayList<>();
		request.getFilterValues()
			.stream()
			.filter(filter -> !this.filterHandlers.isHandledOutOfQueryEngine(filter))
			.forEach(gridFilterValue -> {
				String columnPrototype = gridFilterValue.getColumnPrototype();
				if (StringUtils.isNotBlank(columnPrototype)) {
					QueryFilterColumn queryFilterColumn = prepareFilter(gridFilterValue, columnPrototype);
					filters.add(queryFilterColumn);
				}
			});
		query.setFilterColumns(filters);
	}

	private QueryFilterColumn prepareFilter(GridFilterValue gridFilterValue, String columnPrototype) {
		QueryColumnPrototype filterPrototype;
		filterPrototype = findColumnPrototype(columnPrototype);
		QueryFilterColumn queryFilterColumn = new QueryFilterColumn();
		queryFilterColumn.setColumn(filterPrototype);
		queryFilterColumn.setOperation(Operation.valueOf(gridFilterValue.getOperation()));
		this.gridFilterValueHandlers.handleGridFilterValuesOutOfQueryEngine(gridFilterValue);
		queryFilterColumn.addValues(gridFilterValue.getValues());
		queryFilterColumn.setCufId(gridFilterValue.getCufId());
		return queryFilterColumn;
	}

	private QueryColumnPrototype findColumnPrototype(String columnPrototypeLabel) {
		QueryColumnPrototype filterPrototype;
		if (StringUtils.isBlank(columnPrototypeLabel)) {
			throw new IllegalArgumentException("Cannot find a prototype without its identifier");
		}
		if (prototypesByLabel.containsKey(columnPrototypeLabel)) {
			filterPrototype = prototypesByLabel.get(columnPrototypeLabel);
		} else {
			throw new IllegalArgumentException("Unknown column prototype " + columnPrototypeLabel);
		}
		return filterPrototype;
	}

	// we project only on id column and ordered columns as we will fetch the rows in a second time
	private void prepareProjection(GridRequest request, QueryModel query) {
		List<QueryProjectionColumn> idProjection = prepareProjectionOnId();
		List<QueryProjectionColumn> projections = new ArrayList<>(idProjection);
		List<QueryProjectionColumn> projectionColumns = request.getSort().stream()
//			.filter(gridSort -> !gridSort.getColumnPrototype().equals(getIdentifierColumnName()))
			.filter(gridSort -> !getIdentifierColumnName().contains(gridSort.getColumnPrototype()))
			.map(gridSort -> {
				QueryProjectionColumn queryProjectionColumn = new QueryProjectionColumn();
				queryProjectionColumn.setColumnPrototype(findColumnPrototype(gridSort.getColumnPrototype()));
				queryProjectionColumn.setOperation(Operation.NONE);
				return queryProjectionColumn;
			})
			.collect(Collectors.toList());
		projections.addAll(projectionColumns);
		query.setProjectionColumns(projections);
	}

	private List<QueryProjectionColumn> prepareProjectionOnId() {
		return getIdentifierColumnName().stream()
			.map(columnPrototypeName -> {
				QueryProjectionColumn queryProjectionColumn = new QueryProjectionColumn();
				queryProjectionColumn.setColumnPrototype(findColumnPrototype(columnPrototypeName));
				queryProjectionColumn.setOperation(Operation.NONE);
				return queryProjectionColumn;
			}).collect(Collectors.toList());
	}

	/**
	 * @return The {@link QueryColumnPrototype} names of the columns used as identifier for looked entities.
	 * Note that the first column will be the final entity id, used for building the {@link ResearchResult}
	 * The other columns can be needed to help the query engine to construct the correct query plan
	 * Ex: For TestCase, it will be TEST_CASE_ID... as defined in {@link QueryColumnPrototypeReference}
	 * Ex: For RequirementVersions, it will be REQUIREMENT_VERSION_ID AND REQUIREMENT_ID because the query engine is not able to go from requirement version
	 * to project scope without making a cross join.
	 */
	protected abstract List<String> getIdentifierColumnName();

	protected abstract String getDefaultSortColumnName();
}
