/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.actionword;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.domain.bdd.ActionWordParameter;
import org.squashtest.tm.domain.bdd.util.ActionWordUtil;
import org.squashtest.tm.exception.actionword.InvalidActionWordInputException;
import org.squashtest.tm.exception.actionword.InvalidActionWordParameterNameException;
import org.squashtest.tm.exception.actionword.InvalidActionWordParameterValueException;
import org.squashtest.tm.service.actionword.ActionWordLibraryNodeService;
import org.squashtest.tm.service.actionword.ActionWordParameterService;
import org.squashtest.tm.service.internal.repository.ActionWordParameterDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.security.SecurityCheckableObject;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Transactional
public class ActionWordParameterServiceImpl implements ActionWordParameterService {

	@Inject
	private ActionWordParameterDao actionWordParameterDao;

	@Inject
	private ActionWordLibraryNodeService actionWordLibraryNodeService;

	@Inject
	private PermissionEvaluationService permissionEvaluationService;


	@Override
	public String renameParameter(long parameterId, String newName) {
		ActionWordParameter parameter = actionWordParameterDao.getOne(parameterId);
		if (StringUtils.isBlank(newName)) {
			throw new InvalidActionWordParameterNameException("Action word parameter name cannot be blank.");
		}
		String trimmedName = newName.trim();
		if (! parameter.getName().equals(trimmedName)) {
			ActionWord actionWord = parameter.getActionWord();

			PermissionsUtils.checkPermission(permissionEvaluationService, new SecurityCheckableObject(actionWord, "WRITE"));
			checkNewActionWordLength(actionWord);
			checkIfParamNameIsValid(trimmedName);

			parameter.setName(trimmedName);

			// update ActionWordLibraryNode name
			actionWordLibraryNodeService.renameNodeFromActionWord(actionWord);
		}
		return trimmedName;
	}

	private void checkNewActionWordLength(ActionWord actionWord) {
		String newWord = actionWord.createWord();
		String newWordWithDefaultValues = actionWord.createWordWithDefaultValues();
		if (newWord.length() > 255 || newWordWithDefaultValues.length() > 255 ) {
			throw new InvalidActionWordInputException("Invalid action word input");
		}
	}

	private void checkIfParamNameIsValid(String trimmedName) {
		Pattern pattern = Pattern.compile("[^\\w-]");
		Matcher matcher = pattern.matcher(trimmedName);
		if (matcher.find()) {
			throw new InvalidActionWordParameterNameException("Action word parameter name must contain only alphanumeric, dash or underscore characters.");
		}
	}

	@Override
	public String updateParameterDefaultValue(long parameterId, @NotNull String newDefaultValue) {
		ActionWordParameter parameter = actionWordParameterDao.getOne(parameterId);
		ActionWord actionWord = parameter.getActionWord();
		PermissionsUtils.checkPermission(permissionEvaluationService, new SecurityCheckableObject(actionWord, "WRITE"));

		String updatedNewDefaultValue = ActionWordUtil.replaceExtraSpacesInText(newDefaultValue.trim());

		if (updatedNewDefaultValue.contains("<")
			|| updatedNewDefaultValue.contains(">")
			|| updatedNewDefaultValue.contains("\"")){
			throw new InvalidActionWordParameterValueException("The default value cannot contain <, > or \" character");
		}
		parameter.setDefaultValue(updatedNewDefaultValue);

		checkNewActionWordLength(actionWord);

		return updatedNewDefaultValue;
	}
}
