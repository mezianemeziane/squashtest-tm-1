/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.Date;

public class RemoteAutomationRequestExtenderDto {

	private Long id;
	private Long serverId;
	private String remoteStatus;
	private String remoteIssueKey;
	private String remoteRequestUrl;
	private String remoteAssignedTo;
	private String sentValueForSync;
	private String synchronizableIssueStatus;
	private Date lastSyncDateSquash;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getServerId() {
		return serverId;
	}

	public void setServerId(Long serverId) {
		this.serverId = serverId;
	}

	public String getRemoteStatus() {
		return remoteStatus;
	}

	public void setRemoteStatus(String remoteStatus) {
		this.remoteStatus = remoteStatus;
	}

	public String getRemoteIssueKey() {
		return remoteIssueKey;
	}

	public void setRemoteIssueKey(String remoteIssueKey) {
		this.remoteIssueKey = remoteIssueKey;
	}

	public String getRemoteRequestUrl() {
		return remoteRequestUrl;
	}

	public void setRemoteRequestUrl(String remoteRequestUrl) {
		this.remoteRequestUrl = remoteRequestUrl;
	}

	public String getRemoteAssignedTo() {
		return remoteAssignedTo;
	}

	public void setRemoteAssignedTo(String remoteAssignedTo) {
		this.remoteAssignedTo = remoteAssignedTo;
	}

	public String getSentValueForSync() {
		return sentValueForSync;
	}

	public void setSentValueForSync(String sentValueForSync) {
		this.sentValueForSync = sentValueForSync;
	}

	public String getSynchronizableIssueStatus() {
		return synchronizableIssueStatus;
	}

	public void setSynchronizableIssueStatus(String synchronizableIssueStatus) {
		this.synchronizableIssueStatus = synchronizableIssueStatus;
	}

	public Date getLastSyncDateSquash() {
		return lastSyncDateSquash;
	}

	public void setLastSyncDateSquash(Date lastSyncDateSquash) {
		this.lastSyncDateSquash = lastSyncDateSquash;
	}
}
