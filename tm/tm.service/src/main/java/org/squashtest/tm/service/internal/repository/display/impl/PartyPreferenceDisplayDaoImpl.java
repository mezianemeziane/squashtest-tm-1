/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.users.preferences.CorePartyPreference;
import org.squashtest.tm.service.internal.repository.display.PartyPreferenceDisplayDao;

import java.util.Arrays;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.PARTY_PREFERENCE;

@Repository
public class PartyPreferenceDisplayDaoImpl implements PartyPreferenceDisplayDao {

	private DSLContext dsl;

	public PartyPreferenceDisplayDaoImpl(DSLContext dsl) {
		this.dsl = dsl;
	}

	@Override
	public List<String> findPreferenceKeysForFavoriteDashboardAndUser(long dashboardId, long userId) {

		List<String> workspaceFavoriteKeys = Arrays.asList(CorePartyPreference.FAVORITE_DASHBOARD_CAMPAIGN.getPreferenceKey(),
			CorePartyPreference.FAVORITE_DASHBOARD_TEST_CASE.getPreferenceKey(),
			CorePartyPreference.FAVORITE_DASHBOARD_REQUIREMENT.getPreferenceKey(), CorePartyPreference.FAVORITE_DASHBOARD_HOME.getPreferenceKey());

		return dsl.select(PARTY_PREFERENCE.PREFERENCE_KEY)
			.from(PARTY_PREFERENCE)
			.where(PARTY_PREFERENCE.PARTY_ID.eq(userId))
			.and(PARTY_PREFERENCE.PREFERENCE_KEY.in(workspaceFavoriteKeys))
			.and(PARTY_PREFERENCE.PREFERENCE_VALUE.eq(String.valueOf(dashboardId)))
			.fetch(PARTY_PREFERENCE.PREFERENCE_KEY);
	}
}
