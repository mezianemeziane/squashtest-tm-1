/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.campaign;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.service.campaign.TestSuiteTestPlanManagerService;
import org.squashtest.tm.service.display.campaign.TestSuiteDisplayService;
import org.squashtest.tm.service.internal.display.dto.UserView;
import org.squashtest.tm.service.internal.display.dto.campaign.TestSuiteDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.campaign.TestSuiteTestPlanGrid;
import org.squashtest.tm.service.internal.repository.TestSuiteDao;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.AutomatedSuiteDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.IssueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TestSuiteDisplayDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.UserContextService;

import java.util.List;

import static org.squashtest.tm.service.security.Authorizations.READ_TS_OR_ROLE_ADMIN;

@Service
@Transactional(readOnly = true)
public class TestSuiteDisplayServiceImpl implements TestSuiteDisplayService {

	private static final String READ_UNASSIGNED = "READ_UNASSIGNED";

	private final TestSuiteDisplayDao testSuiteDisplayDao;
	private final TestSuiteDao testSuiteDao;
	private final AttachmentDisplayDao attachmentDisplayDao;
	private final CustomFieldValueDisplayDao customFieldValueDisplayDao;
	private final IssueDisplayDao issueDisplayDao;
	private final MilestoneDisplayDao milestoneDisplayDao;
	private final DSLContext dslContext;
	private final TestSuiteTestPlanManagerService testSuiteTestPlanManagerService;
	private final TestPlanItemSuccessRateCalculator successRateCalculator;
	private final AutomatedSuiteDisplayDao automatedSuiteDisplayDao;
	private final ReadUnassignedTestPlanHelper readUnassignedTestPlanHelper;
	private final AvailableDatasetAppender availableDatasetAppender;
	private final UserContextService userContextService;
	private final PermissionEvaluationService permissionEvaluationService;

	public TestSuiteDisplayServiceImpl(TestSuiteDisplayDao testSuiteDisplayDao,
									   TestSuiteDao testSuiteDao,
									   AttachmentDisplayDao attachmentDisplayDao,
									   CustomFieldValueDisplayDao customFieldValueDisplayDao,
									   IssueDisplayDao issueDisplayDao,
									   MilestoneDisplayDao milestoneDisplayDao,
									   DSLContext dslContext,
									   TestSuiteTestPlanManagerService testSuiteTestPlanManagerService,
									   TestPlanItemSuccessRateCalculator successRateCalculator,
									   AutomatedSuiteDisplayDao automatedSuiteDisplayDao,
									   ReadUnassignedTestPlanHelper readUnassignedTestPlanHelper,
									   AvailableDatasetAppender availableDatasetAppender,
									   UserContextService userContextService,
									   PermissionEvaluationService permissionEvaluationService) {
		this.testSuiteDisplayDao = testSuiteDisplayDao;
		this.testSuiteDao = testSuiteDao;
		this.attachmentDisplayDao = attachmentDisplayDao;
		this.customFieldValueDisplayDao = customFieldValueDisplayDao;
		this.issueDisplayDao = issueDisplayDao;
		this.milestoneDisplayDao = milestoneDisplayDao;
		this.dslContext = dslContext;
		this.testSuiteTestPlanManagerService = testSuiteTestPlanManagerService;
		this.successRateCalculator = successRateCalculator;
		this.automatedSuiteDisplayDao = automatedSuiteDisplayDao;
		this.readUnassignedTestPlanHelper = readUnassignedTestPlanHelper;
		this.availableDatasetAppender = availableDatasetAppender;
		this.userContextService = userContextService;
		this.permissionEvaluationService = permissionEvaluationService;
	}

	@PreAuthorize(READ_TS_OR_ROLE_ADMIN)
	@Override
	public TestSuiteDto findTestSuiteView(Long testSuiteId) {
		TestSuiteDto testSuite = testSuiteDisplayDao.findById(testSuiteId);
		testSuite.setAttachmentList(attachmentDisplayDao.findAttachmentListById(testSuite.getAttachmentListId()));
		testSuite.getCustomFieldValues().addAll(customFieldValueDisplayDao.findCustomFieldValues(BindableEntity.TEST_SUITE, testSuiteId));
		testSuite.setTestPlanStatistics(testSuiteDao.getTestSuiteStatistics(testSuiteId));
		testSuite.setNbIssues(issueDisplayDao.countIssuesByTestSuiteId(testSuiteId));
		testSuite.setMilestones(milestoneDisplayDao.getMilestonesBySuiteId(testSuiteId));
		appendUsers(testSuite);
		testSuite.setExecutionStatusMap(testSuiteDisplayDao.getExecutionStatusMap(testSuiteId));
		testSuite.setNbAutomatedSuites(automatedSuiteDisplayDao.countAutomatedSuiteByTestSuiteId(testSuiteId));
		appendNbTestPlanItems(testSuite);
		return testSuite;
	}

	private void appendUsers(TestSuiteDto testSuite) {
		List<User> users = this.testSuiteTestPlanManagerService.findAssignableUserForTestPlan(testSuite.getId());
		testSuite.setUsers(UserView.fromEntities(users));
	}

	private void appendNbTestPlanItems(TestSuiteDto testSuite) {
		String login = this.userContextService.getUsername();
		int count;

		if(currentUserCanReadUnassigned(testSuite.getId())){
			count = this.testSuiteDisplayDao.getNbTestPlanItem(testSuite.getId(), null);
		} else {
			count = this.testSuiteDisplayDao.getNbTestPlanItem(testSuite.getId(), login);
		}
		testSuite.setNbTestPlanItems(count);
	}

	private boolean currentUserCanReadUnassigned(Long testSuiteId) {
		return this.permissionEvaluationService.hasRoleOrPermissionOnObject(Roles.ROLE_ADMIN, READ_UNASSIGNED, testSuiteId, TestSuite.SIMPLE_CASS_NAME);
	}

	@PreAuthorize(READ_TS_OR_ROLE_ADMIN)
	@Override
	public GridResponse findTestPlan(Long testSuiteId, GridRequest gridRequest) {
		TestSuiteTestPlanGrid testPlanGrid = new TestSuiteTestPlanGrid(testSuiteId);
		this.readUnassignedTestPlanHelper.appendReadUnassignedFilter(gridRequest, testSuiteId, TestSuite.SIMPLE_CASS_NAME);
		GridResponse gridResponse = testPlanGrid.getRows(gridRequest, this.dslContext);
		this.successRateCalculator.appendSuccessRate(gridResponse);
		availableDatasetAppender.appendAvailableDatasets(gridResponse);
		return gridResponse;
	}

	@PreAuthorize(READ_TS_OR_ROLE_ADMIN)
	@Override
	public Long findIterationIdByTestsuiteId(Long testSuiteId) {
		return testSuiteDisplayDao.findIterationIdByTestsuiteId(testSuiteId);
	}
}
