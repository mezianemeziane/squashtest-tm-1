/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.testcase.parameter;

import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.DatasetParamValue;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.service.testcase.ParameterFinder;

import java.util.ArrayList;
import java.util.List;

public class NewDatasetData {
	private Long id;
	private String name;
	private List<DatasetParamValuesData> paramValues = new ArrayList<>();

	public Dataset createDataSet(ParameterFinder parameterFinder) {
		Dataset dataset = new Dataset();
		dataset.setName(name);
		for (DatasetParamValuesData values : paramValues) {
			Parameter parameter = parameterFinder.findById(values.getParamId());
			new DatasetParamValue(parameter, dataset, values.getValue());
		}
		return dataset;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<DatasetParamValuesData> getParamValues() {
		return paramValues;
	}

	public void setParamValues(List<DatasetParamValuesData> paramValues) {
		this.paramValues = paramValues;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
