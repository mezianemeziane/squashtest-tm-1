/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.chart.ChartType;
import org.squashtest.tm.domain.query.ColumnRole;
import org.squashtest.tm.domain.query.DataType;
import org.squashtest.tm.domain.query.Operation;
import org.squashtest.tm.domain.query.QueryColumnPrototype;

import java.util.*;

import static org.squashtest.tm.domain.EntityType.*;

public class WorkbenchData {

	private final ChartBuildingBlocks chartBuildingBlocks;
	private final Long containerId;
	private final Long projectId;

	public WorkbenchData(Map<EntityType, Set<QueryColumnPrototype>> columnPrototypes, Long containerId, Long projectId) {
		this.chartBuildingBlocks = new ChartBuildingBlocks(columnPrototypes);
		this.containerId = containerId;
		this.projectId = projectId;
	}

	public ChartBuildingBlocks getChartBuildingBlocks() {
		return chartBuildingBlocks;
	}

	public Long getProjectId() {
		return projectId;
	}

	public Long getContainerId() {
		return containerId;
	}

	public static class ChartBuildingBlocks {

		private Map<EntityType, Set<QueryColumnPrototype>> columnPrototypes;

		private EnumSet<ChartType> chartTypes = EnumSet.allOf(ChartType.class);

		private Map<ColumnRole, EnumSet<Operation>> columnRoles = new EnumMap<>(
			ColumnRole.class);

		private Map<DataType, EnumSet<Operation>> dataTypes = new EnumMap<>(DataType.class);

		// don't use EnumMap here cuz the business want a custom order
		private Map<EntityType, String> entityTypes = new LinkedHashMap<>();

		public ChartBuildingBlocks(Map<EntityType, Set<QueryColumnPrototype>> columnPrototypes) {
			this.columnPrototypes = columnPrototypes;
			populate();
		}

		private void populate() {
			addColumnRoles();
			addDataType();
			addEntityType();
		}

		private void addColumnRoles() {
			for (ColumnRole cr : ColumnRole.values()) {
				columnRoles.put(cr, (EnumSet<Operation>) cr.getOperations());
			}
		}

		private void addDataType() {
			for (DataType dt : DataType.values()) {
				dataTypes.put(dt, (EnumSet<Operation>) dt.getOperations());
			}
		}

		private void addEntityType() {
			entityTypes.put(REQUIREMENT, "icon-chart-requirement");
			entityTypes.put(REQUIREMENT_VERSION, "icon-chart-requirement-version");
			entityTypes.put(TEST_CASE, "icon-chart-test-case");
			entityTypes.put(CAMPAIGN, "icon-chart-campaign");
			entityTypes.put(ITERATION, "icon-chart-iteration");
			entityTypes.put(ITEM_TEST_PLAN, "icon-chart-item-test-plan");
			entityTypes.put(EXECUTION, "icon-chart-execution");
		}

		public Map<EntityType, String> getEntityTypes() {
			return entityTypes;
		}

		public Map<ColumnRole, EnumSet<Operation>> getColumnRoles() {
			return columnRoles;
		}

		public Map<DataType, EnumSet<Operation>> getDataTypes() {
			return dataTypes;
		}

		public Map<EntityType, Set<QueryColumnPrototype>> getColumnPrototypes() {
			return columnPrototypes;
		}
	}

}
