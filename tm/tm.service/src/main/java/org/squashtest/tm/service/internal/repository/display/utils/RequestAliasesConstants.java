/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.utils;

public interface RequestAliasesConstants {

	String ACTION = "ACTION";
	String CUF_ID = "CUF_ID";
	String ID = "ID";
	String NATURE = "NATURE";
	String PRIORITY = "PRIORITY";
	String PROJECT_NAME = "PROJECT_NAME";
	String REQUIREMENT_CATEGORY_ID = "REQUIREMENT_CATEGORY_ID";
	String REQUIREMENT_VERSION_ID = "REQUIREMENT_VERSION_ID";
	String EXPECTED_RESULT = "EXPECTED_RESULT";
	String STATUS = "STATUS";
	String STEP_INDEX = "STEP_INDEX";
	String TC_NATURE = "TC_NATURE";
	String TC_TYPE = "TC_TYPE";
	String TEST_CASE_NATURE_ID = "TEST_CASE_NATURE_ID";
	String TEST_CASE_TYPE_ID = "TEST_CASE_TYPE_ID";
	String TYPE = "TYPE";
	String VALUE = "VALUE";
	String VERIFIED_BY = "VERIFIED_BY";
	String TC_KIND = "KIND";
	String CAMPAIGN_NAME = "CAMPAIGN_NAME";
	String ITERATION_NAME = "ITERATION_NAME";
	String TEST_SUITE_NAME = "TEST_SUITE_NAME";
	String DATASETS_NAME = "DATASETS_NAME";
	String MILESTONES_ALIAS = "MILESTONES";
	String COVERAGES_ALIAS = "COVERAGES";
	String ATTACHMENTS_ALIAS = "ATTACHMENTS";
	String CRLN_ID_ALIAS = "CUSTOM_REPORT_LIBRARY_NODE_ID";
	String DATE = "DATE";
	String USER = "USER";
	String EVENT = "EVENT";
	String OLD_VALUE = "OLD_VALUE";
	String NEW_VALUE = "NEW_VALUE";
	String SYNC_REQ_CREATION_SOURCE = "SYNC_REQ_CREATION_SOURCE";
	String SYNC_REQ_UPDATE_SOURCE = "SYNC_REQ_UPDATE_SOURCE";
	String SCRIPTED_TEST_CASE_ID = "SCRIPTED_TEST_CASE_ID";
	String KEYWORD_TEST_CASE_ID = "KEYWORD_TEST_CASE_ID";
	String BASE_URL = "BASE_URL";
	String ITEM_ID = "ITEM_ID";
	String STEP_STATUS = "STEP_STATUS";
	String SUCCESS_RATE = "SUCCESS_RATE";
	String STEP_ID = "STEP_ID";
	String TEST_SUITES = "TEST_SUITES";
	String EXECUTION_ID = "EXECUTION_ID";
	String ISSUE_LIST_ID = "ISSUE_LIST_ID";
	String ISSUE_COUNT = "ISSUE_COUNT";
	String ITEM_TEST_PLAN_ID = "ITEM_TEST_PLAN_ID";
	String BOUND_TO_BLOCKING_MILESTONE = "BOUND_TO_BLOCKING_MILESTONE";
	String AVAILABLE_DATASETS = "AVAILABLE_DATASETS";
	String CLN_ID = "CLN_ID";
	String CONFIGURED_REMOTE_FINAL_STATUS = "CONFIGURED_REMOTE_FINAL_STATUS";

}
