/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.execution;

import org.squashtest.tm.domain.execution.Execution;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

public abstract class TestPlanResume {
	protected Long testPlanItemId;
	protected Long executionId;
	protected Integer initialStepIndex;
	protected boolean hasNextTestCase = false;

	public TestPlanResume(Execution execution, boolean hasNextTestCase) {
		if (Objects.nonNull(execution)) {
			this.executionId = execution.getId();
			this.testPlanItemId = execution.getTestPlan().getId();
			this.hasNextTestCase = hasNextTestCase;
			if(execution.hasUnexecutedSteps()){
				this.initialStepIndex = execution.getSteps().indexOf(execution.findFirstUnexecutedStep());
			}
		}
	}

	public Long getTestPlanItemId() {
		return testPlanItemId;
	}

	public Long getExecutionId() {
		return executionId;
	}

	public boolean isHasNextTestCase() {
		return hasNextTestCase;
	}

	public Integer getInitialStepIndex() {
		return initialStepIndex;
	}

	public static class IterationTestPlanResume extends TestPlanResume {
		private final Long iterationId;

		public IterationTestPlanResume(Long iterationId, Execution execution, boolean hasNextTestCase) {
			super(execution, hasNextTestCase);
			this.iterationId = requireNonNull(iterationId);
		}

		public Long getIterationId() {
			return iterationId;
		}
	}

	public static class TestSuiteTestPlanResume extends TestPlanResume {
		private final Long testSuiteId;

		public TestSuiteTestPlanResume(Long testSuiteId, Execution execution, boolean hasNextTestCase) {
			super(execution, hasNextTestCase);
			this.testSuiteId = requireNonNull(testSuiteId);
		}

		public Long getTestSuiteId() {
			return testSuiteId;
		}
	}
}


