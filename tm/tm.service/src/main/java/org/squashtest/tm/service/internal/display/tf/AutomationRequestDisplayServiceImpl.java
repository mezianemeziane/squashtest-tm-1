/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.tf;

import org.jooq.DSLContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.query.Operation;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus;
import org.squashtest.tm.service.display.tf.AutomationRequestDisplayService;
import org.squashtest.tm.service.display.user.UserDisplayService;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.tf.AutomationRequestGrid;
import org.squashtest.tm.service.internal.dto.DetailedUserDto;
import org.squashtest.tm.service.project.ProjectFinder;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class AutomationRequestDisplayServiceImpl implements AutomationRequestDisplayService {
	private final UserDisplayService userDisplayService;
	private final DSLContext dslContext;
	private final ProjectFinder projectFinder;


	public AutomationRequestDisplayServiceImpl(UserDisplayService userDisplayService,
											   DSLContext dslContext,
											   ProjectFinder projectFinder) {
		this.userDisplayService = userDisplayService;
		this.dslContext = dslContext;
		this.projectFinder = projectFinder;
	}

	@Override
	public GridResponse findAutomationRequestAssignedToCurrentUser(GridRequest request) {
		DetailedUserDto currentUser = userDisplayService.findCurrentUser();
		GridFilterValue filterValue = new GridFilterValue();
		filterValue.setId("assignedTo");
		filterValue.setValues(Collections.singletonList(currentUser.getUserId().toString()));
		filterValue.setOperation(Operation.IN.name());
		request.getFilterValues().add(filterValue);
		return fetchData(request, false);
	}

	@Override
	public GridResponse findAutomationRequestToTreat(GridRequest request) {
		GridFilterValue requestStatusFilterValue = new GridFilterValue();
		requestStatusFilterValue.setId("requestStatus");
		requestStatusFilterValue.setValues(Arrays.asList(AutomationRequestStatus.AUTOMATION_IN_PROGRESS.name(),
			AutomationRequestStatus.TRANSMITTED.name()));
		requestStatusFilterValue.setOperation(Operation.IN.name());
		request.getFilterValues().add(requestStatusFilterValue);
		return fetchData(request, true);
	}

	@Override
	public GridResponse findAutomationRequests(GridRequest request) {
		return fetchData(request, false);
	}

	private GridResponse fetchData(GridRequest request, boolean withAssigneeNull) {
		List<Long> readableProjectIds = projectFinder.findAllReadableIdsForAutomationWriter();
		AutomationRequestGrid grid = new AutomationRequestGrid(readableProjectIds, withAssigneeNull);
		return grid.getRows(request, dslContext);
	}
}
