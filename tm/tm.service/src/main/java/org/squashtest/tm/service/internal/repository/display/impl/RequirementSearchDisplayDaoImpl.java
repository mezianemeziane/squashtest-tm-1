/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import com.google.common.base.CaseFormat;
import com.google.common.base.Converter;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.jooq.domain.tables.Requirement;
import org.squashtest.tm.jooq.domain.tables.RequirementVersion;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.repository.display.RequirementSearchDisplayDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.jooq.impl.DSL.*;
import static org.squashtest.tm.jooq.domain.Tables.*;
import static org.squashtest.tm.service.internal.repository.display.impl.DisplayDaoUtils.getMilestoneLocked;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.*;

@Repository
public class RequirementSearchDisplayDaoImpl implements RequirementSearchDisplayDao {

	private static final String ID_ALIAS = "ID";
	private static final String REQ_MILESTONE_LOCKED_ALIAS = "REQ_MILESTONE_LOCKED";
	private static final String REQ_MILESTONE_LOCKED_TABLE = "REQ_MILESTONE_LOCKED_TABLE";

	Table<?> milestoneLockedTable = getMilestoneLocked().asTable(REQ_MILESTONE_LOCKED_TABLE);
	Requirement innerRequirement = REQUIREMENT.as("INNER_REQUIREMENT");
	RequirementVersion innerVersions = REQUIREMENT_VERSION.as("INNER_VERSIONS");

	Field<Long> innerRequirementId = innerRequirement.RLN_ID.as("INNER_REQUIREMENT_ID");
	String versionCountAlias = "VERSIONS_COUNT";
	Field<Integer> versionsCount= countDistinct(innerVersions.RES_ID).as(versionCountAlias);

	SelectHavingStep<Record2<Long, Integer>> innerVersionSelect =
		DSL.select(innerRequirementId, versionsCount)
		.from(innerRequirement)
			.innerJoin(innerVersions).on(innerRequirement.RLN_ID.eq(innerVersions.REQUIREMENT_ID))
		.groupBy(innerRequirementId);

	private DSLContext jooq;

	public RequirementSearchDisplayDaoImpl(DSLContext jooq) {
		this.jooq = jooq;
	}

	@Override
	public GridResponse getRows(List<Long> requirementVersionIds) {
		GridResponse gridResponse = new GridResponse();

		jooq.select(
			REQUIREMENT_VERSION.RES_ID.as(ID_ALIAS), REQUIREMENT_VERSION.REFERENCE, REQUIREMENT_VERSION.REQUIREMENT_STATUS.as(STATUS),
				REQUIREMENT_VERSION.CRITICALITY, REQUIREMENT_VERSION.CATEGORY, REQUIREMENT_VERSION.VERSION_NUMBER, REQUIREMENT_VERSION.REQUIREMENT_ID,
			RESOURCE.NAME, RESOURCE.CREATED_BY, RESOURCE.LAST_MODIFIED_BY,
			REQUIREMENT_LIBRARY_NODE.PROJECT_ID,
			PROJECT.NAME.as(PROJECT_NAME),
			countDistinct(MILESTONE_REQ_VERSION.MILESTONE_ID).as(MILESTONES_ALIAS),
			countDistinct(MILESTONE.MILESTONE_ID).as(REQ_MILESTONE_LOCKED_ALIAS),
			countDistinct(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID).as(COVERAGES_ALIAS),
			countDistinct(ATTACHMENT.ATTACHMENT_ID).as(ATTACHMENTS_ALIAS),
			min(versionsCount).as(versionCountAlias)
		)
			.from(REQUIREMENT_VERSION)
			.innerJoin(RESOURCE).on(RESOURCE.RES_ID.eq(REQUIREMENT_VERSION.RES_ID))
			.innerJoin(REQUIREMENT).on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT.RLN_ID))
			.innerJoin(REQUIREMENT_LIBRARY_NODE).on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
			.innerJoin(PROJECT).on(PROJECT.PROJECT_ID.eq(REQUIREMENT_LIBRARY_NODE.PROJECT_ID))
			.innerJoin(ATTACHMENT_LIST).on(ATTACHMENT_LIST.ATTACHMENT_LIST_ID.eq(RESOURCE.ATTACHMENT_LIST_ID))
			.leftJoin(MILESTONE_REQ_VERSION).on(REQUIREMENT_VERSION.RES_ID.eq(MILESTONE_REQ_VERSION.REQ_VERSION_ID))
			.leftJoin(milestoneLockedTable.as(MILESTONE)).on(MILESTONE.MILESTONE_ID.eq(MILESTONE_REQ_VERSION.MILESTONE_ID))
			.leftJoin(REQUIREMENT_VERSION_COVERAGE).on(REQUIREMENT_VERSION.RES_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID))
			.leftJoin(innerVersionSelect).on(innerRequirementId.eq(REQUIREMENT.RLN_ID))
			.leftJoin(ATTACHMENT).on(ATTACHMENT.ATTACHMENT_LIST_ID.eq(ATTACHMENT_LIST.ATTACHMENT_LIST_ID))
			.where(REQUIREMENT_VERSION.RES_ID.in(requirementVersionIds))
			.groupBy(
				REQUIREMENT_VERSION.RES_ID,
				RESOURCE.RES_ID,
				REQUIREMENT.RLN_ID,
				REQUIREMENT_LIBRARY_NODE.RLN_ID,
				PROJECT.PROJECT_ID,
				ATTACHMENT_LIST.ATTACHMENT_LIST_ID,
				REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID,
				innerRequirementId
			).stream().forEach(record -> {
			DataRow dataRow = convertRecordIntoDataRow(record);
			gridResponse.addDataRow(dataRow);
			});
		return gridResponse;
	}

	private DataRow convertRecordIntoDataRow(Record record) {
		DataRow dataRow = new DataRow();
		dataRow.setId(record.get(ID_ALIAS).toString());
		dataRow.setProjectId(record.get(REQUIREMENT_LIBRARY_NODE.PROJECT_ID));
		Map<String, Object> rawData = record.intoMap();
		Map<String, Object> data = new HashMap<>();

		// Using 'Collectors.toMap' won't work for entries with null values
		for (Map.Entry<String, Object> entry : rawData.entrySet()) {
			data.put(convertField(entry.getKey()), entry.getValue());
		}
		dataRow.setData(data);
		return dataRow;
	}

	private String convertField(String fieldName) {
		Converter<String, String> converter = CaseFormat.UPPER_UNDERSCORE.converterTo(CaseFormat.LOWER_CAMEL);
		return converter.convert(fieldName);
	}
}
