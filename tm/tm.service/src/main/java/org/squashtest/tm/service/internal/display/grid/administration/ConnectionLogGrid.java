/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.administration;

import org.jooq.Field;
import org.jooq.SortField;
import org.jooq.Table;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

import java.util.Arrays;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.CONNECTION_ATTEMPT_LOG;

public class ConnectionLogGrid extends AbstractGrid {

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(CONNECTION_ATTEMPT_LOG.ATTEMPT_ID),
			new GridColumn(CONNECTION_ATTEMPT_LOG.LOGIN),
			new GridColumn(CONNECTION_ATTEMPT_LOG.CONNECTION_DATE),
			new GridColumn(CONNECTION_ATTEMPT_LOG.SUCCESS)
		);
	}

	@Override
	protected Table<?> getTable() {
		return CONNECTION_ATTEMPT_LOG;
	}

	@Override
	protected Field<?> getIdentifier() { return CONNECTION_ATTEMPT_LOG.ATTEMPT_ID; }

	@Override
	protected Field<?> getProjectIdentifier() {
		return null;
	}

	@Override
	protected SortField<?> getDefaultOrder() {
		return CONNECTION_ATTEMPT_LOG.CONNECTION_DATE.desc();
	}
}
