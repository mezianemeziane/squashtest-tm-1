/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl.collectors;

import com.google.common.base.Strings;
import org.jooq.DSLContext;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TreeNodeCollector;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.count;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_CAMPAIGN;

@Component
public class IterationCollector extends AbstractTreeNodeCollector implements TreeNodeCollector {

	public IterationCollector(DSLContext dsl, CustomFieldValueDisplayDao customFieldValueDisplayDao, ActiveMilestoneHolder activeMilestoneHolder, MilestoneDisplayDao milestoneDisplayDao) {
		super(dsl, customFieldValueDisplayDao, activeMilestoneHolder, milestoneDisplayDao);
	}

	@Override
	protected Map<Long, DataRow> doCollect(List<Long> ids) {
		Map<Long, DataRow> iterations = collectIterations(ids);
		appendMilestonesInheritedFromCampaign(iterations);
		return iterations;
	}

	private Map<Long, DataRow> collectIterations(List<Long> ids) {
		return dsl.select(
			//@formatter:off
			ITERATION.ITERATION_ID, ITERATION.NAME.as(NAME_ALIAS), ITERATION.REFERENCE,
			CAMPAIGN_LIBRARY_NODE.PROJECT_ID.as(PROJECT_ID_ALIAS), CAMPAIGN_LIBRARY_NODE.CLN_ID.as(CAMPAIGN_ID_ALIAS),
			count(ITERATION_TEST_SUITE.TEST_SUITE_ID).as(CHILD_COUNT_ALIAS))
			.from(ITERATION)
			.leftJoin(ITERATION_TEST_SUITE).on(ITERATION.ITERATION_ID.eq(ITERATION_TEST_SUITE.ITERATION_ID))
			.leftJoin(CAMPAIGN_ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
			.leftJoin(CAMPAIGN_LIBRARY_NODE).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
			.where(ITERATION.ITERATION_ID.in(ids))
			.groupBy(
				ITERATION.ITERATION_ID,
				CAMPAIGN_LIBRARY_NODE.CLN_ID)
			//@formatter:on
			.fetch().stream()
			.collect(Collectors.toMap(
				tuple -> tuple.get(ITERATION.ITERATION_ID),
				tuple -> {
					DataRow dataRow = new DataRow();
					dataRow.setId(new NodeReference(NodeType.ITERATION, tuple.get(ITERATION.ITERATION_ID)).toNodeId());
					dataRow.setProjectId(tuple.get(PROJECT_ID_ALIAS, Long.class));
					dataRow.setState(tuple.get(CHILD_COUNT_ALIAS, Integer.class) > 0 ? DataRow.State.closed : DataRow.State.leaf);
					dataRow.setData(tuple.intoMap());
					if (!Strings.isNullOrEmpty(tuple.get(ITERATION.REFERENCE, String.class))) {
						dataRow.getData().replace(NAME_ALIAS, tuple.get(ITERATION.REFERENCE) + " - " + tuple.get(ITERATION.NAME));
					}
					return dataRow;
				}));
	}


	@Override
	public NodeType getHandledEntityType() {
		return NodeType.ITERATION;
	}
}
