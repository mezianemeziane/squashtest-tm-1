/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.api.repository;

import org.hibernate.Session;
import org.jooq.ExecuteContext;
import org.jooq.ExecuteListener;
import org.jooq.impl.DefaultExecuteListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Objects;

/**
 * This {@link ExecuteListener} force hibernate {@link Session} flush before executing a jooq request, preventing stale results if a dirty {@link Session} is not yet flushed.
 * As we use jooq mainly for query side of the application, it shouldn't be used too often.
 * However there is some case of jooq fetching after an entity is saved so we can have some bad surprise without ensuring {@link Session} is clean.
 * An alternative could be to use jooq only as sql generator and give generated sql to hibernate. It would be heavy and error prone to do that every time we use jooq.
 * It would also probably prevent us to use the nice mappers provided by jooq (intoMap, fetchInto...)
 *
 * For performance aspects i suspect that hibernate is not able to optimize flushing when doing native query, as a native query can virtually request anything in database,
 * by using views, calling stored procedure...
 *
 * @author jthebault
 * @since SquashTM 2.0
 */
public class JooqSessionGuard extends DefaultExecuteListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(JooqSessionGuard.class);

	private final EntityManagerFactory entityManagerFactory;

	public JooqSessionGuard(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	@Override
	public void executeStart(ExecuteContext ctx) {
		LOGGER.trace("Checking Hibernate Session before executing a request through jooq execution pipeline");
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
		if (Objects.nonNull(entityManager)) {
			Session currentSession = entityManager.unwrap(Session.class);
			if (currentSession.isOpen() && currentSession.isDirty()) {
				LOGGER.debug("Hibernate Session is open and dirty. Will force a flush to prevent stale results of jooq request");
				currentSession.flush();
			}
		}
	}
}
