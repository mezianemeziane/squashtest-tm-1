/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display;

import org.squashtest.tm.service.internal.display.dto.requirement.DetailedStepViewRequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionDto;

import java.util.List;
import java.util.Map;

public interface RequirementVersionDisplayDao {

	/**
	 * Find the currentVersionId for each requirement inside given ids
	 * @param requirementIds the list of requirement ids
	 * @return a map like requirementId : currentVersionId
	 */
	Map<Long, Long> findCurrentRequirementVersions(List<Long> requirementIds);

	/**
	 * Find the currentVersionId for the given requirement
	 * @param requirementId the requirement id
	 * @return the current version id
	 */
	Long findCurrentRequirementVersions(Long requirementId);

	/**
	 * Projection dedicated to level 2 test step view
	 * @param requirementVersionId the req version id
	 * @return the projection
	 */
	DetailedStepViewRequirementVersionDto findById(Long requirementVersionId);

	RequirementVersionDto findRequirementVersion(Long currentVersionId);
}
