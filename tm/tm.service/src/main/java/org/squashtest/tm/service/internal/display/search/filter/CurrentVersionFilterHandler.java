/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search.filter;

import com.google.common.collect.Sets;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.hibernate.HibernateQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.jpql.ExtendedHibernateQuery;
import org.squashtest.tm.domain.requirement.QRequirement;
import org.squashtest.tm.domain.requirement.QRequirementVersion;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

import java.util.List;
import java.util.Set;

import static org.squashtest.tm.domain.requirement.QRequirement.requirement;
import static org.squashtest.tm.domain.requirement.QRequirementVersion.requirementVersion;
import static org.squashtest.tm.service.internal.display.search.filter.CurrentVersionFilterHandler.CurrentVersionFilterValue.CURRENT;

@Component
public class CurrentVersionFilterHandler implements FilterHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(CurrentVersionFilterHandler.class);

	private final Set<String> handledPrototypes = Sets.newHashSet("REQUIREMENT_VERSION_CURRENT_VERSION");

	@Override
	public boolean canHandleFilter(GridFilterValue filter) {
		return this.handledPrototypes.contains(filter.getColumnPrototype());
	}

	@Override
	public void handleFilter(ExtendedHibernateQuery<?> query, GridFilterValue filter) {
		CurrentVersionFilterValue currentVersionFilterValue = extractFilterValue(filter);
		if (currentVersionFilterValue.requireAdditionalFilterClause()) {
			addFilterClause(query, currentVersionFilterValue);
		}
	}

	private void addFilterClause(ExtendedHibernateQuery<?> query, CurrentVersionFilterValue currentVersionFilterValue) {
		QRequirementVersion outerVersion = requirementVersion;
		QRequirement outerRequirement = requirement;
		QRequirement parent = new QRequirement("parent");
		QRequirementVersion initVersion = new QRequirementVersion("initVersion");
		QRequirementVersion maxVersion = new QRequirementVersion("maxVersion");

		HibernateQuery<?> subquery = new ExtendedHibernateQuery<>()
			.select(Expressions.ONE)
			.from(initVersion)
			.where(initVersion.id.eq(outerVersion.id).and(
				initVersion.versionNumber.in(new ExtendedHibernateQuery<>().select(maxVersion.versionNumber.max())
					.from(maxVersion)
					.join(maxVersion.requirement, parent)
					.where(getWhereClause(maxVersion, currentVersionFilterValue).and(parent.id.eq(outerRequirement.id)))
					.groupBy(parent.id)
				)));

		query.where(subquery.exists());
	}

	private BooleanExpression getWhereClause(QRequirementVersion maxVersion, CurrentVersionFilterValue currentVersionFilterValue) {
		if(currentVersionFilterValue.equals(CURRENT)){
			// using a dummy 1=1 where clause to preserve the query structure
			return Expressions.asNumber(1).eq(1);
		}
		return maxVersion.status.ne(RequirementStatus.OBSOLETE);
	}

	private CurrentVersionFilterValue extractFilterValue(GridFilterValue filter) {
		List<String> values = filter.getValues();
		if (values.size() != 1) {
			throw new IllegalArgumentException("Invalid value for filter REQUIREMENT_VERSION_CURRENT_VERSION");
		}

		CurrentVersionFilterValue currentVersionFilterValue = CurrentVersionFilterValue.valueOf(values.get(0));
		return currentVersionFilterValue;
	}

	public enum CurrentVersionFilterValue {
		ALL, LAST_NON_OBSOLETE, CURRENT;

		boolean requireAdditionalFilterClause(){
			return !this.equals(ALL);
		}
	}
}
