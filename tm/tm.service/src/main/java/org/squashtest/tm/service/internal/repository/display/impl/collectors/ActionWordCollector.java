/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl.collectors;

import org.jooq.DSLContext;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.actionword.ActionWordTreeDefinition;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TreeNodeCollector;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.count;
import static org.squashtest.tm.jooq.domain.Tables.*;

@Component
public class ActionWordCollector extends AbstractTreeNodeCollector implements TreeNodeCollector {

	public ActionWordCollector(DSLContext dsl, CustomFieldValueDisplayDao customFieldValueDisplayDao, ActiveMilestoneHolder activeMilestoneHolder, MilestoneDisplayDao milestoneDisplayDao) {
		super(dsl, customFieldValueDisplayDao, activeMilestoneHolder, milestoneDisplayDao);
	}

	@Override
	protected Map<Long, DataRow> doCollect(List<Long> ids) {
		// @formatter:off
		Map<Long, DataRow> actionWords = dsl.select(
			ACTION_WORD_LIBRARY_NODE.AWLN_ID, ACTION_WORD_LIBRARY_NODE.NAME, ACTION_WORD.PROJECT_ID.as(PROJECT_ID_ALIAS))
			.from(ACTION_WORD_LIBRARY_NODE)
			.innerJoin(ACTION_WORD)
				.on(ACTION_WORD.ACTION_WORD_ID.eq(ACTION_WORD_LIBRARY_NODE.ENTITY_ID))
				.and(ACTION_WORD_LIBRARY_NODE.ENTITY_TYPE.eq(ActionWordTreeDefinition.ACTION_WORD.name()))
			.where(ACTION_WORD_LIBRARY_NODE.AWLN_ID.in(ids))
			// @formatter:on
			.fetch().stream()
			.collect(Collectors.toMap(
				tuple -> tuple.get(ACTION_WORD_LIBRARY_NODE.AWLN_ID),
				tuple -> {
					DataRow dataRow = new DataRow();
					dataRow.setId(new NodeReference(NodeType.ACTION_WORD, tuple.get(ACTION_WORD_LIBRARY_NODE.AWLN_ID)).toNodeId());
					dataRow.setProjectId(tuple.get(PROJECT_ID_ALIAS, Long.class));
					dataRow.setData(tuple.intoMap());
					return dataRow;
				}
			));
		appendMilestonesByProject(actionWords);
		return actionWords;
	}

	@Override
	public NodeType getHandledEntityType() {
		return NodeType.ACTION_WORD;
	}
}
