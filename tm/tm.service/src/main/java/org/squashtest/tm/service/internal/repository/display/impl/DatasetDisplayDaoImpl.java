/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.testcase.DataSetDto;
import org.squashtest.tm.service.internal.repository.display.DatasetDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;

@Repository
public class DatasetDisplayDaoImpl implements DatasetDisplayDao {

	@Inject
	private DSLContext dsl;

	@Override
	public List<DataSetDto> findAllByTestCaseId(Long testCaseId) {
		return dsl.select(DATASET.DATASET_ID.as(RequestAliasesConstants.ID), DATASET.NAME)
			.from(DATASET)
			.where(DATASET.TEST_CASE_ID.eq(testCaseId))
			.orderBy(DATASET.NAME)
			.fetchInto(DataSetDto.class);
	}

	@Override
	public Map<Long, List<DataSetDto>> findAllByTestCaseIds(List<Long> testCaseIds) {
		return dsl.select(
				DATASET.TEST_CASE_ID,
				DATASET.DATASET_ID,
				DATASET.NAME)
				.from(DATASET)
				.where(DATASET.TEST_CASE_ID.in(testCaseIds))
				.groupBy(DATASET.TEST_CASE_ID, DATASET.DATASET_ID)
				.orderBy(DATASET.NAME)
				.fetch()
				.stream()
				.collect(groupingBy(tuple -> tuple.get(DATASET.TEST_CASE_ID),
						mapping(tuple -> {
							DataSetDto dto = new DataSetDto();
							dto.setId(tuple.get(DATASET.DATASET_ID));
							dto.setName(tuple.get(DATASET.NAME));
							return dto;
						}, toList())
				));
	}
}
