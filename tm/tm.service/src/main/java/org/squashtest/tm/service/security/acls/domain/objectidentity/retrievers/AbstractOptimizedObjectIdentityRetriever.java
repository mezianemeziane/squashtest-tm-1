/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.security.acls.domain.objectidentity.retrievers;

import org.jooq.DSLContext;
import org.jooq.Record1;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.model.ObjectIdentity;
import org.squashtest.tm.service.security.acls.domain.DatabaseBackedObjectIdentityGeneratorStrategy;

import java.io.Serializable;
import java.util.Optional;

public abstract class AbstractOptimizedObjectIdentityRetriever implements OptimizedObjectIdentityRetriever {

	protected final DSLContext jooq;

	public AbstractOptimizedObjectIdentityRetriever(DSLContext jooq) {
		this.jooq = jooq;
	}

	@Override
	public ObjectIdentity createObjectIdentity(Serializable id) {
		Optional<Record1<Long>> libraryId = this.getLibraryIdRecord((Long) id);
		if(libraryId.isPresent()){
			return new ObjectIdentityImpl(this.getLibraryClassName(),libraryId.get().value1());
		} else {
			return new DatabaseBackedObjectIdentityGeneratorStrategy.UnknownObjectIdentity(this.getLibraryClassName());
		}
	}

	abstract String getLibraryClassName();

	abstract Optional<Record1<Long>> getLibraryIdRecord(Long entityId);

}
