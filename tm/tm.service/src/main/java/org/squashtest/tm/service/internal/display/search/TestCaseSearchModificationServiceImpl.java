/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseStatus;
import org.squashtest.tm.service.display.search.TestCaseSearchModificationService;
import org.squashtest.tm.service.testcase.TestCaseModificationService;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@Transactional
public class TestCaseSearchModificationServiceImpl implements TestCaseSearchModificationService {

	private EntityManager entityManager;

	private TestCaseModificationService testCaseModificationService;

	public TestCaseSearchModificationServiceImpl(EntityManager entityManager, TestCaseModificationService testCaseModificationService) {
		this.entityManager = entityManager;
		this.testCaseModificationService = testCaseModificationService;
	}

	@Override
	public void massUpdate(TestCaseResearchPatch patch) {
		List<List<Long>> partitionedIds = Lists.partition(patch.getTestCaseIds(), 10);
		for (List<Long> ids : partitionedIds) {
			updateProperties(ids, patch);
			entityManager.flush();
			entityManager.clear();
		}
	}

	private void updateProperties(List<Long> testCaseIds, TestCaseResearchPatch patch) {
		for (Long id : testCaseIds) {
			if (patch.getType() != null) {
				testCaseModificationService.changeType(id, patch.getType());
			}
			if (patch.getNature() != null) {
				testCaseModificationService.changeNature(id, patch.getNature());
			}

			if (!Strings.isNullOrEmpty(patch.getAutomatable())) {
				testCaseModificationService.changeAutomatable(TestCaseAutomatable.valueOf(patch.getAutomatable()), id);
			}

			if (!Strings.isNullOrEmpty(patch.getStatus())) {
				testCaseModificationService.changeStatus(id, TestCaseStatus.valueOf(patch.getStatus()));
			}

			if (!Strings.isNullOrEmpty(patch.getImportance())) {
				testCaseModificationService.changeImportanceAuto(id, patch.isImportanceAuto());
				if (!patch.isImportanceAuto()) {
					testCaseModificationService.changeImportance(id, TestCaseImportance.valueOf(patch.getImportance()));
				}

			}
		}
	}
}
