/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.filters;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

import java.util.List;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class InConditionBuilder implements GridFilterConditionBuilder {

	private final Field<?> field;
	private final GridFilterValue gridFilterValue;
	// Constant used to materialize that you want to filter on null values. Its preferable to use a constant that to let nulls be valid identifier...
	// The uuid concatenated is here to guarantee uniqueness as this string can be mixed with "real" ids in filter list.
	// It should be copied as it in front end where you need it...
	public static final String SQUASH_TM_NULL_VALUE_IN_FILTERS = "SQUASH_TM_2_0_FRONT_END_NULL_VALUE_IN_LIST_FILTERS_TOKEN-885a4cf1-063f-4062-9a15-9b6fda0308e0";

	InConditionBuilder(Field<?> field, GridFilterValue gridFilterValue) {
		requireNonNull(field);
		requireNonNull(gridFilterValue);
		this.field = field;
		this.gridFilterValue = gridFilterValue;
	}

	@Override
	public Condition build() {
		List<String> values = gridFilterValue.getValues();

		if (values.isEmpty()) {
			return DSL.val(1).eq(1);
		}

		boolean hasEmptyFilter = values.stream().anyMatch(SQUASH_TM_NULL_VALUE_IN_FILTERS::equals);
		boolean hasValue = values.stream().anyMatch(v -> !Objects.equals(SQUASH_TM_NULL_VALUE_IN_FILTERS, v));

		Condition condition;

		if(hasEmptyFilter && hasValue){
			condition = field.in(values).or(field.isNull());
		} else if(!hasEmptyFilter && hasValue){
			condition = field.in(values);
		} else {
			condition = field.isNull();
		}

		return condition;
	}
}
