/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.Table;
import org.jooq.TableField;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.actionword.ActionWordTreeDefinition;
import org.squashtest.tm.domain.customreport.CustomReportTreeDefinition;

import static org.squashtest.tm.jooq.domain.Tables.*;

/**
 * Declarative enum of ancestor relationship existing in tm modern trees. By modern trees i mean trees data structure with a signe database table to represent the whole hierarchy
 * Ex: CustomReport Tree. All nodes, even libraries are stored in CUSTOM_REPORT_LIBRARY_NODE table. And all relations are available in CRLN_RELATIONSHIP table
 * and are flattened in only one table : CRLN_RELATIONSHIP_CLOSURE.
 * With this unique data structure we are able to fetch the whole tree in one request, so there is only one definition {@link SingleHierarchyAncestorLookupDefinition} for a complete tree
 * Used to fetch ancestors from a node to force open selected nodes in trees.
 */
public enum SingleHierarchyAncestorLookupDefinition {

	CUSTOM_REPORT_TREE_ANCESTORS(NodeType.CUSTOM_REPORT_LIBRARY, CRLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID, CRLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID, CUSTOM_REPORT_LIBRARY_NODE.ENTITY_TYPE, CRLN_RELATIONSHIP_CLOSURE.DEPTH) {
		@Override
		public NodeType discriminateAncestor(String ancestorType) {
			CustomReportTreeDefinition definition = CustomReportTreeDefinition.valueOf(ancestorType);
			return definition.asNodeType();
		}
	},
	ACTION_WORD_TREE_ANCESTORS(NodeType.ACTION_WORD_LIBRARY, AWLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID, AWLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID, ACTION_WORD_LIBRARY_NODE.ENTITY_TYPE, AWLN_RELATIONSHIP_CLOSURE.DEPTH) {
		@Override
		public NodeType discriminateAncestor(String ancestorType) {
			ActionWordTreeDefinition definition = ActionWordTreeDefinition.valueOf(ancestorType);
			return definition.asNodeType();
		}
	};

	SingleHierarchyAncestorLookupDefinition(NodeType libraryNodeType, TableField<?, Long> ancestorField, TableField<?, Long> descendantField, TableField<?, String> discriminatorField, TableField<?, Short> depthField) {
		this.libraryNodeType = libraryNodeType;
		this.ancestorField = ancestorField;
		this.descendantField = descendantField;
		this.discriminatorField = discriminatorField;
		this.depthField = depthField;
	}

	// To make difference between all moderns trees we use the library type of the given type.
	private final NodeType libraryNodeType;
	private final TableField<?, Long> ancestorField;
	private final TableField<?, Long> descendantField;
	private final TableField<?, String> discriminatorField;
	private final TableField<?, Short> depthField;

	public NodeType getLibraryNodeType() {
		return libraryNodeType;
	}

	public TableField<?, Long> getAncestorField() {
		return ancestorField;
	}

	public Table<?> getHierarchyTable() {
		return ancestorField.getTable();
	}

	public Table<?> getNodeTable() {
		return discriminatorField.getTable();
	}

	public TableField<?, Long> getNodePrimaryKey() {
		TableField<?, ?> tableField = discriminatorField.getTable().getPrimaryKey().getFields().get(0);
		boolean isLongPrimaryKey = Long.class.equals(tableField.getDataType().getType());
		if(!isLongPrimaryKey){
			throw new IllegalArgumentException("The primary key of table " + tableField.getTable() + " is not a Long Type ");
		}
		// cast is safe we just checked the type above
		return (TableField<?, Long>) tableField;
	}

	public TableField<?, Short> getDepthField() {
		return depthField;
	}

	public TableField<?, Long> getDescendantField() {
		return descendantField;
	}

	public TableField<?, String> getDiscriminatorField() {
		return discriminatorField;
	}

	/**
	 * Override to allow type discrimination across all ancestors.
	 * Ex: In CUSTOM_REPORT_LIBRARY_NODE table, we fetch result set with ENTITY_TYPE column.
	 * This function will return the correct {@link NodeType} for a given ENTITY_TYPE value.
	 *
	 * @param ancestorType The string value representing the node type in database
	 * @return the correct {@link NodeType}
	 */
	abstract public NodeType discriminateAncestor(String ancestorType);
}
