/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.campaign;

import org.springframework.stereotype.Component;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.filters.GridFilterOperation;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.UserContextService;

import java.util.Collections;

@Component
class ReadUnassignedTestPlanHelper {

	private static final String LOGIN_FIELD = "login";
	static final String READ_UNASSIGNED = "READ_UNASSIGNED";
	private final PermissionEvaluationService permissionEvaluationService;
	private final UserContextService userContextService;

	public ReadUnassignedTestPlanHelper(PermissionEvaluationService permissionEvaluationService, UserContextService userContextService) {
		this.permissionEvaluationService = permissionEvaluationService;
		this.userContextService = userContextService;
	}

	void appendReadUnassignedFilter(GridRequest gridRequest, Long testPlanOwnerId, String testPlanOwnerClassName) {
		boolean readUnassigned = currentUserCanReadUnassigned(testPlanOwnerId, testPlanOwnerClassName);
		if (!readUnassigned) {
			GridFilterValue filterValue = craftOnlyCurrentUserAssignedFilter();
			gridRequest.getFilterValues().add(filterValue);
		}
	}

	private boolean currentUserCanReadUnassigned(Long testPlanOwnerId, String testPlanOwnerClassName) {
		return this.permissionEvaluationService.hasRoleOrPermissionOnObject(Roles.ROLE_ADMIN, READ_UNASSIGNED, testPlanOwnerId, testPlanOwnerClassName);
	}

	private GridFilterValue craftOnlyCurrentUserAssignedFilter() {
		GridFilterValue filterValue = new GridFilterValue();
		filterValue.setValues(Collections.singletonList(this.userContextService.getUsername()));
		filterValue.setId(LOGIN_FIELD);
		filterValue.setOperation(GridFilterOperation.EQUALS.name());
		return filterValue;
	}
}
