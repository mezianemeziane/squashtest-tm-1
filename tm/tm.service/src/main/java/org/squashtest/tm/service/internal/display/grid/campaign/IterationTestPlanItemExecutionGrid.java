/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.SelectHavingStep;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.columns.LevelEnumColumn;

import java.util.Arrays;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_ISSUES_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_CAMPAIGN;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.BOUND_TO_BLOCKING_MILESTONE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ISSUE_COUNT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.STEP_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.STEP_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SUCCESS_RATE;

/**
 * @author qtran - created on 06/01/2021
 */
public class IterationTestPlanItemExecutionGrid extends AbstractGrid {
	private final Long iterationId;
	private final Long itemId;

	public IterationTestPlanItemExecutionGrid(Long iterationId, Long itemId) {
		this.iterationId = iterationId;
		this.itemId = itemId;
	}

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID),
			new GridColumn(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER.as("EXECUTION_ORDER")),
			new GridColumn(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.as("PROJECT_ID")),
			new GridColumn(EXECUTION.EXECUTION_MODE),
			new GridColumn(EXECUTION.REFERENCE.as("EXECUTION_REFERENCE")),
			new GridColumn(EXECUTION.NAME.as("EXECUTION_NAME")),
			new LevelEnumColumn(TestCaseImportance.class, EXECUTION.IMPORTANCE),
			new GridColumn(EXECUTION.DATASET_LABEL.as("DATASET_NAME")),
			new ExecutionStatusColumn(EXECUTION.EXECUTION_STATUS),
			new GridColumn(EXECUTION.LAST_EXECUTED_BY.as("USER")),
			new GridColumn(EXECUTION.LAST_EXECUTED_ON),
			new GridColumn(countIssue().as(ISSUE_COUNT)),
			new GridColumn(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.as("ITEM_TEST_PLAN_ID")),
			new GridColumn(ITERATION.ITERATION_ID.as("ITERATION_ID")),
			new GridColumn(DSL.field(countBlockingMilestoneBindings().greaterThan(0)).as(BOUND_TO_BLOCKING_MILESTONE)));
	}

	/**
	 * Count issues in Issue list linked to current execution
	 * @return number of issues
	 */
	private Field<Integer> countIssue() {
		return DSL.selectCount()
			.from(EXECUTION_ISSUES_CLOSURE)
			.where(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID)).asField(ISSUE_COUNT);
	}

	@Override
	protected Table<?> getTable() {
		return EXECUTION
			.innerJoin(ITEM_TEST_PLAN_EXECUTION).on(EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
			.innerJoin(ITERATION_TEST_PLAN_ITEM).on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
			.leftJoin(DATASET).on(ITERATION_TEST_PLAN_ITEM.DATASET_ID.eq(DATASET.DATASET_ID))
			.innerJoin(ITEM_TEST_PLAN_LIST).on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
			.innerJoin(ITERATION).on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN_ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN_LIBRARY_NODE).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID));
	}

	@Override
	protected Field<?> getIdentifier() {
		return ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID;
	}

	@Override
	protected Field<?> getProjectIdentifier() {
		return CAMPAIGN_LIBRARY_NODE.PROJECT_ID;
	}

	@Override
	protected Condition craftInvariantFilter() {
		return ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(this.itemId).and(ITERATION.ITERATION_ID.eq(this.iterationId));
	}

	@Override
	protected SortField<?> getDefaultOrder() {
		return ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER.desc();
	}

	private Field<Integer> countBlockingMilestoneBindings() {
		return DSL.selectCount()
				.from(MILESTONE)
				.join(MILESTONE_CAMPAIGN).on(MILESTONE_CAMPAIGN.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
				.where(MILESTONE.STATUS.in(MilestoneStatus.MILESTONE_BLOCKING_STATUSES)
						.and(MILESTONE_CAMPAIGN.CAMPAIGN_ID.eq(CAMPAIGN_ITERATION.CAMPAIGN_ID))).asField();
	}
}
