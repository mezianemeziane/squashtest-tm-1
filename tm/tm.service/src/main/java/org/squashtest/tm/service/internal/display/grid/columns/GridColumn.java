/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.columns;

import com.google.common.base.CaseFormat;
import com.google.common.base.Converter;
import org.jooq.Field;
import org.jooq.SortField;
import org.squashtest.tm.service.internal.display.grid.GridSort;

public class GridColumn {
	protected final Field<?> aliasedField;
	protected final Field<?> nativeField;

	public GridColumn(Field<?> nativeField) {
		this.nativeField = nativeField;
		this.aliasedField = nativeField;
	}

	public GridColumn(Field<?> aliasedField, Field<?> nativeField) {
		this.nativeField = nativeField;
		this.aliasedField = aliasedField;
	}

	public Field<?> getAliasedField() {
		return aliasedField;
	}

	public Field<?> getNativeField() {
		return nativeField;
	}

	public String findRuntimeAlias() {
		Converter<String, String> converter = CaseFormat.UPPER_UNDERSCORE.converterTo(CaseFormat.LOWER_CAMEL);
		return converter.convert(aliasedField.getName());
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}

		// delegating equality to wrapped jooq field
		if (that instanceof GridColumn) {
			return ((GridColumn) that).aliasedField.equals(this.aliasedField);
		}
		return super.equals(that);
	}

	public SortField<?> generateOrderClause(GridSort.SortDirection direction) {
		if (direction.equals(GridSort.SortDirection.ASC)) {
			return this.aliasedField.asc();
		} else {
			return this.aliasedField.desc();
		}
	}
}
