/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record2;
import org.jooq.Result;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.AttachmentDto;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT_LIST;

@Repository
public class AttachmentDisplayDaoImpl implements AttachmentDisplayDao {

	@Inject
	private DSLContext dsl;

	@Override
	public AttachmentListDto findAttachmentListById(long attachmentListId) {
		Set<AttachmentListDto> attachmentLists = this.findAttachmentListByIds(Collections.singleton(attachmentListId));
		return new ArrayList<>(attachmentLists).get(0);
	}

	@Override
	public Set<AttachmentListDto> findAttachmentListByIds(Set<Long> attachmentListIds) {
		Set<AttachmentListDto> attachmentLists = dsl
			.select(ATTACHMENT_LIST.ATTACHMENT_LIST_ID, ATTACHMENT.ATTACHMENT_ID.as(RequestAliasesConstants.ID), ATTACHMENT.NAME, ATTACHMENT.SIZE, ATTACHMENT.ADDED_ON)
			.from(ATTACHMENT_LIST)
			.leftJoin(ATTACHMENT).on(ATTACHMENT.ATTACHMENT_LIST_ID.eq(ATTACHMENT_LIST.ATTACHMENT_LIST_ID))
			.where(ATTACHMENT_LIST.ATTACHMENT_LIST_ID.in(attachmentListIds))
			.fetch()
			.intoGroups(ATTACHMENT_LIST.ATTACHMENT_LIST_ID, AttachmentDto.class)
			.entrySet().stream()
			.map(AttachmentListDto::fromMapEntry)
			.collect(Collectors.toSet());

		Set<Long> ids = attachmentLists.stream().map(AttachmentListDto::getId).collect(Collectors.toSet());
		if (!ids.equals(attachmentListIds)) {
			String msg = String.format("Error. AttachmentListIds %s do not correspond to fetched attachment lists %s.", attachmentListIds.toString(), ids.toString());
			throw new IllegalArgumentException(msg);
		}

		return attachmentLists;
	}

	@Override
	public Map<Long, Integer> countAttachmentsByAttachmentListId(List<Long> attachmentListId) {
		return dsl.select(ATTACHMENT.ATTACHMENT_LIST_ID, DSL.count())
			.from(ATTACHMENT)
			.where(ATTACHMENT.ATTACHMENT_LIST_ID.in(attachmentListId))
			.groupBy(ATTACHMENT.ATTACHMENT_LIST_ID)
			.fetch()
			.intoMap(ATTACHMENT.ATTACHMENT_LIST_ID, DSL.count());
	}
}
