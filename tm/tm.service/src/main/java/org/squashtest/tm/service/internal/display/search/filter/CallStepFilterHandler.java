/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search.filter;

import com.google.common.collect.Sets;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.hibernate.HibernateQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.jpql.ExtendedHibernateQuery;
import org.squashtest.tm.domain.testcase.QActionTestStep;
import org.squashtest.tm.domain.testcase.QCallTestStep;
import org.squashtest.tm.domain.testcase.QTestCase;
import org.squashtest.tm.domain.testcase.QTestStep;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

import java.util.Set;

import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.TEST_CASE_CALLSTEPCOUNT;

@Component
public class CallStepFilterHandler implements FilterHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(CallStepFilterHandler.class);

	private final Set<String> handledPrototypes = Sets.newHashSet(TEST_CASE_CALLSTEPCOUNT);

	@Override
	public boolean canHandleFilter(GridFilterValue filter) {
		return this.handledPrototypes.contains(filter.getColumnPrototype());
	}

	@Override
	public void handleFilter(ExtendedHibernateQuery<?> query, GridFilterValue filter) {
		QTestCase outerTestCase = QTestCase.testCase;
		QTestCase initTestCase = new QTestCase("initTestCase");
		QTestStep testStep = new QTestStep("testStep");
		QActionTestStep actionTestStep = new QActionTestStep("actionTestStep");
		QCallTestStep callTestStep = new QCallTestStep("callTestStep");

		HibernateQuery<?> subquery = new ExtendedHibernateQuery<>()
			.select(Expressions.ONE)
			.from(initTestCase)
			.leftJoin(initTestCase.steps, testStep)
			.leftJoin(actionTestStep).on(testStep.id.eq(actionTestStep.id))
			.leftJoin(callTestStep).on(testStep.id.eq(callTestStep.id))
			.where(outerTestCase.id.eq(initTestCase.id))
			.groupBy(initTestCase.id);
		int min = Integer.parseInt(filter.getValues().get(0));
		switch (filter.getOperation()) {
			case "BETWEEN":
				int max = Integer.parseInt(filter.getValues().get(1));
				subquery.having(callTestStep.id.count().between(min, max));
				break;
			case "GREATER":
				subquery.having(callTestStep.id.count().gt(min));
				break;
			case "GREATER_EQUAL":
				subquery.having(callTestStep.id.count().goe(min));
				break;
			case "LOWER":
				subquery.having(callTestStep.id.count().lt(min));
				break;
			case "LOWER_EQUAL":
				subquery.having(callTestStep.id.count().loe(min));
				break;
			case "EQUALS":
				subquery.having(callTestStep.id.count().eq((long) min));
				break;
			default:
				throw new IllegalArgumentException("Unknown operation");
		}

		query.where(subquery.exists());
	}
}
