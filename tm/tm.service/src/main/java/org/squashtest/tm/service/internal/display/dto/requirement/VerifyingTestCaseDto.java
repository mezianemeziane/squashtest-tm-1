/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.requirement;

import java.util.Date;

public class VerifyingTestCaseDto {

	private Long id;
	private String name;
	private String projectName;
	private String reference;
	private String milestoneLabels;
	private Date milestoneMinDate;
	private Date milestoneMaxDate;
	private String status;
	private String importance;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getMilestoneLabels() {
		return milestoneLabels;
	}

	public void setMilestoneLabels(String milestoneLabels) {
		this.milestoneLabels = milestoneLabels;
	}

	public Date getMilestoneMinDate() {
		return milestoneMinDate;
	}

	public void setMilestoneMinDate(Date milestoneMinDate) {
		this.milestoneMinDate = milestoneMinDate;
	}

	public Date getMilestoneMaxDate() {
		return milestoneMaxDate;
	}

	public void setMilestoneMaxDate(Date milestoneMaxDate) {
		this.milestoneMaxDate = milestoneMaxDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getImportance() {
		return importance;
	}

	public void setImportance(String importance) {
		this.importance = importance;
	}
}
