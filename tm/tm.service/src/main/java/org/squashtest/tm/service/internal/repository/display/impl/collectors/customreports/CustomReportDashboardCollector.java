/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl.collectors.customreports;

import org.jooq.DSLContext;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.customreport.CustomReportTreeDefinition;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.impl.collectors.AbstractTreeNodeCollector;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.squashtest.tm.jooq.domain.Tables.*;

@Component
public class CustomReportDashboardCollector extends AbstractTreeNodeCollector {

	public CustomReportDashboardCollector(DSLContext dsl, CustomFieldValueDisplayDao customFieldValueDisplayDao, ActiveMilestoneHolder activeMilestoneHolder, MilestoneDisplayDao milestoneDisplayDao) {
		super(dsl, customFieldValueDisplayDao, activeMilestoneHolder, milestoneDisplayDao);
	}

	@Override
	protected Map<Long, DataRow> doCollect(List<Long> ids) {
		// @formatter:off
		Map<Long, DataRow> customReportDashboards = dsl.select(
			CUSTOM_REPORT_LIBRARY_NODE.CRLN_ID
			, CUSTOM_REPORT_DASHBOARD.CRD_ID, CUSTOM_REPORT_DASHBOARD.NAME, CUSTOM_REPORT_DASHBOARD.PROJECT_ID.as(PROJECT_ID_ALIAS))
			.from(CUSTOM_REPORT_LIBRARY_NODE)
			.innerJoin(CUSTOM_REPORT_DASHBOARD).on(CUSTOM_REPORT_DASHBOARD.CRD_ID.eq(CUSTOM_REPORT_LIBRARY_NODE.ENTITY_ID))
				.and(CUSTOM_REPORT_LIBRARY_NODE.ENTITY_TYPE.eq(CustomReportTreeDefinition.DASHBOARD.name()))
			.where(CUSTOM_REPORT_LIBRARY_NODE.CRLN_ID.in(ids))
			.groupBy(CUSTOM_REPORT_LIBRARY_NODE.CRLN_ID, CUSTOM_REPORT_DASHBOARD.CRD_ID)
			// @formatter:on
			.fetch().stream()
			.collect(Collectors.toMap(
				tuple -> tuple.get(CUSTOM_REPORT_LIBRARY_NODE.CRLN_ID),
				tuple -> {
					DataRow dataRow = new DataRow();
					dataRow.setId(new NodeReference(NodeType.CUSTOM_REPORT_DASHBOARD, tuple.get(CUSTOM_REPORT_LIBRARY_NODE.CRLN_ID)).toNodeId());
					dataRow.setProjectId(tuple.get(PROJECT_ID_ALIAS, Long.class));
					dataRow.setData(tuple.intoMap());
					return dataRow;
				}
			));
		appendMilestonesByProject(customReportDashboards);
		return customReportDashboards;
	}

	@Override
	public NodeType getHandledEntityType() {
		return NodeType.CUSTOM_REPORT_DASHBOARD;
	}
}
