/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.domain.scm.ScmServer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ScmServerDto {
	private Long serverId;
	private String name;
	private String url;
	private String kind;
	private String committerMail;
	private List<ScmRepositoryDto> repositories = new ArrayList<>();

	public ScmServerDto() {
	}

	public ScmServerDto(ScmServer server, List<ScmRepository> repositories) {
		this.serverId = server.getId();
		this.name = server.getName();
		this.url = server.getUrl();
		this.kind = server.getKind();
		this.committerMail = server.getCommitterMail();
		this.repositories = repositories.stream()
			.map(ScmRepositoryDto::new)
			.collect(Collectors.toList());
	}

	public Long getServerId() {
		return serverId;
	}

	public void setServerId(Long serverId) {
		this.serverId = serverId;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getCommitterMail() {
		return committerMail;
	}

	public void setCommitterMail(String committerMail) {
		this.committerMail = committerMail;
	}

	public List<ScmRepositoryDto> getRepositories() {
		return repositories;
	}

	public void setRepositories(List<ScmRepositoryDto> repositories) {
		this.repositories = repositories;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
