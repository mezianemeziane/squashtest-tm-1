/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.campaign;

import com.google.common.base.CaseFormat;
import com.google.common.base.Converter;
import org.jooq.DSLContext;
import org.jooq.Record2;
import org.jooq.SelectHavingStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.field;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITEM_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.STEP_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.STEP_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SUCCESS_RATE;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestPlanItemSuccessRateCalculator {

	private final String LATEST_EXECUTION_ID = "LATEST_EXECUTION_ID";
	private final String MAX_EXECUTION_ORDER = "MAX_EXECUTION_ORDER";

	private final DSLContext dslContext;

	public TestPlanItemSuccessRateCalculator(DSLContext dslContext) {
		this.dslContext = dslContext;
	}

	public void appendSuccessRate(GridResponse gridResponse){
		this.appendSuccessRate(gridResponse.getDataRows());
	}

	private void appendSuccessRate(List<DataRow> dataRows) {
		Set<Long> testPlanItemIds = extractIds(dataRows);

		SelectHavingStep<?> computeSuccessRate = computeSuccessRate(testPlanItemIds);

		Map<Long, Float> rateById = dslContext
			.select(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID, DSL.ifnull(computeSuccessRate.field(SUCCESS_RATE), 0).as(SUCCESS_RATE))
			.from(ITERATION_TEST_PLAN_ITEM)
			.innerJoin(computeSuccessRate)
			.on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(computeSuccessRate.field(ITEM_ID, Long.class)))
			.fetch().intoMap(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID, computeSuccessRate.field(SUCCESS_RATE, Float.class));

		Converter<String, String> converter = CaseFormat.UPPER_UNDERSCORE.converterTo(CaseFormat.LOWER_CAMEL);
		String successRateKey = converter.convert(SUCCESS_RATE);
		String executionModeKey = converter.convert("EXECUTION_MODE");
		String executionStatusKey = converter.convert("EXECUTION_STATUS");

		dataRows.forEach(dataRow -> {
			long id = Long.parseLong(dataRow.getId());
			if (TestCaseExecutionMode.AUTOMATED.name().equals(dataRow.getData().get(executionModeKey))) {
				boolean isAutomatedExecutionInSuccess = ExecutionStatus.SUCCESS.name().equals(dataRow.getData().get(executionStatusKey));
				float automatedExecutionSuccessRate = isAutomatedExecutionInSuccess ? 100 : 0;
				dataRow.getData().put(successRateKey, automatedExecutionSuccessRate);
			} else {
				dataRow.getData().put(successRateKey, rateById.getOrDefault(id, 0f));
			}
		});

	}

	private Set<Long> extractIds(List<DataRow> dataRows) {
		Set<Long> testPlanItemIds = dataRows.stream()
			.map(DataRow::getId)
			.map(Long::parseLong)
			.collect(Collectors.toSet());
		return testPlanItemIds;
	}

	private SelectHavingStep<?> computeSuccessRate(Set<Long> testPlanItemIds) {
		final String NUM_SUCCESS = "NUM_SUCCESS";
		final String NUM_STEPS = "NUM_STEPS";

		SelectHavingStep<?> stepsWithStatus = getStepsWithStatus(testPlanItemIds);

		// ITEM_ID | NUM_SUCCESS
		SelectHavingStep<?> successQuery = DSL.select(
			stepsWithStatus.field(ITEM_ID).as(ITEM_ID),
			DSL.count(field(STEP_ID)).as(NUM_SUCCESS))
			.from(stepsWithStatus)
			.where(stepsWithStatus.field(STEP_STATUS, String.class).eq("SUCCESS"))
			.and(field(ITEM_ID).in(testPlanItemIds))
			.groupBy(stepsWithStatus.field(ITEM_ID));

		// ITEM_ID | NUM_STEPS
		SelectHavingStep<?> totalQuery = DSL.select(
			stepsWithStatus.field(ITEM_ID).as(ITEM_ID),
			DSL.count(field(STEP_ID)).as(NUM_STEPS))
			.from(stepsWithStatus)
			.where(field(ITEM_ID).in(testPlanItemIds))
			.groupBy(stepsWithStatus.field(ITEM_ID));

		// ITEM_ID | SUCCESS_RATE
		return DSL.select(
			totalQuery.field(ITEM_ID).as(ITEM_ID),
			// Postgresql needs these explicit casts to get the division right
			successQuery.field(NUM_SUCCESS).cast(Double.class)
				.divide(totalQuery.field(NUM_STEPS).cast(Double.class)).multiply(100.0).as(SUCCESS_RATE))
			.from(successQuery).rightJoin(totalQuery)
			.on(successQuery.field(ITEM_ID, Long.class).eq(totalQuery.field(ITEM_ID, Long.class)));
	}

	private SelectHavingStep<?> getStepsWithStatus(Set<Long> testPlanItemIds) {

		// ITEM_ID | LATEST_EXECUTION_ID
		SelectHavingStep<?> itpiWithLatestExecution = findItpiWithLatestExecution(testPlanItemIds);

		// ITEM_ID | STEP_ID | STEP_STATUS
		return DSL.select(
			itpiWithLatestExecution.field(ITEM_ID).as(ITEM_ID),
			EXECUTION_STEP.EXECUTION_STEP_ID.as(STEP_ID),
			EXECUTION_STEP.EXECUTION_STATUS.as(STEP_STATUS))
			.from(EXECUTION_EXECUTION_STEPS)
			.innerJoin(EXECUTION_STEP)
			.on(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID.eq(EXECUTION_STEP.EXECUTION_STEP_ID))
			.innerJoin(itpiWithLatestExecution)
			.on(EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(DSL.field(LATEST_EXECUTION_ID, Long.class)));
	}

	private SelectOnConditionStep<Record2<Long, Long>> findItpiWithLatestExecution(Set<Long> testPlanItemIds) {

		// ITEM_ID | MAX_EXECUTION_ORDER
		SelectHavingStep<?> findLastExecutionByOrder = findLastExecutionByOrder(testPlanItemIds);

		return DSL.select(
			ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.as(ITEM_ID),
			ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.as(LATEST_EXECUTION_ID))
			.from(ITEM_TEST_PLAN_EXECUTION)
			.innerJoin(findLastExecutionByOrder)
			.on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID
				.eq(findLastExecutionByOrder.field(ITEM_ID, Long.class)))
			.and(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER
				.eq(findLastExecutionByOrder.field(MAX_EXECUTION_ORDER, Integer.class)));
	}

	private SelectHavingStep<Record2<Long, Integer>> findLastExecutionByOrder(Set<Long> testPlanItemIds) {
		return DSL.select(
			ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.as(ITEM_ID),
			DSL.max(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER).as(MAX_EXECUTION_ORDER))
			.from(ITEM_TEST_PLAN_EXECUTION)
			.where(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.in(testPlanItemIds))
			.groupBy(DSL.field(ITEM_ID));
	}
}
