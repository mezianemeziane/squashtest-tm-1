/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics.campaign;

import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.TestCaseImportance;

import java.util.LinkedHashMap;
import java.util.List;

public final class CampaignStatisticsBundle {

	private List<IterationTestInventoryStatistics> iterationTestInventoryStatistics;

	private CampaignProgressionStatistics campaignProgressionStatistics;

	private LinkedHashMap<ExecutionStatus, Integer> campaignTestCaseStatusStatistics;

	private LinkedHashMap<TestCaseImportance, Integer> campaignNonExecutedTestCaseImportanceStatistics;

	private CampaignTestCaseSuccessRateStatistics campaignTestCaseSuccessRateStatistics;

	private Long selectedId;

	public List<IterationTestInventoryStatistics> getIterationTestInventoryStatistics() {
		return iterationTestInventoryStatistics;
	}

	public void setIterationTestInventoryStatistics(
			List<IterationTestInventoryStatistics> iterationTestInventoryStatistics) {
		this.iterationTestInventoryStatistics = iterationTestInventoryStatistics;
	}


	public CampaignProgressionStatistics getCampaignProgressionStatistics() {
		return campaignProgressionStatistics;
	}


	public void setCampaignProgressionStatistics(
			CampaignProgressionStatistics campaignProgressionStatistics) {
		this.campaignProgressionStatistics = campaignProgressionStatistics;
	}


	public CampaignTestCaseSuccessRateStatistics getCampaignTestCaseSuccessRateStatistics() {
		return campaignTestCaseSuccessRateStatistics;
	}


	public void setCampaignTestCaseSuccessRateStatistics(
			CampaignTestCaseSuccessRateStatistics campaignTestCaseSuccessRateStatistics) {
		this.campaignTestCaseSuccessRateStatistics = campaignTestCaseSuccessRateStatistics;
	}

	public Long getSelectedId() {
		return selectedId;
	}

	public void setSelectedId(Long selectedId) {
		this.selectedId = selectedId;
	}

	public LinkedHashMap<TestCaseImportance, Integer> getCampaignNonExecutedTestCaseImportanceStatistics() {
		return campaignNonExecutedTestCaseImportanceStatistics;
	}

	public void setCampaignNonExecutedTestCaseImportanceStatistics(LinkedHashMap<TestCaseImportance, Integer> campaignNonExecutedTestCaseImportanceStatistics) {
		this.campaignNonExecutedTestCaseImportanceStatistics = campaignNonExecutedTestCaseImportanceStatistics;
	}

	public LinkedHashMap<ExecutionStatus, Integer> getCampaignTestCaseStatusStatistics() {
		return campaignTestCaseStatusStatistics;
	}

	public void setCampaignTestCaseStatusStatistics(LinkedHashMap<ExecutionStatus, Integer> campaignTestCaseStatusStatistics) {
		this.campaignTestCaseStatusStatistics = campaignTestCaseStatusStatistics;
	}
}
