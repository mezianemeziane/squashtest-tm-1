/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import com.google.common.base.CaseFormat;
import com.google.common.base.Converter;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.jooq.domain.tables.Milestone;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.repository.display.TestCaseResearchDisplayDao;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT_LIST;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.impl.DisplayDaoUtils.getMilestoneLocked;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ATTACHMENTS_ALIAS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.COVERAGES_ALIAS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_NAME;

@Repository
public class TestCaseResearchDisplayDaoImpl implements TestCaseResearchDisplayDao {

	private static final String ID_ALIAS = "ID";
	private static final String ITEMS_ALIAS = "ITERATIONS";
	private static final String MILESTONES_ALIAS = "MILESTONES";
	private static final String NATURE_ALIAS = "NATURE";
	private static final String REQ_MILESTONE_LOCKED = "REQ_MILESTONE_LOCKED";
	private static final String STATUS_ALIAS = "STATUS";
	private static final String STEPS_ALIAS = "STEPS";
	private static final String TC_MILESTONE_LOCKED = "TC_MILESTONE_LOCKED";
	private static final String TYPE_ALIAS = "TYPE";
	private static final String ITERATION_LIST = "ITERATION_LIST";
	private static final String TEST_SUITE_LIST = "TEST_SUITE_LIST";
	private static final String CAMPAIGN_LIST = "CAMPAIGN_LIST";

	private final DSLContext jooq;

	public TestCaseResearchDisplayDaoImpl(DSLContext jooq) {
		this.jooq = jooq;
	}

	@Override
	public GridResponse getRows(List<Long> testCaseIds) {
		GridResponse gridResponse = new GridResponse();

		Milestone reqMilestoneLocked = MILESTONE.as(REQ_MILESTONE_LOCKED);
		Milestone tcMilestoneLocked = MILESTONE.as(TC_MILESTONE_LOCKED);

		jooq.select(getFields(tcMilestoneLocked, reqMilestoneLocked))
			.from(
				getTable(tcMilestoneLocked, reqMilestoneLocked)
			)
			.where(TEST_CASE_LIBRARY_NODE.TCLN_ID.in(testCaseIds))
			.groupBy(TEST_CASE_LIBRARY_NODE.TCLN_ID.as(ID_ALIAS), TEST_CASE.TCLN_ID, PROJECT.PROJECT_ID)
			.stream().forEach(record -> {
			DataRow dataRow = new DataRow();
			dataRow.setId(record.get(TEST_CASE_LIBRARY_NODE.TCLN_ID.as(ID_ALIAS)).toString());
			dataRow.setProjectId(record.get(TEST_CASE_LIBRARY_NODE.PROJECT_ID));
			Map<String, Object> rawData = record.intoMap();
			Map<String, Object> data = new HashMap<>();

			// Using 'Collectors.toMap' won't work for entries with null values
			for (Map.Entry<String, Object> entry : rawData.entrySet()) {
				data.put(convertField(entry.getKey()), entry.getValue());
			}
			dataRow.setData(data);
			gridResponse.addDataRow(dataRow);
		});
		return gridResponse;
	}

	private List<Field<?>> getFields(Milestone tcMilestoneLocked, Milestone reqMilestoneLocked) {
		return Arrays.asList(TEST_CASE_LIBRARY_NODE.TCLN_ID.as(ID_ALIAS), TEST_CASE_LIBRARY_NODE.NAME, TEST_CASE_LIBRARY_NODE.PROJECT_ID,
			TEST_CASE_LIBRARY_NODE.CREATED_BY, TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY, TEST_CASE.REFERENCE, TEST_CASE.IMPORTANCE,
			TEST_CASE.IMPORTANCE_AUTO, TEST_CASE.TC_NATURE.as(NATURE_ALIAS), TEST_CASE.TC_TYPE.as(TYPE_ALIAS), TEST_CASE.TC_STATUS.as(STATUS_ALIAS),
			TEST_CASE.AUTOMATABLE, PROJECT.NAME.as(PROJECT_NAME), DSL.countDistinct(MILESTONE_TEST_CASE.MILESTONE_ID).as(MILESTONES_ALIAS),
			DSL.countDistinct(REQUIREMENT_VERSION_COVERAGE.REQUIREMENT_VERSION_COVERAGE_ID).as(COVERAGES_ALIAS),
			DSL.countDistinct(TEST_CASE_STEPS.STEP_ID).as(STEPS_ALIAS), DSL.countDistinct(ITERATION.ITERATION_ID).as(ITEMS_ALIAS),
			DSL.countDistinct(ATTACHMENT.ATTACHMENT_ID).as(ATTACHMENTS_ALIAS), DSL.countDistinct(reqMilestoneLocked.MILESTONE_ID).as(REQ_MILESTONE_LOCKED),
			DSL.groupConcatDistinct(ITERATION.ITERATION_ID).as(ITERATION_LIST), DSL.groupConcatDistinct(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID).as(TEST_SUITE_LIST),
			DSL.groupConcatDistinct(CAMPAIGN.CLN_ID).as(CAMPAIGN_LIST), DSL.countDistinct(tcMilestoneLocked.MILESTONE_ID).as(TC_MILESTONE_LOCKED));
	}

	private Table<?> getTable(Milestone tcMilestoneLocked, Milestone reqMilestoneLocked) {
		return TEST_CASE_LIBRARY_NODE.innerJoin(TEST_CASE).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
			.innerJoin(PROJECT).on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.leftJoin(ATTACHMENT_LIST).on(TEST_CASE_LIBRARY_NODE.ATTACHMENT_LIST_ID.eq(ATTACHMENT_LIST.ATTACHMENT_LIST_ID))
			.leftJoin(ATTACHMENT).on(ATTACHMENT_LIST.ATTACHMENT_LIST_ID.eq(ATTACHMENT.ATTACHMENT_LIST_ID))
			.leftJoin(TEST_CASE_STEPS).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE_STEPS.TEST_CASE_ID))
			.leftJoin(REQUIREMENT_VERSION_COVERAGE).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID))
			.leftJoin(MILESTONE_REQ_VERSION).on(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(MILESTONE_REQ_VERSION.REQ_VERSION_ID))
			.leftJoin(getMilestoneLocked().asTable(REQ_MILESTONE_LOCKED)).on(reqMilestoneLocked.MILESTONE_ID.eq(MILESTONE_REQ_VERSION.MILESTONE_ID))
			.leftJoin(MILESTONE_TEST_CASE).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(MILESTONE_TEST_CASE.TEST_CASE_ID))
			.leftJoin(getMilestoneLocked().asTable(TC_MILESTONE_LOCKED)).on(tcMilestoneLocked.MILESTONE_ID.eq(MILESTONE_TEST_CASE.MILESTONE_ID))
			.leftJoin(ITERATION_TEST_PLAN_ITEM).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(ITERATION_TEST_PLAN_ITEM.TCLN_ID))
			.leftJoin(ITEM_TEST_PLAN_LIST).on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
			.leftJoin(ITERATION).on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
			.leftJoin(TEST_SUITE_TEST_PLAN_ITEM).on(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
			.leftJoin(CAMPAIGN_TEST_PLAN_ITEM).on(CAMPAIGN_TEST_PLAN_ITEM.TEST_CASE_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
			.leftJoin(CAMPAIGN).on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID));
	}


	private String convertField(String fieldName) {
		Converter<String, String> converter = CaseFormat.UPPER_UNDERSCORE.converterTo(CaseFormat.LOWER_CAMEL);
		return converter.convert(fieldName);
	}
}
