/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.columns;

import org.jooq.Field;
import org.jooq.SortField;
import org.squashtest.tm.domain.Level;
import org.squashtest.tm.service.internal.display.grid.GridSort;

import java.util.HashMap;
import java.util.Map;

public class LevelEnumColumn extends GridColumn {

	private Class<? extends Level> levelEnum;

	public LevelEnumColumn(Class<? extends Level> levelEnum, Field<String> field) {
		super(field);
		this.levelEnum = levelEnum;
	}

	// This override won't work if the field is given an alias
	public SortField<?> generateOrderClause(GridSort.SortDirection direction) {
		Map<String, Integer> sortMap = getAscSortMap();

		// Invert ranks if direction is descending
		if (direction.equals(GridSort.SortDirection.DESC)) {
			sortMap.keySet().forEach(key -> sortMap.put(key, sortMap.get(key) * -1));
		}

		return this.aliasedField.sort((Map)sortMap);
	}

	/**
	 * This method must map each enum value (as Strings) with an arbitrary rank used for comparison. Default
	 * implementation uses the 'level' field as a rank.
	 *
	 * @return a mapping between enum string values and the rank used for ordering
	 */
	public Map<String, Integer> getAscSortMap() {
		Map<String, Integer> sortMap = new HashMap<>();

		for (Level level: levelEnum.getEnumConstants()) {
			sortMap.put(level.toString(), level.getLevel());
		}

		return sortMap;
	}
}
