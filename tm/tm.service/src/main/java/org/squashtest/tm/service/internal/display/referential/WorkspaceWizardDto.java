/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.referential;

import org.squashtest.tm.api.security.acls.AccessRule;
import org.squashtest.tm.api.wizard.WorkspaceWizard;
import org.squashtest.tm.api.workspace.WorkspaceType;

public class WorkspaceWizardDto {

    String id;
    WizardMenuDto wizardMenu;
    WorkspaceType displayWorkspace;

	WorkspaceWizardDto() {

    }

    public static WorkspaceWizardDto fromWorkspaceWizard(WorkspaceWizard wizard) {
        WorkspaceWizardDto dto = new WorkspaceWizardDto();
        dto.id = wizard.getId();
		dto.displayWorkspace = wizard.getDisplayWorkspace();

        if (wizard.getWizardMenu() != null) {
            dto.wizardMenu = new WizardMenuDto();
            dto.wizardMenu.label = wizard.getWizardMenu().getLabel();
            dto.wizardMenu.url = wizard.getWizardMenu().getUrl();
            dto.wizardMenu.tooltip = wizard.getWizardMenu().getTooltip();
            dto.wizardMenu.accessRule = wizard.getWizardMenu().getAccessRule();
        }

        return dto;
    }


    static class WizardMenuDto {
        String tooltip;
        String url;
        String label;
        AccessRule accessRule;

        public String getTooltip() {
            return tooltip;
        }

        public void setTooltip(String tooltip) {
            this.tooltip = tooltip;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public AccessRule getAccessRule() {
            return accessRule;
        }

        public void setAccessRule(AccessRule accessRule) {
            this.accessRule = accessRule;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public WizardMenuDto getWizardMenu() {
        return wizardMenu;
    }

    public void setWizardMenu(WizardMenuDto wizardMenu) {
        this.wizardMenu = wizardMenu;
    }

	public WorkspaceType getDisplayWorkspace() {
		return displayWorkspace;
	}

	public void setDisplayWorkspace(WorkspaceType displayWorkspace) {
		this.displayWorkspace = displayWorkspace;
	}
}
