/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.campaign;

import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldValueDto;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportDashboard;

import java.util.ArrayList;
import java.util.List;

public class CampaignFolderDto {
	private Long id;
	private Long projectId;
	private String name;
	private String description;
	private Long attachmentListId;
	private AttachmentListDto attachmentList;
	private List<CustomFieldValueDto> customFieldValues = new ArrayList<>();
	private int nbIssues;
	private JsonCustomReportDashboard dashboard;
	private boolean shouldShowFavoriteDashboard;
	private boolean canShowFavoriteDashboard;
	private Long favoriteDashboardId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getAttachmentListId() {
		return attachmentListId;
	}

	public void setAttachmentListId(Long attachmentListId) {
		this.attachmentListId = attachmentListId;
	}

	public AttachmentListDto getAttachmentList() {
		return attachmentList;
	}

	public void setAttachmentList(AttachmentListDto attachmentList) {
		this.attachmentList = attachmentList;
	}

	public List<CustomFieldValueDto> getCustomFieldValues() {
		return customFieldValues;
	}

	public void setCustomFieldValues(List<CustomFieldValueDto> customFieldValues) {
		this.customFieldValues = customFieldValues;
	}

	public int getNbIssues() {
		return nbIssues;
	}

	public void setNbIssues(int nbIssues) {
		this.nbIssues = nbIssues;
	}

	public JsonCustomReportDashboard getDashboard() {
		return dashboard;
	}

	public void setDashboard(JsonCustomReportDashboard dashboard) {
		this.dashboard = dashboard;
	}

	public boolean isShouldShowFavoriteDashboard() {
		return shouldShowFavoriteDashboard;
	}

	public void setShouldShowFavoriteDashboard(boolean shouldShowFavoriteDashboard) {
		this.shouldShowFavoriteDashboard = shouldShowFavoriteDashboard;
	}

	public boolean isCanShowFavoriteDashboard() {
		return canShowFavoriteDashboard;
	}

	public void setCanShowFavoriteDashboard(boolean canShowFavoriteDashboard) {
		this.canShowFavoriteDashboard = canShowFavoriteDashboard;
	}

	public Long getFavoriteDashboardId() {
		return favoriteDashboardId;
	}

	public void setFavoriteDashboardId(Long favoriteDashboardId) {
		this.favoriteDashboardId = favoriteDashboardId;
	}
}
