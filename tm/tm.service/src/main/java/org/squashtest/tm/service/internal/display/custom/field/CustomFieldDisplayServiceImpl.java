/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.custom.field;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.service.display.custom.field.CustomFieldDisplayService;
import org.squashtest.tm.service.internal.display.dto.CufBindingDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldDto;
import org.squashtest.tm.service.internal.display.dto.ProjectDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.administration.CustomFieldGrid;
import org.squashtest.tm.service.internal.repository.display.CustomFieldDao;

import javax.inject.Inject;
import javax.transaction.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN_OR_PROJECT_MANAGER;

@Service
@Transactional
public class CustomFieldDisplayServiceImpl implements CustomFieldDisplayService {

	private static final String ADMIN_OR_PROJECT_MANAGER_ON_CURRENT_PROJECT = HAS_ROLE_ADMIN +
		" or hasPermission(#projectId, 'org.squashtest.tm.domain.project.Project', 'MANAGEMENT')" +
		" or hasPermission(#projectId, 'org.squashtest.tm.domain.project.ProjectTemplate' , 'MANAGEMENT')";

	@Inject
	private DSLContext dsl;

	@Inject
	CustomFieldDao customFieldDao;

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public GridResponse findAll(GridRequest request) {

		CustomFieldGrid customFieldGrid = new CustomFieldGrid();
		return customFieldGrid.getRows(request, dsl);
	}

	@Override
	public CustomFieldDto getCustomFieldView(Long customFieldId) {
		return customFieldDao.findByIdWithPossibleValues(customFieldId);
	}

	@PreAuthorize(HAS_ROLE_ADMIN_OR_PROJECT_MANAGER)
	@Override
	public List<CustomFieldDto> findAllWithPossibleValues() {
		return customFieldDao.findAllWithPossibleValues();
	}

	@PreAuthorize(ADMIN_OR_PROJECT_MANAGER_ON_CURRENT_PROJECT)
	@Override
	public Map<BindableEntity, Set<CufBindingDto>> findAllCustomFieldBindings(Long projectId) {
		ProjectDto dto = new ProjectDto();
		dto.setId(projectId);
		customFieldDao.appendCustomFieldBindings(Collections.singletonList(dto));

		return dto.getCustomFieldBindings();
	}
}
