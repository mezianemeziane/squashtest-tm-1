/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.referential;

import com.google.common.collect.Multimap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.template.TemplateConfigurablePlugin;
import org.squashtest.tm.domain.bdd.BddScriptLanguage;
import org.squashtest.tm.domain.bdd.Keyword;
import org.squashtest.tm.service.configuration.ConfigurationService;
import org.squashtest.tm.service.display.referential.ReferentialDataProvider;
import org.squashtest.tm.service.display.user.UserDisplayService;
import org.squashtest.tm.service.internal.display.dto.GlobalConfigurationDto;
import org.squashtest.tm.service.internal.display.dto.KeywordDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.ProjectDto;
import org.squashtest.tm.service.internal.display.dto.ProjectFilterDto;
import org.squashtest.tm.service.internal.dto.DetailedUserDto;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.repository.display.AclDisplayDao;
import org.squashtest.tm.service.internal.repository.display.AutomatedTestTechnologyDisplayDao;
import org.squashtest.tm.service.internal.repository.display.BugTrackerDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CustomFieldDao;
import org.squashtest.tm.service.internal.repository.display.InfoListDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ProjectDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ProjectFilterDisplayDao;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionLinkTypeDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ScmServerDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TestAutomationServerDisplayDao;
import org.squashtest.tm.service.internal.security.AuthenticationProviderContext;
import org.squashtest.tm.service.project.CustomProjectFinder;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.ACTIVATED_USER_EXCESS;
import static org.squashtest.tm.service.configuration.ConfigurationService.Properties.PLUGIN_LICENSE_EXPIRATION;


@Service
@Transactional(readOnly = true)
public class ReferentialDataProviderImpl implements ReferentialDataProvider {

	@Inject
	private CustomProjectFinder projectFinder;
	@Inject
	private ProjectFilterDisplayDao projectFilterDao;
	@Inject
	private ProjectDisplayDao projectDao;
	@Inject
	private MilestoneDisplayDao milestoneDao;
	@Inject
	private CustomFieldDao customFieldDao;
	@Inject
	private InfoListDisplayDao infoListDao;
	@Inject
	private ConfigurationService configurationService;
	@Inject
	private AclDisplayDao aclDisplayDao;
	@Inject
	private BugTrackerDisplayDao bugTrackerDisplayDao;
	@Inject
	private TestAutomationServerDisplayDao testAutomationServerDisplayDao;
	@Inject
	private UserDisplayService userDisplayService;
	@Inject
	private RequirementVersionLinkTypeDisplayDao requirementVersionLinkTypeDisplayDao;
	@Inject
	private MessageSource translateService;
	@Inject
	private AutomatedTestTechnologyDisplayDao automatedTestTechnologyDisplayDao;
	@Inject
	private ScmServerDisplayDao scmServerDisplayDao;
	@Inject
	AuthenticationProviderContext authenticationProviderContext;

	@Autowired(required = false)
	private final Collection<TemplateConfigurablePlugin> templatePlugins = Collections.emptyList();

	@Override
	public AdminReferentialData findAdminReferentialData() {
		AdminReferentialData referentialData = new AdminReferentialData();
		appendUser(referentialData);
		appendGlobalConfiguration(referentialData);
		appendLicenseInformation(referentialData);
		appendCustomFields(referentialData);
		appendAvailableTestAutomationServerKinds(referentialData);
		referentialData.setCanManageLocalPassword(authenticationProviderContext.isInternalProviderEnabled());
		appendTemplateConfigurablePlugins(referentialData);
		return referentialData;
	}

	@Override
	public ReferentialData findReferentialData() {
		ReferentialData referentialData = new ReferentialData();
		appendUser(referentialData);
		List<Long> projectIds = projectFinder.findAllReadableIds(referentialData.getUser());
		addProjectFilter(referentialData, referentialData.getUser());
		appendProjects(referentialData, referentialData.getUser(), projectIds);
		appendInterProjectData(referentialData, projectIds);
		appendGlobalConfiguration(referentialData);
		appendRequirementVersionLinkTypes(referentialData);
		appendLicenseInformation(referentialData);
		referentialData.setCanManageLocalPassword(authenticationProviderContext.isInternalProviderEnabled());
		appendTemplateConfigurablePlugins(referentialData);
		return referentialData;
	}

	private void appendScmServers(ReferentialData referentialData) {
		referentialData.setScmServers(scmServerDisplayDao.findAll());
	}

	private void appendRequirementVersionLinkTypes(ReferentialData referentialData) {
		referentialData.setRequirementVersionLinkTypes(requirementVersionLinkTypeDisplayDao.findAll());
	}

	private void appendInterProjectData(ReferentialData referentialData, List<Long> projectIds) {
		appendInfoLists(referentialData);
		appendCustomFields(referentialData);
		appendMilestones(referentialData, projectIds);
		appendBugTrackers(referentialData);
		appendAutomationServers(referentialData);
		appendAutomatedTestTechnologies(referentialData);
		appendScmServers(referentialData);
	}

	private void appendAutomatedTestTechnologies(ReferentialData referentialData) {
		referentialData.setAutomatedTestTechnologies(automatedTestTechnologyDisplayDao.findAll());
	}

	private void appendUser(AdminReferentialData referentialData) {
		DetailedUserDto currentUser = userDisplayService.findCurrentUser();
		referentialData.setUser(currentUser);
	}

	private void appendGlobalConfiguration(AdminReferentialData referentialData) {
		GlobalConfigurationDto globalConfigurationDto = GlobalConfigurationDto.create(this.configurationService.findAllConfiguration());
		referentialData.setGlobalConfiguration(globalConfigurationDto);
	}

	private void appendAutomationServers(ReferentialData referentialData) {
		referentialData.setAutomationServers(testAutomationServerDisplayDao.findAll());
	}

	private void appendBugTrackers(ReferentialData referentialData) {
		referentialData.setBugTrackers(bugTrackerDisplayDao.findAll());
	}

	private void appendCustomFields(AdminReferentialData referentialData) {
		referentialData.setCustomFields(customFieldDao.findAllWithPossibleValues());
	}

	private void appendInfoLists(ReferentialData referentialData) {
		referentialData.setInfoLists(infoListDao.findAllWithItems());
	}

	private void appendProjects(ReferentialData referentialData, UserDto currentUser, List<Long> projectIds) {
		Map<Long, ProjectDto> projects = projectDao.getActiveProjectsByIds(projectIds).stream().collect(Collectors.toMap(ProjectDto::getId, Function.identity()));
		customFieldDao.appendCustomFieldBindings(new ArrayList<>(projects.values()));
		bugTrackerDisplayDao.appendBugTrackerBindings(new ArrayList<>(projects.values()));
		milestoneDao.appendMilestoneBinding(new ArrayList<>(projects.values()));
		appendProjectPermissions(projects, currentUser);
		appendDisabledExecutionStatus(projects);
		appendTranslatedKeywords(new ArrayList<>(projects.values()));
		referentialData.setProjects(new ArrayList<>(projects.values()));
		projectDao.appendActivatedPlugins(new ArrayList<>(projects.values()));
	}

	private void appendMilestones(ReferentialData referentialData, List<Long> projectIds) {
		Collection<Long> milestoneIds = milestoneDao.findMilestonesByProjectId(new HashSet<>(projectIds)).values();
		List<MilestoneDto> milestones = milestoneDao.findByIds(new HashSet<>(milestoneIds));
		referentialData.setMilestones(milestones);
	}

	private void appendDisabledExecutionStatus(Map<Long, ProjectDto> projects) {
		if (!projects.isEmpty()) {
			Set<Long> projectIds = projects.keySet();
			Map<Long, List<String>> disabledExecutionStatus = this.projectDao.getDisabledExecutionStatus(projectIds);
			disabledExecutionStatus.forEach((projectId, disabledStatus) -> {
				projects.get(projectId).setDisabledExecutionStatus(disabledStatus);
			});
		}
	}

	private void appendProjectPermissions(Map<Long, ProjectDto> projects, UserDto currentUser) {
		if (!projects.isEmpty()) {
			Map<Long, Multimap<String, String>> permissions = aclDisplayDao.findPermissions(new ArrayList<>(projects.keySet()), currentUser.getPartyIds());
			permissions.forEach((projectId, projectPermissions) -> {
				projects.get(projectId).setPermissions(projectPermissions.asMap());
			});
		}
	}

	private void appendTranslatedKeywords(List<ProjectDto> projects) {
		projects.forEach(project -> {
			Locale locale = BddScriptLanguage.valueOf(project.getBddScriptLanguage()).getLocale();
			project.setKeywords(
				getTranslatedKeywordsList(locale));
		});
	}

	private List<KeywordDto> getTranslatedKeywordsList(Locale locale) {
		return Arrays.stream(Keyword.values())
			.map(keyword -> new KeywordDto(
				keyword.name(),
				translateService.getMessage(
					keyword.i18nKeywordNameKey(),
					null,
					locale)))
			.collect(Collectors.toList());
	}

	private void addProjectFilter(ReferentialData referentialData, UserDto currentUser) {
		ProjectFilterDto projectFilter = projectFilterDao.getProjectFilterByUserLogin(currentUser.getUsername());

		if (Objects.nonNull(projectFilter)) {
			referentialData.setProjectFilterStatus(projectFilter.getActivated());
			List<Long> projectsIds = projectFilterDao.getProjectIdsByProjectFilter(projectFilter.getId());
			referentialData.setFilteredProjectIds(projectsIds);
		}
	}

	private void appendLicenseInformation(AdminReferentialData referentialData) {
		LicenseInformationDto licenseInfo = new LicenseInformationDto();
		licenseInfo.setPluginLicenseExpiration(configurationService.findConfiguration(PLUGIN_LICENSE_EXPIRATION));
		licenseInfo.setActivatedUserExcess(configurationService.findConfiguration(ACTIVATED_USER_EXCESS));
		referentialData.setLicenseInformation(licenseInfo);
	}

	private void appendAvailableTestAutomationServerKinds(AdminReferentialData referentialData) {
		referentialData.setAvailableTestAutomationServerKinds(testAutomationServerDisplayDao.findAllAvailableKinds());
	}

	private void appendTemplateConfigurablePlugins(AdminReferentialData referentialData) {
		referentialData.setTemplateConfigurablePlugins(templatePlugins.stream()
				.map(TemplateConfigurablePluginDto::fromTemplateConfigurablePlugin)
				.collect(Collectors.toList()));
	}
}
