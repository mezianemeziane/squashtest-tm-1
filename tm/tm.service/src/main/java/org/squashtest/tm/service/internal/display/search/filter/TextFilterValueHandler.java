/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search.filter;

import com.google.common.collect.Sets;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.query.QueryColumnPrototypeReference;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class TextFilterValueHandler implements FilterValueHandler {

	private final Set<String> handledPrototypes = Sets.newHashSet(
		QueryColumnPrototypeReference.TEST_CASE_NAME,
		QueryColumnPrototypeReference.TEST_CASE_REFERENCE,
		QueryColumnPrototypeReference.TEST_CASE_CUF_TEXT,
		QueryColumnPrototypeReference.REQUIREMENT_VERSION_NAME,
		QueryColumnPrototypeReference.REQUIREMENT_VERSION_REFERENCE,
		QueryColumnPrototypeReference.REQUIREMENT_VERSION_CUF_TEXT
		);

	@Override
	public boolean canHandleGridFilterValue(GridFilterValue filter) {
		return handledPrototypes.contains(filter.getColumnPrototype());
	}

	@Override
	public void handleGridFilterValue(GridFilterValue filter) {
			List<String> value = filter.getValues();
			List<String> newValues = value.stream().map(this::formatValue).collect(Collectors.toList());
			filter.setValues(newValues);

	}

	private String formatValue(String value) {
		String newValue = "%" + value + "%";
		return newValue.replace(" ", "%");
	}
}
