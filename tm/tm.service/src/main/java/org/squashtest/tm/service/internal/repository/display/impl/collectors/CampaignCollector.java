/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl.collectors;

import com.google.common.base.Strings;
import com.google.common.collect.Multimap;
import org.jooq.DSLContext;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TreeNodeCollector;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.count;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;

@Component
public class CampaignCollector extends AbstractTreeNodeCollector implements TreeNodeCollector {

	public CampaignCollector(DSLContext dsl, CustomFieldValueDisplayDao customFieldValueDisplayDao, ActiveMilestoneHolder activeMilestoneHolder, MilestoneDisplayDao milestoneDisplayDao) {
		super(dsl, customFieldValueDisplayDao, activeMilestoneHolder, milestoneDisplayDao);
	}

	@Override
	protected Map<Long, DataRow> doCollect(List<Long> ids) {
		Map<Long, DataRow> campaigns = collectCampaigns(ids);
		appendMilestonesInCampaigns(campaigns);
		return campaigns;
	}

	private Map<Long, DataRow> collectCampaigns(List<Long> ids) {
		return dsl.select(
			// @formatter:off
			CAMPAIGN_LIBRARY_NODE.CLN_ID,
			CAMPAIGN_LIBRARY_NODE.NAME.as(NAME_ALIAS),
			CAMPAIGN.REFERENCE,
			CAMPAIGN_LIBRARY_NODE.PROJECT_ID.as(PROJECT_ID_ALIAS),
			count(CAMPAIGN_ITERATION.CAMPAIGN_ID).as(CHILD_COUNT_ALIAS))
			.from(CAMPAIGN_LIBRARY_NODE).innerJoin(CAMPAIGN).on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(CAMPAIGN.CLN_ID))
				.leftJoin(CAMPAIGN_ITERATION).on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(CAMPAIGN_ITERATION.CAMPAIGN_ID))
			.where(CAMPAIGN_LIBRARY_NODE.CLN_ID.in(ids))
				.groupBy(CAMPAIGN_LIBRARY_NODE.CLN_ID, CAMPAIGN_LIBRARY_NODE.NAME, CAMPAIGN.CLN_ID)
			.fetch().stream()
			// @formatter:on
			.collect(Collectors.toMap(
				tuple -> tuple.get(CAMPAIGN_LIBRARY_NODE.CLN_ID),
				tuple -> {
					DataRow dataRow = new DataRow();
					dataRow.setId(new NodeReference(NodeType.CAMPAIGN, tuple.get(CAMPAIGN_LIBRARY_NODE.CLN_ID)).toNodeId());
					dataRow.setProjectId(tuple.get(PROJECT_ID_ALIAS, Long.class));
					dataRow.setState(tuple.get(CHILD_COUNT_ALIAS, Integer.class) > 0 ? DataRow.State.closed : DataRow.State.leaf);
					dataRow.setData(tuple.intoMap());

					if (!Strings.isNullOrEmpty(tuple.get(CAMPAIGN.REFERENCE, String.class))) {
						dataRow.getData().replace(NAME_ALIAS, tuple.get(CAMPAIGN.REFERENCE) + " - " + tuple.get(CAMPAIGN_LIBRARY_NODE.NAME));
					}

					return dataRow;
				})
			);
	}

	private void appendMilestonesInCampaigns(Map<Long, DataRow> campaigns) {
		Multimap<Long, MilestoneDto> milestoneByCampaignId = this.milestoneDisplayDao.findMilestonesByCampaignId(campaigns.keySet());
		campaigns.forEach((campaignId, dataRow) -> dataRow.addData(MILESTONES_ALIAS, milestoneByCampaignId.get(campaignId).stream().map(MilestoneDto::getId).collect(Collectors.toList())));
	}

	@Override
	public NodeType getHandledEntityType() {
		return NodeType.CAMPAIGN;
	}
}
