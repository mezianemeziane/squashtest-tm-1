/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display;

import org.squashtest.tm.service.internal.display.dto.AutomationRequestDto;

import java.util.List;

public interface AutomationRequestDisplayDao {

	AutomationRequestDto findByTestCaseId(Long testCaseId);

	int countByAssignedTo(List<Long> readableProjectIds, Long userId);

	int countTransmitted(List<Long> readableProjectIds);

	int countAll(List<Long> readableProjectIds);

	List<String> getTcLastModifiedByForCurrentUser(List<Long> projectIds, List<String> requestStatus, Long userId);

	List<String> getTcLastModifiedByToAutomationRequestNotAssigned(List<Long> projectIds, List<String> requestStatus);

	List<String> getTcLastModifiedByForAutomationRequests(List<Long> projectIds, List<String> requestStatus);

	List<String> getAssignedUserForAutomationRequests(List<Long> projectIds);
}
