/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.project;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.project.AdministrableProject;
import org.squashtest.tm.domain.users.PartyProjectPermissionsBean;
import org.squashtest.tm.exception.library.CannotDeleteProjectException;
import org.squashtest.tm.service.display.project.ProjectDisplayService;
import org.squashtest.tm.service.display.scm.server.ScmServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.*;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.administration.ProjectGrid;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.project.ProjectDeletionHandler;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.TeamDao;
import org.squashtest.tm.service.internal.repository.display.*;
import org.squashtest.tm.service.milestone.MilestoneBindingManagerService;
import org.squashtest.tm.service.project.*;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.templateplugin.TemplateConfigurablePluginBindingService;
import org.squashtest.tm.service.testautomation.TestAutomationProjectManagerService;
import org.squashtest.tm.service.user.UserAccountService;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN_OR_PROJECT_MANAGER;

@Service
@Transactional
public class ProjectDisplayServiceImpl implements ProjectDisplayService {

	private static final String ROLE_ADMIN = "ROLE_ADMIN";
	private static final String ADMIN_OR_PROJECT_MANAGER_ON_CURRENT_PROJECT = HAS_ROLE_ADMIN +
		" or hasPermission(#projectId, 'org.squashtest.tm.domain.project.Project', 'MANAGEMENT')" +
		" or hasPermission(#projectId, 'org.squashtest.tm.domain.project.ProjectTemplate' , 'MANAGEMENT')";

	private final ProjectDisplayDao projectDisplayDao;
	private final ProjectDeletionHandler projectDeletionHandler;
	private final AttachmentDisplayDao attachmentDisplayDao;
	private final DSLContext dsl;
	private final PermissionEvaluationService permissionEvaluationService;
	private final ProjectDao projectDao;
	private final TeamDao teamDao;
	private final UserAccountService userAccountService;
	private final ProjectTemplateManagerService projectTemplateManagerService;
	private final BugTrackerDisplayDao bugTrackerDisplayDao;
	private final GenericProjectFinder projectFinder;
	private final GenericProjectManagerService projectManager;
	private final InfoListDisplayDao infoListDisplayDao;
	private final CustomFieldDao customFieldDao;
	private final ProjectsPermissionManagementService projectsPermissionManagementService;
	private final ScmServerDisplayService scmServerDisplayService;
	private final TestAutomationServerDisplayDao testAutomationServerDisplayDao;
	private final TestAutomationProjectManagerService testAutomationProjectManagerService;
	private final MilestoneDisplayDao milestoneDisplayDao;
	private final MilestoneBindingManagerService milestoneBindingManagerService;
	private final TemplateConfigurablePluginBindingService templateConfigurablePluginBindingService;


	@Inject
	ProjectDisplayServiceImpl(DSLContext dsl,
							  PermissionEvaluationService permissionEvaluationService,
							  ProjectDao projectDao,
							  TeamDao teamDao,
							  UserAccountService userAccountService,
							  ProjectTemplateManagerService projectTemplateManagerService,
							  ProjectDisplayDao projectDisplayDao,
							  AttachmentDisplayDao attachmentDisplayDao,
							  ProjectDeletionHandler projectDeletionHandler,
							  BugTrackerDisplayDao bugTrackerDisplayDao,
							  GenericProjectFinder projectFinder,
							  GenericProjectManagerService projectManager,
							  InfoListDisplayDao infoListDisplayDao,
							  CustomFieldDao customFieldDao,
							  ProjectsPermissionManagementService projectsPermissionManagementService,
							  ScmServerDisplayService scmServerDisplayService,
							  TestAutomationServerDisplayDao testAutomationServerDisplayDao,
							  MilestoneDisplayDao milestoneDisplayDao,
							  MilestoneBindingManagerService milestoneBindingManagerService,
							  TestAutomationProjectManagerService testAutomationProjectManagerService, TemplateConfigurablePluginBindingService templateConfigurablePluginBindingService) {
		this.dsl = dsl;
		this.permissionEvaluationService = permissionEvaluationService;
		this.projectDao = projectDao;
		this.teamDao = teamDao;
		this.userAccountService = userAccountService;
		this.projectTemplateManagerService = projectTemplateManagerService;
		this.projectDisplayDao = projectDisplayDao;
		this.attachmentDisplayDao = attachmentDisplayDao;
		this.projectDeletionHandler = projectDeletionHandler;
		this.bugTrackerDisplayDao = bugTrackerDisplayDao;
		this.projectFinder = projectFinder;
		this.projectManager =  projectManager;
		this.infoListDisplayDao = infoListDisplayDao;
		this.customFieldDao = customFieldDao;
		this.projectsPermissionManagementService = projectsPermissionManagementService;
		this.scmServerDisplayService = scmServerDisplayService;
		this.testAutomationServerDisplayDao = testAutomationServerDisplayDao;
		this.milestoneDisplayDao = milestoneDisplayDao;
		this.milestoneBindingManagerService = milestoneBindingManagerService;
		this.testAutomationProjectManagerService = testAutomationProjectManagerService;
		this.templateConfigurablePluginBindingService = templateConfigurablePluginBindingService;
	}

	@PreAuthorize(HAS_ROLE_ADMIN_OR_PROJECT_MANAGER)
	@Override
	public GridResponse findAll(GridRequest request) {
		List<Long> projectIds;
		if (!permissionEvaluationService.hasRole(ROLE_ADMIN)) {
			UserDto currentUser = userAccountService.findCurrentUserDto();
			Long managerId = currentUser.getUserId();

			List<Long> partyIds = teamDao.findTeamIds(managerId);
			partyIds.add(managerId);
			projectIds = projectDao.findAllManagedProjectIds(partyIds);

		} else {
			projectIds = projectDao.findAllProjectAndTemplateIds();
		}
		ProjectGrid projectGrid = new ProjectGrid(projectIds);
		return projectGrid.getRows(request, dsl);
	}

	@PreAuthorize(HAS_ROLE_ADMIN_OR_PROJECT_MANAGER)
	@Override
	public List<NamedReference> getTemplateNamedReferences() {
		return projectTemplateManagerService.findAllReferences();
	}

	@PreAuthorize(ADMIN_OR_PROJECT_MANAGER_ON_CURRENT_PROJECT)
	@Override
	public boolean hasProjectData(long projectId) {
		boolean hasData = false;

		try {
			projectDeletionHandler.checkProjectContainsOnlyFolders(projectId);
			projectDeletionHandler.checkProjectHasActivePlugin(projectId);
		} catch (CannotDeleteProjectException e) {
			hasData = true;
		}

		return hasData;
	}

	@PreAuthorize(ADMIN_OR_PROJECT_MANAGER_ON_CURRENT_PROJECT)
	@Override
	public ProjectViewDto getProjectView(long projectId) {
		AdministrableProject adminProject = projectFinder.findAdministrableProjectById(projectId);
		ProjectViewDto project = projectDisplayDao.getProjectOrTemplateById(projectId);

		project.setAttachmentList(attachmentDisplayDao.findAttachmentListById(project.getAttachmentListId()));
		bugTrackerDisplayDao.appendBugTrackerBindings(Collections.singletonList(project));
		project.setHasData(hasProjectData(projectId));
		customFieldDao.appendCustomFieldBindings(Collections.singletonList(project));
		project.setPartyProjectPermissions(getPartyProjectPermissions(projectId));
		project.setAvailableScmServers(scmServerDisplayService.getAllServersAndRepositories());
		project.setAvailableTestAutomationServers(testAutomationServerDisplayDao.findAll());
		project.setBoundTestAutomationProjects(testAutomationProjectManagerService.findAllByTMProject(projectId));
		project.setAvailableBugtrackers(bugTrackerDisplayDao.findAllBugtrackerBindableToProject());
		project.setInfoLists(infoListDisplayDao.findAllWithItems());
		milestoneDisplayDao.appendBoundMilestoneInformation(Collections.singletonList(project));
		appendBugtrackerProjectNames(adminProject, project);
		appendAllowedStatuses(adminProject, project);
		appendStatusesInUse(projectId, project);
		project.setAllowTcModifDuringExec(adminProject.allowTcModifDuringExec());
		project.setHasTemplateConfigurablePluginBinding(templateConfigurablePluginBindingService.isProjectConfigurationBoundToTemplate(projectId));

		if (project.isTemplate()) {
			final boolean isLinkedToProjects = !projectDisplayDao.getProjectsLinkedToTemplate(projectId).isEmpty();
			project.setTemplateLinkedToProjects(isLinkedToProjects);
		}

		return project;
	}

	@PreAuthorize(ADMIN_OR_PROJECT_MANAGER_ON_CURRENT_PROJECT)
	@Override
	public List<PartyProjectPermissionDto> getPartyProjectPermissions(Long projectId) {
		List<PartyProjectPermissionsBean> permissionsBeans = projectsPermissionManagementService.findPartyPermissionsBeanByProject(projectId);

		return permissionsBeans.stream().map(partyProjectPermissionsBean -> {
			PartyProjectPermissionDto dto = new PartyProjectPermissionDto();
			dto.setProjectId(projectId);
			dto.setPermissionGroup(partyProjectPermissionsBean.getPermissionGroup());
			dto.setPartyName(partyProjectPermissionsBean.getParty().getName());
			dto.setTeam("TEAM".equals(partyProjectPermissionsBean.getParty().getType()));
			dto.setPartyId(partyProjectPermissionsBean.getParty().getId());
			return dto;
		}).collect(Collectors.toList());
	}

	@PreAuthorize(ADMIN_OR_PROJECT_MANAGER_ON_CURRENT_PROJECT)
	@Override
	public BindMilestoneToProjectDialogData findAvailableMilestones(long projectId) {
		BindMilestoneToProjectDialogData dialogData = new BindMilestoneToProjectDialogData();
		dialogData.setGlobalMilestones(appendFilteredMilestonesListAccordingType(projectId, "global"));
		dialogData.setPersonalMilestones(appendFilteredMilestonesListAccordingType(projectId, "personal"));
		dialogData.setOtherMilestones(appendFilteredMilestonesListAccordingType(projectId, "other"));
		return dialogData;
	}

	private void appendBugtrackerProjectNames(AdministrableProject adminProject, ProjectViewDto project) {
		if (adminProject.getProject().getBugtrackerBinding() != null) {
			project.setBugtrackerProjectNames(
				adminProject
					.getProject()
					.getBugtrackerBinding()
					.getProjectNames()
			);
		}
	}

	private void appendAllowedStatuses(AdministrableProject adminProject, ProjectViewDto project) {
		CampaignLibrary cl = adminProject.getCampaignLibrary();
		Map<String, Boolean> allowedStatuses = new HashMap<>();
		allowedStatuses.put(ExecutionStatus.SETTLED.toString(), cl.allowsStatus(ExecutionStatus.SETTLED));
		allowedStatuses.put(ExecutionStatus.UNTESTABLE.toString(), cl.allowsStatus(ExecutionStatus.UNTESTABLE));
		project.setAllowedStatuses(allowedStatuses);
	}

	private void appendStatusesInUse(long projectId, ProjectViewDto project) {
		Map<String, Boolean> statusesInUse = new HashMap<>();
		boolean settledIsUsed = projectManager.projectUsesExecutionStatus(projectId, ExecutionStatus.valueOf(ExecutionStatus.SETTLED.toString()));
		boolean untestableIsUsed = projectManager.projectUsesExecutionStatus(projectId, ExecutionStatus.valueOf(ExecutionStatus.UNTESTABLE.toString()));
		statusesInUse.put(ExecutionStatus.SETTLED.toString(), settledIsUsed);
		statusesInUse.put(ExecutionStatus.UNTESTABLE.toString(),untestableIsUsed );
		project.setStatusesInUse(statusesInUse);
	}

	private List<MilestoneDto> appendFilteredMilestonesListAccordingType(long projectId, String type) {
		List<Milestone> filteredMilestones = milestoneBindingManagerService.getAllBindableMilestoneForProject(projectId, type);
		List<MilestoneDto>  filteredMilestonesDto= new ArrayList<>();
		filteredMilestones.forEach(milestone -> {
			MilestoneDto milestoneDto = new MilestoneDto();
			milestoneDto.setId(milestone.getId());
			milestoneDto.setLabel(milestone.getLabel());
			milestoneDto.setDescription(milestone.getDescription());
			milestoneDto.setRange(milestone.getRange().name());
			milestoneDto.setEndDate(milestone.getEndDate());
			milestoneDto.setStatus(milestone.getStatus().name());
			milestoneDto.setOwnerFirstName(milestone.getOwner().getFirstName());
			milestoneDto.setOwnerLastName(milestone.getOwner().getLastName());
			milestoneDto.setOwnerLogin(milestone.getOwner().getLogin());
			filteredMilestonesDto.add(milestoneDto);
		});
		return filteredMilestonesDto;
	}


}
