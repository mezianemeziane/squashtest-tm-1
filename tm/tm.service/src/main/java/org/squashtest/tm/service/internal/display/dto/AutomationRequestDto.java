/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.squashtest.tm.domain.tf.automationrequest.AutomationRequest;

import java.util.Date;

public class AutomationRequestDto {

	private Long id;
	private Integer priority;
	private String requestStatus;
	private Date transmittedOn;
	private RemoteAutomationRequestExtenderDto extender;

	public AutomationRequestDto() {
	}
	public AutomationRequestDto(AutomationRequest automationRequest) {
		this.id = automationRequest.getId();
		this.priority = automationRequest.getAutomationPriority();
		this.requestStatus = automationRequest.getRequestStatus().name();
		this.transmittedOn = automationRequest.getTransmissionDate();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public Date getTransmittedOn() {
		return transmittedOn;
	}

	public void setTransmittedOn(Date transmittedOn) {
		this.transmittedOn = transmittedOn;
	}

	public RemoteAutomationRequestExtenderDto getExtender() {
		return extender;
	}

	public void setExtender(RemoteAutomationRequestExtenderDto extender) {
		this.extender = extender;
	}
}
