/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain

import org.squashtest.tm.domain.Identified
import org.squashtest.tm.domain.IdentifiedComparator
import spock.lang.Specification
import spock.lang.Unroll

/**
 * @author JThebault
 */
class NodeReferenceTest extends Specification {
	@Unroll("#idAsString is parsed as #expectedType with id #expectedId")
	def "parse node reference from string"() {
		expect:
		NodeReference.fromNodeId(idAsString).id == expectedId
		NodeReference.fromNodeId(idAsString).nodeType == expectedType

		where:
		idAsString            || expectedType               | expectedId
		"TestCaseLibrary-1"   || NodeType.TEST_CASE_LIBRARY | 1
		"Test-Case-Library-1" || NodeType.TEST_CASE_LIBRARY | 1
		"TestCase-12"         || NodeType.TEST_CASE         | 12

	}

	@Unroll("#idAsString is rejected")
	def "should reject invalid node reference from string"() {
		when:
		NodeReference.fromNodeId(idAsString)

		then:
		thrown(IllegalArgumentException)

		where:
		idAsString << [
			"",
			"   ",
			"noop-12",
			"TestCaseFolder-",
			"-12"
		]

	}

}


