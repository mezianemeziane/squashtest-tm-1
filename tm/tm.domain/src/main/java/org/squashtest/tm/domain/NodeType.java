/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain;

import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.domain.actionword.ActionWordLibrary;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.domain.campaign.*;
import org.squashtest.tm.domain.chart.ChartDefinition;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customreport.*;
import org.squashtest.tm.domain.report.ReportDefinition;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.testcase.*;

import java.util.EnumSet;
import java.util.Objects;
import java.util.Optional;

/**
 * Enum used to declare the types of nodes in Squash TM trees. Note that the vast majority correspond to classic LibraryNode Hibernate entities but
 * we could also declare some nodes that are representation of other data... coverage, executions... a lot of things could be represented as trees
 */
public enum NodeType {

	// @formatter:off
	TEST_CASE_LIBRARY(TestCaseLibrary.class.getSimpleName(),null, null),
	TEST_CASE_FOLDER(TestCaseFolder.class.getSimpleName(),BindableEntity.TESTCASE_FOLDER, TEST_CASE_LIBRARY),
	TEST_CASE(TestCase.class.getSimpleName(),BindableEntity.TEST_CASE, TEST_CASE_LIBRARY),
	REQUIREMENT_LIBRARY(RequirementLibrary.class.getSimpleName(),null, null),
	REQUIREMENT_FOLDER(RequirementFolder.class.getSimpleName(),BindableEntity.REQUIREMENT_FOLDER, REQUIREMENT_LIBRARY),
	REQUIREMENT(Requirement.class.getSimpleName(),BindableEntity.REQUIREMENT_VERSION, REQUIREMENT_LIBRARY),
	CAMPAIGN_LIBRARY(CampaignLibrary.class.getSimpleName(),null, null),
	CAMPAIGN_FOLDER(CampaignFolder.class.getSimpleName(),BindableEntity.CAMPAIGN_FOLDER, CAMPAIGN_LIBRARY),
	CAMPAIGN(Campaign.class.getSimpleName(),BindableEntity.CAMPAIGN, CAMPAIGN_LIBRARY),
	ITERATION(Iteration.class.getSimpleName(),BindableEntity.ITERATION, CAMPAIGN_LIBRARY),
	TEST_SUITE(TestSuite.class.getSimpleName(),BindableEntity.TEST_SUITE, CAMPAIGN_LIBRARY),
	CUSTOM_REPORT_LIBRARY(CustomReportLibrary.class.getSimpleName(),null, null),
	CUSTOM_REPORT_FOLDER(CustomReportFolder.class.getSimpleName(),null, CUSTOM_REPORT_LIBRARY),
	CHART_DEFINITION(ChartDefinition.class.getSimpleName(),null, CUSTOM_REPORT_LIBRARY),
	CUSTOM_REPORT_CUSTOM_EXPORT(CustomReportCustomExport.class.getSimpleName(),null, CUSTOM_REPORT_LIBRARY),
	CUSTOM_REPORT_DASHBOARD(CustomReportDashboard.class.getSimpleName(),null, CUSTOM_REPORT_LIBRARY),
	REPORT_DEFINITION(ReportDefinition.class.getSimpleName(),null, CUSTOM_REPORT_LIBRARY),
	TEST_STEP(TestStep.class.getSimpleName(), BindableEntity.TEST_STEP, TEST_CASE_LIBRARY),
	ACTION_WORD_LIBRARY(ActionWordLibrary.class.getSimpleName(), null, null),
	ACTION_WORD(ActionWord.class.getSimpleName(), null, ACTION_WORD_LIBRARY);
	// @formatter:on


	NodeType(String typeName, BindableEntity bindableEntity, NodeType library) {
		this.typeName = typeName;
		this.bindableEntity = bindableEntity;
		this.library = library;
	}

	private String typeName;
	private BindableEntity bindableEntity;
	private NodeType library;

	public String getTypeName() {
		return typeName;
	}

	public BindableEntity getBindableEntity() {
		return bindableEntity;
	}

	public boolean isCufHolder() {
		return Objects.nonNull(this.bindableEntity);
	}

	public static NodeType fromTypeName(String typeName) {
		if (StringUtils.isBlank(typeName)) {
			throw new IllegalArgumentException("TypeName can't be empty");
		}

		EnumSet<NodeType> entityTypes = EnumSet.allOf(NodeType.class);

		Optional<NodeType> type = entityTypes.stream()
			.filter(entityType -> entityType.getTypeName().equals(typeName))
			.findFirst();

		if (type.isPresent()) {
			return type.get();
		}

		throw new IllegalArgumentException("No Node Type for type name : " + typeName);
	}


	public NodeType getLibraryType() {
		return library;
	}

	public boolean isLibrary() {
		return Objects.isNull(this.library);
	}
}
