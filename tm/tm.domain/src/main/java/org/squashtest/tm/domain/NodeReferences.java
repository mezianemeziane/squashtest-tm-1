/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NodeReferences {

	private Set<NodeReference> nodeReferences;

	public NodeReferences(Set<NodeReference> nodeReferences) {
		this.nodeReferences = nodeReferences;
	}
	public NodeReferences(List<NodeReference> nodeReferences) {
		this(new HashSet<>(nodeReferences));
	}

	public static NodeReferences fromStrings (List<String> nodeReferences) {
		Set<NodeReference> references = NodeReference.fromNodeIds(nodeReferences);
		return new NodeReferences(references);
	}

	public void removeLibraries() {
		this.nodeReferences = getNonLibraryStream().collect(Collectors.toSet());
	}

	public Set<NodeReference> extractLibraries() {
		return getLibraryStream().collect(Collectors.toSet());
	}

	public Set<NodeReference> extractNonLibraries() {
		return getNonLibraryStream().collect(Collectors.toSet());
	}

	public Set<Long> extractLibraryIds() {
		return getLibraryStream().collect(toIdSet());
	}

	public Set<Long> extractNonLibraryIds() {
		return getNonLibraryStream().collect(toIdSet());
	}

	public Set<NodeReference> asSet() {
		return this.nodeReferences;
	}

	private Stream<NodeReference> asStream() {
		return nodeReferences.stream();
	}

	private Stream<NodeReference> getLibraryStream() {
		return asStream().filter(nodeReference -> nodeReference.getNodeType().isLibrary());
	}

	private Stream<NodeReference> getNonLibraryStream() {
		return asStream().filter(nodeReference -> !nodeReference.getNodeType().isLibrary());
	}

	private static Collector<NodeReference, Set<Long>, Set<Long>> toIdSet() {
		return Collector.of(
			HashSet::new,
			(ids, nodeReference) -> ids.add(nodeReference.getId()),
			(a, b) -> {
				throw new RuntimeException("Not concurrent collector");
			}
		);
	}

	public boolean isEmpty() {
		return this.nodeReferences.isEmpty();
	}
}
