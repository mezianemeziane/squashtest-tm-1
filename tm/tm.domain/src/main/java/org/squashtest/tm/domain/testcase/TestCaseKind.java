/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.testcase;

import org.squashtest.tm.core.foundation.i18n.Internationalizable;

import java.util.Objects;

public enum TestCaseKind implements Internationalizable {
	STANDARD, GHERKIN, KEYWORD;

	public boolean isScripted() {
		return this.equals(GHERKIN);
	}

	@Override
	public String getI18nKey() {
		return "test-case.format." + this.name().toLowerCase();
	}

	public static TestCaseKind fromLeftJoinIds(Long scriptedTestCaseId, Long keywordTestCaseId){
		boolean isGherkin = Objects.nonNull(scriptedTestCaseId);
		boolean isKeyword = Objects.nonNull(keywordTestCaseId);

		if (isGherkin) {
			return TestCaseKind.GHERKIN;
		}

		if (isKeyword) {
			return TestCaseKind.KEYWORD;
		}

		return TestCaseKind.STANDARD;
	}

	public static TestCaseKind fromTestCase(TestCase testCase){
		final TestCaseKind[] testCaseKind = new TestCaseKind[1];
		TestCaseVisitor testCaseVisitor = new TestCaseVisitor() {
			@Override
			public void visit(TestCase testCase) {
				testCaseKind[0] = TestCaseKind.STANDARD;
			}

			@Override
			public void visit(KeywordTestCase keywordTestCase) {
				testCaseKind[0] = TestCaseKind.KEYWORD;
			}

			@Override
			public void visit(ScriptedTestCase scriptedTestCase) {
				testCaseKind[0] = TestCaseKind.GHERKIN;
			}
		};
		testCase.accept(testCaseVisitor);
		return testCaseKind[0];
	}
}
