# Developing a plugin for the 2.0+ version

From v2.0, SquashTM is now an Angular SPA. It changes a lot of things in the way we develop plugins, mainly because the
main SquashTM App has now a reusable core that act as dependency for all plugins.

### Philosophy

As the plugin must not be known by the core, they are developed as standalone angular apps. They can be rendered
freely :

- Inside an Iframe.
- As independent pages accessible by clicking links in the core application. Navigation from sqtm-app into a plugin, or
  between plugins will reset the js context.
- One could probably use web-component to avoid iframe usage, but for now, iframes are used just for plugin
  configuration. So i don't see the need to go further...

Note that plugins front-ends are served by the same backend that main application, like it was the case in v1.X.

### Initialize the new front end app

###### Make git branching

- Create a maintenance branch for the 1.xx plugin, and create associated jenkins job on the forge
- Create a branch for the 2.0+. Migration to angular will have huge impact and can't be undone easily. I recommend
  making that stuff in another branch that will be merged into master branch once done.
- Bump the major version in pom.xml. If you can, try to match to 2.0.X, so the major version will be the same as
  Squash-tm. If 2.0.+ already exist, bump to 3.0 and so on... Avoid keeping 1.X plugin versions for SquashTM 2.0+, it's
  confusing...

```xml

<version>2.0.0.RC1-SNAPSHOT</version>
```

###### Check your global ng cli version

Do a ```ng version``` in shell/cmd. If global cli is not of desired angular version, update the global cli. It can fail
on windows, on that case you can also create the app and immediately do the upgrade.

###### Create the app

The plugin angular front end can be either :

- A dedicated maven submodule. All front end configuration files (package.json and consorts...) are located at the root
  of this module. It's the organisation of the tm-front module in the main app. You can create the app directly with the
  ng-cli. You don't need a complete ng workspace.
- Included in the main maven module for mono maven module plugin. In that case all front end configuration files (
  package.json and consorts...) are located at the root of the main module. Sources for the ng app are located in a
  sibling folder of java directory. See jirasync plugin for example. For app creation, I recommend to use ng-cli to
  create an empty workspace, from the parent folder of your plugin. Use option --new-project-root src/main/front-end to
  configure the project location in src/main/front-end folder. Try to target a sibling folder of java folder. Example
  from root folder of a given plugin :

  ```shell
  cd ..
  ng n <PLUGIN-DIRECTORY-NAME> --create-application false --new-project-root src/main/front-end --style less --package-manager yarn --skip-install true
  ```

  Once workspace is created, that you can initialize the app with generate-application command.
- The creation can fail due to already existing .gitignore fail. Just rename your existing .gitignore and manually merge
  the files after workspace creation.

- After creating the app, change the default change detection strategy in angular.json

```json
      {
        "projectType": "application",
        "schematics": {
          "@schematics/angular:component": {
            "style": "less",
            "changeDetection": "OnPush"
          }
        }
      }
```

- I recommend keeping the original plugin organisation. If it is a mono maven module, just keep it like that. A full
  refactoring is not required just to display a config page !

###### Building the app and serving resources

- Once the app is initialized, you must integrate the ng build to the global maven build. You must include the
  maven-front-plugin to your maven build and configure it. In pom.xml, add something like :
  ```xml
        <plugin>
          <groupId>com.github.eirslett</groupId>
          <artifactId>frontend-maven-plugin</artifactId>
          <version>1.6</version>
          <executions>
            <execution>
              <id>install node and yarn</id>
              <goals>
                <goal>install-node-and-yarn</goal>
              </goals>
              <configuration>
                <nodeVersion>v10.16.0</nodeVersion>
                <yarnVersion>v1.16.0</yarnVersion>
              </configuration>
            </execution>
  
            <execution>
              <id>yarn</id>
              <goals>
                <goal>yarn</goal>
              </goals>
              <configuration>
                <arguments>install --frozen-lockfile</arguments>
              </configuration>
            </execution>
  
            <execution>
              <id>yarn build</id>
              <goals>
                <goal>yarn</goal>
              </goals>
              <configuration>
                <arguments>build</arguments>
              </configuration>
            </execution>
  
          </executions>
        </plugin>
  ```  

- Add a .yarnrc file sibling of package.json to define the npm repository. We must use the corporate nexus.
    ```text
    registry "https://nexus.squashtest.org/nexus/repository/npm-group-registry/"
    ```
- Remove yarn.lock file if any. Do a fresh ``yarn install``.
- Add --prod flag to yarn build script in package.json :
  ```json
  {
  "build": "ng build --prod"
  }
  ```
- Adapt the maven license plugin to avoid checking license on node_module and other stuff.
- Do a ``` mvn clean install ```. A /dist folder should be created, containing the result of the build.
- To allow the app to be served by spring boot, the built app must be placed in META-INF/resources/plugin/<PLUGIN-ID> of
  the final jar. You are free to configure the app to be built directly here or use maven-resource-plugin to move it
  after ng-cli build is done. Example for maven-ressource-plugin solution, pom.xml :
  ```xml
  <!--  Copying resources from the ng app into the maven build directory   -->
        <plugin>
          <artifactId>maven-resources-plugin</artifactId>
          <executions>
            <execution>
              <id>prepare-package</id>
              <goals>
                <goal>copy-resources</goal>
              </goals>
              <phase>prepare-package</phase>
              <configuration>
                <outputDirectory>${basedir}/target/classes/META-INF/resources/plugin/jirasync</outputDirectory>
                <resources>
                  <resource>
                    <directory>${project.basedir}/dist/jirasync</directory>
                  </resource>
                </resources>
              </configuration>
            </execution>
          </executions>
        </plugin>
  ```  

- Rebuild the whole project, and check the content of the jar
  - Unzip the jar
  - It should contain the front-end in META-INF/resources/plugin/<PLUGIN-ID> folder. The built app should be inside.

###### Applying context-path

Squash-tm has a property to configure the root context path of the app. ``` server.servlet.context-path=/squash ```.
It's indeed the servlet context itself. To allow a dynamic configuration of this property at runtime, we need to tweak
the default index.html file generated by ng-cli. We specifically need to template it, by using server side templating
engine, aka Thymeleaf.

- Modify the index.html base tag in HEAD part of file. Change only the plugin name, do not modify anything else,
  especially the id.
  ```html
  <base href="/" data-base-backend="/squash/" th:href="@{/plugin/<PLUGIN-NAME>/}" th:attr="data-base-backend=@{/}" id="sqtm-app-base-href">
  ```
- Create a java IndexController that will be the default endpoint for serving the index.html of the application.
  Example :
  ```java
   @Controller
   public class PluginNameIndexController {

     private static final Logger LOGGER = LoggerFactory.getLogger(PluginNameIndexController.class);
  
     @GetMapping("/plugin/<PLUGIN_NAME>/index")
     public String index() {
       LOGGER.debug("Page request forwarded to <PLUGIN_NAME> index controller. Single page app will be served with current context path.");
       return "plugin/<PLUGIN_NAME>/index.html";
      }
  }
  ```
- Create a java spring configuration file to configure routes to the templated index.html. For each base route of your
  app, you must declare a view-controller pointing to IndexController. Note that the goal here is to serve the
  application under the right context path. It is not a real business backend controller. Example :
  ```java
  @Configuration
  public class PluginNameWebMvcConfig implements WebMvcConfigurer {
  
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
    registry.addViewController("/plugin/<PLUGIN_NAME>/configuration/**").setViewName("/plugin/<PLUGIN_NAME>/index");
    }
    
  }
  ```
- You should now be able to build/deploy your plugin and access the root page directly from the main app, by using a
  link or just by entering the url. Do not add front end routing at this stage, just try to have the application served by 
  squash-tm server.

###### Adding core dep and configure SqtmCoreModule

Now, we need to add sqtm-core npm package to our plugin. Starting with 2.0 version, the front is like the java backend,
it depends on squash-tm. However, the dep is handled by npm/yarn, not maven.

- Add the dep to sqtm-core in the plugin root package.json file. Take care of the version...
- Add the required peer dependencies as real dependencies of the plugin.
- yarn install.
- Configure the SqtmCoreModule, and required providers, in the plugin app-module.ts :

  ```typescript
  @NgModule({
    declarations: [
      AppComponent
    ],
    imports: [
      BrowserAnimationsModule,
      RouterModule.forRoot(routes),
      SqtmCoreModule.forRoot({
        backendRootUrl: 'backend/plugin/<PLUGIN-ID>',
        pluginIdentifier: '<A CONSTANT FOR IDENTIFYNG THE PLUGIN>',
        ngxTranslateFiles: ['./assets/i18n/translations_']
      }),
      SqtmDragAndDropModule,
      UiManagerModule,
    ],
    providers: [
      {
        provide: APP_BASE_HREF,
        useFactory: getBaseLocation
      },
      {
        provide: BACKEND_CONTEXT_PATH,
        useFactory: getBackendContext // <- In plugins APP_BASE_HREF !== BACKEND_CONTEXT_PATH
      },
      httpInterceptorProviders,
    ],
    bootstrap: [AppComponent]
  })
  export class AppModule {
  }
  ```

###### Assets

- The assets from sqtm-core are required to allow a proper display of components. Add a glob to asset section of
  angular.json. Add the proper assets of plugin in same section :

  ```json
  {
  "assets": [
              "src/main/front-end/jirasync/src/favicon.ico",
              "src/main/front-end/jirasync/src/assets",
              {
                "glob": "**/*",
                "input": "./node_modules/sqtm-core/assets",
                "output": "./assets"
              }
            ]
  }
  ```
- Create the plugin i18n bundle at the required place (./assets/i18n/translations_ by default). To ease the migration
  process I recommend using the same prefix for i18n keys that the prefix in the legacy properties files...
- The sqtm-core css files must be imported by the plugins. I recommend to use two files, the main .less and one
  additional .scss file. The two files must be declared into angular.json.
  - src/main/front-end/<PLUGIN_NAME>/src/styles.less (don't forget the cdk css or overlay won't work properly !):
  ```less
  @import "~sqtm-core/assets/sqtm-core/style/sqtm-core";
  @import '~@angular/cdk/overlay-prebuilt.css';
  ```
  - src/main/front-end/<PLUGIN_NAME>/src/styles.scss:
  ```scss
  @import "~sqtm-core/assets/sqtm-core/style/sqtm-core-generated";
  ```
  - angular.json:
  ```json
  {
  "styles": [
              "src/main/front-end/jirasync/src/styles.less",
              "src/main/front-end/jirasync/src/styles.scss"
            ]
  }
  ```
  - Make a full build and check that assets and css are in the dist folder.

###### Routing client side

Routing client side is classic lazy-loaded module. Don't add /plugin/<PLUGIN-NAME> the APP_BASE_PATH should handle that.
Example :

  ```typescript
    export const routes: Routes = [
  {
    path: 'configuration',
    loadChildren: () => import('./pages/config-page/config-page.module')
      .then(m => m.ConfigPageModule)
  }];
  ``` 

###### Routing server side

- The index controller should be the controller for all urls pointing to client side pages. So if a user try to access
  plugin by a deep link, the server will respond with the angular app, and the client routing will mount the proper
  route. So for each lazy-loaded route, a view controller must be declared in PluginWebMvcConfig.
   ```java
  @Configuration
  public class PluginNameWebMvcConfig implements WebMvcConfigurer {
  
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/plugin/<PLUGIN_NAME>/<LAZY-LOADED-ROUTE-1>/**").setViewName("/plugin/<PLUGIN_NAME>/index");
        registry.addViewController("/plugin/<PLUGIN_NAME>/<LAZY-LOADED-ROUTE-2>/**").setViewName("/plugin/<PLUGIN_NAME>/index");
    }
    
  }
  ```
- All business controllers must be prefixed by /backend/plugin/<plugin-name>. It's the same path that must be declared
  in SqtmCoreModule.forRoot() :
  - app.module.ts
  ```typescript
      SqtmCoreModule.forRoot({
        backendRootUrl: 'backend/plugin/<PLUGIN-ID>', // will handle all rest.service prefixs
        pluginIdentifier: '<A CONSTANT FOR IDENTIFYNG THE PLUGIN>', // for inter app navigation
        ngxTranslateFiles: ['./assets/i18n/translations_'] // the plugin i18n
      })
    ```
  - a controller for config page :
  ```java
    // always prefix ALL plugin spring beans to avoid name clashing with other plugins beans or SquashTM itself
    @RestController(value = "squash.tm.plugin.<plugin-name>.configurationController")
    // backend controller url is fully prefixed
    @RequestMapping("backend/plugin/<plugin-name>/configuration/project/{projectId}")
    public class ConfigurationController {}
  ```

  - A client side service calling this controller. The prefix is handled by rest.service and initial SqtmCoreModule
    configuration :
  ```typescript
  this.restService.get<ConfigPageModel>(['configuration/project', projectId.toString()]) // no prefixs
  ```


- Spring Security config is handled by the core
  - All url for pattern : /plugin/<plugin-name>/** doesn't require authentication.
  - All url for pattern : /backend/** require auth (except login/logout controller), including plugin backend. So all
    url like /backend/plugin/<plugin-name>/** are covered by SquashTM security config.

###### Developing a plugin with dev server

One can use the dev server to ease plugin front end developement. As plugin are standalone application, you can use the
dev server to serve your application. However, the backend is required for login, as auth is required for acceding
backend controller. The suggested workflow :

- add some options to the ng serve line in package.json :
  ```json
  {
    "start": "ng serve --proxy-config proxy-conf.json --deploy-url /squash/ --base-href /squash/"
  }
  ```

- add a proxy config file, proxy-conf.json, for redirecting backend request from dev server to real backend :
  ```json
  {
    "/squash/backend": {
      "target": "http://localhost:8080",
      "secure": false
    },
    "logLevel": "debug"
  }
  ```

- You can now login against localhost:8080/squash/login, in another browser tab. One authenticated, the session cookie
  is usable in any browser tab.

- Your plugin pages are at localhost:4200/squash/my-page. There is no need to use prefixs here as the app is server
  without /plugin/<plugin-name>

- As for legacy plugin, changing the backend java code require a full mvn install and copy the new jat or use a symlink.

- To ease development, you can define a no front maven profile to skip angular build. Il will allow you to recompile
  your backend faster, as you use angular dev server to serve the front end. Add a profile to your pom.xml :

```xml

<profile>
  <id>no-front</id>
  <build>
    <plugins>
      <plugin>
        <groupId>com.github.eirslett</groupId>
        <artifactId>frontend-maven-plugin</artifactId>
        <version>1.6</version>
        <executions>
          <execution>
            <id>install node and yarn</id>
            <phase>none</phase>
          </execution>
          <execution>
            <id>yarn</id>
            <phase>none</phase>
          </execution>
          <execution>
            <id>yarn build</id>
            <phase>none</phase>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
</profile>
```
