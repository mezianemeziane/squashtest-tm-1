import {ActionStepModel, TestStepModel} from './test-step.model';
import {TestCaseImportanceKeys} from '../level-enums/level-enum';
import {TestCaseParameterOperationReport} from '../../core/services/test-case/test-case.service';

export class PasteTestStepOperationReport {
  testSteps: TestStepModel[];
  testCaseImportance: TestCaseImportanceKeys;
}

export class AddTestStepOperationReport {
  testStep: TestStepModel;
  operationReport: TestCaseParameterOperationReport;
}

export class DeleteTestStepOperationReport {
  testStepsToDelete: number[];
  operationReport: TestCaseParameterOperationReport;
}
