import {TestCaseStatistics} from '../test-case-statistics.model';
import {EntityModel} from '../../../core/services/entity-view/entity-view.state';
import {CustomDashboardModel} from '../../custom-report/custom-dashboard.model';

export interface TestCaseFolderModel extends EntityModel {
  name: string;
  description: string;
  statistics: TestCaseStatistics;
  dashboard: CustomDashboardModel;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}
