export class CalledTestCase {
  id: number;
  projectName: string;
  reference: string;
  name: string;
  datasetName: string;
  stepOrder: number;
}
