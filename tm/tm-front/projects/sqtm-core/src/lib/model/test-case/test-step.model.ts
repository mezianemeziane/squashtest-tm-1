import {AttachmentListModel} from '../attachment/attachment-list.model';
import {CustomFieldValueModel} from '../customfield/custom-field-value.model';

export interface ParameterRename {
  testStepList: TestStepModel[];
  prerequisite: string;
}

export interface TestStepModel {
  id: number;
  projectId: number;
  stepOrder: number;
  kind: TestStepKind;
}

export interface ActionStepModel extends TestStepModel {
  kind: 'action-step';
  action: string;
  expectedResult: string;
  attachmentList: AttachmentListModel;
  customFieldValues: CustomFieldValueModel[];
}

export interface CallStepModel extends TestStepModel {
  kind: 'call-step';
  calledTcId: number;
  calledTcName: string;
  calledDatasetId: number;
  delegateParam: boolean;
  calledTestCaseSteps: TestStepModel[];
}

export interface KeywordStepModel extends TestStepModel {
  kind: 'keyword-step';
  keyword: string;
  actionWordId: number;
  actionWordProjectId: number;
  action: string;
  styledAction: string;
  datatable: string;
  docstring: string;
  comment: string;
}

// 'dnd-call-step-placeholder' are placeholders used during dnd.
// 'create-action-step-form' are only used when user add action steps.
export type TestStepKind = 'action-step' | 'call-step' | 'keyword-step' | 'dnd-call-step-placeholder' | 'create-action-step-form';
