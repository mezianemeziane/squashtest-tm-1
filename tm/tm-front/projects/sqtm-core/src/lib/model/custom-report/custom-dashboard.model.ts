import {ChartDefinitionModel} from './chart-definition.model';
import {WorkspaceKeys} from '../level-enums/level-enum';
import {ReportDefinitionModel} from './report-definition.model';

export interface CustomDashboardBinding {
  id: number;
  dashboardId: number;
  row: number;
  col: number;
  sizeX: number;
  sizeY: number;
}

export interface ChartBinding extends CustomDashboardBinding {
  chartDefinitionId: number;
  chartInstance: ChartDefinitionModel;
}

export interface ReportBinding extends CustomDashboardBinding {
  reportDefinitionId: number;
  reportInstance?: ReportDefinitionModel;
}

export function isChartBinding(binding: CustomDashboardBinding): binding is ChartBinding {
  return Boolean((binding as ChartBinding).chartDefinitionId);
}

export function isReportBinding(binding: CustomDashboardBinding): binding is ReportBinding {
  return Boolean((binding as ReportBinding).reportDefinitionId);
}

export interface CustomDashboardModel {
  id: number;
  projectId: number;
  customReportLibraryNodeId: number;
  name: string;
  createdBy: string;
  chartBindings: ChartBinding[];
  reportBindings: ReportBinding[];
  favoriteWorkspaces: WorkspaceKeys[];
}

export interface DroppedNewItemInDashboard {
  row: number;
  col: number;
  sizeX: number;
  sizeY: number;
  chartDefinitionNodeId?: number;
  reportDefinitionNodeId?: number;
}
