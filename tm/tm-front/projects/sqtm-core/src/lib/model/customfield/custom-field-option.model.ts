export class CustomFieldOption {
  cfId: number;
  label: string;
  position: number;
  colour: string;
  code: string;
}

export function isOptionCodePatternValid(code: string) {
  return ((!code.match(/\s+/)) && code
    .match(/^[A-Za-z0-9_]*$/));
}
