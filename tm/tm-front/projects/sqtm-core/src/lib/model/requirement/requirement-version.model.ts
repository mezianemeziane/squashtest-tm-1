import {Milestone} from '../milestone/milestone.model';
import {RequirementCriticalityKeys, RequirementStatusKeys} from '../level-enums/level-enum';
import {VerifyingTestCase} from './verifying-test-case';
import {RequirementVersionLink} from './requirement-version.link';
import {RequirementVersionStatsBundle} from './requirement-version-stats-bundle.model';
import {EntityModel} from '../../core/services/entity-view/entity-view.state';

export interface RequirementVersionModel extends EntityModel {
  name: string;
  reference: string;
  versionNumber: number;
  category: number;
  criticality: RequirementCriticalityKeys;
  status: RequirementStatusKeys;
  createdBy: string;
  createdOn: string;
  lastModifiedBy: string;
  lastModifiedOn: string;
  milestones: Milestone[];
  description: string;
  requirementId: number;
  hasExtender: boolean;
  bindableMilestones: Milestone[];
  verifyingTestCases: VerifyingTestCase[];
  requirementVersionLinks: RequirementVersionLink[];
  requirementStats: RequirementVersionStatsBundle;
  nbIssues: number;
  remoteReqUrl: string;
  remoteReqId: string;
  syncStatus: string;
  remoteReqPerimeterStatus: RemoteRequirementPerimeterStatus;
}

export type RemoteRequirementPerimeterStatus =
  'UNKNOWN' |
  'IN_CURRENT_PERIMETER' |
  'OUT_OF_CURRENT_PERIMETER'|
  'NOT_FOUND';

