export enum AuthenticationPolicy {
  USER = 'USER',
  APP_LEVEL = 'APP_LEVEL'
}

export enum AuthenticationProtocol {
  BASIC_AUTH = 'BASIC_AUTH',
  OAUTH_1A = 'OAUTH_1A',
  TOKEN_AUTH = 'TOKEN_AUTH',
}

export function isKnownAuthenticationProtocol(value: string): value is AuthenticationProtocol {
  return Object.values(AuthenticationProtocol).includes(value as any);
}
