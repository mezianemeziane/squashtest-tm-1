import {AuthenticationPolicy, AuthenticationProtocol} from '../third-party-server/authentication.model';
import {Credentials} from '../third-party-server/credentials.model';

export interface ScmServer {
  serverId: number;
  name: string;
  url: string;
  kind: string;
  committerMail: string;
  repositories: ScmRepository[];
}

export interface ScmRepository {
  scmRepositoryId: number;
  serverId: number;
  name: string;
  repositoryPath: string;
  workingFolderPath: string;
  workingBranch: string;
}

export interface AdminScmServer extends ScmServer {
  authPolicy: AuthenticationPolicy;
  authProtocol: AuthenticationProtocol;
  supportedAuthenticationProtocols: AuthenticationProtocol[];
  credentials?: Credentials;
}

export function buildScmRepositoryUrl(scmServer: ScmServer, scmRepo: ScmRepository): string {
  let url = scmServer.url;
  if (!url.endsWith('/')) {
    url = `${url}/`;
  }

  url = `${url}${scmRepo.name} (${scmRepo.workingBranch})`;
  return url;
}
