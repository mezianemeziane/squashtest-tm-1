import {InfoList} from '../infolist/infolist.model';
import {CustomField} from '../customfield/customfield.model';
import {BindableEntity} from '../bindable-entity.model';
import {RenderingLocation} from '../customfield/custom-field-binding.model';
import { Option } from '../option.model';
import {ProjectPermissions} from '../permissions/permissions.model';
import {BugTracker} from '../bugtracker/bug-tracker.model';
import {TestAutomationServer} from '../test-automation/test-automation-server.model';
import {MilestoneView} from '../milestone/milestone.model';
import {ExecutionStatusKeys} from '../level-enums/level-enum';

/**
 * ProjectData represent a project with his related information (infolist, bound cuf, authorization for current user...).
 * It's a kind of denormalized view of a project, much more simpler to use in component than the highly normalized ReferentialDataState.
 */
export class ProjectData {
  id: number;
  name: string;
  uri: string;
  label: string;
  testCaseNature: InfoList;
  testCaseType: InfoList;
  requirementCategory: InfoList;
  allowAutomationWorkflow: boolean;
  customFieldBinding: CustomFieldBindingDataByEntity;
  permissions: ProjectPermissions;
  bugTracker: BugTracker;
  milestones: MilestoneView[];
  taServer: TestAutomationServer;
  automationWorkflowType: string;
  disabledExecutionStatus: ExecutionStatusKeys[];
  bddScriptLanguage: string;
  keywords: Option[];
  allowTcModifDuringExec: boolean;
  activatedPlugins: ActivatedPluginsByWorkspace;
}

export type CustomFieldBindingDataByEntity = { [id in BindableEntity]: CustomFieldBindingData[] };

export type ActivatedPluginsByWorkspace = { [id in WorkspaceTypeForPlugins]: string[]};

export enum WorkspaceTypeForPlugins {
  TEST_CASE_WORKSPACE = 'TEST_CASE_WORKSPACE',
  REQUIREMENT_WORKSPACE= 'REQUIREMENT_WORKSPACE',
  CAMPAIGN_WORKSPACE = 'CAMPAIGN_WORKSPACE',
}

export function createEmptyBindingByEntity(): CustomFieldBindingDataByEntity {
  const bindingDataByEntity: Partial<CustomFieldBindingDataByEntity> = {};
  for (const bindableEntity of Object.keys(BindableEntity)) {
    bindingDataByEntity[bindableEntity] = [];
  }
  return bindingDataByEntity as CustomFieldBindingDataByEntity;
}

export function createEmptyActivatedPluginsByWorkspace(): ActivatedPluginsByWorkspace {
  const activatedPluginsByWorkspace: Partial<ActivatedPluginsByWorkspace> = {};
  for (const workspaceType of Object.keys(WorkspaceTypeForPlugins)) {
    activatedPluginsByWorkspace[workspaceType] = [];
  }
  return activatedPluginsByWorkspace as ActivatedPluginsByWorkspace;
}

export class CustomFieldBindingData {
  id: number;
  bindableEntity: BindableEntity;
  renderingLocations: RenderingLocation[];
  boundProjectId: number;
  position: number;
  customField: CustomField;
}

export interface ProjectDataMap {
  [projectId: number]: ProjectData;
}
