import {IterationTestPlanItem} from './iteration-test-plan.item';
import {TestPlanStatistics} from './test-plan-statistics';
import {SimpleUser} from '../user/user.model';
import {Milestone} from '../milestone/milestone.model';
import {EntityModel} from '../../core/services/entity-view/entity-view.state';
import {NamedReference} from '../named-reference';
import {TestCaseImportanceKeys} from '../level-enums/level-enum';
import {
  CampaignStatisticsBundle,
  ConclusivenessStatusCount,
  ExecutionStatusCount,
  ScheduledIteration
} from './campaign-model';
import {CustomDashboardModel} from '../custom-report/custom-dashboard.model';

export interface IterationModel extends EntityModel {
  name: string;
  reference: string;
  description: string;
  uuid: string;
  iterationStatus: string;
  createdOn: string;
  createdBy: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  testPlanStatistics: TestPlanStatistics;
  actualStartDate: Date;
  actualEndDate: Date;
  actualStartAuto: boolean;
  actualEndAuto: boolean;
  scheduledStartDate: Date;
  scheduledEndDate: Date;
  hasDatasets: boolean;
  executionStatusMap: Map<number, string>;
  itpi: IterationTestPlanItem[];
  nbIssues: number;
  users: SimpleUser[];
  milestones: Milestone[];
  testSuites: NamedReference[];
  iterationStatisticsBundle?: IterationStatisticsBundle;
  nbAutomatedSuites: number;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  dashboard?: CustomDashboardModel;
  nbTestPlanItems: number;
}

export interface IterationProgressionStatistics {
  errors: string[];
  scheduledIteration: ScheduledIteration;
  cumulativeExecutionsPerDate: [string, number][];
}

export interface TestSuiteTestInventoryStatistics {
  testsuiteName: string;
  scheduledStart: string;
  scheduledEnd: string;
  nbTotal: number;
  nbExecuted: number;
  pcProgress: number;
  nbToExecute: number;
  pcSuccess: number;
  pcFailure: number;
  pcPrevProgress: number;
  nbPrevToExecute: number;
  statusStatistics: ExecutionStatusCount;
  importanceStatistics: { [K in TestCaseImportanceKeys]: number };
}

export interface IterationStatisticsBundle {
  iterationProgressionStatistics: IterationProgressionStatistics;
  iterationTestCaseStatusStatistics: ExecutionStatusCount;
  iterationTestCaseSuccessRateStatistics: { conclusiveness: { [K in TestCaseImportanceKeys]: ConclusivenessStatusCount } };
  iterationNonExecutedTestCaseImportanceStatistics: { [K in TestCaseImportanceKeys]: number };
  testsuiteTestInventoryStatisticsList: TestSuiteTestInventoryStatistics[];
}
