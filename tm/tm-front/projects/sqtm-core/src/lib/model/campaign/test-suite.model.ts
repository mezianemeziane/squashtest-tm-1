import {TestPlanStatistics} from './test-plan-statistics';
import {Milestone} from '../milestone/milestone.model';
import {EntityModel} from '../../core/services/entity-view/entity-view.state';
import {SimpleUser} from '../user/user.model';

export interface TestSuiteModel extends EntityModel {
  name: string;
  description: string;
  uuid: string;
  executionStatus: string;
  createdOn: string;
  createdBy: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  testPlanStatistics: TestPlanStatistics;
  nbIssues: number;
  hasDatasets: boolean;
  executionStatusMap: Map<number, string>;
  users: SimpleUser[];
  milestones: Milestone[];
  nbAutomatedSuites: number;
  nbTestPlanItems: number;
}
