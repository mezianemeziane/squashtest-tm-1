export interface TAUsageStatus {
  hasBoundProject: boolean;
  hasExecutedTests: boolean;
}

export interface TestAutomationProject {
  taProjectId: number;
  tmProjectId: number;
  remoteName: string;
  label: string;
  serverId: number;
  executionEnvironments: string;
  canRunBdd: boolean;
}
