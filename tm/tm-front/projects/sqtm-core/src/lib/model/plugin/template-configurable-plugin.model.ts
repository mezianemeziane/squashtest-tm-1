// Model for a template configurable plugin in referential data. Maps to TemplateConfigurablePlugin server-side.
export interface TemplateConfigurablePlugin {
  id: string;
  type: string;
  name: string;
}
