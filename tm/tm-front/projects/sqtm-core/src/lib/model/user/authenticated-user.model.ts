export interface AuthenticatedUser {
  userId: number;
  username: string;
  admin: boolean;
  projectManager: boolean;
  functionalTester: boolean;
  automationProgrammer: boolean;
  firstName: string;
  lastName: string;
}

export enum UserRoles {
  ROLE_ADMIN = 'ROLE_ADMIN',
  ROLE_TM_PROJECT_MANAGER = 'ROLE_TM_PROJECT_MANAGER',
  ROLE_USER = 'ROLE_USER'
}
