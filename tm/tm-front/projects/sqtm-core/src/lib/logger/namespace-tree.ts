import {LoggingLevel, NamespaceConfiguration, UnsetLoggingLevel} from './logger.configuration';

export class NamespaceTree {
  private root: NamespaceTreeElement[] = [];

  public getLoggingLevel(namespace: string): LoggingLevel {
    const path = this.getPath(namespace);
    return this.reduceLoggingLevel(path);
  }

  reduceLoggingLevel(readOnlyPath: Readonly<NamespaceTreeElement[]>): LoggingLevel {
    const path: NamespaceTreeElement[] = [...readOnlyPath];
    while (path.length > 0) {
      const namespaceTreeElement = path.pop();
      const loggingLevel = namespaceTreeElement.configuration.level;
      if (loggingLevel.name !== 'unset') {
        return loggingLevel;
      }
    }
    return new UnsetLoggingLevel();
  }

  addPath(namespace: string, nsConfiguration: NamespaceConfiguration) {
    const splits = namespace.split('.');
    if (splits.length > 0) {
      this.addSubPath(splits, this.root, nsConfiguration);
    }
  }

  getPath(namespace: string): NamespaceTreeElement[] {
    const split = namespace.split('.');
    const path: NamespaceTreeElement[] = [];
    this.root.forEach(element => element.getPath(split, path));
    return path;
  }


  private addSubPath(readOnlyPath: Readonly<string[]>, treeElements: NamespaceTreeElement[], configuration: NamespaceConfiguration): void {
    if (readOnlyPath.length > 0) {
      const path = [...readOnlyPath];
      const nsPart = path.shift();
      const existingNamespace = treeElements.find(element => element.name === nsPart);
      if (existingNamespace) {
        if (path.length === 0) {
          existingNamespace.configuration = configuration;
        } else {
          this.addSubPath(path, existingNamespace.children, configuration);
        }
      } else {
        if (path.length === 0) {
          treeElements.push(new NamespaceTreeElement(nsPart, configuration));
        } else {
          const newNamespace = new NamespaceTreeElement(nsPart, {level: new UnsetLoggingLevel()});
          treeElements.push(newNamespace);
          this.addSubPath(path, newNamespace.children, configuration);
        }
      }
    }
  }
}

export class NamespaceTreeElement {
  children: NamespaceTreeElement[] = [];

  constructor(public name: string, public configuration: NamespaceConfiguration) {
  }

  public getPath(namespaceSplits: Readonly<string[]>, path: NamespaceTreeElement[]): void {
    if (namespaceSplits.length !== 0) {
      const splitCopy = [...namespaceSplits];
      const namespace = splitCopy.shift();
      if (namespace === this.name) {
        path.push(this);
        this.children.forEach(element => element.getPath(splitCopy, path));
      }
    }
  }
}
