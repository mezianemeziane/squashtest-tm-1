import {LoggerService} from './logger.service';
import {SqtmLogger} from './logger';

export class LoggerFactory {
  // problem with isDevMode here in AOT. Message : "Cannot enable prod mode after platform setup."
  // maybe we could make a file replacement as strategy or introduce a global flag on logger config to avoid any performance hit at runtime.
  // will see that in 2020 :)
  static getLogger(namespace: string) {
    // if (isDevMode()) {
      const loggerService = LoggerService.getLoggerService();
      return new SqtmLogger(loggerService, namespace);
    // } else {
    //   return new NoopLogger(namespace);
    // }
  }
}
