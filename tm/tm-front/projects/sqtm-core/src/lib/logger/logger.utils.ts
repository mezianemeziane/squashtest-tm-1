export function validateNamespace(candidate: string): boolean {
  const parts = candidate.split('.');
  let valid = true;
  for (const part of parts) {
    if (part.length === 0) {
      valid = false;
    } else {
      const regExp = /[^A-Za-z0-9_-]+/g;
      valid = !regExp.test(part);
    }
  }
  return valid;
}


// export function validateNamespace(candidate: string): boolean {
//   const parts = candidate.split('.');
//   for (const part of parts) {
//     if (part.length === 0) {
//       throw Error(`Empty segment in namespace : ${candidate}`);
//     } else {
//       const regExp = /[^A-Za-z0-9_-]+/g;
//       if (regExp.test(part)) {
//         throw Error(`Only A-Za-z0-9_- are allowed in namespaces. Invalid char in : ${candidate}`);
//       }
//     }
//   }
// }
