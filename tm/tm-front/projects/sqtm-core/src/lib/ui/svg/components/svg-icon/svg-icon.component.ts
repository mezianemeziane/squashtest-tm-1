import {Component, OnInit, ChangeDetectionStrategy, Input} from '@angular/core';

@Component({
  selector: 'sqtm-core-svg-icon',
  template: `
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
           [ngClass]="getSvgClass()"
      >
        <use [attr.xlink:href]="absUrl + '#' + icon"></use>
      </svg>
  `,
  styleUrls: ['./svg-icon.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SvgIconComponent implements OnInit {

  @Input()
  icon: string;

  @Input()
  size: 'sm' | 'm' | 'nav-content' | 'nav' = 'm';

  @Input()
  vertical = false;

  @Input()
  workspaceSelected = false;

  @Input()
  workspaceHover = false;

  @Input()
  hoverModifier = false;

  @Input()
  classes: string [] = [];

  constructor() {
  }

  ngOnInit() {
  }

  get absUrl() {
    // our svg definitions are pushed into the root html of the app by svg-icon-defintion component
    return window.location.href;
  }

  getSvgClass() {
    const classes = [];
    classes.push(`icon-size-${this.size}`);
    if (this.workspaceSelected) {
      classes.push(`${this.icon}-svg-color`);
    }
    if (this.vertical) {
      classes.push('icon-vertical');
    }
    classes.push(...this.classes);
    return classes;
  }

  getDivClass() {
    const classes = [];
    classes.push(`icon-size-${this.size}`);
    if (this.hoverModifier) {
      classes.push(`${this.icon}-svg-color__hover`);
    } else if (this.workspaceHover) {
      classes.push('current-workspace-svg-color__hover');
    }
    return classes;
  }
}
