import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EditableTaTestComponent} from './components/editable-ta-test/editable-ta-test.component';
import {TestAutomationDialogComponent} from './components/dialog/test-automation-dialog/test-automation-dialog.component';
import {DialogModule} from '../dialog/dialog.module';
import {TranslateModule} from '@ngx-translate/core';
import {SvgModule} from '../svg/svg.module';
import {GridModule} from '../grid/grid.module';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import {ReactiveFormsModule} from '@angular/forms';
import {WorkspaceCommonModule} from '../workspace-common/workspace-common.module';


@NgModule({
  declarations: [
    EditableTaTestComponent,
    TestAutomationDialogComponent
  ],
  imports: [
    CommonModule,
    DialogModule,
    TranslateModule.forChild(),
    SvgModule,
    GridModule,
    NzButtonModule,
    NzToolTipModule,
    ReactiveFormsModule,
    NzInputModule,
    NzIconModule,
    WorkspaceCommonModule
  ],
  exports: [
    EditableTaTestComponent,
    TestAutomationDialogComponent
  ],
  entryComponents: [
    EditableTaTestComponent,
    TestAutomationDialogComponent
  ]
})
export class TaTestModule {
}
