import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {TestAutomationDialogComponent} from './test-automation-dialog.component';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {DialogReference} from '../../../../dialog/model/dialog-reference';
import createSpyObj = jasmine.createSpyObj;

describe('TestAutomationDialogComponent', () => {
  let component: TestAutomationDialogComponent;
  let fixture: ComponentFixture<TestAutomationDialogComponent>;
  const dialogReference = createSpyObj(['close']);
  dialogReference.data = {testCaseId: 1};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), HttpClientTestingModule, TestingUtilsModule],
      declarations: [TestAutomationDialogComponent],
      providers: [
        {provide: DialogReference, useValue: dialogReference}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestAutomationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
