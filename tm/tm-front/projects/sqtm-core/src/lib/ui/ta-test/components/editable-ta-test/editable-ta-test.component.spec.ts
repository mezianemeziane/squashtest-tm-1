import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {EditableTaTestComponent} from './editable-ta-test.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {OverlayModule} from '@angular/cdk/overlay';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';

describe('EditableTaTestComponent', () => {
  let component: EditableTaTestComponent;
  let fixture: ComponentFixture<EditableTaTestComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule, OverlayModule, TranslateModule.forRoot()],
      declarations: [EditableTaTestComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableTaTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
