import {AfterViewInit, Directive, ElementRef, Host, NgZone, OnDestroy, OnInit} from '@angular/core';
import {fromEvent, Subject} from 'rxjs';
import {anchorLogger} from '../anchor.logger';
import {AnchorPanel} from './anchor-panel';
import {distinctUntilChanged, filter, map, skip, takeUntil} from 'rxjs/operators';
import {ViewIdDirective} from './view-id.directive';
import {AnchorService} from '../service/anchor.service';

const logger = anchorLogger.compose('AnchorScrollDirective');

@Directive({
  selector: '[sqtmCoreAnchorScroll]'
})
export class AnchorScrollDirective implements OnInit, AfterViewInit, OnDestroy {
  private unsub$ = new Subject<void>();

  private panels: AnchorPanel[] = [];

  private offsetTop = 10;

  private activePanel: string;

  constructor(@Host() public host: ElementRef, private ngZone: NgZone,
              private viewIdDirective: ViewIdDirective, private anchorService: AnchorService) {
  }

  ngOnInit(): void {
    this.ngZone.runOutsideAngular(() => {
      fromEvent(this.host.nativeElement, 'scroll')
        .pipe(
          skip(1),
          map(scroll => this.selectActivePanel()),
          distinctUntilChanged()
        ).subscribe((panelId: string) => {
        this.activePanel = panelId;
        this.notifyScrollToPanel(panelId);
      });
    });
  }

  ngAfterViewInit(): void {
    this.anchorService.requestScroll$.pipe(
      takeUntil(this.unsub$),
      filter(scrollRequest => this.viewIdDirective.id === scrollRequest.viewId)
    ).subscribe(scrollRequest => {
      const panel = this.panels.find(p => p.getId() === scrollRequest.selectedLinkId);
      if (Boolean(panel)) {
        setTimeout(() => {
          panel.getElementRef().nativeElement.scrollIntoView();
        });
      }
    });
  }

  registerPanel(panel: AnchorPanel) {
    this.panels.push(panel);
  }

  selectActivePanel() {
    const nativeElement = this.host.nativeElement;
    const topHost = nativeElement.getBoundingClientRect().top;
    const sections = [];

    this.panels.forEach(panel => {
      const topElement = panel.getElementRef().nativeElement.getBoundingClientRect().top;
      const offset = topElement - topHost;
      if (offset <= this.offsetTop) {
        sections.push({panelId: panel.getId(), offset: offset});
      }
    });
    const maxSection = sections.reduce((prev, curr) => (curr.offset > prev.offset ? curr : prev));
    return maxSection.panelId;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private notifyScrollToPanel(panelId: string) {
    this.ngZone.run(() => {
      this.anchorService.notifyActiveLink(this.viewIdDirective.id, panelId);
    });
  }
}
