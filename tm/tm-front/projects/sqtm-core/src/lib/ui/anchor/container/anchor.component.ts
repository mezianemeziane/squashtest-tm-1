import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  OnDestroy,
  OnInit
} from '@angular/core';
import {Subject} from 'rxjs';
import {filter, take} from 'rxjs/operators';
import {AnchorLinkComponent} from '../components/anchor-link/anchor-link.component';
import {AnchorService} from '../service/anchor.service';
import {DefaultAnchorLinkDirective} from '../directives/default-anchor-link.directive';
import {anchorLogger} from '../anchor.logger';

const logger = anchorLogger.compose('AnchorComponent');

@Component({
  selector: 'sqtm-core-anchor',
  templateUrl: './anchor.component.html',
  styleUrls: ['./anchor.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnchorComponent implements OnInit, AfterViewInit, OnDestroy {

  unsub$ = new Subject<void>();

  @ContentChild(DefaultAnchorLinkDirective, {read: AnchorLinkComponent})
  defaultLink: AnchorLinkComponent;

  constructor(public cdr: ChangeDetectorRef,
              private anchorService: AnchorService) {
  }

  ngOnInit(): void {
    this.anchorService.beginLoadPhase();
  }

  ngAfterViewInit(): void {
    this.assertHasDefaultLink();
    this.restoreDefaultActiveLinkIfNeeded();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  /**
   * Default link is made active once state is loaded if default link exists and if there's no current active link
   * for its view.
   * @private
   */
  private restoreDefaultActiveLinkIfNeeded() {
    logger.debug('About to restore default active link.');

    this.anchorService.getActiveLink(this.defaultLink.viewId).pipe(
      take(1),
    ).subscribe((activeLink) => {
      if (activeLink == null) {
        logger.debug('No active link : restore default link');
        this.defaultLink.restoreActiveLink();
      } else {
        logger.debug('Active link found ' + activeLink);

        this.anchorService.isAnchorLinkActiveAndVisible(this.defaultLink.viewId, activeLink).pipe(
          take(1),
          filter(activeAndVisible => !activeAndVisible),
        ).subscribe(() => {
          logger.debug('active link is not visible : restore default link ' + activeLink);
          this.defaultLink.restoreActiveLink();
        });
      }
    });
  }

  private assertHasDefaultLink(): void {
    if (this.defaultLink == null) {
      throw new Error('You must provide a default anchor link with DefaultAnchorLinkDirective.');
    }
  }
}
