import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {createSelector} from '@ngrx/store';

export interface AnchorsState extends EntityState<AnchorState> {
  loaded: boolean;
}

export interface AnchorState extends EntityState<LinkState> {
  // id of the view
  viewId: string;
  selectedLinkId: string;
}

export interface LinkState {
  id: string;
  group: string;
  active: boolean;
  visible: boolean;
}

export const anchorsEntityAdapter = createEntityAdapter<AnchorState>({selectId: (anchorState) => anchorState.viewId});
export const anchorEntityAdapter = createEntityAdapter<LinkState>();

export const initialAnchorsState: Readonly<AnchorsState> = anchorsEntityAdapter.getInitialState({
  loaded: false
});

export const initialAnchorState: Readonly<AnchorState> = anchorEntityAdapter.getInitialState({
  viewId: null,
  selectedLinkId: null,
  routingLink: null
});


export const selectAnchorsState = (state: AnchorsState) => state;

export const viewStateSelector = (viewId: string) => {
  return createSelector(selectAnchorsState, (anchorsState: AnchorsState) => {
    return anchorsState.entities[viewId];
  });
};

export const activeLinkSelector = (viewId: string) => {
  return createSelector(selectAnchorsState, (anchorsState: AnchorsState) => {
    const anchorState = anchorsState.entities[viewId];
    return anchorState?.selectedLinkId;
  });
};

export const isAnchorLinkActiveAndVisibleSelector = (viewId: string, linkId: string) => {
  return createSelector(selectAnchorsState, (anchorsState: AnchorsState) => {
    const anchorState = anchorsState.entities[viewId];
    const anchorLinkState = anchorState?.entities[linkId];

    if (anchorLinkState == null) {
      return false;
    }

    return anchorState.selectedLinkId === linkId && anchorLinkState.visible;
  });

};

export const panelIsActiveSelector = (viewId: string, linkId: string) => {
  return createSelector(selectAnchorsState, (anchorsState: AnchorsState) => {
    const anchorState = anchorsState.entities[viewId];
    const linkState = anchorState.entities[linkId];
    return linkState.active;
  });
};

export const activeGroupSelector = (viewId: string) => {
  return createSelector(selectAnchorsState, (anchorsState: AnchorsState) => {
    const anchorState = anchorsState.entities[viewId];
    const selectedLinkId = anchorState?.selectedLinkId;
    const linkState = anchorState?.entities[selectedLinkId];
    return linkState?.group;
  });
};
