import {Directive, Input, OnDestroy} from '@angular/core';
import {AnchorService} from '../service/anchor.service';

@Directive({
  selector: '[sqtmCoreViewId]'
})
export class ViewIdDirective implements OnDestroy {

  @Input('sqtmCoreViewId')
  id: string;

  constructor(public readonly anchorService: AnchorService) {
  }

  ngOnDestroy() {
    this.anchorService.clearView(this.id);
  }
}
