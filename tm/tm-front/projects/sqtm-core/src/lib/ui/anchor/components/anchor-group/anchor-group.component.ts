import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
} from '@angular/core';
import {AnchorService} from '../../service/anchor.service';
import {Observable, Subject} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {ViewIdDirective} from '../../directives/view-id.directive';
import {AnchorLinkComponent} from '../anchor-link/anchor-link.component';

@Component({
  selector: 'sqtm-core-anchor-group',
  templateUrl: './anchor-group.component.html',
  styleUrls: ['./anchor-group.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnchorGroupComponent implements AfterViewInit, OnDestroy {

  private unsub$ = new Subject<void>();

  @Input()
  id: string;

  selected$: Observable<boolean>;

  private anchorLinks: AnchorLinkComponent[] = [];

  constructor(private cdr: ChangeDetectorRef,
              private anchorService: AnchorService,
              private viewIdDirective: ViewIdDirective) {
  }

  registerAnchorLink(anchorLink: AnchorLinkComponent): void {
    this.anchorLinks.push(anchorLink);
  }

  get isEmpty(): boolean {
    return this.anchorLinks
      .filter(anchorLink => anchorLink.visible).length === 0;
  }

  ngAfterViewInit(): void {
    this.selected$ = this.anchorService.getActiveGroup(this.viewIdDirective.id).pipe(
      takeUntil(this.unsub$),
      map((activeGroup) => this.id === activeGroup)
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
