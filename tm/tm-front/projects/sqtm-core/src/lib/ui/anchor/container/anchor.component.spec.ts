import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {AnchorComponent} from './anchor.component';
import {ViewIdDirective} from '../directives/view-id.directive';
import {DefaultAnchorLinkDirective} from '../directives/default-anchor-link.directive';
import {Component, NO_ERRORS_SCHEMA, ViewChild} from '@angular/core';
import {AnchorService} from '../service/anchor.service';
import {EMPTY, of} from 'rxjs';
import {initialAnchorsState} from '../service/anchors.state';
import {AnchorLinkComponent} from '../components/anchor-link/anchor-link.component';
import {RouterTestingModule} from '@angular/router/testing';
import {AnchorGroupComponent} from '../components/anchor-group/anchor-group.component';
import SpyObj = jasmine.SpyObj;

@Component({
  template: `
    <sqtm-core-anchor>
      <sqtm-core-anchor-group>
        <sqtm-core-anchor-link sqtmCoreDefaultAnchorLink>
        </sqtm-core-anchor-link>
      </sqtm-core-anchor-group>
    </sqtm-core-anchor>
  `
})
class TestWrapperComponent {
  @ViewChild(AnchorComponent) anchorComponent: AnchorComponent;
}

function mockAnchorService(): SpyObj<AnchorService> {
  const anchorService = jasmine.createSpyObj<AnchorService>([
    'beginLoadPhase', 'registerLink', 'getActiveLink', 'getActiveGroup']);
  anchorService.state$ = of(initialAnchorsState);
  anchorService.getActiveLink.and.returnValue(EMPTY);
  anchorService.getActiveGroup.and.returnValue(EMPTY);
  return anchorService;
}

describe('AnchorComponent', () => {
  let component: AnchorComponent;
  let fixture: ComponentFixture<TestWrapperComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestWrapperComponent,
        AnchorComponent,
        DefaultAnchorLinkDirective,
        ViewIdDirective,
        AnchorLinkComponent,
        AnchorGroupComponent,
      ],
      imports: [RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: ViewIdDirective, useValue: {}},
        {provide: AnchorService, useValue: mockAnchorService()},
        {provide: AnchorGroupComponent, useValue: {}},
      ]
    })
      .compileComponents();
  }));

  it('should create', waitForAsync(() => {
    fixture = TestBed.createComponent(TestWrapperComponent);
    fixture.detectChanges();
    component = fixture.componentInstance.anchorComponent;
    expect(component).toBeTruthy();
  }));
});
