import {ElementRef} from '@angular/core';

export interface AnchorPanel {

  getElementRef(): ElementRef;

  getId(): string;
}
