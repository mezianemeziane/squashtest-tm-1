import {Injectable} from '@angular/core';
import {createStore, Store} from '../../core/store/store';
import {
  createWorkspaceSelector,
  initialUiManagerState,
  NavBarState,
  PersistedNavBarState,
  selectAllThemes,
  selectCurrentTheme,
  selectNavBarState,
  UiManagerState,
  workspaceLayoutAdapter,
  WorkspaceLayoutState
} from './ui-manager-state';
import {Observable, Subject} from 'rxjs';
import {select} from '@ngrx/store';
import {concatMap, filter, map, shareReplay, take, tap, withLatestFrom} from 'rxjs/operators';
import {Theme} from './theme.model';
import {Identifier} from '../../model/entity.model';
import {LocalPersistenceService} from '../../core/services/local-persistence.service';
import {Router} from '@angular/router';

/**
 * This service manage all ui global state.
 * I mean a state :
 *  - Truly global logically across all app (ex: nav bar collapsed state).
 *  - That must be preserved across navigation.
 *  - That concern ui state. For global referential data use ReferentialDataService.
 */
@Injectable({
  providedIn: 'root'
})
export class UiManagerService {

  private store: Store<UiManagerState> = createStore<UiManagerState>(initialUiManagerState, {
    id: 'UiManagerStore',
    logDiff: 'simple'
  });

  private _requireClosingContextualContent = new Subject<void>();
  public requireClosingContextualContent$ = this._requireClosingContextualContent.asObservable();

  // output observables
  navBarState$: Observable<NavBarState>;
  selectedTheme$: Observable<Theme>;
  availableThemes$: Observable<Theme[]>;

  private readonly persistenceKey = 'nav-bar';

  constructor(private persistenceService: LocalPersistenceService, private router: Router) {


    this.navBarState$ = this.store.state$.pipe(
      select(selectNavBarState),
      shareReplay(1)
    );

    this.availableThemes$ = this.store.state$.pipe(
      select(selectAllThemes),
      shareReplay(1)
    );

    this.selectedTheme$ = this.store.state$.pipe(
      select(selectCurrentTheme),
      shareReplay(1)
    );

    this.persistenceService.get<PersistedNavBarState>(this.persistenceKey).pipe(
      take(1),
      filter(persistedNavBarState => Boolean(persistedNavBarState)),
      withLatestFrom(this.store.state$),
      map(([persistedNavBarState, state]) => {
        const navBarState = {...state.navBarState, collapsed: persistedNavBarState.collapsed};
        return {...state, navBarState};
      })
    ).subscribe((state) => this.store.commit(state));
  }

  toggleNavBar() {
    this.store.state$.pipe(
      take(1),
      map((state: UiManagerState) => {
        const navBarState = {...state.navBarState};
        navBarState.collapsed = !navBarState.collapsed;
        return {...state, navBarState};
      }),
      tap(state => this.store.commit(state)),
      concatMap(state => this.persistenceService
        .set<PersistedNavBarState>(this.persistenceKey, {collapsed: state.navBarState.collapsed})
        .pipe(
          map(() => state)
        ))
    ).subscribe();
  }

  changeCurrentWorkspace(workspaceName: string) {
    this.store.state$.pipe(
      take(1),
      map((state: UiManagerState) => {
        const navBarState = {...state.navBarState};
        if (!workspaceName.includes('administration')) {
          navBarState.urlBeforeAdministration = this.router.url;
        }
        navBarState.currentWorkspace = workspaceName;
        return {...state, navBarState};
      })
    ).subscribe(state => this.store.commit(state));
  }

  public changeTheme(themeId: Identifier) {
    this.store.state$.pipe(
      take(1),
      filter(state => (state.themeState.ids as any[]).includes(themeId)),
      map(state => {
        const themeState = {...state.themeState};
        themeState.selectedTheme = themeId;
        return {...state, themeState};
      }))
      .subscribe(state => this.store.commit(state));
  }

  registerWorkspace(name: string) {
    this.store.state$.pipe(
      take(1),
      map(state => {
        const workspaceLayoutState = workspaceLayoutAdapter.upsertOne({
          id: name,
          treeFolded: false,
          contentOpened: false
        }, state.workspaceLayoutState);
        return {...state, workspaceLayoutState};
      })
    ).subscribe(state => this.store.commit(state));
  }

  // No share on that or you will have an observable leak
  createWorkspaceLayoutObservable(name: string): Observable<WorkspaceLayoutState> {
    return this.store.state$.pipe(
      select(createWorkspaceSelector(name))
    );
  }

  toggleTree(name: string) {
    this.store.state$.pipe(
      take(1),
      filter(state => (state.workspaceLayoutState.ids as any[]).includes(name)),
      map(state => {
        const treeFolded = !state.workspaceLayoutState.entities[name].treeFolded;
        return this.doToggleFoldState(name, treeFolded, state);
      })
    ).subscribe(state => this.store.commit(state));
  }

  unfoldTree(name: string) {
    this.store.state$.pipe(
      take(1),
      filter(state => (state.workspaceLayoutState.ids as any[]).includes(name)),
      map(state => {
        return this.doToggleFoldState(name, false, state);
      })
    ).subscribe(state => this.store.commit(state));
  }

  private doToggleFoldState(name: string, treeFolded: boolean, state: UiManagerState) {
    const workspaceLayoutState = workspaceLayoutAdapter.updateOne({
      id: name,
      changes: {treeFolded}
    }, state.workspaceLayoutState);
    return {...state, workspaceLayoutState};
  }

  closeContextualContent(name: string) {
    this.unfoldTree(name);
    this._requireClosingContextualContent.next();
  }

  changeWorkspaceBeforeNavigationOnAdministration(workspaceName: string) {
    this.store.state$.pipe(
      take(1),
      map((state: UiManagerState) => {
        const navBarState = {...state.navBarState};
        navBarState.urlBeforeAdministration = workspaceName;
        return {...state, navBarState};
      })
    ).subscribe(state => this.store.commit(state));
  }
}

