import {Theme} from './theme.model';
import {Identifier} from '../../model/entity.model';
import {createEntityAdapter, Dictionary, EntityState} from '@ngrx/entity';
import {themes} from './themes';

export interface ThemeState extends EntityState<Theme> {
  selectedTheme: Identifier;
}

export function createInitialThemeState() {

  const dictThemes: Dictionary<Theme> = {};
  const ids = [];
  themes
    .forEach(function (theme) {
      ids.push(theme.id);
      dictThemes[theme.id] = theme;
    });

  return {
    ids,
    selectedTheme: 'default',
    entities: dictThemes
  } as ThemeState;

}

export const themeEntityAdapter = createEntityAdapter<Theme>();
export const {selectAll, selectEntities} = themeEntityAdapter.getSelectors();
