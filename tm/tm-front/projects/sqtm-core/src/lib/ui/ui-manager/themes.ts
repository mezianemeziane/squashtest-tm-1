import {Theme} from './theme.model';

///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///


export const themes: Theme[] = [
  {
    'id': 'default',
    'global-theme': {
      'nav-main-color': '#191D35',
      'rounding-factor': 0,
      'label-color': '#62737F',
      'background-color': '#F2F4F7',
      'gray-accented-color': '#3d485c',
      'container-border-color': '#D9E4EA',
      'container-background-color': '#FFFFFF',
      'user-value-color': 'black',
      'selected-color': '#172b4d'
    },
    'home-workspace': {
      'main-color': '#191D35',
      'brightest-color': '#323c6f',
      'darkest-color': '#121526',
      'contrast-color': '#fffdff'
    },
    'test-case-workspace': {
      'brightest-color': '#05cb7b',
      'main-color': '#037F4C',
      'darkest-color': '#1a1d0d',
      'contrast-color': '#f6f6f6',
      'hovered-row-color': '#c0f9bd85',
      'selected-row-color': '#c0f9bd'
    },
    'requirement-workspace': {
      'brightest-color': '#49befd',
      'main-color': '#0078ae',
      'darkest-color': '#000932',
      'contrast-color': '#ffffff',
      'hovered-row-color': '#c4ecff7a',
      'selected-row-color': '#c4ecff'
    },
    'campaign-workspace': {
      'brightest-color': '#bf42ff',
      'main-color': '#751AA3',
      'darkest-color': '#2f2642',
      'contrast-color': '#fffdff',
      'hovered-row-color': '#e7c7ff78',
      'selected-row-color': '#e7c7ff'
    },
    'custom-report-workspace': {
      'brightest-color': '#da3378',
      'main-color': '#a2104d',
      'darkest-color': '#36001a',
      'contrast-color': '#fffdff',
      'hovered-row-color': '#f5a7b596',
      'selected-row-color': '#f5a7b5'
    },
    'administration-workspace': {
      'brightest-color': '#7b8da7',
      'main-color': '#435979',
      'darkest-color': '#434343',
      'contrast-color': '#000000',
      'hovered-row-color': '#7998c538',
      'selected-row-color': '#7998c57a'
    },
    'bugtracker-workspace': {
      'brightest-color': '#f694222e',
      'main-color': '#f69422',
      'darkest-color': '#96560c',
      'contrast-color': '#000000',
      'hovered-row-color': '#ffe0bc',
      'selected-row-color': '#ffca7d'
    },
    'automation-workspace': {
      'brightest-color': '#05cb7b',
      'main-color': '#037F4C',
      'darkest-color': '#CCCC00',
      'contrast-color': '#000000',
      'hovered-row-color': ' #c0f9bd85',
      'selected-row-color': '#c0f9bd'
    },
    'action-word-workspace': {
      'brightest-color': '#05cb7b',
      'main-color': '#037F4C',
      'darkest-color': '#CCCC00',
      'contrast-color': '#000000',
      'hovered-row-color': ' #c0f9bd85',
      'selected-row-color': '#c0f9bd'
    }
  },
  {
    'id': 'flashy',
    'global-theme': {
      'nav-main-color': '#43136e',
      'rounding-factor': 0,
      'label-color': '#8fa2ae',
      'background-color': '#F2F4F7',
      'gray-accented-color': '#172b4d',
      'container-border-color': '#D9E4EA',
      'container-background-color': '#FFFFFF',
      'user-value-color': 'black',
      'selected-color': '#172b4d'
    },
    'home-workspace': {
      'main-color': '#c8c2c5',
      'brightest-color': '#c8c2c5',
      'darkest-color': '#c8c2c5',
      'contrast-color': '#0a0a0a',
      'hovered-row-color': '#F2F4F7',
      'selected-row-color': '#F2F4F7'
    },
    'test-case-workspace': {
      'brightest-color': '#00f4ff',
      'main-color': '#3fb27e',
      'darkest-color': '#450041',
      'contrast-color': '#ffffff',
      'hovered-row-color': '#F2F4F7',
      'selected-row-color': '#F2F4F7'
    },
    'requirement-workspace': {
      'brightest-color': '#ff80aa',
      'main-color': '#f252a9',
      'darkest-color': '#400800',
      'contrast-color': '#fffdff',
      'hovered-row-color': '#F2F4F7',
      'selected-row-color': '#F2F4F7'
    },
    'campaign-workspace': {
      'brightest-color': '#ffbb4d',
      'main-color': '#ffa100',
      'darkest-color': '#c85600',
      'contrast-color': '#000000',
      'hovered-row-color': '#F2F4F7',
      'selected-row-color': '#F2F4F7'
    },
    'custom-report-workspace': {
      'brightest-color': '#ff0048',
      'main-color': '#750021',
      'darkest-color': '#36001a',
      'contrast-color': '#fffdff',
      'hovered-row-color': '#F2F4F7',
      'selected-row-color': '#F2F4F7'
    },
    'administration-workspace': {
      'brightest-color': '#f6ff00',
      'main-color': '#b7bf00',
      'darkest-color': '#464d00',
      'contrast-color': '#000000',
      'hovered-row-color': '#F2F4F7',
      'selected-row-color': '#F2F4F7'
    },
    'bugtracker-workspace': {
      'brightest-color': '#EE425A',
      'main-color': '#EE425A',
      'darkest-color': '#EE425A',
      'contrast-color': '#ffffff',
      'hovered-row-color': '#F2F4F7',
      'selected-row-color': '#F2F4F7'
    },
    'automation-workspace': {
      'brightest-color': '#CCCC00',
      'main-color': '#CCCC00',
      'darkest-color': '#CCCC00',
      'contrast-color': '#000000',
      'hovered-row-color': '#F2F4F7',
      'selected-row-color': '#F2F4F7'
    },
    'action-word-workspace': {
      'brightest-color': '#05cb7b',
      'main-color': '#037F4C',
      'darkest-color': '#CCCC00',
      'contrast-color': '#000000',
      'hovered-row-color': ' #c0f9bd',
      'selected-row-color': ' #c0f9bd'
    }
  },
];
