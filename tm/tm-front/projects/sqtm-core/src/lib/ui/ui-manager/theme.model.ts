///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///


export type WorkspaceCssProperties = {
  [K in Workspaces]: WorkspaceTheme
};

export interface CssProperties {
  [K: string]: string;
}

export interface GlobalTheme {
  // Navigation bar
  'nav-main-color': string;
  'rounding-factor': number;
  'label-color': string;
  'user-value-color': string;
  'background-color': string;
  'gray-accented-color': string;
  'container-border-color': string;
  'container-background-color': string;
  'selected-color': string;
}

export interface WorkspaceTheme {
  'brightest-color'?: string;
  'main-color': string;
  'darkest-color'?: string;
  'background-color'?: string;
  // A color that contrasts well with main-color.
  'contrast-color'?: string;
  'hovered-row-color'?: string;
  'selected-row-color'?: string;
}

export enum Workspaces {
  'home-workspace' = 'home-workspace',
  'test-case-workspace' = 'test-case-workspace',
  'requirement-workspace' = 'requirement-workspace',
  'campaign-workspace' = 'campaign-workspace',
  'custom-report-workspace' = 'custom-report-workspace',
  'administration-workspace' = 'administration-workspace',
  'bugtracker-workspace' = 'bugtracker-workspace',
  'automation-workspace' = 'automation-workspace',
  'action-word-workspace' = 'action-word-workspace'
}

export interface Theme extends WorkspaceCssProperties {
  id: string;

  // Global theme
  'global-theme': GlobalTheme;
}
