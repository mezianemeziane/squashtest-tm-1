import {createFeatureSelector, createSelector} from '@ngrx/store';
import {createInitialThemeState, selectAll, selectEntities, ThemeState} from './theme.state';
import {createEntityAdapter, EntityState} from '@ngrx/entity';

export interface NavBarState {
  collapsed: boolean;
  currentWorkspace: string;
  urlBeforeAdministration: string;
}

export type PersistedNavBarState = Pick<NavBarState, 'collapsed'>;

export interface WorkspaceLayoutState {
  id: string;
  treeFolded: boolean;
  contentOpened: boolean;
}

export interface UiManagerState {
  themeState: ThemeState;
  navBarState: NavBarState;
  workspaceLayoutState: EntityState<WorkspaceLayoutState>;
}

export const initialUiManagerState: Readonly<UiManagerState> = {
  themeState: createInitialThemeState(),
  navBarState: {
    collapsed: false,
    currentWorkspace: '',
    urlBeforeAdministration: '',
  },
  workspaceLayoutState: {ids: [], entities: {}}
};

export const workspaceLayoutAdapter = createEntityAdapter<WorkspaceLayoutState>();

export const selectNavBarState = createFeatureSelector<UiManagerState, NavBarState>('navBarState');
export const selectThemeState = createFeatureSelector<UiManagerState, ThemeState>('themeState');
export const selectWorkspaceLayoutState =
  createFeatureSelector<UiManagerState, EntityState<WorkspaceLayoutState>>('workspaceLayoutState');

export const selectAllThemes = createSelector(selectThemeState, selectAll);
const selectThemeEntities = createSelector(selectThemeState, selectEntities);
const selectCurrentThemeId = createSelector(selectThemeState, state => state.selectedTheme);
export const selectCurrentTheme = createSelector(selectCurrentThemeId, selectThemeEntities, (thId, thDict) => {
  return thDict[thId];
});

export function createWorkspaceSelector(workspaceName: string) {
  return createSelector(
    selectWorkspaceLayoutState,
    (layoutState: EntityState<WorkspaceLayoutState>) => (layoutState.entities[workspaceName]));
}
