import {uiLogger} from '../ui.logger';

export const platformNavigationLogger = uiLogger.compose('platform-navigation');
