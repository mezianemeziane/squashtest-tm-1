import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlatformLinkDirective } from './directives/platform-link.directive';



@NgModule({
  declarations: [PlatformLinkDirective],
  exports: [PlatformLinkDirective],
  imports: [
    CommonModule
  ]
})
export class PlatformNavigationModule { }
