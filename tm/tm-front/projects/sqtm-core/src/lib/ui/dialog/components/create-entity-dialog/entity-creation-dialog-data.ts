import {Validators} from '@angular/forms';
import {CreationDialogData} from '../dialog-templates/creation-dialog/creation-dialog.configuration';
import {BindableEntity} from '../../../../model/bindable-entity.model';

export class OptionalTextField {
    fieldName: string;
    validators?: Validators;
    // Default value is either a string or a function returning a string
    defaultValue?: string | ((...args: any[]) => string);
}

export const REFERENCE_FIELD: OptionalTextField = {
  fieldName: 'reference',
  validators: [Validators.maxLength(50)],
  defaultValue: ''
};

export interface EntityCreationDialogDefaultValue {
  name: string | ((...args: any[]) => string);
  description: string | ((...args: any[]) => string);
}

export class EntityCreationFormData extends CreationDialogData {
  projectId: number;
  bindableEntity?: BindableEntity;
  optionalTextFields?: OptionalTextField[];
  defaultValues?: Partial<EntityCreationDialogDefaultValue>;
  hasOptionalCopyTestPlanField?: boolean;
}

export class EntityCreationDialogData extends EntityCreationFormData {
  // the parent id as entity ref. ex : TestCaseLibrary-2
  parentEntityReference: string;
  postUrl: string;
}
