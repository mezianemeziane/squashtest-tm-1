import {Directive, HostListener} from '@angular/core';
import {DialogReference} from '../model/dialog-reference';

@Directive({
  selector: '[sqtmCoreDialogClose]'
})
export class DialogCloseDirective {

  constructor(private dialogReference: DialogReference) {
  }

  @HostListener('click', [])
  closeDialog() {
    this.dialogReference.cancel();
  }
}
