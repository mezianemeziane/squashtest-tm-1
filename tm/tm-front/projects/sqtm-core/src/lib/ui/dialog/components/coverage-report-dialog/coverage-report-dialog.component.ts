import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {CoverageReportConfiguration} from './coverage-report-configuration';
import {DialogReference} from '../../model/dialog-reference';

@Component({
  selector: 'sqtm-core-coverage-report-dialog',
  templateUrl: './coverage-report-dialog.component.html',
  styleUrls: ['./coverage-report-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoverageReportDialogComponent implements OnInit {

  data: CoverageReportConfiguration;

  constructor(private dialogReference: DialogReference<CoverageReportConfiguration>) {
    this.data = dialogReference.data;
  }

  ngOnInit(): void {
  }

}
