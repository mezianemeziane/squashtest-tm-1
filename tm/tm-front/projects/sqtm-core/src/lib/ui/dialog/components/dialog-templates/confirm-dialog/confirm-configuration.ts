export class ConfirmConfiguration {
  id: string;
  titleKey: string;
  level: 'WARNING' | 'DANGER' | 'INFO' = 'DANGER';
  messageKey?: string;
}

export const defaultConfirmConfiguration: Readonly<ConfirmConfiguration> = {
  id: 'confirm',
  titleKey: 'sqtm-core.generic.label.confirm',
  level: 'WARNING',
  messageKey: 'sqtm-core.dialog.confirm'
};
