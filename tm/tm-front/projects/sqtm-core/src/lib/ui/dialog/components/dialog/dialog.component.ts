import {
  ChangeDetectionStrategy,
  Component,
  ComponentRef,
  ElementRef,
  EmbeddedViewRef,
  HostBinding,
  Inject,
  NgZone,
  OnInit,
  Renderer2,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation
} from '@angular/core';
import {BasePortalOutlet, CdkPortalOutlet, ComponentPortal, TemplatePortal} from '@angular/cdk/portal';
import {DOCUMENT} from '@angular/common';
import {fromEvent} from 'rxjs';
import {take, takeUntil} from 'rxjs/operators';
import {Overlay, OverlayRef, OverlaySizeConfig} from '@angular/cdk/overlay';
import {OVERLAY_REF} from '../../dialog.constants';


/** @dynamic */
@Component({
  selector: 'sqtm-core-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class DialogComponent extends BasePortalOutlet implements OnInit {

  @ViewChild(CdkPortalOutlet, {static: true})
  portalOutlet: CdkPortalOutlet;

  @HostBinding('style.width')
  public hostWidth = '100%';

  @ViewChild('dialogContainer', {read: ElementRef, static: true})
  private container: ElementRef;

  private resizing = false;

  constructor(@Inject(DOCUMENT) private document: Document, @Inject(OVERLAY_REF) private overlayRef: OverlayRef, private overlay: Overlay,
              private zone: NgZone, private renderer: Renderer2, private vcr: ViewContainerRef) {
    super();
  }

  ngOnInit() {

  }

  attachComponentPortal<T>(portal: ComponentPortal<T>): ComponentRef<T> {
    return this.portalOutlet.attachComponentPortal(portal);
  }

  attachTemplatePortal<C>(portal: TemplatePortal<C>): EmbeddedViewRef<C> {
    throw new Error('Not implemented for templates');
  }

  handleClick($event: MouseEvent) {
    const currentTarget = $event.target as HTMLElement;
    const classList = currentTarget.classList;

    if (classList.contains('dialog-resize-handle') && !this.resizing) {
      const resizeWidth = classList.contains('resize-handle-e') || classList.contains('resize-handle-se');
      const resizeHeight = classList.contains('resize-handle-s') || classList.contains('resize-handle-se');

      this.resizing = true;
      $event.preventDefault();
      $event.stopPropagation();
      const containerElement = this.vcr.element.nativeElement as HTMLElement;

      const mouseUpStream = fromEvent(this.document.documentElement, 'mouseup').pipe(
        take(1)
      );

      const mouseMoveStream = fromEvent(this.document.documentElement, 'mousemove').pipe(
        takeUntil(mouseUpStream)
      );

      mouseUpStream.subscribe(($mouseUpEvent: MouseEvent) => {
        this.resizing = false;
        this.resize(containerElement, $mouseUpEvent, resizeWidth, resizeHeight);
      });

      mouseMoveStream.subscribe(($mouseUpEvent: MouseEvent) => {
        this.resize(containerElement, $mouseUpEvent, resizeWidth, resizeHeight);
      });
    }
  }

  private resize(containerElement, $mouseUpEvent: MouseEvent, resizeWidth: boolean, resizeHeight: boolean) {
    const initialX = containerElement.offsetLeft;
    const initialY = containerElement.offsetTop;
    const newContainerWidth = $mouseUpEvent.clientX - initialX;
    const newContainerHeight = $mouseUpEvent.clientY - initialY;
    const sizeConfig: OverlaySizeConfig = {};
    if (resizeWidth) {
      sizeConfig.width = newContainerWidth;
    }

    if (resizeHeight) {
      sizeConfig.height = newContainerHeight;
    }
    this.overlayRef.updateSize(sizeConfig);
    // const positionStrategy = this.overlay.position()
    //   .global()
    //   .centerVertically()
    //   .centerHorizontally()
    //   .top(initialY)
    //   .left(initialX);
    // this.overlayRef.updatePositionStrategy(positionStrategy);
  }
}
