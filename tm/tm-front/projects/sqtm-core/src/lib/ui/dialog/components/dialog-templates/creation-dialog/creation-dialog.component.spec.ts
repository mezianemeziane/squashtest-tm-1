import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CreationDialogComponent} from './creation-dialog.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {DialogReference} from '../../../model/dialog-reference';
import {CreationDialogData} from './creation-dialog.configuration';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';

describe('CreationDialogComponent', () => {
  let component: CreationDialogComponent;
  let fixture: ComponentFixture<CreationDialogComponent>;

  const overlayReference = jasmine.createSpyObj(['attachments']);

  overlayReference.attachments.and.returnValue(EMPTY);

  const dialogReference: DialogReference<CreationDialogData, any> = new DialogReference<CreationDialogData, any>(
    'create',
    null,
    overlayReference,
    {id: 'toto', titleKey: 'titleKey'},
  );


  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule],
      declarations: [CreationDialogComponent],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference
        }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
