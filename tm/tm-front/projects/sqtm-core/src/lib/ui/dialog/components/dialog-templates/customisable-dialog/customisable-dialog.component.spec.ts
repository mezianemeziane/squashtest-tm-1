import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CustomisableDialogComponent} from './customisable-dialog.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {DialogReference} from '../../../model/dialog-reference';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';


describe('CustomisableDialogComponent', () => {
  let component: CustomisableDialogComponent;
  let fixture: ComponentFixture<CustomisableDialogComponent>;

  const dialogReferenceMock = jasmine.createSpyObj('DialogReference', ['close']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, TranslateModule.forRoot()],
      declarations: [CustomisableDialogComponent],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReferenceMock
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomisableDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
