import {CoverageTestStepInfo} from '../../../../model/test-case/requirement-version-coverage-model';

export class CoverageVerifiedByConfiguration {
  testCaseId: number;
  coverageStepInfoDto: CoverageTestStepInfo[];
  calledTestCaseIds: number[];
}
