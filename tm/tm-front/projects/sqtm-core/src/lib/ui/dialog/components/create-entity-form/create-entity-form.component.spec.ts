import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CreateEntityFormComponent} from './create-entity-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CKEditorModule} from 'ckeditor4-angular';

import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';

describe('CreateEntityFormComponent', () => {
  let component: CreateEntityFormComponent;
  let fixture: ComponentFixture<CreateEntityFormComponent>;

  const referentialDataService = jasmine.createSpyObj<ReferentialDataService>(['findCustomFieldByProjectIdAndDomain']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TestingUtilsModule,
        ReactiveFormsModule,
        TranslateModule.forRoot(),
        CKEditorModule,
      ],
      providers: [
        {
          provide: ReferentialDataService,
          useValue: referentialDataService
        },
      ],
      declarations: [CreateEntityFormComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEntityFormComponent);
    component = fixture.componentInstance;
    component.data = {
      id: '-1',
      titleKey: 'test',
      projectId: -1
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
