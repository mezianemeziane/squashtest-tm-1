import {ChangeDetectionStrategy, Component, HostListener, OnInit} from '@angular/core';
import {DialogReference} from '../../../model/dialog-reference';
import {AlertConfiguration} from './alert-configuration';
import {KeyNames} from '../../../../utils/key-names';

@Component({
  selector: 'sqtm-core-alert-dialog',
  templateUrl: './alert-dialog.component.html',
  styleUrls: ['./alert-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlertDialogComponent implements OnInit {

  configuration: AlertConfiguration;

  constructor(private dialogReference: DialogReference<AlertConfiguration, boolean>) {
    this.configuration = dialogReference.data;
  }

  ngOnInit() {
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    if (event.key === KeyNames.ENTER || event.key === KeyNames.ESCAPE) {
      this.dialogReference.close();
    }
  }

  getTranslateParameters(): any {
    const params = this.configuration.translateParameters;

    if (params == null) {
      return {};
    } else if (Array.isArray(params)) {
      const r = {};
      params.forEach((param, idx) => r[idx.toString()] = param);
      return r;
    }
    return params;
  }
}
