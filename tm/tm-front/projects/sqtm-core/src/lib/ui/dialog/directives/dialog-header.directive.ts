import {Directive, ElementRef, HostListener, Inject, NgZone, OnInit} from '@angular/core';
import {Overlay} from '@angular/cdk/overlay';
import {fromEvent} from 'rxjs';
import {DOCUMENT} from '@angular/common';
import {take, takeUntil} from 'rxjs/operators';
import {DialogReference} from '../model/dialog-reference';

/** @dynamic */
@Directive({
  selector: '[sqtmCoreDialogHeader]'
})
export class DialogHeaderDirective implements OnInit {

  private dragging = false;

  constructor(private dialogReference: DialogReference, private host: ElementRef, private overlay: Overlay,
              @Inject(DOCUMENT) private document: Document, private zone: NgZone) {
  }

  ngOnInit(): void {
  }

  @HostListener('mousedown', ['$event'])
  onMouseDown($event: MouseEvent) {
    if (!this.dragging) {
      this.zone.runOutsideAngular(() => {
        this.dragging = true;
        const mouseUpStream = fromEvent(this.document.documentElement, 'mouseup').pipe(
          take(1)
        );

        const mouseMoveStream = fromEvent(this.document.documentElement, 'mousemove').pipe(
          takeUntil(mouseUpStream)
        );

        mouseUpStream.subscribe(($mouseUpEvent: MouseEvent) => {
          this.moveDialog($mouseUpEvent, $event);
          this.dragging = false;
        });

        mouseMoveStream.subscribe(($mouseUpEvent: MouseEvent) => {
          this.moveDialog($mouseUpEvent, $event);
        });
      });
    }
  }

  private moveDialog($mouseUpEvent: MouseEvent, $startMoveEvent: MouseEvent) {
    const positionStrategy = this.overlay.position()
      .global()
      .left(`${$mouseUpEvent.clientX - $startMoveEvent['layerX']}px`)
      .top(`${$mouseUpEvent.clientY - $startMoveEvent['layerY']}px`);

    this.dialogReference.overlayReference.updatePositionStrategy(positionStrategy);
    this.dialogReference.overlayReference.updatePosition();
  }
}
