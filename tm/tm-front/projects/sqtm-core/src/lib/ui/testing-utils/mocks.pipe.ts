import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'capitalize'
})
export class CapitalizeMockPipe implements PipeTransform {
  public name = 'capitalize';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

@Pipe({name: 'convertFileSize'})
export class ConvertFileSizePipeMock implements PipeTransform {
  transform(): any {
    return 'mock-convert-file-size-pipe';
  }
}

@Pipe({
  name: 'translate'
})
export class TranslateMockPipe implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}
