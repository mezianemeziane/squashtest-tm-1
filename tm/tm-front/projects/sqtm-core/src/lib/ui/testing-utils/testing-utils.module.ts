import {NgModule} from '@angular/core';
import {APP_BASE_HREF, CommonModule} from '@angular/common';
import {
  BACKEND_CONTEXT_PATH,
  CORE_MODULE_CONFIGURATION,
  CORE_MODULE_USER_CONFIGURATION,
  SQTM_MAIN_APP_IDENTIFIER
} from '../../core/sqtm-core.tokens';
import {sqtmConfigurationFactory} from '../../core/sqtm-core.module';
import {CapitalizeMockPipe, ConvertFileSizePipeMock, TranslateMockPipe} from './mocks.pipe';
import {IconDefinition} from '@ant-design/icons-angular';
import * as AllIcons from '@ant-design/icons-angular/icons';
import { NZ_ICONS, NzIconModule } from 'ng-zorro-antd/icon';

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};

const navIcon: string[] = [
  'requirement-workspace',
  'test-case-workspace',
  'campaign-workspace',
  'custom-report-workspace',
  'automation-workspace',
  'bugtrackers',
  'project-filter',
  'help',
  'action-library-workspace',
];

const adminIcon: string[] = [
  'automation',
  'bugtracker',
  'connection_history',
  'custom_field',
  'execution',
  'infolist',
  'information',
  'milestone',
  'plugin',
  'project',
  'requirements_link',
  'scm_server',
  'team',
  'template',
  'user',
  'external_link',
];

const genericIcon: string[] = [
  'delete',
  'copy',
  'plus',
  'paste',
  'more',
  'add',
  'unlink'
];

const testCaseIcons: string[] = [
  'status',
  'double_up'
];

function iconFactory() {
  const icons: IconDefinition[] = Object.keys(antDesignIcons).map(function (key) {
    const i = antDesignIcons[key];
    return i;
  });
  icons.push(...buildIconNamespace('sqtm-core-nav', navIcon));
  icons.push(...buildIconNamespace('sqtm-core-administration', adminIcon));
  icons.push(...buildIconNamespace('sqtm-core-generic', genericIcon));
  icons.push(...buildIconNamespace('sqtm-core-test-case', testCaseIcons));
  return icons;
}

function buildIconNamespace(nameSpace: string, names: string[]): IconDefinition[] {
  return names.map(name => `${nameSpace}:${name}`).map(prefixedName => ({name: prefixedName, icon: '<svg></svg>'}));
}

// @dynamic
@NgModule({
  declarations: [CapitalizeMockPipe, TranslateMockPipe, ConvertFileSizePipeMock],
  imports: [
    CommonModule
  ],
  providers: [
    {
      provide: CORE_MODULE_USER_CONFIGURATION,
      useValue: {pluginIdentifier: SQTM_MAIN_APP_IDENTIFIER}
    },
    {
      provide: CORE_MODULE_CONFIGURATION,
      useFactory: sqtmConfigurationFactory,
      deps: [CORE_MODULE_USER_CONFIGURATION]
    },
    {
      provide: NZ_ICONS,
      useFactory: iconFactory
    },
    {
      provide: APP_BASE_HREF,
      useValue: ''
    },
    {
      provide: BACKEND_CONTEXT_PATH,
      useValue: ''
    }
  ],
  exports: [TranslateMockPipe, CapitalizeMockPipe, ConvertFileSizePipeMock, NzIconModule]
})
export class TestingUtilsModule {
}
