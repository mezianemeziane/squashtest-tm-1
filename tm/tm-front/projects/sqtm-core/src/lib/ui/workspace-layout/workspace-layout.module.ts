import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WorkspaceWithTreeComponent} from './components/workspace-with-tree/workspace-with-tree.component';
import {NavBarModule} from '../navbar/nav-bar.module';
import {RouterModule} from '@angular/router';
import {WorkspaceCommonModule} from '../workspace-common/workspace-common.module';
import {UiManagerModule} from '../ui-manager/ui-manager.module';
import {FoldButtonComponent} from './components/fold-button/fold-button.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import {CloseButtonComponent} from './components/close-button/close-button.component';
import {WorkspaceWithGridComponent} from './components/workspace-with-grid/workspace-with-grid.component';
import {TranslateModule} from '@ngx-translate/core';
import { EmptyWorkspaceWithNavbarComponent } from './components/empty-workspace-with-navbar/empty-workspace-with-navbar.component';
import { TreeKeyboardShortcutDirective } from './directives/tree-keyboard-shortcut.directive';

@NgModule({
  declarations: [
    WorkspaceWithTreeComponent,
    FoldButtonComponent,
    CloseButtonComponent,
    WorkspaceWithGridComponent,
    EmptyWorkspaceWithNavbarComponent,
    TreeKeyboardShortcutDirective
  ],
  imports: [
    CommonModule,
    NavBarModule,
    RouterModule,
    WorkspaceCommonModule,
    UiManagerModule,
    NzIconModule,
    NzToolTipModule,
    TranslateModule.forChild(),
  ],
  exports: [
    WorkspaceWithTreeComponent,
    FoldButtonComponent,
    CloseButtonComponent,
    WorkspaceWithGridComponent,
    EmptyWorkspaceWithNavbarComponent,
    TreeKeyboardShortcutDirective
  ]
})
export class WorkspaceLayoutModule {
}
