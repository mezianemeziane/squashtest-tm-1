import {Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef, Input} from '@angular/core';
import {LicenseInformationState} from '../../../../core/referential/state/license-information.state';
import {NavBarComponent} from '../../../navbar/containers/nav-bar/nav-bar.component';

@Component({
  selector: 'sqtm-core-empty-workspace-with-navbar',
  templateUrl: './empty-workspace-with-navbar.component.html',
  styleUrls: ['./empty-workspace-with-navbar.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmptyWorkspaceWithNavbarComponent implements OnInit {

  @Input()
  name: string;

  private _theme: string;

  get theme(): string {
    return this._theme || this.name;
  }

  @Input()
  set theme(value: string) {
    this._theme = value;
  }

  @Input()
  licenseInformation: LicenseInformationState;

  @Input()
  isLoggedAsAdmin: boolean;

  @ViewChild(NavBarComponent, {read: ElementRef, static: true})
  navBar: ElementRef;


  @ViewChild('workspace', {read: ElementRef, static: true})
  workspace: ElementRef;

  constructor() { }

  ngOnInit(): void {

  }

}
