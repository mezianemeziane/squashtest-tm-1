import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {CUF_DATE_FORMAT, CustomFieldFormControlValue} from '../custom-field-value.utils';
import {CKEditor4} from 'ckeditor4-angular';
import {createCkEditorConfig} from '../../utils/ck-editor-config';
import {TranslateService} from '@ngx-translate/core';
import {parse} from 'date-fns';
import {buildErrorI18nKeyProvider, ErrorI18nKeyProvider} from '../../workspace-common/errors/ErrorKeyProvider';
import {customFieldLogger} from '../custom-field.logger';
import EditorType = CKEditor4.EditorType;
import {CustomField} from '../../../model/customfield/customfield.model';
import {InputType} from '../../../model/customfield/input-type.model';
import {DateFormatUtils} from '../../../core/utils/date-format.utils';

const logger = customFieldLogger.compose('CustomFieldFormComponent');

type CufErrorMap = { [p in number]: ErrorI18nKeyProvider[] };

@Component({
  selector: 'sqtm-core-custom-field-form',
  templateUrl: './custom-field-form.component.html',
  styleUrls: ['./custom-field-form.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomFieldFormComponent implements OnInit {

  private _customFields: CustomField[];

  get customFields(): CustomField[] {
    return this._customFields;
  }

  @Input()
  set customFields(value: CustomField[]) {
    if (!Boolean(this._customFields)) {
      this._customFields = value;
    }
  }

  @Input()
  parentFormGroup: FormGroup;

  @Input()
  alignLabelRight = false;

  get customFieldControls() {
    return (this.parentFormGroup.controls.customFields as FormGroup);
  }

  private customFieldFormGroup: FormGroup;

  @ViewChild('txtField', {static: true})
  private txtFieldTemplate: TemplateRef<CufTemplateContext>;

  @ViewChild('richTextField', {static: true})
  private richTextFieldTemplate: TemplateRef<CufTemplateContext>;

  @ViewChild('tagField', {static: true})
  private tagFieldTemplate: TemplateRef<CufTemplateContext>;

  @ViewChild('dropdownField', {static: true})
  private dropdownFieldTemplate: TemplateRef<CufTemplateContext>;

  @ViewChild('checkboxField', {static: true})
  private checkboxFieldTemplate: TemplateRef<CufTemplateContext>;

  @ViewChild('dateField', {static: true})
  private dateFieldTemplate: TemplateRef<CufTemplateContext>;

  @ViewChild('numericField', {static: true})
  private numericFieldTemplate: TemplateRef<CufTemplateContext>;

  ckEditorConfig: CKEditor4.Config;

  type = EditorType.DIVAREA;

  get datePickerFormat() {
    return DateFormatUtils.shortDate(this.translateService.getBrowserLang());
  }

  private errorsByCuf: CufErrorMap = {};


  constructor(private translateService: TranslateService, private cdRef: ChangeDetectorRef) {
    this.ckEditorConfig = createCkEditorConfig(translateService.getBrowserLang());
  }

  ngOnInit() {
    this.createFormControls();
  }

  createFormControls() {
    const formControls = this.customFields.reduce((controls, customField) => {
      const formFieldDefinition = createCustomFieldFormFieldDefinition(customField);
      controls[customField.id] = formFieldDefinition.generateFormControl();
      return controls;
    }, {});
    this.customFieldFormGroup = new FormGroup(formControls);
    this.parentFormGroup.addControl('customFields', this.customFieldFormGroup);
  }

  resetToDefaultValues() {
    this.customFields.forEach((customField) => {
      const control = this.customFieldFormGroup.controls[customField.id];
      const formFieldDefinition = createCustomFieldFormFieldDefinition(customField);
      control.reset(formFieldDefinition.getCustomFieldDefaultValue());
    });
  }

  getLayoutTemplate(customField: CustomField): TemplateRef<CufTemplateContext> {
    const inputType = customField.inputType;
    let template;
    switch (inputType) {
      case InputType.PLAIN_TEXT:
        template = this.txtFieldTemplate;
        break;
      case InputType.RICH_TEXT:
        template = this.richTextFieldTemplate;
        break;
      case InputType.DROPDOWN_LIST:
        template = this.dropdownFieldTemplate;
        break;
      case InputType.TAG:
        template = this.tagFieldTemplate;
        break;
      case InputType.CHECKBOX:
        template = this.checkboxFieldTemplate;
        break;
      case InputType.DATE_PICKER:
        template = this.dateFieldTemplate;
        break;
      case InputType.NUMERIC:
        template = this.numericFieldTemplate;
        break;
      default:
        throw Error(`Unknown cuf input type ${inputType}`);
    }
    return template;
  }

  getTagFieldOptions(customField: CustomField): Array<{ label: string; value: string }> {
    return customField.options.map(option => {
      return {label: option.label, value: option.label};
    });
  }

  getErrors(customField: CustomField): ErrorI18nKeyProvider[] {
    return this.errorsByCuf[customField.id] || [];
  }

  showClientSideErrors() {
    logger.debug('Try to find errors in cuf form');
    this.errorsByCuf = this.customFields
      .reduce((errorsMap, customField) => {
        return this.appendCustomFieldErrors(customField, errorsMap);
      }, {});
    logger.debug('Cuf errors map : ', [this.errorsByCuf]);
    this.cdRef.detectChanges();
  }

  private appendCustomFieldErrors(customField: CustomField, errorsMap: CufErrorMap): CufErrorMap {
    const control = this.customFieldFormGroup.get([customField.id]);
    if (control.errors) {
      const errors = this.extractControlErrors(control);
      errorsMap[customField.id] = errors;
      logger.debug(`Found errors for cuf : ${customField.name}`, [errors]);
    }
    return errorsMap;
  }

  private extractControlErrors(control: AbstractControl): ErrorI18nKeyProvider[] {
    return Object
      .entries(control.errors)
      .map(([fieldName, errorValue]: [string, any]) => buildErrorI18nKeyProvider(fieldName, errorValue));
  }
}

interface CufTemplateContext {
  customField: CustomField;
}

abstract class CustomFieldFormFieldDefinition {

  protected constructor(protected customField: CustomField) {
  }

  abstract getCustomFieldDefaultValue(): CustomFieldFormControlValue;

  abstract getCustomFieldValidators(): ValidatorFn[];

  generateFormControl(): FormControl {
    return new FormControl(this.getCustomFieldDefaultValue(), this.getCustomFieldValidators());
  }

  protected isRequired() {
    return !this.customField.optional;
  }
}

class PlainTextFormFieldDefinition extends CustomFieldFormFieldDefinition {
  constructor(protected customField: CustomField) {
    super(customField);
  }

  getCustomFieldDefaultValue(): string {
    return this.customField.defaultValue;
  }

  getCustomFieldValidators(): ValidatorFn[] {
    const validators = [Validators.maxLength(255)];
    if (this.isRequired()) {
      validators.push(Validators.required, Validators.pattern('(.|\\s)*\\S(.|\\s)*'));
    }
    return validators;
  }
}

class DropdownFormFieldDefinition extends CustomFieldFormFieldDefinition {
  constructor(protected customField: CustomField) {
    super(customField);
  }

  getCustomFieldDefaultValue(): string {
    return this.customField.defaultValue;
  }

  getCustomFieldValidators(): ValidatorFn[] {
    return [];
  }
}

class TagFormFieldDefinition extends CustomFieldFormFieldDefinition {
  constructor(protected customField: CustomField) {
    super(customField);
  }

  getCustomFieldDefaultValue(): string[] {
    return this.customField.defaultValue.split('|');
  }

  getCustomFieldValidators(): ValidatorFn[] {
    const validators = [];
    if (this.isRequired()) {
      validators.push(Validators.required, Validators.minLength(1));
    }
    return validators;
  }
}

class RichTextFormFieldDefinition extends CustomFieldFormFieldDefinition {
  constructor(protected customField: CustomField) {
    super(customField);
  }

  getCustomFieldDefaultValue(): string {
    return this.customField.largeDefaultValue;
  }

  getCustomFieldValidators(): ValidatorFn[] {
    const validators = [];
    if (this.isRequired()) {
      validators.push(
        Validators.required,
        Validators.minLength(1));
    }
    return validators;
  }
}

class CheckBoxFormFieldDefinition extends CustomFieldFormFieldDefinition {
  constructor(protected customField: CustomField) {
    super(customField);
  }

  getCustomFieldDefaultValue(): boolean {
    return this.customField.defaultValue === 'true';
  }

  getCustomFieldValidators(): ValidatorFn[] {
    return [];
  }
}

class NumericFormFieldDefinition extends CustomFieldFormFieldDefinition {
  constructor(protected customField: CustomField) {
    super(customField);
  }

  getCustomFieldDefaultValue(): number {
    return this.customField.numericDefaultValue;
  }

  getCustomFieldValidators(): ValidatorFn[] {
    const validators = [];
    if (this.isRequired()) {
      validators.push(
        Validators.required,
        Validators.pattern('(.|\\s)*\\S(.|\\s)*'));
    }
    return validators;
  }
}

class DateFormFieldDefinition extends CustomFieldFormFieldDefinition {
  constructor(protected customField: CustomField) {
    super(customField);
  }

  getCustomFieldDefaultValue(): Date {
    let defaultValue: Date = null;
    if (Boolean(this.customField.defaultValue)) {
      defaultValue = parse(this.customField.defaultValue as string, CUF_DATE_FORMAT, new Date());
    }
    return defaultValue;
  }

  getCustomFieldValidators(): ValidatorFn[] {
    return [];
  }
}

export function createCustomFieldFormFieldDefinition(customField: CustomField): CustomFieldFormFieldDefinition {
  let definition: CustomFieldFormFieldDefinition;
  const inputType = customField.inputType;
  switch (inputType) {
    case InputType.PLAIN_TEXT:
      definition = new PlainTextFormFieldDefinition(customField);
      break;
    case InputType.DROPDOWN_LIST:
      definition = new DropdownFormFieldDefinition(customField);
      break;
    case InputType.TAG:
      definition = new TagFormFieldDefinition(customField);
      break;
    case InputType.RICH_TEXT:
      definition = new RichTextFormFieldDefinition(customField);
      break;
    case InputType.CHECKBOX:
      definition = new CheckBoxFormFieldDefinition(customField);
      break;
    case InputType.NUMERIC:
      definition = new NumericFormFieldDefinition(customField);
      break;
    case InputType.DATE_PICKER:
      definition = new DateFormFieldDefinition(customField);
      break;
    default:
      throw Error(`Unknown cuf input type ${inputType}`);
  }
  return definition;
}
