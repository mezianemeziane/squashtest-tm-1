import {FormGroup} from '@angular/forms';
import {format} from 'date-fns';
import {CustomField, RawValueMap} from '../../model/customfield/customfield.model';
import {InputType} from '../../model/customfield/input-type.model';

export const CUF_DATE_FORMAT = 'yyyy-MM-dd';

// This type represent all the types that custom fields from controls can produce
export type CustomFieldFormControlValue = string | number | boolean | Date | string[];

// Convert a form group created by CustomValueFormComponent to the required RawValue map for server
export function convertFormControlValues(customFields: CustomField[], formGroup: FormGroup): RawValueMap {
  return customFields.reduce((rawValueMap, customField) => {
    rawValueMap[customField.id] = convertFormControlValue(customField, formGroup.controls[customField.id].value);
    return rawValueMap;
  }, {});
}

// Convert the strongly typed form control value to our fantastic loosely typed string system...
export function convertFormControlValue(customField: CustomField, rawValue: CustomFieldFormControlValue): string | string[] {
  // Checking only null as some valid values could be filtered if using native type coercion
  // Aka numeric field with value = 0
  // Additional checks could be required for some inputTypes.
  if (rawValue == null) {
    return '';
  }
  const inputType = customField.inputType;
  let convertedValue: string | string[];
  switch (inputType) {
    case InputType.PLAIN_TEXT:
    case InputType.RICH_TEXT:
    case InputType.DROPDOWN_LIST:
      convertedValue = rawValue as string;
      break;
    case InputType.NUMERIC:
      convertedValue = (rawValue as number).toString();
      break;
    case InputType.TAG:
      convertedValue = rawValue as string[];
      break;
    case InputType.CHECKBOX:
      convertedValue = Boolean(rawValue).toString();
      break;
    case InputType.DATE_PICKER:
      if (rawValue) {
        convertedValue = format(rawValue as Date, CUF_DATE_FORMAT);
      } else {
        convertedValue = '';
      }
      break;
    default:
      throw Error(`Unknown cuf input type ${inputType}`);
  }
  return convertedValue;
}
