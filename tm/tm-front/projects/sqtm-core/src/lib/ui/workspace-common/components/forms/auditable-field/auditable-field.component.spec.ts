import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {AuditableFieldComponent} from './auditable-field.component';
import {DatePipe} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';

describe('AuditableFieldComponent', () => {
  let component: AuditableFieldComponent;
  let fixture: ComponentFixture<AuditableFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AuditableFieldComponent],
      providers: [
        {
          provide: DatePipe,
          useValue: jasmine.createSpyObj(['transform'])
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['getBrowserLang', 'instant'])
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditableFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
