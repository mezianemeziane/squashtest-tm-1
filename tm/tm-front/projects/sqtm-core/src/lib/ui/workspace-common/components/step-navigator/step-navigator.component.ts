import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'sqtm-core-step-navigator',
  templateUrl: './step-navigator.component.html',
  styleUrls: ['./step-navigator.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StepNavigatorComponent implements OnInit {

  @Input()
  totalStep: number;

  @Input()
  currentStep: number;

  @Input()
  minimumStep = -1;

  @Input()
  disabled = false;

  @Output()
  navigateTo: EventEmitter<number> = new EventEmitter<number>();

  get currentDisplayedStep(): number {
    return this.currentStep + 1;
  }

  constructor() {
  }

  ngOnInit() {
  }

  navigateBackward() {
    this.navigateTo.emit(this.currentStep - 1);
  }

  navigateForward() {
    this.navigateTo.emit(this.currentStep + 1);
  }

  isLastStep() {
    return this.currentDisplayedStep === this.totalStep;
  }

  navigateToAnyStep(value: number) {
    this.navigateTo.emit(value - 1);
  }

  isFirstStep() {
    return this.currentStep === this.minimumStep;
  }
}
