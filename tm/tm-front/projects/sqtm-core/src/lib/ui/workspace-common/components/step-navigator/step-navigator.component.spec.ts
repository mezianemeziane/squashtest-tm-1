import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {StepNavigatorComponent} from './step-navigator.component';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {OnPushComponentTester} from '../../../testing-utils/on-push-component-tester';
import {WorkspaceCommonModule} from '../../workspace-common.module';


class StepNavigatorTester extends OnPushComponentTester<StepNavigatorComponent> {

  getBackwardButton() {
    return this.element('[data-test-component-id="step-backward"]');
  }

  getForwardButton() {
    return this.element('[data-test-component-id="step-forward"]');
  }

  getCounter() {
    return this.element('[data-test-component-id="step-counter"]').textContent;
  }

  isForwardButtonDisabled() {
    return this.getForwardButton().attr('disabled') != null;
  }
}

describe('StepNavigatorComponent', () => {
  let tester: StepNavigatorTester;
  let fixture: ComponentFixture<StepNavigatorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [NzButtonModule, NzIconModule, WorkspaceCommonModule, TranslateModule.forRoot()],
      declarations: [StepNavigatorComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepNavigatorComponent);
    tester = new StepNavigatorTester(fixture);
    tester.componentInstance.totalStep = 3;
    tester.componentInstance.currentStep = 1;
    fixture.detectChanges();
  });

  it('should create show counter', () => {
    expect(tester.getCounter()).toEqual(' 2 /3');
  });


  it('should disable forward button if last step', () => {
    tester.componentInstance.currentStep = 2;
    tester.detectChanges();
    expect(tester.getCounter()).toEqual(' 3 /3');
    expect(tester.isForwardButtonDisabled()).toBeTruthy();
  });
});
