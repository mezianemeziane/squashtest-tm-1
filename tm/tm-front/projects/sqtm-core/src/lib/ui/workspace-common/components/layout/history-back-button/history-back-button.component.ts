import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'sqtm-core-history-back-button',
  template: `
    <div class="back-button flex-column __hover_pointer"
         (click)="handleBackButtonClicked()"
         [attr.data-test-button-id]="'back'">
      <i nz-icon
         nzType="sqtm-core-generic:back"
         nzTheme="outline"
         class="current-workspace-button action-icon-size m-auto"
         nz-tooltip
         [nzTooltipTitle]="'sqtm-core.generic.label.return' | translate">
      </i>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HistoryBackButtonComponent {

  constructor() {
  }

  handleBackButtonClicked(): void {
    history.back();
  }
}
