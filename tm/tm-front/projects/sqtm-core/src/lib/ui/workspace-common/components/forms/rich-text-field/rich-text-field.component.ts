import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, ViewChild} from '@angular/core';
import {AbstractFormField} from '../abstract-form-field';
import {FormGroup} from '@angular/forms';
import {FieldValidationError} from '../../../../../model/error/error.model';
import {CKEditor4, CKEditorComponent} from 'ckeditor4-angular';
import EditorType = CKEditor4.EditorType;
import {TranslateService} from '@ngx-translate/core';
import {createCkEditorConfig} from '../../../../utils/ck-editor-config';
import {SessionPingService} from '../../../../../core/services/session-ping/session-ping.service';

@Component({
  selector: 'sqtm-core-rich-text-field',
  templateUrl: './rich-text-field.component.html',
  styleUrls: ['./rich-text-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RichTextFieldComponent  extends AbstractFormField implements AfterViewInit, OnDestroy {

  @Input()
  formGroup: FormGroup;

  @Input()
  fieldName: string;

  /** If set to true, it will give the focus to this component at page initialisation */
  @Input()
  initialFocus = false;

  @Input()
  reducedHeight = false;

  @ViewChild(CKEditorComponent)
  ckEditorComponent: CKEditorComponent;

  @Input()
  set serverSideFieldValidationError(fieldsValidationErrors: FieldValidationError[]) {
    this.showServerSideError(fieldsValidationErrors);
  }

  ckConfig: CKEditor4.Config;

  ckEditorType = EditorType.DIVAREA;

  constructor(cdRef: ChangeDetectorRef,
              translateService: TranslateService,
              private readonly sessionPingService: SessionPingService) {
    super(cdRef);
    this.ckConfig = createCkEditorConfig(translateService.getBrowserLang());
  }


  ngAfterViewInit(): void {
    this.sessionPingService.beginLongTermEdition();

    if (this.initialFocus) {
      this.focusInputField();
    }
  }

  ngOnDestroy(): void {
    this.sessionPingService.endLongTermEdition();
  }

  reduceHeightForRichTextField($event: CKEditor4.EventInfo) {
    if (this.reducedHeight) {
      $event.editor.resize('100%', 200);
    }
  }

  grabFocus() {
    this.ckEditorComponent.instance.focus();
  }

  private focusInputField() {
    this.ckConfig['startupFocus'] = true;
  }

}
