import {combineLatest, Subject} from 'rxjs';
import {
  concatMap,
  distinctUntilChanged,
  filter,
  map,
  skip,
  skipUntil,
  take,
  takeUntil,
  tap,
  withLatestFrom
} from 'rxjs/operators';
import {wsCommonLogger} from '../../workspace.common.logger';
import {RestService} from '../../../../core/services/rest.service';
import {GridService} from '../../../grid/services/grid.service';
import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';
import {GridPersistenceService} from '../../../../core/services/grid-persistence/grid-persistence.service';
import camelcase from 'camelcase';
import {Router} from '@angular/router';
import {GridStateSnapshot} from '../../../../core/services/grid-persistence/grid-state-snapshot';
import {DataRow, GridResponse, TreeRequest} from '../../../grid/model/data-row.model';
import {Sort} from '../../../grid/model/column-definition.model';
import {MilestoneModeData} from '../../../../core/referential/state/milestone-filter.state';
import {FilterValueKind, NumericFilterValue} from '../../../filters/state/filter.state';
import {GridFilter} from '../../../grid/model/state/filter.state';
import {
  buildSingleMilestonePickerDialogDefinition,
  SingleSelectMilestoneDialogConfiguration
} from '../../../milestone/component/single-milestone-picker-dialog/milestone.dialog.configuration';
import {Milestone} from '../../../../model/milestone/milestone.model';
import {ChangeDetectorRef, Directive, ViewContainerRef} from '@angular/core';
import {DialogService} from '../../../dialog/services/dialog.service';
import {Identifier} from '../../../../model/entity.model';

const logger = wsCommonLogger.compose('TreeWithStatePersistence');

// same as SquashTmDataRowType but for nodes in trees
enum SquashTmNodeTypes {
  TestCaseLibrary = 'TestCaseLibrary',
  TestCaseFolder = 'TestCaseFolder',
  TestCase = 'TestCase',
  RequirementLibrary = 'RequirementLibrary',
  RequirementFolder = 'RequirementFolder',
  Requirement = 'Requirement',
  CampaignLibrary = 'CampaignLibrary',
  CampaignFolder = 'CampaignFolder',
  Campaign = 'Campaign',
  Iteration = 'Iteration',
  TestSuite = 'TestSuite',
  CustomReportLibrary = 'CustomReportLibrary',
  CustomReportFolder = 'CustomReportFolder',
  ChartDefinition = 'ChartDefinition',
  ReportDefinition = 'ReportDefinition',
  CustomReportDashboard = 'CustomReportDashboard',
  CustomReportCustomExport = 'CustomReportCustomExport',
}

@Directive()
// tslint:disable-next-line:directive-class-suffix
export class TreeWithStatePersistence {


  unsub$ = new Subject<void>();

  private entityIdFromInitialUrl: string;

  public sortValue: 'ALPHABETICAL' | 'POSITIONAL';

  constructor(
    public tree: GridService,
    protected referentialDataService: ReferentialDataService,
    protected gridPersistenceService: GridPersistenceService,
    protected restService: RestService,
    protected router: Router,
    protected dialogService: DialogService,
    protected vcr: ViewContainerRef,
    protected persistenceKey: string,
    private readonly url: string
  ) {
  }

  protected initEntityIdFromInitialUrl() {
    const url = this.router.url.slice(1);
    const urlParts = url.split('/');
    logger.debug(`Try to retrieve entity id from url ${url}. Parts : `, [urlParts]);
    if (urlParts.length > 2) {
      const nodeType = camelcase(urlParts[1], {pascalCase: true});
      logger.debug(`Try to retrieve entity id from node type ${nodeType} `);
      if (Object.keys(SquashTmNodeTypes).includes(nodeType)) {
        this.entityIdFromInitialUrl = `${nodeType}-${urlParts[2]}`;
        logger.debug(`Retrieved valid entity id ${this.entityIdFromInitialUrl}. Should force node selection.`);
      }
    }
  }

  protected initData(configuration?: Partial<TreeWithStatePersistenceOptions>) {
    const options = {...DEFAULT_TREE_OPTIONS, ...configuration};
    this.gridPersistenceService.ready$.pipe(
      takeUntil(this.unsub$),
      filter((ready) => ready),
      take(1)
    ).subscribe(() => {
      this.initMilestoneFilter(options.filterByActiveMilestone);
      this.initProjectFilter(options.filterByProjectFilter);
      this.reloadTreeData(false, options.selectedNodes);
    });
  }

  /**
   * Reload all data for the tree. Typically when project filter or milestone change.
   */
  private reloadTreeData(showSpinner: boolean, selectedNodes?: Identifier[]) {
    let overwriteSelectedNodes: Identifier[] = this.entityIdFromInitialUrl ? [this.entityIdFromInitialUrl] : null;
    overwriteSelectedNodes = selectedNodes || overwriteSelectedNodes;
    this.gridPersistenceService.selectGridSnapshot(this.persistenceKey).pipe(
      take(1),
      tap(() => {
        if (showSpinner) {
          this.tree.beginAsyncOperation();
        }
      }),
      tap((snapshot: GridStateSnapshot) => {
        if (snapshot.noSnapshotExisting) {
          // default sort is on NAME columns in all trees
          this.tree.setColumnSorts([{id: 'NAME', sort: Sort.ASC}]);
        } else {
          this.tree.setColumnSorts(snapshot.sortedColumns);
        }
      }),
      concatMap((snapshot: GridStateSnapshot) => {
        logger.debug('loading workspace tree with snapshot ', [snapshot]);
        const treeRequest: TreeRequest = {
          openedNodes: snapshot.openedRows,
          selectedNodes: overwriteSelectedNodes || snapshot.selectedRowIds
        };
        return this.restService.post<GridResponse>([this.url], treeRequest).pipe(
          map(gridResponse => ({treeRequest, gridResponse}))
        );
      }),
    ).subscribe(({treeRequest, gridResponse}) => {
      const validIds = gridResponse.dataRows.map(row => row.id);
      const validSelectedNodes = treeRequest.selectedNodes.filter(id => validIds.includes(id));
      this.tree.loadInitialDataRows(gridResponse.dataRows, gridResponse.count, validSelectedNodes);
      if (showSpinner) {
        this.tree.completeAsyncOperation();
      }
    });
  }

  protected registerStatePersistence() {
    this.tree.loaded$.pipe(
      takeUntil(this.unsub$),
      filter(init => init),
      take(1)
    ).subscribe(() => {
      this.gridPersistenceService.register(this.persistenceKey, this.tree);
    });
  }

  protected initMilestoneFilter(filterByActiveMilestone: boolean) {
    if (filterByActiveMilestone) {
      this.initMilestoneBasicFilter();
      this.initTreeRefreshWhenActiveMilestoneChange();
    }

  }

  private initTreeRefreshWhenActiveMilestoneChange() {
    this.referentialDataService.milestoneModeData$.pipe(
      takeUntil(this.unsub$),
      skipUntil(this.tree.loaded$),
      filter((milestoneState: MilestoneModeData) => milestoneState.milestoneFeatureEnabled),
      distinctUntilChanged((stateA, stateB) => {
        let same = stateA.milestoneFeatureEnabled === stateB.milestoneFeatureEnabled
          && stateA.milestoneModeEnabled === stateB.milestoneModeEnabled;
        if (same && stateA.selectedMilestone && stateB.selectedMilestone) {
          same = stateA.selectedMilestone.id === stateB.selectedMilestone.id;
        }
        return same;
      }),
      tap(milestoneState => {
        if (milestoneState.milestoneFeatureEnabled && milestoneState.milestoneModeEnabled && milestoneState.selectedMilestone) {
          const value: NumericFilterValue = {
            kind: 'single-numeric-value' as FilterValueKind,
            value: milestoneState.selectedMilestone.id
          };
          this.tree.activateFilter('milestone');
          this.tree.changeFilterValue({id: 'milestone', filterValue: value});
        } else {
          this.tree.inactivateFilter('milestone');
        }
      }),
      skip(1)
    ).subscribe(() => {
      this.reloadTreeData(true);
    });
  }

  openMilestoneSelect() {
    this.referentialDataService.milestoneModeData$.pipe(
      take(1),
    ).subscribe(({milestones, selectedMilestone}) => {
      const dialogDefinition = buildSingleMilestonePickerDialogDefinition(milestones, selectedMilestone);
      dialogDefinition.viewContainerReference = this.vcr;
      const dialogReference = this.dialogService.openDialog<SingleSelectMilestoneDialogConfiguration, Milestone>(dialogDefinition);
      dialogReference.dialogClosed$.pipe(
        filter(nextSelectedMilestone => Boolean(nextSelectedMilestone))
      ).subscribe(nextSelectedMilestone => this.referentialDataService.enableMilestoneMode(nextSelectedMilestone.id));
    });
  }

  private initMilestoneBasicFilter() {
    const value: NumericFilterValue = {kind: 'single-numeric-value' as FilterValueKind, value: null};
    this.tree.addFilters([{
      id: 'milestone',
      value: value,
      active: false,
      initialValue: value,
      tiedToPerimeter: false,
      filterFunction: filterByMilestone
    }]);
  }

  protected unregisterStatePersistence() {
    this.gridPersistenceService.unregister(this.persistenceKey);
  }

  private initProjectFilter(filterByProjectFilter: boolean) {
    if (filterByProjectFilter) {
      combineLatest([this.referentialDataService.filterActivated$, this.referentialDataService.filteredProjectIds$]).pipe(
        takeUntil(this.unsub$),
        withLatestFrom(this.tree.loaded$),
        filter(([, loaded]) => loaded)
      ).subscribe(() => {
        this.reloadTreeData(true);
      });
    }

  }

  changeSort() {
    if (this.sortValue === 'ALPHABETICAL') {
      this.tree.setColumnSorts([{id: 'NAME', sort: Sort.ASC}]);
    } else {
      this.tree.setColumnSorts([]);
    }
  }

  protected initTreeSortMenu() {
    this.tree.sortedColumns$.pipe(
      takeUntil(this.unsub$)
    ).subscribe(sortedColumns => {
      if (sortedColumns.length > 0) {
        this.sortValue = 'ALPHABETICAL';
      } else {
        this.sortValue = 'POSITIONAL';
      }

      const changeDetectorRef = this.vcr.injector.get(ChangeDetectorRef);
      changeDetectorRef.detectChanges();
    });
  }
}

export interface TreeWithStatePersistenceOptions {
  filterByActiveMilestone: boolean;
  filterByProjectFilter: boolean;
  selectedNodes: Identifier[];
}

const DEFAULT_TREE_OPTIONS: Readonly<TreeWithStatePersistenceOptions> = {
  filterByActiveMilestone: true,
  filterByProjectFilter: true,
  selectedNodes: null
};

function filterByMilestone(gridFilter: GridFilter, row: DataRow): boolean {
  const filterValue = gridFilter.value as NumericFilterValue;
  const milestoneId = filterValue.value;
  const boundMilestones: number[] = row.data['ALL_MILESTONES'] || row.data['MILESTONES'] || [];
  return boundMilestones.includes(milestoneId);
}
