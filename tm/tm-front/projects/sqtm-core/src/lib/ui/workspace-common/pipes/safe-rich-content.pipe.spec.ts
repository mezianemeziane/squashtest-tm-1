import { SafeRichContentPipe } from './safe-rich-content.pipe';

describe('SafeRichContentPipe', () => {

  const domSanitizer = jasmine.createSpyObj('DomSanitizer', ['bypassSecurityTrustHtml']);
  domSanitizer.bypassSecurityTrustHtml.and.callFake((str: string) => str);

  const pipe = new SafeRichContentPipe(domSanitizer);

  it('should preserve styling', () => {
    const result = pipe.transform('<p style="color: red; text-align: center;">Hello, World!</p>');
    expect(result).toBe('<p style="color: red; text-align: center;">Hello, World!</p>');
  });
});
