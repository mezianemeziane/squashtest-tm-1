import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

/**
 * This component is intended to be used as a header in pages which displays a node information (Library, Folder, TestCase...).
 * It contains a left part including a title and an area which displays the content projection. A right part contains
 * the 'more' icon and the 'attachment' icon that displays the number of Attachments.
 */
@Component({
  selector: 'sqtm-core-entity-view-header',
  templateUrl: './entity-view-header.component.html',
  styleUrls: ['./entity-view-header.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EntityViewHeaderComponent implements OnInit {

  @Input()
  attachmentCount: number;

  @Input()
  displayDefaultTitle = true;

  @Output()
  attachmentBadgeClickEvent = new EventEmitter<void>();

  @Input()
  nameEditable = true;

  @Input()
  hasAttachmentList = true;

  constructor() {
  }

  ngOnInit() {
  }

  attachmentBadgeClick() {
    this.attachmentBadgeClickEvent.next();
  }
}
