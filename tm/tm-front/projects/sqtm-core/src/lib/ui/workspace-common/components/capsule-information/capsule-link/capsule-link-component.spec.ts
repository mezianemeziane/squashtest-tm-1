import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapsuleLinkComponent } from './capsule-link-component';
import {TranslateModule} from '@ngx-translate/core';

describe('CapsuleLinkComponent', () => {
  let component: CapsuleLinkComponent;
  let fixture: ComponentFixture<CapsuleLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ CapsuleLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapsuleLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
