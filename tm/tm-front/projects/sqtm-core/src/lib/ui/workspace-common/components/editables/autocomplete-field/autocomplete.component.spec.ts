import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AutocompleteComponent} from './autocomplete.component';
import {OverlayModule} from '@angular/cdk/overlay';

describe('AutocompleteComponent', () => {
  let component: AutocompleteComponent;
  let fixture: ComponentFixture<AutocompleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OverlayModule],
      declarations: [AutocompleteComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should show and hide option menu according to the options', () => {
  //   component.options = [];
  //   fixture.detectChanges();
  //   expect(component.isMenuOpen).toBeFalse();
  //   component.options =
  //     [
  //       'je me trouve sur la page de connexion',
  //       'je renseigne <login> dans le champ "login"',
  //       'je renseigne <password> dans le champ "password"',
  //       'je clique sur le bouton "connexion"',
  //       'je suis connecté'
  //     ];
  //   fixture.detectChanges();
  //   expect(component.isMenuOpen).toBeTrue();
  //   component.options = [];
  //   fixture.detectChanges();
  //   expect(component.isMenuOpen).toBeFalse();
  // });
});
