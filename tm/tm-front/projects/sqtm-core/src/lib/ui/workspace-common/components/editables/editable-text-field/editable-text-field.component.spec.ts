import {TestBed, waitForAsync} from '@angular/core/testing';

import {EditableTextFieldComponent} from './editable-text-field.component';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {TestElement} from 'ngx-speculoos';
import {By} from '@angular/platform-browser';
import {timer} from 'rxjs';
import {delay, tap} from 'rxjs/operators';
import {KeyCodes} from '../../../../utils/key-codes';
import {OnPushComponentTester} from '../../../../testing-utils/on-push-component-tester';


class EditableTextFieldComponentTester extends OnPushComponentTester<EditableTextFieldComponent> {
  constructor() {
    super(EditableTextFieldComponent);
  }

  get initialValueSpan(): TestElement<any> {
    return this.element('span');
  }

  get initialValueContent(): string {
    return this.initialValueSpan.textContent;
  }

  get inputField(): HTMLInputElement {
    const inputDe = this.fixture.debugElement.query(By.css('input'));
    return inputDe.nativeElement;
  }

  get cancelButton(): HTMLButtonElement {
    const htmlButtonElement = this.getButtons();
    if (htmlButtonElement && htmlButtonElement.length > 0) {
      return htmlButtonElement[0];
    }
    return null;
  }

  get cancelButtonLabel() {
    return this.getButtonLabel(this.cancelButton);
  }

  get confirmButton(): HTMLButtonElement {
    const htmlButtonElement = this.getButtons();
    if (htmlButtonElement && htmlButtonElement.length > 0) {
      return htmlButtonElement[1];
    }
    return null;
  }

  get confirmButtonLabel() {
    return this.getButtonLabel(this.confirmButton);
  }

  get errorMessages(): HTMLElement[] {
    return this.fixture.debugElement.queryAll(By.css('span.sqtm-core-error-message')).map(
      (dbgElem) => dbgElem.nativeElement);
  }

  private getButtonLabel(button: HTMLButtonElement) {
    if (button) {
      return button.textContent;
    }
    return null;
  }

  private getButtons(): HTMLButtonElement[] {
    return this.fixture.nativeElement.querySelectorAll('button');
  }
}

describe('EditableTextFieldComponent', () => {
  let tester: EditableTextFieldComponentTester;
  const translateService = jasmine.createSpyObj('TranslateService', ['instant']);

  interface DataType {
    editable: boolean;
    initialValue: string;
    expectedValue: string;
    layout: 'compact' | 'no-buttons' | 'default';
    shouldShowButtons: boolean;
    shouldShowButtonLabels: boolean;
  }

  const dataSets: DataType[] = [
    {
      editable: true,
      initialValue: 'initial value',
      expectedValue: 'initial value',
      layout: 'default',
      shouldShowButtons: true,
      shouldShowButtonLabels: true
    },
    {
      editable: true,
      initialValue: '',
      expectedValue: 'sqtm-core.generic.editable.placeholder',
      layout: 'compact',
      shouldShowButtons: true,
      shouldShowButtonLabels: false
    },
    {
      editable: true,
      initialValue: 'Test case 1',
      expectedValue: 'Test case 1',
      layout: 'no-buttons',
      shouldShowButtons: false,
      shouldShowButtonLabels: false
    }
  ];

  dataSets.forEach((dataSet, index) => runTest(dataSet, index));

  function runTest(data: DataType, index: number) {
    describe(`Dataset ${index}`, () => {

      beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
          imports: [TestingUtilsModule, ReactiveFormsModule],
          declarations: [EditableTextFieldComponent],
          providers: [{
            provide: TranslateService,
            useValue: translateService
          }],
          schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents()
          .then(() => {
            tester = new EditableTextFieldComponentTester();
            tester.componentInstance.editable = data.editable;
            tester.componentInstance.value = data.initialValue;
            tester.componentInstance.layout = data.layout;
            tester.componentInstance.ngOnInit();
            tester.detectChanges();
          });
      }));

      it(`It should show initial data`, (done) => {
        expect(tester).toBeTruthy();
        expect(tester.initialValueContent).toContain(data.expectedValue);
        expect(tester.cancelButton).toBeFalsy();
        expect(tester.confirmButton).toBeFalsy();
        done();
      });


      it(`It should pass into editable mode on click and value must be set into input field`, (done) => {
        activateEditMode();
        timer(100).subscribe(() => {
          tester.detectChanges();
          expect(tester.inputField).toBeTruthy();
          expect(tester.inputField.value).toEqual(data.initialValue);
          done();
        });
      });

      it(`It should show buttons and labels on buttons according to layout configuration`, (done) => {
        activateEditMode();
        timer(100)
          .subscribe(() => {
            tester.detectChanges();
            if (data.shouldShowButtons) {
              expect(tester.confirmButton).toBeTruthy();
              expect(tester.cancelButton).toBeTruthy();
            } else {
              expect(tester.confirmButton).toBeFalsy();
              expect(tester.cancelButton).toBeFalsy();
            }

            if (data.shouldShowButtonLabels) {
              expect(tester.confirmButtonLabel).toBeTruthy();
              expect(tester.cancelButtonLabel).toBeTruthy();
            } else {
              expect(tester.confirmButtonLabel).toBeFalsy();
              expect(tester.cancelButtonLabel).toBeFalsy();
            }
            done();
          });
      });

      it(`It should emit new values when typing`, (done) => {
        let outputValue: string;
        tester.componentInstance.keyupEvent.asObservable().subscribe(
          value => outputValue = value
        );
        activateEditMode();
        timer(100).pipe(
          tap(() => {
            tester.inputField.value = data.initialValue + 'a';
            tester.inputField.dispatchEvent(new Event('input'));
            tester.detectChanges();
          }),
          delay(10))
          .subscribe(() => {
            tester.componentInstance.handleKeyboardInput(65);
            expect(outputValue).toEqual(data.initialValue + 'a');
            done();
          });
      });

      it(`It should emit new value in confirm output when typing enter`, (done) => {
        let outputValue: string;
        tester.componentInstance.confirmEvent.asObservable().subscribe(
          value => outputValue = value
        );

        activateEditMode();
        timer(100).pipe(
          tap(() => {
            tester.inputField.value = data.initialValue + 'abcdef';
            tester.inputField.dispatchEvent(new Event('input'));
            tester.detectChanges();
          }),
          delay(10))
          .subscribe(() => {
            tester.componentInstance.handleKeyboardInput(KeyCodes.ENTER);
            expect(outputValue).toEqual(data.initialValue + 'abcdef');
            done();
          });
      });


      it(`It should cancel, emit the value in cancel output and restore initial value`, (done) => {
        let outputValue: string;
        tester.componentInstance.cancelEvent.asObservable().subscribe(
          value => outputValue = value
        );

        activateEditMode();
        timer(100).pipe(
          tap(() => {
            tester.inputField.value = data.initialValue + 'abcdef';
            tester.inputField.dispatchEvent(new Event('input'));
            tester.detectChanges();
          }),
          delay(10),
          tap(() => {
            tester.componentInstance.cancel();
            tester.componentInstance.cdRef.detectChanges();
          }),
          delay(10))
          .subscribe(() => {
            expect(tester.cancelButton).toBeFalsy();
            expect(tester.confirmButton).toBeFalsy();
            expect(tester.initialValueContent).toContain(data.initialValue);
            expect(outputValue).toEqual(data.initialValue + 'abcdef');
            done();
          });
      });

      it(`should show error when confirming blank value while being required`, (done) => {
        tester.componentInstance.required = true;
        tester.componentInstance.trimValue = true;
        tester.detectChanges();

        activateEditMode();
        timer(100).pipe(
          tap(() => {
            tester.inputField.value = '     ';
            tester.inputField.dispatchEvent(new Event('input'));
            tester.detectChanges();
          }),
          delay(10),
          tap(() => {
            tester.componentInstance.handleKeyboardInput(KeyCodes.ENTER);
            // tester.confirmButton.click();
            tester.detectChanges();
          }))
          .subscribe(() => {

            if (data.initialValue) {
              // There was a value so the value change should have been rejected with error message
              expect(tester.inputField.textContent).toEqual('');
              expect(tester.errorMessages.length).toEqual(1);
            } else {
              // No initial value, no value change, edit mode should have been left
              expect(tester.cancelButton).toBeFalsy();
              expect(tester.confirmButton).toBeFalsy();
              expect(tester.initialValueContent).toContain(data.initialValue);
            }

            done();
          });
      });

      it(`It should trim value when trimValue is set to true`, (done) => {
        tester.componentInstance.trimValue = true;
        tester.detectChanges();

        let outputValue: string;
        tester.componentInstance.confirmEvent.asObservable().subscribe(
          value => outputValue = value
        );

        activateEditMode();
        timer(100).pipe(
          tap(() => {
            tester.inputField.value = '    abcdef    ';
            tester.inputField.dispatchEvent(new Event('input'));
            tester.detectChanges();
          }),
          delay(10))
          .subscribe(() => {
            tester.componentInstance.handleKeyboardInput(KeyCodes.ENTER);
            expect(outputValue).toEqual('abcdef');
            done();
          });
      });

      function activateEditMode() {
        expect(tester).toBeTruthy();
        expect(tester.initialValueContent).toContain(data.expectedValue);
        expect(tester.cancelButton).toBeFalsy();
        expect(tester.confirmButton).toBeFalsy();
        const span = tester.fixture.debugElement.query(By.css('div.flex-row'));
        span.triggerEventHandler('click', null);
        tester.detectChanges();
      }

    });
  }
});
