import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChildren
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TextFieldComponent} from '../text-field/text-field.component';
import {
  BasicAuthCredentials,
  Credentials,
  isBasicAuthCredentials,
  isOAuth1aCredentials,
  isTokenAuthCredentials,
  OAuthCredentials,
  TokenAuthCredentials
} from '../../../../../model/third-party-server/credentials.model';
import {AuthenticationProtocol} from '../../../../../model/third-party-server/authentication.model';
import {Subject} from 'rxjs';

const TRANSLATE_KEYS_BASE = 'sqtm-core.administration-workspace.bugtrackers.authentication-policy.';

@Component({
  selector: 'sqtm-core-third-party-credentials-form',
  templateUrl: './third-party-credentials-form.component.html',
  styleUrls: ['./third-party-credentials-form.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThirdPartyCredentialsFormComponent implements OnInit, OnDestroy {

  formGroup: FormGroup;

  /**
   * If true, the authentication form is replaced by a 'Revoke' button whenever the auth protocol is OAuth.
   * e.g. 'My account' page where the user cannot manually enter OAuth credentials.
   */
  @Input() useRevokeButton = false;

  /**
   * Authentication protocol defined at the bugtracker level. Beware that the configured protocol MAY NOT
   * match the stored credentials. E.g. if you configure a BT to use basic auth, have users register their
   * credentials and then change the BT protocol to OAuth. Users will still have Basic Auth credentials registered!
   */
  private _authenticationProtocol: AuthenticationProtocol;

  @Input()
  set authenticationProtocol(value: AuthenticationProtocol) {
    this._authenticationProtocol = value;
    this.buildCredentialsFormGroup();
  }

  get authenticationProtocol(): AuthenticationProtocol {
    return this._authenticationProtocol;
  }

  @Input()
  set credentials(credentials: Credentials) {
    this._credentials = credentials;
    this.buildCredentialsFormGroup();
  }

  get credentials(): Credentials {
    if (this._authenticationProtocol !== this._credentials?.implementedProtocol) {
      return this.makeEmptyCredentials();
    } else {
      // Reconstitute the "full" credentials object in order to have `implementedProtocol` and
      // `type` properties that may not be defined in `_credentials`
      return { ...this.makeEmptyCredentials(), ...(this._credentials ?? {}) };
    }
  }

  @ViewChildren(TextFieldComponent)
  textFields: TextFieldComponent[];

  @Output()
  submit = new EventEmitter<Credentials>();

  @Output()
  revokeTokens = new EventEmitter<void>();

  // Used in template, should be kept internal
  readonly asyncMode$ = new Subject<boolean>();

  private _credentials: Credentials;

  private readonly _obfuscatedPassword = '••••••';
  private _hasRegisteredPassword = false;

  get passwordPlaceholder(): string {
    const shouldObfuscate = this._hasRegisteredPassword && this.formGroup?.pristine;
    return shouldObfuscate ? this._obfuscatedPassword : '';
  }

  get isFormVisible(): boolean {
    return !this.isRevokeButtonVisible;
  }

  get isRevokeButtonVisible(): boolean {
    return this.useRevokeButton && this._authenticationProtocol === AuthenticationProtocol.OAUTH_1A;
  }

  get isRevokeButtonEnabled(): boolean {
    return this.credentials.registered;
  }

  constructor(private readonly fb: FormBuilder,
              private readonly cdRef: ChangeDetectorRef) {
  }

  get loginTranslateKey(): string {
    return this.isOAuth ? TRANSLATE_KEYS_BASE + 'credentials.token' : 'sqtm-core.generic.label.login';
  }

  get passwordTranslateKey(): string {
    if (this.isOAuth) {
      return TRANSLATE_KEYS_BASE + 'credentials.tokenSecret';
    } else if (this.isTokenAuth) {
      return TRANSLATE_KEYS_BASE + 'credentials.token';
    } else {
      return 'sqtm-core.generic.label.password';
    }
  }

  get isOAuth(): boolean {
    return this._authenticationProtocol === AuthenticationProtocol.OAUTH_1A;
  }

  get isTokenAuth(): boolean {
    return this._authenticationProtocol === AuthenticationProtocol.TOKEN_AUTH;
  }

  ngOnInit(): void {
    this.buildCredentialsFormGroup();
  }

  ngOnDestroy(): void {
    this.asyncMode$.complete();
  }

  handleSubmit(): void {
    if (this.checkCredentialsFormValidity()) {
      this.beginAsync();

      if (this.isOAuth) {
        const token = this.formGroup.controls['username'].value;
        const tokenSecret = this.formGroup.controls['password'].value;
        const newCredentials = {
          ...this.credentials,
          token,
          tokenSecret,
        };
        this.submit.emit(newCredentials);
      } else if (this.isTokenAuth) {
        const token = this.formGroup.controls['password'].value;
        const newCredentials = {
          ...this.credentials,
          token,
        };
        this.submit.emit(newCredentials);
      } else {
        const username = this.formGroup.controls['username'].value;
        const password = this.formGroup.controls['password'].value;
        const newCredentials = {
          ...this.credentials,
          username,
          password,
        };
        this.submit.emit(newCredentials);
      }
    }
  }

  beginAsync(): void {
    this.asyncMode$.next(true);
  }

  endAsync(): void {
    this.asyncMode$.next(false);
  }

  private buildCredentialsFormGroup(): void {
    if (Boolean(this.credentials)) {
      this.buildFormGroupFromCredentials(this.credentials);
    } else {
      this.formGroup = this.fb.group({
        username: this.fb.control('', [Validators.required]),
        password: this.fb.control('', [Validators.required]),
      });
    }

    this._hasRegisteredPassword = Boolean(this.credentials) && this.credentials.registered;
    this.cdRef.detectChanges();
  }

  private buildFormGroupFromCredentials(currentCredentials: Credentials): void {
    if (isOAuth1aCredentials(currentCredentials)) {
      this.formGroup = this.fb.group({
        username: this.fb.control(currentCredentials.token, [Validators.required]),
        password: this.fb.control('', [Validators.required]),
      });
    } else if (isBasicAuthCredentials(currentCredentials)) {
      this.formGroup = this.fb.group({
        username: this.fb.control(currentCredentials.username, [Validators.required]),
        password: this.fb.control('', [Validators.required]),
      });
    } else if (isTokenAuthCredentials(currentCredentials)) {
      this.formGroup = this.fb.group({
        password: this.fb.control('', [Validators.required]),
      });
    } else {
      throw new Error('Unhandled credentials type.');
    }
  }

  private checkCredentialsFormValidity(): boolean {
    this.textFields.forEach(textField => textField.showClientSideError());
    return this.formGroup.valid;
  }

  handleRevoke(): void {
    this.revokeTokens.emit();
  }

  private makeEmptyCredentials(): Credentials {
    switch (this._authenticationProtocol) {
      case AuthenticationProtocol.BASIC_AUTH:
        return makeEmptyBasicAuthCredentials();
      case AuthenticationProtocol.OAUTH_1A:
        return makeEmptyOAuth1aCredentials();
      case AuthenticationProtocol.TOKEN_AUTH:
        return makeEmptyTokenAuthCredentials();
      default:
        return makeEmptyBasicAuthCredentials();
    }
  }
}

function makeEmptyBasicAuthCredentials(): BasicAuthCredentials {
  return {
    type: AuthenticationProtocol.BASIC_AUTH,
    implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
    username: '',
    password: '',
    registered: false,
  };
}

function makeEmptyOAuth1aCredentials(): OAuthCredentials {
  return {
    type: AuthenticationProtocol.OAUTH_1A,
    implementedProtocol: AuthenticationProtocol.OAUTH_1A,
    token: '',
    tokenSecret: '',
    registered: false,
  };
}

function makeEmptyTokenAuthCredentials(): TokenAuthCredentials {
  return {
    type: AuthenticationProtocol.TOKEN_AUTH,
    implementedProtocol: AuthenticationProtocol.TOKEN_AUTH,
    token: '',
    registered: false,
  };
}
