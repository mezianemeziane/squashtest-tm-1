import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {AbstractEditableField, EditableLayout} from '../abstract-editable-field';
import {EditableCustomField} from '../../../../custom-field/editable-custom-field';
import * as datefns from 'date-fns';
import {DateFormatUtils} from '../../../../../core/utils/date-format.utils';

@Component({
  selector: 'sqtm-core-editable-date-field',
  templateUrl: './editable-date-field.component.html',
  styleUrls: ['./editable-date-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableDateFieldComponent extends AbstractEditableField implements OnInit, EditableCustomField {

  edit = false;

  get format() {
    return DateFormatUtils.shortDate(this.translateService.getBrowserLang());
  }

  @Input()
  layout: EditableLayout = 'default';

  @Input()
  placeHolder: string;

  @Input()
  allowEmptyValue = false;

  model: Date = new Date();

  private _value: Date;

  pending: boolean;

  @Input()
  set value(value: Date) {
    this.updateAndClose(value);
  }

  get value(): Date {
    return this._value;
  }

  @Input()
  size: 'small' | 'default' | 'large' = 'default';

  @Output()
  confirmEvent = new EventEmitter<Date>();

  constructor(cdRef: ChangeDetectorRef, public translateService: TranslateService) {
    super(cdRef);
  }

  ngOnInit() {
  }

  enableEditMode() {
    if (!this.edit && this.editable) {
      this.edit = true;
      this.model = this.value;
    }
  }

  private updateAndClose(value: Date) {
    this._value = value;
    this.model = value;
    this.disableEditMode();
    this.endAsync();
    this.cdRef.detectChanges();
  }

  confirm() {
    this.executeAutoAsync();
    this.emitDate();
  }

  private emitDate() {
    if (! this.model) {
      this.confirmEvent.next(null);
    } else {
      let formattedDate = new Date(this.model);
      formattedDate = datefns.setHours(formattedDate, 0);
      formattedDate = datefns.setMinutes(formattedDate, 0);
      formattedDate = datefns.setSeconds(formattedDate, 0);
      formattedDate = datefns.setMilliseconds(formattedDate, 0);
      this.confirmEvent.next(formattedDate);
    }
  }

  cancel() {
    this.edit = false;
    this.model = this._value;
  }

  beginAsync() {
    this.pending = true;
  }

  endAsync() {
    this.pending = false;
  }

  disableEditMode() {
    this.edit = false;
  }
}
