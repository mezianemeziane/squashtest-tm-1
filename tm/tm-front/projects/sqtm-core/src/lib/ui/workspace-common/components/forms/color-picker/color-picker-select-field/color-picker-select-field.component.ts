import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef, TemplateRef, EmbeddedViewRef, ViewContainerRef, ChangeDetectorRef
} from '@angular/core';
import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {TemplatePortal} from '@angular/cdk/portal';

@Component({
  selector: 'sqtm-core-color-picker-select-field',
  templateUrl: './color-picker-select-field.component.html',
  styleUrls: ['./color-picker-select-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColorPickerSelectFieldComponent {

  @Input()
  color: string;

  @Input()
  cpPosition: 'auto' | 'top' | 'bottom' | 'left' | 'right' |
    'top-left' | 'top-right'| 'bottom-left'| 'bottom-right' = 'top';

  @Output()
  colorChanged = new EventEmitter<string>();

  @ViewChild('field')
  colorSelectField: ElementRef;

  @ViewChild('template')
  templateRef: TemplateRef<any>;

  private overlayRef: OverlayRef;

  private componentRef: EmbeddedViewRef<any>;

  constructor(private overlay: Overlay, private viewContainerRef: ViewContainerRef, public readonly cdRef: ChangeDetectorRef) {
  }

  get selectedColor(): string {
    return this.color;
  }

  set selectedColor(newColor: string) {
    this.color = newColor;

    this.cdRef.detectChanges();
  }

  edit(): void {

    const positionStrategy = this.overlay.position()
      .flexibleConnectedTo(this.colorSelectField)
      .withPositions([
        {originX: 'start', overlayX: 'start', originY: 'top', overlayY: 'center'},
      ]);

    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
    };

    this.overlayRef = this.overlay.create(overlayConfig);
    const templatePortal = new TemplatePortal(this.templateRef, this.viewContainerRef);
    this.componentRef = this.overlayRef.attach(templatePortal);

    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  handleColorSelectionChange(newColor: string) {
    this.color = newColor;

    this.colorChanged.emit(this.selectedColor);
    this.cdRef.detectChanges();
  }

  private close() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.componentRef = null;
    }

    this.cdRef.detectChanges();
  }
}
