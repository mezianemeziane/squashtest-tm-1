import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {Option} from '../../../../../model/option.model';
import {TranslateService} from '@ngx-translate/core';
import {InfoListItem} from '../../../../../model/infolist/infolistitem.model';
import {EditableSelectFieldComponent} from '../editable-select-field/editable-select-field.component';
import {EditableLayout} from '../abstract-editable-field';

@Component({
  selector: 'sqtm-core-editable-select-infolist-field',
  templateUrl: './editable-select-infolist-field.component.html',
  styleUrls: ['./editable-select-infolist-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableSelectInfolistFieldComponent implements OnInit {


  @ViewChild('child')
  child: EditableSelectFieldComponent;

  @Input()
  edit = false;

  @Input()
  pending = false;

  @Input()
  size: 'small' | 'default' | 'large' = 'default';

  @Input()
  layout: EditableLayout = 'default';

  @Input()
  set selectedItem(infoListItemId: number) {
    // The setTimeout is required to allow Angular to instantiate the childView before setting the value.
    setTimeout(() => {
      this.child.value = infoListItemId.toString();
    });
  }

  @Input()
  infoListItems: InfoListItem[] = [];

  private _editable = true;

  get editable(): boolean {
    return this._editable;
  }

  @Input()
  set editable(value: boolean) {
    this._editable = value;
    this.cdRef.markForCheck();
  }

  @Output() readonly confirmEvent = new EventEmitter<InfoListItem>();

  options: Option[] = [];

  actualOption: Option;

  constructor(private translate: TranslateService, private cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.convertToOption();
  }


  confirm(option: Option) {
    const value = this.infoListItems.find(item => item.id === parseInt(option.value, 10));
    this.confirmEvent.emit(value);
  }

  convertToOption() {
    this.options = [];
    this.infoListItems.forEach(infoListItem => this.options.push(this.infoListItemToOption(infoListItem)));
  }

  infoListItemToOption(infoListItem: InfoListItem): Option {
    const option = new Option();
    let label = '';
    if (infoListItem.system) {
      label = this.translate.instant(`sqtm-core.entity.${infoListItem.label}`);
    } else {
      label = infoListItem.label;
    }
    option.label = label;
    option.value = infoListItem.id.toString();
    return option;
  }

  beginAsync() {
    this.child.beginAsync();
  }

  endAsync() {
    this.child.endAsync();
  }
}
