import {Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'sqtm-core-toggle-icon',
  templateUrl: './toggle-icon.component.html',
  styleUrls: ['./toggle-icon.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToggleIconComponent implements OnInit {

  @Input()
  active = false;

  @Input()
  asyncRunning = false;

  @Input()
  stopPropagation = false;

  @Output()
  clickedInActiveMode = new EventEmitter<MouseEvent>();

  constructor() {
  }

  ngOnInit(): void {
  }

  handleClick($event: MouseEvent) {
    if (this.stopPropagation) {
      $event.stopPropagation();
    }
    if (this.active && !this.asyncRunning) {
      this.clickedInActiveMode.emit($event);
    }
  }
}
