import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EmbeddedViewRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewChildren,
  ViewContainerRef
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject, timer } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, takeUntil, tap } from 'rxjs/operators';
import { EditableCustomField } from '../../../../custom-field/editable-custom-field';
import { KeyCodes } from '../../../../utils/key-codes';
import { ActionAutocompleteFieldOptionComponent } from '../../forms/action-autocomplete-field/action-autocomplete-field-option/action-autocomplete-field-option.component';
import { AbstractEditableField, EditableField, EditableLayout } from '../abstract-editable-field';

@Component({
  selector: 'sqtm-core-editable-action-text-field',
  templateUrl: './editable-action-text-field.component.html',
  styleUrls: ['./editable-action-text-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableActionTextFieldComponent extends AbstractEditableField implements OnInit, EditableField, EditableCustomField {

  private readonly REQUIRED_KEY = 'sqtm-core.validation.errors.required';

  private _edit = false;

  set edit(isEdit: boolean) {
    this._edit = isEdit;
    this.cdRef.detectChanges();
  }

  get edit(): boolean {
    return this._edit;
  }

  dirty = false;

  pending = false;

  @Input()
  required = false;

  @Input()
  placeholder: string;

  @Input()
  size: 'small' | 'default' | 'large' = 'default';

  @ViewChild('input')
  input: ElementRef;

  formControl: FormControl;

  @Input()
  layout: EditableLayout = 'default';

  @Input()
  showPlaceHolder = true;

  @Input()
  autocompleteActive = false;

  @Output()
  keyupEvent = new EventEmitter<string>();

  @Output()
  cancelEvent = new EventEmitter<string>();

  @Output()
  confirmEvent = new EventEmitter<string>();

  @Output()
  validatorErrorEvent = new EventEmitter();

  _options: string[];

  get options() {
    return this._options;
  }

  @Input()
  set options(paramOptions: string[]) {
    if (this.autocompleteActive) {
      this._options = paramOptions;
      if (!this.overlayRef && paramOptions.length > 0) {
        this.openOptionMenu();
      } else if (paramOptions.length === 0) {
        this.closeOptionMenu();
      }
    }
  }

  @ViewChildren(ActionAutocompleteFieldOptionComponent)
  optionElements: QueryList<ActionAutocompleteFieldOptionComponent>;

  @ViewChild('template')
  templateRef: TemplateRef<any>;

  private overlayRef: OverlayRef;

  private componentRef: EmbeddedViewRef<any>;

  private _value: string;

  @Input()
  set value(value: string) {
    this.updateAndClose(value);
  }

  get value() {
    return this._value;
  }

  private _displayValue: string;

  @Input()
  set displayValue(value: string) {
    this._displayValue = value;
  }

  get displayValue() {
    return this._displayValue;
  }

  @Output()
  inputValueChanged = new EventEmitter<string>();

  private debouncer$ = new Subject<string>();

  private unsub$ = new Subject<void>();

  constructor(
    private overlay: Overlay,
    private viewContainerRef: ViewContainerRef,
    public readonly cdRef: ChangeDetectorRef,) {
    super(cdRef);
    this.formControl = new FormControl();
  }

  ngOnInit() {
    this.debouncer$.pipe(
      takeUntil(this.unsub$),
      debounceTime(500),
      distinctUntilChanged()
    ).subscribe(value => this.inputValueChanged.emit(value));
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  handleInputChange(event) {
    this.debouncer$.next(event.target.value);
  }

  enableEditMode() {
    if (this.editable) {
      this.formControl.reset();
      this.formControl.setValue(this.value);
      this.formControl.updateValueAndValidity();
      this.formControl.enable();
      this.dirty = false;
      if (!this.edit) {
        this.edit = true;
        // kind of pulling for waiting that input is displayed and active, so we can focus into.
        const unsub = new Subject();
        timer(1, 10).pipe(
          takeUntil(unsub),
          filter(() => Boolean(this.input)),
          tap(() => this.input.nativeElement.focus()),
          // auto unsub, so we don't have wild timers that run into app after an edition
          tap(() => {
            unsub.next();
            unsub.complete();
          })
        ).subscribe();
      }
    }
    return false;
  }

  disableEditMode() {
    this.edit = false;
    this.pending = false;
  }

  handleKeyupEnter() {
    if (this.overlayRef && this.isOneOptionActive()) {
      this.selectOptionWithEnter();
    } else {
      this.confirm();
    }
  }

  openOptionMenu() {
    if (!this.overlayRef && this.options.length > 0) {
      const positionStrategy = this.overlay.position()
        .flexibleConnectedTo(this.input)
        .withPositions([
          { originX: 'start', overlayX: 'start', originY: 'bottom', overlayY: 'top', offsetY: 2, },
          { originX: 'start', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetY: -2 },
        ]);
      const inputWidth = this.input.nativeElement.getBoundingClientRect().width;
      const overlayConfig: OverlayConfig = {
        positionStrategy,
        hasBackdrop: true,
        disposeOnNavigation: true,
        backdropClass: 'transparent-overlay-backdrop',
        maxHeight: 450,
        width: inputWidth
      };
      this.overlayRef = this.overlay.create(overlayConfig);
      const templatePortal = new TemplatePortal(this.templateRef, this.viewContainerRef);
      this.componentRef = this.overlayRef.attach(templatePortal);
      this.overlayRef.backdropClick().subscribe(() => this.closeOptionMenu());
    }
  }

  closeOptionMenu() {
    this.deactivateOptions();
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.componentRef = null;
    }
    this.cdRef.detectChanges();
  }

  activateOption(paramIndex: number) {
    if (paramIndex === undefined) {
      return;
    }
    this.deactivateOptions();
    const activeOption = this.optionElements
      .find((element, index) => index === paramIndex);
    if (activeOption) {
      activeOption.isActive = true;
    }
  }

  private deactivateOptions() {
    const activeOption = this.optionElements.find(option => option.isActive);
    if (activeOption) {
      activeOption.isActive = false;
    }
  }

  selectOptionWithMouse(event, option: string) {
    this.formControl.setValue(option);
    this.closeOptionMenu();
    this.focusInputField();
    event.preventDefault();
  }

  focusInputField() {
    const textInputNativeElement = this.input.nativeElement as HTMLInputElement;
    textInputNativeElement.focus();
  }

  cancel() {
    this.cancelEvent.emit(this.getInputFieldValue());
    this.disableEditMode();
  }

  confirm() {
    this.dirty = true;
    // First check if required and blank
    if (this.required && this.isBlank(this.formControl.value)) {
      // Reset input to remove whitespaces
      this.formControl.setValue('');
    } else if (!this.formControl.errors) {
      this.executeAutoAsync();
      this.confirmEvent.emit(this.getInputFieldValue());
    }
  }

  beginAsync() {
    this.pending = true;
    this.formControl.disable();
  }

  endAsync() {
    this.pending = false;
    this.formControl.enable();
    this.cdRef.detectChanges();
  }

  private updateAndClose(value: string) {
    this.endAsync();
    this.disableEditMode();
    this._value = value;
    this.cdRef.detectChanges();
  }

  private getInputFieldValue() {
    return this.formControl.value;
  }

  handleBlur() {
    if (this.layout === 'no-buttons') {
      this.cancel();
    }
    this.closeOptionMenu();
  }

  getComponentClasses() {
    const cssClass = [];
    if (this.edit) {
      cssClass.push('edit');
    } else {
      cssClass.push('read');
      if (this.editable) {
        cssClass.push('editable');
      }
    }
    return cssClass;
  }

  private isBlank(value: string): boolean {
    return value.trim() === '';
  }

  getEditableIcon() {
    return 'edit';
  }

  /* Keyboard navigation */

  private isOneOptionActive(): boolean {
    return this.optionElements.filter(option => option.isActive).length > 0;
  }

  selectOptionWithEnter() {
    const option = this.getActiveOptionValue();
    this.formControl.setValue(option);
    this.closeOptionMenu();
    this.focusInputField();
  }

  private getActiveOptionValue(): string {
    return this.optionElements.find(option => option.isActive).value;
  }

  activateOptionAbove(event) {
    event.preventDefault();
    this.openOptionMenu();
    this.activateOption(
      this.getAboveOptionIndex(
        this.getActiveOptionIndex()));
  }

  activateOptionBelow(event) {
    event.preventDefault();
    this.openOptionMenu();
    this.activateOption(
      this.getBelowOptionIndex(
        this.getActiveOptionIndex()));
  }

  private getAboveOptionIndex(index: number) {
    const optionLength = this.optionElements.length;
    if (optionLength === 0) {
      return undefined;
    } else if (index === -1 || index === undefined) {
      return optionLength - 1;
    } else if (index === 0) {
      return optionLength - 1;
    } else {
      return index - 1;
    }
  }

  private getBelowOptionIndex(index: number) {
    const optionLength = this.optionElements.length;
    if (optionLength === 0) {
      return undefined;
    } else if (index === -1 || index === undefined) {
      return 0;
    } else if (index === optionLength - 1) {
      return 0;
    } else {
      return index + 1;
    }
  }

  private getActiveOptionIndex(): number {
    return this.optionElements.toArray().findIndex(option => option.isActive);
  }

}
