import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EditableCustomField} from '../../../../custom-field/editable-custom-field';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-core-editable-tag-field',
  templateUrl: './editable-tag-field.component.html',
  styleUrls: ['./editable-tag-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableTagFieldComponent implements OnInit, EditableCustomField {

  autoAsync: false;

  @Input()
  notFoundContent;

  @Input()
  placeholder;

  private _value: string[];

  @Input()
  set value(value: string[]) {
    this._value = value;
    this.model = value;
  }

  get value(): string[] {
    return this._value;
  }

  @Input()
  options: string[] = [];

  @Input()
  editable: boolean;

  @Input()
  optional = true;

  @Output()
  confirmEvent = new EventEmitter<string[]>();

  model: string[];

  showMandatory = false;

  constructor(private cdRef: ChangeDetectorRef, private translateService: TranslateService) {
  }

  ngOnInit() {
    if (!Boolean(this.notFoundContent)) {
      this.notFoundContent = this.translateService.instant('sqtm-core.generic.editable.tags.no-data');
    }
    if (!Boolean(this.placeholder)) {
      this.placeholder = this.translateService.instant('sqtm-core.generic.editable.tags.placeholder');
    }
  }

  confirm() {
    this.hideErrorMsg();
    if ((!Boolean(this.model) || this.model.length === 0) && !this.optional) {
      this.model = [...this.value];
      this.showErrorMsg();
    } else if ((this.model && this.model.length > 0) || this.optional) {
      this.confirmEvent.emit(this.model);
    }
  }

  private showErrorMsg() {
    this.showMandatory = true;
  }

  private hideErrorMsg() {
    this.showMandatory = false;
  }

  enableEditMode() {
    // NOOP
  }

  disableEditMode() {
    // NOOP
  }

  cancel() {
    this.hideErrorMsg();
  }

  beginAsync() {
    // NOOP
  }

  endAsync() {
    // NOOP
  }


}
