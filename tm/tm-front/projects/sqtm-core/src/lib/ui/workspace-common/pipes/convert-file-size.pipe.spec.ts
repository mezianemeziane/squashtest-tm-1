import { Component } from "@angular/core";
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';
import { ConvertFileSizePipe } from './convert-file-size.pipe';

describe('ConvertFileSizePipe', () => {

  const BYTE_I18N_KEY = 'sqtm-core.generic.file.size.unit.byte';
  const KILOBYTE_I18N_KEY = 'sqtm-core.generic.file.size.unit.kilobyte';
  const MEGABYTE_I18N_KEY = 'sqtm-core.generic.file.size.unit.megabyte';
  const GIGABYTE_I18N_KEY = 'sqtm-core.generic.file.size.unit.gigabyte';
  const TERABYTE_I18N_KEY = 'sqtm-core.generic.file.size.unit.terabyte';

  const ONE_BYTE_IN_BYTES = 1;
  const ONE_KILOBYTE_IN_BYTES = 1024;
  const ONE_MEGABYTE_IN_BYTES = 1048576;
  const ONE_GIGABYTE_IN_BYTES = 1073741824;
  const ONE_TERABYTE_IN_BYTES = 1099511627776;

  const translateService = jasmine.createSpyObj('TranslateService', ['instant']);
  translateService.instant.and.callFake((i18nKey: string) => {
    switch (i18nKey) {
      case BYTE_I18N_KEY:
        return 'bytes';
      case KILOBYTE_I18N_KEY:
        return 'KB';
      case MEGABYTE_I18N_KEY:
        return 'MB';
      case GIGABYTE_I18N_KEY:
        return 'GB';
      case TERABYTE_I18N_KEY:
        return 'TB';
      default:
        throw new Error('i18nKey not defined for this test');
    }
  });

  beforeEach(() => TestBed.configureTestingModule({
    providers: [{
      provide: TranslateService,
      useValue: translateService
    }]
  }));

  describe('isolate test', () => {

    const pipe = new ConvertFileSizePipe(translateService);

    it('should convert into the right unit', () => {
      [
        { size: ONE_BYTE_IN_BYTES, result: '1 bytes' },
        { size: ONE_KILOBYTE_IN_BYTES, result: '1,00 KB' },
        { size: ONE_MEGABYTE_IN_BYTES, result: '1,00 MB' },
        { size: ONE_GIGABYTE_IN_BYTES, result: '1,00 GB' },
        { size: ONE_TERABYTE_IN_BYTES, result: '1,00 TB' }
      ].forEach(dataset =>
        expect(pipe.transform(dataset.size)).toBe(dataset.result));
    });

    it('should convert with the right precision', () => {
      const size = 1200;
      expect(pipe.transform(size)).toBe('1,17 KB');
      [
        { precision: 0, result: '1 KB' },
        { precision: 2, result: '1,17 KB' },
        { precision: 5, result: '1,17188 KB' },
        { precision: 8, result: '1,17187500 KB' },
        { precision: 10, result: '1,1718750000 KB' },
      ].forEach(dataset =>
        expect(pipe.transform(size, dataset.precision)).toBe(dataset.result));
    });

    it('should convert with the right number', () => {
      [
        { size: 0, result: '0 bytes' },
        { size: 4, result: '4 bytes' },
        { size: 1023, result: '1023 bytes' },
        { size: 4525, result: '4,42 KB' },
        { size: 8105876, result: '7,73 MB' },
        { size: 9705124641, result: '9,04 GB' },
        { size: 5003785345114, result: '4,55 TB' }
      ].forEach(dataset =>
        expect(pipe.transform(dataset.size)).toBe(dataset.result));
    });

    it('should not convert with decimals if unit is byte', () => {
      [
        { size: 0, precision: 2, result: '0 bytes' },
        { size: 4, precision: 5, result: '4 bytes' },
        { size: 1023, precision: 10, result: '1023 bytes' }
      ].forEach(dataset =>
        expect(pipe.transform(dataset.size, dataset.precision)).toBe(dataset.result));
    });

    it('should handle corner cases in parameters', () => {
      [
        { size: -4200 },
        { size: undefined },
        { size: null }
      ].forEach(dataset =>
        expect(pipe.transform(dataset.size)).toBe('?'));
      [
        { precision: -5, result:  '1 KB' },
        { precision: undefined, result:  '1,17 KB' },
        { precision: null, result: '1 KB' }
      ].forEach(dataset =>
        expect(pipe.transform(1200, dataset.precision)).toBe(dataset.result));
    });

  });

  describe('shallow test', () => {

    @Component({
      template: `Size: {{ size | convertFileSize:precision }}`
    })
    class TestComponent {
      size;
      precision;
    }

    let component: TestComponent;
    let fixture: ComponentFixture<TestComponent>;
    let el: HTMLElement;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [
          ConvertFileSizePipe,
          TestComponent
        ]
      });
      fixture = TestBed.createComponent(TestComponent);
      component = fixture.componentInstance;
      el = fixture.nativeElement;
    });

    it('should display the right unit', () => {
      [
        { size: ONE_BYTE_IN_BYTES, result: 'Size: 1 bytes' },
        { size: ONE_KILOBYTE_IN_BYTES, result: 'Size: 1,00 KB' },
        { size: ONE_MEGABYTE_IN_BYTES, result: 'Size: 1,00 MB' },
        { size: ONE_GIGABYTE_IN_BYTES, result: 'Size: 1,00 GB' },
        { size: ONE_TERABYTE_IN_BYTES, result: 'Size: 1,00 TB' }
      ].forEach(dataset => {
        component.size = dataset.size;
        fixture.detectChanges();
        expect(el.textContent).toContain(dataset.result);
      });
    });

    it('should display the right precision', () => {
      component.size = 1200;
      [
        { precision: 0, result: 'Size: 1 KB' },
        { precision: 2, result: 'Size: 1,17 KB' },
        { precision: undefined, result: 'Size: 1,17 KB' },
        { precision: 5, result: 'Size: 1,17188 KB' },
        { precision: 8, result: 'Size: 1,17187500 KB' },
        { precision: 10, result: 'Size: 1,1718750000 KB' },
      ].forEach(dataset => {
        component.precision = dataset.precision;
        fixture.detectChanges();
        expect(el.textContent).toContain(dataset.result);
      });
    });

    it('should display the right number', () => {
      [
        { size: 0, result: 'Size: 0 bytes' },
        { size: 4, result: 'Size: 4 bytes' },
        { size: 1023, result: 'Size: 1023 bytes' },
        { size: 4525, result: 'Size: 4,42 KB' },
        { size: 8105876, result: 'Size: 7,73 MB' },
        { size: 9705124641, result: 'Size: 9,04 GB' },
        { size: 5003785345114, result: 'Size: 4,55 TB' }
      ].forEach(dataset => {
        component.size = dataset.size;
        fixture.detectChanges();
        expect(el.textContent).toContain(dataset.result);
      });
    });

    it('should not display decimals if unit is byte', () => {
      [
        { size: 0, precision: 2, result: '0 bytes' },
        { size: 4, precision: 5, result: '4 bytes' },
        { size: 1023, precision: 10, result: '1023 bytes' }
      ].forEach(dataset => {
        component.size = dataset.size;
        component.precision = dataset.precision;
        fixture.detectChanges();
        expect(el.textContent).toContain(dataset.result);
      });
    });

    it('should handle corner cases in parameters', () => {
      [
        { size: -4200 },
        { size: undefined },
        { size: null }
      ].forEach(dataset => {
        component.size = dataset.size;
        fixture.detectChanges();
        expect(el.textContent).toContain('?');
      });

      component.size = 1200;
      [
        { precision: -5, result:  '1 KB' },
        { precision: undefined, result:  '1,17 KB' },
        { precision: null, result: '1 KB' }
      ].forEach(dataset => {
        component.precision = dataset.precision;
        fixture.detectChanges();
        expect(el.textContent).toContain(dataset.result);
      });

    });

  });

});
