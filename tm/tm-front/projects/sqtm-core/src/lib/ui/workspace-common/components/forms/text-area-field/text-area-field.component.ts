import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef, EventEmitter,
  Input, Output,
  ViewChild
} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FieldValidationError} from '../../../../../model/error/error.model';
import {AbstractFormField} from '../abstract-form-field';

@Component({
  selector: 'sqtm-core-text-area-field',
  templateUrl: './text-area-field.component.html',
  styleUrls: ['./text-area-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextAreaFieldComponent extends AbstractFormField implements AfterViewInit {

  @Input()
  formGroup: FormGroup;

  @Input()
  fieldName: string;

  @Input()
  rows = 4;

  @Input()
  set serverSideFieldValidationError(fieldsValidationErrors: FieldValidationError[]) {
    this.showServerSideError(fieldsValidationErrors);
  }

  /** If set to true, it will give the focus to this component at page initialisation */
  @Input()
  initialFocus = false;

  @ViewChild('textAreaField', {static: true, read: ElementRef})
  textAreaField: ElementRef;

  @Output()
  inputBlurred = new EventEmitter<FocusEvent>();

  constructor(cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  ngAfterViewInit(): void {
    if (this.initialFocus) {
      this.focusInputField();
    }
  }

  grabFocus() {
    this.focusInputField();
  }

  private focusInputField() {
    const inputFieldNativeElement = this.textAreaField.nativeElement as HTMLTextAreaElement;
    inputFieldNativeElement.focus();
  }

  handleBlur($event: FocusEvent) {
    this.inputBlurred.next($event);
  }
}
