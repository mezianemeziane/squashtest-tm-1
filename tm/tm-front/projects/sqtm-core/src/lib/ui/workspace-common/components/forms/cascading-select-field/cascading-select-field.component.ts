import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

import {NzSelectOptionInterface} from 'ng-zorro-antd/select';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Identifier} from '../../../../../model/entity.model';

/**
 * Two-level cascading select.
 */
@Component({
  selector: 'sqtm-core-cascading-select-field',
  template: `
    <div class="flex-row container">
      <nz-select
          [nzOptions]="firstOptions"
          [(ngModel)]="firstSelected"
          [nzDisabled]="isFirstDisabled"
          [attr.data-test-component-id]="'first'">
      </nz-select>
      <nz-select
          [nzOptions]="secondOptions"
          [(ngModel)]="secondSelected"
          [nzDisabled]="isSecondDisabled"
          [nzAllowClear]="true"
          [attr.data-test-component-id]="'second'">
      </nz-select>
    </div>
  `,
  styleUrls: ['./cascading-select-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: CascadingSelectFieldComponent,
    multi: true
  }],
})
export class CascadingSelectFieldComponent implements ControlValueAccessor {

  @Input()
  options: CascadingSelectOption[] = [];

  get firstSelected(): Identifier {
    return this._firstSelected;
  }

  set firstSelected(id: Identifier) {
    if (id !== this._firstSelected) {
      this._firstSelected = id;
      this.secondSelected = null;
    }
  }

  get secondSelected(): Identifier {
    return this._secondSelected;
  }

  set secondSelected(id: Identifier) {
    if (id !== this._secondSelected) {
      this._secondSelected = id;
      if (this.onChange != null) {
        this.onChange([this._firstSelected, this._secondSelected]);
      }
    }
  }

  private _firstSelected: Identifier;
  private _secondSelected: Identifier;

  onChange: Function;

  constructor() {
  }

  get isFirstDisabled(): boolean {
    return this.firstOptions.length === 0;
  }

  get isSecondDisabled(): boolean {
    return this.isFirstDisabled
      || !Boolean(this.firstSelected)
      || this.secondOptions.length === 0;
  }

  get firstOptions(): NzSelectOptionInterface[] {
    return this.options
      .map((opt) => ({value: opt.id, label: opt.label}));
  }

  get secondOptions(): NzSelectOptionInterface[] {
    if (this.firstSelected != null) {
      return this.options.find(opt => opt.id === this.firstSelected)?.children
        .map((opt) => ({value: opt.id, label: opt.label})) ?? [];
    }

    return [];
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    // Not interested
  }

  writeValue(obj: any): void {
    // Not interested
    if (Array.isArray(obj)) {
      this.firstSelected = obj[0];
      this.secondSelected = obj[1];
    }
  }
}

export interface CascadingSelectOption {
  id: Identifier;
  label: string;
  children: CascadingSelectOption[];
}
