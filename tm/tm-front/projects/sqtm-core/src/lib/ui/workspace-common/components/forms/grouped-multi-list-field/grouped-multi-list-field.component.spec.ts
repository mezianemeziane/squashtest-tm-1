import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {GroupedMultiListFieldComponent} from './grouped-multi-list-field.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('GroupedMultiListFieldComponent', () => {
  let component: GroupedMultiListFieldComponent;
  let fixture: ComponentFixture<GroupedMultiListFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule],
      declarations: [GroupedMultiListFieldComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupedMultiListFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
