import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FieldValidationError } from '../../../../../../model/error/error.model';
import { AbstractActionWordFieldComponent } from '../abstract-action-word-field.component';

@Component({
  selector: 'sqtm-core-action-text-field',
  templateUrl: './../../text-field/text-field.component.html',
  styleUrls: ['./../../text-field/text-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionTextFieldComponent extends AbstractActionWordFieldComponent implements OnInit {

  @Input() formGroup: FormGroup;
  @Input() fieldName: string;
  @Input() serverSideFieldValidationError: FieldValidationError[];

  @Input()
  inputType = 'text';

  @Input()
  placeholder = '';

  @ViewChild('inputField', {static: true, read: ElementRef})
  inputField: ElementRef;

  constructor(cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  focusInput() {
    const inputFieldNativeElement = this.inputField.nativeElement as HTMLInputElement;
    inputFieldNativeElement.focus();
  }

  blurInput() {
    const inputFieldNativeElement = this.inputField.nativeElement as HTMLInputElement;
    inputFieldNativeElement.blur();
  }

  handleBlur($event: FocusEvent) {
    // NOOP
  }

}
