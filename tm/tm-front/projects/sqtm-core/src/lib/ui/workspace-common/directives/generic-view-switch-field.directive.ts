import {AfterViewInit, Directive, Host, HostListener, Input, OnDestroy, OnInit} from '@angular/core';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {SqtmGenericEntityState} from '../../../core/services/genric-entity-view/generic-entity-view-state';
import {SwitchFieldComponent} from '../components/forms/switch-field/switch-field.component';
import {GenericEntityViewService} from '../../../core/services/genric-entity-view/generic-entity-view.service';

@Directive({
  selector: '[sqtmCoreGenericViewSwitchField]'
})
export class GenericViewSwitchFieldDirective<E extends SqtmGenericEntityState, T extends string>
  implements OnInit, OnDestroy, AfterViewInit {

  @Input('sqtmCoreGenericViewSwitchField')
  fieldName: keyof E;

  unsub$ = new Subject<void>();

  constructor(private genericEntityViewService: GenericEntityViewService<E, T>, @Host() private switchField: SwitchFieldComponent) {
  }

  @HostListener('confirmEvent', ['$event'])
  onConfirm(value: boolean) {
    this.genericEntityViewService.update(this.fieldName, value as any); // cannot use type system on dynamic inputs...
  }

  ngAfterViewInit(): void {
    this.genericEntityViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => componentData[componentData.type]),
      distinctUntilChanged()
    ).subscribe((entity) => {
      const value = entity[this.fieldName as string];
      this.switchField.value = Boolean(value);
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}
