import {Directive, Host, Input} from '@angular/core';
import {GenericEntityViewService} from '../../../core/services/genric-entity-view/generic-entity-view.service';
import {SqtmGenericEntityState} from '../../../core/services/genric-entity-view/generic-entity-view-state';
import {EditableDateFieldComponent} from '../components/editables/editable-date-field/editable-date-field.component';
import {AbstractGenericViewEditableField} from './abstract-generic-view-editable-field';

@Directive({
  selector: '[sqtmCoreGenericViewEditableDateField]'
})
export class GenericViewEditableDateFieldDirective<E extends SqtmGenericEntityState, T extends string>
  extends AbstractGenericViewEditableField<EditableDateFieldComponent, E, T> {

  @Input('sqtmCoreGenericViewEditableDateField')
  fieldName: keyof E;

  constructor(protected genericEntityViewService: GenericEntityViewService<E, T>,
              @Host() protected editable: EditableDateFieldComponent) {
    super(genericEntityViewService, editable);
  }

  protected showExternalErrorMessages(errorMessages: string[]): void {
  }

  protected setValue(newValue: any): void {
    if (!newValue) {
      this.editable.value = null;
    } else if (typeof newValue === 'string' && newValue.trim() !== '') {
      this.editable.value = new Date(newValue);
    } else if (newValue instanceof Date) {
      this.editable.value = newValue;
    } else {
      console.warn('Received unhandled date value. Expect string or Date but received ' + typeof newValue, newValue);
      this.editable.value = null;
    }
  }
}
