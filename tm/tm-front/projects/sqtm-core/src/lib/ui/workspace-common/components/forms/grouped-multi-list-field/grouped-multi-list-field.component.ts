import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EmbeddedViewRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {TemplatePortal} from '@angular/cdk/portal';
import {ListGroup, ListItem} from '../grouped-multi-list/grouped-multi-list.component';

@Component({
  selector: 'sqtm-core-grouped-multi-list-field',
  templateUrl: './grouped-multi-list-field.component.html',
  styleUrls: ['./grouped-multi-list-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupedMultiListFieldComponent implements OnInit, OnDestroy {

  @Input()
  listGroups: ListGroup[] = [];

  @Input()
  listItems: ListItem[] = [];

  @Input()
  height = 500;

  @Input()
  virtualScroll = false;

  @Input()
  separateSelectedItems: boolean;

  @Input()
  searchable = true;

  @Input()
  numberOfSelectedItemsToDisplay = 5;

  @Input()
  numberOfUngroupedItemsToDisplay = 5;

  @Input()
  placeHolder: string;

  @Input()
  disabled = false;

  @Input()
  showClearButton: boolean;

  @Output()
  valueChanged = new EventEmitter<ListItem[]>();

  @ViewChild('field')
  field: ElementRef;

  @ViewChild('template')
  templateRef: TemplateRef<any>;

  private overlayRef: OverlayRef;

  private componentRef: EmbeddedViewRef<any>;

  editMode: boolean;

  get multiListWidth(): number {
    return this.field.nativeElement.getBoundingClientRect().width;
  }

  constructor(private overlay: Overlay, private viewContainerRef: ViewContainerRef, public readonly cdRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.removeOverlay();
  }

  get selectedItems(): ListItem[] {
    return this.listItems.filter(item => item.selected);
  }

  set selectedItems(newSelection: ListItem[]) {
    this.listItems = this.listItems.map(item => ({
      ...item,
      selected: newSelection.map(i => i.id).includes(item.id),
    }));

    this.cdRef.detectChanges();
  }

  get isPlaceHolderVisible(): boolean {
    return this.selectedItems.length === 0;
  }

  get isClearButtonVisible(): boolean {
    return this.showClearButton && this.selectedItems.length > 0;
  }

  edit(): void {
    if (this.disabled) {
      return;
    }

    this.editMode = true;

    const positionStrategy = this.overlay.position()
      .flexibleConnectedTo(this.field)
      .withPositions([
        {originX: 'start', overlayX: 'start', originY: 'bottom', overlayY: 'top', offsetY: 4},
        {originX: 'start', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetY: -4},
      ]);

    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
    };

    this.overlayRef = this.overlay.create(overlayConfig);
    const templatePortal = new TemplatePortal(this.templateRef, this.viewContainerRef);
    this.componentRef = this.overlayRef.attach(templatePortal);

    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  get displayValue(): string {
    return this.isPlaceHolderVisible ? this.placeHolder :
      this.selectedItems.map(item => this.getLabel(item)).join(', ');
  }

  handleItemSelectionChange(listItem: ListItem) {
    this.listItems.forEach(item => {
      if (item.id === listItem.id) {
        item.selected = listItem.selected;
      }
    });

    this.valueChanged.emit(this.selectedItems);
    this.cdRef.detectChanges();
  }

  private close() {
    this.editMode = false;
    this.removeOverlay();

    this.cdRef.detectChanges();
  }

  private removeOverlay() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.componentRef = null;
    }
  }

  private getLabel(listItem: ListItem) {
    return listItem.label;
  }

  clearButtonClicked($event: MouseEvent): void {
    if (this.isClearButtonVisible) {
      $event.stopPropagation();
      this.selectedItems = [];
    }
  }
}
