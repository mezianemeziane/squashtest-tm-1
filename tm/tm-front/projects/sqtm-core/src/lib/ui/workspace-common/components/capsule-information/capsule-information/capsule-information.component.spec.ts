import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CapsuleInformationComponent} from './capsule-information.component';
import {TranslateModule} from '@ngx-translate/core';

describe('CapsuleInformationComponent', () => {
  let component: CapsuleInformationComponent;
  let fixture: ComponentFixture<CapsuleInformationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [CapsuleInformationComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapsuleInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
