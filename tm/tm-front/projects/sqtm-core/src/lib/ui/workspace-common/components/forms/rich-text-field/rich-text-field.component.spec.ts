import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {RichTextFieldComponent} from './rich-text-field.component';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {FormControl, FormGroup} from '@angular/forms';
import {SessionPingService} from '../../../../../core/services/session-ping/session-ping.service';
import {mockSessionPingService} from '../../../../testing-utils/mocks.service';

describe('RichTextFieldComponent', () => {
  let component: RichTextFieldComponent;
  let fixture: ComponentFixture<RichTextFieldComponent>;
  const translateService = jasmine.createSpyObj('TranslateService', ['getBrowserLang']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule],
      declarations: [RichTextFieldComponent],
      providers: [
        {
          provide: TranslateService,
          useValue: translateService
        },
        {
          provide: SessionPingService,
          useValue: mockSessionPingService(),
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RichTextFieldComponent);
    component = fixture.componentInstance;
    component.formGroup = new FormGroup({
      name: new FormControl('toto')
    });
    component.fieldName = 'name';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
