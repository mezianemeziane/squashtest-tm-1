import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {FieldValidationError} from '../../../../../model/error/error.model';
import {DisplayOption} from '../display-option';
import {Identifier} from '../../../../../model/entity.model';

/**
 * A list of checkboxes that can be used inside a formGroup.
 */
@Component({
  selector: 'sqtm-core-checkbox-group-field',
  templateUrl: './checkbox-group-field.component.html',
  styleUrls: ['./checkbox-group-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: CheckboxGroupFieldComponent,
    multi: true
  }],
})
export class CheckboxGroupFieldComponent implements ControlValueAccessor {
  @Input() options: DisplayOption[];
  @Input() serverSideFieldValidationError: FieldValidationError[];

  onChange: Function;

  public value: Identifier[] = [];

  constructor() {
  }

  isChecked(optionId: Identifier): boolean {
    return this.value.includes(optionId);
  }

  setChecked(optionId: Identifier, checked: boolean): void {
    if (checked && !this.isChecked(optionId)) {
      this.value.push(optionId);
    }

    if (!checked && this.isChecked(optionId)) {
      this.value = this.value.filter(opt => opt !== optionId);
    }

    if (this.onChange) {
      this.onChange(this.value);
    }
  }

  handleWrapperClicked(option: DisplayOption): void {
    this.setChecked(option.id, !this.isChecked(option.id));
  }

  writeValue(value: Identifier[]) {
    if (Array.isArray(value)) {
      this.value = value;
      this.value.forEach(optionId => this.setChecked(optionId, true));
    } else {
      this.value = [];
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }
}
