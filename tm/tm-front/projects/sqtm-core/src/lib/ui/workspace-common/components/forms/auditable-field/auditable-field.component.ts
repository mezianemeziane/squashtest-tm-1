import {Component, OnInit, ChangeDetectionStrategy, Input} from '@angular/core';
import {DatePipe, formatDate} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';

// A simple component that displays a time and a user, used for 'Created' and 'Modified' fields
@Component({
  selector: 'sqtm-core-auditable-field',
  template: `<span>{{text}}</span>`,
  styleUrls: ['./auditable-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuditableFieldComponent implements OnInit {

  @Input() date: string;
  @Input() userName: string;

  constructor(private readonly translateService: TranslateService) { }

  ngOnInit(): void {
  }

  get text(): string {
    if (this.date == null) {
      return this.translateService.instant('sqtm-core.generic.label.never');
    }

    const dateFormatted = formatDate(this.date, 'short', this.translateService.getBrowserLang());
    return `${dateFormatted} (${this.userName})`;
  }

}
