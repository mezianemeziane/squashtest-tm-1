import {buildErrorI18nKeyProvider, ErrorI18nKeyProvider} from '../../errors/ErrorKeyProvider';
import {AbstractControl, FormGroup} from '@angular/forms';
import {ChangeDetectorRef, Directive} from '@angular/core';
import {FieldValidationError} from '../../../../model/error/error.model';

/**
 * Common interface for errors handling
 */
@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractFormField {

  abstract formGroup: FormGroup;

  abstract fieldName: string;

  abstract serverSideFieldValidationError: FieldValidationError[];

  errors: ErrorI18nKeyProvider[] = [];

  get formControl(): AbstractControl {
    return this.formGroup?.controls[this.fieldName];
  }

  protected constructor(protected cdr: ChangeDetectorRef) {
  }

  showClientSideError() {
    this.clearErrors();
    if (this.formControl.errors) {
      this.pushClientSideErrors();
    }
    this.cdr.detectChanges();
  }

  private pushClientSideErrors() {
    this.errors.push(...this.generateClientSideErrors());
  }

  private generateClientSideErrors(): ErrorI18nKeyProvider[] {
    return Object
      .entries(this.formControl.errors)
      .map(([fieldName, errorValue]: [string, any]) => buildErrorI18nKeyProvider(fieldName, errorValue));
  }

  protected showServerSideError(fieldsValidationErrors: FieldValidationError[]) {
    this.clearErrors();
    if (fieldsValidationErrors.length > 0) {
      this.errors.push(...this.extractFieldValidationErrorsForThisField(fieldsValidationErrors));
    }
  }

  private clearErrors() {
    this.errors = [];
  }

  private extractFieldValidationErrorsForThisField(fieldsValidationErrors: FieldValidationError[]) {
    return fieldsValidationErrors
      .filter(value => value.fieldName === this.fieldName)
      .map(value => buildErrorI18nKeyProvider(value));
  }
}
