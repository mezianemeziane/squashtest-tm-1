import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {Option} from '../../../../../model/option.model';
import {EditableCustomField} from '../../../../custom-field/editable-custom-field';
import {ValidatorFn} from '@angular/forms';
import {AbstractEditableField, EditableField, EditableLayout} from '../abstract-editable-field';
import {wsCommonLogger} from '../../../workspace.common.logger';
import {TranslateService} from '@ngx-translate/core';

const LOGGER = wsCommonLogger.compose('EditableAutocompleteFieldComponent');

@Component({
  selector: 'sqtm-core-editable-autocomplete-field',
  template: `
    <div [ngClass]="getComponentClasses()" class="flex-row full-height full-width" *ngIf="!edit"
         (click)="enableEditMode()">
          <span *ngIf="value;else noValue" class="field-value-color"
                nz-tooltip [sqtmCoreLabelTooltip]="value" [ellipsis]="true"
                [class.editable]="editable">
        {{value}}
        </span>
      <ng-template #noValue>
        <span class="full-width">-</span>
      </ng-template>
      <i *ngIf="editable" class="pencil-icon flex-fixed-size" nz-icon [nzType]="getEditableIcon()"
         nzTheme="outline"></i>
    </div>
    <div *ngIf="edit" class="flex-column">
      <div class="flex-row full-width">
        <sqtm-core-autocomplete [options]="options" [value]="transientValue" (valueChanged)="change($event)"
                                (confirmValueEvent)="updateTransientValue($event)"
                                (cancelEvent)="cancel()"></sqtm-core-autocomplete>
      </div>
    </div>
  `,
  styleUrls: ['./editable-autocomplete-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableAutocompleteFieldComponent extends AbstractEditableField implements OnInit, EditableField, EditableCustomField {

  @Input()
  layout: EditableLayout = 'no-buttons';

  @Input()
  displayInGrid: boolean;

  private _edit = false;

  @Input()
  set edit(isEdit: boolean) {
    this._edit = isEdit;
    this.toggleEditEvent.emit(isEdit);
    this.cdRef.detectChanges();
  }

  get edit(): boolean {
    return this._edit;
  }

  @Input()
  allowEmptyValue = false;

  @Input()
  showsPlaceholderAsFirstOption = true;

  @Input()
  pending = false;

  @Input()
  size: 'small' | 'default' | 'large' = 'default';

  @Input()
  set validators(validators: ValidatorFn[]) {
    throw Error('Validators are not handled in Select Field');
  }

  /**
   * Temp value set to user choice in NzSelect, before the confirm or cancel action is fired.
   */
  transientValue: string;

  @Input()
  options: string[];

  @Input()
  placeHolder: string;

  private _value: string;

  /**
   * Set the value, end all async operations and close editable mode
   * @param value the new value
   */
  @Input()
  set value(value: string) {
    this.updateAndClose(value);
  }

  get value(): string {
    return this._value;
  }

  @Output()
  readonly confirmEvent = new EventEmitter<string>();

  @Output()
  readonly changeEvent = new EventEmitter<string>();

  @Output()
  readonly toggleEditEvent = new EventEmitter<boolean>();

  constructor(cdRef: ChangeDetectorRef, public readonly translateService: TranslateService) {
    super(cdRef);
  }

  get allOptions(): string[] {
    return this.options;
  }

  ngOnInit() {
  }

  enableEditMode() {
    if (!this.edit && this.editable) {
      this.transientValue = this.value;
      this.edit = true;
    }
    LOGGER.debug(`Transient Value = ${this.transientValue}`);
    return false;
  }

  cancel() {
    this.transientValue = this.value;
    this.disableEditMode();
  }

  updateTransientValue(value: string) {
    this.transientValue = value;
    this.confirm();
  }

  /**
   * Emit transient value.
   * Do NOT set the value neither change the edit mode.
   * As all our editable are used to modify state server side, the listener of this event must :
   * - Use beginAsync() to pass the editable to async mode.
   * - Proceed the request.
   * - If success set the value so the editable will close and the value will be set.
   * - If failure take according measures.
   */
  confirm() {
    this.executeAutoAsync();
    this.confirmEvent.emit(this.transientValue);
  }

  change($event: string) {
    this.changeEvent.emit($event);
    this.transientValue = $event;
  }

  beginAsync() {
    this.pending = true;
  }

  endAsync() {
    this.pending = false;
  }

  private updateAndClose(value: string) {
    this.endAsync();
    this.disableEditMode();
    this._value = value;
    this.transientValue = value;
    this.cdRef.detectChanges();
  }

  disableEditMode() {
    this.edit = false;
    this.pending = false;
  }

  getEditableIcon() {
    if (this.displayInGrid) {
      return '';
    } else {
      return 'edit';
    }
  }

  getComponentClasses() {
    const cssClass = [];
    if (this.edit) {
      cssClass.push('edit');
    } else {
      cssClass.push('read');
      if (this.editable) {
        cssClass.push('editable');
      }
    }
    return cssClass;
  }

  trackByOptionValue(index: number, option: Option): any {
    return option.value;
  }

  // avoid flash of placeholder in native entity field if no value and no-optional field
  // cuf have no problems because the cuf widget will only instantiate after entity has been fetched and loaded in store. So no flash.
  mustShowPlaceHolder(value: string, allowEmptyValue: boolean) {
    return allowEmptyValue && !Boolean(value);
  }

  getActualPlaceHolder() {
    return this.placeHolder || this.translateService.instant('sqtm-core.generic.editable.placeholder');
  }
}
