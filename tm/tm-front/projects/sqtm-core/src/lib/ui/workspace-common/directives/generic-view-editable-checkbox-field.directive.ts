import {Directive, Host, Input} from '@angular/core';
import {GenericEntityViewService} from '../../../core/services/genric-entity-view/generic-entity-view.service';
import {SqtmGenericEntityState} from '../../../core/services/genric-entity-view/generic-entity-view-state';
import {AbstractGenericViewEditableField} from './abstract-generic-view-editable-field';
import {EditableCheckBoxComponent} from '../components/editables/editable-check-box/editable-check-box.component';

@Directive({
  selector: '[sqtmCoreGenericViewEditableCheckboxField]'
})
export class GenericViewEditableCheckboxFieldDirective<E extends SqtmGenericEntityState, T extends string>
  extends AbstractGenericViewEditableField<EditableCheckBoxComponent, E, T> {

  @Input('sqtmCoreGenericViewEditableCheckboxField')
  fieldName: keyof E;

  constructor(protected genericEntityViewService: GenericEntityViewService<E, T>,
              @Host() protected editable: EditableCheckBoxComponent) {
    super(genericEntityViewService, editable);
  }

  protected showExternalErrorMessages(errorMessages: string[]): void {
  }

  protected setValue(newValue: any): void {
    this.editable.value = newValue;
  }
}
