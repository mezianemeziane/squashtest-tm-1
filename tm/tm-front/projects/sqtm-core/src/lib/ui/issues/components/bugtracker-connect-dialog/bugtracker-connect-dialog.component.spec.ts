import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BugtrackerConnectDialogComponent} from './bugtracker-connect-dialog.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {IssuesService} from '../../service/issues.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {AuthenticationProtocol} from '../../../../model/third-party-server/authentication.model';

describe('BugtrackerConnectDialogComponent', () => {
  let component: BugtrackerConnectDialogComponent;
  let fixture: ComponentFixture<BugtrackerConnectDialogComponent>;
  const dialogRef = jasmine.createSpyObj('DialogReference', ['instant']);
  const issuesService = jasmine.createSpyObj('IssuesService', ['instant']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BugtrackerConnectDialogComponent],
      imports: [TestingUtilsModule, HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: DialogReference, useValue: dialogRef},
        {provide: IssuesService, useValue: issuesService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BugtrackerConnectDialogComponent);
    component = fixture.componentInstance;
    component.configuration = {
      bugTracker: {
        authProtocol: AuthenticationProtocol.BASIC_AUTH,
        id: 123,
      },
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
