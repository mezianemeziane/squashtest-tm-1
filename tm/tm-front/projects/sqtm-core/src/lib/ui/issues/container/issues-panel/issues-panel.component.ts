import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IssuesService} from '../../service/issues.service';
import {BugTracker} from '../../../../model/bugtracker/bug-tracker.model';
import {IssueBindableEntity} from '../../../../model/issue/issue-bindable-entity.model';
import {DialogService} from '../../../dialog/services/dialog.service';
import {HttpErrorResponse} from '@angular/common/http';
import {IssueUIModel} from '../../../../model/issue/issue-ui.model';

/**
 * Wrapper around a remote issues grid. This component is responsible for checking the bugtracker
 * status and show the appropriate UI : error message, authentication required, activation required,...
 * When the bugtracker is ready to interact with, a signal gets emitted (loadIssues).
 */
@Component({
  selector: 'sqtm-core-issues-panel',
  templateUrl: './issues-panel.component.html',
  styleUrls: ['./issues-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [IssuesService]
})
export class IssuesPanelComponent implements OnInit {
  @Input()
  entityType: IssueBindableEntity;

  @Input()
  entityId: number;

  @Input()
  bugTracker: BugTracker;

  @Input()
  hasSmallGrid = false;

  /**
   * Emits when the remote issues data is ready to be fetched : user must be authenticated to the
   * bugtracker and bugtracker should be activated if needed (see useManualActivation).
   */
  @Output()
  loadIssues = new EventEmitter<void>();

  /**
   * If true, the error message (unavailable bugtracker) will warn about missing remote data for
   * execution steps.
   */
  @Input()
  warnAboutMissingExecutionStepDetails = false;

  /**
   * If true, the user has to manually trigger the data fetching. This allows to reduce the load
   * time for execution runners by saving a request to the bugtracker.
   */
  @Input()
  useManualActivation = false;

  get bugtrackerErrorStyle(): string {
    return this.hasSmallGrid ? 'bugtracker-error-in-small-grid' : 'bugtracker-error';
  }

  // Local copy for use in template
  readonly DisplayMode = DisplayMode;

  constructor(public readonly issuesService: IssuesService,
              private dialogService: DialogService) {
  }

  ngOnInit() {
    this.issuesService.loadModel(this.entityId, this.entityType)
      .subscribe(issueModel => this.emitLoadIssuesIfReady(issueModel));
  }

  getCurrentDisplayMode(issueUIModel: IssueUIModel): DisplayMode {
    if (!issueUIModel.modelLoaded) {
      return DisplayMode.LOADING;
    } else if (this.bugTrackerNeedsActivation(issueUIModel)) {
      return DisplayMode.ACTIVATION_REQUIRED;
    } else if (issueUIModel.hasError) {
      return DisplayMode.HAS_ERROR;
    } else if (issueUIModel.bugTrackerStatus !== 'AUTHENTICATED') {
      return DisplayMode.AUTHENTICATION_REQUIRED;
    } else {
      return DisplayMode.LOADED;
    }
  }

  showError(error: HttpErrorResponse) {
    this.dialogService.openHttpErrorDialog({
      id: 'bugtracker-error',
      error
    });
  }

  handleConnectSuccess(): void {
    this.issuesService.setAuthenticated()
      .subscribe((state) => this.emitLoadIssuesIfReady(state));
  }

  activateBugTracker(): void {
    this.issuesService.activateBugTracker()
      .subscribe(issueModel => this.emitLoadIssuesIfReady(issueModel));
  }

  private bugTrackerNeedsActivation(bugTrackerUi: IssueUIModel): boolean {
    return this.useManualActivation
      && bugTrackerUi.bugTrackerMode === 'Manual'
      && !bugTrackerUi.activated;
  }

  private emitLoadIssuesIfReady(issueModel: IssueUIModel): void {
    const isAuthenticated = issueModel.modelLoaded && issueModel.bugTrackerStatus === 'AUTHENTICATED';
    const isAutomaticOrActivated = !this.bugTrackerNeedsActivation(issueModel);
    if (isAuthenticated && isAutomaticOrActivated) {
      this.loadIssues.emit();
    }
  }
}

export enum DisplayMode {
  LOADING,
  ACTIVATION_REQUIRED,
  AUTHENTICATION_REQUIRED,
  HAS_ERROR,
  LOADED,
}
