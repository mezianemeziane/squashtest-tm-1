import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ExecutionStatus, ExecutionStatusKeys} from '../../../../../model/level-enums/level-enum';

@Component({
  selector: 'sqtm-core-execution-status',
  template: `
    <div class="execution-status txt__semi-bold" [style.background-color]="getStatusColor()">
      <span class="m-l-3 m-r-3" [style.color]="getTextColor()">{{executionStatus.i18nKey | translate | capitalize}}</span>
    </div>`,
  styleUrls: ['./execution-status.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionStatusComponent implements OnInit {

  get executionStatus() {
    return ExecutionStatus[this.status];
  }

  @Input()
  status: ExecutionStatusKeys;

  @Output()
  changeStatus = new EventEmitter<ExecutionStatusKeys>();

  constructor() {
  }

  ngOnInit() {

  }

  getStatusColor(): string {
    return this.executionStatus.color;
  }

  getTextColor(): string {
    return this.executionStatus.textColor;
  }
}
