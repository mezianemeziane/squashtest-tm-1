import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import {TranslateModule} from '@ngx-translate/core';
import {CategoryLabelComponent} from './components/category-label/category-label.component';
import {WorkspaceCommonModule} from '../../workspace-common/workspace-common.module';


@NgModule({
  declarations: [
    CategoryLabelComponent,
  ],
  exports: [
    CategoryLabelComponent,
  ],
  imports: [
    CommonModule,
    NzToolTipModule,
    NzIconModule,
    TranslateModule.forChild(),
    WorkspaceCommonModule
  ]
})
export class RequirementUiModule {
}
