import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import {TranslateModule} from '@ngx-translate/core';
import {ExecutionStatusComponent} from './components/execution-status/execution-status.component';
import { ExecutionModeComponent } from './components/execution-mode/execution-mode.component';
import {WorkspaceCommonModule} from '../../workspace-common/workspace-common.module';


@NgModule({
  declarations: [
    ExecutionStatusComponent,
    ExecutionModeComponent
  ],
  exports: [
    ExecutionStatusComponent,
    ExecutionModeComponent
  ],
  imports: [
    CommonModule,
    NzToolTipModule,
    NzIconModule,
    TranslateModule.forChild(),
    WorkspaceCommonModule
  ]
})
export class ExecutionUiModule {
}
