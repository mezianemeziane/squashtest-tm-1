import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {NumericFilterComponent} from './numeric-filter.component';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';
import {FormsModule} from '@angular/forms';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {NzInputNumberModule} from 'ng-zorro-antd/input-number';

describe('NumericFilterComponent', () => {
  let component: NumericFilterComponent;
  let fixture: ComponentFixture<NumericFilterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, FormsModule, NzInputNumberModule],
      declarations: [NumericFilterComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumericFilterComponent);
    component = fixture.componentInstance;
    component.setFilter({
      id: 'filter',
      availableOperations: [],
      value: {
        kind: 'single-string-value',
        value: '12'
      }
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
