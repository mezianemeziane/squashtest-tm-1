import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {DateFilterValueRendererComponent} from './date-filter-value-renderer.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';

describe('DateFilterValueRendererComponent', () => {
  let component: DateFilterValueRendererComponent;
  let fixture: ComponentFixture<DateFilterValueRendererComponent>;
  const datePipe = jasmine.createSpyObj('datePipe', ['transform']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DateFilterValueRendererComponent],
      imports: [TestingUtilsModule, TranslateModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{provide: DatePipe, useValue: datePipe}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateFilterValueRendererComponent);
    component = fixture.componentInstance;
    component.filter = {
      id: 'filter',
      label: 'filter',
      value: {
        kind: 'single-date-value',
        value: ''
      }
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
