import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {WithOperationFilterValueRendererComponent} from './with-operation-filter-value-renderer.component';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';

describe('WithOperationFilterValueRendererComponent', () => {
  let component: WithOperationFilterValueRendererComponent;
  let fixture: ComponentFixture<WithOperationFilterValueRendererComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, TranslateModule.forRoot()],
      declarations: [WithOperationFilterValueRendererComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WithOperationFilterValueRendererComponent);
    component = fixture.componentInstance;
    component.filter = {
      id: 'filter',
      value: {
        kind: 'single-string-value',
        value: ''
      }
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
