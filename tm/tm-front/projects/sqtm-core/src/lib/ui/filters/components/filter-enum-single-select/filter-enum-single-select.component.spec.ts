import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {FilterEnumSingleSelectComponent} from './filter-enum-single-select.component';

describe('FilterEnumSingleSelectComponent', () => {
  let component: FilterEnumSingleSelectComponent;
  let fixture: ComponentFixture<FilterEnumSingleSelectComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FilterEnumSingleSelectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterEnumSingleSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
