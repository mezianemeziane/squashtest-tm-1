import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractFilterWidget} from '../abstract-filter-widget';
import {Filter, FilterOperation, isMultiStringValue, isStringValue} from '../../state/filter.state';
import {KeyCodes} from '../../../utils/key-codes';
import {Workspaces} from '../../../ui-manager/theme.model';

@Component({
  selector: 'sqtm-core-numeric-filter',
  templateUrl: './numeric-filter.component.html',
  styleUrls: ['./numeric-filter.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NumericFilterComponent extends AbstractFilterWidget implements OnInit, OnDestroy {

  valueMin: number;
  valueMax: number;

  operation: FilterOperation;

  between: FilterOperation = FilterOperation.BETWEEN;
  workspace: Workspaces;

  constructor(protected cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  setFilter(filter: Filter) {
    this._filter = filter;
    this.operation = filter.operation;
    const filterValue = filter.value;
    this.workspace = filter.uiOptions?.workspace;
    if (isStringValue(filterValue)) {
      this.valueMin = this.parseNumericValue(filterValue.value);
      this.cdRef.detectChanges();
    } else if (isMultiStringValue(filterValue)) {
      this.valueMin = this.parseNumericValue(filterValue.value[0]);
      this.valueMax = this.parseNumericValue(filterValue.value[1]);
      this.cdRef.detectChanges();
    } else {
      throw Error('Not handled kind ' + filterValue.kind);
    }
  }

  private parseNumericValue(value: string) {
    if (!Boolean(value)) {
      return null;
    }
    let parsedValue: number;
    if (value.includes('.')) {
      parsedValue = Number.parseFloat(value);
    } else {
      parsedValue = Number.parseInt(value, 10);
    }
    return parsedValue;
  }

  closeTextField() {
    this.close.next();
  }

  changeValue() {
    if (this.operation === FilterOperation.BETWEEN) {
      if (this.valueMin != null && this.valueMax != null) {
        const value = this.findBetweenValues();
        this.filteringChanged.next({
          filterValue: {
            kind: 'multiple-string-value',
            value
          },
          operation: this.operation
        });
      }
    } else {
      if (this.valueMin != null) {
        this.filteringChanged.next({
          filterValue: {
            kind: 'single-string-value',
            value: this.valueMin.toString()
          },
          operation: this.operation
        });
      }
    }
    this.closeTextField();
  }

  private findBetweenValues() {
    let valueMinCandidate = this.valueMin;
    let valueMaxCandidate = this.valueMax;
    if (valueMinCandidate > valueMaxCandidate) {
      valueMinCandidate = this.valueMax;
      valueMaxCandidate = this.valueMin;
    }
    const value = [valueMinCandidate.toString(), valueMaxCandidate.toString()];
    return value;
  }

  changeOperation(newOperation: FilterOperation) {
    this.operation = newOperation;
    this.cdRef.markForCheck();
  }

  handleKeyboardInput(key: number) {
    if (key === KeyCodes.ENTER) {
      this.changeValue();
      this.closeTextField();
    } else if (key === KeyCodes.ESC) {
      this.closeTextField();
    }
    return false;
  }

}
