import {ChangeDetectionStrategy, Component, ViewContainerRef} from '@angular/core';
import {Overlay} from '@angular/cdk/overlay';
import {TranslateService} from '@ngx-translate/core';
import {AbstractFilterField} from '../abstract-filter-field';

@Component({
  selector: 'sqtm-core-filter-field',
  templateUrl: './filter-field.component.html',
  styleUrls: ['./filter-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterFieldComponent extends AbstractFilterField {

  constructor(protected overlay: Overlay, protected translateService: TranslateService, protected vcr: ViewContainerRef) {
    super(overlay, translateService, vcr);
  }
}
