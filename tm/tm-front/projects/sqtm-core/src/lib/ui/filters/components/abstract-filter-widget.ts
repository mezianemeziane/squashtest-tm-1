import {Filter, FilteringChange, Scope} from '../state/filter.state';
import {ChangeDetectorRef, Directive, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractFilterWidget implements OnDestroy {

  protected _filter: Filter;

  filteringChanged = new Subject<FilteringChange>();
  close = new Subject<void>();

  protected unsub$ = new Subject<void>();

  get filter(): Filter {
    return this._filter;
  }

  abstract setFilter(filter: Filter, scope: Scope);

  protected constructor(protected cdRef: ChangeDetectorRef) {
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.close.complete();
    this.filteringChanged.complete();
  }

}
