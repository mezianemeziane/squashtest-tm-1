import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {FilterFieldComponent} from './filter-field.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {OverlayModule} from '@angular/cdk/overlay';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {DefaultValueRendererComponent} from '../value-renderers/default-value-renderer/default-value-renderer.component';
import {WorkspaceCommonModule} from '../../../workspace-common/workspace-common.module';
import {FiltersModule} from '../../filters.module';
import {OnPushComponentTester} from '../../../testing-utils/on-push-component-tester';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';

class FilterFieldComponentTester extends OnPushComponentTester<FilterFieldComponent> {
  fieldLabel() {
    return this.element('div');
  }

  fieldValue() {
    return this.element('div');
  }
}

describe('FilterFieldComponent', () => {
  let tester: FilterFieldComponentTester;
  let fixture: ComponentFixture<FilterFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, OverlayModule, FormsModule, TranslateModule.forRoot(), WorkspaceCommonModule, FiltersModule],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterFieldComponent);
    tester = new FilterFieldComponentTester(fixture);
    tester.componentInstance.filterData = {
      filter: {
        id: 'name',
        i18nLabelKey: 'hello.key',
        value: {kind: 'single-string-value', value: 'Space Shuttle'},
        valueRenderer: DefaultValueRendererComponent
      },
      scope: {kind: 'project', value: [], initialValue: [], active: true, initialKind: 'project'}
    };
    fixture.detectChanges();
  });

  it('should show label and value', () => {
    expect(tester.componentInstance).toBeTruthy();
    expect(tester.fieldLabel().textContent).toContain('Hello.key');
    expect(tester.fieldValue().textContent).toContain('Space Shuttle');
  });
});
