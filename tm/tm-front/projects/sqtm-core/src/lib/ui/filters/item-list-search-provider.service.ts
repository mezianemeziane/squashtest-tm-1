import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ListItem} from '../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';

@Injectable()
export abstract class ItemListSearchProvider {
  abstract provideList(listKey: string): Observable<ListItem[]>;
}
