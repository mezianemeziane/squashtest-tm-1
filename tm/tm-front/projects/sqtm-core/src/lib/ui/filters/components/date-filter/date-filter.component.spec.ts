import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {DateFilterComponent} from './date-filter.component';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {OnPushComponentTester} from '../../../testing-utils/on-push-component-tester';
import {TestButton} from 'ngx-speculoos/lib/test-button';

class DateComponentTester extends OnPushComponentTester<DateFilterComponent> {
  getUpdateButton(): TestButton {
    return this.button('[data-test-component-id="update-button"]');
  }

  getCancelButton() {
    return this.button('[data-test-component-id="cancel-button"]');
  }
}

describe('DateFilterComponent', () => {
  let component: DateComponentTester;
  let fixture: ComponentFixture<DateFilterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DateFilterComponent],
      imports: [TestingUtilsModule, TranslateModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateFilterComponent);
    component = new DateComponentTester(fixture);
    component.componentInstance.setFilter({
      id: 'thing',
      i18nLabelKey: '',
      value: {
        kind: 'single-date-value',
        value: ''
      },
      availableOperations: []
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
