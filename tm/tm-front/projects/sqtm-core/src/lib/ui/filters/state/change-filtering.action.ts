import {Identifier} from '../../../model/entity.model';
import {FilteringChange} from './filter.state';

export interface ChangeFilteringAction extends FilteringChange {
  id: Identifier;
}
