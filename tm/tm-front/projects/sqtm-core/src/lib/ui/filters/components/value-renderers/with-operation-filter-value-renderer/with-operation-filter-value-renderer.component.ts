import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractFilterValueRenderer} from '../abstract-filter-value-renderer';
import {TranslateService} from '@ngx-translate/core';
import {FilterOperation, MultipleStringFilterValue} from '../../../state/filter.state';

@Component({
  selector: 'sqtm-core-with-operation-filter-value-renderer',
  templateUrl: './with-operation-filter-value-renderer.component.html',
  styleUrls: ['./with-operation-filter-value-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WithOperationFilterValueRendererComponent extends AbstractFilterValueRenderer implements OnInit {
  constructor(protected cdRef: ChangeDetectorRef, protected translateService: TranslateService) {
    super(cdRef, translateService);
  }

  ngOnInit() {
  }


  protected buildMultiStringValue(filterValue: MultipleStringFilterValue, operation: FilterOperation): string {
    if (filterValue.value.length === 0) {
      return '';
    }
    if (operation === FilterOperation.BETWEEN) {
      const and = this.translateService.instant('sqtm-core.generic.label.and');
      return `${filterValue.value[0]} ${and} ${filterValue.value[1]}`;
    } else {
      return filterValue.value.join(', ');
    }
  }

}
