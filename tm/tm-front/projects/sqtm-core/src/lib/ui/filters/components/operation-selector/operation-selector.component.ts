import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FilterOperation} from '../../state/filter.state';

@Component({
  selector: 'sqtm-core-operation-selector',
  templateUrl: './operation-selector.component.html',
  styleUrls: ['./operation-selector.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OperationSelectorComponent implements OnInit {

  _model: string;

  @Input()
  set operation(value: FilterOperation) {
    this._model = value;
  }

  @Input()
  availableOperations: FilterOperation[];

  @Output()
  operationChanged = new EventEmitter<FilterOperation>();

  constructor() {
  }

  ngOnInit() {
  }

  changeOperation() {
    const newOperation = FilterOperation[this._model];
    this.operationChanged.next(newOperation);
  }

}
