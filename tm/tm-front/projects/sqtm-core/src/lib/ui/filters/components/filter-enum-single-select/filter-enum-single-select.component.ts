import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractFilterWidget} from '../abstract-filter-widget';
import {DiscreteFilterValue, Filter, isDiscreteValue} from '../../state/filter.state';
import {ListItem} from '../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';
import {Workspaces} from '../../../ui-manager/theme.model';
import {SearchColumnPrototypeUtils} from '../../state/search-column-prototypes';

@Component({
  selector: 'sqtm-core-filter-enum-single-select',
  templateUrl: './filter-enum-single-select.component.html',
  styleUrls: ['./filter-enum-single-select.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterEnumSingleSelectComponent extends AbstractFilterWidget implements OnInit {

  items: ListItem[];

  private filterValue: DiscreteFilterValue[];

  workspace: Workspaces;

  constructor(protected cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  ngOnInit(): void {
  }

  setFilter(filter: Filter) {
    const filterValue = filter.value;
    if (isDiscreteValue(filterValue)) {
      this._filter = filter;
      this.workspace = filter.uiOptions?.workspace;
      const ids = filterValue.value.map(value => value.id);
      const id = ids.length === 1 ? ids[0] : null;
      const i18nEnum = SearchColumnPrototypeUtils.findEnum(this.filter.columnPrototype);
      this.items = Object.values(i18nEnum).map(levelEnumItem => {
        return {
          id: levelEnumItem.id,
          selected: id === levelEnumItem.id,
          i18nLabelKey: levelEnumItem.i18nKey
        };
      });
      this.filterValue = filterValue.value;
      this.cdRef.detectChanges();
    } else {
      throw Error('Not handled kind ' + filter.value.kind);
    }
  }

  changeSelection($event: ListItem) {
    const i18nEnum = SearchColumnPrototypeUtils.findEnum(this.filter.columnPrototype);
    const i18nEnumItem = i18nEnum[$event.id];
    this.filteringChanged.next({
      filterValue: {
        kind: 'multiple-discrete-value',
        value: [{id: i18nEnumItem.id, i18nLabelKey: i18nEnumItem.i18nKey}]
      }
    });
    this.close.next();
  }

}
