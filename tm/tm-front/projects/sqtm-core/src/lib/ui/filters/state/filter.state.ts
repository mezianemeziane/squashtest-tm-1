import {Type} from '@angular/core';
import {AbstractFilterWidget} from '../components/abstract-filter-widget';
import {AbstractFilterValueRenderer} from '../components/value-renderers/abstract-filter-value-renderer';
import {Identifier} from '../../../model/entity.model';
import {Workspaces} from '../../ui-manager/theme.model';

export interface FilterData {
  scope: Scope;
  filter: Filter;
}

export interface EntityScope {
  id: string;
  label: string;
  projectId: number;
}

export type ScopeKind = 'project' | 'custom';

export interface Scope {
  value: EntityScope[];
  initialValue: EntityScope[];
  initialKind: ScopeKind;
  kind: ScopeKind;
  active: boolean;
}

export type SimpleScope = Pick<Scope, 'kind' | 'value'>;

export type SimpleFilter = Pick<Filter, 'id' | 'value' | 'operation'>;

export interface Filter {
  id: Identifier;
  // each filter should have either a label (typically for custom fields)
  // or an i18nKey (all system fields)
  label?: string;
  i18nLabelKey?: string;
  // Widget for rendering
  widget?: Type<AbstractFilterWidget>;
  // Widget for rendering
  valueRenderer?: Type<AbstractFilterValueRenderer>;
  // Value can be null
  value?: FilterValue;
  // Column prototype unique key for filters backed by custom report engine server side.
  // Some filters are not implemented by columnPrototype but as custom filters. These custom filters must be identified
  // and are treated as standard filters for Angular client concerns.
  columnPrototype?: ResearchColumnPrototype;
  // Available operations. The model include all operation of custom report engine.
  availableOperations?: FilterOperation[];
  // Current selected operation.
  operation?: FilterOperation;
  // For filters groups. Use in filter manager.
  groupId?: string;
  // Id of the custom field.
  cufId?: number;
  // option for ui
  uiOptions?: {
    workspace: Workspaces
  };
  infoListSelectorOptions?: {
    selectedAttribute: 'id' | 'code'
  };
}

// Model used server side mainly for the research controllers.
// Other backend controllers will have to use the same data shape.
export interface FilterValueModel {
  operation?: string;
  columnPrototype?: string;
  values: string[];
  id: string;
  cufId?: number;
}

export interface FilterGroup {
  id: Identifier;
  i18nLabelKey: string;
}

export enum FilterOperation {
  EQUALS = 'EQUALS',
  LIKE = 'LIKE',
  IN = 'IN',
  AND = 'AND',
  GREATER = 'GREATER',
  GREATER_EQUAL = 'GREATER_EQUAL',
  LOWER = 'LOWER',
  LOWER_EQUAL = 'LOWER_EQUAL',
  BETWEEN = 'BETWEEN',
  NONE = 'NONE',
  AT_LEAST_ONE = 'AT_LEAST_ONE',
  MATCHES = 'MATCHES',
  MIN = 'MIN',
  MAX = 'MAX',
  AVG = 'AVG',
  SUM = 'SUM',
  COUNT = 'COUNT',
  IS_NULL = 'IS_NULL',
  NOT_NULL = 'NOT_NULL',
  NOT_EQUALS = 'NOT_EQUALS',
  BY_DAY = 'BY_DAY',
  BY_WEEK = 'BY_WEEK',
  BY_MONTH = 'BY_MONTH',
  BY_YEAR = 'BY_YEAR',
  FULLTEXT = 'FULLTEXT',
}

export enum ResearchColumnPrototype {

  AUTOMATION_REQUEST_ASSIGNED_TO = 'AUTOMATION_REQUEST_ASSIGNED_TO',

  CAMPAIGN_ATTACHMENT_ENTITY = 'CAMPAIGN_ATTACHMENT_ENTITY',
  CAMPAIGN_ATTACHMENT_ID = 'CAMPAIGN_ATTACHMENT_ID',
  CAMPAIGN_ATTLIST_ENTITY = 'CAMPAIGN_ATTLIST_ENTITY',
  CAMPAIGN_ATTLIST_ID = 'CAMPAIGN_ATTLIST_ID',
  CAMPAIGN_CUF_CHECKBOX = 'CAMPAIGN_CUF_CHECKBOX',
  CAMPAIGN_CUF_DATE = 'CAMPAIGN_CUF_DATE',
  CAMPAIGN_CUF_LIST = 'CAMPAIGN_CUF_LIST',
  CAMPAIGN_CUF_NUMERIC = 'CAMPAIGN_CUF_NUMERIC',
  CAMPAIGN_CUF_TAG = 'CAMPAIGN_CUF_TAG',
  CAMPAIGN_CUF_TEXT = 'CAMPAIGN_CUF_TEXT',
  CAMPAIGN_ENTITY = 'CAMPAIGN_ENTITY',
  CAMPAIGN_ID = 'CAMPAIGN_ID',
  CAMPAIGN_ISSUECOUNT = 'CAMPAIGN_ISSUECOUNT',
  CAMPAIGN_ITERCOUNT = 'CAMPAIGN_ITERCOUNT',
  CAMPAIGN_MILESTONE_ENTITY = 'CAMPAIGN_MILESTONE_ENTITY',
  CAMPAIGN_NAME = 'CAMPAIGN_NAME',
  CAMPAIGN_PROJECT = 'CAMPAIGN_PROJECT',
  CAMPAIGN_PROJECT_ENTITY = 'CAMPAIGN_PROJECT_ENTITY',
  CAMPAIGN_PROJECT_ID = 'CAMPAIGN_PROJECT_ID',
  CAMPAIGN_PROJECT_NAME = 'CAMPAIGN_PROJECT_NAME',
  CAMPAIGN_REFERENCE = 'CAMPAIGN_REFERENCE',
  CAMPAIGN_SCHED_END = 'CAMPAIGN_SCHED_END',
  CAMPAIGN_SCHED_START = 'CAMPAIGN_SCHED_START',
  CAMPAIGN_MILESTONE_LABEL = 'CAMPAIGN_MILESTONE_LABEL',
  CAMPAIGN_MILESTONE_ID = 'CAMPAIGN_MILESTONE_ID',
  CAMPAIGN_MILESTONE_STATUS = 'CAMPAIGN_MILESTONE_STATUS',
  CAMPAIGN_SEARCHABLE_MILESTONE_STATUS = 'CAMPAIGN_SEARCHABLE_MILESTONE_STATUS',
  CAMPAIGN_MILESTONE_END_DATE = 'CAMPAIGN_MILESTONE_END_DATE',

  DATASET_NAME = 'DATASET_NAME',

  EXECUTION_EXECUTION_MODE = 'EXECUTION_EXECUTION_MODE',

  ITEM_TEST_PLAN_ID = 'ITEM_TEST_PLAN_ID',
  ITEM_TEST_PLAN_AUTOEXCOUNT = 'ITEM_TEST_PLAN_AUTOEXCOUNT',
  ITEM_TEST_PLAN_DATASET_LABEL = 'ITEM_TEST_PLAN_DATASET_LABEL',
  ITEM_TEST_PLAN_DSCOUNT = 'ITEM_TEST_PLAN_DSCOUNT',
  ITEM_TEST_PLAN_ENTITY = 'ITEM_TEST_PLAN_ENTITY',
  ITEM_TEST_PLAN_ISSUECOUNT = 'ITEM_TEST_PLAN_ISSUECOUNT',
  ITEM_TEST_PLAN_LABEL = 'ITEM_TEST_PLAN_LABEL',
  ITEM_TEST_PLAN_LASTEXECBY = 'ITEM_TEST_PLAN_LASTEXECBY',
  ITEM_TEST_PLAN_MANEXCOUNT = 'ITEM_TEST_PLAN_MANEXCOUNT',
  ITEM_TEST_PLAN_STATUS = 'ITEM_TEST_PLAN_STATUS',
  ITEM_TEST_PLAN_SUITECOUNT = 'ITEM_TEST_PLAN_SUITECOUNT',
  ITEM_TEST_PLAN_TC_ID = 'ITEM_TEST_PLAN_TC_ID',

  ITERATION_NAME = 'ITERATION_NAME',
  ITERATION_TEST_PLAN_ASSIGNED_USER_LOGIN = 'ITERATION_TEST_PLAN_ASSIGNED_USER_LOGIN',


  REQUIREMENT_VERSION_MILESTONE_ID = 'REQUIREMENT_VERSION_MILESTONE_ID',
  REQUIREMENT_VERSION_ID = 'REQUIREMENT_VERSION_ID',
  REQUIREMENT_CATEGORY = 'REQUIREMENT_CATEGORY',
  REQUIREMENT_CRITICALITY = 'REQUIREMENT_CRITICALITY',
  REQUIREMENT_ENTITY = 'REQUIREMENT_ENTITY',
  REQUIREMENT_ID = 'REQUIREMENT_ID',
  REQUIREMENT_NB_VERSIONS = 'REQUIREMENT_NB_VERSIONS',
  REQUIREMENT_PROJECT = 'REQUIREMENT_PROJECT',
  REQUIREMENT_PROJECT_ENTITY = 'REQUIREMENT_PROJECT_ENTITY',
  REQUIREMENT_PROJECT_ID = 'REQUIREMENT_PROJECT_ID',
  REQUIREMENT_PROJECT_NAME = 'REQUIREMENT_PROJECT_NAME',
  REQUIREMENT_STATUS = 'REQUIREMENT_STATUS',
  REQUIREMENT_VERSION_ATTACHMENT_ENTITY = 'REQUIREMENT_VERSION_ATTACHMENT_ENTITY',
  REQUIREMENT_VERSION_ATTACHMENT_ID = 'REQUIREMENT_VERSION_ATTACHMENT_ID',
  REQUIREMENT_VERSION_ATTCOUNT = 'REQUIREMENT_VERSION_ATTCOUNT',
  REQUIREMENT_VERSION_ATTLIST_ENTITY = 'REQUIREMENT_VERSION_ATTLIST_ENTITY',
  REQUIREMENT_VERSION_ATTLIST_ID = 'REQUIREMENT_VERSION_ATTLIST_ID',
  REQUIREMENT_VERSION_CATEGORY = 'REQUIREMENT_VERSION_CATEGORY',
  REQUIREMENT_VERSION_CATEGORY_ENTITY = 'REQUIREMENT_VERSION_CATEGORY_ENTITY',
  REQUIREMENT_VERSION_CATEGORY_ID = 'REQUIREMENT_VERSION_CATEGORY_ID',
  REQUIREMENT_VERSION_CATEGORY_LABEL = 'REQUIREMENT_VERSION_CATEGORY_LABEL',
  REQUIREMENT_VERSION_CREATED_BY = 'REQUIREMENT_VERSION_CREATED_BY',
  REQUIREMENT_VERSION_CREATED_ON = 'REQUIREMENT_VERSION_CREATED_ON',
  REQUIREMENT_VERSION_CRITICALITY = 'REQUIREMENT_VERSION_CRITICALITY',
  REQUIREMENT_VERSION_CUF_CHECKBOX = 'REQUIREMENT_VERSION_CUF_CHECKBOX',
  REQUIREMENT_VERSION_CUF_DATE = 'REQUIREMENT_VERSION_CUF_DATE',
  REQUIREMENT_VERSION_CUF_LIST = 'REQUIREMENT_VERSION_CUF_LIST',
  REQUIREMENT_VERSION_CUF_NUMERIC = 'REQUIREMENT_VERSION_CUF_NUMERIC',
  REQUIREMENT_VERSION_CUF_TAG = 'REQUIREMENT_VERSION_CUF_TAG',
  REQUIREMENT_VERSION_CUF_TEXT = 'REQUIREMENT_VERSION_CUF_TEXT',
  REQUIREMENT_VERSION_DESCRIPTION = 'REQUIREMENT_VERSION_DESCRIPTION',
  REQUIREMENT_VERSION_ENTITY = 'REQUIREMENT_VERSION_ENTITY',
  REQUIREMENT_VERSION_MILCOUNT = 'REQUIREMENT_VERSION_MILCOUNT',
  REQUIREMENT_VERSION_MILESTONE_END_DATE = 'REQUIREMENT_VERSION_MILESTONE_END_DATE',
  REQUIREMENT_VERSION_MILESTONE_ENTITY = 'REQUIREMENT_VERSION_MILESTONE_ENTITY',
  REQUIREMENT_VERSION_MILESTONE_LABEL = 'REQUIREMENT_VERSION_MILESTONE_LABEL',
  REQUIREMENT_VERSION_MILESTONE_STATUS = 'REQUIREMENT_VERSION_MILESTONE_STATUS',
  REQUIREMENT_VERSION_SEARCHABLE_MILESTONE_STATUS = 'REQUIREMENT_VERSION_SEARCHABLE_MILESTONE_STATUS',
  REQUIREMENT_VERSION_MODIFIED_BY = 'REQUIREMENT_VERSION_MODIFIED_BY',
  REQUIREMENT_VERSION_MODIFIED_ON = 'REQUIREMENT_VERSION_MODIFIED_ON',
  REQUIREMENT_VERSION_NAME = 'REQUIREMENT_VERSION_NAME',
  REQUIREMENT_VERSION_REFERENCE = 'REQUIREMENT_VERSION_REFERENCE',
  REQUIREMENT_VERSION_STATUS = 'REQUIREMENT_VERSION_STATUS',
  REQUIREMENT_VERSION_TCCOUNT = 'REQUIREMENT_VERSION_TCCOUNT',
  REQUIREMENT_VERSION_VERS_NUM = 'REQUIREMENT_VERSION_VERS_NUM',
  REQUIREMENT_VERSION_CURRENT_VERSION = 'REQUIREMENT_VERSION_CURRENT_VERSION',
  REQUIREMENT_VERSION_HAS_DESCRIPTION = 'REQUIREMENT_VERSION_HAS_DESCRIPTION',
  REQUIREMENT_VERSION_HAS_CHILDREN = 'REQUIREMENT_VERSION_HAS_CHILDREN',
  REQUIREMENT_VERSION_HAS_PARENT = 'REQUIREMENT_VERSION_HAS_PARENT',
  REQUIREMENT_VERSION_HAS_LINK_TYPE = 'REQUIREMENT_VERSION_HAS_LINK_TYPE',


  TEST_CASE_PROJECT_NAME = 'TEST_CASE_PROJECT_NAME',
  TEST_CASE_ID = 'TEST_CASE_ID',
  TEST_CASE_NAME = 'TEST_CASE_NAME',
  TEST_CASE_REFERENCE = 'TEST_CASE_REFERENCE',
  TEST_CASE_STATUS = 'TEST_CASE_STATUS',
  TEST_CASE_KIND = 'TEST_CASE_KIND',
  TEST_CASE_IMPORTANCE = 'TEST_CASE_IMPORTANCE',
  TEST_CASE_NATURE = 'TEST_CASE_NATURE',
  TEST_CASE_NATURE_ID = 'TEST_CASE_NATURE_ID',
  TEST_CASE_TYPE = 'TEST_CASE_TYPE',
  TEST_CASE_PROJECT_ID = 'TEST_CASE_PROJECT_ID',
  TEST_CASE_AUTOMATABLE = 'TEST_CASE_AUTOMATABLE',
  TEST_CASE_CREATED_BY = 'TEST_CASE_CREATED_BY',
  TEST_CASE_CREATED_ON = 'TEST_CASE_CREATED_ON',
  TEST_CASE_MODIFIED_BY = 'TEST_CASE_MODIFIED_BY',
  TEST_CASE_MODIFIED_ON = 'TEST_CASE_MODIFIED_ON',
  TEST_CASE_MILCOUNT = 'TEST_CASE_MILCOUNT',
  TEST_CASE_ATTCOUNT = 'TEST_CASE_ATTCOUNT',
  TEST_CASE_VERSCOUNT = 'TEST_CASE_VERSCOUNT',
  TEST_CASE_STEPCOUNT = 'TEST_CASE_STEPCOUNT',
  TEST_CASE_ITERCOUNT = 'TEST_CASE_ITERCOUNT',
  TEST_CASE_CUF_TAG = 'TEST_CASE_CUF_TAG',
  TEST_CASE_CUF_TEXT = 'TEST_CASE_CUF_TEXT',
  TEST_CASE_CUF_NUMERIC = 'TEST_CASE_CUF_NUMERIC',
  TEST_CASE_DESCRIPTION = 'TEST_CASE_DESCRIPTION',
  TEST_CASE_PREQUISITE = 'TEST_CASE_PREQUISITE',
  TEST_CASE_TYPE_ID = 'TEST_CASE_TYPE_ID',
  AUTOMATION_REQUEST_STATUS = 'AUTOMATION_REQUEST_STATUS',
  TO_BE_VALIDATED_AUTOMATION_REQUEST_STATUS = 'TO_BE_VALIDATED_AUTOMATION_REQUEST_STATUS',
  TO_BE_TREAT_AUTOMATION_REQUEST_STATUS = 'TO_BE_TREAT_AUTOMATION_REQUEST_STATUS',
  TEST_CASE_MILESTONE_LABEL = 'TEST_CASE_MILESTONE_LABEL',
  TEST_CASE_MILESTONE_ID = 'TEST_CASE_MILESTONE_ID',
  TEST_CASE_MILESTONE_STATUS = 'TEST_CASE_MILESTONE_STATUS',
  TEST_CASE_SEARCHABLE_MILESTONE_STATUS = 'TEST_CASE_SEARCHABLE_MILESTONE_STATUS',
  TEST_CASE_MILESTONE_END_DATE = 'TEST_CASE_MILESTONE_END_DATE',
  TEST_CASE_PARAMCOUNT = 'TEST_CASE_PARAMCOUNT',
  TEST_CASE_DATASETCOUNT = 'TEST_CASE_DATASETCOUNT',
  TEST_CASE_CALLSTEPCOUNT = 'TEST_CASE_CALLSTEPCOUNT',
  TEST_CASE_EXECOUNT = 'TEST_CASE_EXECOUNT',
  TEST_CASE_CUF_DATE = 'TEST_CASE_CUF_DATE',
  TEST_CASE_CUF_LIST = 'TEST_CASE_CUF_LIST',
  TEST_CASE_CUF_CHECKBOX = 'TEST_CASE_CUF_CHECKBOX',
  TEST_CASE_HASAUTOSCRIPT = 'TEST_CASE_HASAUTOSCRIPT',

  ITEM_TEST_PLAN_TESTER = 'ITEM_TEST_PLAN_TESTER',
  ITEM_TEST_PLAN_TC_DELETED = 'ITEM_TEST_PLAN_TC_DELETED',
  ITEM_TEST_PLAN_IS_EXECUTED = 'ITEM_TEST_PLAN_IS_EXECUTED',
  ITEM_TEST_PLAN_LASTEXECON = 'ITEM_TEST_PLAN_LASTEXECON',

  EXECUTION_ISAUTO = 'EXECUTION_ISAUTO',
  EXECUTION_ISSUECOUNT = 'EXECUTION_ISSUECOUNT',
  EXECUTION_STATUS = 'EXECUTION_STATUS',
}

export interface DiscreteFilterValue {
  id: string | number;
  label?: string;
  i18nLabelKey?: string;
}

export abstract class FilterValue {
  readonly kind: FilterValueKind;
  value: string | string[] | number | DiscreteFilterValue[] | number[] | boolean[];
}

export class StringFilterValue extends FilterValue {
  readonly kind: FilterValueKind = 'single-string-value';
  value: string;
}

export class NumericFilterValue extends FilterValue {
  readonly kind: FilterValueKind = 'single-numeric-value';
  value: number;
}

export class DateFilterValue extends FilterValue {
  readonly kind: FilterValueKind = 'single-date-value';
  value: string;
}


export class MultiDateFilterValue extends FilterValue {
  readonly kind: FilterValueKind = 'multiple-date-value';
  value: string[];
}

export class MultipleStringFilterValue extends FilterValue {
  readonly kind: FilterValueKind = 'multiple-string-value';
  value: string[];
}

export class MultipleBooleanFilterValue extends FilterValue {
  readonly kind: FilterValueKind = 'multiple-boolean-value';
  value: boolean[];
}

export class MultipleNumericValue extends FilterValue {
  readonly kind: FilterValueKind = 'multiple-numeric-value';
  value: number[];
}

export class MultiDiscreteFilterValue extends FilterValue {
  readonly kind: FilterValueKind = 'multiple-discrete-value';
  value: DiscreteFilterValue[];
}

export function isStringValue(value: FilterValue): value is StringFilterValue {
  return value.kind === 'single-string-value';
}

export function isMultiStringValue(value: FilterValue): value is MultipleStringFilterValue {
  return value.kind === 'multiple-string-value';
}

export function isDiscreteValue(value: FilterValue): value is MultiDiscreteFilterValue {
  return value.kind === 'multiple-discrete-value';
}

export function isDateValue(value: FilterValue): value is DateFilterValue {
  return value.kind === 'single-date-value';
}

export function isMultiDateValue(value: FilterValue): value is MultiDateFilterValue {
  return value.kind === 'multiple-date-value';
}

export function isMultiNumericValue(value: FilterValue): value is MultipleNumericValue {
  return value.kind === 'multiple-numeric-value';
}

export function isMultiBooleanValue(value: FilterValue): value is MultipleBooleanFilterValue {
  return value.kind === 'multiple-boolean-value';
}

export function isFilterValueEmpty(value: FilterValue): boolean {
  if (isStringValue(value)) {
    return Boolean(value.value);
  }
  if (isDateValue(value)) {
    return Boolean(value.value);
  }
  if (isMultiStringValue(value)) {
    return value.value.length > 0;
  }
  if (isDiscreteValue(value)) {
    return value.value.length > 0;
  }
  if (isMultiDateValue(value)) {
    return value.value.length > 0;
  }
  if (isMultiNumericValue(value)) {
    return value.value.length > 0;
  }
  if (isMultiBooleanValue(value)) {
    return value.value.length > 0;
  }
  throw new Error('Cannot determine if filter value is empty for kind ' + value.kind);
}

export type FilterValueKind =
  'single-string-value'
  | 'single-numeric-value'
  | 'single-date-value'
  | 'multiple-string-value'
  | 'multiple-date-value'
  | 'multiple-discrete-value'
  | 'multiple-numeric-value'
  | 'multiple-boolean-value';

export interface FilteringChange {
  filterValue?: FilterValue;
  operation?: FilterOperation;
}


export const NULL_VALUE_IN_FILTER = 'SQUASH_TM_2_0_FRONT_END_NULL_VALUE_IN_LIST_FILTERS_TOKEN-885a4cf1-063f-4062-9a15-9b6fda0308e0';
