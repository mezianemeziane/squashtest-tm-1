import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EnumEditableCellComponent } from './enum-editable-cell.component';
import {GridTestingModule} from '../../grid/grid-testing/grid-testing.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {TranslateModule} from '@ngx-translate/core';
import {TestingUtilsModule} from '../../testing-utils/testing-utils.module';

describe('EnumEditableCellComponent', () => {
  let component: EnumEditableCellComponent;
  let fixture: ComponentFixture<EnumEditableCellComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, TestingUtilsModule, OverlayModule, TranslateModule.forRoot()],
      declarations: [ EnumEditableCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnumEditableCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
