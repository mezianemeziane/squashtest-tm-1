import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {ExecutionStatus, ExecutionStatusKeys} from '../../../model/level-enums/level-enum';
import {ColumnDefinitionBuilder} from '../../grid/model/column-definition.builder';
import {GridService} from '../../grid/services/grid.service';
import {AbstractCellRendererComponent} from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';

@Component({
  selector: 'sqtm-core-execution-status-cell',
  template: `
    <ng-container *ngIf="row && row.data[columnDisplay.id]">
      <div class="full-width full-height flex-column">
        <div class="status-badge"
             [style.backgroundColor]="color"
             nz-tooltip [nzTooltipTitle]="i18nKey | translate | capitalize">
        </div>
      </div>
    </ng-container>`,
  styleUrls: ['./execution-status-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionStatusCellComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  get statusKey(): ExecutionStatusKeys {
    return this.row.data[this.columnDisplay.id];
  }

  get color(): string {
    return ExecutionStatus[this.statusKey].color;
  }

  get i18nKey(): string {
    return ExecutionStatus[this.statusKey].i18nKey;
  }
}

export function executionStatusColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ExecutionStatusCellComponent)
    .withHeaderPosition('center');
}
