import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

import {Overlay} from '@angular/cdk/overlay';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs';
import {AbstractListCellRendererComponent} from '../../grid/components/cell-renderers/abstract-list-cell-renderer/abstract-list-cell-renderer';
import {AutomatedTestTechnology} from '../../../model/automation/automated-test-technology.model';
import {GridService} from '../../grid/services/grid.service';
import {ReferentialDataService} from '../../../core/referential/services/referential-data.service';
import {RestService} from '../../../core/services/rest.service';
import {ActionErrorDisplayService} from '../../../core/services/errors-handling/action-error-display.service';
import {ListPanelItem} from '../../workspace-common/components/forms/list-panel/list-panel.component';
import {ColumnDefinitionBuilder} from '../../grid/model/column-definition.builder';
import {catchError, finalize, map, withLatestFrom} from 'rxjs/operators';

@Component({
  selector: 'sqtm-core-automated-test-technology',
  templateUrl: './automated-test-technology.component.html',
  styleUrls: ['./automated-test-technology.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomatedTestTechnologyComponent extends AbstractListCellRendererComponent implements OnInit {
  protected edit = false;

  @ViewChild('templatePortalContent', {read: TemplateRef})
  templatePortalContent: TemplateRef<any>;

  @ViewChild('infoListItemRef', {read: ElementRef})
  infoListItemRef: ElementRef;

  asyncOperationRunning = false;

  automatedTestTechnologies$: Observable<AutomatedTestTechnology[]>;

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              public overlay: Overlay,
              public vcr: ViewContainerRef,
              private referentialDataService: ReferentialDataService,
              public translateService: TranslateService,
              public restService: RestService,
              public actionErrorDisplayService: ActionErrorDisplayService) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);
    this.automatedTestTechnologies$ = this.referentialDataService.automatedTestTechnologies$;
  }

  ngOnInit() {
  }

  showTemplate() {
    if (this.isEditable()) {
      this.showList(this.infoListItemRef, this.templatePortalContent);
    }
  }

  getValue(technologies: AutomatedTestTechnology[], itemId: number) {
    return this.getOptionLabel(technologies.find(item => item.id === itemId));
  }

  getOptionLabel(item: AutomatedTestTechnology): string {
    if (item) {
      return item.name;
    } else {
      return '';
    }
  }

  getListPanelItems(technologies: AutomatedTestTechnology[]): ListPanelItem[] {
    const listPanelItems: ListPanelItem[] = [];
    technologies.forEach(item => {
      listPanelItems.push({
        id: item.id,
        label: item.name
      });
    });
    return listPanelItems;
  }

  change(technoId: number) {
    this.beginAsyncOperation();
    this.grid.updateCellValue(this.row, this.columnDisplay, technoId).pipe(
      withLatestFrom(this.automatedTestTechnologies$),
      map(([, technologies]: [void, AutomatedTestTechnology[]]) => {
        const technoName = this.getValue(technologies, technoId);
        const atTechnologyIdValueChange = {columnId: 'atTechnologyId', value: technoId};
        const columnValueChange = {columnId: this.columnDisplay.id, value: technoName};
        return [atTechnologyIdValueChange, columnValueChange];
      }),
      catchError(err => this.errorDisplayService.handleActionError(err)),
      finalize(() => {
        this.close();
        this.endAsyncOperation();
      })
    ).subscribe((tableValueChanges) => {
      this.grid.editRows([this.row.id], tableValueChanges);
    });
  }

  getLabel(data: any) {
    return data != null ? data : this.translateService.instant('sqtm-core.generic.label.none.feminine');
  }
}

export function automatedTestTechnologyColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(AutomatedTestTechnologyComponent);
}
