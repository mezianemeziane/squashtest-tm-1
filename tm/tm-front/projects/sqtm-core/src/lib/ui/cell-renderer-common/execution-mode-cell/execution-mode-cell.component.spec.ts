import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {NO_ERRORS_SCHEMA} from '@angular/core';
import {GridTestingModule} from '../../grid/grid-testing/grid-testing.module';
import {TestingUtilsModule} from '../../testing-utils/testing-utils.module';
import {ExecutionModeCellComponent} from './execution-mode-cell.component';

describe('ExecutionModeCellComponent', () => {
  let component: ExecutionModeCellComponent;
  let fixture: ComponentFixture<ExecutionModeCellComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ExecutionModeCellComponent],
      imports: [GridTestingModule, TestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionModeCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
