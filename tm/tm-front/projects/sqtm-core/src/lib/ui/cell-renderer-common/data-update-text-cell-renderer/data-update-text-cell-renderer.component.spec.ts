import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DataUpdateTextCellRendererComponent } from './data-update-text-cell-renderer.component';
import {GridDefinition} from '../../grid/model/grid-definition.model';
import {RestService} from '../../../core/services/rest.service';
import {GridService} from '../../grid/services/grid.service';
import {gridServiceFactory} from '../../grid/grid.service.provider';
import {ReferentialDataService} from '../../../core/referential/services/referential-data.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {grid} from '../../../model/grids/grid-builders';
import {getBasicGridDisplay} from '../../grid/grid-testing/grid-testing-utils';
import {Limited} from '../../grid/model/column-definition.model';
import {DialogService} from '../../dialog/services/dialog.service';
import {OverlayModule} from '@angular/cdk/overlay';
import {TestingUtilsModule} from '../../testing-utils/testing-utils.module';

describe('DataUpdateTextCellRendererComponent', () => {
  let component: DataUpdateTextCellRendererComponent;
  let fixture: ComponentFixture<DataUpdateTextCellRendererComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DataUpdateTextCellRendererComponent ],
      imports: [TestingUtilsModule, OverlayModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: DialogService,
          userValue: {}
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(DataUpdateTextCellRendererComponent);
        component = fixture.componentInstance;
        component.gridDisplay = getBasicGridDisplay();
        component.columnDisplay = {
          id: 'column-1', show: true,
          widthCalculationStrategy: new Limited(200),
          headerPosition: 'left', contentPosition: 'left', showHeader: true,
          viewportName: 'mainViewport'
        };
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
