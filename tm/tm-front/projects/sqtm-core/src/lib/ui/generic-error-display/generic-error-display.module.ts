import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {GenericErrorDisplayComponent} from './components/generic-error-display/generic-error-display.component';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {TranslateModule} from '@ngx-translate/core';



@NgModule({
  declarations: [
    GenericErrorDisplayComponent,
  ],
  imports: [
    CommonModule,
    NzIconModule,
    TranslateModule,
  ],
  exports: [
    GenericErrorDisplayComponent,
  ]
})
export class GenericErrorDisplayModule { }
