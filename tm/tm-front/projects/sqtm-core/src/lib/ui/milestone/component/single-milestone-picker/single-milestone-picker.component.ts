import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {
  dateColumn,
  richTextColumn,
  singleSelectRowColumn,
  textColumn
} from '../../../grid/model/column-definition.builder';
import {Extendable, Fixed} from '../../../grid/model/column-definition.model';
import {MilestoneRange, MilestoneStatus} from '../../../../model/level-enums/level-enum';
import {sortDate} from '../../../grid/components/cell-renderers/date-cell-renderer/date-cell-renderer.component';
import {GridDefinition} from '../../../grid/model/grid-definition.model';
import {grid} from '../../../../model/grids/grid-builders';
import {PaginationConfigBuilder} from '../../../grid/model/grid-definition.builder';
import {GridService} from '../../../grid/services/grid.service';
import {gridServiceFactory} from '../../../grid/grid.service.provider';
import {Milestone} from '../../../../model/milestone/milestone.model';
import {filter, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {GridFilter} from '../../../grid/model/state/filter.state';
import {Identifier} from '../../../../model/entity.model';
import {RestService} from '../../../../core/services/rest.service';
import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';

export function singleMilestonePickerDefinition(): GridDefinition {
  return grid('single-milestones-picker-grid')
    .withColumns(
      [
        singleSelectRowColumn()
          .changeWidthCalculationStrategy(new Fixed(40)),
        textColumn('label')
          .withI18nKey('sqtm-core.entity.generic.name.label')
          .changeWidthCalculationStrategy(new Extendable(100, 0.8)),
        textColumn('status')
          .withI18nKey('sqtm-core.entity.milestone.status.label')
          .withEnumRenderer(MilestoneStatus, false, true)
          .isEditable(false),
        dateColumn('endDate')
          .withI18nKey('sqtm-core.entity.milestone.term.label')
          .withSortFunction(sortDate),
        textColumn('range')
          .withI18nKey('sqtm-core.entity.milestone.range.label')
          .withEnumRenderer(MilestoneRange, false, true)
          .isEditable(false),
        richTextColumn('description')
          .disableSort()
          .withI18nKey('sqtm-core.entity.milestone.description')
          .changeWidthCalculationStrategy(new Extendable(100, 1))
      ]
    )
    .withPagination(
      new PaginationConfigBuilder().inactive()
    )
    .disableRightToolBar()
    .disableMultiSelection()
    .enableMultipleColumnsFiltering(['label'])
    .build();
}


@Component({
  selector: 'sqtm-core-single-milestone-picker',
  templateUrl: './single-milestone-picker.component.html',
  styleUrls: ['./single-milestone-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridDefinition,
      useFactory: singleMilestonePickerDefinition,
      deps: []
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, GridDefinition, ReferentialDataService]
    }
  ]
})
export class SingleMilestonePickerComponent implements OnInit, OnDestroy {

  @Input()
  milestones: Milestone[];

  @Input()
  initiallySelectedMilestone: number;

  @Output()
  selectedMilestone: EventEmitter<Milestone> = new EventEmitter<Milestone>();

  @Input()
  disableColumns: Identifier[] = [];

  private unsub$ = new Subject<void>();

  constructor(private gridService: GridService) {
  }

  ngOnInit(): void {
    this.initializeData();
    this.initializeFilters();
    this.initializeOutput();
    this.toggleColumnsVisibility();
  }

  private initializeData() {
    if (Boolean(this.initiallySelectedMilestone)) {
      this.gridService.loadInitialData(this.milestones, this.milestones.length, 'id', [this.initiallySelectedMilestone]);
    } else {
      this.gridService.loadInitialData(this.milestones, this.milestones.length);
    }
  }

  private initializeOutput() {
    this.gridService.selectedRows$.pipe(
      takeUntil(this.unsub$),
      filter((rows) => rows.length === 1)
    ).subscribe((rows) => this.selectedMilestone.emit(rows[0].data as Milestone));
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  private initializeFilters() {
    const filters: GridFilter[] = [{
      id: 'label', active: false, initialValue: {kind: 'single-string-value', value: ''}, tiedToPerimeter: false
    }];
    this.gridService.addFilters(filters);
  }

  handleResearchInput($event: string) {
    this.gridService.applyMultiColumnsFilter($event);
  }

  private toggleColumnsVisibility() {
    this.disableColumns.forEach(columnId => {
      this.gridService.setColumnVisibility(columnId, false);
    });
  }
}
