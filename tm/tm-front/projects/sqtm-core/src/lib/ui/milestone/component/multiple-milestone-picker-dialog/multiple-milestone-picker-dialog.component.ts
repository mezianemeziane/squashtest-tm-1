import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {milestoneLogger} from '../../milestone.logger';
import {Subject} from 'rxjs';
import {MultipleSelectMilestoneDialogConfiguration} from '../single-milestone-picker-dialog/milestone.dialog.configuration';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {Milestone} from '../../../../model/milestone/milestone.model';
import {MULTIPLE_MILESTONES_PICKER_GRID} from '../multiple-milestone-picker/multiple-milestone-picker.component';

const logger = milestoneLogger.compose('MultipleMilestonePickerDialogComponent');


@Component({
  selector: 'sqtm-core-multiple-milestone-picker-dialog',
  templateUrl: './multiple-milestone-picker-dialog.component.html',
  styleUrls: ['./multiple-milestone-picker-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultipleMilestonePickerDialogComponent implements OnInit, AfterViewInit, OnDestroy {

  private unsub$ = new Subject<void>();

  configuration: MultipleSelectMilestoneDialogConfiguration;

  constructor(private dialogReference: DialogReference<MultipleSelectMilestoneDialogConfiguration, Milestone[]>) {
    this.configuration = this.dialogReference.data;
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
  }

  confirm() {
    this.dialogReference.close();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  changeMilestoneSelection(milestone: Milestone[]) {
    this.dialogReference.result = milestone;
  }

}
