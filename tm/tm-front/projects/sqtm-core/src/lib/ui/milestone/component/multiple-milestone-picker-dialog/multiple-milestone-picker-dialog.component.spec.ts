import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {MultipleMilestonePickerDialogComponent} from './multiple-milestone-picker-dialog.component';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {NO_ERRORS_SCHEMA} from '@angular/core';

const dialogReference = jasmine.createSpyObj(['close']);
dialogReference['data'] = {};

describe('MultipleMilestonePickerDialogComponent', () => {
  let component: MultipleMilestonePickerDialogComponent;
  let fixture: ComponentFixture<MultipleMilestonePickerDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [MultipleMilestonePickerDialogComponent],
      providers: [{
        provide: DialogReference,
        useValue: dialogReference
      }],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleMilestonePickerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
