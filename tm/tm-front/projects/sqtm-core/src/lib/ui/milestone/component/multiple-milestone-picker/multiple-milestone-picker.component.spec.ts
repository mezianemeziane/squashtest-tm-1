import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {MultipleMilestonePickerComponent} from './multiple-milestone-picker.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('MultipleMilestonePickerComponent', () => {
  let component: MultipleMilestonePickerComponent;
  let fixture: ComponentFixture<MultipleMilestonePickerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [MultipleMilestonePickerComponent],
      imports: [HttpClientTestingModule, TestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleMilestonePickerComponent);
    component = fixture.componentInstance;
    component.milestones = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
