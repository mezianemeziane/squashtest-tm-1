import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {AbstractAttachmentList} from '../attachment-list/abstract-attachment-list';
import {AttachmentService} from '../../../core/services/attachment.service';

/**
 * Same component as attachment-list but with attachments on one row on some data and messages in overlays to reduce footprint on screen
 */
@Component({
  selector: 'sqtm-core-attachment-list-compact',
  templateUrl: './attachment-list-compact.component.html',
  styleUrls: ['./attachment-list-compact.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AttachmentListCompactComponent extends AbstractAttachmentList implements OnInit {

  constructor(attachmentService: AttachmentService) {
    super(attachmentService);
  }

  ngOnInit() {
  }

}
