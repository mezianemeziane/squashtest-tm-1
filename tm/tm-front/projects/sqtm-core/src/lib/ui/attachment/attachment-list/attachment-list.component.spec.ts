import {ComponentFixture, TestBed} from '@angular/core/testing';
import {AttachmentListComponent} from './attachment-list.component';
import {DebugElement, NO_ERRORS_SCHEMA, Pipe, PipeTransform} from '@angular/core';
import {By} from '@angular/platform-browser';
import {Attachment, PersistedAttachment} from '../../../model/attachment/attachment.model';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../testing-utils/testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';

describe('AttachmentListComponent', () => {
  let component: AttachmentListComponent;
  let fixture: ComponentFixture<AttachmentListComponent>;

  const threeAttachments: PersistedAttachment[] = [{
    id: '1',
    name: 'purpose.txt',
    size: 1045,
    addedOn: new Date(),
    kind: 'persisted-attachment',
    pendingDelete: false
  }, {
    id: '2',
    name: 'image.png',
    size: 40960,
    addedOn: new Date(),
    kind: 'persisted-attachment',
    pendingDelete: false
  }, {
    id: '3',
    name: 'specifications.docx',
    size: 3059670,
    addedOn: new Date(),
    kind: 'persisted-attachment',
    pendingDelete: false
  }];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        TestingUtilsModule
      ],
      declarations: [
        AttachmentListComponent
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentListComponent);
    component = fixture.componentInstance;

    // the list should be empty before changeDetection() occurred
    expect(component.attachments).toBe(undefined);
    expect(getAttachmentElements().length).toBe(0);
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display the expected attachments', () => {
    component.attachments = Array.from(threeAttachments);
    fixture.detectChanges();

    const displayedAttachments = getAttachmentElements();

    expect(displayedAttachments.length).toBe(3);
    expect(areAttachmentsWellDisplayed(component.attachments, displayedAttachments)).toBe(true);


  });

  it('should emit the delete event with the right attachment', () => {
    component.attachments = Array.from(threeAttachments);
    fixture.detectChanges();

    const targetAttachment = getAttachmentElements()[2];
    component.deleteAttachmentEvent.subscribe((attachment: PersistedAttachment) =>
      expect(isAttachmentWellDisplayed(attachment, targetAttachment)).toBe(true));

    const deleteButton = targetAttachment.nativeElement.querySelector('i');
    deleteButton.click();
  });


  /**
   * Get all the DOM elements with class 'attachment'.
   */
  function getAttachmentElements(): DebugElement[] {
    return fixture.debugElement.queryAll(By.css('.attachment'));
  }

  /**
   * Compare the given Attachment objects with the given displayed Attachments.
   * Return true if the displayed ones are the right representation of the given objects.
   * @param expectedAttachments the array of Attachment objects
   * @param displayedElements the array of DebugElements representing the displayed Attachments
   */
  function areAttachmentsWellDisplayed(expectedAttachments: Attachment[], displayedElements: DebugElement[]): boolean {
    for (let i = 0; i < expectedAttachments.length; i++) {
      if (!isAttachmentWellDisplayed(expectedAttachments[i], displayedElements[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   * Compare the given Attachment object with the given displayed Attachment.
   * Return true if the displayed one is the right representation of the given object.
   * @param attachmentObject the Attachment object
   * @param displayedElement the DebugElement representing the displayed Attachment
   */
  function isAttachmentWellDisplayed(attachmentObject: Attachment, displayedElement: DebugElement): boolean {
    if (!attachmentObject || !displayedElement) {
      return false;
    }
    if (attachmentObject.name !== getTextContentByClass(displayedElement, 'name')
      || 'mock-convert-file-size-pipe' !== getTextContentByClass(displayedElement, 'size')
      || !getTextContentByClass(displayedElement, 'added-on').includes('sqtm-core.generic.date.prefix.added-on')) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * Given a DebugElement, get the text content of its first child element holding the given class.
   * @param element the element in which to search
   * @param clazz the class the child element has to match
   */
  function getTextContentByClass(element: DebugElement, clazz: string): string {
    return element.query(By.css(`.${clazz}`)).nativeElement.textContent;
  }

});
