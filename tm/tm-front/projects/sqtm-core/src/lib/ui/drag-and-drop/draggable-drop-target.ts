import {DragAndDropData} from './drag-and-drop-data.model';

export interface DraggableDropTarget {
  notifyCancelDrag(dragData: DragAndDropData): void;
}
