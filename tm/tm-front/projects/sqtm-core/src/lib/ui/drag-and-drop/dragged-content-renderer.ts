import {DragAndDropData} from './drag-and-drop-data.model';

export abstract class DraggedContentRenderer {

  // use DRAG_AND_DROP_DATA injection token to inject dnd data into your custom dnd dragged content
  protected constructor(public dragAnDropData: DragAndDropData) {

  }
}
