import {InjectionToken} from '@angular/core';
import {DragAndDropData} from './drag-and-drop-data.model';

export const DRAG_AND_DROP_DATA = new InjectionToken<DragAndDropData>('Token for injecting dnd data into dragged content renderer');
