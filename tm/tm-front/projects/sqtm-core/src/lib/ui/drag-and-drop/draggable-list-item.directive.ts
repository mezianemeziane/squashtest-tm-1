import {Directive, ElementRef, Host, Input, NgZone, OnDestroy, OnInit} from '@angular/core';
import {BehaviorSubject, fromEvent, Observable, Subject} from 'rxjs';
import {filter, map, switchMap, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {DndPosition, DragAndDropZone} from './events';
import {DraggableListDirective} from './draggable-list.directive';
import {DragAndDropService} from './drag-and-drop.service';
import {createStartDragObservable} from './drag-and-drop.utils';
import {DragAndDropData} from './drag-and-drop-data.model';
import {Identifier} from '../../model/entity.model';
import {isLeftClick} from '../../core/utils/mouse-event-utils';
import {dndLogger} from './sqtm-drag-and-drop.logger';

export type DraggableListItemType = 'node' | 'container' | 'leaf';

const logger = dndLogger.compose('DraggableListItemDirective');

@Directive({
  selector: '[sqtmCoreDraggableListItem]'
})
export class DraggableListItemDirective implements OnInit, OnDestroy {

  @Input()
  set dndData(data: any) {
    this._dndDataSubject.next(data);
  }

  private _id: Identifier;

  @Input('sqtmCoreDraggableListItem')
  set id(value: string | number) {
    logger.trace(`Setting id in draggable item ${this._id} => ${value}`);
    this._id = value;
  }

  @Input()
  dndTargetType: DraggableListItemType = 'leaf';

  @Input()
  enableDrag = true;

  private _dndDataSubject = new BehaviorSubject<any>(null);
  private dragStart$ = new Subject<DndPosition>();
  private hasHandler = false;

  private unsub$ = new Subject<void>();
  private disconnectDataSource$ = new Subject<void>();

  constructor(@Host() private host: ElementRef, private dndList: DraggableListDirective, private dndService: DragAndDropService,
              private ngZone: NgZone) {
    logger.debug(`connecting item ${this._id} to list ${this.dndList.listId}`);
  }

  ngOnInit(): void {
    fromEvent(this.host.nativeElement, 'mousedown').pipe(
      takeUntil(this.unsub$),
      // only left clicks allowed for dnd
      filter(($event: MouseEvent) => isLeftClick($event)),
      filter(() => !this.hasHandler),
      map(($event: MouseEvent) => {
        return {left: $event.clientX, top: $event.clientY};
      }),
      switchMap((position: DndPosition) => createStartDragObservable(this.host, position)),
    ).subscribe((position) => {
      this.requireDragStart(position);
    });

    this.dragStart$.pipe(
      filter(() => this.enableDrag),
      switchMap((position: DndPosition) => {
        return this._dndDataSubject.pipe(
          take(1),
          map(data => {
            return {position, data};
          })
        );
      })
    ).subscribe(({position, data}) => {
      this.dndList.notifyStartDrag(position, data);
    });

    this.ngZone.runOutsideAngular(() => {
      fromEvent(this.host.nativeElement, 'mousemove').pipe(
        takeUntil(this.unsub$),
        withLatestFrom(this.dndService.dragData$),
        filter(([$event, dnd]) => Boolean(dnd)),
      ).subscribe(([$event, dragData]: [MouseEvent, DragAndDropData]) => {
        logger.trace(`hover in item ${this._id} connected to list ${this.dndList.listId}.`);
        switch (this.dndTargetType) {
          case 'container':
            this.handleOverContainer($event);
            break;
          case 'leaf':
            this.handleOverLeaf($event);
            break;
          case 'node':
            this.handleOverNode($event);
            break;
        }
      });
    });
  }

  public requireDragStart(position: DndPosition) {
    this.dragStart$.next(position);
  }

  public registerHandler() {
    this.hasHandler = true;
  }

  private handleOverContainer($event: MouseEvent) {
    this.dndList.notifyDragOverItem({id: this._id, zone: 'into'});
  }

  private handleOverLeaf($event: MouseEvent) {
    const nativeElement = this.host.nativeElement as HTMLElement;
    const rect = nativeElement.getBoundingClientRect();
    const middle = rect.height / 2;
    const distanceFromTop = $event.pageY - rect.top;
    const zone: DragAndDropZone = distanceFromTop < middle ? 'above' : 'bellow';
    this.dndList.notifyDragOverItem({id: this._id, zone});
  }

  private handleOverNode($event: MouseEvent) {
    const nativeElement = this.host.nativeElement as HTMLElement;
    const rect = nativeElement.getBoundingClientRect();
    const borderZoneHeight = rect.height / 4;
    const distanceFromTop = $event.pageY - rect.top;
    const distanceFromBottom = rect.bottom - $event.pageY;
    let zone: DragAndDropZone = 'into';
    if (distanceFromTop < borderZoneHeight) {
      zone = 'above';
    } else if (distanceFromBottom < borderZoneHeight) {
      zone = 'bellow';
    }
    this.dndList.notifyDragOverItem({id: this._id, zone});
  }

  ngOnDestroy(): void {
    // console.log(`Destroying draggable item ${this._id}`);
    this._dndDataSubject.complete();
    this.dragStart$.complete();
    this.unsub$.next();
    this.unsub$.complete();
    this.disconnectDataSource$.next();
    this.disconnectDataSource$.complete();
  }

  connectDataSource(datasource: Observable<any>) {
    this.disconnectDataSource$.next();
    datasource.pipe(
      takeUntil(this.disconnectDataSource$)
    ).subscribe((value) => {
      this._dndDataSubject.next(value);
    });
  }

}
