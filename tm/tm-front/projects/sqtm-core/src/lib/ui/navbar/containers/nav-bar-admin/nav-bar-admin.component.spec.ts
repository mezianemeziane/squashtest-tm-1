import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {NavBarAdminComponent} from './nav-bar-admin.component';
import {TranslateModule} from '@ngx-translate/core';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {WorkspaceCommonModule} from '../../../workspace-common/workspace-common.module';
import {HttpClientModule} from '@angular/common/http';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {SquashPlatformNavigationService} from '../../../../core/services/navigation/squash-platform-navigation.service';
import {Router, RouterModule} from '@angular/router';

describe('NavBarAdminComponent', () => {
  let component: NavBarAdminComponent;
  let fixture: ComponentFixture<NavBarAdminComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule,
        OverlayModule,
        BrowserAnimationsModule,
        WorkspaceCommonModule,
        TranslateModule.forRoot(),
        HttpClientModule,
      ],
      declarations: [NavBarAdminComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{provide: Router, useValue: {}}, {provide: SquashPlatformNavigationService, useValue: {}}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
