import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {selectRowColumn, textColumn} from '../../../grid/model/column-definition.builder';
import {GridService} from '../../../grid/services/grid.service';
import {gridServiceFactory} from '../../../grid/grid.service.provider';
import {RestService} from '../../../../core/services/rest.service';
import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';
import {first, map, switchMap, take, tap, withLatestFrom} from 'rxjs/operators';
import {Project} from '../../../../model/project/project.model';
import {grid} from '../../../../model/grids/grid-builders';
import {GridDefinition} from '../../../grid/model/grid-definition.model';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {Fixed, Limited, Sort} from '../../../grid/model/column-definition.model';
// tslint:disable-next-line:max-line-length
import {ToggleSelectionHeaderRendererComponent} from '../../../grid/components/header-renderers/toggle-selection-header-renderer/toggle-selection-header-renderer.component';
import {PaginationConfigBuilder} from '../../../grid/model/grid-definition.builder';
import {GridFilter} from '../../../grid/model/state/filter.state';
import {isMilestoneModeActivated} from '../../../../core/referential/state/milestone-filter.state';
import {Observable} from 'rxjs';
import {DataRow} from '../../../grid/model/data-row.model';
import {Identifier} from '../../../../model/entity.model';

export function projectFilterGridConfigFactory() {
  return grid('project-filter')
    .withColumns([
      selectRowColumn()
        .changeWidthCalculationStrategy(new Fixed(40))
        .withHeaderRenderer(ToggleSelectionHeaderRendererComponent),
      textColumn('name').withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Limited(300)),
      textColumn('label').withI18nKey('sqtm-core.entity.project.label.tag')
        .changeWidthCalculationStrategy(new Limited(300)),
    ])
    .disableRightToolBar()
    .withPagination(
      new PaginationConfigBuilder()
        .fixPaginationSize(-1)
    ).enableMultipleColumnsFiltering(['name', 'label'])
    .build();
}

@Component({
  selector: 'sqtm-core-project-filter',
  templateUrl: './project-filter.component.html',
  styleUrls: ['./project-filter.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridDefinition,
      useFactory: projectFilterGridConfigFactory,
      deps: []
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, GridDefinition, ReferentialDataService]
    }
  ]
})
export class ProjectFilterComponent implements OnInit, OnDestroy {

  showMilestoneMessage$: Observable<boolean>;

  constructor(private dialogReference: DialogReference,
              private gridService: GridService,
              private refDataService: ReferentialDataService) {
  }

  ngOnInit() {
    this.initializeGrid();
    this.initializeMilestoneMessage();
  }

  private initializeGrid() {
    this.refDataService.projects$.pipe(
      take(1),
      map((projects: Project[]) => {
        return projects.map(value => {
          return {id: value.id, data: {...value}};
        });
      }),
      withLatestFrom(this.refDataService.filteredProjectIds$),
      map(([dataRows, selectedProjectIds]) => this.sortDataRows(dataRows, selectedProjectIds)),
      tap(({dataRows, selectedProjectIds}) => this.gridService.loadInitialDataRows(dataRows, dataRows.length, selectedProjectIds))
    ).subscribe(() => {
      this.gridService.addFilters(this.buildFilters());
    });
  }

  private buildFilters(): GridFilter[] {
    return [{
      id: 'name', active: false, initialValue: {kind: 'single-string-value', value: ''}, tiedToPerimeter: false,
    }, {
      id: 'label', active: false, initialValue: {kind: 'single-string-value', value: ''}, tiedToPerimeter: false,
    }];
  }

  handleChangeProjectFilter() {
    this.gridService.selectedRows$.pipe(
      take(1),
      map(selectedRows => selectedRows.map(row => row.id) as number[]),
      switchMap(selectedRows => this.refDataService.updateProjectFilterList(selectedRows))
    ).subscribe(() => this.dialogReference.close());
  }

  handleKeyboardInput($event: string) {
    this.gridService.applyMultiColumnsFilter($event);
  }

  private initializeMilestoneMessage() {
    this.showMilestoneMessage$ = this.refDataService.milestoneModeData$.pipe(
      take(1),
      map(milestoneModeData => isMilestoneModeActivated(milestoneModeData))
    );
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }

  private sortDataRows(dataRows: Partial<DataRow>[], selectedProjectIds: Identifier[]) {
    dataRows.sort((firstRow, secondRow) => {
      if (selectedProjectIds.includes(firstRow.id)) {
        if (selectedProjectIds.includes(secondRow.id)) {
          return firstRow.data.name.localeCompare(secondRow.data.name);
        } else {
          return -1;
        }
      } else {
        if (selectedProjectIds.includes(secondRow.id)) {
          return 1;
        } else {
          return firstRow.data.name.localeCompare(secondRow.data.name);
        }
      }
    });
    return {dataRows, selectedProjectIds};
  }
}
