import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {NavBarComponent} from './nav-bar.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {OverlayModule} from '@angular/cdk/overlay';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {WorkspaceCommonModule} from '../../../workspace-common/workspace-common.module';
import {RouterTestingModule} from '@angular/router/testing';

describe('NavBarComponent', () => {
  let component: NavBarComponent;
  let fixture: ComponentFixture<NavBarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        TestingUtilsModule,
        OverlayModule,
        BrowserAnimationsModule,
        WorkspaceCommonModule,
        TranslateModule.forRoot(),
        HttpClientModule,
        RouterTestingModule
      ],
      declarations: [NavBarComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(NavBarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
