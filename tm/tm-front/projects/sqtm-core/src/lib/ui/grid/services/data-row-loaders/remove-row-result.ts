import {GridState} from '../../model/state/grid.state';
import {DataRow} from '../../model/data-row.model';

export interface RemoveRowResult {
  state: GridState;
  removedRows: DataRow[];
}
