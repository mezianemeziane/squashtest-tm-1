import {NgModule} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {GridService} from '../services/grid.service';
import {gridServiceFactory} from '../grid.service.provider';
import {RestService} from '../../../core/services/rest.service';
import {CORE_MODULE_CONFIGURATION} from '../../../core/sqtm-core.tokens';
import {defaultSqtmConfiguration} from '../../../core/sqtm-core.module';
import {GridDefinition} from '../model/grid-definition.model';
import {grid} from '../../../model/grids/grid-builders';
import {ReferentialDataService} from '../../../core/referential/services/referential-data.service';
import {APP_BASE_HREF} from '@angular/common';

export function defaultGridConfFactory() {
  return grid('grid-test').build();
}

@NgModule({
  declarations: [],
  imports: [HttpClientTestingModule],
  providers: [
    {provide: CORE_MODULE_CONFIGURATION, useValue: defaultSqtmConfiguration},
    {provide: APP_BASE_HREF, useValue: ''},
    {
      provide: GridDefinition,
      useFactory: defaultGridConfFactory
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
    }
  ]
})
export class GridTestingModule {
}
