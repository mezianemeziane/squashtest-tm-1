import {Injectable} from '@angular/core';
import {AbstractServerOperationHandler} from './abstract-server-operation-handler';
import {combineLatest, Observable, of} from 'rxjs';
import {createStore} from '../../../../core/store/store';
import {
  initialTreeNodeOperationHandlerState,
  TreeNodeServerOperationHandlerState
} from './tree-node-server-operation-handler.state';
import {catchError, concatMap, filter, finalize, map, switchMap, take, tap, withLatestFrom} from 'rxjs/operators';
import {DataRow, dndPlaceholderDataRowId} from '../../model/data-row.model';
import {RestService} from '../../../../core/services/rest.service';
import {DialogService} from '../../../dialog/services/dialog.service';
import {Identifier} from '../../../../model/entity.model';
import {ConfirmConfiguration} from '../../../dialog/components/dialog-templates/confirm-dialog/confirm-configuration';
import {parseDataRowId, SquashTmDataRowType} from '../../../../model/grids/data-row.type';
import {ConfirmDeleteConfiguration} from '../../../dialog/components/dialog-templates/confirm-delete-dialog/confirm-delete-configuration';
import {GridState} from '../../model/state/grid.state';
import {GridServiceImpl} from '../grid-service-impl';
import {initialDndState} from '../../model/state/ui.state';
import {getDescendantIds, getDraggedRows, incrementAsyncOperationCounter} from '../data-row-loaders/data-row-utils';
import {ProjectDataMap} from '../../../../model/project/project-data.model';
import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';
import {HttpErrorResponse} from '@angular/common/http';
import {SquashActionError, SquashError} from '../../../../model/error/error.model';
import {gridLogger} from '../../grid.logger';
import {select} from '@ngrx/store';
import {milestoneAllowModification} from '../../../../model/milestone/milestone.model';
import {ActionErrorDisplayService} from '../../../../core/services/errors-handling/action-error-display.service';
import {selectMilestoneModeData} from '../../../../core/referential/state/referential-data.state';
import {isMilestoneModeActivated} from '../../../../core/referential/state/milestone-filter.state';

// this class represent the errors messages coming from delete simulation server side. It's one of the few case where i18n
// will be done server side, as it's the way pre 2.0 and there is no value to change all of this process.
export class DeleteSimulation {
  messageCollection: string[];
}

const logger = gridLogger.compose('TreeNodeServerOperationHandler');

// Class used to handle copy, delete and move operations for trees in Squash TM. Ex : test case workspace tree...
@Injectable()
export class TreeNodeServerOperationHandler extends AbstractServerOperationHandler {

  protected store = createStore<TreeNodeServerOperationHandlerState>(initialTreeNodeOperationHandlerState());

  // Emits false if milestone mode is enabled and the selected milestones has a blocking status,
  // emits true otherwise.
  private readonly milestoneModeAllowsModification$: Observable<boolean>;

  canCreate$: Observable<boolean>;
  canDelete$: Observable<boolean>;
  // Default implementation allows copy if at least one selected row can be moved
  canCopy$: Observable<boolean>;
  canPaste$: Observable<boolean>;
  canExport$: Observable<boolean>;
  canDrag$: Observable<boolean>;

  protected _grid: GridServiceImpl;

  get grid(): GridServiceImpl {
    return this._grid;
  }

  set grid(gridService: GridServiceImpl) {
    this._grid = gridService;
    this.initCanCreate();
    this.initCanDelete();
    this.initCanCopy();
    this.initCanPaste();
    this.initCanDrag();
    this.initCanExport();
  }

  constructor(protected readonly restService: RestService,
              protected readonly dialogService: DialogService,
              protected readonly referentialDataService: ReferentialDataService,
              protected readonly actionErrorDisplayService: ActionErrorDisplayService) {
    super();

    this.milestoneModeAllowsModification$ = this.referentialDataService.referentialData$.pipe(
      select(selectMilestoneModeData),
      map(milestoneState => isMilestoneModeActivated(milestoneState) ?
        milestoneAllowModification(milestoneState.selectedMilestone) : true));
  }

  protected initCanCreate() {
    this.canCreate$ = this._grid.selectedRows$.pipe(
      map(rows => isArrayNotEmpty(rows) && rows.some(rowAllowsCreation)));
  }

  protected initCanDelete() {
    this.canDelete$ = combineLatest([this._grid.selectedRows$, this.milestoneModeAllowsModification$, this.store.state$]).pipe(
      map(([rows, milestoneModeAllowsModification, state]) => ([
        isArrayNotEmpty(rows),
        rows.some(rowCanBeDeleted),
        !doesSelectionContainLibraries(rows),
        milestoneModeAllowsModification,
        !state.deleteOperationIsRunning
      ])),
      map(areAllTruthy),
    );
  }

  protected initCanCopy() {
    this.canCopy$ = this._grid.selectedRows$.pipe(
      map(rows => this.canCopyAllRows(rows)));
  }

  private canCopyAllRows(rows: DataRow[]): boolean {
    const oneRowCantMove = Boolean(rows.find(row => !row.allowMoves));
    return !oneRowCantMove;
  }

  protected initCanPaste() {
    this.canPaste$ = combineLatest([
      this._grid.selectedRows$,
      this.store.state$,
      this.milestoneModeAllowsModification$,
      this.referentialDataService.referentialData$.pipe(
        select(selectMilestoneModeData),
        map(milestoneState => isMilestoneModeActivated(milestoneState)))
    ]).pipe(
      map(([selectedRows, state, milestoneModeAllowsModification, isMilestoneModeEnabled]) => {
        logger.debug(`
          // BEGIN CAN PASTEt
          Determine if we can paste into selection : ${selectedRows.map(r => r.id)}.
          Copied nodes are ${state.copiedNodes.map(r => r.id)}`);

        let canPaste = milestoneModeAllowsModification;

        const selectionAllowsPaste = isArrayNotEmpty(state.copiedNodes) && selectedRows.length === 1;
        canPaste = canPaste && selectionAllowsPaste;

        const target: DataRow = selectedRows[0];

        if (canPaste && target) {
          let targetAllowsPaste = rowAllowsCreation(target);
          logger.trace(`Determine if permissions allow paste into selection. Can paste : ${targetAllowsPaste}`);

          if (isMilestoneModeEnabled) {
            targetAllowsPaste = targetAllowsPaste && ! isDataRowBoundToBlockingMilestone(target);
            logger.trace(`Milestone mode is enabled: check if target is bound to a blocking milestone. Can paste : ${targetAllowsPaste}`);
          }

          const areAllChildrenAllowed = state.copiedNodes.every(node => isNodeTypeAllowedByTarget(node.type, target));
          logger.trace(`Determine if node nature allow paste into selection. Can paste : ${canPaste}`);

          canPaste = canPaste && targetAllowsPaste && areAllChildrenAllowed;
        }

        logger.trace(`Passed all test to . Can paste : ${canPaste}.
           // END CAN PASTE`);
        return canPaste;
      }),
    );
  }

  protected initCanExport() {
    this.canExport$ = this._grid.selectedRows$.pipe(
      map(rows => rows.some(rowCanBeExported)));
  }

  protected initCanDrag() {
    // in trees, we don't have pagination and thus we should be able to dnd
    // note that rows must be checked, but the check is done in grid-service-impl
    this.canDrag$ = of(true);
  }

  copy(): void {
    this.canCopy$.pipe(
      take(1),
      switchMap(() => this._grid.selectedRows$.pipe(take(1))),
      map(nodes => nodes.filter(rowAllowsMove)),
      filter(isArrayNotEmpty),
      withLatestFrom(this.store.state$),
      map(([nodes, state]) => ({...state, copiedNodes: nodes}))
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  paste(): void {
    combineLatest([this._grid.selectedRows$, this._grid.gridState$, this.store.state$, this.canPaste$]).pipe(
      take(1),
      filter(([selectedRows, gridState, state, canPaste]) => canPaste),
    ).subscribe(([selectedRows, gridState, state, canPaste]) => {
      const destination: DataRow = selectedRows[0];
      const copiedIds = state.copiedNodes.map(node => node.id);
      const sourceProjects = new Set(state.copiedNodes.map(node => node.projectId));
      const destinationProject = destination.projectId;
      const showDifferentProjectWarning = sourceProjects.size > 1 || !sourceProjects.has(destinationProject);
      if (this.shouldCheckInterProjectMoves() && showDifferentProjectWarning) {
        this.showInterProjectPasteWarning(destination, copiedIds, gridState.definitionState.serverRootUrl);
      } else {
        if (this.shouldVerifyPasteOrMoveInto()) {
          this.showPasteOrMoveIntoInfoDialog(destination.id.toString(), copiedIds);
        }
        this.doPaste(destination, copiedIds, gridState.definitionState.serverRootUrl);
      }
    });
  }

  delete(): void {
    combineLatest([this._grid.selectedRows$, this._grid.gridState$, this.canDelete$]).pipe(
      take(1),
      map(([selectedRows, gridState, canDelete]) => ({selectedRows, gridState, canDelete})),
      filter((deleteData: DeleteData) => deleteData.canDelete),
      tap(() => this.notifyBeginDeleteOperation()),
      concatMap((deleteData: DeleteData) => this.fetchSimulationData(deleteData)),
      concatMap((deleteData: DeleteData) => this.showConfirmDeleteDialog(deleteData)),
      filter((deleteData: DeleteData) => Boolean(deleteData.confirm)),
      map((deleteData: DeleteData) => withOnlyDeletableRows(deleteData)),
      map((deleteData: DeleteData) => withIncrementedAsyncOperationCounter(deleteData)),
      tap((deleteData: DeleteData) => this.grid.commit(deleteData.gridState)),
      concatMap((deleteData: DeleteData) => this.doDeleteServerSide(deleteData).pipe(
        catchError(err => this.actionErrorDisplayService.handleActionError(err))
      )),
      tap((deleteData: DeleteData) => this.grid.selectSingleRow(deleteData.nodesToRefreshIds[0])),
      concatMap((deleteData: DeleteData) => this.refreshSubTrees(deleteData.nodesToRefreshIds)),
      tap((state: GridState) => this.grid.commit(state)),
      finalize(() => {
        this.grid.completeAsyncOperation();
        this.notifyCompleteDeleteOperation();
      })
    ).subscribe();
  }

  protected doDeleteServerSide(deleteData: DeleteData) {
    const ids = deleteData.selectedRows.map(row => parseDataRowId(row).toString());
    const pathParam = ids.join(',');
    return this.restService.delete([...deleteData.gridState.definitionState.serverRootUrl, pathParam]).pipe(
      map(() => {
        const nodesToRefreshIds = Array.from(new Set(deleteData.selectedRows.map(row => row.parentRowId)));
        return {...deleteData, nodesToRefreshIds};
      })
    );
  }

  protected showConfirmDeleteDialog(deleteData: DeleteData) {
    const configuration: Partial<ConfirmDeleteConfiguration> = {
      messageKey: `sqtm-core.dialog.delete.element.explain`,
      simulationReportMessages: deleteData.deleteSimulation.messageCollection,
      suffixMessageKey: 'sqtm-core.dialog.delete.element.suffix',
      titleKey: 'sqtm-core.dialog.delete.element.title'
    };
    return this.dialogService.openDeletionConfirm(configuration).dialogClosed$.pipe(
      map(confirm => ({...deleteData, confirm}))
    );
  }

  protected fetchSimulationData(deleteData: DeleteData) {
    const ids = deleteData.selectedRows.map(row => parseDataRowId(row).toString());
    const pathParam = ids.join(',');
    const serverRootUrl: string [] = deleteData.gridState.definitionState.serverRootUrl;
    return this.restService.get<DeleteSimulation>([...serverRootUrl, 'deletion-simulation', pathParam]).pipe(
      map(deleteSimulation => ({...deleteData, deleteSimulation}))
    );
  }

  notifyInternalDrop(): void {

    this.grid.gridState$.pipe(
      take(1),
      filter((state) => this.checkIfDropIsAllowedByGridState(state)),
      tap(() => this.grid.beginAsyncOperation()), // there is a commit state here to state is updated
      switchMap(() => this.grid.gridState$.pipe(take(1))), // switch map to get last version after previous commit
      map((state: GridState) => ({gridState: state})),
      map((data: DropOperationData) => ({...data, isInterProjectMove: this.isInterProjectMove(data.gridState)})),
      concatMap((data: DropOperationData) => this.isInterProjectDropConfirmed(data)),
      filter((data) => data.confirm),
      concatMap((data: DropOperationData) => this.doInternalDrop(data.gridState).pipe(
        map(nextState => ({...data, gridState: nextState}))
      )),
      concatMap<DropOperationData, Observable<DropOperationData>>(data => {
        if (data.isInterProjectMove) {
          return this.grid.dataRowLoader.unselectAllRows(data.gridState).pipe(
            concatMap(nextState => this.grid.gridNodeGenerator.computeNodeTree(nextState)),
            map(nextState => ({...data, gridState: nextState}))
          );
        } else {
          return of(data);
        }
      }),
      finalize(() => this.grid.completeAsyncOperation()),
    ).subscribe(
      (data: DropOperationData) => this.grid.commit(data.gridState),
      err => this.handleServerDropError(err)
    );
  }

  private checkIfDropIsAllowedByGridState(state: GridState): boolean {
    if (!dropIsAllowedByGridState(state)) {
      this.grid.cancelDrag();
      return false;
    }

    return true;
  }

  private handleServerDropError(err) {
    if (err instanceof HttpErrorResponse && err.status === 412 && err.error.squashTMError) {
      const squashError: SquashError = err.error.squashTMError;
      if (squashError.kind === 'ACTION_ERROR') {
        console.log(err.error);
        const actionValidationError = (squashError as SquashActionError).actionValidationError;
        const i18nKey = actionValidationError.exception === 'NameAlreadyExistsAtDestinationException' ?
          'sqtm-core.error.generic.duplicate-name' : 'sqtm-core.error.generic.label';
        this.dialogService.openAlert({
          messageKey: i18nKey,
        });
      }
      this.grid.cancelDrag();
    } else {
      throw err;
    }
  }

  /**
   * Some grids should show an alert when moving nodes from a project to another (because of custom fields and other). You can override this
   * behaviour in subclasses and the inter-project move check will be bypassed.
   * @protected
   */
  protected shouldCheckInterProjectMoves(): boolean {
    return true;
  }

  private isInterProjectDropConfirmed(data: DropOperationData): Observable<DropOperationData> {
    if (data.isInterProjectMove) {
      return this.showInterProjectMoveWarning().pipe(
        map(confirm => {
          data.confirm = confirm;
          return data;
        })
      );
    }
    data.confirm = true;
    return of(data);
  }

  private isInterProjectMove(state: GridState): boolean {
    return this.shouldCheckInterProjectMoves() && this.checkInterProjectMove(state);
  }

  private checkInterProjectMove(state: GridState) {
    const movedNodes = state.uiState.dragState.draggedRowIds.map(id => state.dataRowState.entities[id]);
    const sourceProjects = new Set(movedNodes.map(node => node.projectId));
    const destinationId = state.uiState.dragState.currentDndTarget.id;
    const destination = state.dataRowState.entities[destinationId];
    const destinationProject = destination.projectId;
    return sourceProjects.size > 1 || !sourceProjects.has(destinationProject);
  }

  allowDropSibling(id: Identifier, state: GridState): boolean {
    const dataRowState = state.dataRowState;
    const entities = dataRowState.entities;
    const dataRow: DataRow = entities[id];
    const draggedRowIds: Identifier[] = state.uiState.dragState.draggedRowIds;

    if (id === dndPlaceholderDataRowId) {
      return false;
    }

    // basically if a node in a squash-tm tree doesn't allow move, it's a library... so no drop sibling...
    if (!dataRow.allowMoves) {
      return false;
    }

    if (!dataRow.simplePermissions.canWrite) {
      return false;
    }

    const activeFilterCount = Object.values(state.filterState.filters.entities).filter(f => f.active).length;
    if (activeFilterCount > 0) {
      return false;
    }

    const sortCount = state.columnState.sortedColumns.length;
    if (sortCount > 0) {
      return false;
    }

    // prevent auto drop
    if (draggedRowIds.includes(id)) {
      return false;
    }

    const draggedRows = getDraggedRows(state);

    // prevent to drop rows that are bound to locked milestones
    if (draggedRows.some(row => isDataRowBoundToBlockingMilestone(row))) {
      return false;
    }

    // prevent to drop rows inside a row that is bound to locked milestone
    const parentRow: DataRow = entities[dataRow.parentRowId];
    if (parentRow && isDataRowBoundToBlockingMilestone(parentRow)) {
      return false;
    }

    // prevent drop as sibling of a descendant of one of the selected row
    const descendants = getDescendantIds(dataRowState, draggedRows);
    return !descendants.includes(id);
  }

  // drop into containers are allowed even if there is sorts and filters actives.
  allowDropInto(id: Identifier, state: GridState): boolean {
    const entities = state.dataRowState.entities;
    const dataRow = entities[id];
    const draggedRows = getDraggedRows(state);
    const rowTypesInSelection = new Set(draggedRows.map(row => row.type));

    if (id === dndPlaceholderDataRowId) {
      return false;
    }

    if (!dataRow.simplePermissions.canWrite) {
      return false;
    }

    for (const rowType of rowTypesInSelection) {
      if (!dataRow.allowedChildren.includes(rowType)) {
        return false;
      }
    }

    // prevent auto drop
    if (draggedRows.map(row => row.id).includes(id)) {
      return false;
    }

    // prevent drop in a descendant on one of the selected row
    const descendants = getDescendantIds(state.dataRowState, draggedRows);
    return !descendants.includes(id);
  }

  protected refreshSubTrees(ids: Identifier[], forceOpen?: boolean): Observable<GridState> {
    return this.referentialDataService.projectDatas$.pipe(
      take(1),
      concatMap((projectData: ProjectDataMap) =>
        this.grid.dataRowLoader.refreshSubTrees(ids, this.grid.gridState$, projectData, forceOpen)),
      switchMap(state => this.grid.filterManager.applyFilters(state)),
      concatMap(state => this.grid.gridNodeGenerator.computeNodeTree(state))
    );
  }

  protected doPaste(destination: DataRow, copiedIds, serverRootUrl: string[]) {
    this.grid.beginAsyncOperation();
    this.restService.post([...serverRootUrl, destination.id.toString(), 'content', 'paste'], {references: copiedIds}).pipe(
      concatMap(() => this.refreshSubTrees([destination.id], true)),
      catchError(err => this.actionErrorDisplayService.handleActionError(err)),
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe((state: GridState) => this.grid.commit(state));
  }

  private doInternalDrop(state: GridState): Observable<GridState> {
    let observable: Observable<GridState>;
    const currentDndTarget = state.uiState.dragState.currentDndTarget;
    if (Boolean(currentDndTarget)) {
      switch (currentDndTarget.zone) {
        case 'into':
          observable = this.internalDropIntoNode(state);
          break;
        case 'above':
        case 'bellow':
          observable = this.internalDropSibling(state);
          break;
      }
    } else {
      observable = this.grid.doCancelDrag(state);
    }
    return observable;
  }

  private internalDropIntoNode(state: Readonly<GridState>) {
    let observable: Observable<GridState>;
    const id = state.uiState.dragState.currentDndTarget.id;
    if (this.allowDropInto(id, state)) {
      return this.doInternalDropInto(id, state);
    } else {
      observable = this.grid.doCancelDrag(state);
    }
    return observable;
  }

  /**
   * Introduced for Action Word Workspace which may need to show an info dialog on paste and move into
   * Returns false by default
   */
  protected shouldVerifyPasteOrMoveInto(): boolean {
    return false;
  }

  protected showPasteOrMoveIntoInfoDialog(destinationId: string, draggedRowIds: Identifier[]) {
    // NO OP
  }

  private doInternalDropInto(destination: Identifier, initialState: Readonly<GridState>): Observable<GridState> {
    const draggedRowIds = initialState.uiState.dragState.draggedRowIds;
    if (this.shouldVerifyPasteOrMoveInto()) {
      this.showPasteOrMoveIntoInfoDialog(destination.toString(), draggedRowIds);
    }
    logger.debug(`rows to destination : ${destination}`);
    return this.restService
      .post([...initialState.definitionState.serverRootUrl, destination.toString(), 'content', 'move'], {references: draggedRowIds}).pipe(
        withLatestFrom(this.referentialDataService.projectDatas$),
        switchMap(([, projectDataMap]) => {
          const origins = [...new Set(getDraggedRows(initialState).map(row => row.parentRowId))];
          const rowsToRefresh: Identifier[] = [...origins, destination];
          logger.debug(`rows to refresh : [${rowsToRefresh.join(',')}]`);
          return this.grid.dataRowLoader.refreshSubTrees(rowsToRefresh, this.grid.gridState$, projectDataMap, true);
        }),
        map(state => {
          return {...state, uiState: {...state.uiState, dragState: {...initialDndState()}}};
        }),
        concatMap(state => this.grid.filterManager.applyFilters(state)),
        concatMap(state => this.grid.gridNodeGenerator.computeNodeTree(state)),
      );
  }

  private internalDropSibling(state: Readonly<GridState>) {
    let observable: Observable<GridState>;
    const id = state.uiState.dragState.currentDndTarget.id;
    const zone = state.uiState.dragState.currentDndTarget.zone;
    if (this.allowDropSibling(id, state)) {
      let index = this.findIndexForDnd(state, id);
      if (zone === 'bellow') {
        index++;
      }
      observable = this.doInternalDropSibling(id, index, state);
    } else {
      observable = this.grid.doCancelDrag(state);
    }
    return observable;
  }

  private doInternalDropSibling(destination: Identifier, index: number, initialState: Readonly<GridState>): Observable<GridState> {
    const dataRowState = {...initialState.dataRowState};
    const target: DataRow = dataRowState.entities[destination];
    const parent: DataRow = dataRowState.entities[target.parentRowId];
    logger.debug(`rows to destination : ${destination}`);
    return this.restService.post(
      [...initialState.definitionState.serverRootUrl, parent.id.toString(), 'content', 'move', index.toString()],
      {references: initialState.uiState.dragState.draggedRowIds}).pipe(
      withLatestFrom(this.referentialDataService.projectDatas$),
      concatMap(([, projectDataMap]) => {
        const origins = [...new Set(getDraggedRows(initialState).map(row => row.parentRowId))];
        const destinationParent = initialState.dataRowState.entities[destination].parentRowId;
        const rowsToRefresh: Identifier[] = [...origins, destinationParent];
        logger.debug(`rows to refresh : [${rowsToRefresh.join(',')}]`);
        return this.grid.dataRowLoader.refreshSubTrees(rowsToRefresh, this.grid.gridState$, projectDataMap);
      }),
      map(state => {
        return {...state, uiState: {...state.uiState, dragState: {...initialDndState()}}};
      }),
      concatMap(state => this.grid.filterManager.applyFilters(state)),
      concatMap(state => this.grid.gridNodeGenerator.computeNodeTree(state)),
    );
  }

  private findIndexForDnd(state: GridState, targetId: Identifier): number {
    const entities = state.dataRowState.entities;
    const targetParentId = entities[targetId].parentRowId;
    let index: number;
    if (Boolean(targetParentId)) {
      const filteredChildren = entities[targetParentId].children
        .map(id => entities[id])
        .filter(dataRow => Boolean(dataRow) && !state.uiState.dragState.draggedRowIds.includes(dataRow.id))
        .map(dataRow => dataRow.id);
      index = filteredChildren
        .indexOf(targetId);
    } else {
      throw Error('Root nodes cannot be destination of sibling drag and drop in trees...');
    }
    return index;
  }

  private showInterProjectPasteWarning(destination: DataRow, copiedIds: Identifier[], serverRootUrl: any) {
    const config: ConfirmConfiguration = {
      id: 'confirm-inter-project-paste',
      level: 'WARNING',
      messageKey: 'sqtm-core.dialog.paste.message',
      titleKey: 'sqtm-core.dialog.paste.title'
    };
    this.dialogService.openConfirm(config, 700)
      .dialogClosed$.pipe(
      take(1),
      filter(confirm => confirm)
    ).subscribe(() => this.doPaste(destination, copiedIds, serverRootUrl));
  }

  private showInterProjectMoveWarning(): Observable<boolean> {
    const config: ConfirmConfiguration = {
      id: 'confirm-inter-project-move',
      level: 'WARNING',
      messageKey: 'sqtm-core.dialog.paste.message',
      titleKey: 'sqtm-core.dialog.move.title'
    };
    return this.dialogService.openConfirm(config, 700).dialogClosed$.pipe(
      take(1),
      tap(confirm => {
        if (!confirm) {
          this.grid.cancelDrag();
        }
      })
    );
  }

  private notifyBeginDeleteOperation() {
    this.toggleDeleteOperationIsRunning(true);
  }

  private notifyCompleteDeleteOperation() {
    this.toggleDeleteOperationIsRunning(false);
  }

  private toggleDeleteOperationIsRunning(isRunning: boolean) {
    this.store.state$.pipe(
      take(1),
      map(state => ({...state, deleteOperationIsRunning: isRunning}))
    ).subscribe(state => this.store.commit(state));
  }

}

/**
 * Interface representing the data structure used across a delete operation
 */
export interface DeleteData {
  selectedRows: DataRow[];
  gridState: GridState;
  canDelete: boolean;
  deleteSimulation?: DeleteSimulation;
  confirm?: boolean;
  nodesToRefreshIds?: Identifier[];
}

/**
 * Interface representing the data structure used across a drop operation
 */
export interface DropOperationData {
  gridState: GridState;
  confirm?: boolean;
  isInterProjectMove?: boolean;
}

/* semantic sugar : pure functions to help make raw data manipulation more human-readable */

export function isDataRowBoundToBlockingMilestone(dataRow: DataRow): boolean {
  // TODO PCK: in trees we get the uppercase version whereas in tables (e.g. iteration test plan) we get it in camelcase...
  //  I would need more time to investigate on this. A quick check made me realise we already had lots of casing inconsistencies
  //  in the data rows so... Could be better!
  return Boolean(dataRow.data['boundToBlockingMilestone']) || Boolean(dataRow.data['BOUND_TO_BLOCKING_MILESTONE']);
}

function isArrayNotEmpty(rows: DataRow[]): boolean {
  return rows && rows.length > 0;
}

function rowCanBeDeleted(row: DataRow): boolean {
  return row.allowMoves
    && row.simplePermissions.canDelete
    && !isDataRowBoundToBlockingMilestone(row);
}

function doesSelectionContainLibraries(dataRows: DataRow[]): boolean {
  return dataRows.some(row => isLibraryDataRow(row));
}

function isLibraryDataRow(dataRow: DataRow): boolean {
  return [
    SquashTmDataRowType.ActionWordLibrary,
    SquashTmDataRowType.CampaignLibrary,
    SquashTmDataRowType.CustomReportLibrary,
    SquashTmDataRowType.RequirementLibrary,
    SquashTmDataRowType.TestCaseLibrary,
  ].includes(dataRow.type);
}

function areAllTruthy(array: boolean[]): boolean {
  return array.every(predicate => Boolean(predicate));
}

function rowAllowsCreation(row: DataRow): boolean {
  return row.simplePermissions.canCreate;
}

function rowAllowsMove(row: DataRow): boolean {
  return row.allowMoves && row.simplePermissions.canWrite;
}

function rowCanBeExported(row: DataRow): boolean {
  return row.simplePermissions.canExport;
}

function isNodeTypeAllowedByTarget(type: any, target: DataRow): boolean {
  return target.allowedChildren.includes(type);
}

function dropIsAllowedByGridState(state: GridState): boolean {
  return state.definitionState.enableInternalDrop
    && state.uiState.dragState.dragging
    && Boolean(state.uiState.dragState.currentDndTarget);
}

function withOnlyDeletableRows(deleteData: DeleteData): DeleteData {
  return {
    ...deleteData,
    selectedRows: deleteData.selectedRows.filter(rowCanBeDeleted),
  };
}

function withIncrementedAsyncOperationCounter(deleteData: DeleteData): DeleteData {
  return {
    ...deleteData,
    gridState: incrementAsyncOperationCounter(deleteData.gridState),
  };
}
