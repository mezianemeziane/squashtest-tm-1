import {Identifier} from '../../../../model/entity.model';
import {GridViewportName} from '../state/column.state';

export class MoveColumnAction {
  id: Identifier;
  index: number;
  moveToViewport: GridViewportName;
}
