import {GridState} from '../../model/state/grid.state';
import {Observable, of} from 'rxjs';
import {
  filterEntityAdapter,
  filterGroupEntityAdapter,
  FilterState,
  GridFilter,
  GridFilters
} from '../../model/state/filter.state';
import {FilterManager} from './filter-manager';
import {createEntityAdapter} from '@ngrx/entity';
import {DataRow} from '../../model/data-row.model';
import {getAncestorIds} from '../data-row-loaders/data-row-utils';
import {ChangeFilteringAction} from '../../../filters/state/change-filtering.action';
import {Update} from '@ngrx/entity/src/models';
import {ProjectDataMap} from '../../../../model/project/project-data.model';
import {CustomField} from '../../../../model/customfield/customfield.model';
import {InputType} from '../../../../model/customfield/input-type.model';
// tslint:disable-next-line:max-line-length
import {MultiValueCufFilterComponent} from '../../../filters/components/custom-fields/multi-value-cuf-filter/multi-value-cuf-filter.component';
import {BindableEntity} from '../../../../model/bindable-entity.model';
// tslint:disable-next-line:max-line-length
import {WithOperationFilterValueRendererComponent} from '../../../filters/components/value-renderers/with-operation-filter-value-renderer/with-operation-filter-value-renderer.component';
import {DateFilterComponent} from '../../../filters/components/date-filter/date-filter.component';
// tslint:disable-next-line:max-line-length
import {DateFilterValueRendererComponent} from '../../../filters/components/value-renderers/date-filter-value-renderer/date-filter-value-renderer.component';
import {CheckboxCufFilterComponent} from '../../../filters/components/custom-fields/checkbox-cuf-filter/checkbox-cuf-filter.component';
import {
  FilterGroup,
  FilterOperation,
  FilterValue,
  isDiscreteValue,
  isMultiBooleanValue,
  isStringValue,
  ResearchColumnPrototype,
  Scope
} from '../../../filters/state/filter.state';
import {NumericFilterComponent} from '../../../filters/components/numeric-filter/numeric-filter.component';
import {EditFilterTextComponent} from '../../../filters/components/edit-filter-text/edit-filter-text.component';

type CufFilterVariantParts = Pick<GridFilter,
  'columnPrototype'
  | 'availableOperations'
  | 'operation'
  | 'initialValue'
  | 'widget'
  | 'valueRenderer'>;

export abstract class AbstractFilterManager implements FilterManager {

  protected adapter = createEntityAdapter<GridFilter>();

  changeFilterValue(action: ChangeFilteringAction, state: GridState): Observable<GridState> {
    const filterValue: FilterValue = action.filterValue;

    if (!Boolean(filterValue)) {
      return of(state);
    }

    if (isStringValue(filterValue)) {
      filterValue.value.trim();
    }

    const filters = filterEntityAdapter.updateOne({
      id: action.id as any,
      changes: {value: action.filterValue}
    }, state.filterState.filters);
    return of({...state, filterState: {...state.filterState, filters}});
  }

  changeFilterOperation(action: ChangeFilteringAction, state: GridState): Observable<GridState> {
    const operation = action.operation;
    if (!Boolean(operation)) {
      return of(state);
    }

    const filters = filterEntityAdapter.updateOne({
      id: action.id as any,
      changes: {operation}
    }, state.filterState.filters);
    return of({...state, filterState: {...state.filterState, filters}});
  }

  applyFilters(state: GridState): Observable<GridState> {
    const filterState = {...state.filterState};
    if (state.filterState.filters.ids.length === 0) {
      filterState.filters.matchingRowIds = [];
    } else {
      const filteredRows = this.filterDataRows(Object.values(state.dataRowState.entities), state.filterState);
      filterState.filters.matchingRowIds = filteredRows.map(row => row.id);
      filterState.filters.ancestorMatchingRowIds = getAncestorIds(state.dataRowState, filteredRows);
    }
    return of({...state, filterState});
  }

  private filterDataRows(dataRows: DataRow[], filterState: FilterState): DataRow[] {
    const filters = Object.values(filterState.filters.entities);
    if (filters.length === 0) {
      return dataRows;
    }

    return dataRows.filter(row => {
      let result = true;
      const filterStack = [...filters].filter(f => f.active);
      let filter = filterStack.pop();
      while (result && filter) {
        const filterFunction = filter.filterFunction;
        if (filterFunction && typeof filterFunction === 'function') {
          result = filterFunction(filter, row);
        } else {
          result = defaultFilterDataRows(filter, row);
        }
        filter = filterStack.pop();
      }
      return result;
    });
  }

  addFilters(addedFilters: GridFilter[], state: GridState): GridState {
    addedFilters.forEach(addedFilter => {
      if (addedFilter.value == null) {
        addedFilter.value = addedFilter.initialValue;
      }
    });
    const filters = filterEntityAdapter.addMany(addedFilters, state.filterState.filters);
    return {...state, filterState: {...state.filterState, filters}};
  }

  replaceFilters(addedFilters: GridFilter[], state: GridState): GridState {
    addedFilters.forEach(addedFilter => {
      if (addedFilter.value == null) {
        addedFilter.value = addedFilter.initialValue;
      }
    });
    const filters = filterEntityAdapter.setAll(addedFilters, state.filterState.filters);
    return {...state, filterState: {...state.filterState, filters}};
  }

  addFilterGroups(addedGroups: FilterGroup[], state: GridState): GridState {
    const filterGroups = filterGroupEntityAdapter.addMany(addedGroups, state.filterState.filterGroups);
    return {...state, filterState: {...state.filterState, filterGroups}};
  }

  replaceFilterGroups(addedGroups: FilterGroup[], state: GridState): GridState {
    const filterGroups = filterGroupEntityAdapter.setAll(addedGroups, state.filterState.filterGroups);
    return {...state, filterState: {...state.filterState, filterGroups}};
  }

  toggleFilter(id: string, state: Readonly<GridState>): GridState {
    const active = !state.filterState.filters.entities[id].active;
    const filters = filterEntityAdapter.updateOne({
      id,
      changes: {active, preventOpening: false}
    }, state.filterState.filters);
    return {...state, filterState: {...state.filterState, filters}};
  }

  activateFilter(id: string, state: Readonly<GridState>): GridState {
    const filters = filterEntityAdapter.updateOne({
      id,
      changes: {active: true, preventOpening: false}
    }, state.filterState.filters);
    return {...state, filterState: {...state.filterState, filters}};
  }

  inactivateFilter(id: string, state: Readonly<GridState>): GridState {
    const filters = filterEntityAdapter.updateOne({
      id,
      changes: {active: false, preventOpening: false}
    }, state.filterState.filters);
    return {...state, filterState: {...state.filterState, filters}};
  }

  resetFilter(id: string, state: Readonly<GridState>): GridState {
    const value = state.filterState.filters.entities[id];
    const nextValue = {...value.initialValue};
    const filters = filterEntityAdapter.updateOne({id, changes: {value: nextValue}}, state.filterState.filters);
    return {...state, filterState: {...state.filterState, filters}};
  }

  resetFilters(ids: string[], state: Readonly<GridState>): GridState {
    const initialFilters = ids.map(id => state.filterState.filters.entities[id]);
    const changes: Update<GridFilter>[] = initialFilters.map(initialFilter => {
      return {
        id: initialFilter.id as any,
        changes: {value: initialFilter.initialValue, active: initialFilter.alwaysActive}
      };
    });
    const filters = filterEntityAdapter.updateMany(changes, state.filterState.filters);
    return {...state, filterState: {...state.filterState, filters}};
  }

  resetAllFilters(state: Readonly<GridState>): GridState {
    return this.resetFilters(state.filterState.filters.ids as string[], state);
  }

  replaceScope(scope: Scope, projectMap: ProjectDataMap, state: Readonly<GridState>): GridState {
    const generatedFilters = this.generateCustomFieldFilters(scope, state, projectMap);
    let filters: GridFilters = state.filterState.filters;
    // find and remove old filters that are not in the new scope
    const filtersToRemove: string[] = Object.values(filters.entities)
      .filter(f => f.cufId != null) // we want to remove only cuf filters, not system filters...
      .filter(f => generatedFilters.findIndex(generatedFilter => generatedFilter.cufId === f.cufId) === -1)
      .map(f => f.id.toString());
    filters = filterEntityAdapter.removeMany(filtersToRemove, filters);
    // Ngrx Entity Framework will not add already existing filters
    filters = filterEntityAdapter.addMany(generatedFilters, filters);
    const filtersToReset = Object.values(filters.entities)
      .filter(f => f.tiedToPerimeter)
      .map(f => f.id);
    const nextState = {...state, filterState: {...state.filterState, filters, scope: {...scope}}};
    return this.resetFilters(filtersToReset as string[], nextState);
  }

  private generateCustomFieldFilters(scope: Scope, state: Readonly<GridState>, projectMap: ProjectDataMap): GridFilter[] {
    const bindableEntity = state.definitionState.addFilterCufFrom;
    if (bindableEntity) {
      // 1 Extract all the project id from scope.
      const projectIds = scope.value.map(entity => entity.projectId);

      // 2 Extract all custom fields from these projects.
      const customFields: CustomField[] = projectIds
        .map(projectId => projectMap[projectId])
        .map(projectData => projectData.customFieldBinding[bindableEntity])
        .flat(1)
        .map(binding => binding.customField)
        // .distinct( is not implemented yet, so doing it old way:
        // Filter all items where the actual index is not the first index for this item)
        .filter(
          (cuf, index, array) => array.findIndex(c => c.id === cuf.id) === index);

      customFields.sort((a, b) => a.label.localeCompare(b.label));

      // 3 Dynamically generate filters for custom fields
      return customFields
        // temporary filter to avoid crash during dev when all widget types are not implemented
        .filter(cuf => cuf.inputType !== InputType.RICH_TEXT)
        .map(cuf => {
          return this.generateOneCufFilter(cuf, bindableEntity);
        });
    } else {
      return [];
    }
  }

  private generateOneCufFilter(cuf: CustomField, bindableEntity: BindableEntity): GridFilter {
    let variantFilterParts: CufFilterVariantParts;
    switch (cuf.inputType) {
      case InputType.TAG:
        variantFilterParts = this.generateTagFilter(bindableEntity);
        break;
      case InputType.PLAIN_TEXT:
        variantFilterParts = this.generatePlainTextFilter(bindableEntity);
        break;
      case InputType.NUMERIC:
        variantFilterParts = this.generateNumericFilter(bindableEntity);
        break;
      case InputType.DATE_PICKER:
        variantFilterParts = this.generateDateFilter(bindableEntity);
        break;
      case InputType.DROPDOWN_LIST:
        variantFilterParts = this.generateListFilter(bindableEntity);
        break;
      case InputType.CHECKBOX:
        variantFilterParts = this.generateCheckboxFilter(bindableEntity);
        break;
      default:
        throw Error('Not handled type ' + cuf.inputType);
    }
    return this.generateFilterFromVariantParts(variantFilterParts, cuf);
  }

  private generateTagFilter(bindableEntity: BindableEntity): CufFilterVariantParts {
    return {
      columnPrototype: ResearchColumnPrototype[`${bindableEntity}_CUF_TAG`],
      availableOperations: [FilterOperation.IN],
      operation: FilterOperation.IN,
      initialValue: {kind: 'multiple-discrete-value', value: []},
      widget: MultiValueCufFilterComponent,
    };
  }

  private generateCheckboxFilter(bindableEntity: BindableEntity): CufFilterVariantParts {
    return {
      columnPrototype: ResearchColumnPrototype[`${bindableEntity}_CUF_CHECKBOX`],
      availableOperations: [FilterOperation.IN],
      operation: FilterOperation.IN,
      initialValue: {kind: 'multiple-discrete-value', value: []},
      widget: CheckboxCufFilterComponent,
    };
  }


  private generateListFilter(bindableEntity: BindableEntity): CufFilterVariantParts {
    return {
      columnPrototype: ResearchColumnPrototype[`${bindableEntity}_CUF_LIST`],
      availableOperations: [FilterOperation.IN],
      operation: FilterOperation.IN,
      initialValue: {kind: 'multiple-discrete-value', value: []},
      widget: MultiValueCufFilterComponent,
    };
  }

  private generatePlainTextFilter(bindableEntity: BindableEntity): CufFilterVariantParts {
    return {
      columnPrototype: ResearchColumnPrototype[`${bindableEntity}_CUF_TEXT`],
      availableOperations: [FilterOperation.LIKE],
      operation: FilterOperation.LIKE,
      initialValue: {kind: 'single-string-value', value: ''},
      widget: EditFilterTextComponent,
    };
  }

  private generateNumericFilter(bindableEntity: BindableEntity): CufFilterVariantParts {
    return {
      columnPrototype: ResearchColumnPrototype[`${bindableEntity}_CUF_NUMERIC`],
      availableOperations: [
        FilterOperation.EQUALS,
        FilterOperation.GREATER,
        FilterOperation.GREATER_EQUAL,
        FilterOperation.LOWER,
        FilterOperation.LOWER_EQUAL,
        FilterOperation.BETWEEN,
      ],
      operation: FilterOperation.EQUALS,
      initialValue: {kind: 'single-string-value', value: null},
      widget: NumericFilterComponent,
      valueRenderer: WithOperationFilterValueRendererComponent
    };
  }

  private generateDateFilter(bindableEntity: BindableEntity): CufFilterVariantParts {
    return {
      columnPrototype: ResearchColumnPrototype[`${bindableEntity}_CUF_DATE`],
      availableOperations: [
        FilterOperation.EQUALS,
        FilterOperation.GREATER,
        FilterOperation.GREATER_EQUAL,
        FilterOperation.LOWER,
        FilterOperation.LOWER_EQUAL,
        FilterOperation.BETWEEN,
      ],
      operation: FilterOperation.EQUALS,
      initialValue: {kind: 'single-date-value', value: ''},
      widget: DateFilterComponent,
      valueRenderer: DateFilterValueRendererComponent
    };
  }

  private generateFilterFromVariantParts(variantFilterParts: CufFilterVariantParts, cuf: CustomField) {
    const filter: GridFilter = {
      ...variantFilterParts,
      cufId: cuf.id,
      active: false,
      id: `CUF_FILTER_${cuf.id}`,
      groupId: 'custom-fields',
      alwaysActive: false,
      label: cuf.label,
      value: {...variantFilterParts.initialValue},
      tiedToPerimeter: false, // no need to tie to perimeter as the whole filter is removed/added
    };
    return filter;
  }

  resetScope(state: Readonly<GridState>): GridState {
    const scope = {...state.filterState.scope};
    scope.value = [...scope.initialValue];
    scope.kind = scope.initialKind;
    return {...state, filterState: {...state.filterState, scope}};
  }

  applyMultiColumnsFilter(state: GridState, value: any): Observable<GridState> {
    const columns = state.definitionState.multipleColumnsFilter;
    if (columns.length === 0) {
      return of(state);
    }
    const filterState = state.filterState;
    const filters = Object.values(filterState.filters.entities);
    const globalFilters: GridFilter[] = filters.filter(f => columns.includes(f.id.toString()));
    const filteredDataRows = this.filterDataRowsMultiColumns(Object.values(state.dataRowState.entities), globalFilters);
    filterState.filters.matchingRowIds = filteredDataRows.map(v => v.id);
    filterState.filters.ancestorMatchingRowIds = getAncestorIds(state.dataRowState, filteredDataRows);
    return of({...state, filterState});
  }

  updateMultiColumnsFilterValue(state: GridState, value: any): GridState {
    const initialFilterState = state.filterState;
    const columns: string[] = state.definitionState.multipleColumnsFilter;
    const initialFilters = columns.map(c => initialFilterState.filters.entities[c]);
    const changes: Update<GridFilter>[] = initialFilters.map(initialFilter => {
      initialFilter.value.value = value;
      return {
        id: initialFilter.id as any,
        changes: {value: initialFilter.value, active: true}
      };
    });
    const filters = filterEntityAdapter.updateMany(changes, state.filterState.filters);
    return {...state, filterState: {...state.filterState, filters}};
  }

  private filterDataRowsMultiColumns(dataRows: DataRow[], gridFilters: GridFilter[]): DataRow[] {
    return dataRows.filter(row => {
      const result = gridFilters.filter(filter => {
        const filterFunction = filter.filterFunction;
        let filtered: boolean;
        if (filterFunction && typeof filterFunction === 'function') {
          filtered = filterFunction(filter, row);
        } else {
          filtered = defaultFilterDataRows(filter, row);
        }
        return filtered;
      });
      return result.length > 0;
    });
  }

}

function defaultFilterDataRows(filter: GridFilter, row: DataRow): boolean {
  let result = true;
  const rowValue = row.data[filter.id];
  if (rowValue == null) {
    result = false;
  } else if (typeof rowValue === 'string') {
    const filterValue = filter.value;
    const lowerCaseRowValue = rowValue.toLowerCase();
    if (isStringValue(filterValue)) {
      result = lowerCaseRowValue.includes(filterValue.value.toLowerCase());
    }
    if (isDiscreteValue(filterValue)) {
      result = filterValue.value.length === 0 || filterValue.value.map(v => v.id.toString().toLowerCase()).includes(lowerCaseRowValue);
    }

  } else if (typeof rowValue === 'boolean') {
    const filterValue = filter.value;
    const lowerCaseRowValue = rowValue.toString().toLowerCase();
    if (isMultiBooleanValue(filterValue)) {
      result = filterValue.value.length === 0 || filterValue.value.map(v => v.toString().toLowerCase()).includes(lowerCaseRowValue);
    }
  }

  return result;
}
