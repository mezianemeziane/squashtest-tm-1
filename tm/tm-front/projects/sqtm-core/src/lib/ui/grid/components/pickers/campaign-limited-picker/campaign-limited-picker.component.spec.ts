import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CampaignLimitedPickerComponent} from './campaign-limited-picker.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {RouterTestingModule} from '@angular/router/testing';

describe('CampaignLimitedPickerComponent', () => {
  let component: CampaignLimitedPickerComponent;
  let fixture: ComponentFixture<CampaignLimitedPickerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule, OverlayModule, RouterTestingModule],
      declarations: [CampaignLimitedPickerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignLimitedPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
