import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {EditableRichTextRendererComponent} from './editable-rich-text-renderer.component';
import {grid} from '../../../../../model/grids/grid-builders';
import {GridDefinition} from '../../../model/grid-definition.model';
import {RestService} from '../../../../../core/services/rest.service';
import {GridService} from '../../../services/grid.service';
import {gridServiceFactory} from '../../../grid.service.provider';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {getBasicGridDisplay} from '../../../grid-testing/grid-testing-utils';
import {TestCaseLibrary} from '../../../../../model/grids/data-row.type';
import {Limited} from '../../../model/column-definition.model';
import {ReferentialDataService} from '../../../../../core/referential/services/referential-data.service';

describe('EditableRichTextRendererComponent', () => {
  let component: EditableRichTextRendererComponent;
  let fixture: ComponentFixture<EditableRichTextRendererComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EditableRichTextRendererComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(EditableRichTextRendererComponent);
        component = fixture.componentInstance;
        component.gridDisplay = getBasicGridDisplay();
        component.columnDisplay = {
          id: 'column-1', show: true,
          widthCalculationStrategy: new Limited(200),
          headerPosition: 'left', contentPosition: 'left', showHeader: true,
          viewportName: 'mainViewport'
        };
        component.row = {
          ...new TestCaseLibrary(),
          id: 'tcln-1',
          data: {id: 'tcln-1'},
        };
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
