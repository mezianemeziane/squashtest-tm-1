import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CheckBoxCellRendererComponent} from './check-box-cell-renderer.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestCaseLibrary} from '../../../../../model/grids/data-row.type';
import {GridDefinition} from '../../../model/grid-definition.model';
import {RestService} from '../../../../../core/services/rest.service';
import {GridService} from '../../../services/grid.service';
import {gridServiceFactory} from '../../../grid.service.provider';
import {getBasicGridDisplay} from '../../../grid-testing/grid-testing-utils';
import {grid} from '../../../../../model/grids/grid-builders';
import {Limited} from '../../../model/column-definition.model';
import {ReferentialDataService} from '../../../../../core/referential/services/referential-data.service';

describe('CheckBoxCellRendererComponent', () => {
  let component: CheckBoxCellRendererComponent;
  let fixture: ComponentFixture<CheckBoxCellRendererComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CheckBoxCellRendererComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents()
      .then(value => {
        fixture = TestBed.createComponent(CheckBoxCellRendererComponent);
        component = fixture.componentInstance;
        component.gridDisplay = getBasicGridDisplay();
        component.columnDisplay = {
          id: 'column-1',
          show: true,
          widthCalculationStrategy: new Limited(200),
          headerPosition: 'left', contentPosition: 'left', showHeader: true,
          viewportName: 'mainViewport'
        };
        component.row = {
          ...new TestCaseLibrary(),
          id: 'tcln-1',
          data: {id: 'tcln-1'},
        };
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
