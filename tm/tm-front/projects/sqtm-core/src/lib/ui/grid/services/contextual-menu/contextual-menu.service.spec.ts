import {TestBed} from '@angular/core/testing';

import {Overlay, OverlayModule} from '@angular/cdk/overlay';
import {ContextualMenuService} from './contextual-menu.service';

describe('OverlayMenuService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [OverlayModule],
    providers: [
      {provide: ContextualMenuService, useClass: ContextualMenuService, deps: [Overlay]}
    ]
  }));

  it('should be created', () => {
    const service: ContextualMenuService = TestBed.inject(ContextualMenuService);
    expect(service).toBeTruthy();
  });
});
