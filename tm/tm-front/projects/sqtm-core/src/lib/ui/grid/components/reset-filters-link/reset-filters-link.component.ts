import {ChangeDetectionStrategy, Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'sqtm-core-reset-filters-link',
  template: `
    <div class="flex-row">
      <i nz-icon nzTheme="outline" [nzType]="'filter'"
         class="font-12-px reset-filter-icon"></i>
      <a class="reset-filter-link"
         (click)="handleResetFiltersClicked()">
        {{'sqtm-core.grid.reset-filters.label' | translate}}</a>
    </div>
  `,
  styleUrls: ['./reset-filters-link.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResetFiltersLinkComponent {

  @Output()
  resetFiltersLinkPressed = new EventEmitter<void>();

  constructor() {
  }

  handleResetFiltersClicked(): void {
    this.resetFiltersLinkPressed.emit();
  }
}
