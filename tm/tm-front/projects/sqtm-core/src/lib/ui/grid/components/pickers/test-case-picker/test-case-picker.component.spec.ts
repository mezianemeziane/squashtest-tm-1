import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {TestCasePickerComponent} from './test-case-picker.component';
import {EMPTY} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {RouterTestingModule} from '@angular/router/testing';

describe('TestCasePickerComponent', () => {
  let component: TestCasePickerComponent;
  let fixture: ComponentFixture<TestCasePickerComponent>;

  const tableMock = jasmine.createSpyObj('tableMock', ['load']);
  tableMock.selectedRows$ = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule, OverlayModule, RouterTestingModule],
      declarations: [TestCasePickerComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCasePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
