// @formatter:off
/*
* Hierarchy of testing dataset is:
*
* TestCaseLibrary-1
*     TestCase-12
*     TestCaseFolder-1
*         TestCaseFolder-3
*             TestCaseFolder-7
*                 TestCase-9
*             TestCase-8
*         TestCase-4
*         TestCase-5
*         TestCaseFolder-6
*     TestCaseFolder-2
* TestCaseLibrary-2
*     TestCase-10
*     TestCaseFolder-11
* */
// @formatter:on
import {DataRow, DataRowOpenState} from '../../model/data-row.model';
import {convertSqtmLiteral, convertSqtmLiterals, SquashTmDataRowType} from '../../../../model/grids/data-row.type';

export function getDataRowDataset(): DataRow[] {
  const tcl1 = {
    id: 'TestCaseLibrary-1',
    type: SquashTmDataRowType.TestCaseLibrary,
    state: DataRowOpenState.closed,
    children: ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2'],
    data: {
      name: 'Project-1',
      projectId: 1,
    },
  };

  const tcl2 = {
    id: 'TestCaseLibrary-2',
    type: SquashTmDataRowType.TestCaseLibrary,
    state: DataRowOpenState.closed,
    children: ['TestCase-10', 'TestCaseFolder-11'],
    data: {}
  };

  const tcf1 = {
    id: 'TestCaseFolder-1',
    type: SquashTmDataRowType.TestCaseFolder,
    state: DataRowOpenState.closed,
    parentRowId: 'TestCaseLibrary-1',
    children: ['TestCaseFolder-3', 'TestCase-4', 'TestCase-5', 'TestCaseFolder-6'],
    data: {}
  };

  const tcf2 = {
    id: 'TestCaseFolder-2',
    type: SquashTmDataRowType.TestCaseFolder,
    state: DataRowOpenState.closed,
    parentRowId: 'TestCaseLibrary-1',
    children: [],
    data: {}
  };

  const tcf3 = {
    id: 'TestCaseFolder-3',
    type: SquashTmDataRowType.TestCaseFolder,
    state: DataRowOpenState.closed,
    parentRowId: 'TestCaseFolder-1',
    children: ['TestCaseFolder-7', 'TestCase-8'],
    data: {}
  };

  const tcf6 = {
    id: 'TestCaseFolder-6',
    type: SquashTmDataRowType.TestCaseFolder,
    state: DataRowOpenState.closed,
    parentRowId: 'TestCaseFolder-1',
    children: [],
    data: {}
  };

  const tcf7 = {
    id: 'TestCaseFolder-7',
    type: SquashTmDataRowType.TestCaseFolder,
    state: DataRowOpenState.closed,
    parentRowId: 'TestCaseFolder-3',
    children: ['TestCase-9'],
    data: {}
  };

  const tcf10 = {
    id: 'TestCaseFolder-11',
    type: SquashTmDataRowType.TestCaseFolder,
    state: DataRowOpenState.closed,
    parentRowId: 'TestCaseLibrary-2',
    children: [],
    data: {}
  };

  const tc12 = {
    id: 'TestCase-12',
    type: SquashTmDataRowType.TestCase,
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseLibrary-1',
    children: [],
    data: {}
  };

  const tc4 = {
    id: 'TestCase-4',
    type: SquashTmDataRowType.TestCase,
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseFolder-1',
    children: [],
    data: {}
  };

  const tc5 = {
    id: 'TestCase-5',
    type: SquashTmDataRowType.TestCase,
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseFolder-1',
    children: [],
    data: {}
  };

  const tc8 = {
    id: 'TestCase-8',
    type: SquashTmDataRowType.TestCase,
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseFolder-3',
    children: [],
    data: {}
  };

  const tc9 = {
    id: 'TestCase-9',
    type: SquashTmDataRowType.TestCase,
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseFolder-7',
    children: [],
    data: {}
  };

  const tc10 = {
    id: 'TestCase-10',
    type: SquashTmDataRowType.TestCase,
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseLibrary-2',
    children: [],
    data: {
      name: 'TestCase 10',
    }
  };


  return convertSqtmLiterals([tcl1, tcf1, tcf2, tcf3, tcf6, tcf7, tc12, tc4, tc5, tcl2, tc8, tc9, tc10, tcf10], []);
}

export function getDataRowDatasetWithContainersOpened(): DataRow[] {
  return getDataRowDataset().map(row => {
    if (row.state === DataRowOpenState.closed) {
      row.state = DataRowOpenState.open;
    }
    return {...row};
  });
}

export function getTestCaseForInsert(): DataRow[] {
  const tc13 = {
    id: 'TestCase-13',
    type: SquashTmDataRowType.TestCase,
    state: DataRowOpenState.leaf,
    children: [],
    data: {}
  };

  const tc14 = {
    id: 'TestCase-14',
    type: SquashTmDataRowType.TestCase,
    state: DataRowOpenState.leaf,
    children: [],
    data: {}
  };

  return convertSqtmLiterals([tc13, tc14], []);
}

export function getSubtreesForInsert(): DataRow[] {
  const tc23 = {
    id: 'TestCase-23',
    type: SquashTmDataRowType.TestCase,
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseFolder-20',
    children: [],
    data: {}
  };

  const tc24 = {
    id: 'TestCase-24',
    type: SquashTmDataRowType.TestCase,
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseFolder-20',
    children: [],
    data: {}
  };

  const tcf20 = {
    id: 'TestCaseFolder-20',
    type: SquashTmDataRowType.TestCaseFolder,
    state: DataRowOpenState.closed,
    children: ['TestCase-23', 'TestCaseFolder-30', 'TestCase-24'],
    data: {}
  };

  const tcf30 = {
    id: 'TestCaseFolder-30',
    type: SquashTmDataRowType.TestCaseFolder,
    parentRowId: 'TestCaseFolder-20',
    state: DataRowOpenState.closed,
    children: ['TestCase-31', 'TestCase-32'],
    data: {}
  };

  const tc31 = {
    id: 'TestCase-31',
    type: SquashTmDataRowType.TestCase,
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseFolder-30',
    children: [],
    data: {}
  };

  const tc32 = {
    id: 'TestCase-32',
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseFolder-30',
    type: SquashTmDataRowType.TestCase,
    children: [],
    data: {}
  };

  const tcf40 = {
    id: 'TestCaseFolder-40',
    type: SquashTmDataRowType.TestCaseFolder,
    state: DataRowOpenState.closed,
    children: ['TestCase-41'],
    data: {}
  };

  const tc41 = {
    id: 'TestCase-41',
    type: SquashTmDataRowType.TestCase,
    state: DataRowOpenState.leaf,
    parentRowId: 'TestCaseFolder-40',
    children: [],
    data: {
      label: 'Test Case 41'
    }
  };

  return convertSqtmLiterals([tc23, tc24, tcf20, tcf30, tc31, tc32, tcf40, tc41], []);
}

export function getFlatGrid(lines: number): DataRow[] {
  return [...Array(lines).keys()]
    .map(index => {
      return {
        id: `Execution-${index}`,
        type: SquashTmDataRowType.Generic,
        data: {}
      };
    })
    .map(literal => convertSqtmLiteral(literal, []));
}


