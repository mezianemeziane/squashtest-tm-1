import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {GridFooterComponent} from './grid-footer.component';
import {GridTestingModule} from '../../grid-testing/grid-testing.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';

describe('GridFooterComponent', () => {
  let component: GridFooterComponent;
  let fixture: ComponentFixture<GridFooterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [GridFooterComponent],
      imports: [GridTestingModule, TestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
