import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {DateCellRendererComponent} from './date-cell-renderer.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ReferentialDataService} from '../../../../../core/referential/services/referential-data.service';
import {DatePipe} from '@angular/common';
import {RestService} from '../../../../../core/services/rest.service';
import {grid} from '../../../../../model/grids/grid-builders';
import {GridDefinition} from '../../../model/grid-definition.model';
import {GridService} from '../../../services/grid.service';
import {gridServiceFactory} from '../../../grid.service.provider';

describe('DateCellRendererComponent', () => {
  let component: DateCellRendererComponent;
  let fixture: ComponentFixture<DateCellRendererComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};
  const translateService = jasmine.createSpyObj('TranslateService', ['instant']);
  const datePipeMock = jasmine.createSpyObj('datePipe', ['transform']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DateCellRendererComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: TranslateService,
          useValue: translateService
        }, {
          provide: DatePipe,
          useValue: datePipeMock
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
