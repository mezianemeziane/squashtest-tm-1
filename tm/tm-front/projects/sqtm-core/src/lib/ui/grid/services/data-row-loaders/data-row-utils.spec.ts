import {DataRow, DataRowOpenState} from '../../model/data-row.model';
import {getAncestorIds, getDescendantIds} from './data-row-utils';
import {convertSqtmLiteral, convertSqtmLiterals} from '../../../../model/grids/data-row.type';
import {getDataRowDataset, getFlatGrid} from './data-row-dataset.spec';
import {DataRowState, initialDataRowState} from '../../model/state/datarow.state';
import {createEntityAdapter} from '@ngrx/entity';
import {Identifier} from '../../../../model/entity.model';

describe('Data row utils', () => {
  /**
   * Tests of getDescendant method
   */
  describe('DataRow Get Descendant', () => {
    it('should return empty array', () => {
      const dataRow = convertSqtmLiteral({
        id: 'TestCaseLibrary-1',
        state: DataRowOpenState.closed,
        children: [],
        data: {}
      }, []);

      let dataRowState = initialDataRowState();
      dataRowState = createEntityAdapter<DataRow>().setAll([dataRow], dataRowState);

      const result = getDescendantIds(dataRowState, [dataRow]);
      expect(result).toEqual([]);
      expect(dataRowState).toBe(dataRowState);
    });

    it('should return first level children', () => {

      const entities = getDataRowState(getEntities()).entities;

      let result = getDescendantIds(getDataRowState(getEntities()), [entities['TestCaseFolder-7']]);
      expect(result).toEqual(['TestCase-9']);
      expect(entities).toBe(entities);

      result = getDescendantIds(getDataRowState(getEntities()), [entities['TestCaseLibrary-2']]);
      expect(result).toEqual(['TestCase-10', 'TestCaseFolder-11']);

    });

    it('should return deep nested children of libraries', () => {

      const entities = getDataRowState(getEntities()).entities;

      let result = getDescendantIds(getDataRowState(getEntities()), [entities['TestCaseLibrary-1']]);
      expect(result.sort()).toEqual(
        ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2', 'TestCaseFolder-3', 'TestCase-4',
          'TestCase-5', 'TestCaseFolder-6', 'TestCaseFolder-7', 'TestCase-8', 'TestCase-9'].sort());
      expect(entities).toBe(entities);

      result = getDescendantIds(getDataRowState(getEntities()), [entities['TestCaseLibrary-2']]);
      expect(result.sort()).toEqual(
        ['TestCase-10', 'TestCaseFolder-11'].sort());
      expect(entities).toBe(entities);

    });

    it('should return children of nested folders', () => {

      const entities = getDataRowState(getEntities()).entities;

      let result = getDescendantIds(getDataRowState(getEntities()), [entities['TestCaseFolder-7']]);
      expect(result.sort()).toEqual(
        ['TestCase-9'].sort());

      result = getDescendantIds(getDataRowState(getEntities()), [entities['TestCaseFolder-3']]);
      expect(result.sort()).toEqual(
        ['TestCaseFolder-7', 'TestCase-8', 'TestCase-9'].sort());

      result = getDescendantIds(getDataRowState(getEntities()), [entities['TestCase-9']]);
      expect(result.sort()).toEqual(
        [].sort());
    });

    it('should return children of multiple nested folders', () => {

      const entities = getDataRowState(getEntities()).entities;

      let result = getDescendantIds(getDataRowState(getEntities()), [entities['TestCaseFolder-7'], entities['TestCaseFolder-3']]);
      expect(result.sort()).toEqual(
        ['TestCaseFolder-7', 'TestCase-8', 'TestCase-9'].sort());

      result = getDescendantIds(getDataRowState(getEntities()), [entities['TestCaseLibrary-1'], entities['TestCaseLibrary-2']]);
      expect(result.sort()).toEqual(
        ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2', 'TestCaseFolder-3',
          'TestCase-4', 'TestCase-5', 'TestCaseFolder-6', 'TestCaseFolder-7',
          'TestCase-8', 'TestCase-9', 'TestCase-10', 'TestCaseFolder-11'].sort());
    });


    it('should gracefully return empty array for root nodes', (done) => {
      const entities = getDataRowState(getFlatGrid(100)).entities;

      let result = getDescendantIds(getDataRowState(getFlatGrid(100)), [entities['Execution-25']]);
      expect(result).toEqual([]);

      result = getDescendantIds(getDataRowState(getFlatGrid(100)), [entities['Execution-0']]);
      expect(result).toEqual([]);

      result = getDescendantIds(getDataRowState(getFlatGrid(100)), [entities['Execution-99']]);
      expect(result).toEqual([]);
      done();
    });

  });

  describe('TreeNode Get Ancestors', () => {
    interface DataType {
      dataRowState: DataRowState;
      descendantIds: Identifier[];
      expectedAncestorIds: Identifier[];
    }

    const dataSets: DataType[] = [
      {
        dataRowState: getDataRowState(getDataRowDataset()),
        descendantIds: [],
        expectedAncestorIds: []
      },
      {
        dataRowState: getDataRowState(getDataRowDataset()),
        descendantIds: ['TestCaseLibrary-1', 'TestCaseLibrary-2'],
        expectedAncestorIds: []
      },
      {
        dataRowState: getDataRowState(getDataRowDataset()),
        descendantIds: ['TestCase-12'],
        expectedAncestorIds: ['TestCaseLibrary-1']
      },
      {
        dataRowState: getDataRowState(getDataRowDataset()),
        descendantIds: ['TestCase-12', 'TestCaseFolder-1'],
        expectedAncestorIds: ['TestCaseLibrary-1']
      },
      {
        dataRowState: getDataRowState(getDataRowDataset()),
        descendantIds: ['TestCase-12', 'TestCaseFolder-1', 'TestCase-10', 'TestCaseFolder-1'],
        expectedAncestorIds: ['TestCaseLibrary-1', 'TestCaseLibrary-2']
      },
      {
        dataRowState: getDataRowState(getDataRowDataset()),
        descendantIds: ['TestCase-9'],
        expectedAncestorIds: ['TestCaseFolder-7', 'TestCaseFolder-3', 'TestCaseFolder-1', 'TestCaseLibrary-1']
      },
      {
        dataRowState: getDataRowState(getDataRowDataset()),
        descendantIds: ['TestCase-9', 'TestCase-10', 'TestCaseFolder-11'],
        expectedAncestorIds: ['TestCaseFolder-7', 'TestCaseLibrary-2', 'TestCaseFolder-3', 'TestCaseFolder-1', 'TestCaseLibrary-1']
      },
      {
        dataRowState: getDataRowState(getDataRowDataset()),
        descendantIds: ['TestCase-9', 'TestCaseFolder-3'],
        expectedAncestorIds: ['TestCaseFolder-7', 'TestCaseFolder-1', 'TestCaseFolder-3', 'TestCaseLibrary-1']
      },
      {
        dataRowState: getDataRowState(getFlatGrid(100)),
        descendantIds: ['Execution-99', 'Execution-98', 'Execution-50'],
        expectedAncestorIds: []
      },
    ];

    dataSets.forEach((dataSet, index) => runTest(dataSet, index));

    function runTest(data: DataType, index: number) {
      it(`DataSet ${index} - It should find ${data.expectedAncestorIds.length} ancestors of ${JSON.stringify(data.descendantIds)}`,
        (done) => {

          const entities = data.dataRowState.entities;
          const descendants = data.descendantIds.map(id => entities[id]);

          const result = getAncestorIds(data.dataRowState, descendants);
          expect(result).toEqual(
            data.expectedAncestorIds);
          done();
        });
    }
  });

  function getEntities(): DataRow[] {
    const tcl1 = {
      id: 'TestCaseLibrary-1',
      state: DataRowOpenState.closed,
      children: ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2'],
      data: {
        name: 'Project-1',
        projectId: 1,
      }
    };

    const tcl2 = {
      id: 'TestCaseLibrary-2',
      state: DataRowOpenState.closed,
      children: ['TestCase-10', 'TestCaseFolder-11'],
      data: {}
    };

    const tcf1 = {
      id: 'TestCaseFolder-1',
      state: DataRowOpenState.open,
      parentRowId: 'TestCaseLibrary-1',
      children: ['TestCaseFolder-3', 'TestCase-4', 'TestCase-5', 'TestCaseFolder-6'],
      data: {}
    };

    const tcf2 = {
      id: 'TestCaseFolder-2',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseLibrary-1',
      children: [],
      data: {}
    };

    const tcf3 = {
      id: 'TestCaseFolder-3',
      state: DataRowOpenState.open,
      parentRowId: 'TestCaseFolder-1',
      children: ['TestCaseFolder-7', 'TestCase-8'],
      data: {}
    };

    const tcf6 = {
      id: 'TestCaseFolder-6',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {}
    };

    const tcf7 = {
      id: 'TestCaseFolder-7',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseFolder-3',
      children: ['TestCase-9'],
      data: {}
    };

    const tcf10 = {
      id: 'TestCaseFolder-11',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseLibrary-2',
      children: [],
      data: {}
    };

    const tc12 = {
      id: 'TestCase-12',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseLibrary-1',
      children: [],
      data: {}
    };

    const tc4 = {
      id: 'TestCase-4',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {}
    };

    const tc5 = {
      id: 'TestCase-5',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {}
    };

    const tc8 = {
      id: 'TestCase-8',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-3',
      children: [],
      data: {}
    };

    const tc9 = {
      id: 'TestCase-9',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-7',
      children: [],
      data: {}
    };

    const tc10 = {
      id: 'TestCase-10',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseLibrary-2',
      children: [],
      data: {
        name: 'TestCase 10',
      }
    };


    return convertSqtmLiterals([tcl1, tcf1, tcf2, tcf3, tcf6, tcf7, tc12, tc4, tc5, tcl2, tc8, tc9, tc10, tcf10], []);
  }

  function getDataRowState(dataRows: DataRow[]): DataRowState {
    const dataRowState: DataRowState = initialDataRowState();
    return createEntityAdapter<DataRow>().setAll(dataRows, dataRowState);
  }
});

