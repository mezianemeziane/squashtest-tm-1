import {Identifier} from '../../../../model/entity.model';
import {DragAndDropTarget} from '../../../drag-and-drop/events';

export class UiState {
  dragState: DragAndDropState;

  showColumnManager: boolean;
  showFilterManager: boolean;
  showContextualMenu: boolean;
  // does async operation are running. If > 0, an overlay with a spinner will be added to grid, preventing any further interaction
  // before async operation is finished. However, as some external interaction can be stacked, the counter may be > 1
  globalAsyncOperationCounter: number;
}

export type UiStateReadOnly = Readonly<UiState>;

export function initialUiState(): UiStateReadOnly {
  return {
    dragState: initialDndState(),
    showColumnManager: false,
    showFilterManager: false,
    showContextualMenu: false,
    globalAsyncOperationCounter: 0
  };
}

export class DragAndDropState {
  initialNodeOrder: number[];
  dragging: boolean;
  currentDndTarget: DragAndDropTarget;
  draggedRowIds: Identifier[];
}

export function initialDndState(): Readonly<DragAndDropState> {
  return {
    initialNodeOrder: [],
    dragging: false,
    currentDndTarget: null,
    draggedRowIds: []
  };
}
