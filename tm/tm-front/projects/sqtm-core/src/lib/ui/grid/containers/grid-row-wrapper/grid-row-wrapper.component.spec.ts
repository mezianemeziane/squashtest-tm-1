import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {GridRowWrapperComponent} from './grid-row-wrapper.component';
import {CORE_MODULE_CONFIGURATION} from '../../../../core/sqtm-core.tokens';
import {defaultSqtmConfiguration} from '../../../../core/sqtm-core.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {GridNode} from '../../model/grid-node.model';
import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';
import {WorkspaceCommonModule} from '../../../workspace-common/workspace-common.module';
import {RestService} from '../../../../core/services/rest.service';
import {grid} from '../../../../model/grids/grid-builders';
import {GridDefinition} from '../../model/grid-definition.model';
import {GridService} from '../../services/grid.service';
import {gridServiceFactory} from '../../grid.service.provider';
import {TextCellRendererComponent} from '../../components/cell-renderers/text-cell-renderer/text-cell-renderer.component';
import {DataRow} from '../../model/data-row.model';

describe('GridRowWrapperComponent', () => {
  let component: GridRowWrapperComponent;
  let fixture: ComponentFixture<GridRowWrapperComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, WorkspaceCommonModule],
      declarations: [GridRowWrapperComponent],
      providers: [
        {provide: CORE_MODULE_CONFIGURATION, useValue: defaultSqtmConfiguration},
        RestService,
        {
          provide: GridDefinition,
          useValue: grid('grid-test').build()
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridRowWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.ngOnInit();
    fixture.detectChanges();
    component.ngAfterViewInit();
    fixture.detectChanges();
    const dataRow = {component: TextCellRendererComponent} as unknown as DataRow;
    component.gridNode = {id: 12, dataRow} as GridNode;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
