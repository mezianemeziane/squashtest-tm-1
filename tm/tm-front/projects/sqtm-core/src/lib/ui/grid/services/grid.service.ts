import {Observable, Subject} from 'rxjs';
import {DataRow} from '../model/data-row.model';
import {GridDisplay} from '../model/grid-display.model';
import {PaginationDisplay} from '../model/pagination-display.model';
import {ColumnDisplay, ColumnWithFilter} from '../model/column-display.model';
import {Identifier} from '../../../model/entity.model';
import {MoveColumnAction} from '../model/actions/move-column.action';
import {ChangeFilteringAction} from '../../filters/state/change-filtering.action';
import {Dictionary} from '@ngrx/entity';
import {GridFilter} from '../model/state/filter.state';
import {TableValueChange} from '../model/actions/table-value-change';
import {FilterConfiguration} from '../model/state/configuration.state';
import {OpenContextualMenuAction} from '../model/actions/open-contextual-menu.action';
import {GridNode} from '../model/grid-node.model';
import {GridType} from '../model/grid-definition.model';
import {ColumnDefinition, SortedColumn} from '../model/column-definition.model';
import {UiState} from '../model/state/ui.state';
import {GridState} from '../model/state/grid.state';
import {FilterGroup, Scope} from '../../filters/state/filter.state';
import {DragAndDropTarget} from '../../drag-and-drop/events';
import {AbstractServerOperationHandler} from './row-server-operation-handlers/abstract-server-operation-handler';
import {GridPartialConfiguration} from '../model/grid-partial-configuration.model';

/**
 * Facade for the grid.
 * The essentials jobs of this class are :
 *  - Propose a clean facade to expose modifications methods.
 *  - Expose it's internal state across a brand of output Observables.
 *
 *  Note about the DI System : Each instance of the GridComponent must be provided it's own instance of grid service. The easier method to
 *  perform this injection is to provide the service somewhere in component hierarchy above the <sqtm-core-grid>. Two provider site make
 *  sense :
 *
 *   - Directly in a wrapper component if the grid doesn't need to interact with other component.
 *   - In the main component of the page (aka the component given in routes of the module).
 */
export abstract class GridService {

  // Output Observables

  /**
   * This observable emits the full sets of internal DataRows. It represent all the rows known by the grid.
   * If the grid is paginated server side, the rows are limited the current page.
   * If the grid is paginated client side, all the rows are emitted. The rows emitted in client pagination are not sorted or filtered.
   */
  dataRows$: Observable<Dictionary<DataRow>>;

  /**
   * This observable emits the nodes that will be rendered.
   * If the grid is paginated, the rows are limited the current page.
   * If the grid is not paginated (AKA hierarchical data), this observable emit the whole tree each time a flatTree is required.
   */
  gridNodes$: Observable<GridNode[]>;

  /**
   * This observable emits the configuration for displaying the grid.
   */
  gridDisplay$: Observable<GridDisplay>;

  /**
   * This observable emits the information for pagination.
   */
  paginationDisplay$: Observable<PaginationDisplay>;

  /**
   * This observable emit true when a row dragging is running in the grid.
   */
  dragging$: Observable<boolean>;

  /**
   * This observable emits the ui information (opening/closing auxiliary components like column manager, configuration manager)
   */
  uiState$: Observable<UiState>;

  /**
   * This observable emits all filters information (witch columns are filtered, with each value...)
   */
  filters$: Observable<GridFilter[]>;

  /**
   * This observable emits all sorts information (witch columns are sorted...)
   */
  sortedColumns$: Observable<SortedColumn[]>;

  /**
   * This observable emits the active filter information (witch columns is filtered, with each value...)
   */
  activeFilters$: Observable<GridFilter[]>;

  /**
   * This observable emits the actual scope of the Grid. The scope is mainly used in research screens.
   */
  scope$: Observable<Scope>;

  /**
   * Does the current state allow dnd (aka no filter and no sorts...)
   */
  enableDnd$: Observable<boolean>;

  /**
   * This observable emits the selected rows ids only. It will not fire if a selected row is modified, but only if user select/deselect rows
   */
  selectedRowIds$: Observable<Identifier[]>;

  /**
   * This observable emits the opened rows ids only.
   */
  openedRowIds$: Observable<Identifier[]>;

  /**
   * This observable emits the selected rows.
   */
  selectedRows$: Observable<DataRow[]>;

  /**
   * This observable emits an aggregate of columns and the filters. Useful to feed some auxiliary components.
   */
  columns$: Observable<ColumnWithFilter[]>;

  /**
   * This observable emits the available filter configurations.
   */
  filterConfigurations$: Observable<FilterConfiguration[]>;

  /**
   * This observable emits the full grid state.
   * Prefer other more specialized observables when possible to avoid data complexity and frequent ui renderings
   */
  gridState$: Observable<GridState>;

  /**
   * This observable emit each time the contextual menu is opened.
   */
  openContextualMenu$: Observable<OpenContextualMenuAction>;

  /**
   * This observable emit each time the contextual menu is closed.
   */
  closeContextualMenu$: Observable<void>;


  openDisplayConfigurationDialog$: Observable<void>;


  closeDisplayConfigurationDialog$: Observable<void>;

  /**
   * This observable emits each time the user wants to remove a row from the grid
   */
  onDeleteDataRow$: Subject<DataRow>;

  /**
   * This observable emits each time the user mouse pointer enter a row.
   * Take care of doing any subscription outside of angular zone to prevent any DetectChange to kick in,
   * as we have a lot of event fired here.
   */
  enterRow$: Observable<Identifier>;

  /**
   * This observable emits each time the user mouse pointer leave a row.
   * Take care of doing any subscription outside of angular zone to prevent any DetectChange to kick in,
   * as we have a lot of event fired here.
   */
  leaveRow$: Observable<Identifier>;

  /**
   * This observable emit each time user selection is modified. Emit true if copy is legal, false else
   */
  abstract canCopy$: Observable<boolean>;

  /**
   * This observable emit each time user selection is modified and when copying. Emit true if paste operation is legal, false else
   */
  abstract canPaste$: Observable<boolean>;

  /**
   * This observable emit each time user selection is modified. Emit true if delete is legal, false else
   */
  abstract canDelete$: Observable<boolean>;

  /**
   * This observable emit each time user selection is modified. Emit true if create operation is legal, false else
   */
  abstract canCreate$: Observable<boolean>;

  /**
   * This observable emits each time use selection is modified. Emits true if export operation is legal, false otherwise.
   */
  abstract canExport$: Observable<boolean>;

  /**
   * This observable emit each time user selection is modified. Emit true if at least one row is selected
   */
  abstract hasSelectedRows$: Observable<boolean>;

  /**
   * This observable emits true there's at least one sorted column or one non empty active filter.
   */
  abstract isSortedOrFiltered$: Observable<boolean>;

  /**
   * This observable emit true when grid is performing an async operation. Either because an internal event occurred (ex pagination...),
   * or an external source declared the grid as target of an async operation (ex: when dropping requirements inside test case page)
   */
  abstract globalAsyncOperationRunning$: Observable<boolean>;

  abstract serverOperationHandler: AbstractServerOperationHandler;

  /**
   * This observable emit true when grid data is loaded at least one time.
   */
  abstract loaded$: Observable<boolean>;

  /**
   * Load the grid with the given DataRows. All the rows previously in grid are removed, selection is reset.
   * @param payload The DataRows to load
   * @param count The total number of rows. If the grid is feed server side, this number represent the whole set of rows, not only the
   * initial loaded rows. If the grid is loaded client side, this number should be payload.length.
   * @param initialSelectedRows Ids of rows that must be selected after loading.
   */
  abstract loadInitialDataRows(payload: Partial<DataRow>[], count: number, initialSelectedRows?: Identifier[]);

  /**
   * Load the grid with the given array. All the rows previously in grid are removed, selection is reset.
   * @param payload The rows to load.
   * @param count The total number of rows. If the grid is feed server side, this number represent the whole set of rows, not only the
   * initial loaded rows. If the grid is loaded client side, this number should be payload.length.
   * @param idAttribute Attribute of the rows that will be used as id. If not provided, the grid will look for an 'id' attribute. Note that
   * if a row has nullish id, the grid will throw Error and crash.
   * @param initialSelectedRows Ids of rows that must be selected after loading.
   */
  abstract loadInitialData<T>(payload: T[], count: number, idAttribute?: keyof T, initialSelectedRows?: Identifier[]);

  abstract connectToDatasource<T>(datasource: Observable<T[]>, idAttribute?: keyof T);

  abstract connectToDataRowSource<T>(datasource: Observable<DataRow[]>);

  abstract beginAsyncOperation();

  abstract completeAsyncOperation();

  abstract releaseDatasource();

  /**
   * Go to the last pagination page.
   */
  abstract goToLastPage();

  /**
   * Go to the first pagination page.
   */
  abstract goToFirstPage();

  abstract paginateNextPage();

  abstract paginatePreviousPage();

  abstract changePaginationSize(pageSize: number);

  /**
   * Add rows in grid
   * @param payload the raw data. (aka just the data of DataRows)
   * @param parentId optional parent. If not provided rows will be added as root rows
   * @param position optional position. If not provided rows will be added at last position in parent or root
   * @param idAttribute optional idAttribute to use if your data have not a proper 'id'
   */
  abstract addRows<T>(payload: T[], parentId?: Identifier, position?: number, idAttribute?: keyof T);

  abstract addAndSelectRow(row: DataRow, parentId: Identifier, position?: number);

  abstract openRow(id: string | number): void;

  abstract openRows(ids: Identifier[]): void;

  /**
   * Open all closed rows. Will only have implementation for client side grid for now.
   * Doing it with server backed trees could lead to massive performance impact event with a highly optimized fetch node process.
   */
  abstract openAllRows(): void;

  /**
   * Simply close all opened rows.
   */
  abstract closeAllRows(): void;

  abstract closeRow(id: string | number);

  abstract addToSelection(id: string | number);

  /**
   * Toggle the given row, according to Squash TM rules relative to the hierarchy. Aka you cant select the child of an already selected
   * nodes... Do not replace the previously selected rows, and add new toggled row to the selection.
   * @param id The id of the toggled row.
   */
  abstract toggleRowSelection(id: string | number);

  /**
   * Force the selection to one given row. It replace all previously selected rows.
   * @param id The id of the selected row.
   */
  abstract selectSingleRow(id: Identifier);

  /**
   * Toggle the column sort. If it was not Sorted before, it add it to the sort in last position. If it was sort ASC it change the sort to
   * DESC. If it was sort DESC it is removed from the sorts.
   * @param id The id of the column.
   */
  abstract toggleColumnSort(id: string | number);

  /**
   * Set all the sorts to the given sorts.
   * @param sortedColumns the new sorted columns
   */
  abstract setColumnSorts(sortedColumns: SortedColumn[]);

  /**
   * Notify the grid that the user is initiating a drag and drop action. At this stage we cannot determine if it's an internal
   * or external dnd operation.
   * @param draggedRowIds ids of dragged rows
   */
  abstract startDrag(draggedRowIds: Identifier[]): void;

  /**
   * notify grid model that the user is dragging over nodes
   * @param dragTarget The target as id + zone
   */
  abstract notifyDragOver(dragTarget: DragAndDropTarget): void;

  /**
   * notify grid model that user finished an internal dnd operation in the grid. DnD from other container should not use this method that
   * is reserved to internal dnd operations.
   */
  abstract notifyInternalDrop(): void;

  /**
   * Notify the grid that the user canceled an dnd, by releasing the mouse with an invalid target for drop.
   */
  abstract cancelDrag();

  /**
   * Return an observable emitting the drop status according to the hovered row, for a drop at the same hierarchy level.
   * @param id The hovered row.
   */
  abstract allowDropSibling(id: Identifier): Observable<boolean>;

  /**
   * Return an observable emitting the drop status according to the hovered row, for a drop into the target.
   * @param id The hovered row.
   */
  abstract allowDropInto(id: Identifier): Observable<boolean>;

  /**
   * Modify the filtered value for a filter.
   * @param changeFilter Value + id of column
   */
  abstract changeFilterValue(changeFilter: ChangeFilteringAction): void;


  abstract replaceFilters(filters: GridFilter[], scope: Scope, filterGroups?: FilterGroup[]): void;

  /**
   * Toggle the visibility of a column.
   * @param id of the column.
   */
  abstract toggleColumnVisibility(id: Identifier): void;

  /**
   * Hide/Show a column.
   * @param id of the column.
   * @param show visibility of the column
   */
  abstract setColumnVisibility(id: Identifier, show: boolean): void;

  /**
   * Move a column. Can be used to change position in the same viewport, or change viewport.
   * @param moveColumnAction id + index + viewport
   */
  abstract moveColumn(moveColumnAction: MoveColumnAction): void;

  /**
   * Change a value in rows. DO NOT recompute the node tree, so the filters and sorts are not applied to the new value. I prevent the rows
   * to jump around during modification witch would be a bad user experience.
   * @param ids The ids of modified rows
   * @param changes columnId + new value
   */
  abstract editRows(ids: Identifier[], changes: TableValueChange[]);

  /**
   * Return an observable that will track the state of a row.
   * @param id of the row.
   */
  abstract connectToDataRow(id): Observable<DataRow>;

  /**
   * Notify that the grid that column manager should be opened/closed
   */
  abstract toggleColumnManager();

  /**
   * Notify that the grid that filter manager should be opened/closed
   */
  abstract toggleFilterManager();

  /**
   * Change the width of a column.
   * @param id of the column
   * @param offset to add or remove to the width.
   */
  abstract offsetColumnWidth(id: Identifier, offset: number);

  /**
   * Notify the grid that contextual menu should be opened.
   * @param action coordinate of the user click...
   */
  abstract openContextualMenu(action: OpenContextualMenuAction);

  /**
   * Notify the grid that contextual menu should be closed.
   */
  abstract closeContextualMenu();


  /**
   * Notify that the grid that column manager should be opened/closed
   */
  abstract openDisplayConfigurationManager();

  abstract addColumnAtIndex(columnDefinition: ColumnDefinition[], index?: number);

  abstract toggleGridType(gridType: GridType);

  /**
   * Remove rows from the grid. DO NOT PERFORM ANY KIND OF SERVER REQUEST
   * @param ids of the rows to remove.
   */
  abstract removeRows(ids: Identifier[]);

  /**
   * DELETE selected rows SERVER SIDE using the ServerOperationHandler given to the grid and after that update the
   * grid. If you need only updating grid after external deletion use removeRows method. You can also use a dedicated cell renderer
   * if you only want a delete button on each row.
   */
  abstract deleteRows();

  /**
   * Define the id of the grid. Note that nothing will be rendered if a null id was defined previously, before you call this method.
   * This method WILL THROW if id is already defined. The id of e grid must be declared statically or initialized one time...
   * @param id the new id
   */
  abstract defineId(id: string);

  abstract setServerUrl(serverUrl: string[]);

  abstract selectAllRows();

  abstract unselectAllRows();

  abstract invertSelection();

  abstract refreshData();

  abstract refreshDataAndKeepSelectedRows();

  abstract refreshSubTree(ids: Identifier[]);

  abstract activateFilter(id: string);

  abstract inactivateFilter(id: string);

  /**
   * Reset all filters to their initial state
   */
  abstract resetFilters();

  abstract changeScope(scope: Scope);

  /**
   * Update a cell value server side. It make a post request to change data server side.
   */
  abstract updateCellValue(row: DataRow, column: ColumnDisplay, value: any): Observable<any>;

  abstract copy(): void;

  abstract paste(): void;

  abstract applyMultiColumnsFilter(value: any);

  abstract addFilters(gridFilter: GridFilter[]);

  abstract deleteColumns(columnIds: Identifier[]);

  abstract suspendDrag();

  abstract renameColumn(columnId: Identifier, newLabel: string);

  abstract complete();

  abstract notifyEnterRow(id: Identifier);

  abstract notifyLeaveRow(id: Identifier);

  abstract lockGrid();

  abstract unlockGrid();

  abstract setMultiSelectionEnabled(shouldBeEnabled: boolean): void;

  abstract addInitialConfiguration(conf: GridPartialConfiguration): Observable<GridState>;

  abstract setEnableDrag(canDrag: boolean);
}

