import {DataRow} from '../../model/data-row.model';
import {Identifier} from '../../../../model/entity.model';
import {EntityState} from '@ngrx/entity';
import {DataRowState} from '../../model/state/datarow.state';
import {GridState} from '../../model/state/grid.state';

export function getDescendantIds(dataRowState: EntityState<DataRow>, ancestors: DataRow[]): Identifier[] {
  const descendantIds = new Set<string | number>([]);
  let childrenIds;
  let nextLayerIds = [];
  ancestors.forEach(ancestor => nextLayerIds.push(...ancestor.children));
  do {
    childrenIds = [...nextLayerIds];
    nextLayerIds = [];
    for (const childrenId of childrenIds) {
      descendantIds.add(childrenId);
      const node = dataRowState.entities[childrenId];
      nextLayerIds.push(...node.children);
    }
  } while (nextLayerIds.length > 0);

  return [...descendantIds];
}

export function getDescendants(dataRowState: EntityState<DataRow>, ancestors: DataRow[]): DataRow[] {
  const descendantIds = getDescendantIds(dataRowState, ancestors);
  return descendantIds.map(id => dataRowState.entities[id]);
}

export function getAncestorIds(dataRowState: EntityState<DataRow>, descendants: DataRow[]): Identifier[] {
  const ancestorIds = new Set<string | number>([]);
  let nextLayerIds = descendants
    .filter(descendant => descendant.parentRowId != null)
    .map(descendant => descendant.parentRowId);

  do {
    nextLayerIds.forEach(parentId => ancestorIds.add(parentId));
    nextLayerIds = nextLayerIds
      .filter(parentId => dataRowState.entities.hasOwnProperty(parentId))
      .map(parentId => dataRowState.entities[parentId])
      .filter(parent => parent.parentRowId != null)
      .map(parent => parent.parentRowId);
  } while (nextLayerIds.length > 0);

  return [...ancestorIds];
}

export function extractRootRows(rows: DataRow[]): DataRow[] {
  return rows.filter(row => row.parentRowId == null);
}

export function extractRootIds(rows: DataRow[]): Identifier[] {
  return extractRootRows(rows).map(row => row.id);
}

export function getSelectedRows(dataRowState: DataRowState): DataRow[] {
  return dataRowState.selectedRows.map(id => dataRowState.entities[id]);
}

export function getDraggedRows(state: GridState): DataRow[] {
  return state.uiState.dragState.draggedRowIds.map(id => state.dataRowState.entities[id]);
}

export function incrementAsyncOperationCounter(state: GridState) {
  return {
    ...state,
    uiState: {...state.uiState, globalAsyncOperationCounter: state.uiState.globalAsyncOperationCounter + 1}
  };
}


export function decrementAsyncOperationCounter(state: GridState) {
  const globalAsyncOperationCounter = Math.max(0, state.uiState.globalAsyncOperationCounter - 1);
  return {
    ...state,
    uiState: {...state.uiState, globalAsyncOperationCounter}
  };
}

export function hasRowSelected(state: Readonly<GridState>): boolean {
  return state.dataRowState.selectedRows.length > 0;
}

export function getLastSelectedRow(state: Readonly<GridState>): DataRow {
  const selectedRows = state.dataRowState.selectedRows;
  const lastRowId = state.dataRowState.lastToggledRowId || selectedRows[selectedRows.length - 1];
  return state.dataRowState.entities[lastRowId];
}

export function areRowsSibling(rowA: DataRow, rowB: DataRow): boolean {
  return rowA.parentRowId === rowB.parentRowId;
}

// Because some nodes can be filtered and sorted, we must retrieve the sibling order from state.nodeState and not from
// state.dataRowState
export function getAllSiblingIds(id: Identifier, state: Readonly<GridState>): Identifier[] {
  const dataRow = state.dataRowState.entities[id];
  let siblingIds;
  if (dataRow.parentRowId == null) {
    siblingIds = state.dataRowState.rootRowIds;
  } else {
    const parent = getDataRow(dataRow.parentRowId, state);
    siblingIds = parent.children;
  }
  // from nodeState witch is correctly ordered and filtered
  return (state.nodesState.ids as any[]).filter(nodeId => siblingIds.includes(nodeId));
}

// export function getAllSiblingIdsWhenSorting(id: Identifier, state: Readonly<GridState>): Identifier[] {
//   const dataRow = state.dataRowState.entities[id];
//   if (dataRow.parentRowId == null) {
//     return [...state.dataRowState.rootRowIds];
//   } else {
//     const parent = getDataRow(dataRow.parentRowId, state);
//     return [...parent.children];
//   }
// }

export function getDataRow(id: Identifier, state: Readonly<GridState>): DataRow {
  return state.dataRowState.entities[id];
}
