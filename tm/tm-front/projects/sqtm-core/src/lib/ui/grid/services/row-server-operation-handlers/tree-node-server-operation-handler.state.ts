import {DataRow} from '../../model/data-row.model';

export class TreeNodeServerOperationHandlerState {
  copiedNodes: DataRow[];
  deleteOperationIsRunning: boolean;
}

export function initialTreeNodeOperationHandlerState(): Readonly<TreeNodeServerOperationHandlerState> {
  return {
    copiedNodes: [],
    deleteOperationIsRunning: false
  };
}
