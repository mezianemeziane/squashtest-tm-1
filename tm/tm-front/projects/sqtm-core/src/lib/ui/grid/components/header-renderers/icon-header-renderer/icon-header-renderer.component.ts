import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {ColumnDisplay, ColumnWithFilter} from '../../../model/column-display.model';
import {AbstractHeaderRendererComponent} from '../abstract-header-renderer/abstract-hearder-renderer.component';
import {GridService} from '../../../services/grid.service';
import {GridViewportName} from '../../../model/state/column.state';
import {GridDisplay} from '../../../model/grid-display.model';

@Component({
  selector: 'sqtm-core-icon-header-renderer',
  template: `
    <ng-container *ngIf="gridDisplay">
      <div class="full-width flex-row" [style.height]="calculateRowHeight(gridDisplay)">
        <i nz-icon style="margin: auto" [nzType]="columnDisplay?.iconName" [nzTheme]="columnDisplay?.iconTheme"></i>
        <div class="full-height m-r-5 flex-fixed-size resize-handler">
        </div>
      </div>

    </ng-container>`,
  styleUrls: ['./icon-header-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconHeaderRendererComponent extends AbstractHeaderRendererComponent implements OnInit {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  ngOnInit() {
  }

  calculateRowHeight(gridDisplay: GridDisplay): string {
    return `${gridDisplay.rowHeight}px`;
  }

}
