import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractCellRendererComponent} from '../abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../../services/grid.service';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'sqtm-core-date-cell-renderer',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <span
          style="margin: auto 0;"
          class="txt-ellipsis"
          nz-tooltip [sqtmCoreLabelTooltip]="displayString">{{displayString}}</span>
      </div>
    </ng-container>`,
  styleUrls: ['./date-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class DateCellRendererComponent extends AbstractCellRendererComponent implements OnInit {

  protected format = 'shortDate';

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef,
              public translateService: TranslateService, public datePipe: DatePipe) {
    super(grid, cdRef);
  }

  ngOnInit(): void {
  }

  get displayString(): string {
    const data = this.row.data[this.columnDisplay.id];
    if (data == null) {
      return '-';
    } else {
      return this.formatDate(data);
    }
  }

  private formatDate(date: Date): string {
    return this.datePipe.transform(date, this.format, '', this.translateService.getBrowserLang());
  }
}

@Component({
  selector: 'sqtm-core-date-time-cell-renderer',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <span
          style="margin: auto 5px;"
          class="txt-ellipsis"
          nz-tooltip [sqtmCoreLabelTooltip]="displayString">{{displayString}}</span>
      </div>
    </ng-container>`,
  styleUrls: ['./date-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateTimeCellRendererComponent extends DateCellRendererComponent {
  protected format = 'short';
}

export function sortDate(dateA: Date, dateB: Date): number {

  const dA = new Date(dateA);
  const dB = new Date(dateB);
  let result = 0;

  const same = dA.getTime() === dB.getTime();

  if (same) {
    result = 0;
  }

  if (dA > dB) {
    result = 1;
  }

  if (dA < dB) {
    result = -1;
  }

  return result;
}
