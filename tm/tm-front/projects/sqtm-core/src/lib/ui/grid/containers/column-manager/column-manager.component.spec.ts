import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ColumnManagerComponent} from './column-manager.component';
import {RestService} from '../../../../core/services/rest.service';
import {GridService} from '../../services/grid.service';
import {gridServiceFactory} from '../../grid.service.provider';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {GridDefinition} from '../../model/grid-definition.model';
import {getBasicGridDisplay} from '../../grid-testing/grid-testing-utils';
import {of} from 'rxjs';
import {grid} from '../../../../model/grids/grid-builders';
import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';

describe('ColumnManagerComponent', () => {
  let component: ColumnManagerComponent;
  let fixture: ComponentFixture<ColumnManagerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, DragDropModule, TestingUtilsModule],
      declarations: [ColumnManagerComponent],
      providers: [
        RestService,
        {
          provide: GridDefinition,
          useValue: grid('grid-test').build()
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnManagerComponent);
    component = fixture.componentInstance;
    component.gridDisplay$ = of(getBasicGridDisplay());
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
