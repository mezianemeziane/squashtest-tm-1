import {
  ChangeDetectorRef,
  Directive,
  DoCheck,
  EmbeddedViewRef,
  Input,
  IterableChangeRecord,
  IterableDiffer,
  IterableDiffers, OnDestroy,
  OnInit,
  SkipSelf,
  TemplateRef,
  TrackByFunction,
  ViewContainerRef
} from '@angular/core';
import {ListRange} from '@angular/cdk/collections';
import {GridViewportComponent} from '../containers/grid-viewport/grid-viewport.component';
import {GridNode} from '../model/grid-node.model';

/**
 * Modified version of Angular Material CDK VirtualScrollDirective.
 * The main modification is that SquashTM grid needs 3 coordinated viewport witch doesn't seems possible with
 * cdk as the component throw error if you try to attach several virtualForOf to the same VirtualScrollComponent. Using
 * 3 components leads to different rendering speed across the 3 viewports and some other bugs.
 */
@Directive({
  selector: '[sqtmCoreVirtualScrollFor][sqtmCoreVirtualScrollForOf]'
})
export class VirtualScrollDirective implements OnInit, DoCheck, OnDestroy {

  private rangeToRender: ListRange;
  private dirty = false;
  private items: GridNode[] = [];
  private itemsToRender: GridNode[] = [];
  private differ: IterableDiffer<GridNode>;
  private _trackByFn: TrackByFunction<GridNode>;
  private collectionHasChanged = false;
  private templateCacheSize = 30;
  private _templateCache: EmbeddedViewRef<any>[] = [];

  @Input()
  set sqtmCoreVirtualScrollForOf(collection: GridNode[]) {
    this.items = collection;
    this.dirty = true;
    this.collectionHasChanged = true;
  }

  @Input()
  set sqtmCoreVirtualScrollForRangeToRender(rangeToRender: ListRange) {
    // console.log(`Set range to render ${JSON.stringify(rangeToRender)}`);
    this.rangeToRender = rangeToRender;
    this.dirty = true;
  }

  @Input()
  set sqtmCoreVirtualScrollForTrackBy(trackByFn: TrackByFunction<GridNode>) {
    this._trackByFn = trackByFn;
    this.differ = this.differs.find(this.itemsToRender).create(this._trackByFn);
  }


  constructor(private viewContainerRef: ViewContainerRef, private template: TemplateRef<any>, private differs: IterableDiffers,
              public cdRef: ChangeDetectorRef, @SkipSelf() private gridViewport: GridViewportComponent) {
    // console.log(`init virtual scroll`);
    // console.log(Boolean(this.cdRef));
  }

  ngOnInit(): void {
    this.gridViewport.attachVirtualScroll(this);
  }

  ngDoCheck() {
    // console.log(`Doing checks in virtual scroll ${new Date()}`);
    if (this.dirty && this.rangeToRender && this.differ) {
      this.dirty = false;
      // console.log(`this thing is dirty we should render`);
      // console.log(this.itemsToRender.map(item => item['id']));
      this.itemsToRender = this.items.slice(this.rangeToRender.start, this.rangeToRender.end);
      // console.log(this.itemsToRender.map(item => item['id']));
      const changes = this.differ.diff(this.itemsToRender);
      if (changes) {
        changes.forEachOperation((record, adjustedPreviousIndex, currentIndex) => {
          // console.log(`Operation for row ${JSON.stringify(record.item['id'])}. adjustedPreviousIndex :${adjustedPreviousIndex}.
          // Current index:${currentIndex}`)
          if (record.previousIndex == null) {
            const viewRef = this.insertView(currentIndex);
            viewRef.context.$implicit = record.item;
            viewRef.markForCheck();
          } else if (currentIndex == null) {
            this.removeView(adjustedPreviousIndex);
          } else {
            const view: EmbeddedViewRef<any> = this.viewContainerRef.get(adjustedPreviousIndex) as EmbeddedViewRef<any>;
            this.viewContainerRef.move(view, currentIndex);
            view.context.$implicit = record.item;
            view.detectChanges();
          }
        });

        // Update $implicit for any items that had an identity change.
        changes.forEachIdentityChange((record: IterableChangeRecord<any>) => {
          const view = this.viewContainerRef.get(record.currentIndex) as
            EmbeddedViewRef<any>;
          view.context.$implicit = record.item;
          view.detectChanges();
        });
      }


    }
    if (this.collectionHasChanged) {
      this.collectionHasChanged = false;
      for (let i = 0; i < this.viewContainerRef.length; i++) {
        const view = this.viewContainerRef.get(i) as EmbeddedViewRef<any>;
        view.context.$implicit = this.itemsToRender[i];
        view.detectChanges();
      }
    }
  }

  private removeView(adjustedPreviousIndex) {
    const viewRef = this.viewContainerRef.detach(adjustedPreviousIndex) as EmbeddedViewRef<any>;
    if (this._templateCache.length < this.templateCacheSize) {
      this._templateCache.push(viewRef);
    } else {
      const index = this.viewContainerRef.indexOf(viewRef);
      if (index === -1) {
        viewRef.destroy();
      } else {
        this.viewContainerRef.remove(index);
      }
    }
  }

  private insertView(index: number): EmbeddedViewRef<any> {
    return this.retrieveViewFromCache(index) || this.viewContainerRef.createEmbeddedView(this.template, {
      $implicit: null
    }, index);
  }

  private retrieveViewFromCache(index: number): EmbeddedViewRef<any> {
    const cachedView = this._templateCache.pop();
    if (cachedView) {
      this.viewContainerRef.insert(cachedView, index);
    }
    return cachedView || null;
  }

  ngOnDestroy(): void {
    this._templateCache = [];
  }
}
