import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CampaignPickerComponent} from './campaign-picker.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {RouterTestingModule} from '@angular/router/testing';

describe('CampaignPickerComponent', () => {
  let component: CampaignPickerComponent;
  let fixture: ComponentFixture<CampaignPickerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule, OverlayModule, RouterTestingModule],
      declarations: [CampaignPickerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
