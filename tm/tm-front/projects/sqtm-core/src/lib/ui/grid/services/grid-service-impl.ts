import {Observable, of, Subject, throwError} from 'rxjs';
import {DataRow, DataRowOpenState, dndPlaceholderDataRowId} from '../model/data-row.model';
import {concatMap, filter, map, switchMap, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {DefinitionState} from '../model/state/definition.state';
import {ColumnDefinition, Sort, SortedColumn} from '../model/column-definition.model';
import {GridDisplay, ViewportDisplay} from '../model/grid-display.model';
import {PaginationDisplay} from '../model/pagination-display.model';
import {GridDataProvider, GridDefinition, GridType} from '../model/grid-definition.model';
import {ColumnDisplay, ColumnWithFilter} from '../model/column-display.model';
import {GridNodeGenerator} from './node-generators/grid-node-generator';
import {DataRowLoader} from './data-row-loaders/data-row-loader';
import {PaginationState} from '../model/state/pagination.state';
import {ColumnDefinitionManager} from './column-definition-managers/column-definition-manager';
import {Identifier} from '../../../model/entity.model';
import {MoveColumnAction} from '../model/actions/move-column.action';
import {ChangeFilteringAction} from '../../filters/state/change-filtering.action';
import {
  getColumnState,
  getDataRowByIdFactory,
  getDataRowState,
  getDefinitionState,
  getFilterConfigurationState,
  getNodeState,
  getPaginationState,
  getUiState,
  GridState,
  initialGridState,
  selectSorts
} from '../model/state/grid.state';
import {ColumnState, GridViewportName} from '../model/state/column.state';
import {dataRowEntityAdapter, DataRowState, selectOpenedRowIds} from '../model/state/datarow.state';
import {createSelector, select} from '@ngrx/store';
import {Dictionary, Update} from '@ngrx/entity';
import {
  FilterState,
  GridFilter,
  selectActiveFilters,
  selectAllFilters,
  selectFilterState,
  selectScope
} from '../model/state/filter.state';
import {FilterManager} from './filter-managers/filter-manager';
import {TableValueChange} from '../model/actions/table-value-change';
import {initialDndState, UiState} from '../model/state/ui.state';
import {FilterConfiguration} from '../model/state/configuration.state';
import {OpenContextualMenuAction} from '../model/actions/open-contextual-menu.action';
import {ContextualMenuService} from './contextual-menu/contextual-menu.service';
import {GridNodesState} from '../model/state/grid-nodes.state';
import {GridNode} from '../model/grid-node.model';
import {GridService} from './grid.service';
import {ReferentialDataService} from '../../../core/referential/services/referential-data.service';
import {BindableEntity} from '../../../model/bindable-entity.model';
import {gridLogger} from '../grid.logger';
import {createStore, Store} from '../../../core/store/store';
import {ProjectDataMap} from '../../../model/project/project-data.model';
import {FilterGroup, FilterValue, isFilterValueEmpty, Scope} from '../../filters/state/filter.state';
import {DragAndDropTarget} from '../../drag-and-drop/events';
import {AbstractServerOperationHandler} from './row-server-operation-handlers/abstract-server-operation-handler';
import {arraysAreSame} from '../../../core/utils/distinct-until-changed-arrays';
import {decrementAsyncOperationCounter, incrementAsyncOperationCounter} from './data-row-loaders/data-row-utils';
import {GridPartialConfiguration} from '../model/grid-partial-configuration.model';
import {checkAndSetInitialConfiguration} from './grid-service-functions';

export const logger = gridLogger.compose('GridServiceImpl');

function createGridNodeSelector() {
  return createSelector(getUiState, getNodeState, (uiState: UiState, gridNodesState: GridNodesState) => {
    return (gridNodesState.ids as any[]).map(id => {
      const nodeState = gridNodesState.entities[id];
      const currentDndTarget = uiState.dragState.currentDndTarget;
      const showAsDndTarget = uiState.dragState.dragging
        && currentDndTarget
        && currentDndTarget.zone === 'into'
        && nodeState.id === currentDndTarget.id;
      return {...nodeState, showAsDndTarget};
    });
  });
}

export class GridServiceImpl implements GridService {


  private store: Store<GridState> = createStore<GridState>(initialGridState(), {id: 'GridStore', logDiff: 'simple'});

  private releaseDataSource$ = new Subject<void>();

  private releaseColumns$ = new Subject<void>();

  public dataRows$: Observable<Dictionary<DataRow>>;
  private dataRowsState$: Observable<DataRowState>;

  // Output observables
  public gridNodes$: Observable<GridNode[]>;
  public gridState$: Observable<GridState>;
  public gridDisplay$: Observable<GridDisplay>;
  public paginationDisplay$: Observable<PaginationDisplay>;
  public dragging$: Observable<boolean>;
  public uiState$: Observable<UiState>;
  public filters$: Observable<GridFilter[]>;
  public sortedColumns$: Observable<SortedColumn[]>;
  public activeFilters$: Observable<GridFilter[]>;
  public scope$: Observable<Scope>;
  public enableDnd$: Observable<boolean>;
  public selectedRows$: Observable<DataRow[]>;
  public selectedRowIds$: Observable<Identifier[]>;
  public openedRowIds$: Observable<Identifier[]>;
  public columns$: Observable<ColumnWithFilter[]>;
  public filterConfigurations$: Observable<FilterConfiguration[]>;
  public onDeleteDataRow$ = new Subject<DataRow>();
  public enterRow$ = new Subject<Identifier>();
  public leaveRow$ = new Subject<Identifier>();

  // Side effects observable used by the grid itself
  // The actions here are not put in uiState, as they have noy any kind of persistence needed.
  // Basically just opening/closing overlays should be handle like that.
  private openContextualMenuSubject = new Subject<OpenContextualMenuAction>();
  private closeContextualMenuSubject = new Subject<void>();
  public openContextualMenu$: Observable<OpenContextualMenuAction> = this.openContextualMenuSubject.asObservable();
  public closeContextualMenu$: Observable<void> = this.closeContextualMenuSubject.asObservable();

  private openDisplayConfigurationDialogSubject = new Subject<void>();
  private closeDisplayConfigurationDialogSubject = new Subject<void>();
  public openDisplayConfigurationDialog$: Observable<void> = this.openDisplayConfigurationDialogSubject.asObservable();
  public closeDisplayConfigurationDialog$: Observable<void> = this.closeDisplayConfigurationDialogSubject.asObservable();


  public canCopy$: Observable<boolean>;
  public canPaste$: Observable<boolean>;
  public canDelete$: Observable<boolean>;
  public canCreate$: Observable<boolean>;
  public canExport$: Observable<boolean>;
  public hasSelectedRows$: Observable<boolean>;
  public isSortedOrFiltered$: Observable<boolean>;
  public operationHandlerAllowsDrag$: Observable<boolean>;

  private _serverOperationHandler: AbstractServerOperationHandler;

  public globalAsyncOperationRunning$: Observable<boolean>;
  public loaded$: Observable<boolean>;

  get serverOperationHandler(): AbstractServerOperationHandler {
    return this._serverOperationHandler;
  }

  set serverOperationHandler(abstractServerOperationHandler: AbstractServerOperationHandler) {
    if (this._serverOperationHandler != null) {
      throw Error('Row copier already defined for this grid');
    }
    this._serverOperationHandler = abstractServerOperationHandler;
    this.canCopy$ = abstractServerOperationHandler.canCopy$;
    this.canPaste$ = abstractServerOperationHandler.canPaste$;
    this.canDelete$ = abstractServerOperationHandler.canDelete$;
    this.canCreate$ = abstractServerOperationHandler.canCreate$;
    this.canExport$ = abstractServerOperationHandler.canExport$;
    this.operationHandlerAllowsDrag$ = abstractServerOperationHandler.canDrag$;
  }

  constructor(gridDefinition: GridDefinition,
              private referentialDataService: ReferentialDataService,
              public gridNodeGenerator: GridNodeGenerator,
              public dataRowLoader: DataRowLoader,
              public columnDefinitionManager: ColumnDefinitionManager,
              public filterManager: FilterManager,
              private contextualMenuService: ContextualMenuService) {

    const nextState: GridState = {...initialGridState()};
    if (logger.isDebugEnabled()) {
      logger.debug(`Loading grid configuration :`);
      logger.debug(JSON.stringify(gridDefinition));
    }
    nextState.paginationState = {...nextState.paginationState, ...gridDefinition.pagination};
    nextState.definitionState = {...nextState.definitionState, ...gridDefinition};
    nextState.uiState = {...nextState.uiState, showFilterManager: gridDefinition.showFilterPanel};
    this.dataRowLoader.setStore(this.store);
    this.initializeOutputObservables();
    this.commit(nextState);
    this.initializeColumnDefinitions(
      gridDefinition.columnDefinitions,
      gridDefinition.initialSortedColumns,
      gridDefinition.appendCufFrom,
      gridDefinition.shouldResetSorts);
  }

  private initializeOutputObservables() {
    this.dataRowsState$ = this.store.state$.pipe(
      select(getDataRowState),
    );

    this.dataRows$ = this.store.state$.pipe(
      select(createSelector(getDataRowState, dataRowState => dataRowState.entities)),
    );

    this.selectedRows$ = this.store.state$.pipe(
      select(createSelector(getDataRowState, dataRowState => {
        return dataRowState.selectedRows
          .map(rowId => {
            return dataRowState.entities[rowId];
          })
          .filter(row => Boolean(row));
      })),
    );

    this.hasSelectedRows$ = this.store.state$.pipe(
      map(state => state.dataRowState.selectedRows.length > 0)
    );

    this.selectedRowIds$ = this.store.state$.pipe(
      select(createSelector(getDataRowState, dataRowState => {
        return dataRowState.selectedRows;
      })));

    this.openedRowIds$ = this.store.state$.pipe(
      select(createSelector(getDataRowState, selectOpenedRowIds)));

    const gridNodeSelector = createGridNodeSelector();

    const gridDisplaySelector = createSelector(getColumnState, getDefinitionState, gridNodeSelector, getUiState, selectFilterState,
      GridServiceImpl.transformGridDisplay);

    this.gridState$ = this.store.state$;

    this.gridNodes$ = this.store.state$.pipe(
      select(gridNodeSelector),
    );

    this.gridDisplay$ = this.store.state$.pipe(
      select(gridDisplaySelector),
      filter(value => Boolean(value.id)),
    );

    this.paginationDisplay$ = this.store.state$.pipe(
      select(
        createSelector(
          getPaginationState,
          getDataRowState,
          selectFilterState,
          getDefinitionState,
          GridServiceImpl.transformPaginationDisplay
        )),
    );

    this.filters$ = this.store.state$.pipe(
      select(selectAllFilters)
    );

    this.sortedColumns$ = this.store.state$.pipe(
      select(selectSorts)
    );

    this.activeFilters$ = this.store.state$.pipe(
      select(selectActiveFilters)
    );

    this.scope$ = this.store.state$.pipe(
      select(selectScope)
    );

    this.enableDnd$ = this.store.state$.pipe(
      select(createSelector(selectFilterState, getColumnState, (filterState, columnState) => {
          return filterState.filters.ids.length === 0 && columnState.sortedColumns.length === 0;
        })
      ),
    );

    this.uiState$ = this.store.state$.pipe(
      select(getUiState),
    );

    this.dragging$ = this.store.state$.pipe(
      select(createSelector(getUiState, uiState => uiState.dragState.dragging)),
    );

    this.columns$ = this.store.state$.pipe(
      select(createSelector(gridDisplaySelector, selectFilterState, GridServiceImpl.transformColumns)),
    );

    this.filterConfigurations$ = this.store.state$.pipe(
      select(createSelector(getFilterConfigurationState, (filterConfigurationState) => {
        const availableConfigurations: FilterConfiguration[] = [];
        availableConfigurations.push(filterConfigurationState.defaultConfiguration);
        availableConfigurations.push(...Object.values(filterConfigurationState.entities));
        return availableConfigurations;
      })),
    );

    this.globalAsyncOperationRunning$ = this.store.state$.pipe(
      select(createSelector(getUiState, getDefinitionState, (uiState, definitionState) => {
        return uiState.globalAsyncOperationCounter > 0 ||
          definitionState.style.showOverlayBeforeLoad && !definitionState.loaded;
      })),
    );

    this.loaded$ = this.store.state$.pipe(
      select(createSelector(getDefinitionState, (definitionState) => definitionState.loaded)),
    );

    this.isSortedOrFiltered$ = this.store.state$.pipe(
      map(state => this.isFilteredOrSorted(state))
    );
  }

  public static transformGridDisplay(columnState: ColumnState, definitionState: DefinitionState,
                                     gridNodes: GridNode[], uiState: UiState, filterState: FilterState)
    : GridDisplay {
    return {
      id: definitionState.id,
      rowHeight: definitionState.rowHeight,
      gridType: definitionState.gridType,
      allowShowAll: definitionState.allowShowAll,
      enableHeaders: definitionState.enableHeaders,
      enableRightToolBar: definitionState.enableRightToolBar,
      dynamicHeight: definitionState.dynamicHeight,
      enableDrag: definitionState.enableDrag,
      enableInternalDrop: definitionState.enableInternalDrop,
      draggedContentRenderer: definitionState.draggedContentRenderer,
      leftViewport: computeInvariableViewport(columnState, definitionState, gridNodes, 'leftViewport', filterState),
      mainViewport: computeInvariableViewport(columnState, definitionState, gridNodes, 'mainViewport', filterState),
      rightViewport: computeInvariableViewport(columnState, definitionState, gridNodes, 'rightViewport', filterState),
      style: definitionState.style,
      loaded: definitionState.loaded,
      allowModifications: definitionState.allowModifications,
      scopeDefinition: definitionState.scopeDefinition,
      scope: filterState.scope
    };
  }

  private static transformPaginationDisplay(
    pagination: PaginationState,
    dataRowState: DataRowState,
    filterState: FilterState,
    definitionState: DefinitionState
  ): PaginationDisplay {
    let maxPage;
    const activeFilters = Object.values(filterState.filters.entities).filter(f => f.active);
    if (activeFilters.length > 0 && definitionState.dataProvider === GridDataProvider.CLIENT) {
      maxPage = GridServiceImpl.getMaxPage(pagination, filterState.filters.matchingRowIds.length);
    } else {
      maxPage = GridServiceImpl.getMaxPage(pagination, dataRowState.count);
    }
    let showPaginationFooter = pagination.active;
    let count = dataRowState.count;

    if (activeFilters.length > 0 && definitionState.dataProvider === GridDataProvider.CLIENT) {
      count = filterState.filters.matchingRowIds.length;
    }

    if (showPaginationFooter) {
      if (pagination.fixedPaginationSize && count <= pagination.size) {
        showPaginationFooter = false;
      }
    }
    return {
      ...pagination,
      count,
      showPaginationFooter,
      maxPage
    };
  }

  private static transformColumns(gridDisplay: GridDisplay, filterState: FilterState): ColumnWithFilter[] {
    const columns: ColumnWithFilter[] = [];
    columns.push(...gridDisplay.leftViewport.columnDisplays);
    columns.push(...gridDisplay.mainViewport.columnDisplays);
    columns.push(...gridDisplay.rightViewport.columnDisplays);

    columns.forEach((column: ColumnWithFilter) => {
      const filterId = column.associateToFilter;
      column.filter = filterState.filters.entities[filterId];
    });
    return columns;
  }

  private static getMaxPage(pagination: PaginationState, count: number) {
    if (!Boolean(count)) {
      return 0;
    }
    const modulo = count % pagination.size;
    let maxPage = Math.floor(count / pagination.size) - 1;
    if (modulo > 0) {
      maxPage++;
    }
    return maxPage;
  }

  public loadInitialDataRows(payload: Partial<DataRow>[], count: number, initialSelectedRows?: Identifier[]) {
    const selectedRows = initialSelectedRows || [];
    this.store.state$.pipe(
      take(1),
      withLatestFrom(this.referentialDataService.projectDatas$),
      concatMap(([state, projectData]: [GridState, ProjectDataMap]) =>
        this.dataRowLoader.loadInitialData(payload, count, selectedRows, state, projectData)
      ),
      concatMap(state => this.filterManager.applyFilters(state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state)),
      map(state => this.notifyDataIsLoaded(state))
    ).subscribe(state => this.commit(state));
  }

  public connectToDatasource<T>(datasource: Observable<T[]>, idAttribute?: keyof T) {
    this.releaseDatasource();
    datasource.pipe(
      takeUntil(this.releaseDataSource$),
      withLatestFrom(this.store.state$, this.referentialDataService.projectDatas$, this.selectedRowIds$),
      concatMap(([data, state, projectData, selectedRowIds]) => {
        const dataRows = this.transformArrayToDataRows(data, idAttribute);
        logger.debug(`Connect to Data Source ProjectData : ${projectData}`);
        // Keeping previous open/close state if row already exist in previous state
        return this.updateConnectedRows(dataRows, state, selectedRowIds, projectData);
      }),
      concatMap(state => this.filterManager.applyFilters(state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state)),
      map((state) => this.notifyDataIsLoaded(state))
    ).subscribe(state => this.commit(state));
  }

  public connectToDataRowSource(datasource: Observable<DataRow[]>) {
    this.releaseDatasource();
    datasource.pipe(
      takeUntil(this.releaseDataSource$),
      withLatestFrom(this.store.state$, this.referentialDataService.projectDatas$, this.selectedRowIds$),
      concatMap(([dataRows, state, projectData, selectedRowIds]) => {
        logger.debug(`Connect to DataRows Observable Source : ${projectData}`);
        // Keeping previous open/close state if row already exist in previous state
        return this.updateConnectedRows(dataRows, state, selectedRowIds, projectData);
      }),
      concatMap(state => this.filterManager.applyFilters(state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state)),
      map((state) => this.notifyDataIsLoaded(state))
    ).subscribe(state => this.commit(state));
  }

  private updateConnectedRows(dataRows, state, selectedRowIds, projectData): Observable<GridState> {
    dataRows
      .filter(row => Boolean(state.dataRowState.entities[row.id]))
      .forEach(row => {
        row.state = state.dataRowState.entities[row.id].state;
      });
    // Only keep selected rows that are still present in grid
    selectedRowIds = selectedRowIds.filter(rowId => dataRows.map(row => row.id).includes(rowId));
    if (state.paginationState.active) {
      state = this.limitCurrentPageBasedOnMaxPage(dataRows.length, state);
    }

    return this.dataRowLoader.loadInitialData(dataRows, dataRows.length, selectedRowIds, state, projectData);
  }

  private limitCurrentPageBasedOnMaxPage(rowCount: number, state: GridState): GridState {
    const maxPage = GridServiceImpl.getMaxPage(state.paginationState, rowCount);
    const currentPage = state.paginationState.page;
    const updatedPaginationState = {
      ...state,
      paginationState: {
        ...state.paginationState,
        page: maxPage
      }
    };

    return currentPage > maxPage ? updatedPaginationState : state;
  }

  public beginAsyncOperation() {
    this.store.state$.pipe(
      take(1),
      map(state => incrementAsyncOperationCounter(state))
    ).subscribe(state => this.commit(state));
  }

  public completeAsyncOperation() {
    this.store.state$.pipe(
      take(1),
      map(state => decrementAsyncOperationCounter(state))
    ).subscribe(state => this.commit(state));
  }

  public releaseDatasource() {
    this.releaseDataSource$.next();
  }

  public addColumnAtIndex(columnDefinition: ColumnDefinition[], index?: number) {
    this.store.state$.pipe(
      take(1),
      concatMap(state => {
        return this.columnDefinitionManager.addColumnAtIndex(columnDefinition, state, index);
      })
    ).subscribe(state => this.commit(state));
  }

  // public methods to manipulate state
  public loadInitialData<T>(payload: T[], count: number, idAttribute?: keyof T, initialSelectedRows?: Identifier[]) {
    const selectedRows = initialSelectedRows || [];
    this.store.state$.pipe(
      take(1),
      withLatestFrom(this.referentialDataService.projectDatas$),
      concatMap(([state, projectData]: [GridState, ProjectDataMap]) => {
        const dataRows = this.transformArrayToDataRows(payload, idAttribute);
        return this.dataRowLoader.loadInitialData(dataRows, count, selectedRows, state, projectData);
      }),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state)),
      map(state => this.notifyDataIsLoaded(state))
    ).subscribe(state => this.commit(state));
  }

  public goToLastPage() {
    this.store.state$.pipe(
      take(1),
      map(state => {
        const paginationState = {...state.paginationState};
        paginationState.page = GridServiceImpl.getMaxPage(paginationState, state.dataRowState.count);
        return {...state, paginationState};
      }),
      concatMap(state => this.refreshRows(state)),
    ).subscribe(state => this.commit(state));
  }

  public goToFirstPage() {
    this.store.state$.pipe(
      take(1),
      map(state => {
        const paginationState = {...state.paginationState};
        paginationState.page = 0;
        return {...state, paginationState};
      }),
      concatMap(state => this.refreshRows(state)),
    ).subscribe(state => this.commit(state));
  }

  public paginateNextPage() {
    this.store.state$.pipe(
      take(1),
      map(state => {
        const paginationState = {...state.paginationState};
        if (paginationState.page < GridServiceImpl.getMaxPage(paginationState, state.dataRowState.count)) {
          paginationState.page++;
        }
        return {...state, paginationState};
      }),
      concatMap(state => this.refreshRows(state)),
    ).subscribe(state => this.commit(state));
  }

  public paginatePreviousPage() {
    this.store.state$.pipe(
      take(1),
      map(state => {
        const paginationState = {...state.paginationState};
        if (paginationState.page > 0) {
          paginationState.page--;
        }
        return {...state, paginationState};
      }),
      concatMap(state => this.refreshRows(state)),
    ).subscribe(state => this.store.commit(state));
  }

  public addRows<T>(payload: T[], parentId?: Identifier, position?: number, idAttribute?: keyof T) {
    this.store.state$.pipe(
      take(1),
      tap(() => logger.debug(`Adding rows to: ${parentId} : `, payload)),
      withLatestFrom(this.referentialDataService.projectDatas$),
      map(([state, projectData]: [GridState, ProjectDataMap]) => {
        const dataRowLiterals = this.transformArrayToDataRows(payload, idAttribute);
        const dataRows = state.definitionState.literalDataRowConverter(dataRowLiterals, projectData);
        return this.dataRowLoader.addRows(dataRows, state, parentId);
      }),
      concatMap(state => this.refreshRows(state)),
      tap(state => logger.debug(`refresh rows: ${JSON.stringify(state)}`)),
      concatMap(state => this.filterManager.applyFilters(state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    ).subscribe(state => this.commit(state));
  }

  private addDataRows(rows: DataRow[], parentId?: Identifier): void {
    this.store.state$.pipe(
      take(1),
      withLatestFrom(this.referentialDataService.projectDatas$),
      map(([state, projectData]: [GridState, ProjectDataMap]) => {
        const dataRows = state.definitionState.literalDataRowConverter(rows, projectData);
        return this.dataRowLoader.addRows(dataRows, state, parentId);
      }),
      concatMap(state => this.filterManager.applyFilters(state)),
      concatMap(state => this.refreshRows(state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    ).subscribe(state => this.commit(state));
  }

  addAndSelectRow(added: DataRow, parentId: Identifier): void {
    this.connectToDataRow(parentId).pipe(
      take(1),
      filter((parentRow) => Boolean(parentRow))
    ).subscribe((parentRow) => {
      if (parentRow.state === DataRowOpenState.open) {
        this.addDataRows([added], parentRow.id);
        this.selectSingleRow(added.id);
      } else {
        this.openRowAndSelectChild(parentRow.id, added.id);
      }
    });
  }

  public changePaginationSize(pageSize: number) {
    this.store.state$.pipe(
      take(1),
      tap(() => logger.debug(`Change pagination size to : ${pageSize}`)),
      filter(state => state.paginationState.allowedPageSizes.includes(pageSize) || (state.definitionState.allowShowAll && pageSize === -1)),
      map(state => {
        const paginationState = {...state.paginationState};
        paginationState.size = pageSize;
        paginationState.page = 0;
        return {...state, paginationState};
      }),
      concatMap(state => this.refreshRows(state))
    ).subscribe(state => this.commit(state));
  }

  private resetPage(state: GridState): Observable<GridState> {
    const paginationState = {...state.paginationState};
    paginationState.page = 0;
    return of({...state, paginationState});
  }

  private openRowAndSelectChild(parentId: Identifier, childId: Identifier): void {
    this.doOpenRows([parentId]).pipe(map(nextState => this.commit(nextState)))
      .subscribe(() => this.selectSingleRow(childId));
  }

  public openRow(id: string | number): void {
    this.doOpenRows([id]).subscribe(nextState => this.commit(nextState));
  }

  public openRows(ids: Identifier[]): void {
    this.doOpenRows(ids).subscribe(nextState => this.commit(nextState));
  }

  public openAllRows(): void {
    this.store.state$.pipe(
      take(1),
      concatMap(state => this.doOpenRows(state.dataRowState.ids))
    ).subscribe(nextState => this.commit(nextState));
  }

  public closeAllRows(): void {
    this.store.state$.pipe(
      take(1),
      switchMap((state) => this.dataRowLoader.closeRows(state.dataRowState.ids, state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    ).subscribe(nextState => this.commit(nextState));
  }

  private doOpenRows(ids: Identifier[]): Observable<GridState> {
    return this.referentialDataService.projectDatas$.pipe(
      take(1),
      withLatestFrom(this.store.state$),
      map(([projectData, state]) => {
        const nextState: GridState = this.beginRowsAsync(ids, state);
        return [projectData, nextState];
      }),
      concatMap(([projectData, state]: [ProjectDataMap, GridState]) => {
        return this.gridNodeGenerator.computeNodeTree(state).pipe(
          map(nextState => [projectData, nextState])
        );
      }),
      tap(([, state]: [ProjectDataMap, GridState]) => this.commit(state)),
      concatMap(([projectData]) => this.dataRowLoader.openRows(ids, this.store.state$, projectData)),
      concatMap(state => this.filterManager.applyFilters(state)),
      map(state => this.endRowsAsync(ids, state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    );
  }

  private beginRowsAsync(ids: Identifier[], state): GridState {
    return this.updateRowsAsync(ids, true, state);
  }

  private endRowsAsync(ids: Identifier[], state): GridState {
    return this.updateRowsAsync(ids, false, state);
  }

  private updateRowsAsync(ids: Identifier[], async: boolean, state: GridState) {
    const updates: Update<DataRow>[] = ids.map(id => ({id: id as any, changes: {async}}));
    const dataRowState = dataRowEntityAdapter.updateMany(updates, state.dataRowState);
    return {...state, dataRowState};
  }

  private doRefreshRows(ids: Identifier[]): Observable<GridState> {
    return this.referentialDataService.projectDatas$.pipe(
      take(1),
      switchMap((projectData: ProjectDataMap) => this.dataRowLoader.openRows(ids, this.store.state$, projectData)),
      switchMap(state => this.filterManager.applyFilters(state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    );
  }

  public closeRow(id: string | number) {
    this.store.state$.pipe(
      take(1),
      switchMap((state) => this.dataRowLoader.closeRows([id], state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    ).subscribe(nextState => this.commit(nextState));
  }

  public addToSelection(id: string | number) {
    this.store.state$.pipe(
      take(1),
      switchMap((state) => this.dataRowLoader.addToSelection(id, state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    ).subscribe(nextState => this.commit(nextState));
  }

  public toggleRowSelection(id: string | number) {
    this.store.state$.pipe(
      take(1),
      filter((state) => state.definitionState.enableMultiRowSelection),
      switchMap((state) => this.dataRowLoader.toggleRowSelection(id, state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    ).subscribe(nextState => this.commit(nextState));
  }

  /**
   * This method select the designed row and unselect everything else if the selection is legal
   * @param id identifier of the candidate to selection
   */
  public selectSingleRow(id: Identifier) {
    this.store.state$.pipe(
      take(1),
      tap(state => logger.debug(`${state.definitionState.id}- Select Row : ${id}`)),
      filter(state => (state.dataRowState.ids as any[]).includes(id) && !arraysAreSame([id], state.dataRowState.selectedRows)),
      switchMap((state) => this.dataRowLoader.selectSingleRow(id, state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    ).subscribe(nextState => this.commit(nextState));
  }

  public toggleColumnSort(id: string | number) {
    this.store.state$.pipe(
      take(1),
      concatMap((state) => this.columnDefinitionManager.toggleSortColumn(id, state)),
      concatMap(state => this.resetPage(state)),
      concatMap(state => this.refreshRows(state))
    ).subscribe(state => this.commit(state));
  }

  public setColumnSorts(sortedColumns: SortedColumn[]) {
    this.store.state$.pipe(
      take(1),
      concatMap((state) => this.columnDefinitionManager.setSortColumn(sortedColumns, state)),
      concatMap(state => this.resetPage(state)),
      concatMap(state => this.refreshRows(state))
    ).subscribe(state => this.commit(state));
  }

  public startDrag(draggedRowIds: Identifier[]): void {
    this.store.state$.pipe(
      take(1),
      filter((state) => state.definitionState.enableDrag && state.definitionState.enableInternalDrop),
      filter((state) => this.checkDragAllowed(draggedRowIds, state)),
      withLatestFrom(this.operationHandlerAllowsDrag$ ?? of(true)),
      filter(([, operationHandlerAllowsDrag]) => operationHandlerAllowsDrag),
      map(([state]) => {
        return {
          ...state,
          uiState: {...state.uiState, dragState: {...state.uiState.dragState, draggedRowIds, dragging: true}}
        };
      }),
      concatMap(state => this.gridNodeGenerator.dragStart(state))
    ).subscribe(state => {
      this.commit(state);
    });
  }

  private isFilteredOrSorted(state: GridState): boolean {
    return this.hasActiveNonEmptyFilters(state) || this.hasSortedColumns(state);
  }

  private hasActiveNonEmptyFilters(state: GridState): boolean {
    return (state.filterState.filters.ids as Identifier[])
      .map(id => state.filterState.filters.entities[id])
      .filter(filterState => filterState.active && this.filterHasNonEmptyValue(filterState))
      .length > 0;
  }

  private filterHasNonEmptyValue(gridFilter: GridFilter): Boolean {
    return isFilterValueEmpty(gridFilter.value);
  }

  private hasSortedColumns(state: GridState): boolean {
    return state.columnState.sortedColumns.length > 0;
  }

  private checkDragAllowed(draggedRowIds: Identifier[], state: GridState) {
    const forbidden = draggedRowIds
      .map(id => state.dataRowState.entities[id])
      .map(row => row.allowMoves)
      .includes(false);
    return !forbidden;
  }

  notifyDragOver(dragTarget: DragAndDropTarget): void {
    this.store.state$.pipe(
      take(1),
      filter(state => this.isInternalDragAndDropEnabled(state)),
      withLatestFrom(this.operationHandlerAllowsDrag$ ?? of(true)),
      filter(([, operationHandlerAllowsDrag]) => operationHandlerAllowsDrag),
      map(([state]) => state),
      filter(state => this.isNewTargetDifferentFromPrevious(state, dragTarget)),
      filter(() => this.isNotPlaceholder(dragTarget)),
      withLatestFrom(dragTarget.zone === 'into' ? this.allowDropInto(dragTarget.id) : this.allowDropSibling(dragTarget.id)),
      filter(([, allowDrop]: [GridState, boolean]) => allowDrop),
      concatMap(([state]: [GridState, boolean]) => this.gridNodeGenerator.dragOver(dragTarget, state))
    ).subscribe((state: GridState) => this.commit(state));
  }

  private isNotPlaceholder(dragTarget: DragAndDropTarget) {
    return dragTarget.id !== dndPlaceholderDataRowId;
  }

  private isNewTargetDifferentFromPrevious(state: GridState, dragTarget: DragAndDropTarget): boolean {
    const currentDndTarget = state.uiState.dragState.currentDndTarget;
    return !currentDndTarget || !(currentDndTarget.id === dragTarget.id && currentDndTarget.zone === dragTarget.zone);
  }

  private isInternalDragAndDropEnabled(state: GridState): boolean {
    return state.definitionState.enableInternalDrop;
  }

  notifyInternalDrop() {
    this._serverOperationHandler.notifyInternalDrop();
  }

  cancelDrag() {
    this.store.state$.pipe(
      take(1),
      filter(state => this.isDragAndDropRunning(state)),
      concatMap(state => this.doCancelDrag(state))
    ).subscribe(state => this.commit(state));
  }

  private isDragAndDropRunning(state: GridState): boolean {
    return state.uiState.dragState.dragging;
  }

  doCancelDrag(initialState: Readonly<GridState>): Observable<GridState> {
    return of(initialState).pipe(
      map(state => {
        return {...state, uiState: {...state.uiState, dragState: {...initialDndState()}}};
      }),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    );
  }

  suspendDrag() {
    this.store.state$.pipe(
      take(1),
      filter(state => this.isDragAndDropRunning(state)),
      concatMap(state => this.doSuspendDrag(state))
    ).subscribe(state => this.commit(state));
  }

  doSuspendDrag(initialState: Readonly<GridState>): Observable<GridState> {
    return of(initialState).pipe(
      map(state => {
        return {...state, uiState: {...state.uiState, dragState: {...state.uiState.dragState, dragging: false}}};
      }),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    );
  }

  allowDropSibling(id: Identifier): Observable<boolean> {
    if (this.serverOperationHandler == null) {
      throw Error(
        `Providing serverOperationHandler is mandatory to allow drag and drop operation in the grid.
         See grid.service.provider.ts#gridServiceFactory`);
    }
    return this.store.state$.pipe(
      take(1),
      map(state => this.serverOperationHandler.allowDropSibling(id, state))
    );
  }

  allowDropInto(id: Identifier): Observable<boolean> {
    if (this.serverOperationHandler == null) {
      throw Error(
        `Providing serverOperationHandler is mandatory to allow drag and drop operation in the grid.
         See grid.service.provider.ts#gridServiceFactory`);
    }
    return this.store.state$.pipe(
      take(1),
      map(state => this.serverOperationHandler.allowDropInto(id, state))
    );
  }

  public changeFilterValue(changeFilterValueAction: ChangeFilteringAction): void {
    this.store.state$.pipe(
      take(1),
      concatMap(state => this.filterManager.changeFilterValue(changeFilterValueAction, state)),
      concatMap(state => this.filterManager.changeFilterOperation(changeFilterValueAction, state)),
      concatMap(state => this.filterManager.applyFilters(state)),
      concatMap(state => this.dataRowLoader.unselectAllRows(state)),
      concatMap(state => this.resetPage(state)),
      concatMap(state => this.refreshRows(state))
    ).subscribe(state => this.commit(state));
  }

  public replaceFilters(filters: GridFilter[], scope: Scope, filterGroups?: FilterGroup[]): void {
    this.store.state$.pipe(
      take(1),
      map(state => this.filterManager.replaceFilters(filters, state)),
      map(state => {
        if (Boolean(filterGroups)) {
          return this.filterManager.replaceFilterGroups(filterGroups, state);
        }
        return state;
      }),
      withLatestFrom(this.referentialDataService.projectDatas$),
      map(([state, projectMap]) => this.filterManager.replaceScope(scope, projectMap, state)),
      concatMap(state => this.filterManager.applyFilters(state)),
      concatMap(state => this.dataRowLoader.unselectAllRows(state)),
      concatMap(state => this.resetPage(state)),
      concatMap(state => this.refreshRows(state)),
      map((state: GridState) => this.notifyDataIsLoaded(state)),
    ).subscribe((state: GridState) => this.commit(state));
  }

  public toggleColumnVisibility(id: Identifier): void {
    this.store.state$.pipe(
      take(1),
      switchMap(state => this.columnDefinitionManager.toggleColumnVisibility(id, state))
    ).subscribe(state => this.commit(state));
  }

  public setColumnVisibility(id: Identifier, show: boolean): void {
    this.store.state$.pipe(
      take(1),
      switchMap(state => this.columnDefinitionManager.setColumnVisibility(show, id, state))
    ).subscribe(state => this.commit(state));
  }

  public moveColumn(moveColumnAction: MoveColumnAction): void {
    this.store.state$.pipe(
      take(1),
      switchMap(state => this.columnDefinitionManager.moveColumn(moveColumnAction, state))
    ).subscribe(state => this.commit(state));
  }

  // Do not recompute all nodes when editing to avoid rows jumping around if the edited column is ordered or filtered !
  // However we do the filter so when the user will require a correct display by sorting, opening a node... the filter is correctly done.
  public editRows(ids: Identifier[], changes: TableValueChange[]) {
    this.store.state$.pipe(
      take(1),
      concatMap(state => this.dataRowLoader.updateRows(ids, changes, state)),
      concatMap(state => this.gridNodeGenerator.updateNodesData(ids, state)),
      concatMap(nextState => {
        const filteredColumnIds = nextState.filterState.filters.ids as Identifier[];
        const changedColumns = changes.map(change => change.columnId);
        let mustFilter = false;
        for (const changedColumn of changedColumns) {
          if (filteredColumnIds.includes(changedColumn)) {
            mustFilter = true;
            break;
          }
        }
        if (mustFilter) {
          return of(nextState).pipe(
            concatMap(state => this.filterManager.applyFilters(state))
          );
        } else {
          return of(nextState);
        }
      })
    ).subscribe(state => this.commit(state));
  }

  public removeRows(ids: Identifier[]): void {
    this.store.state$.pipe(
      take(1),
      concatMap(state => this.dataRowLoader.removeRows(ids, state)),
      concatMap(removeResult => this.gridNodeGenerator.computeNodeTree(removeResult.state))
    ).subscribe(state => this.commit(state));
  }

  public deleteRows(): void {
    if (this.serverOperationHandler == null) {
      throw Error(
        `Providing serverOperationHandler is mandatory to allow delete operation in the grid.
         See grid.service.provider.ts#gridServiceFactory`);
    }
    this._serverOperationHandler.delete();
  }

  // COLD OBSERVABLE ! No share() or shareReplay()
  // We don't want these observables survive to the RowComponent destruction or we will have a massive memory leak.
  public connectToDataRow(id): Observable<DataRow> {
    return this.store.state$.pipe(
      select(getDataRowByIdFactory(id)));
  }

  toggleColumnManager() {
    this.store.state$.pipe(
      take(1),
      map(state => {
        const uiState = {...state.uiState};
        uiState.showColumnManager = !uiState.showColumnManager;
        return {...state, uiState};
      })
    ).subscribe(state => this.commit(state));
  }

  toggleFilterManager() {
    this.store.state$.pipe(
      take(1),
      map(state => {
        const uiState = {...state.uiState};
        uiState.showFilterManager = !uiState.showFilterManager;
        return {...state, uiState};
      })
    ).subscribe(state => this.commit(state));
  }

  offsetColumnWidth(id: Identifier, offset: number) {
    this.store.state$.pipe(
      take(1),
      concatMap(state => {
        return this.columnDefinitionManager.resizeColumn(id, offset, state);
      })
    ).subscribe(state => this.commit(state));
  }

  defineId(id: string) {
    if (!Boolean(id)) {
      throw Error('id cannot be blank');
    }
    this.store.state$.pipe(
      take(1),
      concatMap(gridState => {
        if (Boolean(gridState.definitionState.id)) {
          return (throwError(`Id already defined to ${gridState.definitionState.id}`));
        } else {
          return of(gridState);
        }
      }),
      map((gridState: GridState) => ({...gridState, definitionState: {...gridState.definitionState, id}}))
    ).subscribe(state => this.commit(state));
  }

  setServerUrl(serverUrl: string[]) {
    this.store.state$.pipe(
      take(1),
      map(state => {
        const definitionState = {...state.definitionState};
        definitionState.serverRootUrl = serverUrl;
        return {...state, definitionState};
      }),
      concatMap(state => this.refreshRows(state)),
      map(state => this.notifyDataIsLoaded(state))
    ).subscribe(state => this.commit(state));
  }


  openContextualMenu(action: OpenContextualMenuAction) {
    this.store.state$.pipe(
      take(1),
      filter(state => Boolean(state.definitionState.contextualMenuComponent)),
      map(state => {
        return this.contextualMenuService.openContextualMenu(action, state);
      }),
      // Emitting in side effect so the components can react to the action.
      tap(state => {
        action.component = state.definitionState.contextualMenuComponent;
        this.openContextualMenuSubject.next(action);
      })
    ).subscribe(state => this.commit(state));
  }

  closeContextualMenu() {
    this.store.state$.pipe(
      take(1),
      filter(state => Boolean(state.definitionState.contextualMenuComponent)),
      map(state => {
        return this.contextualMenuService.closeContextualMenu(state);
      }),
      // Emitting in side effect so the components can react to the action.
      tap(() => {
        this.closeContextualMenuSubject.next();
      })
    ).subscribe(state => this.commit(state));
  }

  toggleGridType(gridType: GridType) {
    this.store.state$.pipe(
      map(state => {
        return {...state, definitionState: {...state.definitionState, gridType: gridType}};
      })
    ).subscribe(state => this.commit(state));
  }


  openDisplayConfigurationManager() {
    this.closeContextualMenu();
    this.openDisplayConfigurationDialogSubject.next();
  }

  selectAllRows() {
    this.store.state$.pipe(
      take(1),
      concatMap(state => this.dataRowLoader.selectAllRows(state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    ).subscribe(nextState => this.store.commit(nextState));
  }

  unselectAllRows() {
    this.store.state$.pipe(
      take(1),
      tap(state => logger.debug(`${state.definitionState.id}- Unselect All Rows`)),
      concatMap(state => this.dataRowLoader.unselectAllRows(state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    ).subscribe(state => this.store.commit(state));
  }

  invertSelection() {
    this.store.state$.pipe(
      take(1),
      concatMap(state => this.dataRowLoader.invertSelection(state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    ).subscribe(state => this.store.commit(state));
  }

  refreshData() {
    this.store.state$.pipe(
      take(1),
      concatMap(state => this.refreshRows(state)),
      map(state => this.notifyDataIsLoaded(state))
    ).subscribe(state => this.commit(state));
  }

  refreshDataAndKeepSelectedRows() {
    this.store.state$.pipe(
      take(1),
      concatMap(state => this.refreshRows(state, true))
    ).subscribe(state => this.commit(state));
  }

  refreshSubTree(ids: Identifier[]) {
    return this.store.state$.pipe(
      take(1),
      map(state => {
        return ids
          .filter(id => Boolean(id))
          .filter(id => Boolean(state.dataRowState.entities[id]));
      }),
      withLatestFrom(this.referentialDataService.projectDatas$),
      concatMap(([filteredIds, projectData]: [Identifier[], ProjectDataMap]) =>
        this.dataRowLoader.refreshSubTrees(filteredIds, this.gridState$, projectData)),
      switchMap(state => this.filterManager.applyFilters(state)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    ).subscribe(state => this.commit(state));
  }

  private refreshRows(gridState: GridState, keepSelectedRows = false): Observable<GridState> {
    return of(gridState).pipe(
      take(1),
      map(state => this.dataRowLoader.beginAsync(state)),
      // commit before potential long async request so we can commit the already done state modifications
      tap(state => this.commit(state)),
      withLatestFrom(this.referentialDataService.projectDatas$),
      concatMap(([state, projectData]: [GridState, ProjectDataMap]) =>
        this.dataRowLoader.refreshData(state, projectData, keepSelectedRows)),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state)),
      map(state => this.dataRowLoader.endAsync(state))
    );
  }

  private transformArrayToDataRows<T>(payload: T[], idAttribute?: keyof T) {
    const resolvedIdAttribute: any = idAttribute || 'id';
    const dataRows: Partial<DataRow>[] = payload.map(item => {
      const id = item[resolvedIdAttribute];
      if (id == null) {
        throw Error(`No id attribute '${id}' for item ${JSON.stringify(item)}. The grid require that each line to be identified`);
      }
      return {id, data: {...item}};
    });
    return dataRows;
  }

  private initializeColumnDefinitions(columnDefinitions: ColumnDefinition[],
                                      initialSortedColumns: SortedColumn[],
                                      bindableEntity: BindableEntity,
                                      shouldResetSorts: boolean): void {
    this.store.state$.pipe(
      take(1),
      concatMap(state => this.columnDefinitionManager.initializeColumns(
        columnDefinitions,
        state,
        initialSortedColumns,
        bindableEntity,
        shouldResetSorts))
    ).subscribe(state => this.commit(state));
  }

  // commit method is public in GridServiceImpl but not exposed in GridService abstract class.
  // so collaborators can use it but external grid user should not...
  commit(state: GridState): void {
    logger.debug(`New Grid State :`, [state]);
    this.store.commit(state);
  }

  activateFilter(id: string, filterValue?: FilterValue) {
    this.store.state$.pipe(
      take(1),
      map(state => this.filterManager.activateFilter(id, state)),
      map(state => this.filterManager.resetFilter(id, state)),
    ).subscribe(state => this.commit(state));
  }

  inactivateFilter(id: string) {
    this.store.state$.pipe(
      take(1),
      map(state => this.filterManager.inactivateFilter(id, state)),
      concatMap(state => this.filterManager.applyFilters(state)),
      concatMap(state => this.dataRowLoader.unselectAllRows(state)),
      concatMap(state => this.resetPage(state)),
      concatMap(state => this.refreshRows(state)),
    ).subscribe(state => this.commit(state));
  }

  resetFilters() {
    this.store.state$.pipe(
      take(1),
      map(state => this.filterManager.resetAllFilters(state)),
      map(state => this.filterManager.resetScope(state)),
      concatMap(state => this.filterManager.applyFilters(state)),
      concatMap(state => this.dataRowLoader.unselectAllRows(state)),
      concatMap(state => this.resetPage(state)),
      concatMap(state => this.refreshRows(state)),
    ).subscribe(state => this.commit(state));
  }

  changeScope(scope: Scope) {
    this.store.state$.pipe(
      take(1),
      withLatestFrom(this.referentialDataService.projectDatas$),
      map(([state, projectMap]) => this.filterManager.replaceScope(scope, projectMap, state)),
      concatMap(state => this.filterManager.applyFilters(state)),
      concatMap(state => this.dataRowLoader.unselectAllRows(state)),
      concatMap(state => this.resetPage(state)),
      concatMap(state => this.refreshRows(state)),
    ).subscribe(state => this.commit(state));
  }

  updateCellValue(row: DataRow, column: ColumnDisplay, value: any): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      map(state => ([`${state.definitionState.serverModificationUrl}/${row.id}/${column.id}`])),
      concatMap(url => this.dataRowLoader.updateCell(url, {[column.id]: value}))
    );
  }

  copy() {
    if (this.serverOperationHandler == null) {
      throw Error(
        // tslint:disable-next-line:max-line-length
        'Providing serverOperationHandler is mandatory to allow copy/paste operation in the grid. See grid.service.provider.ts#gridServiceFactory');
    }
    this.serverOperationHandler.copy();
  }

  paste() {
    if (this.serverOperationHandler == null) {
      throw Error(
        // tslint:disable-next-line:max-line-length
        'Providing serverOperationHandler copier is mandatory to allow copy/paste operation in the grid. See grid.service.provider.ts#gridServiceFactory');
    }
    this.serverOperationHandler.paste();
  }

  applyMultiColumnsFilter(value: string): void {
    this.store.state$.pipe(
      take(1),
      map(state => {
        const paginationState = {...state.paginationState};
        paginationState.page = 0;
        return {...state, paginationState};
      }),
      map(state => this.filterManager.updateMultiColumnsFilterValue(state, value)),
      concatMap(state => this.filterManager.applyMultiColumnsFilter(state, value)),
      concatMap(state => this.refreshRows(state))
    ).subscribe(state => this.store.commit(state));
  }

  addFilters(gridFilters: GridFilter[]) {
    this.store.state$.pipe(
      take(1),
      map(state => this.filterManager.addFilters(gridFilters, state)),
      concatMap(state => this.filterManager.applyFilters(state))
    ).subscribe(state => this.store.commit(state));
  }

  deleteColumns(columnIds: Identifier[]) {
    this.store.state$.pipe(
      take(1),
      concatMap(state => this.columnDefinitionManager.removeColumn(columnIds, state))
    ).subscribe(state => this.store.commit(state));
  }

  renameColumn(columnId: Identifier, newLabel: string) {
    this.store.state$.pipe(
      take(1),
      concatMap(state => this.columnDefinitionManager.renameColumn(columnId, state, newLabel))
    ).subscribe(state => this.store.commit(state));
  }

  complete() {
    this.releaseDataSource$.next();
    this.releaseDataSource$.complete();
    this.openDisplayConfigurationDialogSubject.complete();
    this.closeDisplayConfigurationDialogSubject.complete();
    this.openContextualMenuSubject.complete();
    this.closeContextualMenuSubject.complete();
    this.enterRow$.complete();
    this.leaveRow$.complete();
    this.store.complete();
    logger.debug('Completing grid service');
  }

  notifyEnterRow(id: Identifier) {
    this.enterRow$.next(id);
  }

  notifyLeaveRow(id: Identifier) {
    this.leaveRow$.next(id);
  }

  lockGrid() {
    this.toggleGridLock(false);
  }

  unlockGrid() {
    this.toggleGridLock(true);
  }

  private toggleGridLock(allowModifications: boolean) {
    this.store.state$.pipe(
      take(1),
      map((state: GridState) => ({...state, definitionState: {...state.definitionState, allowModifications}})),
      concatMap(state => this.gridNodeGenerator.computeNodeTree(state))
    ).subscribe(state => this.store.commit(state));
  }

  private notifyDataIsLoaded(state: GridState): GridState {
    if (state.definitionState.loaded) {
      return {...state};
    }
    return {...state, definitionState: {...state.definitionState, loaded: true}};
  }

  setMultiSelectionEnabled(enableMultiRowSelection: boolean): void {
    this.store.state$.pipe(
      take(1),
      map((state: GridState) => ({...state, definitionState: {...state.definitionState, enableMultiRowSelection}})),
    ).subscribe(state => this.store.commit(state));
  }

  addInitialConfiguration(conf: GridPartialConfiguration): Observable<GridState> {
    return this.store.state$.pipe(
      take(1),
      tap((state: GridState) => {
        if (state.definitionState.loaded) {
          throw Error(`Cannot add configuration after grid has been initialized.
          Please add your configuration before any row loading/fetching or use other method to update conf after initialisation`);
        }
      }),
      map(state => checkAndSetInitialConfiguration(conf, state)),
      tap(state => this.commit(state))
    );
  }

  setEnableDrag(enableDrag: boolean): void {
    this.store.state$.pipe(
      take(1),
      map((state: GridState) => ({...state, definitionState: {...state.definitionState, enableDrag}})),
    ).subscribe(state => this.store.commit(state));
  }
}

// Calculate width for columns without accounting viewport container size
export function computeInvariableViewport(columnState: ColumnState,
                                          definitionState: DefinitionState,
                                          gridNodes: GridNode[],
                                          viewportName: GridViewportName,
                                          filterState: FilterState): ViewportDisplay {
  const columnOrder = columnState[viewportName].order;
  return Array.from(columnOrder).reduce<ViewportDisplay>
  ((viewportColumnDisplay: ViewportDisplay, id: Identifier) => {
    const previousWidth = viewportColumnDisplay.totalWidth;
    const columnDefinition = columnState.entities[id];
    const columnDisplay: ColumnWithFilter = {
      ...columnDefinition,
      sort: Sort.NO_SORT
    };
    const sortedColumn = columnState.sortedColumns.find(col => col.id === columnDefinition.id);
    if (sortedColumn) {
      columnDisplay.sort = sortedColumn.sort;
    }
    if (columnDisplay.show) {
      columnDisplay.resizable = columnDefinition.resizable && columnDefinition.widthCalculationStrategy.isResizable;
      viewportColumnDisplay.totalWidth = previousWidth + columnDisplay.widthCalculationStrategy.width;
    }
    if (Boolean(columnDisplay.associateToFilter)) {
      columnDisplay.filter = filterState.filters.entities[columnDisplay.associateToFilter];
    }
    viewportColumnDisplay.columnDisplays.push(columnDisplay);
    return viewportColumnDisplay;
  }, {totalWidth: 0, columnDisplays: [], name: viewportName});
}
