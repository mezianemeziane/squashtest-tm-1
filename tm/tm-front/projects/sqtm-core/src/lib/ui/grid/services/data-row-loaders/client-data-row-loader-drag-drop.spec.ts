// import {DataRow} from '../../model/data-row.model';
// import {dataRowLoaderFactory} from '../../grid.service.provider';
// import {ClientDataRowLoader} from './client-data-row-loader';
// import {getDataRowDataset, getFlatGrid} from './data-row-dataset.spec';
// import {Identifier} from '../../../../model/entity.model';
// import {extractRootIds} from './data-row-utils';
// import {GridState, initialGridState} from '../../model/state/grid.state';
// import {tap} from 'rxjs/operators';
// import {createEntityAdapter} from '@ngrx/entity';
// import {grid} from '../../../../model/grids/grid-builders';
// import apply = Reflect.apply;
// import {async} from '@angular/core/testing';
//
// function getStateWithRowsLoaded() {
//   const gridState: GridState = initialGridState();
//   gridState.dataRowState = createEntityAdapter<DataRow>().addAll(getDataRowDataset(), gridState.dataRowState);
//   return gridState;
// }
//
// describe('Client Data Row Loader Should Perform Move', () => {
//
//   const gridConfig = grid('grid-test').build();
//   const restService = {};
//   const dataRowLoader: ClientDataRowLoader = apply(dataRowLoaderFactory(gridConfig), this, [restService]);
//
//   describe('Moving as sibling above target', () => {
//     interface DataType {
//       targetId: string;
//       draggedRowIds: string[];
//       dataRows: DataRow[];
//       expectedParent: Identifier;
//       expectedChildren: Identifier[];
//       expectedRoots: Identifier[];
//     }
//
//     const dataset: DataType[] = [
//       {
//         targetId: 'Execution-5',
//         draggedRowIds: ['Execution-3'],
//         dataRows: getFlatGrid(6),
//         expectedParent: undefined,
//         expectedChildren: [],
//         expectedRoots: ['Execution-0', 'Execution-1', 'Execution-2', 'Execution-4', 'Execution-3', 'Execution-5']
//       },
//       {
//         targetId: 'Execution-2',
//         draggedRowIds: ['Execution-3'],
//         dataRows: getFlatGrid(6),
//         expectedParent: undefined,
//         expectedChildren: [],
//         expectedRoots: ['Execution-0', 'Execution-1', 'Execution-3', 'Execution-2', 'Execution-4', 'Execution-5']
//       },
//       {
//         targetId: 'Execution-0',
//         draggedRowIds: ['Execution-3'],
//         dataRows: getFlatGrid(6),
//         expectedParent: undefined,
//         expectedChildren: [],
//         expectedRoots: ['Execution-3', 'Execution-0', 'Execution-1', 'Execution-2', 'Execution-4', 'Execution-5']
//       },
//       {
//         targetId: 'Execution-0',
//         draggedRowIds: ['Execution-3', 'Execution-1', 'Execution-5'],
//         dataRows: getFlatGrid(6),
//         expectedParent: undefined,
//         expectedChildren: [],
//         expectedRoots: ['Execution-3', 'Execution-1', 'Execution-5', 'Execution-0', 'Execution-2', 'Execution-4']
//       },
//       {
//         targetId: 'TestCaseFolder-2',
//         draggedRowIds: ['TestCaseFolder-3'],
//         dataRows: getDataRowDataset(),
//         expectedParent: 'TestCaseLibrary-1',
//         expectedChildren: ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-3', 'TestCaseFolder-2'],
//         expectedRoots: ['TestCaseLibrary-1', 'TestCaseLibrary-2']
//       },
//       {
//         targetId: 'TestCase-10',
//         draggedRowIds: ['TestCaseFolder-1'],
//         dataRows: getDataRowDataset(),
//         expectedParent: 'TestCaseLibrary-2',
//         expectedChildren: ['TestCaseFolder-1', 'TestCase-10', 'TestCaseFolder-11'],
//         expectedRoots: ['TestCaseLibrary-1', 'TestCaseLibrary-2']
//       },
//     ];
//
//     dataset.forEach((data, index) => runTest(data, index));
//
//     function runTest(data: DataType, index: number) {
//       it(`DataSet ${index} - Should move rows ${JSON.stringify(data.draggedRowIds)} above ${data.targetId}`, async(function () {
//         const gridState: GridState = initialGridState();
//         gridState.dataRowState.rootRowIds = extractRootIds(data.dataRows);
//         gridState.uiState.dragState.draggedRowIds = data.draggedRowIds;
//         gridState.dataRowState = createEntityAdapter<DataRow>().addAll(data.dataRows, gridState.dataRowState);
//
//         dataRowLoader.dropSelectedRowsAbove(data.targetId, gridState).subscribe((nextState) => {
//           expect(nextState.dataRowState.rootRowIds).toEqual(data.expectedRoots);
//           const entities = nextState.dataRowState.entities;
//           expect(nextState.dataRowState.ids.length).toEqual(data.dataRows.length);
//           const movedRows = data.draggedRowIds.map(id => entities[id]);
//           movedRows.forEach(row => expect(row.parentRowId).toEqual(data.expectedParent));
//           if (Boolean(data.expectedParent)) {
//             expect(entities[data.expectedParent].children).toEqual(data.expectedChildren);
//           }
//         });
//       }));
//     }
//   });
//
//   describe('Moving as sibling below target', () => {
//     interface DataType {
//       targetId: string;
//       draggedRowIds: string[];
//       dataRows: DataRow[];
//       expectedParent: Identifier;
//       expectedChildren: Identifier[];
//       expectedRoots: Identifier[];
//     }
//
//     const dataset: DataType[] = [
//       {
//         targetId: 'Execution-5',
//         draggedRowIds: ['Execution-3'],
//         dataRows: getFlatGrid(6),
//         expectedParent: undefined,
//         expectedChildren: [],
//         expectedRoots: ['Execution-0', 'Execution-1', 'Execution-2', 'Execution-4', 'Execution-5', 'Execution-3']
//       },
//       {
//         targetId: 'Execution-2',
//         draggedRowIds: ['Execution-3'],
//         dataRows: getFlatGrid(6),
//         expectedParent: undefined,
//         expectedChildren: [],
//         expectedRoots: ['Execution-0', 'Execution-1', 'Execution-2', 'Execution-3', 'Execution-4', 'Execution-5']
//       },
//       {
//         targetId: 'Execution-0',
//         draggedRowIds: ['Execution-3'],
//         dataRows: getFlatGrid(6),
//         expectedParent: undefined,
//         expectedChildren: [],
//         expectedRoots: ['Execution-0', 'Execution-3', 'Execution-1', 'Execution-2', 'Execution-4', 'Execution-5']
//       },
//       {
//         targetId: 'Execution-0',
//         draggedRowIds: ['Execution-3', 'Execution-1', 'Execution-5'],
//         dataRows: getFlatGrid(6),
//         expectedParent: undefined,
//         expectedChildren: [],
//         expectedRoots: ['Execution-0', 'Execution-3', 'Execution-1', 'Execution-5', 'Execution-2', 'Execution-4']
//       },
//       {
//         targetId: 'TestCaseFolder-2',
//         draggedRowIds: ['TestCaseFolder-3'],
//         dataRows: getDataRowDataset(),
//         expectedParent: 'TestCaseLibrary-1',
//         expectedChildren: ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2', 'TestCaseFolder-3'],
//         expectedRoots: ['TestCaseLibrary-1', 'TestCaseLibrary-2']
//       },
//       {
//         targetId: 'TestCase-10',
//         draggedRowIds: ['TestCaseFolder-1'],
//         dataRows: getDataRowDataset(),
//         expectedParent: 'TestCaseLibrary-2',
//         expectedChildren: ['TestCase-10', 'TestCaseFolder-1', 'TestCaseFolder-11'],
//         expectedRoots: ['TestCaseLibrary-1', 'TestCaseLibrary-2']
//       },
//     ];
//
//     dataset.forEach((data, index) => runTest(data, index));
//
//     function runTest(data: DataType, index: number) {
//       it(`DataSet ${index} - Should move rows ${JSON.stringify(data.draggedRowIds)} below ${data.targetId}`, function (done) {
//         const gridState: GridState = initialGridState();
//         gridState.dataRowState = createEntityAdapter<DataRow>().addAll(data.dataRows, gridState.dataRowState);
//         gridState.dataRowState.rootRowIds = extractRootIds(data.dataRows);
//         gridState.uiState.dragState.draggedRowIds = data.draggedRowIds;
//
//         dataRowLoader.dropSelectedRowsBelow(data.targetId, gridState).pipe(
//           tap(nextState => {
//             expect(nextState.dataRowState.rootRowIds).toEqual(data.expectedRoots);
//             const entities = nextState.dataRowState.entities;
//             expect(nextState.dataRowState.ids.length).toEqual(data.dataRows.length);
//             const movedRows = data.draggedRowIds.map(id => entities[id]);
//             movedRows.forEach(row => expect(row.parentRowId).toEqual(data.expectedParent));
//             if (Boolean(data.expectedParent)) {
//               expect(entities[data.expectedParent].children).toEqual(data.expectedChildren);
//             }
//           })
//         ).subscribe(() => done());
//       });
//     }
//   });
//
//   describe('Moving into target', () => {
//     interface DataType {
//       targetId: string;
//       draggedRowIds: string[];
//       dataRows: DataRow[];
//       expectedParent: Identifier;
//       expectedChildren: Identifier[];
//       expectedRoots: Identifier[];
//     }
//
//     const dataset: DataType[] = [
//       {
//         targetId: 'TestCaseFolder-2',
//         draggedRowIds: ['TestCaseFolder-3'],
//         dataRows: getDataRowDataset(),
//         expectedParent: 'TestCaseFolder-2',
//         expectedChildren: ['TestCaseFolder-3'],
//         expectedRoots: ['TestCaseLibrary-1', 'TestCaseLibrary-2']
//       },
//       {
//         targetId: 'TestCaseFolder-3',
//         draggedRowIds: ['TestCase-10', 'TestCaseFolder-11', 'TestCase-12'],
//         dataRows: getDataRowDataset(),
//         expectedParent: 'TestCaseFolder-3',
//         expectedChildren: ['TestCaseFolder-7', 'TestCase-8', 'TestCase-10', 'TestCaseFolder-11', 'TestCase-12'],
//         expectedRoots: ['TestCaseLibrary-1', 'TestCaseLibrary-2']
//       },
//     ];
//
//     dataset.forEach((data, index) => runTest(data, index));
//
//     function runTest(data: DataType, index: number) {
//       it(`DataSet ${index} - Should move rows ${JSON.stringify(data.draggedRowIds)} below ${data.targetId}`, function (done) {
//         const gridState: GridState = initialGridState();
//         gridState.dataRowState = createEntityAdapter<DataRow>().addAll(data.dataRows, gridState.dataRowState);
//         gridState.dataRowState.rootRowIds = extractRootIds(data.dataRows);
//         gridState.uiState.dragState.draggedRowIds = data.draggedRowIds;
//
//         dataRowLoader.dropSelectedRowsInto(data.targetId, gridState).pipe(
//           tap(nextState => {
//             expect(nextState.dataRowState.rootRowIds).toEqual(data.expectedRoots);
//             const entities = nextState.dataRowState.entities;
//             expect(nextState.dataRowState.ids.length).toEqual(data.dataRows.length);
//             const movedRows = data.draggedRowIds.map(id => entities[id]);
//             movedRows.forEach(row => expect(row.parentRowId).toEqual(data.expectedParent));
//             if (Boolean(data.expectedParent)) {
//               expect(entities[data.expectedParent].children).toEqual(data.expectedChildren);
//             }
//           })
//         ).subscribe(() => done());
//       });
//     }
//   });
//
// });
//
