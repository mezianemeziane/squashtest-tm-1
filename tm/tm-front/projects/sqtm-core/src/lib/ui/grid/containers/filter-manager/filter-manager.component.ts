import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {GridService} from '../../services/grid.service';
import {Observable, Subject} from 'rxjs';
import {ColumnWithFilter} from '../../model/column-display.model';
import {GridFilter, selectFilterData} from '../../model/state/filter.state';
import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {ComponentPortal} from '@angular/cdk/portal';
import {SelectFiltersComponent} from '../../components/select-filters/select-filters.component';
import {takeUntil} from 'rxjs/operators';
import {select} from '@ngrx/store';
import {Filter, FilterGroup, FilteringChange, Scope} from '../../../filters/state/filter.state';

@Component({
  selector: 'sqtm-core-filter-manager',
  template: `
    <ng-container *ngIf="grid.gridDisplay$ | async as gridDisplay">
      <div class="full-height flex-column grid-filter-manager" *ngIf="filterData$|async as filterData">
        <div class="flex-fixed-size full-width flex-row">
          <button nz-button nzType="primary"
                  (click)="validateFilters()">{{'sqtm-core.generic.label.research' | translate | capitalize}}</button>
          <div
            class="flex-fixed-size reset-link current-workspace-main-color __hover_pointer __hover_current_ws_border m-r-20"
            style="margin-left: auto"
            [attr.data-test-component-id]="'grid-filter-manager-reset-filters'"
            (click)="resetFilters()">
            <span>{{'sqtm-core.search.generic.new'|translate}}</span>
          </div>
        </div>
        <div #addFilter
             class="flex-fixed-size criteria-link current-workspace-main-color m-t-20 __hover_pointer __hover_current_ws_border"
             [class.current-workspace-border-color]="openSelectFilterMenu"
             [attr.data-test-component-id]="'grid-filter-manager-add-criteria'"
             (click)="showAvailableFilters()">
          <i nz-icon nzType="plus"></i><span
          class="m-l-8">{{'sqtm-core.search.generic.add-criteria'|translate}}</span>
        </div>
        <div class="criteria-scroller">
          <div class="m-t-10" *ngIf="filterData.scope.active">
            <sqtm-core-scope [scope]="filterData.scope"
                             [allowCustomScope]="gridDisplay.scopeDefinition.allowCustomScope"
                             [customScopeKind]="gridDisplay.scopeDefinition.customScopeKind"
                             (valueChanged)="changeScope($event)"></sqtm-core-scope>
          </div>
          <div class="m-t-10">
            <ng-container *ngFor="let filter of filterData.filters; trackBy:trackByFilterId">
              <sqtm-core-filter-field class="m-t-5" *ngIf="filter.active || filter.alwaysActive"
                                      [filterData]="{filter:filter, scope:filterData.scope}"
                                      [allowDelete]="!filter.alwaysActive"
                                      [preventOpening]="filter.preventOpening"
                                      [lockedValue]="filter.lockedValue"
                                      [filterGroups]="filterData.filterGroups"
                                      (valueChanged)="changeFilterValue($event, filter)"
                                      (filterInactivated)="inactivateFilter(filter.id)"
              ></sqtm-core-filter-field>
            </ng-container>
          </div>
        </div>
      </div>
    </ng-container>
  `,
  styleUrls: ['./filter-manager.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterManagerComponent implements OnInit, OnDestroy {

  @ViewChild('addFilter', {read: ElementRef})
  addFilterRef: ElementRef;

  private unsub$ = new Subject<void>();
  private overlayRef: OverlayRef;
  openSelectFilterMenu = false;
  filterData$: Observable<{ filters: GridFilter[]; scope: Scope; filterGroups: FilterGroup[] }>;


  constructor(public grid: GridService, private overlay: Overlay, private vcr: ViewContainerRef, private cdRef: ChangeDetectorRef) {
    this.filterData$ = this.grid.gridState$.pipe(
      takeUntil(this.unsub$),
      select(selectFilterData)
    );
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  trackByColumnId(column: ColumnWithFilter) {
    return column.id;
  }

  validateFilters() {
    this.grid.refreshData();
  }

  changeFilterValue(change: FilteringChange, filter: GridFilter) {
    this.grid.changeFilterValue({id: filter.id, ...change});
  }

  showAvailableFilters() {
    this.openSelectFilterMenu = true;
    this.cdRef.detectChanges();
    const positionStrategy = this.overlay.position().flexibleConnectedTo(this.addFilterRef)
      .withPositions([
        {originX: 'center', overlayX: 'center', originY: 'bottom', overlayY: 'top', offsetY: 10},
        {originX: 'center', overlayX: 'center', originY: 'top', overlayY: 'bottom', offsetY: -10},
      ]);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const componentPortal = new ComponentPortal(SelectFiltersComponent, this.vcr);
    const componentComponentRef = this.overlayRef.attach(componentPortal);
    componentComponentRef.instance.activateFilter$.pipe(
      takeUntil(this.unsub$)
    ).subscribe(() => this.close());
    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  close() {
    if (this.overlayRef) {
      this.openSelectFilterMenu = false;
      this.overlayRef.dispose();
      this.cdRef.detectChanges();
    }
  }

  trackByFilterId(index: number, filter: Filter) {
    return filter.id;
  }

  inactivateFilter(id: string) {
    this.grid.inactivateFilter(id);
  }

  resetFilters() {
    this.grid.resetFilters();
  }

  changeScope($event: Scope) {
    this.grid.changeScope($event);
  }
}
