import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  NgZone,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import {BehaviorSubject, combineLatest, fromEvent, Observable, Subject} from 'rxjs';
import {distinctUntilChanged, map, shareReplay, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {ListRange} from '@angular/cdk/collections';
import {VirtualScrollDirective} from '../../directives/virtual-scroll.directive';
import {GridViewportName} from '../../model/state/column.state';
import {ScrollBarMeasurerService} from '../../../../core/services/scroll-bar-measurer.service';
import {GridNode} from '../../model/grid-node.model';
import {PaginationDisplay} from '../../model/pagination-display.model';
import * as CssElementQuery from 'css-element-queries';
import {GridDisplay, ViewportDisplay} from '../../model/grid-display.model';
import {GridService} from '../../services/grid.service';
import {GridType} from '../../model/grid-definition.model';
import {gridLogger} from '../../grid.logger';
import {GridDndData} from '../../model/drag-and-drop/grid-dnd-data';
import {
  SqtmDragCancelEvent,
  SqtmDragLeaveEvent,
  SqtmDragOverEvent,
  SqtmDragStartEvent,
  SqtmDropEvent
} from '../../../drag-and-drop/events';
import {GridViewportService, RenderedGridViewport} from '../../services/grid-viewport.service';

const logger = gridLogger.compose('GridViewPortComponent');

@Component({
  selector: 'sqtm-core-grid-viewport',
  templateUrl: './grid-viewport.component.html',
  styleUrls: ['./grid-viewport.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridViewportComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('horizontalScroll', {read: ElementRef})
  horizontalScroll: ElementRef;

  @ViewChild('horizontalScrollContent', {read: ElementRef})
  horizontalScrollContent: ElementRef;

  @ViewChild('leftViewport', {read: ElementRef})
  leftViewport: ElementRef;

  @ViewChild('centerClipper', {read: ElementRef})
  centerClipper: ElementRef;

  @ViewChild('centerViewport', {read: ElementRef})
  centerViewport: ElementRef;

  @ViewChild('rightViewport', {read: ElementRef})
  rightViewport: ElementRef;

  @ViewChild('centerRowContainer', {read: ElementRef})
  centerRowContainer: ElementRef;

  @ViewChild('leftHeaders', {read: ElementRef})
  leftHeaders: ElementRef;

  @ViewChild('centerHeaders', {read: ElementRef})
  centerHeaders: ElementRef;

  @ViewChild('rightHeaders', {read: ElementRef})
  rightHeaders: ElementRef;

  @ViewChild('headerScrollPlaceholder', {read: ElementRef})
  headerScrollPlaceholder: ElementRef;

  @ViewChild('gridViewport', {read: ElementRef})
  gridViewport: ElementRef;

  @ViewChild('gridViewportWrapper', {read: ElementRef, static: true})
  gridViewportWrapper: ElementRef;

  virtualScrolls: VirtualScrollDirective[] = [];

  private viewportTopScroll: BehaviorSubject<number> = new BehaviorSubject(0);
  private rangeToRender: BehaviorSubject<ListRange> = new BehaviorSubject({start: 0, end: 0});
  rangeToRender$: Observable<ListRange> = this.rangeToRender.asObservable().pipe(
    distinctUntilChanged((x, y) => {
      return x.start === y.start && x.end === y.end;
    }),
    shareReplay(1)
  );

  private viewportLeftScroll: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  private resizeSubject: Subject<ResizeEvent> = new Subject();
  private resize$ = this.resizeSubject.pipe(
    // debounceTime(30)
  );

  gridNodes$: Observable<GridNode[]>;
  gridDisplay$: Observable<GridDisplay>;
  private unsub$ = new Subject<void>();
  private clipperResizeSensor: CssElementQuery.ResizeSensor;
  private viewportResizeSensor: CssElementQuery.ResizeSensor;

  public globalAsyncOperationRunning$: Observable<boolean>;

  constructor(private grid: GridService, private cdRef: ChangeDetectorRef, private ngZone: NgZone,
              private scrollBarMeasurer: ScrollBarMeasurerService, private renderer: Renderer2,
              private gridViewportService: GridViewportService) {

    this.gridNodes$ = this.grid.gridNodes$.pipe(
      takeUntil(this.unsub$)
    );

    this.gridDisplay$ = this.grid.gridDisplay$.pipe(
      takeUntil(this.unsub$)
    );
  }

  ngOnInit(): void {
    this.globalAsyncOperationRunning$ = this.grid.globalAsyncOperationRunning$.pipe(takeUntil(this.unsub$));
  }

  private hasVerticalOverflow(): boolean {
    const nativeElement = this.gridViewport.nativeElement;
    return nativeElement.scrollHeight - nativeElement.clientHeight > 0;
  }

  ngAfterViewInit(): void {
    this.initResizeSensor();
    this.ngZone.runOutsideAngular(() => {
      fromEvent(this.gridViewport.nativeElement, 'scroll')
        .pipe(
          withLatestFrom(this.grid.paginationDisplay$, this.grid.gridDisplay$),
          takeUntil(this.unsub$)
        ).subscribe(([$event, paginationDisplay, gridDisplay]: [MouseEvent, PaginationDisplay, GridDisplay]) => {
        const scrollTop: number = $event.target['scrollTop'];
        this.viewportTopScroll.next(scrollTop);
      });

      fromEvent(this.horizontalScroll.nativeElement, 'scroll')
        .pipe(
          takeUntil(this.unsub$)
        )
        .subscribe((event: MouseEvent) => {
          const nativeElement = this.centerViewport.nativeElement as HTMLElement;
          const left = event.target['scrollLeft'];
          nativeElement.scrollTo({left: left});
          const nativeHeaderElement = this.centerHeaders.nativeElement as HTMLElement;
          nativeHeaderElement.scrollTo({left: left});
          this.viewportLeftScroll.next(left);
        });

      this.gridViewportService.renderedGridViewport$.pipe(
        takeUntil(this.unsub$)
      ).subscribe(renderedGridViewport => {
        this.handleHorizontalScrollBarWidth(renderedGridViewport);
        if (this.hasVerticalOverflow()) {
          this.renderer.setStyle(this.headerScrollPlaceholder.nativeElement, 'display', 'block');
          this.renderer.setStyle(this.headerScrollPlaceholder.nativeElement, 'width', `${this.scrollBarMeasurer.scrollBarWidth}px`);
        } else {
          this.renderer.setStyle(this.headerScrollPlaceholder.nativeElement, 'display', 'none');
        }
      });

      combineLatest([this.gridViewportService.renderedGridViewport$, this.viewportLeftScroll]).pipe(
        takeUntil(this.unsub$),
      ).subscribe(([renderedGridViewport, scrollLeft]) => {
        this.handleLeftViewportShadow(renderedGridViewport, scrollLeft);
        this.handleRightViewportShadow(renderedGridViewport, scrollLeft);
      });
    });


    this.rangeToRender$.subscribe((range: ListRange) => {
      // console.log(`we must update rendered items `);
      this.ngZone.run(() => {
        // console.log(`Updating ${this.virtualScrolls.length} viewports`);
        this.virtualScrolls.forEach(scroll => {
          // console.log(`Should update viewport`);
          scroll.sqtmCoreVirtualScrollForRangeToRender = range;
        });
        this.cdRef.detectChanges();
      });
    });

    combineLatest([this.viewportTopScroll, this.grid.paginationDisplay$, this.gridDisplay$]).pipe(
      takeUntil(this.unsub$)
    ).subscribe(([scrollTop, paginationDisplay, gridDisplay]) => {
      this._updateRangeToRender(scrollTop, paginationDisplay, gridDisplay);
    });

    this.resize$.pipe(
      map(resize => resize.height),
      distinctUntilChanged(),
      withLatestFrom(this.viewportTopScroll, this.grid.paginationDisplay$, this.gridDisplay$)
    ).subscribe(([, scrollTop, paginationDisplay, gridDisplay]) => {
      this._updateRangeToRender(scrollTop, paginationDisplay, gridDisplay);
    });

  }

  private handleHorizontalScrollBarWidth(renderedGridViewport: RenderedGridViewport) {
    const hasHorizontalOverflow = this.hasHorizontalOverflow(renderedGridViewport);
    if (hasHorizontalOverflow) {
      let totalWidth = renderedGridViewport.mainViewport.totalWidth;
      if (!this.hasVerticalOverflow()) {
        totalWidth -= this.scrollBarMeasurer.scrollBarWidth;
      }
      this.renderer.setStyle(this.horizontalScrollContent.nativeElement, 'width', `${totalWidth}px`);
    } else {
      this.renderer.setStyle(this.horizontalScrollContent.nativeElement, 'width', `0px`);
    }
  }

  private handleRightViewportShadow(renderedGridViewport: RenderedGridViewport, scrollLeft: number) {
    const scrollLeftIsMax = renderedGridViewport.mainViewport.viewportWidth + scrollLeft
      === renderedGridViewport.mainViewport.totalWidth;
    const mustShowRightViewPortShadow = this.hasHorizontalOverflow(renderedGridViewport) && !scrollLeftIsMax;
    if (mustShowRightViewPortShadow) {
      this.renderer.setStyle(this.rightViewport.nativeElement, 'box-shadow', '-8px 0 8px -10px #000000');
      this.renderer.setStyle(this.rightHeaders.nativeElement, 'box-shadow', '-8px 0 8px -10px #000000');
    } else {
      this.renderer.setStyle(this.rightViewport.nativeElement, 'box-shadow', '0 0 0 0');
      this.renderer.setStyle(this.rightHeaders.nativeElement, 'box-shadow', '0 0 0 0');
    }
  }

  private handleLeftViewportShadow(renderedGridViewport: RenderedGridViewport, scrollLeft: number) {
    const mustShowLeftViewPortShadow = this.hasHorizontalOverflow(renderedGridViewport) && scrollLeft > 0;
    if (mustShowLeftViewPortShadow) {
      this.renderer.setStyle(this.leftViewport.nativeElement, 'box-shadow', '8px 0 8px -10px #000000');
      this.renderer.setStyle(this.leftHeaders.nativeElement, 'box-shadow', '8px 0 8px -10px #000000');
    } else {
      this.renderer.setStyle(this.leftViewport.nativeElement, 'box-shadow', '0 0 0 0');
      this.renderer.setStyle(this.leftHeaders.nativeElement, 'box-shadow', '0 0 0 0');
    }
  }

  private hasHorizontalOverflow(renderedGridViewport: RenderedGridViewport) {
    return renderedGridViewport.mainViewport.totalWidth > this.centerViewport.nativeElement.clientWidth;
  }

  private initResizeSensor() {
    const that = this;
    this.clipperResizeSensor = new CssElementQuery.ResizeSensor(this.centerClipper.nativeElement, function ($event: ResizeEvent) {
      if (logger.isTraceEnabled()) {
        logger.trace(`Resize event detected on clipper: `, [$event]);
      }
      that.resizeSubject.next($event);
      that.gridViewportService.notifyContainerViewPortResize({
        ...$event,
        hasVerticalOverflow: that.hasVerticalOverflow()
      });
    });

    this.viewportResizeSensor = new CssElementQuery.ResizeSensor(this.gridViewport.nativeElement, function ($event: ResizeEvent) {
      if (logger.isTraceEnabled()) {
        logger.trace(`Resize event detected on viewport: `, [$event]);
      }
      that.resizeSubject.next($event);
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    if (this.clipperResizeSensor) {
      this.clipperResizeSensor.detach();
    }
    if (this.viewportResizeSensor) {
      this.viewportResizeSensor.detach();
    }
    // completing all subjects
    this.resizeSubject.complete();
    this.viewportTopScroll.complete();
    this.viewportLeftScroll.complete();
    this.rangeToRender.complete();
  }

  private _updateRangeToRender(scrollTop: number, paginationDisplay: PaginationDisplay, gridDisplay: GridDisplay) {
    this.ngZone.run(() => {
      if (gridDisplay.gridType === GridType.TABLE && paginationDisplay.active && paginationDisplay.size !== -1) {
        this.rangeToRender.next({start: 0, end: paginationDisplay.size});
      } else {
        const height = this.gridViewport.nativeElement['clientHeight'];
        const firstItem = Math.floor(scrollTop / gridDisplay.rowHeight);
        const start = Math.max(0, (firstItem - 4));
        const end = firstItem + Math.floor(height / gridDisplay.rowHeight) + 5;
        logger.trace(`scroll to ${scrollTop}px. Items :  ${start} to ${end}`);
        this.rangeToRender.next({start, end});
      }
    });
  }

  getRowStyle(row: GridNode, gridDisplay: GridDisplay) {
    if (gridDisplay.gridType !== GridType.TABLE) {
      return {
        position: 'absolute',
        transform: `translateY(${row.index * gridDisplay.rowHeight}px)`
      };
    }
    return {};
  }

  calculateViewportHeight(rows: any[], gridDisplay: GridDisplay): string {
    return gridDisplay.gridType === GridType.TABLE ? '' : `${rows.length * gridDisplay.rowHeight}px`;
  }

  trackByRowId(index: number, row: GridNode) {
    return row.id;
  }

  calculateRowHeight(gridDisplay: GridDisplay): string {
    return `${gridDisplay.rowHeight}px`;
  }

  calculateViewportWidth(viewportName: GridViewportName, gridDisplay: GridDisplay) {
    const viewportDisplay = gridDisplay[viewportName];
    return `${viewportDisplay.totalWidth}px`;
  }

  calculateMainViewportWidth(gridDisplay: GridDisplay) {
    const totalWidth = gridDisplay.mainViewport.totalWidth;
    return `${totalWidth}px`;
  }

  attachVirtualScroll(scroll: VirtualScrollDirective) {
    this.virtualScrolls.push(scroll);
  }

  calculateHeadersHeight(gridDisplay: GridDisplay) {
    return `${gridDisplay.rowHeight + 2}px`;
  }

  computeHeadersDisplay(gridDisplay: GridDisplay): string {
    return gridDisplay.enableHeaders ? 'flex' : 'none';
  }

  getScrollBarHeight() {
    // + 1px for chrome that is not able to show the bar if height === sizeBarWidth
    return `${this.scrollBarMeasurer.scrollBarWidth + 1}px`;
  }

  getScrollBarWidth() {
    return `${this.scrollBarMeasurer.scrollBarWidth}px`;
  }

  handleDragStart($event: SqtmDragStartEvent) {
    const gridDndData = $event.dragAndDropData.data as GridDndData;
    this.grid.startDrag(gridDndData.dataRows.map(row => row.id));
  }

  handleDragOver($event: SqtmDragOverEvent) {
    this.ngZone.run(() => {
      this.gridDisplay$.pipe(
        take(1),
      ).subscribe((gridDisplay) => {
        if ($event.dragAndDropData.origin === gridDisplay.id) {
          this.grid.notifyDragOver($event.dndTarget);
        }
      });
    });
  }

  handleDrop($event: SqtmDropEvent) {
    this.gridDisplay$.pipe(
      take(1),
    ).subscribe((gridDisplay) => {
      if ($event.dragAndDropData.origin === gridDisplay.id) {
        this.grid.notifyInternalDrop();
      }
    });
  }

  handleDragLeave($event: SqtmDragLeaveEvent) {
    this.gridDisplay$.pipe(
      take(1),
    ).subscribe((gridDisplay) => {
      if ($event.dragAndDropData.origin === gridDisplay.id) {
        this.grid.suspendDrag();
      }
    });
  }

  handleDragCancel($event: SqtmDragCancelEvent) {
    this.gridDisplay$.pipe(
      take(1),
    ).subscribe((gridDisplay) => {
      if ($event.dragAndDropData.origin === gridDisplay.id) {
        this.grid.cancelDrag();
      }
    });
  }
}

interface ResizeEvent {
  width: number;
  height: number;
}
