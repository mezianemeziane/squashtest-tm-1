import {ColumnDisplay, ColumnWithFilter} from './column-display.model';
import {GridService} from '../services/grid.service';
import {GridDisplay} from './grid-display.model';
import {DataRow} from './data-row.model';
import {ChangeDetectorRef} from '@angular/core';


export interface GridElementRenderer {
  columnDisplay: ColumnWithFilter;
  gridDisplay: GridDisplay;
  cdRef: ChangeDetectorRef;
  grid: GridService;
}

export interface CellRenderer extends GridElementRenderer {
  index: number;
  row: DataRow;
  depth: number;
  selected: boolean;
  showAsFilteredParent: boolean;
  isLast: boolean;
  depthMap: boolean[];
}

export interface HeaderRenderer extends GridElementRenderer {
  viewportName: string;

}
