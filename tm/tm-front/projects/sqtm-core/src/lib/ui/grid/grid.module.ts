import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GridComponent} from './containers/grid/grid.component';
import {PaginationSizeSelectorComponent} from './components/pagination-size-selector/pagination-size-selector.component';
import {GridRowComponent} from './components/row-renderers/grid-row/grid-row.component';
import {TextCellRendererComponent} from './components/cell-renderers/text-cell-renderer/text-cell-renderer.component';
import {TranslateModule} from '@ngx-translate/core';
import {CheckBoxCellRendererComponent} from './components/cell-renderers/check-box-cell-renderer/check-box-cell-renderer.component';
import {GridViewportDirective} from './directives/grid-viewport.directive';
import {LevelEnumCellRendererComponent} from './components/cell-renderers/level-enum-cell-renderer/level-enum-cell-renderer.component';
import {ColumnManagerComponent} from './containers/column-manager/column-manager.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {EditableTextCellRendererComponent} from './components/cell-renderers/editable-text-cell-renderer/editable-text-cell-renderer.component';
import {SideBarNavigatorComponent} from './components/side-bar-navigator/side-bar-navigator.component';
import {SelectableDirective} from './directives/selectable.directive';
import {WorkspaceCommonModule} from '../workspace-common/workspace-common.module';
import {GridFilterHeaderDirective} from './directives/grid-filter-header.directive';
import {GridFooterComponent} from './containers/grid-footer/grid-footer.component';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {GridViewportComponent} from './containers/grid-viewport/grid-viewport.component';
import {VirtualScrollDirective} from './directives/virtual-scroll.directive';
import {GridHeaderRendererComponent} from './components/header-renderers/grid-header-renderer/grid-header-renderer.component';
import {GridHeaderRowComponent} from './containers/grid-header-row/grid-header-row.component';
import {FilterManagerComponent} from './containers/filter-manager/filter-manager.component';
import {PortalModule} from '@angular/cdk/portal';
import {ConfigurationManagerComponent} from './containers/configuration-manager/configuration-manager.component';
import {ConfigurationManagerDialogComponent} from './components/configuration-manager-dialog/configuration-manager-dialog.component';
import {EditableRichTextRendererComponent} from './components/cell-renderers/editable-rich-text-renderer/editable-rich-text-renderer.component';
import {GridHeaderDirective} from './directives/grid-header.directive';
import {IconHeaderRendererComponent} from './components/header-renderers/icon-header-renderer/icon-header-renderer.component';
import {IndexCellRendererComponent} from './components/cell-renderers/index-cell-renderer/index-cell-renderer.component';
import {RouterModule} from '@angular/router';
import {CustomFieldCheckBoxRendererComponent} from './components/cell-renderers/custom-field-renderers/custom-field-check-box-renderer/custom-field-check-box-renderer.component';
import {EditableDateCellRendererComponent} from './components/cell-renderers/editable-date-cell-renderer/editable-date-cell-renderer.component';
import {EditableNumericCellRendererComponent} from './components/cell-renderers/editable-numeric-cell-renderer/editable-numeric-cell-renderer.component';
import {CustomFieldModule} from '../custom-field/custom-field.module';
import {GridRowWrapperComponent} from './containers/grid-row-wrapper/grid-row-wrapper.component';
import {EmptyGridMessageComponent} from './components/row-renderers/empty-grid-message/empty-grid-message.component';
import {GridCellComponent} from './containers/grid-cell/grid-cell.component';
import {SqtmDragAndDropModule} from '../drag-and-drop/sqtm-drag-and-drop.module';
import {DragAndDropPlaceholderComponent} from './components/row-renderers/drag-and-drop-placeholder/drag-and-drop-placeholder.component';
import {DefaultGridDraggedContentComponent} from './components/dragged-content/default-grid-dragged-content/default-grid-dragged-content.component';
import {
  DateCellRendererComponent,
  DateTimeCellRendererComponent
} from './components/cell-renderers/date-cell-renderer/date-cell-renderer.component';
import {ToggleSelectionHeaderRendererComponent} from './components/header-renderers/toggle-selection-header-renderer/toggle-selection-header-renderer.component';
import {RichTextRendererComponent} from './components/cell-renderers/rich-text-renderer/rich-text-renderer.component';
import {DialogModule} from '../dialog/dialog.module';
import {NumericCellRendererComponent} from './components/cell-renderers/numeric-cell-renderer/numeric-cell-renderer.component';
import {FiltersModule} from '../filters/filters.module';
import {BooleanCellRendererComponent} from './components/cell-renderers/boolean-cell-renderer/boolean-cell-renderer.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {SelectFiltersComponent} from './components/select-filters/select-filters.component';
import {ScopeComponent} from './components/scope/scope/scope.component';
import {ProjectScopeComponent} from './components/scope/project-scope/project-scope.component';
import {TestCasePickerComponent} from './components/pickers/test-case-picker/test-case-picker.component';
import {RequirementPickerComponent} from './components/pickers/requirement-picker/requirement-picker.component';
import {DummyCellComponent} from './components/cell-renderers/dummy-cell/dummy-cell.component';
import {RadioCellRendererComponent} from './components/cell-renderers/radio-cell-renderer/radio-cell-renderer.component';
import {SelectableTextCellRendererComponent} from './components/cell-renderers/selectable-text-cell-renderer/selectable-text-cell-renderer.component';
import {GridHoverRowDirective} from './directives/grid-hover-row.directive';
import {SelectableNumericCellRendererComponent} from './components/cell-renderers/selectable-numeric-cell-renderer/selectable-numeric-cell-renderer.component';
import {ProjectPickerComponent} from './components/pickers/project-picker/project-picker.component';
import {CampaignLimitedPickerComponent} from './components/pickers/campaign-limited-picker/campaign-limited-picker.component';
import {DefaultTreeDraggedContentComponent} from './components/dragged-content/default-tree-dragged-content/default-tree-dragged-content.component';
import {CampaignPickerComponent} from './components/pickers/campaign-picker/campaign-picker.component';
import { IterationPickerComponent } from './components/pickers/iteration-picker/iteration-picker.component';
import { RestrictedCampaignPickerComponent } from './components/pickers/restricted-campaign-picker/restricted-campaign-picker.component';
import {ResetFiltersLinkComponent} from './components/reset-filters-link/reset-filters-link.component';


@NgModule({
  declarations: [
    GridComponent,
    PaginationSizeSelectorComponent,
    GridRowComponent,
    TextCellRendererComponent,
    GridViewportDirective,
    CheckBoxCellRendererComponent,
    LevelEnumCellRendererComponent,
    ColumnManagerComponent,
    EditableTextCellRendererComponent,
    SideBarNavigatorComponent,
    SelectableDirective,
    GridFilterHeaderDirective,
    GridFooterComponent,
    VirtualScrollDirective,
    GridViewportComponent,
    GridHeaderRendererComponent,
    GridHeaderRowComponent,
    FilterManagerComponent,
    ConfigurationManagerComponent,
    ConfigurationManagerDialogComponent,
    GridHeaderDirective,
    GridHeaderRendererComponent,
    IconHeaderRendererComponent,
    IndexCellRendererComponent,
    EditableRichTextRendererComponent,
    CustomFieldCheckBoxRendererComponent,
    EditableDateCellRendererComponent,
    EditableNumericCellRendererComponent,
    GridRowWrapperComponent,
    EmptyGridMessageComponent,
    GridCellComponent,
    DragAndDropPlaceholderComponent,
    DefaultGridDraggedContentComponent,
    DefaultTreeDraggedContentComponent,
    DateCellRendererComponent,
    ToggleSelectionHeaderRendererComponent,
    RichTextRendererComponent,
    NumericCellRendererComponent,
    DateTimeCellRendererComponent,
    BooleanCellRendererComponent,
    SelectFiltersComponent,
    ScopeComponent,
    ProjectScopeComponent,
    TestCasePickerComponent,
    RequirementPickerComponent,
    DummyCellComponent,
    RadioCellRendererComponent,
    SelectableTextCellRendererComponent,
    GridHoverRowDirective,
    SelectableNumericCellRendererComponent,
    ProjectPickerComponent,
    CampaignLimitedPickerComponent,
    CampaignPickerComponent,
    IterationPickerComponent,
    RestrictedCampaignPickerComponent,
    ResetFiltersLinkComponent
  ],
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    DragDropModule,
    WorkspaceCommonModule,
    NzButtonModule,
    NzIconModule,
    NzSelectModule,
    FormsModule,
    ReactiveFormsModule,
    ScrollingModule,
    PortalModule,
    NzMenuModule,
    NzTagModule,
    RouterModule,
    NzCheckboxModule,
    CustomFieldModule,
    SqtmDragAndDropModule,
    NzToolTipModule,
    DialogModule,
    NzInputModule,
    FiltersModule,
    OverlayModule,
    NzTabsModule
  ],
    exports: [
        GridComponent,
        SelectableDirective,
        CustomFieldCheckBoxRendererComponent,
        TestCasePickerComponent,
        RequirementPickerComponent,
        TextCellRendererComponent,
        CheckBoxCellRendererComponent,
        GridHeaderRendererComponent,
        GridCellComponent,
        GridHoverRowDirective,
        ProjectPickerComponent,
        CampaignLimitedPickerComponent,
        CampaignPickerComponent,
        IterationPickerComponent,
        RestrictedCampaignPickerComponent,
        ResetFiltersLinkComponent,
    ]
})
export class GridModule {

  constructor() {
  }
}
