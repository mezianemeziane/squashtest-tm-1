import {AbstractFilterManager} from './abstract-filter-manager';
import {GridState} from '../../model/state/grid.state';
import {Observable, of} from 'rxjs';
import {GridType} from '../../model/grid-definition.model';

export class ServerFilterManager extends AbstractFilterManager {

  applyFilters(state: GridState): Observable<GridState> {
    if (state.definitionState.gridType === GridType.TREE) {
      return super.applyFilters(state);
    }
    // NOOP, all filters operations are done server side by the refresh method.
    return of(state);
  }

  applyMultiColumnsFilter(state: GridState, value: any): Observable<GridState> {
    return of(state);
  }
}
