import {AbstractColumnDefinitionManager} from './abstract-column-definition.manager';
import {ColumnDefinitionManager} from './column-definition-manager';
import {ColumnDefinition, SortedColumn} from '../../model/column-definition.model';
import {GridState} from '../../model/state/grid.state';
import {Observable} from 'rxjs';
import {GridViewportName} from '../../model/state/column.state';
import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';
import {map, take} from 'rxjs/operators';
import {ProjectData} from '../../../../model/project/project-data.model';
import {CustomField} from '../../../../model/customfield/customfield.model';
import {textColumn} from '../../model/column-definition.builder';
import {BindableEntity} from '../../../../model/bindable-entity.model';
import {Identifier} from '../../../../model/entity.model';

export class DynamicColumnDefinitionManagerService extends AbstractColumnDefinitionManager implements ColumnDefinitionManager {

  constructor(private referentialDataService: ReferentialDataService) {
    super();
  }

  public initializeColumns(columnDefinitions: ColumnDefinition[],
                           state: GridState,
                           initialSortedColumns: SortedColumn[],
                           bindableEntity: BindableEntity,
                           shouldResetSorts: boolean):
    Observable<GridState> {
    return this.generateCustomFieldColumns(bindableEntity).pipe(
      map(cufColumns => {
        const allColumns = [...columnDefinitions, ...cufColumns];
        const columnState = state.columnState;
        const mainViewport = {...columnState.mainViewport};
        mainViewport.order = allColumns.map(colDef => colDef.id);
        columnState.mainViewport = mainViewport;
        columnState.sortedColumns = initialSortedColumns;
        columnState.shouldResetSorts = shouldResetSorts;
        return {...state, columnState: this.adapter.setAll(allColumns, columnState)};
      })
    );
  }

  public addColumns(columnDefinitions: ColumnDefinition[], viewport: GridViewportName, state: GridState): Observable<GridState> {
    throw new Error('Programmatic error. You cannot add columns to a DynamicColumnDefinitionManagerService');
  }

  private generateCustomFieldColumns(bindableEntity: BindableEntity): Observable<ColumnDefinition[]> {
    return this.referentialDataService.filteredProjects$.pipe(
      take(1),
      map((projectsData: ProjectData[]) => {
        const customFields = projectsData.reduce((cufs, projectData) => {
          projectData.customFieldBinding[bindableEntity].map(binding => binding.customField).forEach(cuf => cufs[cuf.id] = cuf);
          return cufs;
        }, {});
        return Object.values(customFields) as CustomField[];
      }),
      map((customFields: CustomField[]) => {
        return customFields.map(customField => textColumn('CUF_COLUMN_' + customField.id).withLabel(customField.label).build());
      })
    );
  }

  addColumnAtIndex(newColumns: ColumnDefinition[], state: GridState,
                   index?: number): Observable<GridState> {
    throw new Error('Programmatic error. You cannot add columns to a DynamicColumnDefinitionManagerService');
  }

  removeColumn(columnIds: Identifier[], state: GridState): Observable<GridState> {
    throw new Error('Programmatic error. You cannot remove columns to a DynamicColumnDefinitionManagerService');
  }


}
