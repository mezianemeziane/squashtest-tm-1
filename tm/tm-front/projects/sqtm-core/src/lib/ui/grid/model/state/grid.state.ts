import {initialPaginationState, PaginationState} from './pagination.state';
import {DataRowState, initialDataRowState} from './datarow.state';
import {DefinitionState, initialDefinitionState} from './definition.state';
import {ColumnState, initialColumnState} from './column.state';
import {initialUiState, UiState} from './ui.state';
import {Identifier} from '../../../../model/entity.model';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {FilterState, initialFilterState, selectFilters} from './filter.state';
import {GridConfigurationState, initialUserConfigurationState} from './configuration.state';
import {GridNodesState, initialNodeState} from './grid-nodes.state';

export interface GridState {
  paginationState: PaginationState;
  dataRowState: DataRowState;
  nodesState: GridNodesState;
  definitionState: DefinitionState;
  columnState: ColumnState;
  uiState: UiState;
  filterState: FilterState;
  configurationState: GridConfigurationState;
}

export type GridStateReadOnly = Readonly<GridState>;

export function initialGridState(): GridStateReadOnly {
  return {
    paginationState: initialPaginationState(),
    dataRowState: initialDataRowState(),
    nodesState: initialNodeState(),
    definitionState: initialDefinitionState(),
    columnState: initialColumnState(),
    uiState: initialUiState(),
    filterState: initialFilterState(),
    configurationState: initialUserConfigurationState()
  };
}

export const getPaginationState = createFeatureSelector<GridState, PaginationState>('paginationState');
export const getDataRowState = createFeatureSelector<GridState, DataRowState>('dataRowState');
export const getNodeState = createFeatureSelector<GridState, GridNodesState>('nodesState');
export const getDefinitionState = createFeatureSelector<GridState, DefinitionState>('definitionState');
export const getColumnState = createFeatureSelector<GridState, ColumnState>('columnState');
export const getUiState = createFeatureSelector<GridState, UiState>('uiState');
export const getConfigurationState = createFeatureSelector<GridState, GridConfigurationState>('configurationState');
export const getFilterConfigurationState = createSelector(getConfigurationState,
  (configurationState) => configurationState.filterConfigurationState);

export function getDataRowByIdFactory(id: Identifier) {
  return createSelector(getDataRowState, (dataRowState) => {
    return dataRowState.entities[id];
  });
}

export function getFilterValueByIdFactory(id: Identifier) {
  return createSelector(selectFilters, (filterState) => {
    return filterState.entities[id];
  });
}

export const selectSorts = createSelector(getColumnState, columnState => columnState.sortedColumns);
