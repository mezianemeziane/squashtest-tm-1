import {GridDisplay, ViewportDisplay} from '../../model/grid-display.model';
import {GridNode} from '../../model/grid-node.model';
import {ChangeDetectorRef} from '@angular/core';

export interface RowRenderer {
  gridDisplay: GridDisplay;
  gridNode: GridNode;
  viewport: ViewportDisplay;
  cdRef: ChangeDetectorRef;
}
