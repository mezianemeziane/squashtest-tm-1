import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewContainerRef
} from '@angular/core';
import {GridDefinition} from '../../../model/grid-definition.model';
import {treePicker} from '../../../../../model/grids/grid-builders';
import {CAMPAIGN_LIMITED_TREE_PICKER_CONFIG, CAMPAIGN_LIMITED_TREE_PICKER_ID} from '../../../tree-pickers.constant';
import {column} from '../../../model/column-definition.builder';
import {Extendable} from '../../../model/column-definition.model';
import {TreeNodeCellRendererComponent} from '../../../../cell-renderer-common/tree-node-cell-renderer/tree-node-cell-renderer.component';
import {GridService} from '../../../services/grid.service';
import {gridServiceFactory} from '../../../grid.service.provider';
import {DataRow, DataRowOpenState} from '../../../model/data-row.model';
import {ProjectDataMap} from '../../../../../model/project/project-data.model';
import {
  Campaign,
  CampaignFolder,
  CampaignLibrary,
  SquashTmDataRow,
  SquashTmDataRowType
} from '../../../../../model/grids/data-row.type';
import {Subject, throwError} from 'rxjs';
import {CampaignPermissions} from '../../../../../model/permissions/simple-permissions';
import {Identifier} from '../../../../../model/entity.model';
import {DialogService} from '../../../../dialog/services/dialog.service';
import {Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {TreeWithStatePersistence} from '../../../../workspace-common/components/tree/tree-with-state-persistence';
import {ReferentialDataService} from '../../../../../core/referential/services/referential-data.service';
import {RestService} from '../../../../../core/services/rest.service';
import {GridPersistenceService} from '../../../../../core/services/grid-persistence/grid-persistence.service';

export function convertCampaignLimitedLiterals(literals: Partial<DataRow>[], projectsData: ProjectDataMap) {
  // DON'T INLINE THE VAR, IT WOULD MAKE BUILD CRASH WITH ERROR :
  // {"__symbolic":"error","message":"Lambda not supported","line":42,"character":22}
  const converted: SquashTmDataRow[] = literals.map(literal => convertCampaignLimitedLiteral(literal, projectsData));
  return converted;
}

export function convertCampaignLimitedLiteral(literal: Partial<DataRow>, projectsData: ProjectDataMap): SquashTmDataRow {
  let dataRow: DataRow;
  let type = 'Generic';
  if (literal.type) {
    type = literal.type;
  } else {
    if (typeof literal.id === 'string') {
      try {
        type = literal.id.split('-')[0];
      } catch (e) {
        console.log(`Unable to auto assign row type from id : ${literal.id}. Will keep generic type`);
      }
    }
  }

  switch (type) {
    // Campaign WS
    case SquashTmDataRowType.CampaignLibrary:
      dataRow = new CampaignLibrary();
      break;
    case SquashTmDataRowType.CampaignFolder:
      dataRow = new CampaignFolder();
      break;
    case SquashTmDataRowType.Campaign:
      dataRow = new Campaign();
      break;
    default:
      throwError('Not handled type ' + type);
  }

  dataRow.projectId = literal.projectId;
  const project = projectsData[dataRow.projectId];
  dataRow.simplePermissions = new CampaignPermissions(project);

  Object.assign(dataRow, literal);
  // we disable hierarchy for campaign so no other nodes will be displayed
  if (type === SquashTmDataRowType.Campaign) {
    dataRow.state = DataRowOpenState.leaf;
    dataRow.children = [];
  }
  return dataRow;
}

/** @dynamic */
export function campaignLimitedTreePickerConfigFactory(): GridDefinition {
  return treePicker(CAMPAIGN_LIMITED_TREE_PICKER_ID)
    .server()
    .withServerUrl(['campaign-tree'])
    .withColumns([
      column('NAME')
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(TreeNodeCellRendererComponent)
    ])
    .withRowConverter(convertCampaignLimitedLiterals)
    .enableDrag()
    .build();
}


@Component({
  selector: 'sqtm-core-campaign-limited-picker',
  template: `
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./campaign-limited-picker.component.less'],
  providers: [
    {
      provide: CAMPAIGN_LIMITED_TREE_PICKER_CONFIG,
      useFactory: campaignLimitedTreePickerConfigFactory
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, CAMPAIGN_LIMITED_TREE_PICKER_CONFIG, ReferentialDataService]
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CampaignLimitedPickerComponent extends TreeWithStatePersistence implements OnInit, OnDestroy {

  @Input()
  set pickerPersistenceKey(persistenceKey) {
    if (!Boolean(this.persistenceKey)) {
      this.persistenceKey = persistenceKey;
    } else {
      throw Error('Cannot change the persistence key dynamically ' + this.persistenceKey);
    }
  }

  @Input()
  initialSelectedNodes: Identifier[] = [];

  @Output()
  selectedRows = new EventEmitter<DataRow[]>();

  unsub$ = new Subject<void>();

  constructor(public tree: GridService,
              protected referentialDataService: ReferentialDataService,
              protected restService: RestService,
              protected dialogService: DialogService,
              protected vcr: ViewContainerRef,
              protected gridPersistenceService: GridPersistenceService,
              protected router: Router) {
    super(tree, referentialDataService, gridPersistenceService, restService, router, dialogService, vcr, null, 'campaign-tree');
  }

  ngOnInit() {
    this.tree.selectedRows$.pipe(
      takeUntil(this.unsub$)
    ).subscribe((rows) => {
      this.selectedRows.next(rows);
    });

    this.initData({selectedNodes: this.initialSelectedNodes});
    if (Boolean(this.persistenceKey)) {
      this.registerStatePersistence();
    }
  }

  ngOnDestroy(): void {
    if (this.persistenceKey) {
      this.unregisterStatePersistence();
    }
    this.tree.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

}
