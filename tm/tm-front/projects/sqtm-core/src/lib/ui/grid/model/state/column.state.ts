import {ColumnDefinition, SortedColumn} from '../column-definition.model';
import {EntityState} from '@ngrx/entity';
import {Identifier} from '../../../../model/entity.model';

export interface ColumnState extends EntityState<ColumnDefinition> {
  sortedColumns: SortedColumn[];
  mainViewport: ViewportState;
  leftViewport: ViewportState;
  rightViewport: ViewportState;
  shouldResetSorts?: boolean;
}

export interface ViewportState {
  order: Identifier[];
}

export type GridViewportName = 'leftViewport' | 'mainViewport' | 'rightViewport';
export const gridViewportNames: GridViewportName[] = ['leftViewport', 'mainViewport', 'rightViewport'];

type ColumnStateReadOnly = Readonly<ColumnState>;

export function initialColumnState(): ColumnStateReadOnly {
  return {
    ids: [],
    entities: {},
    sortedColumns: [],
    mainViewport: {...initialViewportState()},
    leftViewport: {...initialViewportState()},
    rightViewport: {...initialViewportState()},
    shouldResetSorts: false
  };
}

export function initialViewportState() {
  return {
    order: []
  };
}
