import {
  ChangeDetectionStrategy,
  Component,
  ComponentRef,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {Subject} from 'rxjs';
import {ComponentPortal} from '@angular/cdk/portal';
import {takeUntil} from 'rxjs/operators';
import {ProjectScopeComponent} from '../project-scope/project-scope.component';
import {Scope} from '../../../../filters/state/filter.state';
import {CustomScopeKind} from '../../../model/state/definition.state';

@Component({
  selector: 'sqtm-core-scope',
  templateUrl: './scope.component.html',
  styleUrls: ['./scope.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScopeComponent implements OnInit, OnDestroy {

  get scope(): Scope {
    return this._scope;
  }

  @Input()
  set scope(scope: Scope) {
    this._scope = scope;
    if (this.componentRef && this.overlayRef) {
      this.componentRef.instance.setScope(scope);
    }
  }

  private _scope: Scope;

  @Input()
  allowCustomScope: boolean;

  @Input()
  customScopeKind: CustomScopeKind;

  @Output()
  valueChanged = new EventEmitter<Scope>();

  @ViewChild('scopeField', {read: ElementRef})
  private scopeField: ElementRef;

  private overlayRef: OverlayRef;

  private unsub$ = new Subject<void>();

  private componentRef: ComponentRef<ProjectScopeComponent>;

  constructor(private overlay: Overlay) {
  }

  ngOnInit() {
  }

  edit() {
    const positionStrategy = this.overlay.position().flexibleConnectedTo(this.scopeField)
      .withPositions([
        {originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'center', offsetX: 10, offsetY: 20},
      ]);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
      panelClass: 'sqtm-overlay__background'
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const componentPortal = new ComponentPortal(ProjectScopeComponent);
    this.componentRef = this.overlayRef.attach(componentPortal);
    if (this.allowCustomScope) {
      this.componentRef.instance.customScopeKind = this.customScopeKind;
    }
    this.componentRef.instance.setScope({...this.scope});
    this.componentRef.instance.valueChanged.pipe(
      takeUntil(this.unsub$)
    ).subscribe(value => this.changeValue(value));
    this.componentRef.instance.close.pipe(
      takeUntil(this.unsub$)
    ).subscribe(() => this.close());
    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  getDisplayValue() {
    return this.scope.value.map(v => v.label)
      .sort()
      .join(', ');
  }

  close() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.componentRef = null;
    }
  }

  changeValue(value: Scope) {
    this.valueChanged.next(value);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
