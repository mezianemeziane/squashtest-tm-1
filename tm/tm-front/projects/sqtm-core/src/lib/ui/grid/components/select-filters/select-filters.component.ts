import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {GridService} from '../../services/grid.service';
import {Observable, Subject} from 'rxjs';
import {select} from '@ngrx/store';
import {selectFiltersAndGroupsForMultiList} from '../../model/state/filter.state';
import {takeUntil} from 'rxjs/operators';
import {
  ListGroup,
  ListItem
} from '../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';


@Component({
  selector: 'sqtm-core-select-filters',
  templateUrl: './select-filters.component.html',
  styleUrls: ['./select-filters.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectFiltersComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();
  activateFilter$ = new Subject<void>();
  multiListData$: Observable<{ itemGroups: ListGroup[]; items: ListItem[] }>;

  constructor(private grid: GridService) {
  }

  ngOnInit() {
    this.multiListData$ = this.grid.gridState$.pipe(
      takeUntil(this.unsub$),
      select(selectFiltersAndGroupsForMultiList),
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.activateFilter$.complete();
  }

  toggleFilter($event: { id: string; selected: boolean }) {
    this.grid.activateFilter($event.id);
    this.activateFilter$.next();
  }
}
