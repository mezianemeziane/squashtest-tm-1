import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CustomFieldCheckBoxRendererComponent} from './custom-field-check-box-renderer.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {grid} from '../../../../../../model/grids/grid-builders';
import {GridDefinition} from '../../../../model/grid-definition.model';
import {RestService} from '../../../../../../core/services/rest.service';
import {GridService} from '../../../../services/grid.service';
import {gridServiceFactory} from '../../../../grid.service.provider';
import {ReferentialDataService} from '../../../../../../core/referential/services/referential-data.service';

describe('CustomFieldCheckBoxRendererComponent', () => {
  let component: CustomFieldCheckBoxRendererComponent;
  let fixture: ComponentFixture<CustomFieldCheckBoxRendererComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CustomFieldCheckBoxRendererComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomFieldCheckBoxRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
