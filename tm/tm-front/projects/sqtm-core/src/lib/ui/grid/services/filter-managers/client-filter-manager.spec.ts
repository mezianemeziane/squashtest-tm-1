import {ColumnState, initialColumnState} from '../../model/state/column.state';
import {FilterState, initialFilterState} from '../../model/state/filter.state';
import {DataRowState} from '../../model/state/datarow.state';
import {Identifier} from '../../../../model/entity.model';
import {Sort} from '../../model/column-definition.model';
import {DataRow, DataRowOpenState} from '../../model/data-row.model';
import {createEntityAdapter} from '@ngrx/entity';
import {extractRootIds} from '../data-row-loaders/data-row-utils';
import {ClientFilterManager} from './client-filter-manager';
import {initialGridState} from '../../model/state/grid.state';
import {createColumn} from '../../grid-testing/grid-testing-utils';
import {convertSqtmLiterals} from '../../../../model/grids/data-row.type';

describe(`Client Filter Manager`, function () {

  describe(`Filters`, () => {
    interface DataType {
      columnState: ColumnState;
      filterState: FilterState;
      dataRowState: DataRowState;
      expectedDataRowIds: Identifier[];
      expectedAncestorMatchingRowIds: Identifier[];
    }

    const columnState = {
      ...initialColumnState(),
      ids: ['name'],
      entities: {'name': createColumn('name', true)},
      sortedColumns: [{id: 'name', sort: Sort.ASC}],
    };

    const dataSets: DataType[] = [{
      columnState: columnState,
      filterState: initialFilterState(),
      dataRowState: getDataRowState(getDataRows()),
      expectedDataRowIds: [],
      expectedAncestorMatchingRowIds: []
    }, {
      columnState: columnState,
      filterState: {
        ...initialFilterState(), filters: {
          ids: ['name'],
          entities: {
            'name': {
              id: 'name',
              value: {kind: 'single-string-value', value: 'Login'},
              availableOperations: [],
              i18nLabelKey: '',
              active: true,
              initialValue: {kind: 'single-string-value', value: 'Login'},
              tiedToPerimeter: false,
            }
          },
          matchingRowIds: [],
          ancestorMatchingRowIds: [],
        }
      },
      dataRowState: getDataRowState(getDataRows()),
      expectedDataRowIds: ['TestCase-12', 'TestCase-4'],
      expectedAncestorMatchingRowIds: ['TestCaseLibrary-1', 'TestCaseFolder-1']
    }];


    // todo remake that after filtering is stabilized
    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - It should filter with filters : ${JSON.stringify(data.filterState.filters.entities)}`, () => {
        const gridState = {
          ...initialGridState(),
          ...data
        };
        new ClientFilterManager().applyFilters(gridState).subscribe(
          state => {
            expect(state.filterState.filters.matchingRowIds).toEqual(data.expectedDataRowIds);
            expect(state.filterState.filters.ancestorMatchingRowIds).toEqual(data.expectedAncestorMatchingRowIds);
          }
        );

      });

    }

  });

  function getDataRows(): DataRow[] {
    const tcl1 = {
      id: 'TestCaseLibrary-1',
      state: DataRowOpenState.closed,
      children: ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2'],
      data: {
        name: 'Project-1',
        reference: 'A'
      }
    };

    const tcl2 = {
      id: 'TestCaseLibrary-2',
      state: DataRowOpenState.closed,
      children: ['TestCase-10', 'TestCaseFolder-11'],
      data: {
        name: 'A-TNR',
        reference: 'T4',
        num: 78
      }
    };

    const tcf1 = {
      id: 'TestCaseFolder-1',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseLibrary-1',
      children: ['TestCaseFolder-3', 'TestCase-4', 'TestCase-5', 'TestCaseFolder-6'],
      data: {
        name: 'Performance',
        num: 7
      }
    };

    const tcf2 = {
      id: 'TestCaseFolder-2',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseLibrary-1',
      children: [],
      data: {
        name: 'Security',
        num: NaN
      }
    };

    const tcf3 = {
      id: 'TestCaseFolder-3',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseFolder-1',
      children: ['TestCaseFolder-7', 'TestCase-8'],
      data: {
        name: 'Page 1',
        num: -12
      }
    };

    const tcf6 = {
      id: 'TestCaseFolder-6',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'Page 2',
        num: -1
      }
    };

    const tcf7 = {
      id: 'TestCaseFolder-7',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseFolder-3',
      children: ['TestCase-9'],
      data: {
        name: 'Page 1 Tab 1',
        num: 5
      }
    };

    const tcf11 = {
      id: 'TestCaseFolder-11',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseLibrary-2',
      children: [],
      data: {
        name: 'zzz',
        reference: 'B',
        num: 9
      }
    };

    const tc12 = {
      id: 'TestCase-12',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseLibrary-1',
      children: [],
      data: {
        name: 'Login test',
        reference: null,
        num: null
      }
    };

    const tc4 = {
      id: 'TestCase-4',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'Login',
        num: 3
      }
    };

    const tc5 = {
      id: 'TestCase-5',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'Show in 2 seconds',
        reference: '',
        num: 1
      }
    };

    const tc8 = {
      id: 'TestCase-8',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-3',
      children: [],
      data: {
        name: 'This report should not make the server hang',
        num: 0
      }
    };

    const tc9 = {
      id: 'TestCase-9',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-7',
      children: [],
      data: {
        name: 'Hibernate shouldn\'t make n + 1 !!!',
        num: 45
      }
    };

    const tc10 = {
      id: 'TestCase-10',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseLibrary-2',
      children: [],
      data: {
        name: 'TestCase 10',
        num: 1000
      }
    };


    return convertSqtmLiterals([tcl1, tcf1, tcf2, tcf3, tcf6, tcf7, tc12, tc4, tc5, tcl2, tc8, tc9, tc10, tcf11], []);
  }

  function getDataRowsMultipleAttributeTesting(): DataRow[] {
    const tcl1 = {
      id: 'TestCaseLibrary-1',
      state: DataRowOpenState.closed,
      children: ['TestCaseFolder-1'],
      data: {
        name: 'Project-1',
      }
    };

    const tcf1 = {
      id: 'TestCaseFolder-1',
      state: DataRowOpenState.closed,
      parentRowId: 'TestCaseLibrary-1',
      children: ['TestCase-1', 'TestCase-2', 'TestCase-3', 'TestCase-4', 'TestCase-5'],
      data: {
        name: 'TC Folder',
      }
    };

    const tc1 = {
      id: 'TestCase-1',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'Show in 2 seconds',
        weight: 1
      }
    };

    const tc2 = {
      id: 'TestCase-2',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'Tutu',
        weight: 1
      }
    };

    const tc3 = {
      id: 'TestCase-3',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'Ahhh',
        weight: 1
      }
    };

    const tc4 = {
      id: 'TestCase-4',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'TC-4',
        weight: 2
      }
    };

    const tc5 = {
      id: 'TestCase-5',
      state: DataRowOpenState.leaf,
      parentRowId: 'TestCaseFolder-1',
      children: [],
      data: {
        name: 'Three in a row',
        weight: 2
      }
    };

    return convertSqtmLiterals([tcl1, tcf1, tc1, tc2, tc3, tc4, tc5], []);
  }

  function getDataRowState(datarows: DataRow[]): DataRowState {
    const adapter = createEntityAdapter<DataRow>();
    const initialState = {
      ids: [],
      entities: {},
      selectedRows: [],
      rootRowIds: extractRootIds(Array.from(datarows)),
      count: datarows.length,
      lastToggledRowId: null,
    };
    return adapter.setAll(datarows, initialState);
  }
});

