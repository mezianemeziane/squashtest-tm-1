import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ComponentRef,
  ElementRef,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import {GridType} from '../../model/grid-definition.model';
import {ColumnWithFilter} from '../../model/column-display.model';
import {GridDisplay} from '../../model/grid-display.model';
import {GridNode} from '../../model/grid-node.model';
import {CellRenderer} from '../../model/cell-renderer';
import {DynamicComponentDirective} from '../../../workspace-common/directives/dynamic-component.directive';
import {GridViewportColumns, GridViewportService, RenderedGridViewport} from '../../services/grid-viewport.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {GridViewportName} from '../../model/state/column.state';


@Component({
  selector: 'sqtm-core-grid-cell',
  templateUrl: './grid-cell.component.html',
  styleUrls: ['./grid-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridCellComponent implements OnInit, OnDestroy, AfterViewInit {

  private _columnDisplay: ColumnWithFilter;

  get columnDisplay(): ColumnWithFilter {
    return this._columnDisplay;
  }

  @Input()
  set columnDisplay(value: ColumnWithFilter) {
    this._columnDisplay = value;
    if (!this.cellComponentRef) {
      this.createCell();
    }
    this.updateDynamicComponent();
  }

  private _gridNode: GridNode;

  get gridNode(): GridNode {
    return this._gridNode;
  }

  @Input()
  set gridNode(value: GridNode) {
    this._gridNode = value;
    this.updateDynamicComponent();
  }

  private _gridDisplay: GridDisplay;

  get gridDisplay(): GridDisplay {
    return this._gridDisplay;
  }

  @Input()
  set gridDisplay(value: GridDisplay) {
    this._gridDisplay = value;
    this.updateDynamicComponent();
  }

  private _viewportName: GridViewportName;

  get viewportName(): GridViewportName {
    return this._viewportName;
  }

  @Input()
  set viewportName(viewportName: GridViewportName) {
    this._viewportName = viewportName;
    this.updateDynamicComponent();
  }

  @ViewChild('cellElement', {read: ElementRef})
  cellElement: ElementRef;

  @ViewChild(DynamicComponentDirective, {static: true})
  dynamicContainer: DynamicComponentDirective<CellRenderer>;

  private cellComponentRef: ComponentRef<CellRenderer>;

  private unsub$ = new Subject<void>();

  constructor(private zone: NgZone,
              private renderer: Renderer2,
              private gridViewportService: GridViewportService) {

  }

  ngOnInit() {

  }

  ngAfterViewInit(): void {
    this.zone.runOutsideAngular(() => {
      this.gridViewportService.renderedGridViewport$
        .pipe(takeUntil(this.unsub$))
        .subscribe(renderedGridViewport => this.resizeCell(renderedGridViewport));
    });
  }

  private resizeCell(renderedGridViewport: RenderedGridViewport) {
    const gridViewportColumns: GridViewportColumns = renderedGridViewport[this.viewportName];
    const columnId = this.columnDisplay.id;
    const column = gridViewportColumns.columns[columnId];
    this.renderer.setStyle(this.cellElement.nativeElement, 'width', `${column.calculatedWidth}px`);
    if (this.gridDisplay.gridType !== GridType.TABLE) {
      this.renderer.setStyle(this.cellElement.nativeElement, 'height', `${this.gridDisplay.rowHeight}px`);
      this.renderer.setStyle(this.cellElement.nativeElement, 'left', `${column.left}px`);
      this.renderer.setStyle(this.cellElement.nativeElement, 'position', 'absolute');
    }
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }


  private createCell() {
    this.dynamicContainer.type = this._columnDisplay.cellRenderer;
    this.cellComponentRef = this.dynamicContainer.instantiateComponent();
  }

  private cellIsRendered() {
    return Boolean(this.cellComponentRef);
  }

  private allAttributesAreSet() {
    return this.gridNode && this.gridDisplay && this.columnDisplay;
  }

  updateDynamicComponent() {
    if (this.allAttributesAreSet()) {
      if (!this.cellIsRendered()) {
        this.dynamicContainer.type = this._columnDisplay.cellRenderer;
        const cellRendererComponentRef = this.dynamicContainer.instantiateComponent();
        this.cellComponentRef = cellRendererComponentRef;
        this.doUpdateCell();
      } else {
        this.doUpdateCell();
      }
    }
  }

  private doUpdateCell() {
    const instance: CellRenderer = this.cellComponentRef.instance;
    instance.index = this.gridNode.index;
    instance.gridDisplay = this._gridDisplay;
    instance.depth = this.gridNode.depth;
    instance.row = this.gridNode.dataRow;
    instance.selected = this.gridNode.selected;
    instance.showAsFilteredParent = this.gridNode.showAsFilteredParent;
    instance.columnDisplay = this.columnDisplay;
    instance.isLast = this.gridNode.isLast;
    instance.depthMap = this.gridNode.depthMap;
    instance.cdRef.detectChanges();
  }


}
