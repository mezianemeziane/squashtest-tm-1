import {Component, OnInit, ChangeDetectionStrategy, Inject} from '@angular/core';
import {GridDndData} from '../../../model/drag-and-drop/grid-dnd-data';
import {DataRow} from '../../../model/data-row.model';
import {DraggedContentRenderer} from '../../../../drag-and-drop/dragged-content-renderer';
import {DRAG_AND_DROP_DATA} from '../../../../drag-and-drop/constants';
import {DragAndDropData} from '../../../../drag-and-drop/drag-and-drop-data.model';

@Component({
  selector: 'sqtm-core-default-tree-dragged-content',
  templateUrl: './default-tree-dragged-content.component.html',
  styleUrls: ['./default-tree-dragged-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DefaultTreeDraggedContentComponent extends DraggedContentRenderer implements OnInit {

  private readonly maxRowCount = 10;

  get dataRows(): Readonly<DataRow>[] {
    if (this._dataRows.length <= this.maxRowCount) {
      return this._dataRows;
    } else {
      return this._dataRows.slice(0, this.maxRowCount);
    }
  }

  set dataRows(value: Readonly<DataRow>[]) {
    this._dataRows = value;
  }

  private _dataRows: Readonly<DataRow>[];

  constructor(@Inject(DRAG_AND_DROP_DATA) public dragAnDropData: DragAndDropData) {
    super(dragAnDropData);
    this._dataRows = (dragAnDropData.data as GridDndData).dataRows;
  }

  ngOnInit() {
  }

}
