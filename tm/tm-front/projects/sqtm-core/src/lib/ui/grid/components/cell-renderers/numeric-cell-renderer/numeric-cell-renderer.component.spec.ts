import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {NumericCellRendererComponent} from './numeric-cell-renderer.component';
import {GridService} from '../../../services/grid.service';
import {RestService} from '../../../../../core/services/rest.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {GridDefinition} from '../../../model/grid-definition.model';
import {gridServiceFactory} from '../../../grid.service.provider';
import {ReferentialDataService} from '../../../../../core/referential/services/referential-data.service';
import {grid} from '../../../../../model/grids/grid-builders';

describe('NumericCellRendererComponent', () => {
  let component: NumericCellRendererComponent;
  let fixture: ComponentFixture<NumericCellRendererComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [NumericCellRendererComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumericCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
