import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-core-pagination-size-selector',
  template: `
    <nz-select [(ngModel)]="pageSize" [nzSize]="'small'" (ngModelChange)="handleChange($event)" [ngStyle]="getStyle()">
      <nz-option *ngFor="let size of allowedPageSizes" [nzValue]="size" [nzLabel]="size.toString(10)"></nz-option>
      <nz-option *ngIf="allowShowAll" nzValue="-1" [nzLabel]="translateService.instant('grid.show-all.label')"></nz-option>
    </nz-select>
  `,
  styleUrls: ['./pagination-size-selector.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginationSizeSelectorComponent implements OnInit {

  @Input()
  pageSize: number;

  @Input()
  allowedPageSizes: number [];

  @Input()
  allowShowAll: boolean;

  @Output()
  changeSize = new EventEmitter<number>();

  constructor(public translateService: TranslateService) {
  }

  ngOnInit() {
  }

  handleChange(value: string) {
    this.changeSize.emit(Number.parseInt(value, 10));
  }

  getStyle() {
    return {
      width: `${this.allowShowAll ? 120 : 80}px`
    };
  }
}
