import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {EmptyGridMessageComponent} from './empty-grid-message.component';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {GridDisplay} from '../../../model/grid-display.model';

describe('EmptyGridMessageComponent', () => {
  let component: EmptyGridMessageComponent;
  let fixture: ComponentFixture<EmptyGridMessageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        TestingUtilsModule
      ],
      declarations: [EmptyGridMessageComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptyGridMessageComponent);
    component = fixture.componentInstance;
    component.gridDisplay = {} as unknown as GridDisplay;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
