import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {AbstractCellRendererComponent} from '../abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../../services/grid.service';

@Component({
  selector: 'sqtm-core-radio-cell-renderer',
  templateUrl: './radio-cell-renderer.component.html',
  styleUrls: ['./radio-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RadioCellRendererComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  ngOnInit(): void {
  }

}
