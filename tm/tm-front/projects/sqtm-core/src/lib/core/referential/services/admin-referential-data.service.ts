import {Injectable} from '@angular/core';
import {RestService} from '../../services/rest.service';
import {
  AdminReferentialDataState,
  getAdminLoadedState,
  getAdminUserState,
  getGlobalConfiguration,
  getLicenseInformation,
  initialAdminReferentialDataState
} from '../state/admin-referential-data.state';
import {Observable} from 'rxjs';
import {AuthenticatedUser} from '../../../model/user/authenticated-user.model';
import {createSelector, select} from '@ngrx/store';
import {concatMap, filter, map, take, tap, withLatestFrom} from 'rxjs/operators';
import {UserState} from '../state/user.state';
import {createStore, Store} from '../../store/store';
import {GlobalConfiguration} from '../state/global-configuration.state';
import {LicenseInformation} from '../state/license-information.state';
import {CustomField} from '../../../model/customfield/customfield.model';
import {createEntityAdapter} from '@ngrx/entity';
import {TestAutomationServerKind} from '../../../model/test-automation/test-automation-server.model';
import {UserProxyService} from '../../services/user-proxy.service';
import {TemplateConfigurablePlugin} from '../../../model/plugin/template-configurable-plugin.model';

@Injectable({
  providedIn: 'root'
})
export class AdminReferentialDataService {

  private store: Store<AdminReferentialDataState> = createStore(initialAdminReferentialDataState(), {
    logDiff: 'none',
    id: 'AdminReferentialDataStore'
  });

  public adminReferentialData$: Observable<AdminReferentialDataState> = this.store.state$;
  public globalConfiguration$: Observable<GlobalConfiguration>;
  public loggedAsAdmin$: Observable<boolean>;
  public loaded$: Observable<boolean>;
  public authenticatedUser$: Observable<AuthenticatedUser>;
  public milestoneFeatureEnabled$: Observable<boolean>;
  public licenseInformation$: Observable<LicenseInformation>;

  private customFieldAdapter = createEntityAdapter<CustomField>();

  constructor(private restService: RestService, private userProxyService: UserProxyService) {
    this.loaded$ = this.store.state$.pipe(
      select(createSelector(getAdminLoadedState, (loaded) => loaded)),
    );

    this.authenticatedUser$ = this.store.state$.pipe(
      select(getAdminUserState),
      filter(userState => Boolean(userState.username))
    );

    this.loggedAsAdmin$ = this.authenticatedUser$.pipe(
      map(user => user.admin),
    );

    this.globalConfiguration$ = this.store.state$.pipe(
      select(getGlobalConfiguration),
    );

    this.milestoneFeatureEnabled$ = this.globalConfiguration$.pipe(
      map(globalConfiguration => globalConfiguration.milestoneFeatureEnabled)
    );

    this.licenseInformation$ = this.store.state$.pipe(
      select(getLicenseInformation),
    );
  }

  public refresh(): Observable<boolean> {
    return this.store.state$.pipe(
      take(1),
      concatMap((state: AdminReferentialDataState) => {
        return this.restService.get<AdminReferentialDataModel>(['referential/admin']).pipe(
          map(referentialData => {
            const userState: UserState = referentialData.user;
            const globalConfigurationState: GlobalConfiguration = referentialData.globalConfiguration;
            const customFieldState = this.customFieldAdapter.setAll(referentialData.customFields, state.customFieldState);

            const refData: AdminReferentialDataState = {
              ...state,
              loaded: true,
              userState,
              globalConfigurationState,
              licenseInformation: referentialData.licenseInformation,
              customFieldState,
              availableTestAutomationServerKinds: referentialData.availableTestAutomationServerKinds,
              canManageLocalPassword: referentialData.canManageLocalPassword,
              templateConfigurablePlugins: referentialData.templateConfigurablePlugins,
            };

            return refData;
          })
        );
      }),
      tap(nextState => this.userProxyService.publishUser(nextState.userState)),
      tap(nextState => this.store.commit(nextState)),
      map(() => true)
    );
  }

  setMilestoneFeatureEnabled(enabled: boolean): Observable<any> {
    return this.restService.post<any>(['features', 'milestones'], {enabled}).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]) => ({
        ...state,
        globalConfigurationState: {
          ...state.globalConfigurationState,
          milestoneFeatureEnabled: enabled,
        }
      })),
      map((newState) => this.store.commit(newState)));
  }
}

/**
 * Class representing the AdminReferentialData fetched from server.
 */
export class AdminReferentialDataModel {
  user: AuthenticatedUser;
  globalConfiguration: GlobalConfiguration;
  licenseInformation: LicenseInformation;
  customFields: CustomField[];
  availableTestAutomationServerKinds: TestAutomationServerKind[];
  canManageLocalPassword: boolean;
  templateConfigurablePlugins: TemplateConfigurablePlugin[];
}
