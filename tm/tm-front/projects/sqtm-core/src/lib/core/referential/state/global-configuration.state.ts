export class GlobalConfigurationState {
  milestoneFeatureEnabled: boolean;
  uploadFileExtensionWhitelist: string[];
  uploadFileSizeLimit: number;
}

export function provideGlobalConfigurationInitialState(): Readonly<GlobalConfigurationState> {
  return {
    milestoneFeatureEnabled: false,
    uploadFileExtensionWhitelist: [],
    uploadFileSizeLimit: 0
  };
}

export type GlobalConfiguration = GlobalConfigurationState;
