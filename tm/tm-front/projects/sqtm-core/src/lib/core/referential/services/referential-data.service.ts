import {Injectable} from '@angular/core';

import {Observable, of} from 'rxjs';
import {concatMap, distinctUntilChanged, map, take, tap, withLatestFrom} from 'rxjs/operators';
import {createEntityAdapter} from '@ngrx/entity';
import {createSelector, select} from '@ngrx/store';
import {createStore, Store} from '../../store/store';
import {InfoListAttribute, Project} from '../../../model/project/project.model';
import {Identifier} from '../../../model/entity.model';
import {ProjectState} from '../state/project.state';
import {RestService} from '../../services/rest.service';
import {
  customFieldSelectorFactory,
  generateProjectData,
  getAutomatedTestTechnologiesState,
  getBugTrackerState,
  getCustomFieldState,
  getInfoListState,
  getLoadedState,
  getMilestoneState,
  getProjectState,
  getRequirementVersionLinkTypeState,
  getScmServersState,
  getTaServerState,
  getUserState,
  initialReferentialDataState,
  isAdmin,
  isAdminOrProjectManager,
  projectDataSelectorFactory,
  ReferentialDataState,
  selectInfolistByProjectsFactory,
  selectInfoListItem,
  selectMilestoneModeData,
  selectMilestonesByProjectsFactory,
  selectMilestonesFactory,
  selectTestCaseNatureByProjectsFactory,
  selectTestCaseTypeByProjectsFactory
} from '../state/referential-data.state';
import {InfoList} from '../../../model/infolist/infolist.model';
import {ProjectData, ProjectDataMap} from '../../../model/project/project-data.model';
import {CustomField} from '../../../model/customfield/customfield.model';
import {AuthenticatedUser} from '../../../model/user/authenticated-user.model';
import {UserState} from '../state/user.state';
import {CustomFieldState} from '../state/custom-field-state';
import {BindableEntity} from '../../../model/bindable-entity.model';
import {InfoListState} from '../state/info-list.state';
import {GlobalConfiguration} from '../state/global-configuration.state';
import {BugTrackerState} from '../state/bug-tracker-state';
import {referentialLogger} from '../referential.logger';
import {MilestoneState} from '../state/milestone.state';
import {TestAutomationServerState} from '../state/test-automation-server.state';
import {TestAutomationServer} from '../../../model/test-automation/test-automation-server.model';
import {arraysAreSame} from '../../utils/distinct-until-changed-arrays';
import {BugTracker} from '../../../model/bugtracker/bug-tracker.model';
import {Milestone} from '../../../model/milestone/milestone.model';
import {MilestoneLocalPersistenceService} from '../../services/milestone-local-persistence.service';
import {MilestoneModeData, MilestoneStateSnapshot} from '../state/milestone-filter.state';
import {UserProxyService} from '../../services/user-proxy.service';
import {InfoListItem} from '../../../model/infolist/infolistitem.model';
import {Permissions} from '../../../model/permissions/permissions.model';
import {RequirementVersionLinkType} from '../../../model/requirement/requirement-version-link-type.model';
import {RequirementVersionLinkTypeState} from '../state/requirement-version-link-type.state';
import {ReferentialData} from '../state/referential-data.model';
import {getGlobalConfiguration, getLicenseInformation} from '../state/admin-referential-data.state';
import {LicenseInformationState} from '../state/license-information.state';
import {AutomatedTestTechnologyState} from '../state/automated-test-technology.state';
import {AutomatedTestTechnology} from '../../../model/automation/automated-test-technology.model';
import {ScmServer} from '../../../model/scm-server/scm-server.model';
import {ScmServerState} from '../state/scm-server.state';

const logger = referentialLogger.compose('ReferentialDataService');

@Injectable({
  providedIn: 'root'
})
export class ReferentialDataService {

  private store: Store<ReferentialDataState> = createStore(initialReferentialDataState(), {
    logDiff: 'none',
    id: 'ReferentialDataStore'
  });

  private projectAdapter = createEntityAdapter<Project>();
  private infolistAdapter = createEntityAdapter<InfoList>();
  private customFieldAdapter = createEntityAdapter<CustomField>();
  private bugTrackerAdapter = createEntityAdapter<BugTracker>();
  private milestoneAdapter = createEntityAdapter<Milestone>();
  private taServerAdapter = createEntityAdapter<TestAutomationServer>();
  private reqVersionLinkTypeAdapter = createEntityAdapter<RequirementVersionLinkType>();
  private automatedTestTechnologyAdapter = createEntityAdapter<AutomatedTestTechnology>();
  private scmServerAdapter = createEntityAdapter<ScmServer>({
    selectId: (scmServer: ScmServer) => scmServer.serverId,
  });

  public projects$: Observable<Project[]>;
  public referentialData$: Observable<ReferentialDataState> = this.store.state$;
  public loaded$: Observable<boolean>;
  public projectDatas$: Observable<ProjectDataMap>;
  public filteredProjectIds$: Observable<Identifier[]>;
  public filteredProjects$: Observable<ProjectData[]>;
  public authenticatedUser$: Observable<AuthenticatedUser>;
  public filterActivated$: Observable<boolean>;
  public globalConfiguration$: Observable<GlobalConfiguration>;
  public licenseInformation$: Observable<LicenseInformationState>;
  public milestoneModeData$: Observable<MilestoneModeData>;
  public projectsManaged$: Observable<ProjectData[]>;
  public bugTrackers$: Observable<BugTracker[]>;
  public infoLists$: Observable<InfoList[]>;
  public customFields$: Observable<CustomField[]>;
  public requirementVersionLinkTypes$: Observable<RequirementVersionLinkType[]>;
  public bugTrackersBindToProject$: Observable<BugTracker[]>;
  public automatedTestTechnologies$: Observable<AutomatedTestTechnology[]>;
  public scmServers$: Observable<ScmServer[]>;

  constructor(private restService: RestService,
              private milestoneLocalPersistenceService: MilestoneLocalPersistenceService,
              private userProxyService: UserProxyService) {

    this.loaded$ = this.store.state$.pipe(
      select(createSelector(getLoadedState, (loaded) => loaded)),
    );

    this.filterActivated$ = this.store.state$.pipe(
      select(createSelector(getProjectState, (projectState: ProjectState) => projectState.filterActivated)),
    );

    this.projects$ = this.store.state$.pipe(
      select(createSelector(getProjectState, (projectState: ProjectState) => Object.values(projectState.entities))),
    );

    this.filteredProjectIds$ = this.store.state$.pipe(
      select(createSelector(getProjectState, (projectState: ProjectState) => [...projectState.filteredProjectSet])),
      tap(projectIds => logger.debug('Before distinctUntilChanged for new project filtered ids', projectIds)),
      distinctUntilChanged((a, b) => arraysAreSame(a, b)),
      tap(projectIds => logger.debug('New project filtered ids', projectIds)),
    );

    this.projectDatas$ = this.store.state$.pipe(
      select(createSelector(getProjectState, getInfoListState, getCustomFieldState, getUserState,
        getBugTrackerState, getMilestoneState, getTaServerState,
        (projectState: ProjectState, infoListState: InfoListState, customFieldState: CustomFieldState, userState: UserState,
         bugTrackerState: BugTrackerState, milestoneState: MilestoneState, taServerState: TestAutomationServerState) => {
          const projectIds: number[] = projectState.ids as number[];

          const dataMap: ProjectDataMap = {};
          projectIds.forEach(id => {
            dataMap[id] = generateProjectData(projectState, id, customFieldState,
              infoListState, userState, bugTrackerState, milestoneState, taServerState);
          });

          return dataMap;
        })),
    );

    this.filteredProjects$ = this.store.state$.pipe(
      select(createSelector(getProjectState, getInfoListState, getCustomFieldState, getUserState,
        getBugTrackerState, getMilestoneState, getTaServerState,
        (projectState: ProjectState, infoListState: InfoListState, customFieldState: CustomFieldState, userState: UserState,
         bugTrackerState: BugTrackerState, milestoneState: MilestoneState, taServerState: TestAutomationServerState) => {
          let projectIds: number[] = projectState.ids as number[];
          if (projectState.filterActivated) {
            projectIds = Array.from(projectState.filteredProjectSet);
          }
          return projectIds.map(projectId => generateProjectData(projectState, projectId, customFieldState,
            infoListState, userState, bugTrackerState, milestoneState, taServerState));
        })),
    );

    this.projectsManaged$ = this.store.state$.pipe(
      select(createSelector(getProjectState, getInfoListState, getCustomFieldState, getUserState,
        getBugTrackerState, getMilestoneState, getTaServerState,
        (projectState: ProjectState, infoListState: InfoListState, customFieldState: CustomFieldState, userState: UserState,
         bugTrackerState: BugTrackerState, milestoneState: MilestoneState, taServerState: TestAutomationServerState) => {
          let projectIds: number[] = projectState.ids as number[];
          if (projectState.filterActivated) {
            projectIds = Array.from(projectState.filteredProjectSet);
          }
          let projectData = projectIds.map(projectId => generateProjectData(projectState, projectId, customFieldState,
            infoListState, userState, bugTrackerState, milestoneState, taServerState));
          if (!userState.admin) {
            projectData = projectData.filter(project => {
              const projectPermissions = project.permissions.PROJECT;
              return projectPermissions != null && projectPermissions.includes(Permissions.MANAGEMENT);
            });
          }
          return projectData.sort((projectA, projectB) => projectA.name.localeCompare(projectB.name));
        })),
    );

    this.authenticatedUser$ = this.store.state$.pipe(
      select(getUserState)
    );

    this.globalConfiguration$ = this.store.state$.pipe(
      select(getGlobalConfiguration),
    );

    this.licenseInformation$ = this.store.state$.pipe(
      select(getLicenseInformation),
    );

    this.milestoneModeData$ = this.store.state$.pipe(
      select(selectMilestoneModeData)
    );

    this.bugTrackers$ = this.store.state$.pipe(
      select(createSelector(getBugTrackerState, (bugTrackerState: BugTrackerState) => Object.values(bugTrackerState.entities)))
    );

    this.infoLists$ = this.store.state$.pipe(
      select(createSelector(getInfoListState, (infoListState: InfoListState) => Object.values(infoListState.entities)))
    );

    this.customFields$ = this.store.state$.pipe(
      select(createSelector(getCustomFieldState, (customFieldState: CustomFieldState) => Object.values(customFieldState.entities)))
    );

    this.automatedTestTechnologies$ = this.store.state$.pipe(
      select(createSelector(getAutomatedTestTechnologiesState,
        (automatedTestTechnologyState: AutomatedTestTechnologyState) => Object.values(automatedTestTechnologyState.entities)))
    );

    this.requirementVersionLinkTypes$ = this.store.state$.pipe(
      select(createSelector(getRequirementVersionLinkTypeState,
        (linkTypeState: RequirementVersionLinkTypeState) => Object.values(linkTypeState.entities)))
    );

    this.bugTrackersBindToProject$ = this.store.state$.pipe(
      select(createSelector(getProjectState, getBugTrackerState, (projectState: ProjectState, bugTrackerState: BugTrackerState) => {
        const projects = Object.values(projectState.entities);
        const bugTrackerIdBindToProject = [...new Set(projects
          .filter(project => project.bugTrackerBinding != null)
          .map(project => project.bugTrackerBinding.bugTrackerId))];
        const bugTrackers: BugTracker[] = Object.values(bugTrackerState.entities);
        return bugTrackers.filter(bugTracker => bugTrackerIdBindToProject.includes(bugTracker.id));
      }))
    );

    this.scmServers$ = this.store.state$.pipe(
      select(createSelector(getScmServersState, (scmServerState: ScmServerState) => Object.values(scmServerState.entities)))
    );
  }

  public refresh(): Observable<boolean> {
    return this.store.state$.pipe(
      take(1),
      concatMap((state: ReferentialDataState) => {
        return this.restService.get<ReferentialData>(['referential']).pipe(
          map(referentialData => {
            const projectState = this.projectAdapter.setAll(referentialData.projects, state.projectState);
            projectState.filteredProjectSet = referentialData.filteredProjectIds;
            projectState.filterActivated = referentialData.projectFilterStatus;
            const infoListState = this.infolistAdapter.setAll(referentialData.infoLists, state.infoListState);
            const userState: UserState = referentialData.user;
            const customFieldState = this.customFieldAdapter.setAll(referentialData.customFields, state.customFieldState);
            const bugTrackerState = this.bugTrackerAdapter.setAll(referentialData.bugTrackers, state.bugTrackerState);
            const milestoneState = this.milestoneAdapter.setAll(referentialData.milestones, state.milestoneState);
            const taServerState = this.taServerAdapter.setAll(referentialData.automationServers, state.taServerState);
            const requirementVersionLinkTypeState = this.reqVersionLinkTypeAdapter.setAll(
              referentialData.requirementVersionLinkTypes, state.requirementVersionLinkTypeState);
            const automatedTestTechnologiesState = this.automatedTestTechnologyAdapter.setAll(
              referentialData.automatedTestTechnologies, state.automatedTestTechnologiesState);
            const scmServerState = this.scmServerAdapter.setAll(
              referentialData.scmServers, state.scmServerState);
            const referentialDataState: ReferentialDataState = {
              ...state,
              loaded: true,
              projectState,
              infoListState,
              customFieldState,
              userState,
              globalConfigurationState: referentialData.globalConfiguration,
              bugTrackerState,
              milestoneState,
              taServerState,
              requirementVersionLinkTypeState,
              workspacePlugins: referentialData.workspacePlugins,
              workspaceWizards: referentialData.workspaceWizards,
              licenseInformation: referentialData.licenseInformation,
              automatedTestTechnologiesState,
              scmServerState,
              availableTestAutomationServerKinds: referentialData.availableTestAutomationServerKinds,
              canManageLocalPassword: referentialData.canManageLocalPassword,
              templateConfigurablePlugins: referentialData.templateConfigurablePlugins,
            };

            return referentialDataState;
          })
        );
      }),
      tap(nextState => logger.debug('New referential data state : ', [nextState])),
      tap(nextState => this.userProxyService.publishUser(nextState.userState)),
      concatMap(nextState => this.restoreMilestoneState(nextState)),
      tap(nextState => this.store.commit(nextState)),
      map(() => true)
    );
  }

  private restoreMilestoneState(nextState: ReferentialDataState) {
    if (nextState.milestoneFilterState.stateRestored) {
      return of(nextState);
    } else {
      return this.doRestoreMilestoneState(nextState);
    }
  }

  private doRestoreMilestoneState(nextState: ReferentialDataState) {
    return this.milestoneLocalPersistenceService.restoreMilestoneMode().pipe(
      map((snapshot: MilestoneStateSnapshot) => {
        let milestoneFilterState = {...nextState.milestoneFilterState, stateRestored: true};
        const persistedStateIsValid = this.validatePersistedState(snapshot, nextState);
        if (persistedStateIsValid) {
          milestoneFilterState = {...milestoneFilterState, ...snapshot};
        }
        return {...nextState, milestoneFilterState};
      })
    );
  }

  private validatePersistedState(snapshot: MilestoneStateSnapshot, nextState: ReferentialDataState): boolean {
    return Boolean(snapshot)
      && (nextState.milestoneState.ids as number[]).includes(snapshot.selectedMilestoneId);
  }

  deactivateProjectFilter() {
    this.store.state$.pipe(
      take(1),
      concatMap(state => {
        return this.restService.post(['global-filter', 'disable-filter']).pipe(
          map(() => state)
        );
      }),
      map(state => {
        const projectState = {...state.projectState};
        projectState.filterActivated = false;
        return {...state, projectState};
      })).subscribe(newState => this.store.commit(newState));
  }

  updateProjectFilterList(projectIdSet: number[]): Observable<any> {
    return this.restService.post(['global-filter', 'filter'], projectIdSet).pipe(
      // ignoring response as server return 200 - void on project filter updates
      withLatestFrom(this.store.state$),
      map(([, state]: [void, ReferentialDataState]) => {
        logger.debug('Updating global filter client side');
        const projectState = {...state.projectState};
        logger.debug('project state before ', [projectState]);
        projectState.filterActivated = true;
        projectState.filteredProjectSet = [...projectIdSet];
        logger.debug('project state after ', [projectState]);
        return {...state, projectState};
      }),
      tap(state => this.store.commit(state))
    );
  }

  connectToProjectData(id: Identifier): Observable<ProjectData> {
    return this.store.state$.pipe(
      select(projectDataSelectorFactory(id))
    );
  }

  findInfoListInProjects(ids: number[], attribute: InfoListAttribute): Observable<InfoList[]> {
    return this.store.state$.pipe(
      select(selectInfolistByProjectsFactory(ids, attribute))
    );
  }

  findTcNatureInfoListInProjects(ids: number[]): Observable<InfoList[]> {
    return this.store.state$.pipe(
      select(selectTestCaseNatureByProjectsFactory(ids))
    );
  }

  findInfoListItem(id: number): Observable<InfoListItem> {
    return this.store.state$.pipe(
      select(selectInfoListItem(id))
    );
  }

  findTcTypeInfoListInProjects(ids: number[]): Observable<InfoList[]> {
    return this.store.state$.pipe(
      select(selectTestCaseTypeByProjectsFactory(ids))
    );
  }

  findMilestonesInProjects(ids: number[]): Observable<Milestone[]> {
    return this.store.state$.pipe(
      select(selectMilestonesByProjectsFactory(ids))
    );
  }

  findCustomFieldByProjectIdAndDomain(id: Identifier, bindableEntity: BindableEntity): Observable<CustomField[]> {
    logger.debug(`Looking for cuf for project ${id}, entity :${bindableEntity}.`);
    return this.connectToProjectData(id).pipe(
      map(project => {
        const bindings = project.customFieldBinding[bindableEntity];
        const customFields = bindings.map(binding => binding.customField);
        logger.debug(`Found cuf for project ${id}, entity :${bindableEntity}.`, [customFields]);
        return customFields;
      })
    );
  }

  connectToCustomField(id: Identifier): Observable<CustomField> {
    return this.store.state$.pipe(
      select(customFieldSelectorFactory(id))
    );
  }

  isAdmin(): Observable<boolean> {
    return this.store.state$.pipe(
      select(state => isAdmin(state.userState))
    );
  }

  isAdminOrProjectManager(): Observable<boolean> {
    return this.store.state$.pipe(
      select(state => isAdminOrProjectManager(state.userState))
    );
  }

  findMilestones(ids: number[]): Observable<Milestone[]> {
    return this.store.state$.pipe(
      select(selectMilestonesFactory(ids))
    );
  }

  disableMilestoneMode() {
    this.store.state$.pipe(
      take(1),
      map((state: ReferentialDataState) => {
        return {
          ...state, milestoneFilterState: {
            ...state.milestoneFilterState,
            milestoneModeEnabled: false
          }
        };
      }),
      concatMap((state: ReferentialDataState) => this.persistMilestoneFilter(state))
    ).subscribe((state: ReferentialDataState) => this.store.commit(state));
  }

  enableMilestoneMode(selectedMilestoneId: number) {
    this.store.state$.pipe(
      take(1),
      map((state: ReferentialDataState) => {
        return {
          ...state, milestoneFilterState: {
            ...state.milestoneFilterState,
            milestoneModeEnabled: true,
            selectedMilestoneId
          }
        };
      }),
      concatMap((state: ReferentialDataState) => this.persistMilestoneFilter(state))
    ).subscribe((state: ReferentialDataState) => this.store.commit(state));
  }

  findMilestonePerimeter(milestoneId: number): Observable<ProjectData[]> {
    return this.store.state$.pipe(
      select(createSelector(getProjectState, getInfoListState, getCustomFieldState, getUserState,
        getBugTrackerState, getMilestoneState, getTaServerState,
        (projectState: ProjectState, infoListState: InfoListState, customFieldState: CustomFieldState, userState: UserState,
         bugTrackerState: BugTrackerState, milestoneState: MilestoneState, taServerState: TestAutomationServerState) => {
          const projectIds: number[] = Object.values(projectState.entities)
            .filter(project => project.milestoneBindings.map(b => b.milestoneId).includes(milestoneId))
            .map(project => project.id);
          return projectIds.map(projectId => generateProjectData(projectState, projectId, customFieldState,
            infoListState, userState, bugTrackerState, milestoneState, taServerState));
        })),
    );
  }

  private persistMilestoneFilter(state: ReferentialDataState) {
    return this.milestoneLocalPersistenceService.persistMilestoneMode({
      selectedMilestoneId: state.milestoneFilterState.selectedMilestoneId,
      milestoneModeEnabled: state.milestoneFilterState.milestoneModeEnabled
    }).pipe(
      map(() => state)
    );
  }
}

