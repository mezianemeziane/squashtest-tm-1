export interface LicenseInformationState {
  activatedUserExcess: string;
  pluginLicenseExpiration: string;
}

export function provideLicenseInformationInitialState(): Readonly<LicenseInformationState> {
  return {
    activatedUserExcess: null,
    pluginLicenseExpiration: null,
  };
}

export type LicenseInformation = LicenseInformationState;
