import {ReferentialDataService} from './referential-data.service';
import {RestService} from '../../services/rest.service';
import {Project} from '../../../model/project/project.model';
import {EMPTY, of} from 'rxjs';
import {ReferentialDataState} from '../state/referential-data.state';
import {concatMap} from 'rxjs/operators';
import {NO_PERMISSIONS} from '../../../model/permissions/permissions.model';
import {Bindings} from '../../../model/bindable-entity.model';
import {ReferentialData} from '../state/referential-data.model';
import {ActivatedPluginsByWorkspace} from '../../../model/project/project-data.model';

const restService = jasmine.createSpyObj('RestService', ['get', 'post']);
const milestonePersistenceService = jasmine.createSpyObj('MilestoneLocalPersistenceService',
  ['persistMilestoneMode', 'restoreMilestoneMode']);
milestonePersistenceService.restoreMilestoneMode.and.callFake(function (state) {
  return of(state);
});
const userProxyService = jasmine.createSpyObj('UserProxyService', ['publishUser']);

describe('ReferentialDataService', () => {

  function serviceFactory(): ReferentialDataService {
    const refData: ReferentialData = {
      projects: getProjects(),
      projectFilterStatus: false,
      filteredProjectIds: [],
      infoLists: [],
      user: {
        username: '',
        userId: 12,
        admin: true,
        firstName: '',
        lastName: '',
        projectManager: true,
        functionalTester: true,
        automationProgrammer: true
      },
      customFields: [],
      globalConfiguration: {uploadFileSizeLimit: 0, uploadFileExtensionWhitelist: [], milestoneFeatureEnabled: false},
      bugTrackers: [],
      milestones: [],
      automationServers: [],
      requirementVersionLinkTypes: [],
      workspacePlugins: [],
      workspaceWizards: [],
      licenseInformation: null,
      automatedTestTechnologies: [],
      scmServers: [],
      availableTestAutomationServerKinds: [],
      canManageLocalPassword: true,
      templateConfigurablePlugins: [],
    };
    restService.get.and.returnValue(of(refData));
    restService.post.and.returnValue(of(EMPTY));
    return new ReferentialDataService(restService, milestonePersistenceService, userProxyService);
  }

  it('should be created', () => {
    const service: ReferentialDataService = serviceFactory();
    expect(service).toBeTruthy();
  });

  it('should refresh its store', (done) => {
    const service: ReferentialDataService = serviceFactory();
    let nextState: ReferentialDataState;
    service['store'].state$.subscribe(state => {
      nextState = state;
    });

    service.refresh().subscribe(() => {
      expect(nextState.projectState.ids.length).toBe(6);
      const projects = getProjects();
      projects.forEach(project => expect(nextState.projectState.entities[project.id]).toEqual(project));
      expect(nextState.projectState.filteredProjectSet.length).toBe(0);
      expect(nextState.projectState.filterActivated).toBe(false);
      done();
    });
  });

  it('should deactivate project filter', (done) => {

    const service: ReferentialDataService = serviceFactory();
    let nextState: ReferentialDataState;
    service['store'].state$.subscribe(state => {
      nextState = state;
    });

    service.refresh().subscribe(() => {
      service.deactivateProjectFilter();
      expect(nextState.projectState.ids.length).toBe(6);
      const projects = getProjects();
      projects.forEach(project => expect(nextState.projectState.entities[project.id]).toEqual(project));
      expect(nextState.projectState.filteredProjectSet.length).toBe(0);
      expect(nextState.projectState.filterActivated).toBe(false);
      done();
    });
  });

  it('should update project filter list with project filter inactive', (done) => {
    const service: ReferentialDataService = serviceFactory();
    let nextState: ReferentialDataState;
    service['store'].state$.subscribe(state => {
      nextState = state;
    });

    const filteredProjectIdSet = [1, 4, 6];

    service.refresh().pipe(
      concatMap(() => service.updateProjectFilterList(filteredProjectIdSet))
    ).subscribe(() => {
      expect(nextState.projectState.ids.length).toBe(6);
      const projects = getProjects();
      projects.forEach(project => expect(nextState.projectState.entities[project.id]).toEqual(project));
      expect(nextState.projectState.filteredProjectSet.length).toBe(3);
      expect(Array.from(nextState.projectState.filteredProjectSet)).toEqual([1, 4, 6]);
      expect(nextState.projectState.filterActivated).toBe(true);
      done();
    });
  });
});

function getProjects(): Project[] {
  const emptyBindings: Bindings = {
    CAMPAIGN: [],
    CAMPAIGN_FOLDER: [],
    CUSTOM_REPORT_FOLDER: [],
    EXECUTION: [],
    EXECUTION_STEP: [],
    ITERATION: [],
    REQUIREMENT_FOLDER: [],
    REQUIREMENT_VERSION: [],
    TESTCASE_FOLDER: [],
    TEST_CASE: [],
    TEST_STEP: [],
    TEST_SUITE: []
  };

  const emptyActivatedPlugins: ActivatedPluginsByWorkspace = {
    CAMPAIGN_WORKSPACE: [],
    TEST_CASE_WORKSPACE: [],
    REQUIREMENT_WORKSPACE: [],
  };

  return [
    {
      id: 1,
      uri: '/projects/projet-1',
      name: 'projet-1',
      label: 'PROJET-1',
      requirementCategoryId: 1,
      testCaseNatureId: 1,
      testCaseTypeId: 1,
      allowAutomationWorkflow: false,
      customFieldBindings: emptyBindings,
      permissions: NO_PERMISSIONS,
      bugTrackerBinding: null,
      milestoneBindings: [],
      taServerId: 1,
      automationWorkflowType: 'NATIVE',
      disabledExecutionStatus: [],
      attachmentList: {id: null, attachments: []},
      description: '',
      createdBy: null,
      createdOn: null,
      lastModifiedBy: null,
      lastModifiedOn: null,
      linkedTemplate: null,
      linkedTemplateId: null,
      template: false,
      keywords: [],
      bddScriptLanguage: 'ENGLISH',
      allowTcModifDuringExec: false,
      activatedPlugins: emptyActivatedPlugins,
      automatedSuitesLifetime: 0,
      hasTemplateConfigurablePluginBinding: false,
    },
    {
      id: 2, uri: '/projects/projet-2', name: 'projet-2', label: 'PROJET-2', requirementCategoryId: 1,
      testCaseNatureId: 1,
      testCaseTypeId: 1,
      allowAutomationWorkflow: false,
      customFieldBindings: emptyBindings,
      permissions: NO_PERMISSIONS,
      bugTrackerBinding: null,
      milestoneBindings: [],
      taServerId: 1,
      automationWorkflowType: 'NATIVE',
      disabledExecutionStatus: [],
      attachmentList: {id: null, attachments: []},
      description: '',
      createdBy: null,
      createdOn: null,
      lastModifiedBy: null,
      lastModifiedOn: null,
      linkedTemplate: null,
      linkedTemplateId: null,
      template: false,
      keywords: [],
      bddScriptLanguage: 'ENGLISH',
      allowTcModifDuringExec: false,
      activatedPlugins: emptyActivatedPlugins,
      automatedSuitesLifetime: 0,
      hasTemplateConfigurablePluginBinding: false,
    },
    {
      id: 3, uri: '/projects/projet-3', name: 'projet-3', label: 'PROJET-3', requirementCategoryId: 1,
      testCaseNatureId: 1,
      testCaseTypeId: 1,
      allowAutomationWorkflow: false,
      customFieldBindings: emptyBindings,
      permissions: NO_PERMISSIONS,
      bugTrackerBinding: null,
      milestoneBindings: [],
      taServerId: 1,
      automationWorkflowType: 'NATIVE',
      disabledExecutionStatus: [],
      attachmentList: {id: null, attachments: []},
      description: '',
      createdBy: null,
      createdOn: null,
      lastModifiedBy: null,
      lastModifiedOn: null,
      linkedTemplate: null,
      linkedTemplateId: null,
      template: false,
      keywords: [],
      bddScriptLanguage: 'ENGLISH',
      allowTcModifDuringExec: false,
      activatedPlugins: emptyActivatedPlugins,
      automatedSuitesLifetime: 0,
      hasTemplateConfigurablePluginBinding: false,
    },
    {
      id: 4, uri: '/projects/projet-4', name: 'projet-4', label: 'PROJET-4', requirementCategoryId: 1,
      testCaseNatureId: 1,
      testCaseTypeId: 1,
      allowAutomationWorkflow: false,
      customFieldBindings: emptyBindings,
      permissions: NO_PERMISSIONS,
      bugTrackerBinding: null,
      milestoneBindings: [],
      taServerId: 1,
      automationWorkflowType: 'NATIVE',
      disabledExecutionStatus: [],
      attachmentList: {id: null, attachments: []},
      description: '',
      createdBy: null,
      createdOn: null,
      lastModifiedBy: null,
      lastModifiedOn: null,
      linkedTemplate: null,
      linkedTemplateId: null,
      template: false,
      keywords: [],
      bddScriptLanguage: 'ENGLISH',
      allowTcModifDuringExec: false,
      activatedPlugins: emptyActivatedPlugins,
      automatedSuitesLifetime: 0,
      hasTemplateConfigurablePluginBinding: false,
    },
    {
      id: 5, uri: '/projects/projet-5', name: 'projet-5', label: 'PROJET-5', requirementCategoryId: 1,
      testCaseNatureId: 1,
      testCaseTypeId: 1,
      allowAutomationWorkflow: false,
      customFieldBindings: emptyBindings,
      permissions: NO_PERMISSIONS,
      bugTrackerBinding: null,
      milestoneBindings: [],
      taServerId: 1,
      automationWorkflowType: 'NATIVE',
      disabledExecutionStatus: [],
      attachmentList: {id: null, attachments: []},
      description: '',
      createdBy: null,
      createdOn: null,
      lastModifiedBy: null,
      lastModifiedOn: null,
      linkedTemplate: null,
      linkedTemplateId: null,
      template: false,
      keywords: [],
      bddScriptLanguage: 'ENGLISH',
      allowTcModifDuringExec: false,
      activatedPlugins: emptyActivatedPlugins,
      automatedSuitesLifetime: 0,
      hasTemplateConfigurablePluginBinding: false,
    },
    {
      id: 6, uri: '/projects/projet-6', name: 'projet-6', label: 'PROJET-6', requirementCategoryId: 1,
      testCaseNatureId: 1,
      testCaseTypeId: 1,
      allowAutomationWorkflow: false,
      customFieldBindings: emptyBindings,
      permissions: NO_PERMISSIONS,
      bugTrackerBinding: null,
      milestoneBindings: [],
      taServerId: 1,
      automationWorkflowType: 'NATIVE',
      disabledExecutionStatus: [],
      attachmentList: {id: null, attachments: []},
      description: '',
      createdBy: null,
      createdOn: null,
      lastModifiedBy: null,
      lastModifiedOn: null,
      linkedTemplate: null,
      linkedTemplateId: null,
      template: false,
      keywords: [],
      bddScriptLanguage: 'ENGLISH',
      allowTcModifDuringExec: false,
      activatedPlugins: emptyActivatedPlugins,
      automatedSuitesLifetime: 0,
      hasTemplateConfigurablePluginBinding: false,
    }];
}
