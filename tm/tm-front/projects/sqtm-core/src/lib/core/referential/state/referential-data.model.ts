import {Project} from '../../../model/project/project.model';
import {InfoList} from '../../../model/infolist/infolist.model';
import {AuthenticatedUser} from '../../../model/user/authenticated-user.model';
import {CustomField} from '../../../model/customfield/customfield.model';
import {GlobalConfiguration} from './global-configuration.state';
import {BugTracker} from '../../../model/bugtracker/bug-tracker.model';
import {Milestone} from '../../../model/milestone/milestone.model';
import {TestAutomationServer, TestAutomationServerKind} from '../../../model/test-automation/test-automation-server.model';
import {RequirementVersionLinkType} from '../../../model/requirement/requirement-version-link-type.model';
import {LicenseInformation} from './license-information.state';
import {AutomatedTestTechnology} from '../../../model/automation/automated-test-technology.model';
import {WorkspaceWizard} from '../../../model/workspace-wizard/workspace-wizard.model';
import {ScmServer} from '../../../model/scm-server/scm-server.model';
import {TemplateConfigurablePlugin} from '../../../model/plugin/template-configurable-plugin.model';


/**
 * Class representing the ReferentialData fetched from server.
 */
export class ReferentialData {
  projects: Project[];
  infoLists: InfoList[];
  filteredProjectIds: number[];
  projectFilterStatus: boolean;
  user: AuthenticatedUser;
  customFields: CustomField[];
  globalConfiguration: GlobalConfiguration;
  bugTrackers: BugTracker[];
  milestones: Milestone[];
  automationServers: TestAutomationServer[];
  requirementVersionLinkTypes: RequirementVersionLinkType[];
  workspacePlugins: WorkspacePlugin[];
  workspaceWizards: WorkspaceWizard[];
  licenseInformation: LicenseInformation;
  automatedTestTechnologies: AutomatedTestTechnology[];
  availableTestAutomationServerKinds: TestAutomationServerKind[];
  scmServers: ScmServer[];
  canManageLocalPassword: boolean;
  templateConfigurablePlugins: TemplateConfigurablePlugin[];
}

export interface WorkspacePlugin {
  id: string;
  workspaceId: string;
  name: string;
  iconName: string;
  tooltip: string;
  theme: string;
  url: string;
}
