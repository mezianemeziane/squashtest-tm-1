import {createFeatureSelector, Selector} from '@ngrx/store';
import {initialUserState, UserState} from './user.state';
import {GlobalConfiguration, GlobalConfigurationState, provideGlobalConfigurationInitialState} from './global-configuration.state';
import {LicenseInformationState, provideLicenseInformationInitialState} from './license-information.state';
import {CustomFieldState, initialCustomFieldState} from './custom-field-state';
import {TestAutomationServerKind} from '../../../model/test-automation/test-automation-server.model';
import {TemplateConfigurablePlugin} from '../../../model/plugin/template-configurable-plugin.model';

export interface AdminReferentialDataState {
  loaded: boolean;
  userState: UserState;
  globalConfigurationState: GlobalConfigurationState;
  licenseInformation: LicenseInformationState;
  customFieldState: CustomFieldState;
  availableTestAutomationServerKinds: TestAutomationServerKind[];
  canManageLocalPassword: boolean;
  templateConfigurablePlugins: TemplateConfigurablePlugin[];
}

export type AdminReferentialDataStateReadOnly = Readonly<AdminReferentialDataState>;

export function initialAdminReferentialDataState(): AdminReferentialDataStateReadOnly {
  return {
    loaded: false,
    userState: initialUserState(),
    globalConfigurationState: provideGlobalConfigurationInitialState(),
    licenseInformation: provideLicenseInformationInitialState(),
    customFieldState: initialCustomFieldState(),
    availableTestAutomationServerKinds: [],
    canManageLocalPassword: false,
    templateConfigurablePlugins: [],
  };
}

export const getAdminLoadedState: Selector<AdminReferentialDataState, boolean> =
  createFeatureSelector<AdminReferentialDataState, boolean>('loaded');

export const getAdminUserState: Selector<AdminReferentialDataState, UserState> =
  createFeatureSelector<AdminReferentialDataState, UserState>('userState');

export const getGlobalConfiguration: Selector<AdminReferentialDataState, GlobalConfiguration> =
  createFeatureSelector<AdminReferentialDataState, GlobalConfiguration>('globalConfigurationState');

export const getLicenseInformation: Selector<AdminReferentialDataState, LicenseInformationState> =
  createFeatureSelector<AdminReferentialDataState, LicenseInformationState>('licenseInformation');
