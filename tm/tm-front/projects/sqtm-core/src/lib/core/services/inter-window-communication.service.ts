import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {filter} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InterWindowCommunicationService {

  private _messageQueue = new Subject<InterWindowMessages>();

  public interWindowMessages$: Observable<InterWindowMessages> = this._messageQueue.asObservable();

  private readonly squashTmMessageFunctionKey = 'squashTmMessage';

  constructor() {
    const self = this;
    window[(this.squashTmMessageFunctionKey)] = function (message: InterWindowMessages) {
      self._messageQueue.next(message);
    };
  }

  isAbleToSendMessagesToOpener() {
    let able = false;
    if (window && window.opener) {
      const messageFunction = window.opener[this.squashTmMessageFunctionKey];
      if (messageFunction && typeof messageFunction === 'function') {
        able = true;
      }
    }
    return able;
  }

  sendMessage(message: InterWindowMessages) {
    if (this.isAbleToSendMessagesToOpener()) {
      const messageFunction = window.opener[this.squashTmMessageFunctionKey];
      messageFunction(message);
    }
  }

  isAbleToSendMessagesToParent() {
    let able = false;
    if (window && window.parent) {
      const messageFunction = window.parent[this.squashTmMessageFunctionKey];
      if (messageFunction && typeof messageFunction === 'function') {
        able = true;
      }
    }
    return able;
  }

  sendMessageToParent(message: InterWindowMessages) {
    if (this.isAbleToSendMessagesToParent()) {
      const messageFunction = window.parent[this.squashTmMessageFunctionKey];
      messageFunction(message);
    }
  }


}

export class InterWindowMessages {
  constructor(private readonly type: InterWindowMessageTypes, public readonly payload: any = {}) {
  }

  isTypeOf(type: InterWindowMessageTypes): boolean {
    return this.type === type;
  }
}

export type InterWindowMessageTypes =
  'EXECUTION-STEP-CHANGED'
  | 'MODIFICATION-DURING-EXECUTION'
  | 'MODIFICATION-DURING-EXECUTION-STEP'
  | 'REQUIRE-MODIFICATION-DURING-EXECUTION-PROLOGUE'
  | 'REQUIRE-MODIFICATION-DURING-EXECUTION-STEP'
  | 'REQUIRE-OPENING-NEW-WINDOW'
  | 'PLUGIN-CLOSE-DIALOG'
  | 'OAUTH-CONNECT-SUCCESS';
