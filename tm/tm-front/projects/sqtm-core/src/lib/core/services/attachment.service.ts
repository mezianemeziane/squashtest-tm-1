import {Injectable} from '@angular/core';
import {RestService} from './rest.service';
import {Observable} from 'rxjs';
import {UploadSummary} from '../../model/attachment/upload-summary.model';
import {HttpEvent} from '@angular/common/http';
import {PersistedAttachment} from '../../model/attachment/attachment.model';

@Injectable({
  providedIn: 'root'
})
export class AttachmentService {

  constructor(private restService: RestService) {
  }

  uploadAttachments(files: File[], attachmentListId: number, entityId: number, entityType: string): Observable<HttpEvent<UploadSummary[]>> {
    const formData = new FormData();
    for (const file of files) {
      formData.append('attachment[]', file, file.name);
    }

    formData.append('entityId', entityId.toString());
    formData.append('entityType', entityType);

    return this.restService.postFile<UploadSummary[]>(
      ['attach-list', attachmentListId.toString(), 'attachments', 'upload'], formData);
  }

  deleteAttachments(attachmentIds: number[], attachmentListId: number, entityId: number, entityType: string): Observable<any> {
    const options = {
      params:
        {
          entityId: entityId.toString(),
          entityType
        }
    };
    return this.restService.delete(['attach-list', attachmentListId.toString(), 'attachments', attachmentIds.join(',')], options);
  }

  getAttachmentDownloadURL(attachmentListId: number, attachment: PersistedAttachment) {
    return `${this.restService.backendRootUrl}attach-list/${attachmentListId}/attachments/download/${attachment.id}`;
  }
}
