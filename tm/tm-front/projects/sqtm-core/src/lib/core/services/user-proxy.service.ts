import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {filter} from 'rxjs/operators';
import {AuthenticatedUser} from '../../model/user/authenticated-user.model';

/**
 * This class is used to resolve circular dep between ReferentialDataService and LocalPersistenceService.
 * The ReferentialDataService publish the authuser, so the LocalPersistenceService can use it without injecting ReferentialDataService
 * And thus, ReferentialDataService can use LocalPersistenceService to store some state part (Milestone filters...)
 */
@Injectable({
  providedIn: 'root'
})
export class UserProxyService {

  private authenticatedUser = new BehaviorSubject<AuthenticatedUser>(null);

  public authenticatedUser$ = this.authenticatedUser.asObservable().pipe(
    filter(authUser => Boolean(authUser))
  );

  constructor() {
  }

  public publishUser(authenticatedUser: AuthenticatedUser) {
    this.authenticatedUser.next(authenticatedUser);
  }
}
