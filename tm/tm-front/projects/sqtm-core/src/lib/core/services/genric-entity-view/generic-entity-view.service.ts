import {Observable, of, Subject, throwError} from 'rxjs';
import {createStore, Store, StoreOptions} from '../../store/store';
import {RestService} from '../rest.service';

import {AttachmentService} from '../attachment.service';
import {TranslateService} from '@ngx-translate/core';
import {EntityViewAttachmentHelperService} from '../entity-view/entity-view-attachment-helper.service';
import {catchError, concatMap, filter, map, switchMap, take, withLatestFrom} from 'rxjs/operators';
import decamelize from 'decamelize';
import {HttpErrorResponse} from '@angular/common/http';
import {FieldValidationError, SquashError, SquashFieldError} from '../../../model/error/error.model';
import {createEntityAdapter, Update} from '@ngrx/entity';
import {entityViewLogger} from '../entity-view/entity.logger';
import {
  EditableGenericEntityFieldState,
  GenericEntityViewComponentData,
  GenericEntityViewState,
  SqtmGenericEntityState
} from './generic-entity-view-state';
import {updateGenericEntityInViewState} from '../entity-view/generic-entity-view.utils';
import {UploadAttachmentEvent} from '../entity-view/upload-attachment.event';
import {attachmentEntityAdapter, AttachmentState} from '../entity-view/entity-view.state';
import {PersistedAttachment} from '../../../model/attachment/attachment.model';

const logger = entityViewLogger.compose('AdminEntityViewService');

export abstract class GenericEntityViewService<S extends SqtmGenericEntityState, T extends string> {


  public componentData$: Observable<GenericEntityViewComponentData<S, T>>;

  private _externalRefreshRequired = new Subject<{ id: number, key?: keyof S, value?: any }>();
  public externalRefreshRequired$: Observable<{ id: number, key?: keyof S, value?: any }> = this._externalRefreshRequired.asObservable();

  protected readonly store: Store<GenericEntityViewState<S, T>>;
  protected readonly state$: Observable<GenericEntityViewState<S, T>>;

  protected fieldEntityAdapter = createEntityAdapter<EditableGenericEntityFieldState<S>>();

  protected readonly rootUrl: string;
  protected readonly entityType: string;

  /**
   * Set a list of simple attributes that will cause externalRefreshRequired$ emissions.
   * It's mainly used when the view must update other elements (trees...)
   * @param value a list of attributes names
   */
  set simpleAttributeRequiringRefresh(value: (keyof S)[]) {
    this._simpleAttributeRequiringRefresh = value;
  }

  private _simpleAttributeRequiringRefresh: (keyof S)[] = [];

  protected constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    storeOptions?: StoreOptions,
  ) {
    if (logger.isDebugEnabled()) {
      logger.debug(`Init store with conf : `, [storeOptions]);
    }

    const initialState = this.getInitialState();
    this.rootUrl = this.getRootUrl(initialState);
    this.entityType = initialState.type;
    this.store = createStore<GenericEntityViewState<S, T>>(initialState, storeOptions);
    this.state$ = this.store.state$;

    this.initializeComponentData();
  }

  protected initializeComponentData() {
    this.componentData$ = this.store.state$.pipe(
      filter(state => Boolean(this.getEntity(state)) && Boolean(this.getEntity(state).id)),
      map((state: GenericEntityViewState<S, T>) => {
          const componentData: GenericEntityViewComponentData<S, T> = {
            ...state,
            type: state.type,
          };
          return componentData;
        }
      )
    );
  }

  abstract getInitialState(): GenericEntityViewState<S, T>;

  protected abstract getRootUrl(initialState?): string;

  complete() {
    this.store.complete();
  }

  initializeEntityState(entityState: S): void {
    this.state$.pipe(
      take(1),
      map((state: GenericEntityViewState<S, T>) => {
        const nextState = {...state};
        nextState[this.entityType] = entityState;
        return nextState;
      })
    ).subscribe(state => {
      this.commit(state);
    });
  }

  getEntity(evm: GenericEntityViewState<S, T>): S {
    const type: string = evm.type;
    return evm[type];
  }

  update<K extends keyof S>(key: K, value: S[K]) {
    this.state$.pipe(
      take(1),
      filter((state) => {
        const entity = this.getEntity(state);
        const keys = Object.keys(entity);
        if (!keys.includes(key as string)) {
          console.error(`Unknown property name "${key}" into ${JSON.stringify(entity)}`);
          return false;
        }
        return true;
      }),
      concatMap(() => this.doUpdate(key, value))
    ).subscribe(state => {
      this.commit(state);
      if (this._simpleAttributeRequiringRefresh.includes(key)) {
        this.requireExternalUpdate(this.getEntity(state).id, key, value);
      }
    });
  }

  addAttachments(files: File[], attachmentListId: number): void {
    if (logger.isDebugEnabled()) {
      logger.debug(`Adding files to entity : `, [files]);
    }

    this.store.state$.pipe(
      take(1),
      switchMap((state: GenericEntityViewState<S, T>) => {
        const entity = this.getEntity(state);
        return this.attachmentHelper.addAttachments(
          files,
          attachmentListId,
          this.getAttachmentPermissionHolderEntityId(entity),
          this.getAttachmentPermissionHolderEntityType(state));
      }),
      withLatestFrom(this.store.state$)
    ).subscribe(([event, state]: [UploadAttachmentEvent, GenericEntityViewState<S, T>]) => {
      const entity = this.getEntity(state);
      const attachmentState = this.attachmentHelper.mapUploadEventToState(entity.attachmentList.attachments, event);
      const nextState = this.updateAttachmentState(state, attachmentState);
      this.commit(nextState);
    });
  }

  protected getAttachmentPermissionHolderEntityType(state: { type: T }): string {
    return state.type;
  }

  protected getAttachmentPermissionHolderEntityId(entity: S) {
    return entity.id;
  }

  updateAttachmentState(state: GenericEntityViewState<S, T>, attachmentState: AttachmentState) {
    const entity = {...this.getEntity(state)};
    const attachmentList = {...entity.attachmentList};
    attachmentList.attachments = {...attachmentState};
    entity.attachmentList = attachmentList;
    return this.updateEntity(entity, state);
  }

  markAttachmentsToDelete(ids: string[]) {
    this.changeAttachmentToDelete(ids, true);
  }

  cancelDeleteAttachments(ids: string[]) {
    this.changeAttachmentToDelete(ids, false);
  }

  changeAttachmentToDelete(ids: string[], pendingDelete: boolean) {
    this.state$.pipe(
      take(1),
      map((state => {
        const entity = this.getEntity(state);
        const attachmentState = this.changeAttachmentDeleteStatus(entity.attachmentList.attachments, ids, pendingDelete);
        return this.updateAttachmentState(state, attachmentState);
      }))).subscribe(state => this.commit(state));
  }

  protected changeAttachmentDeleteStatus(state: AttachmentState, ids: string[], pendingDelete: boolean): AttachmentState {
    const changes: Update<PersistedAttachment>[] = ids.map(id => {
      return {id, changes: {pendingDelete}};
    });
    return attachmentEntityAdapter.updateMany(changes, state);
  }

  deleteAttachments(ids: string[], attachmentListId: number) {
    const idsAsNumber = ids.map(id => parseInt(id, 10));

    this.state$.pipe(
      take(1),
      switchMap((state: GenericEntityViewState<S, T>) => {
        const entity = this.getEntity(state);
        return this.attachmentHelper.eraseAttachments(
          idsAsNumber,
          attachmentListId,
          this.getAttachmentPermissionHolderEntityId(entity),
          this.getAttachmentPermissionHolderEntityType(state));
      }),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, GenericEntityViewState<S, T>]) => {
        const entity = {...this.getEntity((state))};
        entity.attachmentList.attachments = attachmentEntityAdapter.removeMany(ids, entity.attachmentList.attachments);
        return this.updateEntity(entity, state);
      })
    ).subscribe((state: GenericEntityViewState<S, T>) => {
      this.commit(state);
    });
  }

  removeRejectedAttachments(ids: string[]) {
    this.state$.pipe(
      take(1),
      map((state) => {
        const entity = {...this.getEntity(state)};
        entity.attachmentList.attachments = attachmentEntityAdapter.removeMany(ids, entity.attachmentList.attachments);
        return this.updateEntity(entity, state);
      })).subscribe(state => this.commit(state));
  }


  commit(state: GenericEntityViewState<S, T>) {
    this.store.commit(state);
  }


  private doUpdate(key: keyof S, value: any): Observable<GenericEntityViewState<S, T>> {
    // url is snake case form of the key aka importance-auto
    const url = decamelize(key as string, '-');
    const update: any = {};
    // key is camel case form of the key aka importanceAuto
    update[key] = value;
    return this.state$.pipe(
      take(1),
      concatMap(state => this.restService.post([this.rootUrl, this.getEntity(state).id.toString(), url], update).pipe(
        map(() => {
          const entity = {...this.getEntity(state)};
          let nextState = {...state};
          (entity [key] as any) = value;
          nextState[this.entityType] = entity;
          nextState = this.changeFieldStatusToValid(key, nextState);
          return nextState;
        }),
        catchError(err => this.doSquashErrorTreatment(key, state, err))
      ))
    );
  }

  // Doing an instance version for easier use in subclass
  // However, this method is pure
  protected updateEntity(entity: S, state: GenericEntityViewState<S, T>): GenericEntityViewState<S, T> {
    return updateGenericEntityInViewState<S, T>(entity, state);
  }

  private changeFieldStatusToValid(key: keyof S, state: GenericEntityViewState<S, T>): GenericEntityViewState<S, T> {
    let fieldState = state.uiState.fieldState;
    fieldState = this.fieldEntityAdapter.upsertOne({
      id: key,
      status: 'VALID',
      errorMsg: [],
      errorMsgKey: []
    }, fieldState);
    return {...state, uiState: {...state.uiState, fieldState}};
  }

  private changeFieldStatusToServerError(
    key: keyof S,
    state: GenericEntityViewState<S, T>,
    fieldValidationError: FieldValidationError): Observable<GenericEntityViewState<S, T>> {
    let fieldState = state.uiState.fieldState;
    fieldState = this.fieldEntityAdapter.upsertOne({
      id: key,
      status: 'SERVER_ERROR',
      errorMsg: [this.translateService.instant(fieldValidationError.i18nKey)],
      errorMsgKey: []
    }, fieldState);
    return of({...state, uiState: {...state.uiState, fieldState}});
  }

  private doSquashErrorTreatment(key: keyof S, state: GenericEntityViewState<S, T>, error) {
    if (error instanceof HttpErrorResponse && error.status === 412 && error.error.squashTMError) {
      const squashError: SquashError = error.error.squashTMError;
      if (squashError.kind === 'FIELD_VALIDATION_ERROR') {
        const fieldValidationErrors = (squashError as SquashFieldError).fieldValidationErrors;
        return this.changeFieldStatusToServerError(key, state, fieldValidationErrors[0]);
      }
      return throwError(error);
    }
    return throwError(error);
  }


  protected requireExternalUpdate(id: number, key?: keyof S, value?: any) {
    this._externalRefreshRequired.next({id, key, value});
  }
}











