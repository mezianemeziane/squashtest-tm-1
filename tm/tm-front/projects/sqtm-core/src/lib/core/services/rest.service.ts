import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {coreLogger} from '../core.logger';

const logger = coreLogger.compose('RestService');

@Injectable({
  providedIn: 'root'
})
export class RestService {

  set backendContextPath(value: string) {
    logger.debug(`Setting backendContextPath to ${value}`);
    if (Boolean(this._backendContextPath)) {
      throw Error(`Backend context path was already defined to ${this._backendContextPath}`);
    }
    this._backendContextPath = value;
  }

  private _backendContextPath: string;

  set backendRootUrl(value: string) {
    logger.debug(`Setting backendRootUrl to ${value}`);
    if (Boolean(this._backendRootUrl)) {
      throw Error(`Backend root url was already defined to ${this._backendRootUrl}`);
    }
    this._backendRootUrl = value;
  }

  get backendRootUrl(): string {
    return `${this._backendContextPath}${this._backendRootUrl}/`;
  }

  private _backendRootUrl: string;

  constructor(private http: HttpClient) {

  }

  get<T>(parts: string[], params?: { [param: string]: any }): Observable<T> {
    const options = {};
    if (params) {
      let headers = new HttpHeaders();
      headers = headers.set('Content-Type', 'application/x-www-form-urlencoded');
      const paramKeys = Object.keys(params);
      let httpParams = new HttpParams();
      paramKeys.forEach((paramKey) => {
        const httpParamValue = params[paramKey];
        if (typeof httpParamValue === 'string') {
          httpParams = httpParams.append(paramKey, httpParamValue);
        } else if (typeof httpParamValue === 'object') {
          httpParams = httpParams.append(paramKey, JSON.stringify(httpParamValue));
        } else {
          logger.debug(JSON.stringify(httpParams));
          httpParams = httpParams.append(paramKey, httpParamValue.toString());
        }
      });
      options['params'] = httpParams;
      options['headers'] = headers;
    }

    const url = this.backendRootUrl + parts.join('/');
    logger.debug(`Executing GET request to url ${url}. Options : `, [{options}]);
    return this.http.get<T>(url, options);
  }

  /**
   * Methode that allow get http request that are not handled by classical ServerInternalErrorResponseInterceptor.
   * It it not the default behavior, if an error is expected you should add Exceptions server side and return "nice" error code, like 412
   * with a comprehensible error message for user.
   * However in some case, like bugtracker connection, it make sense to let the bugtracker rest API error 500 be returned to front end,
   * and thus be handled specifically by the calling component and not the generic interceptor.
   * @param parts request path parts
   * @param params request params
   */
  getWithoutErrorHandling<T>(parts: string[], params?: { [param: string]: any }): Observable<T> {
    return this.get(parts, this.addErrorIsHandledParam(params));
  }

  post<T>(parts: string[], body?: { [param: string]: any }, options?: {
    headers?: HttpHeaders, params?: HttpParams | {
      [param: string]: string | string[];
    }
  }): Observable<T> {
    const url = this.backendRootUrl + parts.join('/');
    logger.debug(`Executing POST request to url ${url}. Body and options : `, [{body, options}]);
    return this.http.post<T>(url, body, options);
  }

  postFile<T>(parts: string[], body: any, options?: { headers?: HttpHeaders }): Observable<HttpEvent<T>> {
    const url = this.backendRootUrl + parts.join('/');
    const request = new HttpRequest('POST', url, body, {...options, reportProgress: true});
    return this.http.request<T>(request);
  }

  delete<T>(parts: string[], options?: {
    headers?: HttpHeaders, params?: HttpParams | {
      [param: string]: string | string[];
    }
  }): Observable<T> {
    const url = this.backendRootUrl + parts.join('/');
    logger.debug(`Executing DELETE request to url ${url}. Options : `, [{options}]);
    return this.http.delete<T>(url, options);
  }

  private addErrorIsHandledParam(params?: { [params: string]: any }) {
    let newParams = {};
    if (params) {
      newParams = params;
    }
    newParams[FRONT_END_ERROR_IS_HANDLED_PARAM] = true;
    return newParams;
  }

  buildExportUrlWithParams(path: string, params: {}): string {
    logger.debug(`Building export url to ${path}`);
    let url = `${this.backendRootUrl}${path}?`;
    const paramsKeys = Object.keys(params);

    const paramUrl = [];
    paramsKeys.forEach(key => {
      paramUrl.push(`${key}=${params[key]}`);
    });
    const paramString = paramUrl.join('&');
    url = url + paramString;
    logger.debug(`Built export url to ${url}`);
    return url;
  }
}

export const FRONT_END_ERROR_IS_HANDLED_PARAM = 'frontEndErrorIsHandled';
export const STACK_TRACE_PARAM = 'Stack-Trace';
