import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {Observable} from 'rxjs';
import {
  ReportDefinitionModel,
  ReportDefinitionViewModel,
  ReportWorkbenchData
} from '../../../model/custom-report/report-definition.model';

@Injectable({
  providedIn: 'root'
})
export class ReportDefinitionService {

  constructor(private restService: RestService) {
  }

  getWorkbenchData(customReportLibraryNodeId: number): Observable<ReportWorkbenchData> {
    return this.restService.get(['report-workbench', customReportLibraryNodeId.toString()]);
  }

  getReportDefinitionViewModel(customReportLibraryNodeId: number): Observable<ReportDefinitionViewModel> {
    return this.restService.get(['report-definition-view', customReportLibraryNodeId.toString()]);
  }

  getReportDefinitionViewModelByReportDefinitionId(reportDefinitionId: number): Observable<ReportDefinitionViewModel> {
    return this.restService.get(['report-definition-view/report-definition', reportDefinitionId.toString()]);
  }

  renameReportDefinitionViewModel(customReportLibraryNodeId: number, name: string): Observable<void> {
    return this.restService.post(['report-definition-view', customReportLibraryNodeId.toString(), 'name'], {name});
  }

  saveNewReportDefinition(parentCustomReportLibraryNodeId: number, reportDefinition: ReportDefinitionModel): Observable<{ id: number }> {
    return this.restService.post(['report-workbench', 'new', parentCustomReportLibraryNodeId.toString()], reportDefinition);
  }

  updateReportDefinition(parentCustomReportLibraryNodeId: number, reportDefinition: ReportDefinitionModel): Observable<{ id: number }> {
    return this.restService.post(['report-workbench', 'update', parentCustomReportLibraryNodeId.toString()], reportDefinition);
  }
}
