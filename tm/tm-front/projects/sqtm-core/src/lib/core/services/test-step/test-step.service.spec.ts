import {TestBed} from '@angular/core/testing';

import {TestStepService} from './test-step.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../../ui/testing-utils/testing-utils.module';

describe('TestStepService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, TestingUtilsModule],
    providers: [TestStepService]
  }));

  it('should be created', () => {
    const service: TestStepService = TestBed.inject(TestStepService);
    expect(service).toBeTruthy();
  });
});
