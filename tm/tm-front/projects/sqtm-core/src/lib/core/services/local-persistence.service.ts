import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {map, take, tap} from 'rxjs/operators';
import {AuthenticatedUser} from '../../model/user/authenticated-user.model';
import {UserProxyService} from './user-proxy.service';

/**
 * Wrapper around LocalStorage to suffix all keys by user name.
 * So different successive users on the same browser will not have strange behaviors
 * because they are using different filter, have different permissions and so on...
 */
@Injectable({
  providedIn: 'root'
})
export class LocalPersistenceService {

  constructor(private userProxyService: UserProxyService) {
  }

  get<T>(index: string): Observable<T> {
    return this.userProxyService.authenticatedUser$.pipe(
      take(1),
      map(authUser => this.appendUserLogin(authUser, index)),
      map(prefixedIndex => {
        const str = window.localStorage.getItem(prefixedIndex);
        if (Boolean(str) && str.length > 0) {
          return JSON.parse(str) as T;
        }
        return null;
      })
    );
  }

  set<T>(index: string, value: T): Observable<any> {
    return this.userProxyService.authenticatedUser$.pipe(
      take(1),
      map(authUser => this.appendUserLogin(authUser, index)),
      tap(prefixedIndex => window.localStorage.setItem(prefixedIndex, JSON.stringify(value)))
    );
  }

  clearAll(): Observable<undefined> {
    window.localStorage.clear();
    return of(null);
  }

  private appendUserLogin(authUser: AuthenticatedUser, index: string): string {
    if (Boolean(authUser) && Boolean(authUser.username)) {
      return `${authUser.username}__${index}`;
    }
    return index;
  }
}
