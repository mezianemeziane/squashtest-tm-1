import {TestBed} from '@angular/core/testing';

import {ActionErrorDisplayService} from './action-error-display.service';
import {OverlayModule} from '@angular/cdk/overlay';

describe('ActionErrorDisplayService', () => {
  let service: ActionErrorDisplayService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule]
    });
    service = TestBed.inject(ActionErrorDisplayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
