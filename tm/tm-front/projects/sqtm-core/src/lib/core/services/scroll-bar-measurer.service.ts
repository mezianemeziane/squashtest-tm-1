import {Inject, Injectable} from '@angular/core';
import {DOCUMENT} from '@angular/common';

/** @dynamic */
@Injectable({
  providedIn: 'root'
})
// Inspired by https://davidwalsh.name/detect-scrollbar-width
export class ScrollBarMeasurerService {

  private _scrollBarWidth: number;

  constructor(@Inject(DOCUMENT) private document: Document) {
    const scrollDiv = this.document.createElement('div');
    // Global style as we are in a service.
    scrollDiv.className = 'scrollbar-measure';
    this.document.body.appendChild(scrollDiv);
    this.scrollBarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    this.document.body.removeChild(scrollDiv);
  }

  get scrollBarWidth(): number {
    return this._scrollBarWidth || 18;
  }

  set scrollBarWidth(scrollBarWidth: number) {
    this._scrollBarWidth = scrollBarWidth;
  }
}
