import {coreLogger} from '../../core.logger';

export const entityViewLogger = coreLogger.compose('entity-view');
