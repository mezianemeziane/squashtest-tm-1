import {Injectable} from '@angular/core';
import {attachmentEntityAdapter, AttachmentState} from './entity-view.state';
import {AttachmentService} from '../attachment.service';
import {Observable} from 'rxjs';
import {ReferentialDataService} from '../../referential/services/referential-data.service';
import {filter, map, switchMap, take} from 'rxjs/operators';
import {GlobalConfiguration} from '../../referential/state/global-configuration.state';
import {TranslateService} from '@ngx-translate/core';
import uuid from 'uuid';
import {HttpEvent, HttpEventType, HttpProgressEvent, HttpResponse, HttpSentEvent} from '@angular/common/http';
import {UploadSummary} from '../../../model/attachment/upload-summary.model';
import {StartUpload, UploadAttachmentEvent, UploadComplete, UploadProgress} from './upload-attachment.event';
import {EntityState, Update} from '@ngrx/entity';
import {
  Attachment,
  AttachmentModel,
  PersistedAttachment,
  RejectedAttachment,
  UploadingAttachment
} from '../../../model/attachment/attachment.model';

type UploadHttpEvent = HttpSentEvent | HttpProgressEvent | HttpResponse<UploadSummary[]>;

@Injectable({
  providedIn: 'root'
})
export class EntityViewAttachmentHelperService {

  constructor(private attachmentService: AttachmentService, private referentialDataService: ReferentialDataService,
              private translateService: TranslateService) {
  }

  public initializeAttachmentState(attachmentModels: AttachmentModel[]): AttachmentState {
    const initialState = attachmentEntityAdapter.getInitialState();
    const attachments: PersistedAttachment[] = attachmentModels.map(attachment => {
      return {...attachment, id: attachment.id.toString(), kind: 'persisted-attachment', pendingDelete: false};
    });
    return attachmentEntityAdapter.setAll(attachments, initialState);
  }

  addAttachments(files: File[], attachmentListId: number, entityId: number, entityType: string): Observable<UploadAttachmentEvent> {
    return this.getGlobalConfiguration().pipe(
      take(1),
      map((globalConfiguration: GlobalConfiguration) => {
        return this.validateFiles(files, globalConfiguration);
      }),
      switchMap((fileCandidates: FileCandidate[]) => {
        const validFiles = fileCandidates.filter(candidate => candidate.isValid());
        const validIds = validFiles.map(f => f.id);
        const invalidFiles = fileCandidates.filter(candidate => candidate.isInvalid());
        const attachments: (UploadingAttachment | RejectedAttachment)[] = this.createAttachments(validFiles, invalidFiles);
        return this.attachmentService.uploadAttachments(validFiles.map(file => file.file), attachmentListId, entityId, entityType).pipe(
          filter((event: HttpEvent<UploadSummary[]>) =>
            event.type === HttpEventType.Response
            || event.type === HttpEventType.UploadProgress
            || event.type === HttpEventType.Sent),
          map((event: UploadHttpEvent) => this.handleHttpUploadEvent(event, attachments, validIds))
        );
      })
    );
  }

  mapUploadEventToState(initialState: EntityState<Attachment>, event: UploadAttachmentEvent): EntityState<Attachment> {
    let attachmentState = {...initialState};
    switch (event.kind) {
      case 'start':
        const startUpload = event as StartUpload;
        attachmentState = attachmentEntityAdapter.addMany(startUpload.attachments, attachmentState);
        break;
      case 'progress':
        const uploadProgressEvent = event as UploadProgress;
        const updates: Update<Attachment>[] = uploadProgressEvent.attachments.map(({id, uploadProgress}) => ({
          id,
          changes: {uploadProgress}
        }));
        attachmentState = attachmentEntityAdapter.updateMany(updates, attachmentState);
        break;
      case 'complete':
        const uploadCompleteEvent = event as UploadComplete;
        attachmentState = attachmentEntityAdapter.removeMany(uploadCompleteEvent.uploadingAttachmentsToDelete, attachmentState);
        attachmentState = attachmentEntityAdapter.addMany(uploadCompleteEvent.attachments, attachmentState);
        break;
      default:
        throw Error('Unknown Event Type :' + JSON.stringify(event));
    }
    return attachmentState;
  }

  eraseAttachments(attachmentIds: number[], attachmentListId: number, entityId: number, entityType: string): Observable<any> {
    return this.attachmentService.deleteAttachments(attachmentIds, attachmentListId, entityId, entityType);
  }

  protected getGlobalConfiguration(): Observable<GlobalConfiguration> {
    return this.referentialDataService.globalConfiguration$;
  }

  private handleHttpUploadEvent(event: UploadHttpEvent, attachments: (UploadingAttachment | RejectedAttachment)[], validIds: string[]) {
    const type = event.type;
    let uploadAttachmentEvent: UploadAttachmentEvent;
    switch (type) {
      case HttpEventType.Sent:
        uploadAttachmentEvent = new StartUpload(attachments);
        break;
      case HttpEventType.UploadProgress:
        uploadAttachmentEvent = this.handleHttpProgress(event, validIds);
        break;
      case HttpEventType.Response:
        uploadAttachmentEvent = this.handleHttpComplete(event, validIds);
        break;
      default:
        throw Error('Unknown Event Type :' + JSON.stringify(event));
    }
    return uploadAttachmentEvent;
  }

  private handleHttpComplete(event: HttpSentEvent | HttpProgressEvent | HttpResponse<UploadSummary[]>, validIds: string[]): UploadComplete {
    const response = event as HttpResponse<UploadSummary[]>;
    const persistedAttachments: PersistedAttachment[] = response.body.map(uploadSummary => {
      return {
        ...uploadSummary.attachmentDto,
        id: uploadSummary.attachmentDto.id.toString(),
        kind: 'persisted-attachment',
        pendingDelete: false
      };
    });
    return new UploadComplete(persistedAttachments, validIds);
  }

  private handleHttpProgress(event: HttpSentEvent | HttpProgressEvent | HttpResponse<UploadSummary[]>, validIds: string[]): UploadProgress {
    const progressEvent = event as HttpProgressEvent;
    const uploadProgress = Math.floor((progressEvent.loaded / progressEvent.total) * 100);
    const updates = validIds.map(id => ({id, uploadProgress}));
    return new UploadProgress(updates);
  }

  validateFiles(files: File[], globalConfiguration: GlobalConfiguration): FileCandidate[] {
    const fileCandidates = [];
    for (const file of files) {
      const errors: string[] = this.validateOneFile(file, globalConfiguration);
      fileCandidates.push(new FileCandidate(file, errors));
    }
    return fileCandidates;
  }

  private validateOneFile(file: File, globalConfiguration: GlobalConfiguration) {
    const errors = [];
    if (file.size > globalConfiguration.uploadFileSizeLimit) {
      errors.push(this.translateService.instant('sqtm-core.attachment-drawer.error.file-too-big'));
    }
    const extensionPointIndex = file.name.lastIndexOf('.');
    if (extensionPointIndex === -1 || extensionPointIndex === file.name.length - 1) {
      errors.push(this.translateService.instant('sqtm-core.attachment-drawer.error.no-extension'));
    } else {
      const extension = file.name.slice(extensionPointIndex + 1);
      if (!includesIgnoreCase(globalConfiguration.uploadFileExtensionWhitelist, extension)) {
        errors.push(this.translateService.instant('sqtm-core.attachment-drawer.error.bad-extension', {extension}));
      }
    }
    return errors;
  }

  private createAttachments(validFiles: FileCandidate[], invalidFiles: FileCandidate[]): (UploadingAttachment | RejectedAttachment)[] {
    return [
      ...validFiles.map(validFile => this.createUploadingAttachment(validFile)),
      ...invalidFiles.map(invalidFile => this.createRejectedAttachment(invalidFile))
    ];
  }

  private createUploadingAttachment(validFile: FileCandidate): UploadingAttachment {
    return {
      kind: 'uploading-attachment',
      id: validFile.id,
      name: validFile.file.name,
      size: validFile.file.size,
      addedOn: new Date(),
      uploadProgress: 0
    };
  }

  private createRejectedAttachment(invalidFile: FileCandidate): RejectedAttachment {
    return {
      kind: 'rejected-attachment',
      id: invalidFile.id,
      name: invalidFile.file.name,
      size: invalidFile.file.size,
      addedOn: new Date(),
      errors: invalidFile.errors
    };
  }
}

class FileCandidate {
  readonly id: string;

  constructor(public readonly file: File, public readonly errors: string[]) {
    this.id = uuid();
  }

  isValid(): boolean {
    return this.errors.length === 0;
  }

  isInvalid(): boolean {
    return this.errors.length > 0;
  }
}

function includesIgnoreCase(stringArray: string[], item: string): boolean {
  return Boolean(stringArray.map(str => str.toLowerCase()).includes(item.toLowerCase()));
}
