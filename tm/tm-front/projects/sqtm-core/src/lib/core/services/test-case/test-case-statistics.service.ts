import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {Identifier} from '../../../model/entity.model';
import {TestCaseStatistics} from '../../../model/test-case/test-case-statistics.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TestCaseStatisticsService {

  constructor(private restService: RestService) {
  }

  fetchStatistics(references: Identifier[]): Observable<TestCaseStatistics> {
    return this.restService.post<TestCaseStatistics>(['test-case', 'statistics'], {references});
  }
}
