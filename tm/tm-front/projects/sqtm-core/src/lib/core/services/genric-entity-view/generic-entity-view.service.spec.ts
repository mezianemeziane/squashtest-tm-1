import {TestBed, waitForAsync} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CORE_MODULE_CONFIGURATION} from '../../sqtm-core.tokens';
import {defaultSqtmConfiguration} from '../../sqtm-core.module';
import {TranslateService} from '@ngx-translate/core';
import {RestService} from '../rest.service';
import {AttachmentService} from '../attachment.service';
import {of} from 'rxjs';
import {PersistedAttachment, RejectedAttachment, UploadingAttachment} from '../../../model/attachment/attachment.model';
import {GenericEntityViewService} from './generic-entity-view.service';
import {GenericEntityUiState, SqtmGenericEntityState} from './generic-entity-view-state';
import {EntityViewAttachmentHelperService} from '../entity-view/entity-view-attachment-helper.service';

interface DummyEntity extends SqtmGenericEntityState {
  name: string;
}

class DummyEntityViewService extends GenericEntityViewService<DummyEntity, 'dummy'> {
  constructor(protected restService: RestService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
  ) {
    super(
      restService,
      attachmentService,
      translateService,
      attachmentHelper
    );
  }

  getInitialState(): { [K in 'dummy']: DummyEntity } & { type: 'dummy', uiState: GenericEntityUiState<any> } {
    return {
      type: 'dummy',
      uiState: {fieldState: {ids: [], entities: {}}},
      dummy: null
    };
  }

  protected getRootUrl(initialState?): string {
    return '';
  }
}

let restServiceMock: any;
let attachmentServiceMock: any;
let translateServiceMock: any;
let attachmentHelperMock: any;


function getSimpleEntityState(): DummyEntity {
  return {
    id: 12,
    name: 'hello',
    attachmentList: {
      id: 1,
      attachments:
        {
          ids: [],
          entities: {}
        }
    }
  };
}

function getEntityStateWithAttachments() {
  const simpleEntityState = getSimpleEntityState();
  simpleEntityState.attachmentList.attachments.ids = [
    '1',
    '2',
    'def429ab-3db2-4a4a-9742-db878344fd87',
    '3a2003a4-95ed-4d21-9063-4e3eef5122a6'
  ];
  const attachment1: PersistedAttachment = {
    id: '1',
    kind: 'persisted-attachment',
    addedOn: new Date(),
    name: 'attachment1',
    size: 500,
    pendingDelete: true
  };
  const attachment2: PersistedAttachment = {
    id: '2',
    kind: 'persisted-attachment',
    addedOn: new Date(),
    name: 'attachment2',
    size: 500,
    pendingDelete: false
  };
  const rejectedAttachment: RejectedAttachment = {
    id: 'def429ab-3db2-4a4a-9742-db878344fd87',
    kind: 'rejected-attachment',
    addedOn: new Date(),
    name: 'log_jira_sync.txt',
    size: 50000000000,
    errors: ['Too Heavy']
  };
  const uploadingAttachment: UploadingAttachment = {
    id: '3a2003a4-95ed-4d21-9063-4e3eef5122a6',
    kind: 'uploading-attachment',
    addedOn: new Date(),
    name: 'hello.txt',
    size: 1,
    uploadProgress: 50
  };


  simpleEntityState.attachmentList.attachments.entities = {
    '1': attachment1,
    '2': attachment2,
    'def429ab-3db2-4a4a-9742-db878344fd87': rejectedAttachment,
    '3a2003a4-95ed-4d21-9063-4e3eef5122a6': uploadingAttachment,
  };
  return simpleEntityState;
}

describe('GenericEntityViewService', () => {
  beforeEach(() => {
    restServiceMock = jasmine.createSpyObj('restService', ['get', 'post']);
    attachmentServiceMock = jasmine.createSpyObj('attachmentService', ['uploadAttachments', 'deleteAttachments']);
    translateServiceMock = jasmine.createSpyObj('translateService', ['instant']);
    attachmentHelperMock = jasmine.createSpyObj('attachmentHelper', ['initializeAttachmentState', 'addAttachments', 'eraseAttachments']);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: CORE_MODULE_CONFIGURATION,
          useValue: defaultSqtmConfiguration
        },
        {
          provide: RestService,
          useValue: restServiceMock
        },
        {
          provide: TranslateService,
          useValue: translateServiceMock
        },
        {
          provide: AttachmentService,
          useValue: attachmentServiceMock
        },
        {
          provide: EntityViewAttachmentHelperService,
          useValue: attachmentHelperMock
        },
        {
          provide: GenericEntityViewService,
          useClass: DummyEntityViewService,
          deps: [
            RestService,
            AttachmentService,
            TranslateService,
            EntityViewAttachmentHelperService,

          ]
        }
      ]
    });
  });

  it('should be created', () => {
    const service: GenericEntityViewService<any, 'dummy'>
      = TestBed.inject<GenericEntityViewService<any, 'dummy'>>(GenericEntityViewService);
    expect(service).toBeTruthy();
  });

  it('should load entity', (done) => {
    const service: GenericEntityViewService<any, 'dummy'>
      = TestBed.inject<GenericEntityViewService<any, 'dummy'>>(GenericEntityViewService);

    let componentData;
    service.componentData$.subscribe(data => {
      componentData = data;
    });
    service.initializeEntityState(getSimpleEntityState());
    expect(componentData['dummy'].id).toEqual(12);
    expect(componentData['dummy'].name).toEqual('hello');
    done();
  });

  it('should update entity', (done) => {
    const service: GenericEntityViewService<any, 'dummy'>
      = TestBed.inject<GenericEntityViewService<any, 'dummy'>>(GenericEntityViewService);
    restServiceMock.post.and.returnValue(of(null));

    let componentData;
    service.componentData$.subscribe(data => {
      componentData = data;
    });

    service.initializeEntityState(getSimpleEntityState());
    service.update('name', 'updated');
    expect(componentData['dummy'].id).toEqual(12);
    expect(componentData['dummy'].name).toEqual('updated');
    expect(restServiceMock.post).toHaveBeenCalledTimes(1);
    done();
  });

  it('should reject invalid update entity', (done) => {
    const service: GenericEntityViewService<any, 'dummy'>
      = TestBed.inject<GenericEntityViewService<any, 'dummy'>>(GenericEntityViewService);
    let componentData;
    service.componentData$.subscribe(data => {
      componentData = data;
    });
    service.initializeEntityState(getSimpleEntityState());
    const spyCommit = spyOn(service, 'commit').and.callThrough();
    service.update('bob' as any, 'updated');
    expect(componentData['dummy'].id).toEqual(12);
    expect(componentData['dummy'].name).toEqual('hello');
    expect(restServiceMock.post).toHaveBeenCalledTimes(0);
    expect(spyCommit).toHaveBeenCalledTimes(0);
    done();
  });

  it('should mark attachment to delete', (done) => {
    const service: GenericEntityViewService<any, 'dummy'>
      = TestBed.inject<GenericEntityViewService<any, 'dummy'>>(GenericEntityViewService);

    let componentData;
    service.componentData$.subscribe(data => {
      componentData = data;
    });
    const entity = getEntityStateWithAttachments();
    const initialAttachment = entity.attachmentList.attachments.entities['1'];
    const spyCommit = spyOn(service, 'commit').and.callThrough();
    service.initializeEntityState(entity);
    service.markAttachmentsToDelete(['1']);
    expect(componentData['dummy']).not.toBe(entity);
    expect(componentData['dummy'].attachmentList).not.toBe(entity.attachmentList);
    expect(componentData['dummy'].attachmentList.attachments.ids.length).toEqual(4);
    // attachment reference must have change
    expect(componentData['dummy'].attachmentList.attachments.entities['1']).not.toBe(initialAttachment);
    expect(componentData['dummy'].attachmentList.attachments.entities['1'].pendingDelete).toBeTruthy();
    expect(spyCommit).toHaveBeenCalledTimes(2);
    done();
  });

  it('should cancel attachment to delete', (done) => {
    const service: GenericEntityViewService<any, 'dummy'>
      = TestBed.inject<GenericEntityViewService<any, 'dummy'>>(GenericEntityViewService);

    let componentData;
    service.componentData$.subscribe(data => {
      componentData = data;
    });
    const entity = getEntityStateWithAttachments();
    const initialAttachment = entity.attachmentList.attachments.entities['1'];
    const spyCommit = spyOn(service, 'commit').and.callThrough();
    service.initializeEntityState(entity);
    service.cancelDeleteAttachments(['1']);
    expect(componentData['dummy'].attachmentList.attachments.ids.length).toEqual(4);
    // attachment reference must have change
    expect(componentData['dummy'].attachmentList.attachments.entities[1]).not.toBe(initialAttachment);
    expect(componentData['dummy'].attachmentList.attachments.entities[1].pendingDelete).toBeFalsy();
    expect(spyCommit).toHaveBeenCalledTimes(2);
    done();
  });

  it('should remove attachment', (done) => {
    const service: GenericEntityViewService<any, 'dummy'>
      = TestBed.inject<GenericEntityViewService<any, 'dummy'>>(GenericEntityViewService);
    attachmentHelperMock.eraseAttachments.and.returnValue(of(null));

    let componentData;
    service.componentData$.subscribe(data => {
      componentData = data;
    });
    const simpleEntityState = getEntityStateWithAttachments();
    const spyCommit = spyOn(service, 'commit').and.callThrough();
    service.initializeEntityState(simpleEntityState);
    service.deleteAttachments(['1'], 1);
    expect(componentData['dummy'].attachmentList.attachments.ids.length).toEqual(3);
    expect(componentData['dummy'].attachmentList.attachments.entities['1']).toBeFalsy();
    expect(spyCommit).toHaveBeenCalledTimes(2);
    done();
  });

  it('should update attachment state', waitForAsync(function () {
    const service: GenericEntityViewService<any, 'dummy'>
      = TestBed.inject<GenericEntityViewService<any, 'dummy'>>(GenericEntityViewService);

    const simpleEntityState = getSimpleEntityState();
    const attachmentList = simpleEntityState.attachmentList;
    Object.freeze(attachmentList);
    const initialState = service.getInitialState();
    initialState['dummy'] = simpleEntityState;
    const updatedState = service.updateAttachmentState(initialState, {...attachmentList.attachments});
    expect(updatedState).not.toBe(initialState);
    const updatedEntity = updatedState['dummy'] as DummyEntity;
    expect(updatedEntity).not.toBe(simpleEntityState);
    expect(updatedEntity.attachmentList).not.toBe(attachmentList);
  }));
});
