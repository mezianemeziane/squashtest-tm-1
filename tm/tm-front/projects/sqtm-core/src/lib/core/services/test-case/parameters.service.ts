import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {Observable} from 'rxjs';
import {ParameterRename, TestStepModel} from '../../../model/test-case/test-step.model';

@Injectable({
  providedIn: 'root'
})
export class ParametersService {

  constructor(private restService: RestService) {
  }

  renameParameter(parameterId, paramName: string): Observable<ParameterRename> {
    return this.restService.post([`parameters/${parameterId}/rename`], {name: paramName});
  }

  changeDescription(parameterId, description: string): Observable<any> {
    return this.restService.post([`parameters/${parameterId}/description`], {description: description});
  }
}
