import {TestBed} from '@angular/core/testing';

import {ChartDefinitionService} from './chart-definition.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../../ui/testing-utils/testing-utils.module';

describe('ChartDefinitionService', () => {
  let service: ChartDefinitionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule]
    });
    service = TestBed.inject(ChartDefinitionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
