import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { AutomatedSuiteService } from './automated-suite.service';

describe('AutomatedSuiteService', () => {
  let service: AutomatedSuiteService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(AutomatedSuiteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
