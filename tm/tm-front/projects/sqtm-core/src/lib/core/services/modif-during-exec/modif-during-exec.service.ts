import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {ActionStepExecViewModel, ModifDuringExecModel} from '../../../model/modif-during-exec/modif-during-exec.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModifDuringExecService {

  constructor(private restService: RestService) {
  }

  getModifDuringExecModel(executionId: number): Observable<ModifDuringExecModel> {
    return this.restService.get<ModifDuringExecModel>(['execution', executionId.toString(), 'modification-during-execution']);
  }

  // actionStepId is the id of the test step linked to the current modified executionStep.
  getActionStepExecViewModel(executionId: number, actionStepId: number): Observable<ActionStepExecViewModel> {
    return this.restService.get<ActionStepExecViewModel>(['execution', executionId.toString(), 'modification-during-execution', 'action-step',
      actionStepId.toString()]);
  }

  checkPermissionsOnPrologue(executionId: number): Observable<void> {
    return this.restService.get<void>(['execution', executionId.toString(), 'modification-during-execution', 'permissions']);
  }

  checkPermissions(executionId: number, executionStepId: number): Observable<void> {
    return this.restService.get<void>(['execution', executionId.toString(), 'modification-during-execution', 'permissions',
      executionStepId.toString()]);
  }

  updateExecutionStepsAfterModif(executionId: number): Observable<number> {
    return this.restService.post<number>(['execution', executionId.toString(), 'modification-during-execution', 'update-steps']);
  }
}
