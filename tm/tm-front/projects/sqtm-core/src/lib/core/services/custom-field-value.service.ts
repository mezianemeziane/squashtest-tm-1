import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpHeaders} from '@angular/common/http';
import {RestService} from './rest.service';

@Injectable({
  providedIn: 'root'
})
export class CustomFieldValueService {

  constructor(private restService: RestService) {
  }

  /**
   * Update a custom field value server side.
   * TAKE CARE OF WHAT YOU SENT in value. The CUF system server side is a mess of untyped data stored as strings.
   * Especially the dates must be formatted as yyyy-MM-dd but the server seems to accept any date.toString() even without format. It would
   * probably accept any kind of data as long it's a fucking string...
   * @param cfvId of the custom field value.
   * @param value of the cfv. Really take care of what you pass here...
   * See RawValue.java server side
   */
  postCustomFieldValue(cfvId: number, value: string | string[]): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
    if (typeof value === 'string') {
      return this.restService.post(['custom-fields/values', cfvId.toString(), 'single-value'], {value}, {headers});
    } else {
      return this.restService.post(['custom-fields/values', cfvId.toString(), 'multi-value'], {values: value}, {headers});
    }
  }
}
