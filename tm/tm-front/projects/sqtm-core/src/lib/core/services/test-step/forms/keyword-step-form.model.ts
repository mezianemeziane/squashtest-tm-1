export interface KeywordStepFormModel {
  keyword: string;
  actionWord: string;
  index?: number;
  actionWordId?: number;
}
