import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AutomatedSuiteOverview } from '../../../model/test-automation/automated-suite-overview.model';
import {
  AutomatedSuiteCreationSpecification,
  AutomatedSuitePreview
} from '../../../model/test-automation/automated-suite-preview.model';
import { RestService } from '../rest.service';

@Injectable({
  providedIn: 'root'
})
export class AutomatedSuiteService {

  constructor(private restService: RestService) {
  }

  updateTaScriptsForIteration(iterationId: number): Observable<any> {
    const options = { params: { 'iterationId': iterationId.toString() }};
    return this.restService.post(['automation-requests', 'associate-TA-script'], {}, options);
  }

  updateTaScriptsForTestSuite(testSuiteId: number): Observable<any> {
    const options = { params: { 'testSuiteId': testSuiteId.toString() }};
    return this.restService.post(['automation-requests', 'associate-TA-script'], {}, options);
  }

  updateTaScriptsForItems(itemIds: number[]): Observable<any> {
    const options = { params: { 'testPlanItemsIds[]': itemIds.map(id => id.toString()) }};
    return this.restService.post(['automation-requests', 'associate-TA-script'], {}, options);
  }

  generateAutomatedSuitePreview(specification: AutomatedSuiteCreationSpecification): Observable<AutomatedSuitePreview> {
    return this.restService.post<AutomatedSuitePreview>(['automated-suites', 'preview'], specification);
  }

  fetchTestListForAutomationProject(
    automationProjectId: number, specification: AutomatedSuiteCreationSpecification): Observable<string[]> {
    const options = { params: { 'auto-project-id': automationProjectId.toString() } };
    return this.restService.post<string[]>(['automated-suites', 'preview', 'test-list'], specification, options);
  }

  createAndExecuteAutomatedSuite(specification: AutomatedSuiteCreationSpecification): Observable<AutomatedSuiteOverview> {
    return this.restService.post<AutomatedSuiteOverview>(['automated-suites', 'create-and-execute'], specification);
  };

  updateExecutionsInformation(automatedSuiteId: string): Observable<AutomatedSuiteOverview> {
    return this.restService.get<AutomatedSuiteOverview>(['automated-suites', automatedSuiteId.toString(), 'executions']);
  }
}
