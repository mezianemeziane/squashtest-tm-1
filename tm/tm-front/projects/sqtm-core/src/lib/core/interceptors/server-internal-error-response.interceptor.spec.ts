import {TestBed, waitForAsync} from '@angular/core/testing';

import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {ServerInternalErrorResponseInterceptor} from './server-internal-error-response.interceptor';
import {GenericErrorDisplayService} from '../services/errors-handling/generic-error-display.service';
import SpyObj = jasmine.SpyObj;

describe('ServerInternalErrorResponseInterceptor', () => {
  let http: HttpTestingController;
  let httpClient: HttpClient;
  let defaultErrorDisplayService: SpyObj<GenericErrorDisplayService>;

  beforeEach(waitForAsync(() => {
    defaultErrorDisplayService = jasmine.createSpyObj<GenericErrorDisplayService>(['showError']);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: GenericErrorDisplayService,
          useValue: defaultErrorDisplayService
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ServerInternalErrorResponseInterceptor,
          multi: true,
          deps: [GenericErrorDisplayService]
        }
      ]
    });
  }));

  beforeEach(() => {
    http = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should catch 500 error and send message', waitForAsync(() => {

    const observer = {
      next: () => fail(),
      error: () => expect(defaultErrorDisplayService.showError).toHaveBeenCalled()
    };

    defaultErrorDisplayService.showError.and.callFake(() => undefined);

    httpClient.get('/error').subscribe(observer);

    http.expectOne('/error').flush({}, {
      status: 500,
      statusText: 'Internal Servor Error'
    });
    http.verify();
  }));

  it('should throw error with status other than 500', waitForAsync(() => {

    const observer = {
      next: () => fail(),
      error: (err) => {
        expect(err.status).toEqual(403);
      },
      complete: () => fail()
    };

    defaultErrorDisplayService.showError.and.callFake(() => undefined);

    httpClient.get('/error').subscribe(observer);

    http.expectOne('/error').flush({}, {
      status: 403,
      statusText: 'Forbidden'
    });
    http.verify();
  }));
});
