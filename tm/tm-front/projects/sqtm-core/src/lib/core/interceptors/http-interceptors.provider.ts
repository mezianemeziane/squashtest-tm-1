import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {UnauthorizedResponseInterceptor} from './unauthorized-response.interceptor';
import {Router} from '@angular/router';
import {ServerInternalErrorResponseInterceptor} from './server-internal-error-response.interceptor';
import {SquashPlatformNavigationService} from '../services/navigation/squash-platform-navigation.service';
import {GenericErrorDisplayService} from '../services/errors-handling/generic-error-display.service';
import {APP_BASE_HREF} from '@angular/common';
// tslint:disable-next-line:import-blacklist
import {BACKEND_CONTEXT_PATH, CORE_MODULE_CONFIGURATION} from '../sqtm-core.tokens';

export const httpInterceptorProviders = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: ServerInternalErrorResponseInterceptor,
    multi: true,
    deps: [GenericErrorDisplayService]
  }, {
    provide: HTTP_INTERCEPTORS,
    useClass: UnauthorizedResponseInterceptor,
    multi: true,
    deps: [SquashPlatformNavigationService, Router, APP_BASE_HREF, CORE_MODULE_CONFIGURATION]
  },
];
