import {LoggingConfiguration} from 'sqtm-core';

export const appLoggerConfiguration: LoggingConfiguration = {
  alwaysShowTrace: false,
  loggers: {
    namespaces: {
      ['sqtm-core']: 'info',
      // ['sqtm-core.GridPersistenceService']: 'debug',
      // ['sqtm-core.ui.grid']: 'debug',
      // ['sqtm-core.ui.grid.TreeNodeServerOperationHandler']: 'trace',
      // ['sqtm-core.ui.attachment']: 'debug',
      // ['sqtm-core.ui.dnd']: 'trace',
      // ['sqtm-core.ui.anchor']: 'debug',
      // ['sqtm-core.ui.dialog']: 'debug',
      // ['sqtm-core.ui.custom-field']: 'debug',
      // ['sqtm-core.ui.platform-navigation']: 'debug',
      // ['sqtm-core.ui.workspace-common']: 'debug',
      // ['sqtm-core.ui.workspace-common.CreateEntityDialogComponent']: 'debug',
      // ['sqtm-core.ui.workspace-layout']: 'debug',
      // ['sqtm-core.core.Store']: 'debug',
      // ['sqtm-core.core.ActionErrorDisplayService']: 'debug',
      // ['sqtm-core.core.UnauthorizedResponseInterceptor']: 'debug',
      // ['sqtm-core.core.RestService']: 'debug',
      // ['sqtm-core.core.referential']: 'debug',
      ['sqtm-app']: 'info',
      // ['sqtm-app.CustomRouteReuseStrategy']: 'debug',
      // ['sqtm-app.charts']: 'debug',
      // ['sqtm-app.report-workbench']: 'debug',
      // ['sqtm-app.campaign-statistics']: 'debug',
      // ['sqtm-app.dashboard']: 'debug',
      // ['sqtm-app.chart-workbench']: 'debug',
      // ['sqtm-app.CustomRouteReuseStrategy']: 'debug',
      // ['sqtm-app.test-case-workspace']: 'debug',
      // ['sqtm-app.test-case-search-page']: 'debug',
      // ['sqtm-core.lib.model']: 'debug',
      // ['sqtm-app.execution-runner']: 'debug',
      // ['sqtm-app.test-case-view']: 'debug',
      // ['sqtm-app.test-steps']: 'debug',
      // ['sqtm-app.test-case-folder-view']: 'debug'
      // ['sqtm-app.administration.milestone']: 'debug',
    }
  }
};
