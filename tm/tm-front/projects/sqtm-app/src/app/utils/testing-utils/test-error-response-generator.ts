import {HttpErrorResponse} from '@angular/common/http';
import {ActionValidationError, FieldValidationError} from 'sqtm-core';

export function makeHttpFieldValidationError(fieldValidationErrors: FieldValidationError[]): HttpErrorResponse {
  return new HttpErrorResponse({
    status: 412,
    error: {
      squashTMError: {
        kind: 'FIELD_VALIDATION_ERROR',
        fieldValidationErrors,
      }
    }
  });
}

export function makeHttpActionError(actionValidationError: ActionValidationError): HttpErrorResponse {
  return new HttpErrorResponse({
    status: 412,
    error: {
      squashTMError: {
        kind: 'ACTION_ERROR',
        actionValidationError,
      },
    }
  });
}
