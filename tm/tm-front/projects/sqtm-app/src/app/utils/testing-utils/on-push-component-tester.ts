import {ComponentTester} from 'ngx-speculoos';
import {ChangeDetectorRef} from '@angular/core';

export class OnPushComponentTester<T> extends ComponentTester<T> {

  detectChanges(checkNoChanges?: boolean): void {
    const cdRef = this.fixture.componentRef.injector.get(ChangeDetectorRef);
    cdRef.detectChanges();
  }

  protected getElementByDataTestComponentId(id: string) {
    return this.element(`[data-test-component-id="${id}"]`);
  }
}
