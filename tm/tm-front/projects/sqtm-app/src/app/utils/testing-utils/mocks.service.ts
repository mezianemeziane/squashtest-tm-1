import {
  ActionErrorDisplayService,
  AdminReferentialDataService,
  AuthenticatedUser,
  DialogService,
  GridService,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {BehaviorSubject, EMPTY, of, Subject} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import SpyObj = jasmine.SpyObj;
import SpyObjMethodNames = jasmine.SpyObjMethodNames;

export function mockRestService(): SpyObj<RestService> {
  const methods: SpyObjMethodNames<RestService> = [
    'delete',
    'get',
    'post',
  ];

  const mock = jasmine.createSpyObj('RestService', methods);

  methods.forEach(spy => mock[spy].and.returnValue(of({})));

  return mock;
}

export function mockGridService(): SpyObj<GridService> {
  const mock = jasmine.createSpyObj('GridService', [
    'beginAsyncOperation',
    'complete',
    'completeAsyncOperation',
    'connectToDatasource',
    'load',
    'refreshData',
    'refreshDataAndKeepSelectedRows',
    'addFilters',
    'applyMultiColumnsFilter',
    'setServerUrl',
    'setColumnVisibility',
    'loadInitialDataRows',
    'addInitialConfiguration',
    'cancelDrag',
    'setEnableDrag'
  ]);

  mock.dataRows$ = EMPTY;
  mock.selectedRows$ = EMPTY;
  mock.selectedRowIds$ = EMPTY;
  mock.hasSelectedRows$ = EMPTY;
  mock.isSortedOrFiltered$ = EMPTY;
  mock.loaded$ = new BehaviorSubject<boolean>(false);
  mock.addInitialConfiguration.and.returnValue(of(true));
  mock.setServerUrl.and.callFake(() => {
    mock.loaded$.next(true);
  });
  mock.activateFilter$ = EMPTY;
  return mock;
}

export function mockDialogService(): SpyObj<DialogService> {
  const openers: SpyObjMethodNames<DialogService> = [
    'openAlert',
    'openConfirm',
    'openDeletionConfirm',
    'openDialog',
  ];

  const mock = jasmine.createSpyObj('DialogService', openers);

  openers.forEach(spy => mock[spy].and.returnValue({
    dialogClosed$: EMPTY,
    dialogResultChanged$: EMPTY,
  }));

  return mock;
}

// Variant which automatically closes with a truthy result
export function mockAutoConfirmDialogService(): SpyObj<DialogService> {
  const confirmDialogs = [
    'openConfirm',
    'openDeletionConfirm',
    'openDialog',
  ];

  const mock = mockDialogService();
  confirmDialogs.forEach((spy) => mock[spy].and.returnValue({dialogClosed$: of(true)}));
  return mock;
}

export function mockGridWithStatePersistenceService(): SpyObj<GridService> {
  const mock = jasmine.createSpyObj('GridWithStatePersistence', ['popGridState']);
  mock.popGridState.and.returnValue(of({noSnapshotExisting: true}));
  return mock;
}

// Still another higher-level variant. This one lets you control when you want to
// simulate the close event. The actual mock is wrapped with convenience methods.

interface ClosableDialogService<R> {
  service: SpyObj<DialogService>;

  closeDialogsWithResult(result: R): void;

  resetSubjects(): void;

  resetCalls(): void;
}

export function mockClosableDialogService<R>(): ClosableDialogService<R> {
  const closableDialogs = [
    'openAlert',
    'openConfirm',
    'openDeletionConfirm',
    'openDialog',
  ];

  const dialogRef = {
    dialogClosed$: new Subject<R>(),
    dialogResultChanged$: new Subject<R>(),
  };

  const mock = jasmine.createSpyObj('DialogService', closableDialogs);
  closableDialogs.forEach((spy) => mock[spy].and.returnValue(dialogRef));

  return {
    service: mock,
    closeDialogsWithResult: (result) => {
      dialogRef.dialogResultChanged$.next(result);
      dialogRef.dialogClosed$.next(result);
      dialogRef.dialogResultChanged$.complete();
      dialogRef.dialogClosed$.complete();
    },
    resetSubjects: () => {
      dialogRef.dialogResultChanged$ = new Subject<R>();
      dialogRef.dialogClosed$ = new Subject<R>();
    },
    resetCalls: () => closableDialogs.forEach((spy) => mock[spy].calls.reset()),
  };
}

export function mockReferentialDataService(): SpyObj<ReferentialDataService> {
  const mock = jasmine.createSpyObj('ReferentialDataService', [
    'refresh', 'findCustomFieldByProjectIdAndDomain',
  ]);

  mock.refresh.and.returnValue(of(true));
  mock.projectDatas$ = EMPTY;

  return mock;
}

export function mockAdminReferentialDataService(): SpyObj<AdminReferentialDataService> {
  const mock = jasmine.createSpyObj<AdminReferentialDataService>('AdminReferentialDataService', [
    'refresh',
  ]);

  mock.refresh.and.returnValue(EMPTY);
  mock.authenticatedUser$ = EMPTY;
  mock.loggedAsAdmin$ = EMPTY;
  mock.licenseInformation$ = EMPTY;
  mock.adminReferentialData$ = EMPTY;

  return mock;
}

export function mockAdminReferentialDataServiceWithUser(authenticatedUser: AuthenticatedUser): SpyObj<AdminReferentialDataService> {
  const mock = mockAdminReferentialDataService();
  mock.authenticatedUser$ = of(authenticatedUser);
  mock.loggedAsAdmin$ = of(authenticatedUser.admin);

  return mock;
}

// A translate service that.. won't translate ! Its `instant()` method returns what was passed in.
export function mockPassThroughTranslateService(): SpyObj<TranslateService> {
  const mock = jasmine.createSpyObj('TranslateService', [
    'instant', 'get',
  ]);

  mock.instant.and.callFake(a => a);
  mock.get.and.returnValue(EMPTY);

  return mock;
}

export function mockRouter(): SpyObj<Router> {
  const mock: SpyObj<Router> = jasmine.createSpyObj<Router>('Router', ['navigate']);
  (mock as any).url = '/some/url/';
  return mock;
}

export function mockActionErrorDisplayService(): SpyObj<ActionErrorDisplayService> {
  const methods: SpyObjMethodNames<ActionErrorDisplayService> = [
    'handleActionError',
    'showActionError',
  ];

  const mock = jasmine.createSpyObj('ActionErrorDisplayService', methods);

  methods.forEach(spy => mock[spy].and.returnValue(of({})));

  return mock;
}
