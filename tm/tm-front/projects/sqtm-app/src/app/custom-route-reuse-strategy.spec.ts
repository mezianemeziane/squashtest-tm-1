import {TestBed} from '@angular/core/testing';
import {CustomRouteReuseStrategy} from './custom-route-reuse-strategy';
import {ActivatedRouteSnapshot, UrlSegment} from '@angular/router';

describe('ChartWorkbenchService', () => {
  let service: CustomRouteReuseStrategy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        CustomRouteReuseStrategy
      ]
    });
    service = TestBed.inject(CustomRouteReuseStrategy);
  });

  function generateActivatedRouteSnapshotMock(paths: string[]): ActivatedRouteSnapshot {
    const url: UrlSegment[] = paths.map(path => ({path} as UrlSegment));
    return {
      url
    } as ActivatedRouteSnapshot;
  }

  describe('Should Reuse Route', () => {

    interface DataType {
      future: string[];
      curr: string[];
      shouldReuseRoute: boolean;
    }

    const dataSets: DataType[] = [
      {
        future: ['test-case-workspace'],
        curr: ['test-case-workspace'],
        shouldReuseRoute: true
      },
      {
        future: ['test-case', '12'],
        curr: ['test-case', '12'],
        shouldReuseRoute: true
      },
      {
        future: ['test-case', '10'],
        curr: ['test-case', '12'],
        shouldReuseRoute: false
      }
    ];

    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - It should reuse route :${data.shouldReuseRoute}.`, () => {
        const result = service.shouldReuseRoute(
          generateActivatedRouteSnapshotMock(data.future),
          generateActivatedRouteSnapshotMock(data.curr));
        expect(result).toEqual(data.shouldReuseRoute);
      });

    }
  });
});



