import {ActivatedRouteSnapshot, DetachedRouteHandle, RouteReuseStrategy} from '@angular/router';
import {Injectable} from '@angular/core';
import {sqtmAppLogger} from './app-logger';

const logger = sqtmAppLogger.compose('CustomRouteReuseStrategy');

@Injectable()
export class CustomRouteReuseStrategy implements RouteReuseStrategy {

  shouldDetach(route: ActivatedRouteSnapshot): boolean {
    return false;
  }

  store(route: ActivatedRouteSnapshot, detachedTree: DetachedRouteHandle): void {
  }

  shouldAttach(route: ActivatedRouteSnapshot): boolean {
    return false;
  }

  retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle | null {
    return null;
  }

  shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
    logger.debug('--- Should reuse route ---');
    logger.debug(`Future : ${future.component}`, [future]);
    logger.debug(`Current : ${curr.component}`, [curr]);
    const shouldReuseRoute = CustomRouteReuseStrategy.doesRuntimeUrlsAreSame(future, curr);
    logger.debug(`--- End of should reuse route. Should reuse ? : ${shouldReuseRoute}`);
    return shouldReuseRoute;
  }

  private static doesRuntimeUrlsAreSame(
    future: ActivatedRouteSnapshot,
    curr: ActivatedRouteSnapshot): boolean {
    const length = future.url.length;
    if (curr.url.length !== length) {
      return false;
    }
    for (let i = 0; i < length; i++) {
      const identical = CustomRouteReuseStrategy.doesSegmentHaveSamePath(future, curr, i);
      if (!identical) {
        return false;
      }
    }
    return true;
  }

  private static doesSegmentHaveSamePath(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot, identifierIndex: number) {
    const futureUrlSegment = future.url[identifierIndex];
    const currentUrlSegment = curr.url[identifierIndex];
    if (!futureUrlSegment || !currentUrlSegment) {
      throw Error('Empty url segment. Future and current must not be nulls');
    }
    const pathFuture: string = futureUrlSegment.path;
    const pathCurr: string = currentUrlSegment.path;
    if (!pathFuture || !pathCurr) {
      throw Error(`Empty path at index ${identifierIndex}. Future and current paths must not be nulls`);
    }
    return pathFuture === pathCurr;
  }

}
