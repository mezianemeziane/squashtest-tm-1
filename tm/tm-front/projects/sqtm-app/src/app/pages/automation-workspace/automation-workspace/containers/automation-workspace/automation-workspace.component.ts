import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ReferentialDataService} from 'sqtm-core';
import {AutomationWorkspaceService} from '../../services/automation-workspace.service';
import {concatMap} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-automation-workspace',
  templateUrl: './automation-workspace.component.html',
  styleUrls: ['./automation-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: AutomationWorkspaceService,
      useClass: AutomationWorkspaceService/*,
      deps: [RestService, AutomationRequestService]*/
    }
  ]
})
export class AutomationWorkspaceComponent implements OnInit {

  constructor(private referentialDataService: ReferentialDataService,
              private automationWorkspaceService: AutomationWorkspaceService) {
  }

  ngOnInit(): void {
    this.referentialDataService.refresh().pipe(
      concatMap(() => this.automationWorkspaceService.load())
    ).subscribe();
  }

}
