import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FunctionalTesterWorkspaceComponent} from './containers/functional-tester-workspace/functional-tester-workspace.component';
import {RouterModule, Routes} from '@angular/router';
import {
  AnchorModule, GridModule,
  NavBarModule,
  SvgModule,
  UiManagerModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule
} from 'sqtm-core';
import { ReadyForTransmissionFunctionalTesterViewComponent } from './containers/ready-for-transmission-functional-tester-view/ready-for-transmission-functional-tester-view.component';
import { ToBeValidatedFunctionalTesterViewComponent } from './containers/to-be-validated-automation-tester-view/to-be-validated-functional-tester-view.component';
import { GlobalFunctionalTesterViewComponent } from './containers/global-functional-tester-view/global-functional-tester-view.component';
import {TranslateModule} from '@ngx-translate/core';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import { FunctionalTesterWorkspaceAnchorsComponent } from './components/functional-tester-workspace-anchors/functional-tester-workspace-anchors.component';
import {AutomationModule} from '../../../components/automation/automation.module';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzIconModule} from 'ng-zorro-antd/icon';
import { FunctionalTesterTestCaseViewComponent } from './components/functional-tester-test-case-view/functional-tester-test-case-view.component';
import {TestCaseViewDetailComponent} from '../../test-case-workspace/test-case-view/containers/test-case-view-detail/test-case-view-detail.component';
import {TestCaseViewContentComponent} from '../../test-case-workspace/test-case-view/components/test-case-view-content/test-case-view-content.component';
import {TestStepsComponent} from '../../test-case-workspace/test-case-view/components/test-steps/test-steps.component';
import {IssuesComponent} from '../../test-case-workspace/test-case-view/components/issues/issues.component';
import {ExecutionsComponent} from '../../test-case-workspace/test-case-view/components/executions/executions.component';
import {ScriptComponent} from '../../test-case-workspace/test-case-view/components/script/script.component';
import {KeywordTestStepsComponent} from '../../test-case-workspace/test-case-view/components/keyword-test-steps/keyword-test-steps.component';
import {NzButtonModule} from 'ng-zorro-antd/button';

export const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'ready-for-transmission'
  },
  {
    path: '',
    component: FunctionalTesterWorkspaceComponent,
    children: [
      {path: 'ready-for-transmission', component: ReadyForTransmissionFunctionalTesterViewComponent, children: [
          {path: ':testCaseId', component: FunctionalTesterTestCaseViewComponent, children: [
              {
                path: '', component: TestCaseViewDetailComponent, children: [
                  {path: '', redirectTo: 'content'},
                  {path: 'content', component: TestCaseViewContentComponent},
                  {path: 'steps', component: TestStepsComponent},
                  {path: 'issues', component: IssuesComponent},
                  {path: 'executions', component: ExecutionsComponent},
                  {path: 'script', component: ScriptComponent},
                  {path: 'keywordSteps', component: KeywordTestStepsComponent},
                ]
              }
            ]
          }
        ]},
      {path: 'validate', component: ToBeValidatedFunctionalTesterViewComponent, children: [
          {path: ':testCaseId', component: FunctionalTesterTestCaseViewComponent, children: [
              {
                path: '', component: TestCaseViewDetailComponent, children: [
                  {path: '', redirectTo: 'content'},
                  {path: 'content', component: TestCaseViewContentComponent},
                  {path: 'steps', component: TestStepsComponent},
                  {path: 'issues', component: IssuesComponent},
                  {path: 'executions', component: ExecutionsComponent},
                  {path: 'script', component: ScriptComponent},
                  {path: 'keywordSteps', component: KeywordTestStepsComponent},
                ]
              }
            ]
          }
        ]},
      {path: 'global', component: GlobalFunctionalTesterViewComponent, children: [
          {path: ':testCaseId', component: FunctionalTesterTestCaseViewComponent, children: [
              {
                path: '', component: TestCaseViewDetailComponent, children: [
                  {path: '', redirectTo: 'content'},
                  {path: 'content', component: TestCaseViewContentComponent},
                  {path: 'steps', component: TestStepsComponent},
                  {path: 'issues', component: IssuesComponent},
                  {path: 'executions', component: ExecutionsComponent},
                  {path: 'script', component: ScriptComponent},
                  {path: 'keywordSteps', component: KeywordTestStepsComponent},
                ]
              }
            ]
          }
        ]}
    ]
  }
];

@NgModule({
  declarations: [
    FunctionalTesterWorkspaceComponent,
    ReadyForTransmissionFunctionalTesterViewComponent,
    ToBeValidatedFunctionalTesterViewComponent,
    GlobalFunctionalTesterViewComponent,
    FunctionalTesterWorkspaceAnchorsComponent,
    FunctionalTesterTestCaseViewComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        NavBarModule,
        SvgModule,
        WorkspaceCommonModule,
        UiManagerModule,
        AnchorModule,
        TranslateModule.forChild(),
        NzToolTipModule,
        WorkspaceLayoutModule,
        AutomationModule,
        NzDropDownModule,
        GridModule,
        NzIconModule,
        NzButtonModule
    ]
})
export class FunctionalTesterWorkspaceModule {
}
