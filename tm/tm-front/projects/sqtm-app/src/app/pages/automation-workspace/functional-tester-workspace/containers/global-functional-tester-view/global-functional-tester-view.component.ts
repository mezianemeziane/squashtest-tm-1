import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AFTW_GLOBAL_VIEW_TABLE, AFTW_GLOBAL_VIEW_TABLE_CONFIG} from '../../functional-tester-workspace.constant';
import {
  AutomationRequestStatus,
  buildFilters,
  DataRow,
  dateColumn,
  DateFilterComponent,
  DateFilterValueRendererComponent,
  DialogService,
  Extendable,
  FilterBuilder,
  FilterOperation,
  Fixed,
  grid,
  GridFilter, GridFilterUtils,
  GridService,
  gridServiceFactory,
  i18nEnumResearchFilter,
  indexColumn,
  isTestCaseEditable,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  selectableTextColumn,
  serverBackedGridEqualFilter,
  serverBackedGridTextFilter,
  Sort,
  StyleDefinitionBuilder,
  TestCaseKind,
  textColumn,
  userHistoryResearchFilter,
  UserHistorySearchProvider,
} from 'sqtm-core';
import {Observable, of, Subject} from 'rxjs';
import {FunctionalTesterWorkspaceState} from '../../state/functional-tester-workspace.state';
import {FunctionalTesterWorkspaceService} from '../../services/functional-tester-workspace.service';
import {automationRequestLiteralConverter} from '../../../../../components/automation/automation-utils';
import {filter, map, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {uuidColumn} from '../../../../../components/automation/components/cell-renderers/uuid-cell-renderer/uuid-cell-renderer.component';
import {GlobalFunctionalTesterUserProvider} from './global-functional-tester-user-provider';
import {isAutomReqEditable} from '../../functional-tester-workspace.utils';

export function aftwGlobalViewTableDefinition() {
  return grid('functional-tester-global-view')
    .withColumns([
      indexColumn()
        .changeWidthCalculationStrategy(new Fixed(60))
        .withViewport('leftViewport'),
      textColumn('projectName')
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2))
        .withAssociatedFilter(),
      textColumn('tclnId')
        .withI18nKey('sqtm-core.entity.generic.id.capitalize')
        .changeWidthCalculationStrategy(new Extendable(20, 0.2))
        .withAssociatedFilter(),
      textColumn('reference')
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.reference')
        .withTitleI18nKey('sqtm-core.grid.header.reference')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2))
        .withAssociatedFilter(),
      selectableTextColumn('name')
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .changeWidthCalculationStrategy(new Extendable(120, 0.2))
        .withAssociatedFilter(),
      uuidColumn('uuid')
        .withI18nKey('sqtm-core.entity.test-case.uuid.label')
        .changeWidthCalculationStrategy(new Extendable(20, 0.2))
        .disableSort(),
      textColumn('kind')
        .withEnumRenderer(TestCaseKind, false, true)
        .withI18nKey('sqtm-core.entity.test-case.kind.label')
        .changeWidthCalculationStrategy(new Extendable(40, 0.2)),
      textColumn('login')
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.tester')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2))
        .withAssociatedFilter(),
      textColumn('automationPriority')
        .isEditable(isTestCaseEditable)
        .withI18nKey('sqtm-core.grid.header.priority.short')
        .withTitleI18nKey('sqtm-core.grid.header.priority.long')
        .changeWidthCalculationStrategy(new Extendable(20, 0.2))
        .withAssociatedFilter(),
      textColumn('requestStatus')
        .withEnumRenderer(AutomationRequestStatus, false, true)
        .isEditable(false)
        .withI18nKey('sqtm-core.entity.automation-request.status.label.short')
        .withTitleI18nKey('sqtm-core.entity.automation-request.status.label.full')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2))
        .withAssociatedFilter(),
      dateColumn('transmittedOn')
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.transmitted-on')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1))
        .withAssociatedFilter()
    ])
    .server().withServerUrl(['automation-tester-workspace/global-view'])
    .withRowConverter(automationRequestLiteralConverter)
    .withInitialSortedColumns([
        {id: 'automationPriority', sort: Sort.DESC},
        {id: 'requestStatus', sort: Sort.ASC},
        {id: 'kind', sort: Sort.DESC}
      ]
    )
    .withModificationUrl(['automation-requests'])
    .disableRightToolBar()
    .withRowHeight(35)
    .withStyle(new StyleDefinitionBuilder()
      .enableInitialLoadAnimation()
      .showLines())
    .build();
}

@Component({
  selector: 'sqtm-app-global-functional-tester-view',
  templateUrl: './global-functional-tester-view.component.html',
  styleUrls: ['./global-functional-tester-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: AFTW_GLOBAL_VIEW_TABLE_CONFIG,
      useFactory: aftwGlobalViewTableDefinition,
      deps: []
    },
    {
      provide: AFTW_GLOBAL_VIEW_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, AFTW_GLOBAL_VIEW_TABLE_CONFIG, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: AFTW_GLOBAL_VIEW_TABLE
    },
    {provide: UserHistorySearchProvider, useClass: GlobalFunctionalTesterUserProvider}
  ]
})
export class GlobalFunctionalTesterViewComponent implements OnInit, AfterViewInit {

  componentData$: Observable<FunctionalTesterWorkspaceState>;
  unsub$ = new Subject<void>();

  canEditRows$: Observable<boolean>;
  activeFilters$: Observable<GridFilter[]>;

  constructor(public referentialDataService: ReferentialDataService,
              public gridService: GridService,
              private dialogService: DialogService,
              private automationTesterWorkspaceService: FunctionalTesterWorkspaceService) {
    this.componentData$ = this.automationTesterWorkspaceService.componentData$;
    this.canEditRows$ = this.gridService.selectedRows$.pipe(
      map(rows => {
        return this.filterNonEditableRows(rows).length > 0;
      })
    );
    this.activeFilters$ = this.gridService.activeFilters$.pipe(
      takeUntil(this.unsub$),
      map((gridFilters: GridFilter[]) => gridFilters.filter(gridFilter => GridFilterUtils.mustIncludeFilter(gridFilter))),
    );
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.componentData$.pipe(
      filter(component => component.nbGlobalAutomReq > 0),
      take(1),
      tap(() => this.gridService.addFilters(this.buildGridFilters()))
    ).subscribe(() => {
      this.gridService.refreshData();
    });
  }

  private buildGridFilters(): GridFilter[] {
    return buildFilters(
      [
        serverBackedGridTextFilter('projectName'),
        serverBackedGridEqualFilter('tclnId'),
        serverBackedGridTextFilter('reference'),
        serverBackedGridTextFilter('name'),
        serverBackedGridTextFilter('uuid'),
        serverBackedGridEqualFilter('automationPriority'),
        i18nEnumResearchFilter('requestStatus', ResearchColumnPrototype.AUTOMATION_REQUEST_STATUS)
          .alwaysActive(),
        userHistoryResearchFilter('login', ResearchColumnPrototype.TEST_CASE_MODIFIED_BY)
          .alwaysActive(),
        this.buildTransmittedDateFilter()
      ]
    );
  }

  private buildTransmittedDateFilter(): FilterBuilder {
    return new FilterBuilder('transmittedOn')
      .alwaysActive()
      .withColumnPrototype(ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECON)
      .withAvailableOperations([FilterOperation.BETWEEN])
      .withOperations(FilterOperation.BETWEEN)
      .withInitialValue({kind: 'multiple-date-value', value: []})
      .withWidget(DateFilterComponent)
      .withValueRenderer(DateFilterValueRendererComponent);
  }

  transmitSelection() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter(rows => this.filterNonEditableRows(rows).length > 0),
      switchMap(rows => this.showConfirmDialogIfNeeded(rows)),
      switchMap(editableRows => {
        const rowIds = this.filterNonTransmittedAutomationRequests(editableRows).map(row => row.id);
        return this.automationTesterWorkspaceService.changeStatusToTransmitted(rowIds as number[]);
      })
    ).subscribe(() => this.gridService.refreshData());
  }

  changeStatusToSuspended() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter(rows => this.filterNonEditableRows(rows).length > 0),
      switchMap(rows => this.showConfirmDialogIfNeeded(rows)),
      switchMap(editableRows => {
        const rowIds = editableRows.map(row => row.id);
        return this.automationTesterWorkspaceService.changeStatusToSuspended(rowIds as number[]);
      })
    ).subscribe(() => this.gridService.refreshData());
  }

  changeStatusToReady() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter(rows => this.filterNonEditableRows(rows).length > 0),
      switchMap(rows => this.showConfirmDialogIfNeeded(rows)),
      switchMap(editableRows => {
        const rowIds = editableRows.map(row => row.id);
        return this.automationTesterWorkspaceService.changeStatusToReady(rowIds as number[]);
      })
    ).subscribe(() => this.gridService.refreshData());
  }

  changeStatusToWorkInProgress() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter(rows => this.filterNonEditableRows(rows).length > 0),
      switchMap(rows => this.showConfirmDialogIfNeeded(rows)),
      switchMap(editableRows => {
        const rowIds = editableRows.map(row => row.id);
        return this.automationTesterWorkspaceService.changeStatusToWorkInProgress(rowIds as number[]);
      })
    ).subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDialogIfNeeded(rows: DataRow[]): Observable<DataRow[]> {
    const editableRows: DataRow[] = this.filterNonEditableRows(rows);
    if (editableRows.length !== rows.length) {
      const dialogRef = this.dialogService.openAlert({
        id: 'change-automation-state-warning',
        titleKey: 'sqtm-core.automation-workspace.functional-tester-view.dialog.title',
        messageKey: 'sqtm-core.automation-workspace.functional-tester-view.dialog.message',
      });

      return dialogRef.dialogClosed$.pipe(
        map(confirm => editableRows)
      );
    } else {
      return of(editableRows);
    }
  }

  private filterNonEditableRows(dataRows: DataRow[]) {
    return dataRows.filter(row => isAutomReqEditable(row));
  }

  private filterNonTransmittedAutomationRequests(rows: DataRow[]): DataRow[] {
    return rows.filter(row => row.data['requestStatus'] !== AutomationRequestStatus.TRANSMITTED.id);
  }

  shouldShowResetFilterLink(activeFilters: GridFilter[]): boolean {
    return activeFilters.length > 0;
  }

  resetFilters() {
    this.gridService.resetFilters();
  }
}
