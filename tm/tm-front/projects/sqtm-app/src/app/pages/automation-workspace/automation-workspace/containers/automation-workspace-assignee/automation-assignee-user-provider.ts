import {ResearchColumnPrototype, UserHistorySearchProvider, UserListElement} from 'sqtm-core';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {AutomationWorkspaceService} from '../../services/automation-workspace.service';

@Injectable()
export class AutomationAssigneeUserProvider extends UserHistorySearchProvider {

  constructor(private automationWorkspaceService: AutomationWorkspaceService) {
    super();
  }

  provideUserList(columnPrototype: ResearchColumnPrototype): Observable<UserListElement[]> {
    return this.automationWorkspaceService.componentData$.pipe(
      take(1),
      map(data => data.usersWhoModifiedTestCasesAssignView.map(u => ({...u, selected: false})))
    );
  }
}
