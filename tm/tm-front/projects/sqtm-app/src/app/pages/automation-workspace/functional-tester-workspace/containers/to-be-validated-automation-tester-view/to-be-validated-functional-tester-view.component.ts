import {AfterViewInit, ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {
  AFTW_TO_BE_VALIDATED_TABLE,
  AFTW_TO_BE_VALIDATED_TABLE_CONFIG
} from '../../functional-tester-workspace.constant';
import {
  AutomationRequestStatus,
  buildFilters,
  DataRow,
  DialogService,
  Extendable,
  Fixed,
  grid,
  GridFilter, GridFilterUtils,
  GridService,
  gridServiceFactory,
  i18nEnumResearchFilter,
  indexColumn,
  isTestCaseEditable,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  selectableTextColumn,
  serverBackedGridEqualFilter,
  serverBackedGridTextFilter,
  Sort,
  StyleDefinitionBuilder,
  TestCaseKind,
  textColumn,
  userHistoryResearchFilter,
  UserHistorySearchProvider
} from 'sqtm-core';
import {automationRequestLiteralConverter} from '../../../../../components/automation/automation-utils';
import {uuidColumn} from '../../../../../components/automation/components/cell-renderers/uuid-cell-renderer/uuid-cell-renderer.component';
import {Observable, of, Subject} from 'rxjs';
import {FunctionalTesterWorkspaceState} from '../../state/functional-tester-workspace.state';
import {FunctionalTesterWorkspaceService} from '../../services/functional-tester-workspace.service';
import {filter, map, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {ToBeValidatedUserProvider} from './to-be-validated-user-provider';
import {isAutomReqEditable} from '../../functional-tester-workspace.utils';

export function aftwToBeValidatedTableDefinition() {
  return grid('functional-tester-to-be-validated')
    .withColumns([
      indexColumn()
        .changeWidthCalculationStrategy(new Fixed(60))
        .withViewport('leftViewport'),
      textColumn('projectName')
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2))
        .withAssociatedFilter(),
      textColumn('tclnId')
        .withI18nKey('sqtm-core.entity.generic.id.capitalize')
        .changeWidthCalculationStrategy(new Extendable(20, 0.2))
        .withAssociatedFilter(),
      textColumn('reference')
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.reference')
        .withTitleI18nKey('sqtm-core.grid.header.reference')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2))
        .withAssociatedFilter(),
      selectableTextColumn('name')
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .changeWidthCalculationStrategy(new Extendable(120, 0.2))
        .withAssociatedFilter(),
      uuidColumn('uuid')
        .withI18nKey('sqtm-core.entity.test-case.uuid.label')
        .changeWidthCalculationStrategy(new Extendable(20, 0.2))
        .disableSort(),
      textColumn('kind')
        .withEnumRenderer(TestCaseKind, false, true)
        .withI18nKey('sqtm-core.entity.test-case.kind.label')
        .changeWidthCalculationStrategy(new Extendable(40, 0.2)),
      textColumn('login')
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.tester')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2))
        .withAssociatedFilter(),
      textColumn('automationPriority')
        .isEditable(isTestCaseEditable)
        .withI18nKey('sqtm-core.grid.header.priority.short')
        .withTitleI18nKey('sqtm-core.grid.header.priority.long')
        .changeWidthCalculationStrategy(new Extendable(20, 0.2))
        .withAssociatedFilter(),
      textColumn('requestStatus')
        .withEnumRenderer(AutomationRequestStatus, false, true)
        .isEditable(false)
        .withI18nKey('sqtm-core.entity.automation-request.status.label.short')
        .withTitleI18nKey('sqtm-core.entity.automation-request.status.label.full')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2))
        .withAssociatedFilter()
    ])
    .server().withServerUrl(['automation-tester-workspace/to-be-validated'])
    .withRowConverter(automationRequestLiteralConverter)
    .withInitialSortedColumns([
      {id: 'automationPriority', sort: Sort.DESC},
      {id: 'requestStatus', sort: Sort.ASC},
      {id: 'kind', sort: Sort.DESC},
    ])
    .withModificationUrl(['automation-requests'])
    .disableRightToolBar()
    .withRowHeight(35)
    .withStyle(new StyleDefinitionBuilder()
      .enableInitialLoadAnimation()
      .showLines())
    .build();
}

@Component({
  selector: 'sqtm-app-to-be-validated-functional-tester-view',
  templateUrl: './to-be-validated-functional-tester-view.component.html',
  styleUrls: ['./to-be-validated-functional-tester-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: AFTW_TO_BE_VALIDATED_TABLE_CONFIG,
      useFactory: aftwToBeValidatedTableDefinition,
      deps: []
    },
    {
      provide: AFTW_TO_BE_VALIDATED_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, AFTW_TO_BE_VALIDATED_TABLE_CONFIG, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: AFTW_TO_BE_VALIDATED_TABLE
    },
    {provide: UserHistorySearchProvider, useClass: ToBeValidatedUserProvider}
  ]
})
export class ToBeValidatedFunctionalTesterViewComponent implements OnInit, AfterViewInit {

  componentData$: Observable<FunctionalTesterWorkspaceState>;
  unsub$ = new Subject<void>();

  canEditRows$: Observable<boolean>;
  activeFilters$: Observable<GridFilter[]>;

  constructor(public referentialDataService: ReferentialDataService,
              public gridService: GridService,
              private dialogService: DialogService,
              private automationTesterWorkspaceService: FunctionalTesterWorkspaceService) {
    this.componentData$ = this.automationTesterWorkspaceService.componentData$;
    this.canEditRows$ = this.gridService.selectedRows$.pipe(
      map(rows => {
        return this.filterNonEditableRows(rows).length > 0;
      })
    );
    this.activeFilters$ = this.gridService.activeFilters$.pipe(
      takeUntil(this.unsub$),
      map((gridFilters: GridFilter[]) => gridFilters.filter(gridFilter => GridFilterUtils.mustIncludeFilter(gridFilter))),
    );
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.componentData$.pipe(
      filter(component => component.nbToBeValidatedAutomReq > 0),
      take(1),
      tap(() => this.gridService.addFilters(this.buildGridFilters()))
    ).subscribe(() => {
      this.gridService.refreshData();
    });
  }

  private buildGridFilters(): GridFilter[] {
    return buildFilters(
      [
        serverBackedGridTextFilter('projectName'),
        serverBackedGridEqualFilter('tclnId'),
        serverBackedGridTextFilter('reference'),
        serverBackedGridTextFilter('name'),
        serverBackedGridTextFilter('uuid'),
        serverBackedGridEqualFilter('automationPriority'),
        i18nEnumResearchFilter('requestStatus', ResearchColumnPrototype.TO_BE_VALIDATED_AUTOMATION_REQUEST_STATUS)
          .alwaysActive(),
        userHistoryResearchFilter('login', ResearchColumnPrototype.TEST_CASE_MODIFIED_BY)
          .alwaysActive()
      ]
    );
  }

  changeStatusToReady() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter(rows => this.filterNonEditableRows(rows).length > 0),
      switchMap(rows => this.showConfirmDialogIfNeeded(rows)),
      switchMap(editableRows => {
        const rowIds = editableRows.map(row => row.id);
        return this.automationTesterWorkspaceService.changeStatusToReady(rowIds as number[]);
      })
    ).subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDialogIfNeeded(rows: DataRow[]): Observable<DataRow[]> {
    const editableRows: DataRow[] = this.filterNonEditableRows(rows);
    if (editableRows.length !== rows.length) {
      const dialogRef = this.dialogService.openAlert({
        id: 'change-automation-state-warning',
        titleKey: 'sqtm-core.automation-workspace.functional-tester-view.dialog.title',
        messageKey: 'sqtm-core.automation-workspace.functional-tester-view.dialog.message',
      });

      return dialogRef.dialogClosed$.pipe(
        map(confirm => editableRows)
      );
    } else {
      return of(editableRows);
    }
  }

  private filterNonEditableRows(dataRows: DataRow[]) {
    return dataRows.filter(row => isAutomReqEditable(row));
  }

  shouldShowResetFilterLink(activeFilters: GridFilter[]): boolean {
    return activeFilters.length > 0;
  }

  resetFilters() {
    this.gridService.resetFilters();
  }
}
