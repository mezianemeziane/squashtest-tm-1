import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {DialogReference} from 'sqtm-core';
import {ConflictAssociationDialogConfiguration} from './conflict-association-dialog.configuration';

@Component({
  selector: 'sqtm-app-conflict-association-dialog',
  templateUrl: './conflict-association-dialog.component.html',
  styleUrls: ['./conflict-association-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConflictAssociationDialogComponent implements OnInit {

  conflictAssociation: string[];

  constructor(private dialogReference: DialogReference<ConflictAssociationDialogConfiguration>) {
    this.conflictAssociation = this.dialogReference.data.conflictAssociation;
  }

  ngOnInit(): void {
  }

}
