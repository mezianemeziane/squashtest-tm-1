import {ItemListSearchProvider, ListItem, ReferentialDataService, ScmRepository, ScmServer} from 'sqtm-core';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {Injectable} from '@angular/core';

@Injectable()
export class ScmRepositoryProvider extends ItemListSearchProvider {

  constructor(private referentialDataService: ReferentialDataService) {
    super();
  }

  provideList(listKey: string): Observable<ListItem[]> {
    return this.referentialDataService.scmServers$.pipe(
      take(1),
      map((scmServers: ScmServer[]) => {
        const itemList: ListItem[] = [];
        scmServers.forEach(server => {
          const repos = server.repositories.map(repo => {
            return {id: repo.scmRepositoryId, label: this.buildRepoUrl(server, repo)};
          });
          itemList.push(...repos);
        });
        return itemList;
      })
    );
  }

  private buildRepoUrl(scmServer: ScmServer, scmRepo: ScmRepository): string {
    let url = scmServer.url;
    if (!url.endsWith('/')) {
      url = `${url}/`;
    }

    url = `${url}${scmRepo.name} (${scmRepo.workingBranch})`;
    return url;
  }
}
