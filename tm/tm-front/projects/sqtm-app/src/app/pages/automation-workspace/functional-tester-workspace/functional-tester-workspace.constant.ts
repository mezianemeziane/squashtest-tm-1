import {InjectionToken} from '@angular/core';
import {GridDefinition, GridService} from 'sqtm-core';

export const AFTW_READY_FOR_TRANSMISSION_TABLE_CONFIG = new InjectionToken<GridDefinition>
('Grid config instance for the automation functional tester workspace ready for transmission table');
export const AFTW_READY_FOR_TRANSMISSION_TABLE = new InjectionToken<GridService>
('Grid table instance for the automation functional tester workspace ready for transmission table');

export const AFTW_TO_BE_VALIDATED_TABLE_CONFIG = new InjectionToken<GridDefinition>
('Grid config instance for the automation functional tester workspace to be validated table');
export const AFTW_TO_BE_VALIDATED_TABLE = new InjectionToken<GridService>
('Grid table instance for the automation functional tester workspace to be validated table');

export const AFTW_GLOBAL_VIEW_TABLE_CONFIG = new InjectionToken<GridDefinition>
('Grid config instance for the automation functional tester workspace global view table');
export const AFTW_GLOBAL_VIEW_TABLE = new InjectionToken<GridService>
('Grid table instance for the automation functional tester workspace global view table');
