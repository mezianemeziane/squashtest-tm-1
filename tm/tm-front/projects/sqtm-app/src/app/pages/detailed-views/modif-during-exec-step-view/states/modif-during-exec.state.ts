import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {ExecutionStepActionTestStepPair} from 'sqtm-core';

export interface ModifDuringExecState {
  executionId: number;
  initialStepId: number;
  currentExecutionStepId: number;
  executionStepActionTestStepPairs: EntityState<ExecutionStepActionTestStepPair>;
}

export function provideInitialModifDuringExecState(): Readonly<ModifDuringExecState> {
  return {
    initialStepId: null,
    executionId: null,
    currentExecutionStepId: null,
    executionStepActionTestStepPairs: executionStepPairAdapter.getInitialState()
  };
}

export const executionStepPairAdapter = createEntityAdapter<ExecutionStepActionTestStepPair>({selectId: (model => model.executionStepId)});

const selectCurrentExecutionStepId = createFeatureSelector<ModifDuringExecState, number>('currentExecutionStepId');
const selectExecutionStepPairs = createFeatureSelector<ModifDuringExecState, EntityState<ExecutionStepActionTestStepPair>>('executionStepActionTestStepPairs');

export const selectCurrentActionStepId
  = createSelector(selectCurrentExecutionStepId, selectExecutionStepPairs, (current, pairs) => pairs.entities[current]?.testStepId);

export const selectCurrentActionStepIndex = createSelector(selectCurrentExecutionStepId, selectExecutionStepPairs, (current, pairs) => {
  const ids = pairs.ids as number[];
  return ids.indexOf(current);
});

export const selectTotalStepCount = createSelector(selectExecutionStepPairs, (pairs) => pairs.ids.length);
