import {entitySelector, EntityViewState, provideInitialViewState, SqtmEntityState} from 'sqtm-core';
import {
  coverageEntitySelectors,
  CoverageState
} from '../../../test-case-workspace/test-case-view/state/requirement-version-coverage.state';
import {createSelector} from '@ngrx/store';
import {TestCaseViewUiState} from '../../../test-case-workspace/test-case-view/state/test-case.state';

export interface ModifDuringExecStepState extends SqtmEntityState {
  id: number;
  actionStepTestCaseId: number;
  actionStepTestCaseName: string;
  actionStepTestCaseReference: string;
  executionTestCaseId: number;
  executionTestCaseName: string;
  executionTestCaseReference: string;
  action: string;
  expectedResult: string;
  coverages: CoverageState;
  uiState: TestCaseViewUiState;
}

export interface ModifDuringExecStepViewState extends EntityViewState<ModifDuringExecStepState, 'actionStep'> {
  actionStep: ModifDuringExecStepState;
}

export function provideModifDuringExecStepViewState(): Readonly<ModifDuringExecStepViewState> {
  return provideInitialViewState<ModifDuringExecStepState, 'actionStep'>('actionStep');
}


const coverageStateSelector = createSelector(entitySelector, (actionStep: ModifDuringExecStepState) => {
  return actionStep.coverages;
});

const idSelector = createSelector(entitySelector, (actionStep: ModifDuringExecStepState) => {
  return actionStep.id;
});

const testCaseIdSelector = createSelector(entitySelector, (actionStep: ModifDuringExecStepState) => {
  return actionStep.actionStepTestCaseId;
});

const coveragesSelector = createSelector(coverageStateSelector, coverageEntitySelectors.selectAll);

const directCoveragesSelector = createSelector(coveragesSelector, testCaseIdSelector, (coverages, id) => {
  return coverages.filter(coverage => coverage.directlyVerified);
  });

export const linkedCoverageSelector = createSelector(directCoveragesSelector, idSelector, (coverages, actionStepId) => {
  return coverages.map(coverage => {
    const linkedToStep = coverage.coverageStepInfos.filter(stepInfo => stepInfo.id === actionStepId).length > 0;
    return {...coverage, linkedToStep};
  });
});
