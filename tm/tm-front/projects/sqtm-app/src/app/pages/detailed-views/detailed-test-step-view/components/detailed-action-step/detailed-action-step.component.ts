import {ChangeDetectionStrategy, Component, Input, OnInit, ViewChild} from '@angular/core';
import {ActionStepState} from '../../../../test-case-workspace/test-case-view/state/test-step.state';
import {
  ActionErrorDisplayService,
  Attachment,
  CustomField,
  CustomFieldValue,
  EditableRichTextComponent,
  PersistedAttachment,
  RejectedAttachment
} from 'sqtm-core';
import {catchError, finalize} from 'rxjs/operators';
import {DetailedTestStepViewService} from '../../services/detailed-test-step-view.service';

@Component({
  selector: 'sqtm-app-detailed-action-step',
  templateUrl: './detailed-action-step.component.html',
  styleUrls: ['./detailed-action-step.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailedActionStepComponent implements OnInit {

  @Input()
  actionStep: ActionStepState;

  @Input()
  customFields: CustomField[];

  @ViewChild('action', {read: EditableRichTextComponent})
  actionField: EditableRichTextComponent;

  @ViewChild('expectedResult', {read: EditableRichTextComponent})
  expectedResultField: EditableRichTextComponent;

  @Input()
  editable = false;

  constructor(private detailedTestStepViewService: DetailedTestStepViewService,
              private actionErrorDisplayService: ActionErrorDisplayService) {
  }

  ngOnInit(): void {
  }

  getCfvValue(cufData: CustomField, actionStep: ActionStepState): string | string[] {
    const cfv = this.getCfv(cufData, actionStep);
    if (Boolean(cfv)) {
      return cfv.value;
    }
    return '';
  }

  private getCfv(customField: CustomField, actionStep: ActionStepState): CustomFieldValue {
    const cufId: number = customField.id;
    const cfvs: CustomFieldValue[] = Object.values(actionStep.customFieldValues.entities);
    return cfvs.find(candidate => candidate.cufId === cufId);
  }

  changeAction(action: string) {
    this.detailedTestStepViewService.changeAction(this.actionStep.id, action).pipe(
      catchError(err => this.actionErrorDisplayService.handleActionError(err)),
      finalize(() => this.actionField.endAsync()),
    ).subscribe(() => this.actionField.disableEditMode());
  }

  changeExpectedResult(expectedResult: string) {
    this.detailedTestStepViewService.changeExpectedResult(this.actionStep.id, expectedResult).pipe(
      catchError(err => this.actionErrorDisplayService.handleActionError(err)),
      finalize(() => this.expectedResultField.endAsync())
    ).subscribe(() => this.actionField.disableEditMode());
  }

  updateCustomFieldValue(customField: CustomField, value: string | string[]) {
    const cfv = this.getCfv(customField, this.actionStep);
    this.detailedTestStepViewService.updateStepCustomFieldValue(this.actionStep.id, cfv.id, value);
  }

  getAttachments(actionStep: ActionStepState): Attachment[] {
    return Object.values(actionStep.attachmentList.attachments.entities);
  }

  addAttachment(files: File[]) {
      this.detailedTestStepViewService.addAttachmentsToStep(files, this.actionStep.id, this.actionStep.attachmentList.id);
  }

  deleteAttachment(attachment: PersistedAttachment) {
    this.detailedTestStepViewService.markStepAttachmentsToDelete([attachment.id], this.actionStep.id);
  }

  confirmDeleteAttachmentEvent(attachment: PersistedAttachment) {
    this.detailedTestStepViewService.deleteStepAttachments([attachment.id], this.actionStep.id, this.actionStep.attachmentList.id);
  }

  cancelDeleteAttachmentEvent(attachment: PersistedAttachment) {
    this.detailedTestStepViewService.cancelStepAttachmentsToDelete([attachment.id], this.actionStep.id);
  }

  removeRejectedAttachment(attachment: RejectedAttachment) {
    this.detailedTestStepViewService.removeStepRejectedAttachments([attachment.id], this.actionStep.id);
  }


}
