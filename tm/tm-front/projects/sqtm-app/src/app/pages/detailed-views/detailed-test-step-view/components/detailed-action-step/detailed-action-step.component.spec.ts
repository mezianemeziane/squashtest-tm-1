import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DetailedActionStepComponent} from './detailed-action-step.component';
import {DetailedTestStepViewService} from '../../services/detailed-test-step-view.service';
import {OverlayModule} from '@angular/cdk/overlay';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ActionStepState} from '../../../../test-case-workspace/test-case-view/state/test-step.state';
import {of} from 'rxjs';
import {DetailedTestStepViewState} from '../../state/detailed-test-step-view.state';

describe('DetailedActionStepComponent', () => {
  let component: DetailedActionStepComponent;
  let fixture: ComponentFixture<DetailedActionStepComponent>;

  const serviceMock = jasmine.createSpyObj<DetailedTestStepViewService>(['changeCurrentStepIndex']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule, TranslateModule.forRoot()],
      declarations: [DetailedActionStepComponent],
      providers: [
        {
          provide: DetailedTestStepViewService,
          useValue: serviceMock
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedActionStepComponent);
    component = fixture.componentInstance;
    // component.actionStep = {} as unknown as ActionStepState;
    component.actionStep = {attachmentList: {id: 1, attachments: {ids: [], entities: {}}}} as unknown as ActionStepState;
    component.customFields = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
