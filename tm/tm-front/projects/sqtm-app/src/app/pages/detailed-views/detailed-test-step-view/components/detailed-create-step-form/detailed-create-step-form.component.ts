import {ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  ActionErrorDisplayService,
  ActionStepFormModel,
  convertFormControlValues,
  CustomField,
  CustomFieldFormComponent
} from 'sqtm-core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {catchError, takeUntil} from 'rxjs/operators';
import {CreateActionStepFormState} from '../../../../test-case-workspace/test-case-view/state/test-step.state';
import {DetailedTestStepViewService} from '../../services/detailed-test-step-view.service';
import {Subject} from 'rxjs';

@Component({
  selector: 'sqtm-app-detailed-create-step-form',
  templateUrl: './detailed-create-step-form.component.html',
  styleUrls: ['./detailed-create-step-form.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailedCreateStepFormComponent implements OnInit, OnDestroy {

  @Input()
  step: CreateActionStepFormState;

  @Input()
  testCaseId: number;

  @ViewChild(CustomFieldFormComponent, {static: true})
  customFieldFormComponent: CustomFieldFormComponent;

  @Input()
  customFields: CustomField[] = [];

  actionStepFormGroup: FormGroup;

  unsub$ = new Subject<void>();

  constructor(private formBuilder: FormBuilder,
              private translateService: TranslateService,
              private detailedTestStepViewService: DetailedTestStepViewService,
              private actionErrorDisplayService: ActionErrorDisplayService) {
    this.actionStepFormGroup = this.formBuilder.group({
      action: new FormControl(''),
      expectedResult: new FormControl('')
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  ngOnInit(): void {
    this.detailedTestStepViewService.notifyCreateStep$.pipe(
      takeUntil(this.unsub$)
    ).subscribe(() => this.addStep());
  }

  addStep() {
    if (this.isFormValid()) {
      this.doAddStep();
    } else {
      this.customFieldFormComponent.showClientSideErrors();
    }
  }

  private doAddStep() {
    const actionStepFormModel = this.getFormValue();
    this.detailedTestStepViewService.confirmAddActionStep(actionStepFormModel, this.testCaseId).pipe(
      catchError(err => this.actionErrorDisplayService.handleActionError(err))
    ).subscribe();
  }

  private isFormValid() {
    return this.actionStepFormGroup.status === 'VALID';
  }

  private getFormValue() {
    const customFieldsControls = this.actionStepFormGroup.controls['customFields'] as FormGroup;
    const rawValueMap = convertFormControlValues(this.customFields, customFieldsControls);
    const actionStepFormModel: ActionStepFormModel = {
      action: this.actionStepFormGroup.controls['action'].value,
      expectedResult: this.actionStepFormGroup.controls['expectedResult'].value,
      index: this.step.stepOrder,
      customFields: rawValueMap
    };
    return actionStepFormModel;
  }
}
