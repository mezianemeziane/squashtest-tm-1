import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { interval, Observable, Subject } from 'rxjs';
import { map, switchMap, take, takeUntil } from 'rxjs/operators';
import {
  AutomatedSuiteOverview,
  AutomatedSuitePreview,
  AutomatedSuiteService,
  DataRow,
  DialogReference,
  ReferentialDataService
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-automated-tests-execution-supervision-dialog',
  templateUrl: './automated-tests-execution-supervision-dialog.component.html',
  styleUrls: ['./automated-tests-execution-supervision-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomatedTestsExecutionSupervisionDialogComponent implements OnInit, OnDestroy {

  configuration: AutomatedSuitePreview;

  currentStep: ExecutionSupervisionDialogStep;

  tfExecutionDataRows: Partial<DataRow>[] = [];
  tfExecutionProgress: number = 0;
  squashAutomExecutionDataRows: Partial<DataRow>[] = [];

  formGroup: FormGroup;

  private automatedSuiteId: string;

  private interval$: Observable<any> = interval(5000);

  private unsub$ = new Subject<void>();

  constructor(
    private dialogRef: DialogReference<AutomatedSuitePreview>,
    private fb: FormBuilder,
    private automatedSuiteService: AutomatedSuiteService,
    private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.formGroup = this.fb.group({});
    this.configuration = this.dialogRef.data as unknown as AutomatedSuitePreview;
    if (this.configuration.isManualSlaveSelection) {
      this.currentStep = 'select-execution-server';
    } else {
      this.loadExecutionPhase();
    }
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  confirmSelectServerStep() {
    this.configuration.specification.executionConfigurations =
      Object.keys(this.formGroup.value)
        .map((key: string) => ({ projectId: +key, node: this.formGroup.value[key] }));

    this.loadExecutionPhase();
  }

  loadExecutionPhase() {
    this.automatedSuiteService.createAndExecuteAutomatedSuite(this.configuration.specification)
      .pipe(
        take(1),
        map((overview: AutomatedSuiteOverview) => ({
          ...overview,
          executions: overview.executions.map(execution => ({ id: execution.id, data: execution})),
          items: overview.items.map(item => ({ id: item.id, data: item }))
        }))).subscribe((overview: { suiteId: string, executions: Partial<DataRow>[], items: Partial<DataRow>[] }) => {
      this.dialogRef.result = true;
      this.automatedSuiteId = overview.suiteId;
      if (overview.executions.length > 0) {
        this.tfExecutionDataRows = overview.executions;
        this.startPullingExecutionStatus();
      }
      if (overview.items.length > 0) {
        this.squashAutomExecutionDataRows = overview.items;
      }
      this.cdr.detectChanges();
    });
    this.currentStep = 'execution-supervision';
  }

  startPullingExecutionStatus(): void {
    this.interval$.pipe(
        takeUntil(this.unsub$),
        switchMap(() => this.automatedSuiteService.updateExecutionsInformation(this.automatedSuiteId)),
        map((overview: AutomatedSuiteOverview) => ({
          ...overview,
          executions: overview.executions.map(execution => ({ id: execution.id, data: execution}))
        })))
        .subscribe((overview: { executions: any[], percentage: number }) => {
          this.tfExecutionDataRows = overview.executions;
          this.tfExecutionProgress = overview.percentage;
          this.cdr.detectChanges();
          if (overview.percentage === 100) {
            this.unsub$.next();
          }
        });
  }

  closeExecutionPhase() {
    if (this.tfExecutionDataRows.length > 0 && this.tfExecutionProgress !== 100) {
      this.currentStep = 'close-warning';
    } else {
      this.dialogRef.close();
    }
  }

  resumeExecutionPhase() {
    this.currentStep = 'execution-supervision';
  }

  confirmCloseExecutionPhase() {
    this.dialogRef.close();
  }
}

export type ExecutionSupervisionDialogStep = 'select-execution-server' | 'execution-supervision' | 'close-warning';
