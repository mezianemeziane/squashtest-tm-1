import {campaignWorkspaceLogger} from '../campaign-workspace.logger';

export const campaignViewLogger = campaignWorkspaceLogger.compose('campaign-view');
