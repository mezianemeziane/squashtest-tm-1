import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {
  AddTestCaseToIterationResponse,
  AttachmentService,
  CampaignPermissions,
  CustomDashboardService,
  CustomFieldValueService, EntityReference, EntityType,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ExecutionStatus,
  FavoriteDashboardValue,
  Identifier,
  IterationModel,
  IterationService,
  IterationTestPlanItem,
  PartyPreferencesService,
  ProjectData,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {IterationState} from '../state/iteration.state';
import {IterationViewState, provideInitialIterationView} from '../state/iteration-view.state';
import {itpiEntityAdapter} from '../state/iteration-test-plan-item.state';
import {concatMap, map, switchMap, take, tap, withLatestFrom} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {ItpiMassEditPatch} from '../components/itpi-multi-edit-dialog/itpi-multi-edit-dialog.component';
import {GenericTestPlanViewService} from '../../generic-test-plan-view-service';

@Injectable()
export class IterationViewService extends EntityViewService<IterationState, 'iteration', CampaignPermissions>
                                  implements GenericTestPlanViewService {

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected customFieldValueService: CustomFieldValueService,
              protected translateService: TranslateService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService,
              protected iterationService: IterationService,
              private partyPreferenceService: PartyPreferencesService,
              private customDashboardService: CustomDashboardService) {
    super(restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper);
  }

  load(id: number): Observable<any> {
    return this.restService.get<IterationModel>(['iteration-view', id.toString()]).pipe(
      map(iterationModel => {
        this.initializeIteration(iterationModel);
      })
    );
  }

  complete() {
    super.complete();
  }

  initializeIteration(iterationModel: IterationModel) {
    const attachmentEntityState = this.initializeAttachmentState(iterationModel.attachmentList.attachments);
    const customFieldValueState = this.initializeCustomFieldValueState(iterationModel.customFieldValues);
    const entityState: IterationState = {
      ...iterationModel,
      executionStatusMap: new Map<number, string>(Object.entries(iterationModel.executionStatusMap)
        .map(entry => [parseInt(entry[0], 10), entry[1] as string])),
      attachmentList: {id: iterationModel.attachmentList.id, attachments: attachmentEntityState},
      customFieldValues: customFieldValueState,
      uiState: {
        openTestCaseTreePicker: false
      }
    };

    this.initializeEntityState(entityState);
  }

  private initializeItpiState(itpi: IterationTestPlanItem[]) {
    return itpiEntityAdapter.setAll(itpi, itpiEntityAdapter.getInitialState());
  }

  addSimplePermissions(projectData: ProjectData): CampaignPermissions {
    return new CampaignPermissions(projectData);
  }

  getInitialState(): IterationViewState {
    return provideInitialIterationView();
  }

  toggleTestCaseTreePicker() {
    this.state$.pipe(
      take(1),
      map((state: IterationViewState) => this.doToggleTestCaseTreePicker(state))
    ).subscribe(state => this.commit(state));
  }

  private doToggleTestCaseTreePicker(state: IterationViewState) {
    const pickerState = state.iteration.uiState.openTestCaseTreePicker;
    return {
      ...state,
      iteration: {...state.iteration, uiState: {...state.iteration.uiState, openTestCaseTreePicker: !pickerState}}
    };
  }

  addTestCaseIntoTestPlan(testCaseIds: number[]): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: IterationViewState) => this.iterationService.addTestCase(testCaseIds, state.iteration.id)),
      withLatestFrom(this.store.state$),
      map(([response, state]) => this.updateStateAfterAddingTestCase(response, state)),
      tap(state => this.store.commit(state))
    );
  }

  private updateStateAfterAddingTestCase(response: AddTestCaseToIterationResponse, state: IterationViewState): IterationViewState {
          const executionStatusMap = new Map(state.iteration.executionStatusMap);
          const nbTestPlanItems = state.iteration.nbTestPlanItems + response.itemTestPlanIds.length;
          for (const id of response.itemTestPlanIds) {
            executionStatusMap.set(id, ExecutionStatus.READY.id);
          }

          if (!state.iteration.hasDatasets && response.hasDataSet) {
            return ({...state, iteration: {...state.iteration, hasDatasets: response.hasDataSet, executionStatusMap, nbTestPlanItems}});
          } else {
            return ({...state, iteration: {...state.iteration, executionStatusMap, nbTestPlanItems}});
          }
  }

  updateExecutionStatus(status: string, testPlanItemId: number): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap(state => this.restService.post(
        ['test-plan-item', testPlanItemId.toString(), 'execution-status'],
        {executionStatus: status})),
      withLatestFrom(this.state$),
      map(([, state]: [any, IterationViewState]) => {
        const executionStatusMap = new Map(state.iteration.executionStatusMap);
        executionStatusMap.set(testPlanItemId, status);
        return {...state, iteration: {...state.iteration, executionStatusMap}};
      }),
      tap(state => {
        this.requireExternalUpdate(state.iteration.id, state);
        this.commit(state);
      })
    );
  }

  updateScheduledStartDate(scheduledStartDate: Date): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: IterationViewState) => this.iterationService.updateScheduledStartDate(state.iteration.id, scheduledStartDate)),
      withLatestFrom(this.state$),
      map(([, state]: [void, IterationViewState]) => {
        return {...state, iteration: {...state.iteration, scheduledStartDate}};
      }),
      tap(state => this.store.commit(state))
    );
  }

  updateScheduledEndDate(scheduledEndDate: Date): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: IterationViewState) => this.iterationService.updateScheduledEndDate(state.iteration.id, scheduledEndDate)),
      withLatestFrom(this.state$),
      map(([, state]: [void, IterationViewState]) => {
        return {...state, iteration: {...state.iteration, scheduledEndDate}};
      }),
      tap(state => this.store.commit(state))
    );
  }

  updateActualStartDate(actualStartDate: Date): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: IterationViewState) => this.iterationService.updateActualStartDate(state.iteration.id, actualStartDate)),
      withLatestFrom(this.state$),
      map(([, state]: [void, IterationViewState]) => {
        return {...state, iteration: {...state.iteration, actualStartDate}};
      }),
      tap(state => this.store.commit(state))
    );
  }

  updateActualEndDate(actualEndDate: Date): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: IterationViewState) => this.iterationService.updateActualEndDate(state.iteration.id, actualEndDate)),
      withLatestFrom(this.state$),
      map(([, state]: [void, IterationViewState]) => {
        return {...state, iteration: {...state.iteration, actualEndDate}};
      }),
      tap(state => this.store.commit(state))
    );
  }

  updateActualStartAuto(actualStartAuto: boolean) {
    return this.state$.pipe(
      take(1),
      concatMap((state: IterationViewState) => this.iterationService.updateActualStartAuto(state.iteration.id, actualStartAuto)),
      withLatestFrom(this.state$),
      map(([date, state]: [Date, IterationViewState]) => {
        return {...state, iteration: {...state.iteration, actualStartAuto: actualStartAuto, actualStartDate: date}};
      }),
      tap(state => this.store.commit(state))
    );
  }

  updateActualEndAuto(actualEndAuto: boolean) {
    return this.state$.pipe(
      take(1),
      concatMap((state: IterationViewState) => this.iterationService.updateActualEndAuto(state.iteration.id, actualEndAuto)),
      withLatestFrom(this.state$),
      map(([date, state]: [Date, IterationViewState]) => {
        return {...state, iteration: {...state.iteration, actualEndAuto: actualEndAuto, actualEndDate: date}};
      }),
      tap(state => this.store.commit(state))
    );
  }

  updateAssignedUser(testPlanItemIds: number[], userId: number) {
    return this.state$.pipe(
      take(1),
      concatMap(state => this.restService.post(
        ['test-plan-item', testPlanItemIds.toString(), 'assign-user'],
        {assignee: userId}))
    );
  }

  changeItemsPosition(itemsToMove: Identifier[], position: number): Observable<any> {
    return this.state$.pipe(
      take(1),
      switchMap((state: IterationViewState) => this.iterationService.changeItemsPosition(state.iteration.id, itemsToMove, position)));
  }

  refreshStats(): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: IterationViewState) => this.iterationService.getIterationStatistics(state.iteration.id).pipe(
        map(iterationStatisticsBundle => ({...state, iteration: {...state.iteration, iterationStatisticsBundle}}))
      )),
      tap((state: IterationViewState) => this.commit(state))
    );
  }

  refreshStateAfterDeletingTestPlanItems(rowIds: number[], nbIssues: number) {
    return this.state$.pipe(
      take(1),
      map((state: IterationViewState) => {
        const executionStatusMap = new Map(state.iteration.executionStatusMap);
        for (const id of rowIds) {
          executionStatusMap.delete(id);
        }
        const nbTestPlanItems = state.iteration.nbTestPlanItems - rowIds.length;
        const iteration = {...state.iteration, executionStatusMap, nbTestPlanItems, nbIssues};
        return {...state, iteration};
      }),
      tap((state: IterationViewState) => this.commit(state))
    );
  }

  refreshExecutionStatusMapAfterMassEdit(itpiIds: number[], payload: ItpiMassEditPatch): Observable<any> {
    if (payload.executionStatus) {
      return this.state$.pipe(
        take(1),
        map((state: IterationViewState) => {
          const executionStatusMap = new Map(state.iteration.executionStatusMap);
          for (const id of itpiIds) {
            executionStatusMap.set(id, payload.executionStatus.toString());
          }
          const iteration = {...state.iteration, executionStatusMap};
          return {...state, iteration};
        }),
        tap((state: IterationViewState) => this.commit(state))
      );
    }
    return this.state$;
  }

  changeDashboardToDisplay(preferenceValue: FavoriteDashboardValue) {
    this.partyPreferenceService.changeCampaignWorkspaceFavoriteDashboard(preferenceValue).pipe(
      concatMap(() => {
        if (preferenceValue === 'default') {
          return this.refreshStats();
        } else {
          return this.refreshDashboard();
        }
      }),
      map((state: IterationViewState) => ({
        ...state,
        iteration: {...state.iteration, shouldShowFavoriteDashboard: preferenceValue === 'dashboard'}
      }))
    ).subscribe(state => this.commit(state));
  }

  refreshDashboard() {
    return this.state$.pipe(
      take(1),
      concatMap((initialState: IterationViewState) => {
        if (initialState.iteration.canShowFavoriteDashboard) {
          return this.customDashboardService.getDashboardWithDynamicScope(initialState.iteration.favoriteDashboardId, {
            milestoneDashboard: false,
            workspaceName: 'CAMPAIGN', iterationIds: [initialState.iteration.id]
          }).pipe(
            withLatestFrom(this.state$),
            map(([dashboard, state]) => ({
              ...state,
              iteration: {
                ...state.iteration,
                dashboard: {...dashboard},
                generatedDashboardOn: new Date()
              }
            }))
          );
        } else {
          return of({
            ...initialState
          });
        }
      })
    );
  }

  incrementAutomatedSuiteCount(): Observable<IterationViewState> {
    return this.state$.pipe(
      take(1),
      map((state: IterationViewState) => {
        const iteration = {...state.iteration, nbAutomatedSuites: state.iteration.nbAutomatedSuites + 1};
        return {...state, iteration};
      }),
      tap((state: IterationViewState) => this.commit(state))
    );
  }

  getEntityReference() {
    return this.state$.pipe(
      take(1),
      map((state: IterationViewState) => new EntityReference(state.iteration.id, EntityType.ITERATION))
    );
  }

  updateStateAfterExecutionDeletedInTestPlanItem(nbIssues: number): Observable<IterationViewState> {
    return this.state$.pipe(
      take(1),
      map((state: IterationViewState) => {
        const iteration = {...state.iteration, nbIssues};
        return {...state, iteration};
      }),
      tap((state: IterationViewState) => this.commit(state))
    );
  }
}
