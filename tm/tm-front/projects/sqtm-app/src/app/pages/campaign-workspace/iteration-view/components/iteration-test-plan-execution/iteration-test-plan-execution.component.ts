import {APP_BASE_HREF, DatePipe} from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {Router} from '@angular/router';
import { Dictionary } from '@ngrx/entity/src/models';
import {BehaviorSubject, combineLatest, Observable, Subject} from 'rxjs';
import {
  catchError,
  concatMap,
  delayWhen,
  filter,
  finalize,
  map,
  switchMap,
  take,
  takeUntil,
  tap,
  withLatestFrom
} from 'rxjs/operators';
import {
  ActionErrorDisplayService,
  assigneeFilter,
  AutomatedSuitePreview,
  AutomatedSuiteService,
  BindableEntity,
  buildFilters,
  CampaignPermissions,
  centredTextColumn,
  convertSqtmLiterals,
  CreateEntityDialogComponent,
  DataRow,
  DateFilterComponent,
  DateFilterValueRendererComponent,
  DialogConfiguration,
  DialogService,
  DRAG_AND_DROP_DATA,
  EntityReference,
  EntityRowReference,
  EntityType,
  EntityViewComponentData,
  executionModeColumn,
  ExecutionStatus,
  executionStatusFilter,
  Extendable,
  FilterBuilder,
  FilterOperation,
  Fixed,
  grid,
  GRID_PERSISTENCE_KEY,
  GridDefinition,
  GridDndData,
  GridFilter, GridFilterUtils,
  GridService,
  gridServiceFactory,
  GridWithStatePersistence,
  i18nEnumResearchFilter,
  indexColumn,
  InterWindowCommunicationService,
  InterWindowMessages,
  isDndDataFromTestCaseTreePicker,
  IterationService,
  milestoneLabelColumn,
  parseDataRowId,
  ProjectDataMap,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  serverBackedGridTextFilter,
  sortDate,
  SqtmDragEnterEvent,
  SqtmDragLeaveEvent,
  SqtmDropEvent,
  SquashTmDataRowType,
  StyleDefinitionBuilder,
  TEST_CASE_TREE_PICKER_ID,
  testCaseImportanceColumn,
  TestPlanResumeModel,
  textColumn,
  UserHistorySearchProvider,
  Workspaces,
  WorkspaceWithTreeComponent
} from 'sqtm-core';
import {
  EXECUTION_DIALOG_RUNNER_URL,
  EXECUTION_RUNNER_PROLOGUE_URL
} from '../../../../execution/execution-runner/execution-runner.constant';
import {dataSetColumn} from '../../../campaign-workspace/components/test-plan/dataset-cell-renderer/dataset-cell-renderer.component';
import {withProjectLinkColumn} from '../../../campaign-workspace/components/test-plan/project-link-cell/project-link-cell.component';
import {withTestCaseLinkColumn} from '../../../campaign-workspace/components/test-plan/test-case-link-cell/test-case-link-cell.component';
import {IterationViewComponentData} from '../../container/iteration-view/iteration-view.component';
import {ITERATION_TEST_PLAN_DROP_ZONE_ID, ITV_ITPE_TABLE, ITV_ITPE_TABLE_CONF} from '../../iteration-view.constant';
import {iterationViewLogger} from '../../iteration-view.logger';
import {IterationTestPlanOperationHandler} from '../../services/iteration-test-plan-operation-handler';
import {IterationViewService} from '../../services/iteration-view.service';
import {IterationViewState} from '../../state/iteration-view.state';
import {IterationState} from '../../state/iteration.state';
import {AutomatedTestsExecutionSupervisionDialogComponent} from '../automated-tests-execution-supervision-dialog/automated-tests-execution-supervision-dialog.component';
import {assignedUserColumn} from '../cell-renderers/assigned-user-cell/assigned-user-cell.component';
import {deleteTestPlanItemColumn} from '../cell-renderers/delete-test-plan-item/delete-test-plan-item.component';
import {filteredExecutionStatusColumn} from '../cell-renderers/filtered-execution-status-cell/filtered-execution-status-cell.component';
import {lastExecutionDateColumn} from '../cell-renderers/last-execution-date-cell/last-execution-date-cell.component';
import {startExecutionColumn} from '../cell-renderers/start-execution/start-execution.component';
import {SuccessRateComponent} from '../cell-renderers/success-rate/success-rate.component';
import {IterationTestPlanDraggedContentComponent} from '../iteration-test-plan-dragged-content/iteration-test-plan-dragged-content.component';
import {ItpiMultiEditDialogComponent} from '../itpi-multi-edit-dialog/itpi-multi-edit-dialog.component';
import {ItpiMultiEditDialogConfiguration} from '../itpi-multi-edit-dialog/itpi-multi-edit-dialog.configuration';
import {IterationAssignableUsersProvider} from './iteration-assignable-user-provider';
import {ExecutionRunnerOpenerService} from '../../../../execution/execution-runner/services/execution-runner-opener.service';

export function itvItpeTableDefinition(): GridDefinition {
  return grid('iteration-test-plan')
    .withColumns([
      indexColumn()
        .enableDnd()
        .withViewport('leftViewport'),
      withProjectLinkColumn('projectName')
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Extendable(80, 1))
        .withAssociatedFilter(),
      milestoneLabelColumn('milestoneLabels')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      executionModeColumn('executionMode')
        .withI18nKey('sqtm-core.entity.execution.mode.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(80)),
      textColumn('testCaseReference')
        .withI18nKey('sqtm-core.entity.generic.reference.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(90)),
      withTestCaseLinkColumn('testCaseName')
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Extendable(150, 2)),
      testCaseImportanceColumn('importance')
        .withI18nKey('sqtm-core.entity.test-case.importance.label-short-dot')
        .withTitleI18nKey('sqtm-core.entity.test-case.importance.label')
        .isEditable(false)
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(80)),
      dataSetColumn('datasetName', {kind: 'dataset', itemIdType: 'itemTestPlanId'})
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withAssociatedFilter()
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .changeWidthCalculationStrategy(new Fixed(80)),
      textColumn('testSuites')
        .withI18nKey('sqtm-core.entity.test-suite.label.singular')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Extendable(60, 2)),
      filteredExecutionStatusColumn('executionStatus')
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(60)),
      centredTextColumn('successRate')
        .disableSort()
        .withRenderer(SuccessRateComponent)
        .withI18nKey('sqtm-core.entity.execution-plan.success-rate.label.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.success-rate.label.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      assignedUserColumn('user')
        .withI18nKey('sqtm-core.generic.label.user')
        .withAssociatedFilter('login')
        .changeWidthCalculationStrategy(new Extendable(100, 2)),
      lastExecutionDateColumn('lastExecutedOn')
        .withSortFunction(sortDate)
        .withI18nKey('sqtm-core.entity.execution-plan.last-execution.label.short-dot')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.last-execution.label.long')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(130)),
      startExecutionColumn('startExecution')
        .withLabel('')
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(50))
        .withViewport('rightViewport'),
      deleteTestPlanItemColumn('delete')
        .withLabel('')
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(50))
        .withViewport('rightViewport')
    ]).server()
    .withRowConverter(itpiLiteralConverter)
    .disableRightToolBar()
    .withRowHeight(35)
    .withStyle(new StyleDefinitionBuilder()
      .enableInitialLoadAnimation()
      .showLines())
    .enableInternalDrop()
    .enableDrag()
    .withDraggedContentRenderer(IterationTestPlanDraggedContentComponent)
    .build();
}

const logger = iterationViewLogger.compose('IterationTestPlanExecutionComponent');

@Component({
  selector: 'sqtm-app-iteration-test-plan-execution',
  templateUrl: './iteration-test-plan-execution.component.html',
  styleUrls: ['./iteration-test-plan-execution.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: ITV_ITPE_TABLE_CONF,
      useFactory: itvItpeTableDefinition
    },
    IterationTestPlanOperationHandler,
    {
      provide: ITV_ITPE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ITV_ITPE_TABLE_CONF, ReferentialDataService, IterationTestPlanOperationHandler]
    },
    {provide: GridService, useExisting: ITV_ITPE_TABLE},
    {provide: UserHistorySearchProvider, useClass: IterationAssignableUsersProvider},
    {
      provide: IterationTestPlanDraggedContentComponent,
      useClass: IterationTestPlanDraggedContentComponent,
      deps: [DRAG_AND_DROP_DATA, ITV_ITPE_TABLE],
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'iteration-test-plan-grid'
    },
    GridWithStatePersistence
  ]
})
export class IterationTestPlanExecutionComponent implements OnInit, AfterViewInit, OnDestroy {

  dropZoneId = ITERATION_TEST_PLAN_DROP_ZONE_ID;

  testCaseWorkspace = Workspaces['test-case-workspace'];

  @ViewChild('content', {read: ElementRef})
  content: ElementRef;

  launchAutomatedMenuVisible = false;

  relaunchTestPlanButtonAsync$ = new BehaviorSubject<Boolean>(false);

  resumeTestPlanButtonAsync$ = new BehaviorSubject<Boolean>(false);

  launchAutomatedTestPlanButtonAsync$ = new BehaviorSubject<Boolean>(false);

  unsub$ = new Subject<void>();

  hasSelectedRows$: Observable<boolean>;
  hasSelectedAutomatedRows$: Observable<boolean>;
  activeFilters$: Observable<GridFilter[]>;

  componentData$: Observable<IterationComponentData>;

  private areAllItemsReady$: Observable<boolean>;
  private isOneItemReadyOrRunning$: Observable<boolean>;
  isOneItemAutomated$: Observable<boolean>;

  isLaunchButtonVisible$: Observable<boolean>;
  isReLaunchButtonVisible$: Observable<boolean>;
  isResumeButtonVisible$: Observable<boolean>;
  canEditExecPlan$: Observable<boolean>;
  canDeleteInExecPlan$: Observable<boolean>;

  constructor(private gridService: GridService,
              private iterationViewService: IterationViewService,
              private renderer: Renderer2,
              private interWindowCommunicationService: InterWindowCommunicationService,
              private dialogService: DialogService,
              private restService: RestService,
              private viewContainerRef: ViewContainerRef,
              private workspaceWithTree: WorkspaceWithTreeComponent,
              private iterationService: IterationService,
              private automatedSuiteService: AutomatedSuiteService,
              private actionErrorDisplayService: ActionErrorDisplayService,
              private router: Router,
              private cdref: ChangeDetectorRef,
              private gridWithStatePersistence: GridWithStatePersistence,
              private executionRunnerOpenerService: ExecutionRunnerOpenerService,
              @Inject(APP_BASE_HREF) private baseUrl: string) {
  }

  ngOnInit() {
    this.componentData$ = this.iterationViewService.componentData$;
    this.initializeCommunicationWithExecutionDialog();
    this.initializeGridFilters();
    this.initializeStateObservables();
    this.enableDragInGridAccordingToPermissions();
  }

  private enableDragInGridAccordingToPermissions() {
    this.componentData$.pipe(
      take(1)
    ).subscribe((componentData: IterationComponentData) => {
      this.gridService.setEnableDrag(componentData.permissions.canLink);
    });
  }

  private initializeStateObservables() {
    this.hasSelectedRows$ = this.gridService.hasSelectedRows$.pipe(
      takeUntil(this.unsub$)
    );

    this.hasSelectedAutomatedRows$ = this.gridService.selectedRows$.pipe(
      takeUntil(this.unsub$),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) =>
        rows.filter((row: DataRow) => row.data['executionMode'] === 'AUTOMATED').length > 0));

    this.areAllItemsReady$ = this.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => Array.from(componentData.iteration.executionStatusMap.values())
        .every(status => status === ExecutionStatus.READY.id)),
    );

    this.isOneItemReadyOrRunning$ = this.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => Array.from(componentData.iteration.executionStatusMap.values())
        .some(status => status === ExecutionStatus.READY.id || status === ExecutionStatus.RUNNING.id))
    );

    this.isOneItemAutomated$ = this.gridService.dataRows$.pipe(
      takeUntil(this.unsub$),
      map((rows: Dictionary<DataRow>) => Object.values(rows)),
      map((rows: DataRow[]) => rows.some(row => row.data['executionMode'] === 'AUTOMATED')),
    );
    this.initializeButtonObservables();
  }

  private initializeButtonObservables() {
    this.isLaunchButtonVisible$ = this.areAllItemsReady$;
    this.isReLaunchButtonVisible$ = this.areAllItemsReady$.pipe(
      map(areAllItemsReady => !areAllItemsReady));
    this.isResumeButtonVisible$ = this.areAllItemsReady$.pipe(
      map(areAllItemsReady => !areAllItemsReady),
      withLatestFrom(this.isOneItemReadyOrRunning$),
      map(([notAllReady, oneReadyOrRunning]) => notAllReady && oneReadyOrRunning));
    this.canEditExecPlan$ = combineLatest([this.hasSelectedRows$, this.componentData$]).pipe(
      takeUntil(this.unsub$),
      map(([hasSelectedRow, componentData]: [boolean, IterationComponentData]) => hasSelectedRow && componentData.permissions.canWrite)
    );
    this.canDeleteInExecPlan$ = combineLatest([this.hasSelectedRows$, this.componentData$]).pipe(
      takeUntil(this.unsub$),
      map(([hasSelectedRow, componentData]: [boolean, IterationComponentData]) => hasSelectedRow && componentData.permissions.canLink)
    );
  }

  private canDeleteAllRows(selectedRows: DataRow[]) {
    return this.filterDeletableRows(selectedRows).length === selectedRows.length;
  }

  private initializeGridFilters() {
    this.gridService.addFilters(this.buildGridFilters());
  }

  private initializeCommunicationWithExecutionDialog() {
    this.interWindowCommunicationService.interWindowMessages$.pipe(
      takeUntil(this.unsub$),
      filter((message: InterWindowMessages) => message.isTypeOf('EXECUTION-STEP-CHANGED') || message.isTypeOf('MODIFICATION-DURING-EXECUTION')),
      switchMap(() => this.refreshComponentData()),
    ).subscribe(() => {
      this.gridService.refreshData();
    });
  }

  ngAfterViewInit(): void {
    this.gridWithStatePersistence.popGridState()
      .subscribe(() => this.fetchTestPlan());

    this.activeFilters$ = this.gridService.activeFilters$.pipe(
      takeUntil(this.unsub$),
      map((gridFilters: GridFilter[]) => gridFilters.filter(gridFilter => GridFilterUtils.mustIncludeFilter(gridFilter))),
    );
  }

  private fetchTestPlan() {
    this.iterationViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        filter((componentData: IterationViewComponentData) => Boolean(componentData.iteration.id)),
        take(1),
        map(componentData => {
          const showMilestones = componentData.globalConfiguration.milestoneFeatureEnabled;
          this.gridService.setColumnVisibility('milestoneLabels', showMilestones);
          this.gridService.setColumnVisibility('hasExecutions', false);
          return componentData;
        })
      )
      .subscribe(componentData => {
        this.gridService.setServerUrl([`iteration/${componentData.iteration.id}/test-plan`]);
      });
  }

  private refreshComponentData() {
    return this.componentData$.pipe(
      take(1),
      switchMap(componentData => this.iterationViewService.load(componentData.iteration.id)),
    );
  }

  private buildGridFilters(): GridFilter[] {
    return buildFilters(
      [
        serverBackedGridTextFilter('projectName'),
        serverBackedGridTextFilter('testCaseName'),
        serverBackedGridTextFilter('testCaseReference'),
        serverBackedGridTextFilter('testSuites'),
        serverBackedGridTextFilter('datasetName'),
        executionStatusFilter('executionStatus', ResearchColumnPrototype.EXECUTION_STATUS)
          .alwaysActive(),
        i18nEnumResearchFilter('importance', ResearchColumnPrototype.TEST_CASE_IMPORTANCE)
          .alwaysActive(),
        i18nEnumResearchFilter('executionMode', ResearchColumnPrototype.EXECUTION_EXECUTION_MODE)
          .alwaysActive(),
        assigneeFilter('login', ResearchColumnPrototype.ITEM_TEST_PLAN_TESTER)
          .alwaysActive(),
        this.buildExecutionDateFilter()
      ]
    );
  }

  private buildExecutionDateFilter(): FilterBuilder {
    return new FilterBuilder('lastExecutedOn')
      .alwaysActive()
      .withColumnPrototype(ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECON)
      .withAvailableOperations([FilterOperation.BETWEEN])
      .withOperations(FilterOperation.BETWEEN)
      .withInitialValue({kind: 'multiple-date-value', value: []})
      .withWidget(DateFilterComponent)
      .withValueRenderer(DateFilterValueRendererComponent);
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.relaunchTestPlanButtonAsync$.complete();
    this.resumeTestPlanButtonAsync$.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  toggleTestCasePickerDrawer() {
    this.iterationViewService.toggleTestCaseTreePicker();
  }

  dropIntoTestPlan($event: SqtmDropEvent) {
    if ($event.dragAndDropData.origin === TEST_CASE_TREE_PICKER_ID) {
      this.dropTestCasesIntoTestPlan($event);
    }
  }

  private dropTestCasesIntoTestPlan($event: SqtmDropEvent) {
    const data = $event.dragAndDropData.data as GridDndData;
    if (logger.isDebugEnabled()) {
      logger.debug(`Dropping test cases into iteration test plan.`, [data]);
    }
    const testCaseIds = data.dataRows.map(row => parseDataRowId(row));
    this.iterationViewService.addTestCaseIntoTestPlan(testCaseIds)
      .subscribe((res: IterationViewState) => {
        this.gridService.refreshData();
        this.unmarkAsDropZone();
      });
  }

  dragEnter($event: SqtmDragEnterEvent) {
    if (isDndDataFromTestCaseTreePicker($event)) {
      this.markAsDropZone();
    }
  }

  dragLeave($event: SqtmDragLeaveEvent) {
    if (isDndDataFromTestCaseTreePicker($event)) {
      this.unmarkAsDropZone();
    }
  }

  dragCancel() {
    this.unmarkAsDropZone();
  }

  private markAsDropZone() {
    this.renderer.addClass(this.content.nativeElement, 'drop-test-case');
  }

  private unmarkAsDropZone() {
    this.renderer.removeClass(this.content.nativeElement, 'drop-test-case');
  }

  showMassDeleteItpiDialog(): void {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter(rows => !this.canDeleteAllRows(rows)),
    ).subscribe(() => {
      this.dialogService.openAlert({
        titleKey: 'sqtm-core.campaign-workspace.dialog.title.mass-remove-itpi',
        messageKey: 'sqtm-core.campaign-workspace.dialog.message.mass-remove-association.unbind-some'
      });
    });

    this.gridService.selectedRows$.pipe(
      take(1),
      filter(rows => this.canDeleteAllRows(rows)),
      switchMap((rows: DataRow[]) => this.doOpenMassDeleteDialog(rows)),
      concatMap(({iterationId, rowIds}) =>
        this.restService.delete<{nbIssues: number}>([`iteration/${iterationId.toString()}/test-plan/${rowIds.toString()}`]).pipe(
        map((response) => ({response, rowIds}))
      )),
      concatMap(({response, rowIds}) => this.iterationViewService.refreshStateAfterDeletingTestPlanItems(rowIds, response.nbIssues))
    ).subscribe(() => {
      this.gridService.refreshData();
    });
  }

  private doOpenMassDeleteDialog(selectedRows: DataRow[]):
    Observable<{ iterationId: number, rowIds: number[] }> {

    const rowIds = selectedRows.map(row => parseInt(row.data.itemTestPlanId, 10));
    const hasExecutions = selectedRows.filter(row => row.data.lastExecutedOn != null).length > 0;

    const iterationId = selectedRows[0].data.iterationId;

    let messageKey = 'sqtm-core.campaign-workspace.dialog.message.mass-remove-association.unbind';

    if (hasExecutions) {
      messageKey = 'sqtm-core.campaign-workspace.dialog.message.mass-remove-association.delete';
    }

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.mass-remove-itpi',
      messageKey: messageKey,
      level: hasExecutions ? 'DANGER' : 'WARNING'
    });

    return dialogReference.dialogClosed$.pipe(
      filter(result => Boolean(result)),
      map(() => ({iterationId, rowIds})));
  }

  private filterDeletableRows(rows: DataRow[]): DataRow[] {
    return rows.filter(row => {
      if (row.data.lastExecutedOn == null) {
        return row.simplePermissions.canWrite;
      } else {
        return row.simplePermissions.canExtendedDelete;
      }
    });
  }

  openMassEditDialog(): void {
    this.gridService.selectedRows$.pipe(
      take(1),
      switchMap((rows: DataRow[]) => this.doOpenMassEditDialog(rows)),
      filter(dialogResult => Boolean(dialogResult))
    ).subscribe(() => {
      this.gridService.refreshData();
      this.refreshIterationNode();
    });
  }

  private refreshIterationNode(): void {
    this.iterationViewService.componentData$.pipe(
      take(1),
      map(({iteration}) => new EntityRowReference(iteration.id, SquashTmDataRowType.Iteration).asString())
    ).subscribe((identifier) => this.workspaceWithTree.requireNodeRefresh([identifier]));
  }

  private doOpenMassEditDialog(rows: DataRow[]): Observable<void | undefined> {
    const dialogReference = this.dialogService.openDialog<ItpiMultiEditDialogConfiguration, void>({
      id: 'itpi-multi-edit',
      component: ItpiMultiEditDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        id: 'itpi-multi-edit',
        titleKey: 'sqtm-core.search.generic.modify.selection',
        rows,
      }
    });

    return dialogReference.dialogClosed$;
  }

  openCreateTestSuiteDialog(): void {
    this.gridService.selectedRows$.pipe(
      take(1),
      map(dataRows => dataRows.map(row => row.data.itemTestPlanId)),
      withLatestFrom(this.componentData$),
      switchMap(([itpiIds, componentData]) => this.doOpenCreateTestSuiteDialog(itpiIds, componentData.iteration)),
      filter(dialogResult => Boolean(dialogResult)),
      takeUntil(this.unsub$),
    ).subscribe();
  }

  private doOpenCreateTestSuiteDialog(selectedItems: number[], iteration: IterationState): Observable<any> {
    const parentEntityReference = new EntityRowReference(iteration.id, SquashTmDataRowType.Iteration).asString();
    const projectId = iteration.projectId;

    const configuration: DialogConfiguration = {
      id: 'add-suite',
      width: 800,
      viewContainerReference: this.viewContainerRef,
      component: CreateEntityDialogComponent,
      data: {
        titleKey: 'sqtm-core.campaign-workspace.dialog.title.new-suite',
        id: 'add-suite',
        projectId,
        bindableEntity: BindableEntity.TEST_SUITE,
        parentEntityReference,
        postUrl: 'campaign-tree/new-test-suite',
      },
    };

    const dialogReference = this.dialogService.openDialog(configuration);

    return dialogReference.dialogResultChanged$.pipe(
      takeUntil(dialogReference.dialogClosed$),
      takeUntil(this.unsub$),
      filter((addedRow) => addedRow != null),
      switchMap((addedRow: DataRow) => this.bindSelectionToTestSuite(selectedItems, addedRow)),
      tap(() => this.refreshIterationNode()),
      tap(() => this.gridService.refreshData()));
  }

  private bindSelectionToTestSuite(selectedItems: number[], testSuiteRow: DataRow): Observable<DataRow> {
    const testSuiteId: number = testSuiteRow.data.ID;
    return this.restService.post(['test-suites/test-plan/bind'], {
      itemIds: selectedItems,
      testSuiteIds: [testSuiteId],
    }).pipe(map(() => testSuiteRow));
  }

  resumeExecution() {
    this.componentData$.pipe(
      take(1),
      map(d => d.iteration.id),
      tap(() => this.resumeTestPlanButtonAsync$.next(true)),
      concatMap(id => this.iterationService.resume(id)),
      catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      finalize(() => this.resumeTestPlanButtonAsync$.next(false))
    ).subscribe((resume: TestPlanResumeModel) => this.navigateToTestPlanRunner(resume));
  }

  relaunchExecution() {
    this.dialogService.openDeletionConfirm({
      id: 'confirm-delete-all-execution',
      level: 'DANGER',
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.iteration.mass-delete-execution',
      messageKey: 'sqtm-core.campaign-workspace.dialog.message.iteration.mass-delete-execution'
    }).dialogClosed$.pipe(
      filter((confirm: boolean) => confirm),
      withLatestFrom(this.componentData$),
      tap(() => this.relaunchTestPlanButtonAsync$.next(true)),
      concatMap(([, componentData]) => this.iterationService.relaunch(componentData.iteration.id)),
      catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      finalize(() => this.relaunchTestPlanButtonAsync$.next(false)),
      map((resume: TestPlanResumeModel) => this.navigateToTestPlanRunner(resume)),
      switchMap(() => this.refreshComponentData()),
    ).subscribe(() => this.gridService.refreshData());
  }

  private navigateToTestPlanRunner(resume: TestPlanResumeModel) {
    let baseUrl = `${this.baseUrl}${EXECUTION_DIALOG_RUNNER_URL}/iteration/${resume.iterationId}/test-plan/${resume.testPlanItemId}/execution/${resume.executionId}`;
    if (resume.initialStepIndex && resume.initialStepIndex > 0) {
      baseUrl += `/step/${resume.initialStepIndex + 1}`;
    } else {
      baseUrl += `/${EXECUTION_RUNNER_PROLOGUE_URL}`;
    }
    const url = this.router.createUrlTree([baseUrl], {queryParams: {hasNextTestCase: resume.hasNextTestCase}}).toString();

    this.executionRunnerOpenerService.openExecutionWithProvidedUrl(url);
  }

  launchAllAutomatedTests() {
    this.componentData$.pipe(
      take(1),
      map(componentData => componentData.iteration.id),
      delayWhen((iterationId: number) => this.automatedSuiteService.updateTaScriptsForIteration(iterationId)),
      switchMap((iterationId: number) => this.automatedSuiteService.generateAutomatedSuitePreview({
        context: new EntityReference(iterationId, EntityType.ITERATION),
        testPlanSubsetIds: []
      }))
    ).subscribe((preview: AutomatedSuitePreview) => this.openAutomatedExecutionSupervisionDialog(preview));
  }

  launchSelectedAutomatedTests() {
    this.gridService.selectedRows$.pipe(
      take(1),
      map((rows: DataRow[]) =>
        rows.filter((row: DataRow) => row.data['executionMode'] === 'AUTOMATED')),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) => rows.map(row => row.id)),
      delayWhen((itemIds: number[]) => this.automatedSuiteService.updateTaScriptsForItems(itemIds)),
      withLatestFrom(this.componentData$),
      switchMap(([itemIds, componentData]) => this.automatedSuiteService.generateAutomatedSuitePreview({
        context: new EntityReference(componentData.iteration.id, EntityType.ITERATION),
        testPlanSubsetIds: itemIds
      }))
    ).subscribe((preview: AutomatedSuitePreview) => this.openAutomatedExecutionSupervisionDialog(preview));
  }

  private openAutomatedExecutionSupervisionDialog(data: AutomatedSuitePreview) {
    const automatedExecutionDialog = this.dialogService.openDialog({
      id: 'automated-tests-execution-supervision',
      viewContainerReference: this.viewContainerRef,
      component: AutomatedTestsExecutionSupervisionDialogComponent,
      data,
      height: 600,
      width: 800
    });
    automatedExecutionDialog.dialogClosed$.pipe(
      filter(result => Boolean(result)),
      switchMap(() => this.iterationViewService.incrementAutomatedSuiteCount())
    ).subscribe(() => this.gridService.refreshData());
  }

  openLaunchAutomatedMenu() {
    this.launchAutomatedMenuVisible = true;
  }

  shouldShowResetFilterLink(activeFilters: GridFilter[]): boolean {
    return activeFilters?.length > 0;
  }

  resetFilters() {
    this.gridService.resetFilters();
  }
}

export function itpiLiteralConverter(literals: Partial<DataRow>[], projectDataMap: ProjectDataMap): DataRow[] {
  const itpiLiterals = literals.map(li => ({...li, type: SquashTmDataRowType.IterationTestPlanItem}));

  return convertSqtmLiterals(itpiLiterals, projectDataMap);
}

export type IterationComponentData = EntityViewComponentData<IterationState, 'iteration', CampaignPermissions>;
