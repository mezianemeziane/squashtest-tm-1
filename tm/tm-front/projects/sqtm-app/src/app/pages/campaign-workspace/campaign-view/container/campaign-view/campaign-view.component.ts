import {ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {CampaignViewService} from '../../service/campaign-view.service';
import {
  AttachmentDrawerComponent,
  AttachmentState,
  CampaignPermissions,
  CampaignStatus,
  CapsuleInformationData,
  EntityRowReference,
  EntityViewComponentData,
  EntityViewService,
  GenericEntityViewService,
  Identifier,
  ReferentialDataService,
  SquashTmDataRowType,
  TestPlanStatus,
  WorkspaceWithTreeComponent
} from 'sqtm-core';
import {filter, map, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {ActivatedRoute, ParamMap} from '@angular/router';
// import {campaignFolderViewLogger} from "../../../test-case-folder-view/test-case-folder-view.logger";
import {CampaignState} from '../../state/campaign.state';

// const logger = campaignFolderViewLogger.compose('TestCaseLibraryViewComponent');

@Component({
  selector: 'sqtm-app-campaign-view',
  templateUrl: './campaign-view.component.html',
  styleUrls: ['./campaign-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: CampaignViewService,
      useClass: CampaignViewService,
    },
    {
      provide: EntityViewService,
      useExisting: CampaignViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: CampaignViewService
    }]
})
export class CampaignViewComponent implements OnInit, OnDestroy {

  componentData$: Observable<CampaignViewComponentData>;
  unsub$ = new Subject<void>();

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  @ViewChild('content', {read: ElementRef})
  content: ElementRef;

  constructor(public readonly campaignService: CampaignViewService,
              private referentialDataService: ReferentialDataService,
              private route: ActivatedRoute,
              private workspaceWithTree: WorkspaceWithTreeComponent) {
    this.componentData$ = this.campaignService.componentData$;
  }

  ngOnInit() {
    this.referentialDataService.loaded$.pipe(
      takeUntil(this.unsub$),
      filter(loaded => loaded),
      take(1)
    ).subscribe(() => {
      this.loadData();
      this.initializeTreeSynchronization();
    });
  }

  private initializeTreeSynchronization() {
    this.campaignService.simpleAttributeRequiringRefresh = ['name', 'reference', 'campaignStatus', 'description'];
    this.campaignService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.campaignService.componentData$),
      map(([{}, componentData]: [{}, CampaignViewComponentData]) =>
        new EntityRowReference(componentData.campaign.id, SquashTmDataRowType.Campaign).asString())
    ).subscribe((identifier: Identifier) => {
      this.workspaceWithTree.requireNodeRefresh([identifier]);
    });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('campaignId')),
      ).subscribe((id) => {
      this.campaignService.load(parseInt(id, 10));
    });
  }

  ngOnDestroy(): void {
    this.campaignService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  getAttachmentCount(attachmentState: AttachmentState): number {
    const attachments = Object.values(attachmentState.entities);
    return attachments.filter(attachment => attachment.kind === 'persisted-attachment').length;
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }

  createNewCampaign() {

  }

  getCampaignStatusInformationData(statusKey: string): CapsuleInformationData {
    const campaignStatus = CampaignStatus[statusKey];
    return {
      id: campaignStatus.id,
      color: campaignStatus.color,
      icon: campaignStatus.icon,
      labelI18nKey: campaignStatus.i18nKey,
      titleI18nKey: 'sqtm-core.campaign-workspace.state.label'
    };
  }


  getProgressStateInformationData(statusKey: string): CapsuleInformationData {
    const progressStatus = TestPlanStatus[statusKey];
    return {
      id: progressStatus.id,
      labelI18nKey: progressStatus.i18nKey,
      titleI18nKey: 'sqtm-core.campaign-workspace.progress-state.label'
    };
  }
}

export interface CampaignViewComponentData
  extends EntityViewComponentData<CampaignState, 'campaign', CampaignPermissions> {
}

