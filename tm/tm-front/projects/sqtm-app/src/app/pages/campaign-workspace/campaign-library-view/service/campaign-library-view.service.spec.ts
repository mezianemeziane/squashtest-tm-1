import {TestBed} from '@angular/core/testing';

import {CampaignLibraryViewService} from './campaign-library-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';

describe('CampaignLibraryViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot()],
    providers: [CampaignLibraryViewService]
  }));

  it('should be created', () => {
    const service: CampaignLibraryViewService = TestBed.inject(CampaignLibraryViewService);
    expect(service).toBeTruthy();
  });
});
