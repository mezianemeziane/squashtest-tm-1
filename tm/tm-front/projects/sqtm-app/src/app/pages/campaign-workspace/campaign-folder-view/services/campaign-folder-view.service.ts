import {Injectable} from '@angular/core';
import {
  AttachmentService,
  CampaignFolderModel,
  CampaignFolderStatisticsBundle,
  CampaignPermissions,
  CustomDashboardService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  FavoriteDashboardValue,
  PartyPreferencesService,
  ProjectData,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {CampaignFolderState} from '../state/campaign-folder.state';
import {CampaignFolderViewState, provideInitialCampaignFolderView} from '../state/campaign-folder-view.state';
import {TranslateService} from '@ngx-translate/core';
import {concatMap, map, take, tap, withLatestFrom} from 'rxjs/operators';
import {Observable, of} from 'rxjs';


@Injectable()
export class CampaignFolderViewService extends EntityViewService<CampaignFolderState, 'campaignFolder', CampaignPermissions> {

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected customFieldValueService: CustomFieldValueService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService,
              private partyPreferencesService: PartyPreferencesService,
              private customDashboardService: CustomDashboardService) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper
    );
  }

  addSimplePermissions(projectData: ProjectData): CampaignPermissions {
    return new CampaignPermissions(projectData);
  }

  getInitialState(): CampaignFolderViewState {
    return provideInitialCampaignFolderView();
  }

  load(id: number) {
    this.restService.get<CampaignFolderModel>(['campaign-folder-view', id.toString()])
      .subscribe((campaignFolderModel: CampaignFolderModel) => {
        const campaignFolder = this.initializeCampaignFolderState(campaignFolderModel);
        this.initializeEntityState(campaignFolder);
      });
  }

  private initializeCampaignFolderState(campaignFolderModel: CampaignFolderModel): CampaignFolderState {
    const attachmentEntityState = this.initializeAttachmentState(campaignFolderModel.attachmentList.attachments);
    const customFieldValueState = this.initializeCustomFieldValueState(campaignFolderModel.customFieldValues);
    return {
      ...campaignFolderModel,
      attachmentList: {id: campaignFolderModel.attachmentList.id, attachments: attachmentEntityState},
      customFieldValues: customFieldValueState
    };
  }

  changeDashboardToDisplay(preferenceValue: FavoriteDashboardValue) {
    this.partyPreferencesService.changeCampaignWorkspaceFavoriteDashboard(preferenceValue).pipe(
      concatMap(() => {
        if (preferenceValue === 'default') {
          return this.refreshStats();
        } else {
          return this.refreshDashboard();
        }
      })
    ).subscribe();
  }

  refreshStats() {
    return this.state$.pipe(
      take(1),
      concatMap((state: CampaignFolderViewState) => this.getCampaignFolderStatisticsServerSide(state.campaignFolder.id).pipe(
        map(campaignFolderStatisticsBundle => ({
          ...state, campaignFolder: {
            ...state.campaignFolder,
            campaignFolderStatisticsBundle,
            shouldShowFavoriteDashboard: false
          }
        }))
      )),
      tap(state => this.commit(state))
    );
  }

  private getCampaignFolderStatisticsServerSide(campaignFolderId): Observable<CampaignFolderStatisticsBundle> {
    return this.restService.get(['campaign-folder-view', campaignFolderId.toString(), 'statistics']);
  }

  refreshDashboard() {
    return this.state$.pipe(
      take(1),
      concatMap((initialState: CampaignFolderViewState) => {
        if (initialState.campaignFolder.favoriteDashboardId != null) {
          return this.customDashboardService.getDashboardWithDynamicScope(initialState.campaignFolder.favoriteDashboardId, {
            milestoneDashboard: false,
            workspaceName: 'CAMPAIGN', campaignFolderIds: [initialState.campaignFolder.id]
          }).pipe(
            withLatestFrom(this.state$),
            map(([dashboard, state]) => ({
              ...state,
              campaignFolder: {
                ...state.campaignFolder,
                dashboard: {...dashboard},
                generatedDashboardOn: new Date(),
                shouldShowFavoriteDashboard: true
              }
            }))
          );
        } else {
          return of({
            ...initialState,
            campaign: {...initialState.campaignFolder, shouldShowFavoriteDashboard: true}
          });
        }
      }),
      tap(state => this.commit(state))
    );
  }
}
