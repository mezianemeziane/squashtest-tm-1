import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  AttachmentDrawerComponent,
  AttachmentService,
  AttachmentState,
  CampaignPermissions,
  CampaignStatus,
  CapsuleInformationData, CustomDashboardService,
  CustomFieldValueService,
  EntityRowReference,
  EntityViewAttachmentHelperService,
  EntityViewComponentData,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  GenericEntityViewService,
  Identifier,
  IterationService,
  PartyPreferencesService,
  ReferentialDataService,
  RestService,
  SquashTmDataRowType,
  TestPlanStatus,
  WorkspaceWithTreeComponent
} from 'sqtm-core';
import {IterationState} from '../../state/iteration.state';
import {Observable, Subject} from 'rxjs';
import {filter, map, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {IterationViewService} from '../../services/iteration-view.service';
import {TranslateService} from '@ngx-translate/core';
import {GENERIC_TEST_PLAN_VIEW_SERVICE} from '../../../generic-test-plan-view-service';

@Component({
  selector: 'sqtm-app-iteration-view',
  templateUrl: './iteration-view.component.html',
  styleUrls: ['./iteration-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: IterationViewService,
      useClass: IterationViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        CustomFieldValueService,
        TranslateService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        IterationService,
        PartyPreferencesService,
        CustomDashboardService
      ]
    },
    {
      provide: EntityViewService,
      useExisting: IterationViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: IterationViewService
    },
    {
      provide: GENERIC_TEST_PLAN_VIEW_SERVICE,
      useExisting: IterationViewService
    }
  ]
})
export class IterationViewComponent implements OnInit, OnDestroy {

  componentData$: Observable<IterationViewComponentData>;

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  unsub$ = new Subject<void>();

  constructor(private route: ActivatedRoute,
              private iterationService: IterationViewService,
              private cdRef: ChangeDetectorRef,
              private referentialDataService: ReferentialDataService,
              private workspaceWithTree: WorkspaceWithTreeComponent) {
    this.componentData$ = this.iterationService.componentData$;
  }

  ngOnInit() {
    this.referentialDataService.loaded$.pipe(
      takeUntil(this.unsub$),
      filter(loaded => loaded),
      take(1)
    ).subscribe(() => {
      this.loadData();
    });
    this.initializeTreeSynchronization();
  }

  private initializeTreeSynchronization() {
    this.iterationService.simpleAttributeRequiringRefresh = ['name', 'reference', 'description', 'iterationStatus', 'users'];
    this.iterationService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.iterationService.componentData$),
      map(([{}, componentData]: [{}, IterationViewComponentData]) =>
        new EntityRowReference(componentData.iteration.id, SquashTmDataRowType.Iteration).asString())
    ).subscribe((identifier: Identifier) => {
      this.workspaceWithTree.requireNodeRefresh([identifier]);
    });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('iterationId')),
      ).subscribe((id) => {
      this.iterationService.load(parseInt(id, 10)).subscribe();
    });
  }

  ngOnDestroy(): void {
    this.iterationService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  getAttachmentCount(attachmentState: AttachmentState): number {
    const attachments = Object.values(attachmentState.entities);
    return attachments.filter(attachment => attachment.kind === 'persisted-attachment').length;
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }


  createNewIteration() {

  }

  getIterationStatusInformationData(statusKey: string): CapsuleInformationData {
    const iterationStatus = CampaignStatus[statusKey];
    return {
      id: iterationStatus.id,
      color: iterationStatus.color,
      icon: iterationStatus.icon,
      labelI18nKey: iterationStatus.i18nKey,
      titleI18nKey: 'sqtm-core.campaign-workspace.state.label'
    };
  }

  getProgressStateInformationData(statusKey: string): CapsuleInformationData {
    const progressStatus = TestPlanStatus[statusKey];
    return {
      id: progressStatus.id,
      labelI18nKey: progressStatus.i18nKey,
      titleI18nKey: 'sqtm-core.campaign-workspace.progress-state.label'
    };
  }
}

export interface IterationViewComponentData extends EntityViewComponentData<IterationState, 'iteration', CampaignPermissions> {

}
