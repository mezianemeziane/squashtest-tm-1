import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { EMPTY } from 'rxjs';
import { AutomatedSuitePreview, DialogReference, RestService } from 'sqtm-core';
import { EntityReference, EntityType } from '../../../../../../../../../cypress/integration/model/entity.model';
import { AppTestingUtilsModule } from '../../../../../utils/testing-utils/app-testing-utils.module';

import { AutomatedTestsExecutionSupervisionDialogComponent } from './automated-tests-execution-supervision-dialog.component';

describe('AutomatedTestsExecutionSupervisionDialogComponent', () => {
  let component: AutomatedTestsExecutionSupervisionDialogComponent;
  let fixture: ComponentFixture<AutomatedTestsExecutionSupervisionDialogComponent>;

  const overlayReference = jasmine.createSpyObj(['attachments']);
  overlayReference.attachments.and.returnValue(EMPTY);

  const data: AutomatedSuitePreview = {
    isManualSlaveSelection: true,
    specification: {
      context: new EntityReference(8, EntityType.ITERATION),
      testPlanSubsetIds: [3, 4, 5, 6]
    },
    projects: [{
      projectId: 1,
      label: 'Job1',
      server: 'Jenkins',
      nodes: ['Agent1', 'Agent2', 'Agent3'],
      testCount: 4
    }]
  };

  const dialogReference = new DialogReference<AutomatedSuitePreview>(
    'automated-tests-execution-supervision',
    null,
    overlayReference,
    data
  );

beforeEach(async () => {
  await TestBed.configureTestingModule({
    declarations: [ AutomatedTestsExecutionSupervisionDialogComponent ],
    imports: [AppTestingUtilsModule, ReactiveFormsModule, HttpClientTestingModule],
    providers: [{
      provide: DialogReference, useValue: dialogReference
    },
      RestService],
    schemas: [NO_ERRORS_SCHEMA]
  })
    .compileComponents();
});

beforeEach(() => {
  fixture = TestBed.createComponent(AutomatedTestsExecutionSupervisionDialogComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
});

it('should create', () => {
  expect(component).toBeTruthy();
});
});
