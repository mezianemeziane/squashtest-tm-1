import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  AbstractListCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  formatFullUserName,
  GridService,
  ListPanelItem,
  RestService,
  SimpleUser,
  TableValueChange
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {Overlay} from '@angular/cdk/overlay';
import {CampaignViewService} from '../../../service/campaign-view.service';
import {CampaignViewComponentData} from '../../../container/campaign-view/campaign-view.component';
import {filter, map, take, takeUntil} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'sqtm-app-campaign-assigned-user-cell',
  templateUrl: './campaign-assigned-user-cell.component.html',
  styleUrls: ['./campaign-assigned-user-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CampaignAssignedUserCellComponent extends AbstractListCellRendererComponent implements OnInit {

  componentData$: Observable<CampaignViewComponentData>;
  canEdit$: Observable<boolean>;

  @ViewChild('availableUsers', {read: ElementRef})
  availableUsers: ElementRef;

  @ViewChild('templatePortalContent', {read: TemplateRef})
  templatePortalContent: TemplateRef<any>;

  scrollbar: Boolean = false;
  panelItems: ListPanelItem[] = [];
  readonly rowHeight = 29;
  numberOfItemsToDisplay = 8;

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              public readonly translateService: TranslateService,
              public readonly overlay: Overlay,
              public readonly vcr: ViewContainerRef,
              public readonly restService: RestService,
              public readonly actionErrorDisplayService: ActionErrorDisplayService,
              public campaignViewService: CampaignViewService) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);
  }

  ngOnInit(): void {
    this.componentData$ = this.campaignViewService.componentData$.pipe(
      takeUntil(this.unsub$));

    this.canEdit$ = this.componentData$.pipe(
      map(componentData => componentData.permissions.canWrite && componentData.milestonesAllowModification));

    this.componentData$.pipe(
      take(1)
    ).subscribe(componentData => {
      this.panelItems.push({id: 0, label: this.translateService.instant('sqtm-core.campaign-workspace.test-plan.label.user.unassigned')});
      componentData.campaign.users.map(user => this.panelItems.push({id: user.id, label: this.getFullUsername(user)}));
      this.scrollbar = this.panelItems.length > this.numberOfItemsToDisplay;
    });
  }

  getFullUsername(user: SimpleUser): string {
    return formatFullUserName(user) || '-';
  }

  get assignedUser(): string {
    return this.row.data[this.columnDisplay.id] || '-';
  }

  showAvailableUsersList() {
    this.canEdit$.pipe(
      take(1),
      filter(canEdit => canEdit)
    ).subscribe(() => this.showList(this.availableUsers, this.templatePortalContent,
      [{originX: 'start', overlayX: 'start', originY: 'bottom', overlayY: 'top', offsetX: -10, offsetY: 6},
              {originX: 'start', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetX: -10, offsetY: -6}]
    ));
  }

  change(userId: number) {
    const userItem = this.panelItems.filter(item => item.id === userId);
    const newUserName = userId === 0 ? '-' : userItem[0].label;
    this.campaignViewService.updateAssignedUser([this.row.data['ctpiId']], userId).subscribe();
    const changes: TableValueChange[] = [
      {columnId: 'user', value: newUserName},
      {columnId: 'userId', value: userId === 0 ? null : userId},
    ];
    this.grid.editRows([this.row.id], changes);
    this.close();
  }

  get minAvailableItemsHeight(): number {
    const nbItemMin = Math.min(this.panelItems.length, this.numberOfItemsToDisplay);
    return nbItemMin * this.rowHeight;
  }
}

export function campaignAssignedUserColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(CampaignAssignedUserCellComponent)
    .withHeaderPosition('center');
}
