import {Injectable} from '@angular/core';
import {
  AddTestCaseToTestSuiteResponse,
  AttachmentService,
  CampaignPermissions,
  CustomFieldValueService, EntityReference, EntityType,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ExecutionStatus,
  Identifier,
  ProjectData,
  ReferentialDataService,
  RestService,
  TestSuiteModel,
  TestSuiteService
} from 'sqtm-core';
import {TestSuiteState} from '../state/test-suite.state';
import {TranslateService} from '@ngx-translate/core';
import {provideInitialTestSuiteView, TestSuiteViewState} from '../state/test-suite-view.state';
import {Observable} from 'rxjs';
import {concatMap, map, switchMap, take, tap, withLatestFrom} from 'rxjs/operators';
import {ItpiMassEditPatch} from '../../iteration-view/components/itpi-multi-edit-dialog/itpi-multi-edit-dialog.component';
import {GenericTestPlanViewService} from '../../generic-test-plan-view-service';

@Injectable()
export class TestSuiteViewService extends EntityViewService<TestSuiteState, 'testSuite', CampaignPermissions>
                                  implements GenericTestPlanViewService {

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected customFieldValueService: CustomFieldValueService,
              protected translateService: TranslateService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService,
              protected testSuiteService: TestSuiteService) {
    super(restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper);
  }

  load(id: number): Observable<any> {
    return this.restService.get<TestSuiteModel>(['test-suite-view', id.toString()]).pipe(
      map(testSuiteModel => {
        this.initializeTestSuite(testSuiteModel);
      })
    );
  }

  complete() {
    super.complete();
  }

  initializeTestSuite(testSuiteModel: TestSuiteModel) {
    const attachmentEntityState = this.initializeAttachmentState(testSuiteModel.attachmentList.attachments);
    const customFieldValueState = this.initializeCustomFieldValueState(testSuiteModel.customFieldValues);
    const entityState: TestSuiteState = {
      ...testSuiteModel,
      executionStatusMap: new Map<number, string>(Object.entries(testSuiteModel.executionStatusMap)
        .map(entry => [parseInt(entry[0], 10), entry[1] as string])),
      attachmentList: {id: testSuiteModel.attachmentList.id, attachments: attachmentEntityState},
      customFieldValues: customFieldValueState,
      uiState: {
        openTestCaseTreePicker: false
      }
    };

    this.initializeEntityState(entityState);
  }

  addSimplePermissions(projectData: ProjectData): CampaignPermissions {
    return new CampaignPermissions(projectData);
  }

  getInitialState(): TestSuiteViewState {
    return provideInitialTestSuiteView();
  }

  updateAssignedUser(itemTestPlanIds: number[], userId: number) {
    return this.state$.pipe(
      take(1),
      concatMap(state => this.restService.post(
        ['test-plan-item', itemTestPlanIds.toString(), 'assign-user'],
        {assignee: userId}))
    );
  }

  updateExecutionStatus(status: string, testPlanItemId: number): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap(state => this.restService.post(
        ['test-plan-item', testPlanItemId.toString(), 'execution-status'],
        {executionStatus: status})),
      withLatestFrom(this.state$),
      map(([, state]: [any, TestSuiteViewState]) => {
        const executionStatusMap = new Map(state.testSuite.executionStatusMap);
        executionStatusMap.set(testPlanItemId, status);
        const executionStatus = status;
        return {...state, testSuite: {...state.testSuite, executionStatus, executionStatusMap}};
      }),
      tap(state => {
        this.requireExternalUpdate(state.testSuite.id, state);
        this.commit(state);
      })
    );
  }

  changeItemsPosition(itemsToMove: Identifier[], position: number): Observable<any> {
    return this.state$.pipe(
      take(1),
      switchMap((state: TestSuiteViewState) => this.testSuiteService.changeItemsPosition(state.testSuite.id, itemsToMove, position))
    );
  }

  toggleTestCaseTreePicker() {
    this.state$.pipe(
      take(1),
      map((state: TestSuiteViewState) => this.doToggleTestCaseTreePicker(state))
    ).subscribe(state => this.commit(state));
  }

  addTestCaseIntoTestPlan(testCaseIds: number[]): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: TestSuiteViewState) => this.testSuiteService.addTestCase(testCaseIds, state.testSuite.id)),
      withLatestFrom(this.store.state$),
      map(([response, state]) => this.updateStateAfterAddingTestCase(response, state)),
      tap(state => this.store.commit(state))
    );
  }

  private updateStateAfterAddingTestCase(response: AddTestCaseToTestSuiteResponse, state: TestSuiteViewState): TestSuiteViewState {
    const executionStatusMap = new Map(state.testSuite.executionStatusMap);
    const nbTestPlanItems = state.testSuite.nbTestPlanItems + response.itemTestPlanIds.length;
    for (const id of response.itemTestPlanIds) {
      executionStatusMap.set(id, ExecutionStatus.READY.id);
    }

    if (!state.testSuite.hasDatasets && response.hasDataSet) {
      return ({...state, testSuite: {...state.testSuite, hasDatasets: response.hasDataSet, executionStatusMap, nbTestPlanItems}});
    } else {
      return ({...state, testSuite: {...state.testSuite, executionStatusMap, nbTestPlanItems}});
    }
  }

  private doToggleTestCaseTreePicker(state: TestSuiteViewState) {
    const pickerState = state.testSuite.uiState.openTestCaseTreePicker;
    return {
      ...state,
      testSuite: {...state.testSuite, uiState: {...state.testSuite.uiState, openTestCaseTreePicker: !pickerState}}
    };
  }

  refreshStateAfterDeletingTestPlanItems(itemTestPlanIds: number[], nbIssues: number) {
    return this.state$.pipe(
      take(1),
      map((state: TestSuiteViewState) => {
        const executionStatusMap = new Map(state.testSuite.executionStatusMap);
        for (const id of itemTestPlanIds) {
          executionStatusMap.delete(id);
        }
        const testSuite = {...state.testSuite,
          executionStatusMap,
          nbTestPlanItems: state.testSuite.nbTestPlanItems - itemTestPlanIds.length,
          nbIssues
        };
        return {...state, testSuite};
      }),
      tap((state: TestSuiteViewState) => this.commit(state))
    );
  }

  refreshExecutionStatusMapAfterMassEdit(itpiIds: number[], payload: ItpiMassEditPatch) {
    if (payload.executionStatus) {
      return this.state$.pipe(
        take(1),
        map((state: TestSuiteViewState) => {
          const executionStatusMap = new Map(state.testSuite.executionStatusMap);
          for (const id of itpiIds) {
            executionStatusMap.set(id, payload.executionStatus.toString());
          }
          const testSuite = {...state.testSuite, executionStatusMap};
          return {...state, testSuite};
        }),
        tap((state: TestSuiteViewState) => this.commit(state))
      );
    }
    return this.state$;
  }

  incrementAutomatedSuiteCount(): Observable<any> {
    return this.state$.pipe(
      take(1),
      map((state: TestSuiteViewState) => {
        const testSuite = {...state.testSuite, nbAutomatedSuites: state.testSuite.nbAutomatedSuites + 1};
        return {...state, testSuite};
      }),
      tap((state: TestSuiteViewState) => this.commit(state))
    );
  }

  getEntityReference() {
    return this.state$.pipe(
      take(1),
      map((state: TestSuiteViewState) => new EntityReference(state.testSuite.id, EntityType.TEST_SUITE))
    );
  }

  updateStateAfterExecutionDeletedInTestPlanItem(nbIssues: number): Observable<TestSuiteViewState> {
    return this.state$.pipe(
      take(1),
      map((state: TestSuiteViewState) => {
        const testSuite = {...state.testSuite, nbIssues};
        return {...state, testSuite};
      }),
      tap((state: TestSuiteViewState) => this.commit(state))
    );
  }
}
