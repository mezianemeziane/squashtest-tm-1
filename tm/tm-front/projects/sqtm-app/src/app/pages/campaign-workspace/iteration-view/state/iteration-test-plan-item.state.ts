import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {IterationTestPlanItem} from 'sqtm-core';

export interface IterationTestPlanItemState extends EntityState<IterationTestPlanItem> {
  executionStatus: string;
  user: string;
  dataSet: string;
}

export const itpiEntityAdapter = createEntityAdapter<IterationTestPlanItem>({
  selectId: (itpi) => itpi.id
});

export const itpiEntitySelectors = itpiEntityAdapter.getSelectors();
