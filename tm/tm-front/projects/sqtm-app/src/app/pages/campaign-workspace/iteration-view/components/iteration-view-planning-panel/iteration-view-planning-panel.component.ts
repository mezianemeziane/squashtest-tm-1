import {ChangeDetectionStrategy, Component, ElementRef, Host, Input, ViewChild} from '@angular/core';
import {DialogReference, DialogService, EditableDateFieldComponent} from 'sqtm-core';
import {IterationViewComponentData} from '../../container/iteration-view/iteration-view.component';
import {IterationViewService} from '../../services/iteration-view.service';

@Component({
  selector: 'sqtm-app-iteration-view-planning-panel',
  templateUrl: './iteration-view-planning-panel.component.html',
  styleUrls: ['./iteration-view-planning-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IterationViewPlanningPanelComponent {

  @ViewChild('actualStartDate')
  actualStartDate: EditableDateFieldComponent;

  @ViewChild('actualEndDate')
  actualEndDate: EditableDateFieldComponent;

  @ViewChild('scheduledStartDate')
  scheduledStartDate: EditableDateFieldComponent;

  @ViewChild('scheduledEndDate')
  scheduledEndDate: EditableDateFieldComponent;

  @Input()
  iterationViewComponentData: IterationViewComponentData;

  private readonly TWO_COLUMNS_LAYOUT_MIN_SIZE = 600;

  constructor(private iterationViewService: IterationViewService,
              private dialogService: DialogService,
              @Host() private hostElement: ElementRef<HTMLElement>) {}


  get isSmallLayout(): boolean {
    return this.hostElement.nativeElement.getBoundingClientRect().width < this.TWO_COLUMNS_LAYOUT_MIN_SIZE;
  }

  get canEdit(): boolean {
    return this.iterationViewComponentData.permissions.canWrite && this.iterationViewComponentData.milestonesAllowModification;
  }

  get isActualStartDateEditable(): boolean {
    return this.canEdit && !this.iterationViewComponentData.iteration.actualStartAuto;
  }

  get isActualEndDateEditable(): boolean {
    return this.canEdit && !this.iterationViewComponentData.iteration.actualEndAuto;
  }

  updateScheduledStartDate(scheduledStartDate: Date, scheduledEndDate: Date) {
    if (this.canUpdateDate(scheduledStartDate, scheduledEndDate)) {
      this.iterationViewService.updateScheduledStartDate(scheduledStartDate).subscribe(
        () => this.scheduledStartDate.value = scheduledStartDate
      );
    } else {
      this.showAlertDateDialog();
      this.scheduledStartDate.cancel();
    }
  }

  updateActualStartDate(actualStartDate: Date, actualEndDate: Date) {
    if (this.canUpdateDate(actualStartDate, actualEndDate)) {
      this.iterationViewService.updateActualStartDate(actualStartDate).subscribe(
        () => this.actualStartDate.value = actualStartDate
      );
    } else {
      this.showAlertDateDialog();
      this.actualStartDate.cancel();
    }
  }

  updateActualStartAuto(startAuto: boolean) {
    this.iterationViewService.updateActualStartAuto(startAuto).subscribe();
  }

  updateScheduledEndDate(scheduledEndDate: Date, scheduledStartDate: Date) {
    if (this.canUpdateDate(scheduledStartDate, scheduledEndDate)) {
      this.iterationViewService.updateScheduledEndDate(scheduledEndDate).subscribe(
        () => this.scheduledEndDate.value = scheduledEndDate
      );
    } else {
      this.showAlertDateDialog();
      this.scheduledEndDate.cancel();
    }
  }

  updateActualEndDate(actualEndDate: Date, actualStartDate: Date) {
    if (this.canUpdateDate(actualStartDate, actualEndDate)) {
      this.iterationViewService.updateActualEndDate(actualEndDate).subscribe(
        () => this.actualEndDate.value = actualEndDate
      );
    } else {
      this.showAlertDateDialog();
      this.actualEndDate.cancel();
    }
  }

  updateActualEndAuto(endAuto: boolean) {
    this.iterationViewService.updateActualEndAuto(endAuto).subscribe();
  }

  showAlertDateDialog(): DialogReference {
    return this.dialogService.openAlert({
      level: 'DANGER',
      messageKey: 'sqtm-core.validation.errors.time-period-not-consistent'
    });
  }

  private canUpdateDate(startDate: Date, endDate: Date) {
    if (startDate != null && endDate != null) {
      const start = new Date(startDate);
      const end = new Date(endDate);
      return start < end;
    } else {
      return true;
    }
  }
}
