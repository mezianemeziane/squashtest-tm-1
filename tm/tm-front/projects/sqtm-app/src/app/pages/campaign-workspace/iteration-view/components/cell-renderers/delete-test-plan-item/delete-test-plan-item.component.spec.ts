import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DeleteTestPlanItemComponent} from './delete-test-plan-item.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {
  DataRow,
  DialogService, EntityViewService,
  GenericDataRow,
  getBasicGridDisplay,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  Limited,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {EMPTY} from 'rxjs';
import {IterationViewService} from '../../../services/iteration-view.service';

describe('DeleteTestPlanItemComponent', () => {
  let component: DeleteTestPlanItemComponent;
  let fixture: ComponentFixture<DeleteTestPlanItemComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  let iterationViewService = jasmine.createSpyObj('iterationViewService', ['load']);
  iterationViewService = {...iterationViewService, componentData$: EMPTY};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteTestPlanItemComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: DialogService,
          useValue: dialogService
        },
        {
          provide: IterationViewService,
          useValue: iterationViewService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  it('should create', waitForAsync(() => {
    prepareComponent({
      ...new GenericDataRow(),
      id: 'itpi-1',
      data: {itemTestPlanId: 'itpi-1', lastExecutedOn: '2020-02-05', testCaseName: 'TC1'}
    });
    expect(component).toBeTruthy();
  }));

  it('should display delete icon', waitForAsync(() => {
    const dataRow = {
      ...new GenericDataRow(),
      id: 'itpi-1',
      data: {itemTestPlanId: 'itpi-1', lastExecutedOn: '2020-02-05', testCaseName: 'TC1'},
    };
    prepareComponent(dataRow);

    const iconName = component.getIcon(component.row);
    expect(iconName).toBe('sqtm-core-generic:delete');

    // const iconElem = fixture.nativeElement.querySelector('i');
    // expect(iconElem.classList.contains('anticon-delete')).toBeTruthy();
  }));

  it('should display minus icon', waitForAsync(() => {
    const dataRow = {
      ...new GenericDataRow(),
      id: 'itpi-1',
      data: {itemTestPlanId: 'itpi-1', lastExecutedOn: null, testCaseName: 'TC1'},
    };
    prepareComponent(dataRow);

    const expectedName = 'sqtm-core-generic:unlink';
    const iconName = component.getIcon(component.row);
    expect(iconName).toBe(expectedName);
  }));

  function prepareComponent(row: DataRow) {
    fixture = TestBed.createComponent(DeleteTestPlanItemComponent);
    component = fixture.componentInstance;
    component.gridDisplay = getBasicGridDisplay();
    component.columnDisplay = {
      id: 'NAME', show: true,
      widthCalculationStrategy: new Limited(200),
      headerPosition: 'left', contentPosition: 'left', showHeader: true,
      viewportName: 'mainViewport'
    };
    component.row = row;
    fixture.detectChanges();
  }
});
