import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import { TestSuiteTestPlanDraggedContentComponent } from './test-suite-test-plan-dragged-content.component';
import {DRAG_AND_DROP_DATA, GridService} from 'sqtm-core';
import {mockGridService, mockPassThroughTranslateService} from '../../../../../utils/testing-utils/mocks.service';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('TestSuiteTestPlanDraggedContentComponent', () => {
  let component: TestSuiteTestPlanDraggedContentComponent;
  let fixture: ComponentFixture<TestSuiteTestPlanDraggedContentComponent>;

  beforeEach(waitForAsync( () => {
    TestBed.configureTestingModule({
      declarations: [TestSuiteTestPlanDraggedContentComponent],
      providers: [
        {provide: DRAG_AND_DROP_DATA, useValue: {data: {dataRows: []}}},
        {provide: GridService, useValue: mockGridService()},
        {provide: TranslateService, useValue: mockPassThroughTranslateService()},
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestSuiteTestPlanDraggedContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
