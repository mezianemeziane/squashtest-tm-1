import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CampaignViewComponent} from './campaign-view.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import {WorkspaceWithTreeComponent} from 'sqtm-core';

describe('CampaignViewComponent', () => {
  let component: CampaignViewComponent;
  let fixture: ComponentFixture<CampaignViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, RouterTestingModule, TranslateModule.forRoot(), NzDropDownModule],
      declarations: [CampaignViewComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: WorkspaceWithTreeComponent, useValue: WorkspaceWithTreeComponent}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
