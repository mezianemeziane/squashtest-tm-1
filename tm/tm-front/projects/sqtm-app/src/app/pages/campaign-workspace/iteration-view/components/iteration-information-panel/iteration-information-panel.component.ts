import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {IterationViewComponentData} from '../../container/iteration-view/iteration-view.component';
import {CustomFieldData, TestPlanStatus} from 'sqtm-core';
import {IterationViewService} from '../../services/iteration-view.service';
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'sqtm-app-iteration-information-panel',
  templateUrl: './iteration-information-panel.component.html',
  styleUrls: ['./iteration-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IterationInformationPanelComponent implements OnInit {

  @Input()
  iterationViewComponentData: IterationViewComponentData;

  @Input()
  customFieldData: CustomFieldData[];

  constructor(public iterationViewService: IterationViewService,
              public translateService: TranslateService) {
  }

  ngOnInit(): void {
  }

  trackCfd(cfd: CustomFieldData) {
    return cfd.id;
  }

  getProgressStatus(statusKey: any) {
    const testPlanStatus = TestPlanStatus[statusKey];
    return testPlanStatus.i18nKey;
  }

  copy() {

  }
}
