import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CampaignTestPlanExecutionComponent, cpvCtpeTableDefinition} from './campaign-test-plan-execution.component';
import {
  DataRow,
  DialogService, GRID_PERSISTENCE_KEY,
  GridService,
  GridTestingModule, GridWithStatePersistence,
  RestService,
  WorkspaceWithTreeComponent
} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {CampaignViewService} from '../../service/campaign-view.service';
import {BehaviorSubject, of} from 'rxjs';
import {CampaignViewComponentData} from '../../container/campaign-view/campaign-view.component';
import {
  mockClosableDialogService,
  mockGridService,
  mockRestService
} from '../../../../../utils/testing-utils/mocks.service';
import {mockGlobalConfiguration} from '../../../../../utils/testing-utils/mocks.data';
import {CampaignState} from '../../state/campaign.state';
import {CPV_CTPE_TABLE, CPV_CTPE_TABLE_CONF} from '../../campaign-view.constant';

describe('CampaignTestPlanExecutionComponent', () => {
  let component: CampaignTestPlanExecutionComponent;
  let fixture: ComponentFixture<CampaignTestPlanExecutionComponent>;

  let componentData$: BehaviorSubject<CampaignViewComponentData>;
  let gridService;
  let campaignViewService;
  let dialogMock;
  let restService;
  let workspaceWithTree;

  beforeEach(waitForAsync(() => {
    componentData$ = new BehaviorSubject<CampaignViewComponentData>({
      globalConfiguration: mockGlobalConfiguration({milestoneFeatureEnabled: true}),
      campaign: {
        id: 2,
        hasDatasets: true,
        uiState: {
          openTestCaseTreePicker: false,
        }
      } as unknown as CampaignState
    } as CampaignViewComponentData);
    campaignViewService = jasmine.createSpyObj('campaignViewService', ['toggleTestCaseTreePicker',
      'refreshNbTestPlanItemsAfterDelete']);
    campaignViewService = {...campaignViewService, componentData$};
    gridService = mockGridService();
    dialogMock = mockClosableDialogService();
    restService = mockRestService();
    workspaceWithTree = jasmine.createSpyObj(['requireNodeRefresh']);
    workspaceWithTree.gridService = mockGridService();
    campaignViewService.refreshNbTestPlanItemsAfterDelete.and.returnValue(of(null));

    TestBed.configureTestingModule({
      declarations: [CampaignTestPlanExecutionComponent],
      imports: [GridTestingModule, AppTestingUtilsModule, HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: CampaignViewService, useValue: campaignViewService},
        {
          provide: CPV_CTPE_TABLE_CONF,
          useFactory: cpvCtpeTableDefinition
        },
        {
          provide: CPV_CTPE_TABLE,
          useValue: gridService
        },
        {
          provide: GridService,
          useValue: gridService
        },
        {provide: RestService, useValue: restService},
        {provide: DialogService, useValue: dialogMock.service},
        {provide: WorkspaceWithTreeComponent, useValue: workspaceWithTree},
        {
          provide: GRID_PERSISTENCE_KEY,
          useValue: 'execution-plan-grid'
        },
        GridWithStatePersistence,
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignTestPlanExecutionComponent);
    component = fixture.componentInstance;
    component['gridService'] = gridService;
    gridService.activeFilters$ = of([]);
    fixture.detectChanges();
  });

  it('should toggle test case picker drawer', waitForAsync(() => {
    component.toggleTestCasePickerDrawer();
    expect(campaignViewService.toggleTestCaseTreePicker).toHaveBeenCalled();
  }));

  it('should open mass delete dialog', () => {
    gridService.selectedRows$ = of([
      {data: {ctpiId: 3, campaignId: 2}, simplePermissions: {canExtendedDelete: true}} as unknown as DataRow,
      {data: {ctpiId: 5, campaignId: 2}, simplePermissions: {canExtendedDelete: true}} as unknown as DataRow,
    ]);
    component.showMassDeleteCtpiDialog();
    dialogMock.closeDialogsWithResult(true);

    expect(restService.delete).toHaveBeenCalledWith(['campaign/2/test-plan/3,5']);
    expect(gridService.refreshData).toHaveBeenCalled();
  });

  it('should open mass edit dialog', () => {
    gridService.selectedRows$ = of([{data: {ctpiId: 2}} as unknown as DataRow]);
    component.openMassEditDialog();
    dialogMock.closeDialogsWithResult(true);

    expect(gridService.refreshData).toHaveBeenCalled();
    expect(workspaceWithTree.requireNodeRefresh).toHaveBeenCalledWith(['Campaign-2']);
  });
});
