import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CampaignTestPlanDraggedContentComponent} from './campaign-test-plan-dragged-content.component';
import {DRAG_AND_DROP_DATA, GridService} from 'sqtm-core';
import {mockGridService, mockPassThroughTranslateService} from '../../../../../utils/testing-utils/mocks.service';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('CampaignTestPlanDraggedContentComponent', () => {
  let component: CampaignTestPlanDraggedContentComponent;
  let fixture: ComponentFixture<CampaignTestPlanDraggedContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CampaignTestPlanDraggedContentComponent],
      providers: [
        {provide: DRAG_AND_DROP_DATA, useValue: {data: {dataRows: []}}},
        {provide: GridService, useValue: mockGridService()},
        {provide: TranslateService, useValue: mockPassThroughTranslateService()},
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignTestPlanDraggedContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
