import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {take, tap} from 'rxjs/operators';
import {
  Extendable,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  issueExecutionColumn,
  issueKeyColumn,
  PaginationConfigBuilder,
  ReferentialDataService,
  RestService,
  Sort,
  textColumn
} from 'sqtm-core';
import {CAMPAIGN_FOLDER_ISSUE_TABLE, CAMPAIGN_FOLDER_ISSUE_TABLE_CONF} from '../../campaign-folder-view.constant';
import {CampaignFolderViewService} from '../../services/campaign-folder-view.service';

@Component({
  selector: 'sqtm-app-campaign-issues',
  template: `
    <ng-container *ngIf="campaignViewService.componentData$ | async as componentData">
      <div class="flex-column full-height full-width p-r-15" style="overflow-y: hidden;">
        <div class="sqtm-large-grid-container-title">
          {{'sqtm-core.test-case-workspace.title.issues' | translate}}
        </div>
        <div class="sqtm-large-grid-container full-width full-height p-15">
          <sqtm-core-issues-panel [entityId]="componentData.campaignFolder.id"
                                  [entityType]="'CAMPAIGN_FOLDER_TYPE'"
                                  [bugTracker]="componentData.projectData.bugTracker"
                                  (loadIssues)="loadData()">
            <sqtm-core-grid></sqtm-core-grid>
          </sqtm-core-issues-panel>
        </div>
      </div>
    </ng-container>`,
  styleUrls: ['./campaign-folder-issues.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: CAMPAIGN_FOLDER_ISSUE_TABLE_CONF,
      useFactory: campaignFolderIssuesTableDefinition
    },
    {
      provide: CAMPAIGN_FOLDER_ISSUE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, CAMPAIGN_FOLDER_ISSUE_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: CAMPAIGN_FOLDER_ISSUE_TABLE
    }
  ]
})
export class CampaignFolderIssuesComponent implements OnInit {

  constructor(public campaignViewService: CampaignFolderViewService, private gridService: GridService) {
  }

  ngOnInit(): void {
  }

  loadData() {
    this.campaignViewService.componentData$.pipe(
      take(1),
      tap(componentData =>
        this.gridService.setServerUrl([`issues/campaign-folder/${componentData.campaignFolder.id}/known-issues`]))
    ).subscribe();
  }

}

export function campaignFolderIssuesTableDefinition(): GridDefinition {
  return grid('campaign-folder-view-issue').withColumns([
    issueKeyColumn('remoteId').withI18nKey('sqtm-core.entity.issue.key.label'),
    textColumn('btProject').withI18nKey('sqtm-core.entity.issue.project.label')
      .changeWidthCalculationStrategy(new Extendable(100, 1)).disableSort(),
    textColumn('summary').withI18nKey('sqtm-core.entity.issue.summary.label')
      .changeWidthCalculationStrategy(new Extendable(100, 1)).disableSort(),
    textColumn('priority').withI18nKey('sqtm-core.entity.issue.priority.label')
      .changeWidthCalculationStrategy(new Extendable(100, 0.4)).disableSort(),
    textColumn('status').withI18nKey('sqtm-core.entity.issue.status.label')
      .changeWidthCalculationStrategy(new Extendable(100, 0.4)).disableSort(),
    textColumn('assignee').withI18nKey('sqtm-core.entity.issue.assignee.label')
      .changeWidthCalculationStrategy(new Extendable(100, 1)).disableSort(),
    issueExecutionColumn('executionName').withI18nKey('sqtm-core.entity.issue.reported-in.label')
      .changeWidthCalculationStrategy(new Extendable(100, 1.5)).disableSort(),
  ]).disableRightToolBar().server().withInitialSortedColumns([{
    id: 'remoteId',
    sort: Sort.DESC
  }]).withPagination(new PaginationConfigBuilder().initialSize(25)).withRowHeight(35).build();
}
