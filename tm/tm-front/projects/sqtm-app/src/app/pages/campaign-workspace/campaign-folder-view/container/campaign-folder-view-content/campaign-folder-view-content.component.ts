import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {CampaignFolderViewService} from '../../services/campaign-folder-view.service';
import {Observable, Subject} from 'rxjs';
import {CampaignFolderViewComponentData} from '../campaign-folder-view/campaign-folder-view.component';
import {BindableEntity, createCustomFieldValueDataSelector, CustomFieldData} from 'sqtm-core';
import {takeUntil} from 'rxjs/operators';
import {select} from '@ngrx/store';

@Component({
  selector: 'sqtm-app-campaign-folder-view-content',
  templateUrl: './campaign-folder-view-content.component.html',
  styleUrls: ['./campaign-folder-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CampaignFolderViewContentComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();
  componentData$: Observable<CampaignFolderViewComponentData>;

  customFieldData: CustomFieldData[];

  constructor(private campaignFolderViewService: CampaignFolderViewService) {
    this.componentData$ = campaignFolderViewService.componentData$;
  }

  ngOnInit() {
    this.initCufs();
  }

  initCufs() {
    this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createCustomFieldValueDataSelector(BindableEntity.CAMPAIGN_FOLDER))
    ).subscribe(customFieldData => {
      this.customFieldData = customFieldData;
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  trackCfd(cfd: CustomFieldData) {
    return cfd.id;
  }
}
