import {ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {CampaignFolderViewService} from '../../services/campaign-folder-view.service';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {filter, map, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {
  AttachmentDrawerComponent,
  AttachmentState,
  CampaignPermissions,
  EntityRowReference,
  EntityViewComponentData,
  EntityViewService,
  GenericEntityViewService,
  Identifier,
  ReferentialDataService,
  SquashTmDataRowType,
  WorkspaceWithTreeComponent
} from 'sqtm-core';
import {CampaignFolderState} from '../../state/campaign-folder.state';
import {campaignFolderViewLogger} from '../../campaign-folder-view.logger';

const logger = campaignFolderViewLogger.compose('CampaignFolderViewComponent');

@Component({
  selector: 'sqtm-app-campaign-folder-view',
  templateUrl: './campaign-folder-view.component.html',
  styleUrls: ['./campaign-folder-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: CampaignFolderViewService,
      useClass: CampaignFolderViewService,
    },
    {
      provide: EntityViewService,
      useExisting: CampaignFolderViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: CampaignFolderViewService
    }
  ]
})
export class CampaignFolderViewComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  @ViewChild('content', {read: ElementRef})
  content: ElementRef;

  constructor(private route: ActivatedRoute,
              public readonly campaignFolderService: CampaignFolderViewService,
              private referentialDataService: ReferentialDataService,
              private workspaceWithTree: WorkspaceWithTreeComponent) {
    logger.debug('Instantiate CampaignFolderViewComponent');
  }

  ngOnInit() {
    this.referentialDataService.loaded$.pipe(
      takeUntil(this.unsub$),
      filter(loaded => loaded),
      take(1)
    ).subscribe(() => {
      logger.debug(`Loading CampaignFolderViewComponent Data by http request`);
      this.loadData();
    });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('campaignFolderId')),
      ).subscribe((id) => {
      this.campaignFolderService.load(parseInt(id, 10));
      this.initializeTreeSynchronization();
    });
  }

  ngOnDestroy(): void {
    this.campaignFolderService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  private initializeTreeSynchronization() {
    this.campaignFolderService.simpleAttributeRequiringRefresh = ['name'];
    this.campaignFolderService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.campaignFolderService.componentData$),
      map(([{}, componentData]: [{}, CampaignFolderViewComponentData]) =>
        new EntityRowReference(componentData.campaignFolder.id, SquashTmDataRowType.CampaignFolder).asString())
    ).subscribe((identifier: Identifier) => {
      this.workspaceWithTree.requireNodeRefresh([identifier]);
    });
  }


  getAttachmentCount(attachmentState: AttachmentState): number {
    const attachments = Object.values(attachmentState.entities);
    return attachments.filter(attachment => attachment.kind === 'persisted-attachment').length;
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }
}

export interface CampaignFolderViewComponentData
  extends EntityViewComponentData<CampaignFolderState, 'campaignFolder', CampaignPermissions> {
}

