import {
  dateTimeColumn,
  executionStatusColumn,
  Extendable,
  grid,
  GridDefinition,
  indexColumn,
  Sort,
  textColumn
} from 'sqtm-core';
import {automatedExecutionDetailsColumn} from './iteration-view/components/cell-renderers/automated-execution-details-renderer/automated-execution-details-renderer.component';
import {automatedExecutionReportColumn} from './iteration-view/components/cell-renderers/automated-execution-report-renderer/automated-execution-report-renderer.component';

export function automatedSuiteTableDefinition(): GridDefinition {
  return grid('automated-suite-grid')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      dateTimeColumn('createdOn')
        .withI18nKey('sqtm-core.entity.generic.created-on.masculine')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      executionStatusColumn('executionStatus')
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .disableSort(),
      automatedExecutionDetailsColumn('executionDetails')
        .withI18nKey('sqtm-core.campaign-workspace.automated-suite.label.exec-details')
        .changeWidthCalculationStrategy(new Extendable(100, 1)).disableSort(),
      automatedExecutionReportColumn('executionReport')
        .withI18nKey('sqtm-core.campaign-workspace.automated-suite.label.exec-report')
        .changeWidthCalculationStrategy(new Extendable(100, 1)).disableSort(),
      textColumn('createdBy')
        .withI18nKey('sqtm-core.grid.header.created-by.masculine')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      dateTimeColumn('lastModifiedOn')
        .withI18nKey('sqtm-core.entity.generic.last-modified-on.feminine')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
    ]).server()
    .withInitialSortedColumns([{id: 'createdOn', sort: Sort.DESC}])
    .disableRightToolBar()
    .withRowHeight(35)
    .build();
}
