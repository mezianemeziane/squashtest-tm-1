import {Component, OnInit, ChangeDetectionStrategy, Input, ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import {CampaignViewComponentData} from '../campaign-view/container/campaign-view/campaign-view.component';
import {CampaignViewService} from '../campaign-view/service/campaign-view.service';
import {TranslateService} from '@ngx-translate/core';
import {IterationViewComponentData} from '../iteration-view/container/iteration-view/iteration-view.component';
import {TestSuiteViewComponentData} from '../test-suite-view/containers/test-suite-view/test-suite-view.component';
import {ExecutionStatus, ExecutionStatusKeys, ProjectData} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-campaign-statistics-panel',
  templateUrl: './campaign-statistics-panel.component.html',
  styleUrls: ['./campaign-statistics-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CampaignStatisticsPanelComponent implements OnInit {

  @Input()
  componentData: CampaignViewComponentData['campaign'] | IterationViewComponentData['iteration'] | TestSuiteViewComponentData['testSuite'];

  @Input()
  projectData: ProjectData;

  constructor(public translateService: TranslateService) { }

  ngOnInit(): void {
  }

  getDoneTestCases(): number {
    return this.componentData.testPlanStatistics.nbDone;
  }

  getTotalTestCases(): number {
    return this.componentData.testPlanStatistics.nbTestCases;
  }

  getCampaignProgression() {
    return this.componentData.testPlanStatistics.progression;
  }

  displayCampaignProgression(): string {
    return `( ${this.getDoneTestCases()} / ${this.getTotalTestCases()} )`;
  }

  enableExecutionStatus(statusKey: string): boolean {
    return this.projectData.disabledExecutionStatus.filter(status => status === statusKey).length === 0;
  }

  getI1I8nLabel(status: string) {
    return `sqtm-core.campaign-workspace.statistics.label.${status}`;
  }

  getStatusStats(): any[] {
    const stats: any[] = new Array();
    stats.push({label: this.getI1I8nLabel('COUNT'), value: this.componentData.testPlanStatistics.nbTestCases});
    stats.push({label: this.getI1I8nLabel('READY'), value: this.componentData.testPlanStatistics.nbReady});
    stats.push({label: this.getI1I8nLabel('RUNNING'), value: this.componentData.testPlanStatistics.nbRunning});
    stats.push({label: this.getI1I8nLabel('SUCCESS'), value: this.componentData.testPlanStatistics.nbSuccess});
    if (this.enableExecutionStatus('SETTLED')) {
      stats.push({label: this.getI1I8nLabel('SETTLED'), value: this.componentData.testPlanStatistics.nbSettled});
    }
    stats.push({label: this.getI1I8nLabel('FAILURE'), value: this.componentData.testPlanStatistics.nbFailure});
    stats.push({label: this.getI1I8nLabel('BLOCKED'), value: this.componentData.testPlanStatistics.nbBlocked});
    if (this.enableExecutionStatus('UNTESTABLE')) {
      stats.push({label: this.getI1I8nLabel('UNTESTABLE'), value: this.componentData.testPlanStatistics.nbUntestable});
    }
    return stats;
  }
}
