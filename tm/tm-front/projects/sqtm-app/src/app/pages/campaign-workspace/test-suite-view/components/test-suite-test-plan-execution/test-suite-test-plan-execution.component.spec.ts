import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {TestSuiteTestPlanExecutionComponent, tsvItpeTableDefinition} from './test-suite-test-plan-execution.component';
import {TestSuiteState} from '../../state/test-suite.state';
import {
  CampaignPermissions,
  DialogService,
  EntityViewComponentData, GRID_PERSISTENCE_KEY,
  GridService,
  GridTestingModule, GridWithStatePersistence,
  InterWindowCommunicationService,
  InterWindowMessages,
  RestService,
  WorkspaceWithTreeComponent
} from 'sqtm-core';
import {BehaviorSubject, of, Subject} from 'rxjs';
import {mockGlobalConfiguration} from '../../../../../utils/testing-utils/mocks.data';
import {
  mockClosableDialogService,
  mockGridService,
  mockRestService
} from '../../../../../utils/testing-utils/mocks.service';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TSV_ITPE_TABLE, TSV_ITPE_TABLE_CONF} from '../../test-suite-view.constant';
import {TestSuiteViewService} from '../../services/test-suite-view.service';
import {RouterTestingModule} from '@angular/router/testing';


type ComponentData = EntityViewComponentData<TestSuiteState, 'testSuite', CampaignPermissions>;

describe('TestSuiteTestPlanExecutionComponent', () => {
  let component: TestSuiteTestPlanExecutionComponent;
  let fixture: ComponentFixture<TestSuiteTestPlanExecutionComponent>;

  const componentData$ = new BehaviorSubject<ComponentData>({
    globalConfiguration: mockGlobalConfiguration({milestoneFeatureEnabled: true}),
    permissions: {
      canLink: true
    },
    testSuite: {
      id: 2,
      hasDatasets: true,
      uiState: {
        openTestCaseTreePicker: false,
      }
    } as TestSuiteState,
  } as ComponentData);

  let testSuiteViewService = jasmine.createSpyObj('testSuiteViewService', ['toggleTestCaseTreePicker']);
  testSuiteViewService = {...testSuiteViewService, componentData$};

  const gridService = mockGridService();
  const interWindowCommunicationService = {interWindowMessages$: new Subject<InterWindowMessages>()};
  const restService = mockRestService();
  const workspaceWithTree = jasmine.createSpyObj(['requireNodeRefresh']);
  workspaceWithTree.gridService = mockGridService();
  const dialogMock = mockClosableDialogService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TestSuiteTestPlanExecutionComponent],
      imports: [GridTestingModule, AppTestingUtilsModule, HttpClientTestingModule, RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: TSV_ITPE_TABLE_CONF,
          useFactory: tsvItpeTableDefinition()
        },
        {
          provide: TSV_ITPE_TABLE,
          useValue: GridService
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {provide: TestSuiteViewService, useValue: testSuiteViewService},
        {provide: DialogService, useValue: dialogMock.service},
        {provide: InterWindowCommunicationService, useValue: interWindowCommunicationService},
        {provide: RestService, useValue: restService},
        {provide: WorkspaceWithTreeComponent, useValue: workspaceWithTree},
        {
          provide: GRID_PERSISTENCE_KEY,
          useValue: 'execution-plan-grid'
        },
        GridWithStatePersistence,
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestSuiteTestPlanExecutionComponent);
    component = fixture.componentInstance;
    component['gridService'] = gridService;
    gridService.activeFilters$ = of([]);
    fixture.detectChanges();

    gridService.refreshData.calls.reset();
    dialogMock.resetSubjects();
    dialogMock.resetCalls();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
