import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {AttachmentService, DialogConfiguration, DialogReference} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-automated-suite-report-url-dialog',
  templateUrl: './automated-suite-report-url-dialog.component.html',
  styleUrls: ['./automated-suite-report-url-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomatedSuiteReportUrlDialogComponent implements OnInit {

  conf: any;

  constructor(private dialogReference: DialogReference<DialogConfiguration>, private attachmentService: AttachmentService) {
    this.conf = this.dialogReference.data;
  }

  ngOnInit(): void {
  }

  getAttachmentDownloadURL(attachmentListId: number, attachment: any) {
    return this.attachmentService.getAttachmentDownloadURL(attachmentListId, attachment);
  }
}
