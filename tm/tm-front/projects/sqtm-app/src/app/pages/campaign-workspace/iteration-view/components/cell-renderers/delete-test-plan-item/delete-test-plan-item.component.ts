import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DataRow,
  DialogService,
  GridService,
  RestService
} from 'sqtm-core';
import {concatMap, filter, takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {IterationViewService} from '../../../services/iteration-view.service';
import {IterationViewComponentData} from '../../../container/iteration-view/iteration-view.component';

@Component({
  selector: 'sqtm-app-delete-test-plan-item',
  template: `
    <ng-container *ngIf="componentData$ | async as componentData">
      <ng-container *ngIf="row && componentData.milestonesAllowModification">
        <div *ngIf="canDelete(row)"
             class="full-height full-width flex-column icon-container current-workspace-main-color __hover_pointer"
             (click)="removeItem(row)">
          <i nz-icon [nzType]="getIcon(row)" nzTheme="outline" class="table-icon-size"></i>
        </div>
      </ng-container>
    </ng-container>
  `,
  styleUrls: ['./delete-test-plan-item.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
// tslint:disable-next-line:max-line-length
export class DeleteTestPlanItemComponent extends AbstractCellRendererComponent implements OnInit, OnDestroy {

  unsub$ = new Subject<void>();

  componentData$ = new Observable<IterationViewComponentData>();

  constructor(public grid: GridService, cdr: ChangeDetectorRef,
              private dialogService: DialogService,
              private restService: RestService,
              private iterationViewService: IterationViewService) {
    super(grid, cdr);
    this.componentData$ = this.iterationViewService.componentData$;
  }

  ngOnInit() {

  }

  getIcon(row: DataRow): string {
    return this.rowHasExecution(row) ? 'sqtm-core-generic:delete' : 'sqtm-core-generic:unlink';
  }

  removeItem(row: DataRow) {

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.remove-association',
      messageKey: this.rowHasExecution(row) ?
        'sqtm-core.campaign-workspace.dialog.message.remove-association.delete' :
        'sqtm-core.campaign-workspace.dialog.message.remove-association.unbind',
      level: this.rowHasExecution(row) ? 'DANGER' : 'WARNING'
    });

    dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        filter(result => result === true),
        concatMap(() => this.restService.delete([`iteration/${row.data['iterationId']}/test-plan/${[row.data['itemTestPlanId']]}`])),
        concatMap((response) => this.iterationViewService.refreshStateAfterDeletingTestPlanItems([row.data['itemTestPlanId']], response['nbIssues']))
      )
      .subscribe(() => {
        this.grid.refreshData();
      });
  }

  private rowHasExecution(row: DataRow): boolean {
    return row.data['lastExecutedOn'] != null;
  }

  canDelete(row: DataRow): boolean {
    return row.simplePermissions && (this.rowHasExecution(row) ?
      row.simplePermissions.canExtendedDelete :
      row.simplePermissions.canDelete);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export function deleteTestPlanItemColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DeleteTestPlanItemComponent);
}
