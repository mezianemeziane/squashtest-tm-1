import {
  CustomDashboardModel,
  IterationStatisticsBundle,
  Milestone,
  NamedReference,
  SimpleUser,
  SqtmEntityState
} from 'sqtm-core';
import {TestPlanStatistics} from '../../campaign-view/state/campaign.state';


export interface IterationState extends SqtmEntityState {
  name: string;
  reference: string;
  description: string;
  uuid: string;
  iterationStatus: string;
  createdOn: string;
  createdBy: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  testPlanStatistics: TestPlanStatistics;
  actualStartDate: Date;
  actualEndDate: Date;
  actualStartAuto: boolean;
  actualEndAuto: boolean;
  scheduledStartDate: Date;
  scheduledEndDate: Date;
  hasDatasets: boolean;
  executionStatusMap: Map<number, string>;
  uiState: {
    openTestCaseTreePicker: boolean
  };
  nbIssues: number;
  users: SimpleUser[];
  milestones: Milestone[];
  testSuites: NamedReference[];
  iterationStatisticsBundle?: IterationStatisticsBundle;
  nbAutomatedSuites: number;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  dashboard?: CustomDashboardModel;
  nbTestPlanItems: number;
}
