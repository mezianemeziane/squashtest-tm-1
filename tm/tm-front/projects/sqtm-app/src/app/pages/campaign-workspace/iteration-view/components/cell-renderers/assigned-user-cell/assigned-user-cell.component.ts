import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  AbstractListCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  formatFullUserName,
  GridService,
  ListPanelItem,
  RestService,
  SimpleUser,
  TableValueChange
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {Overlay, OverlayRef} from '@angular/cdk/overlay';
import {IterationViewService} from '../../../services/iteration-view.service';
import {IterationViewComponentData} from '../../../container/iteration-view/iteration-view.component';
import {catchError, filter, finalize, map, take, takeUntil} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'sqtm-app-assigned-user-cell',
  templateUrl: './assigned-user-cell.component.html',
  styleUrls: ['./assigned-user-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AssignedUserCellComponent extends AbstractListCellRendererComponent implements OnInit {

  @ViewChild('templatePortalContent', {read: TemplateRef})
  templatePortalContent: TemplateRef<any>;

  @ViewChild('availableUsers', {read: ElementRef})
  availableUsers: ElementRef;

  overlayRef: OverlayRef;

  readonly rowHeight = 29;
  numberOfItemsToDisplay = 8;
  scrollbar: Boolean = false;

  panelItems: ListPanelItem[] = [];

  componentData$: Observable<IterationViewComponentData>;
  canEdit$: Observable<boolean>;

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              public readonly translateService: TranslateService,
              public readonly overlay: Overlay,
              public readonly vcr: ViewContainerRef,
              public readonly restService: RestService,
              public readonly actionErrorDisplayService: ActionErrorDisplayService,
              public iterationViewService: IterationViewService) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);
  }

  get assignedUser(): string {
    return this.row.data[this.columnDisplay.id] || '-';
  }

  ngOnInit(): void {
    this.componentData$ = this.iterationViewService.componentData$.pipe(
      takeUntil(this.unsub$));

    this.canEdit$ = this.componentData$.pipe(
      map(componentData => componentData.permissions.canWrite && componentData.milestonesAllowModification));

    this.componentData$.pipe(
      take(1)
    ).subscribe(componentData => {
      this.panelItems.push({id: 0, label: this.translateService.instant('sqtm-core.campaign-workspace.test-plan.label.user.unassigned')});
      componentData.iteration.users.map(user => this.panelItems.push({id: user.id, label: this.getFullUsername(user)}));
      this.scrollbar = this.panelItems.length > this.numberOfItemsToDisplay;
    });
  }

  showAvailableUsersList() {
    this.canEdit$.pipe(
      take(1),
      filter(canEdit => canEdit)
    ).subscribe(() => {
      this.showList(
        this.availableUsers,
        this.templatePortalContent,
        [
          {originX: 'start', overlayX: 'start', originY: 'bottom', overlayY: 'top', offsetX: -10, offsetY: 6},
          {originX: 'start', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetX: -10, offsetY: -6},
        ]
      );
    });
  }

  get minAvailableItemsHeight(): number {
    const nbItemMin = Math.min(this.panelItems.length, this.numberOfItemsToDisplay);
    return nbItemMin * this.rowHeight;
  }

  getFullUsername(user: SimpleUser): string {
    return formatFullUserName(user) || '-';
  }

  change(userId: number) {
    const userItem = this.panelItems.filter(item => item.id === userId);
    const newUserName = userId === 0 ? '-' : userItem[0].label;
    const itemTestPlanIds = [this.row.data['itemTestPlanId']];

    this.beginAsyncOperation();

    this.iterationViewService.updateAssignedUser(itemTestPlanIds, userId).pipe(
      catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
      finalize(() => this.endAsyncOperation())
    ).subscribe(() => {
      const changes: TableValueChange[] = [
        {columnId: 'user', value: newUserName},
        {columnId: 'userId', value: userId === 0 ? null : userId},
      ];
      this.grid.editRows([this.row.id], changes);
      this.close();
    });
  }
}

export function assignedUserColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(AssignedUserCellComponent)
    .withHeaderPosition('left');
}
