import {Injectable} from '@angular/core';
import {
  CampaignStatisticsBundle,
  createStore, CustomDashboardModel, EntityScope, ExecutionStatusKeys, FavoriteDashboardValue,
  PartyPreferencesService, ProjectData, ProjectReference,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {CampaignMilestoneViewState, initialCampaignMilestoneViewState} from '../state/campaign-milestone-view-state';
import {Observable} from 'rxjs';
import {concatMap, filter, map, take, tap, withLatestFrom} from 'rxjs/operators';

@Injectable()
export class CampaignMilestoneViewService {

  private readonly store = createStore<CampaignMilestoneViewState>(initialCampaignMilestoneViewState());

  public componentData$: Observable<Readonly<CampaignMilestoneViewState>> = this.store.state$.pipe(
    filter(state => Boolean(state.milestone))
  );

  constructor(private restService: RestService,
              private referentialDataService: ReferentialDataService,
              private partyPreferencesService: PartyPreferencesService) {

  }

  init() {
    this.restService.get<MilestoneCampaignDashboard>(['campaign-milestone-dashboard']).pipe(
      withLatestFrom(this.referentialDataService.milestoneModeData$),
      map(([statistics, milestoneModeData]) => {
        const state: CampaignMilestoneViewState = {
          ...statistics,
          milestone: {...milestoneModeData.selectedMilestone},
          generatedDashboardOn: new Date(),
          scope: [],
          disabledExecutionStatus: []
        };
        return state;
      }),
      concatMap((state: CampaignMilestoneViewState) => {
        return this.referentialDataService.findMilestonePerimeter(state.milestone.id).pipe(
          take(1),
          map(projects => ([state, projects]))
        );
      }),
      map(([state, projects]: [CampaignMilestoneViewState, ProjectData[]]) => {
        const allDisabledExecutionStatus: ExecutionStatusKeys[][] = projects.map(project => project.disabledExecutionStatus);

        const milestoneDisabledExecutionStatus: ExecutionStatusKeys[] = allDisabledExecutionStatus.shift()
          .reduce(function(res, executionStatus) {
            if (res.indexOf(executionStatus) === -1 && allDisabledExecutionStatus.every(function(status) {
              return status.indexOf(executionStatus) !== -1;
            })) {
              res.push(executionStatus);
            }
            return res;
          }, []);

        const scope: EntityScope[] = projects.map(project => ({
          id: new ProjectReference(project.id).asString(),
          label: project.name,
          projectId: project.id
        }));
        return {...state, scope, disabledExecutionStatus: milestoneDisabledExecutionStatus};
      }),
    ).subscribe((state: CampaignMilestoneViewState) => this.store.commit(state));
  }

  complete() {
    this.store.complete();
  }

  refreshStatistics() {
    this.restService.get<MilestoneCampaignDashboard>(['campaign-milestone-dashboard']).pipe(
      withLatestFrom(this.store.state$),
      map(([statistics, state]) => {
        const nextState: CampaignMilestoneViewState = {
          ...state,
          ...statistics,
          generatedDashboardOn: new Date()
        };
        return nextState;
      })
    ).subscribe((state: CampaignMilestoneViewState) => this.store.commit(state));
  }

  changeDashboardToDisplay(preferenceValue: FavoriteDashboardValue): Observable<void> {
    return this.partyPreferencesService.changeCampaignWorkspaceFavoriteDashboard(preferenceValue);
  }
}

export class MilestoneCampaignDashboard {
  statistics: CampaignStatisticsBundle;
  dashboard: CustomDashboardModel;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}
