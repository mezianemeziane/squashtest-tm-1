import {CampaignFolderStatisticsBundle, CustomDashboardModel, SqtmEntityState} from 'sqtm-core';

// tslint:disable-next-line:no-empty-interface
export interface CampaignFolderState extends SqtmEntityState {
  name: string;
  description: string;
  nbIssues: number;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  campaignFolderStatisticsBundle?: CampaignFolderStatisticsBundle;
  dashboard?: CustomDashboardModel;
}
