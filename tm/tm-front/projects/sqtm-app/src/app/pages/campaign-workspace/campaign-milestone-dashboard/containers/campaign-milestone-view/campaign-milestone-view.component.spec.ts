import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import { CampaignMilestoneViewComponent } from './campaign-milestone-view.component';
import {CampaignMilestoneViewService} from '../../services/campaign-milestone-view.service';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('CampaignMilestoneViewComponent', () => {
  let component: CampaignMilestoneViewComponent;
  let fixture: ComponentFixture<CampaignMilestoneViewComponent>;

  const serviceMock = jasmine.createSpyObj<CampaignMilestoneViewService>(['init']);

  beforeEach(waitForAsync( () => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), HttpClientTestingModule, AppTestingUtilsModule],
      declarations: [ CampaignMilestoneViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignMilestoneViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
