import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {
  dateTimeColumn,
  executionStatusColumn,
  Extendable,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  indexColumn,
  ReferentialDataService,
  RestService,
  Sort,
  textColumn
} from 'sqtm-core';
import {DatePipe} from '@angular/common';
import {filter, take, takeUntil} from 'rxjs/operators';
import {automatedExecutionDetailsColumn} from '../../../iteration-view/components/cell-renderers/automated-execution-details-renderer/automated-execution-details-renderer.component';
import {automatedExecutionReportColumn} from '../../../iteration-view/components/cell-renderers/automated-execution-report-renderer/automated-execution-report-renderer.component';
import {TS_AUTOMATED_SUITE_TABLE, TS_AUTOMATED_SUITE_TABLE_CONF} from '../../test-suite-view.constant';
import {TestSuiteViewService} from '../../services/test-suite-view.service';
import {TestSuiteViewComponentData} from '../../containers/test-suite-view/test-suite-view.component';
import {automatedSuiteTableDefinition} from '../../../automated-suite-grid-conf';

@Component({
  selector: 'sqtm-app-test-suite-automated-suite',
  templateUrl: './test-suite-automated-suite.component.html',
  styleUrls: ['./test-suite-automated-suite.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: TS_AUTOMATED_SUITE_TABLE_CONF,
      useFactory: automatedSuiteTableDefinition
    },
    {
      provide: TS_AUTOMATED_SUITE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TS_AUTOMATED_SUITE_TABLE_CONF, ReferentialDataService]
    },
    {provide: GridService, useExisting: TS_AUTOMATED_SUITE_TABLE},
  ]
})
export class TestSuiteAutomatedSuiteComponent implements OnInit, AfterViewInit, OnDestroy {

  componentData$: Observable<TestSuiteViewComponentData>;
  unsub$ = new Subject<void>();

  constructor(private testSuiteViewService: TestSuiteViewService, private gridService: GridService) {
  }

  ngOnInit(): void {
    this.componentData$ = this.testSuiteViewService.componentData$;
  }

  ngAfterViewInit(): void {
    this.fetchTestPlan();
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  private fetchTestPlan() {
    this.testSuiteViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        filter((componentData: TestSuiteViewComponentData) => Boolean(componentData.testSuite.id)),
        take(1)
      )
      .subscribe(componentData => {
        this.gridService.setServerUrl([`test-suite/${componentData.testSuite.id}/automated-suite`]);
      });
  }

}
