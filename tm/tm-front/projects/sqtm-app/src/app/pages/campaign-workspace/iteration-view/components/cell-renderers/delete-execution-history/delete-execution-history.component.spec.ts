import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DeleteExecutionHistoryComponent} from './delete-execution-history.component';
import {
  DataRow,
  DialogService,
  GenericDataRow,
  getBasicGridDisplay,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  Limited,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {EMPTY} from 'rxjs';
import {IterationViewService} from '../../../services/iteration-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {GENERIC_TEST_PLAN_VIEW_SERVICE} from '../../../../generic-test-plan-view-service';

describe('DeleteExecutionHistoryComponent', () => {
  let component: DeleteExecutionHistoryComponent;
  let fixture: ComponentFixture<DeleteExecutionHistoryComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  let iterationViewService = jasmine.createSpyObj('iterationViewService', ['load']);
  const genericTestPlanViewService = jasmine.createSpyObj('genericTestPlanViewService', ['getEntityReference']);
  iterationViewService = {...iterationViewService, componentData$: EMPTY};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteExecutionHistoryComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: DialogService,
          useValue: dialogService
        },
        {
          provide: IterationViewService,
          useValue: iterationViewService
        },
        {
          provide: GENERIC_TEST_PLAN_VIEW_SERVICE,
          useValue: genericTestPlanViewService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  it('should create', waitForAsync(() => {
    prepareComponent({
      ...new GenericDataRow(),
      id: 'exe-1',
      data: {}
    });
    expect(component).toBeTruthy();
  }));

  function prepareComponent(row: DataRow) {
    fixture = TestBed.createComponent(DeleteExecutionHistoryComponent);
    component = fixture.componentInstance;
    component.gridDisplay = getBasicGridDisplay();
    component.columnDisplay = {
      id: 'NAME', show: true,
      widthCalculationStrategy: new Limited(200),
      headerPosition: 'left', contentPosition: 'left', showHeader: true,
      viewportName: 'mainViewport'
    };
    component.row = row;
    fixture.detectChanges();
  }
});
