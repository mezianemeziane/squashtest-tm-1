import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {DeleteCampaignTestPlanItemComponent} from './delete-campaign-test-plan-item.component';
import {
  DataRow,
  DialogService,
  GenericDataRow,
  getBasicGridDisplay,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  Limited,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {CampaignViewService} from '../../../service/campaign-view.service';

describe('DeleteCampaignTestPlanItemComponent', () => {
  let component: DeleteCampaignTestPlanItemComponent;
  let fixture: ComponentFixture<DeleteCampaignTestPlanItemComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteCampaignTestPlanItemComponent],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: DialogService,
          useValue: dialogService
        }, {
          provide: CampaignViewService,
          useValue: { ...jasmine.createSpyObj(['load']), componentData$: EMPTY }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteCampaignTestPlanItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', waitForAsync(() => {
    prepareComponent({
      ...new GenericDataRow(),
      id: 'ctpi-1',
      data: {}
    });
    expect(component).toBeTruthy();
  }));

  function prepareComponent(row: DataRow) {
    fixture = TestBed.createComponent(DeleteCampaignTestPlanItemComponent);
    component = fixture.componentInstance;
    component.gridDisplay = getBasicGridDisplay();
    component.columnDisplay = {
      id: 'NAME', show: true,
      widthCalculationStrategy: new Limited(200),
      headerPosition: 'left', contentPosition: 'left', showHeader: true,
      viewportName: 'mainViewport'
    };
    component.row = row;
    fixture.detectChanges();
  }
});
