import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {IterationTestPlanDraggedContentComponent} from './iteration-test-plan-dragged-content.component';
import {DRAG_AND_DROP_DATA, GridService} from 'sqtm-core';
import {mockGridService, mockPassThroughTranslateService} from '../../../../../utils/testing-utils/mocks.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

describe('IterationTestPlanDraggedContentComponent', () => {
  let component: IterationTestPlanDraggedContentComponent;
  let fixture: ComponentFixture<IterationTestPlanDraggedContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [IterationTestPlanDraggedContentComponent],
      providers: [
        {provide: DRAG_AND_DROP_DATA, useValue: {data: {dataRows: []}}},
        {provide: GridService, useValue: mockGridService()},
        {provide: TranslateService, useValue: mockPassThroughTranslateService()},
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IterationTestPlanDraggedContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
