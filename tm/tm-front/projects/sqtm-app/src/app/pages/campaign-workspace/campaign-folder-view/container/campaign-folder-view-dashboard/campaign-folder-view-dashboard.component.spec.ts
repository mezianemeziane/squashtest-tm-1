import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignFolderViewDashboardComponent } from './campaign-folder-view-dashboard.component';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {OverlayModule} from '@angular/cdk/overlay';
import {CampaignFolderViewService} from '../../services/campaign-folder-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('CampaignFolderViewDashboardComponent', () => {
  let component: CampaignFolderViewDashboardComponent;
  let fixture: ComponentFixture<CampaignFolderViewDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot(), OverlayModule],
      providers: [CampaignFolderViewService],
      declarations: [ CampaignFolderViewDashboardComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignFolderViewDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
