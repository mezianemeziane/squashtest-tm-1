import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-project-link-cell',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <ng-container *ngIf="testCaseExist(); else DeletedTestCase">
          <a [routerLink]="getUrl()" class="text-overflow link-cell" nz-tooltip
             (mousedown)="preventDefault($event)"
             [sqtmCoreLabelTooltip]="row.data[columnDisplay.id]"
             [nzTooltipPlacement]="'topLeft'">
            {{projectName}}
          </a>
        </ng-container>
        <ng-template #DeletedTestCase>
          <span
            style="margin: auto 0;"
            class="txt-ellipsis"
            nz-tooltip [sqtmCoreLabelTooltip]="projectName">{{projectName}}</span>
        </ng-template>
      </div>
    </ng-container>`,
  styleUrls: ['./project-link-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectLinkCellComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(grid: GridService, public cdr: ChangeDetectorRef) {
    super(grid, cdr);
  }

  getUrl(): string {
    return '/test-case-workspace/test-case/' + this.row.data['testCaseId'] + '/content';
  }

  get projectName(): string {
    return this.row.data[this.columnDisplay.id] || '-';
  }

  preventDefault($event: MouseEvent) {
    $event.preventDefault();
  }

  ngOnInit(): void {
  }

  testCaseExist() {
    return this.row.data['testCaseId'] !== null;
  }
}

export function withProjectLinkColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ProjectLinkCellComponent);
}
