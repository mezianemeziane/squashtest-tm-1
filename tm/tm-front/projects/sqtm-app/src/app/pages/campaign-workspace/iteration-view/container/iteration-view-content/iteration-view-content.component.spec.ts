import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {IterationViewContentComponent} from './iteration-view-content.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {IterationViewService} from '../../services/iteration-view.service';
import {EMPTY} from 'rxjs';
import {TranslateModule} from '@ngx-translate/core';

describe('IterationViewContentComponent', () => {
  let component: IterationViewContentComponent;
  let fixture: ComponentFixture<IterationViewContentComponent>;

  let iterationViewService = jasmine.createSpyObj('iterationViewService', ['load']);
  iterationViewService = {...iterationViewService, componentData$: EMPTY};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [
        {provide: IterationViewService, useValue: iterationViewService}],
      declarations: [IterationViewContentComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IterationViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
