import {DataRow} from 'sqtm-core';

export interface ItpiMultiEditDialogConfiguration {
  id: string;
  titleKey: string;
  rows: DataRow[];
}
