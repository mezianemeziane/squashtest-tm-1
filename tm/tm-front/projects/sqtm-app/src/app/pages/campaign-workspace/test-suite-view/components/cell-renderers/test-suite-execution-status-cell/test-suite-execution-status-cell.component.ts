import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewContainerRef} from '@angular/core';
import {AbstractExecutionStatusCell} from '../../../../campaign-workspace/components/test-plan/abstract-execution-status-cell';
import {TestSuiteViewComponentData} from '../../../containers/test-suite-view/test-suite-view.component';
import {
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  GridService,
  ListPanelItem,
  ReferentialDataService,
  RestService,
  TableValueChange
} from 'sqtm-core';
import {Overlay} from '@angular/cdk/overlay';
import {TranslateService} from '@ngx-translate/core';
import {TestSuiteViewService} from '../../../services/test-suite-view.service';
import {map, pluck, take, takeUntil} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'sqtm-app-test-suite-execution-status-cell',
  templateUrl: './test-suite-execution-status-cell.component.html',
  styleUrls: ['./test-suite-execution-status-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestSuiteExecutionStatusCellComponent extends AbstractExecutionStatusCell {

  readonly canEdit$: Observable<boolean>;

  readonly panelItems$: Observable<ListPanelItem[]>;

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              public readonly overlay: Overlay,
              public readonly vcr: ViewContainerRef,
              public readonly translateService: TranslateService,
              public readonly restService: RestService,
              public readonly actionErrorDisplayService: ActionErrorDisplayService,
              public referentialDataService: ReferentialDataService,
              public testSuiteViewService: TestSuiteViewService) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService, referentialDataService);

    this.canEdit$ = this.testSuiteViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => this.canEdit(componentData))
    );

    this.panelItems$ = this.testSuiteViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      pluck('projectData'),
      map(projectData => this.getFilteredExecutionStatusKeys(projectData)),
      map((statuses: string[]) => this.asListItemOptions(statuses))
    );
  }

  private canEdit(componentData: TestSuiteViewComponentData): boolean {
    return componentData
      && componentData.permissions.canWrite
      && componentData.milestonesAllowModification;
  }

  change(executionStatusKey: string) {
    this.authenticatedUser$.pipe(
      take(1),
    ).subscribe((authenticatedUser) => {
      this.testSuiteViewService.updateExecutionStatus(executionStatusKey, this.row.data['itemTestPlanId']).subscribe();
      const changes: TableValueChange[] = [
        {columnId: this.columnDisplay.id, value: executionStatusKey},
        {columnId: 'user', value: this.getUser(authenticatedUser)},
        {columnId: 'lastExecutedOn', value: new Date()}
      ];
      this.grid.editRows([this.row.id], changes);
      this.close();
    });
  }
}

export function testSuiteExecutionStatusColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(TestSuiteExecutionStatusCellComponent)
    .withHeaderPosition('center');
}
