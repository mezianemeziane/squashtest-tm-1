import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewContainerRef} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, DialogService, GridService} from 'sqtm-core';
import {AutomatedSuiteExecutionsDialogComponent} from '../../automated-suite-executions-dialog/automated-suite-executions-dialog.component';

@Component({
  selector: 'sqtm-app-automated-execution-details-renderer',
  template: `
    <ng-container *ngIf="row">
      <div class="full-height full-width icon-container">
        <div *ngIf="row.data['hasExecution']; else noExecutions" class=" current-workspace-main-color __hover_pointer"
             (click)="showExecutionHistory()">
          <i nz-icon nzType="sqtm-core-campaign:exec_details" nzTheme="outline" class="table-icon-size"></i>
        </div>
      </div>
      <ng-template #noExecutions>
        <span>/</span>
      </ng-template>
    </ng-container>
  `,
  styleUrls: ['./automated-execution-details-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomatedExecutionDetailsRendererComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(gridService: GridService, cdRef: ChangeDetectorRef,
              private dialogService: DialogService, private vcr: ViewContainerRef) {
    super(gridService, cdRef);
  }

  ngOnInit(): void {
  }

  showExecutionHistory() {
    this.dialogService.openDialog({
      id: 'automated-suite-executions',
      component: AutomatedSuiteExecutionsDialogComponent,
      viewContainerReference: this.vcr,
      data: {suiteId: this.row.data.suiteId},
      height: 600,
      width: 800
    });
  }
}

export function automatedExecutionDetailsColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withHeaderPosition('center')
    .withRenderer(AutomatedExecutionDetailsRendererComponent);
}
