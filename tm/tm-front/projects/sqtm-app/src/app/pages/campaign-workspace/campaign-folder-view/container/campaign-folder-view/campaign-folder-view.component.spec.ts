import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CampaignFolderViewComponent} from './campaign-folder-view.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {WorkspaceWithTreeComponent} from 'sqtm-core';

describe('CampaignFolderViewComponent', () => {
  let component: CampaignFolderViewComponent;
  let fixture: ComponentFixture<CampaignFolderViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      declarations: [CampaignFolderViewComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: WorkspaceWithTreeComponent, useValue: WorkspaceWithTreeComponent}]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignFolderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
