import {ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {CampaignViewService} from '../../service/campaign-view.service';
import {CampaignViewComponentData} from '../campaign-view/campaign-view.component';
import {
  BindableEntity,
  CampaignService,
  createCustomFieldValueDataSelector,
  CustomFieldData,
  DialogService,
  IterationPlanning
} from 'sqtm-core';
import {takeUntil} from 'rxjs/operators';
import {select} from '@ngrx/store';
import {IterationPlanningDialogComponent} from '../../components/dialog/iteration-planning-dialog/iteration-planning-dialog.component';
import {baseIterationPlanningDialogConfiguration} from '../../components/dialog/iteration-planning-dialog/iteration-planning-dialog.configuration';

@Component({
  selector: 'sqtm-app-campaign-view-content',
  templateUrl: './campaign-view-content.component.html',
  styleUrls: ['./campaign-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CampaignViewContentComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();
  componentData$: Observable<CampaignViewComponentData>;

  customFieldData: CustomFieldData[];

  constructor(private campaignViewService: CampaignViewService,
              private campaignService: CampaignService,
              private dialogService: DialogService,
              private vcr: ViewContainerRef) {
    this.componentData$ = this.campaignViewService.componentData$;
  }

  ngOnInit() {
    this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createCustomFieldValueDataSelector(BindableEntity.CAMPAIGN))
    ).subscribe((customFieldData: CustomFieldData[]) => {
      this.customFieldData = customFieldData;
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openPlanningDialog(event, id: number) {
    event.stopPropagation();
    this.campaignService.findIterationsPlanning(id).subscribe(result => {
        const dialogRef = this.dialogService.openDialog({
          ...baseIterationPlanningDialogConfiguration,
          viewContainerReference: this.vcr,
          data: {iterationPlannings: result}
        });

        dialogRef.dialogClosed$.subscribe((plannings: IterationPlanning[]) => {
          if (plannings != null) {
            this.campaignService.setIterationsPlanning(id, plannings).subscribe();
          }
        });
      }
    );
  }
}
