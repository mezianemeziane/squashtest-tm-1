import {Injectable} from '@angular/core';
import {AbstractServerOperationHandler, GridService, GridState, Identifier} from 'sqtm-core';
import {Observable, of} from 'rxjs';
import {CampaignViewService} from './campaign-view.service';
import {distinctUntilChanged, filter, finalize, map, switchMap, take} from 'rxjs/operators';

@Injectable()
export class CampaignTestPlanOperationHandler extends AbstractServerOperationHandler {

  // We're only interested in drag and drop internal reordering
  canCopy$: Observable<boolean> = of(false);
  canPaste$: Observable<boolean> = of(false);
  canDelete$: Observable<boolean> = of(false);
  canCreate$: Observable<boolean> = of(false);
  canDrag$: Observable<boolean>;

  private _grid: GridService;

  public constructor(private readonly campaignViewService: CampaignViewService) {
    super();
  }

  get grid(): GridService {
    return this._grid;
  }

  set grid(gridService: GridService) {
    this._grid = gridService;

    this.canDrag$ = this._grid.isSortedOrFiltered$.pipe(
      map(isSortedOrFiltered => !isSortedOrFiltered),
      distinctUntilChanged());
  }

  allowDropInto(): boolean {
    return false;
  }

  allowDropSibling(): boolean {
    return true;
  }

  copy(): void {
  }

  delete(): void {
  }

  paste(): void {
  }

  notifyInternalDrop(): void {
    this.grid.isSortedOrFiltered$.pipe(
      take(1),
      filter(isSortedOrFiltered => !isSortedOrFiltered),
      switchMap(() => this.grid.gridState$),
      take(1),
    ).subscribe((gridState) => {
      const dragState = gridState.uiState.dragState;
      if (dragState.dragging && dragState.currentDndTarget) {
        this.reorderOptions(gridState);
      } else {
        this.grid.cancelDrag();
      }
    });
  }

  private reorderOptions(gridState: GridState) {
    const filteredList = (gridState.dataRowState.ids as Identifier[])
      .filter((id) => Boolean(id) && !gridState.uiState.dragState.draggedRowIds.includes(id));

    const newPosition = this.findDropPosition(gridState, filteredList);
    const draggedRows = gridState.uiState.dragState.draggedRowIds;

    this.grid.beginAsyncOperation();
    this.doChangePosition(draggedRows, newPosition).pipe(
      finalize(() => {
        this.grid.refreshDataAndKeepSelectedRows();
        this.grid.completeAsyncOperation();
      })
    ).subscribe();
  }

  private findDropPosition(gridState: GridState, filteredList: Identifier[]): number {
    const targetId = gridState.uiState.dragState.currentDndTarget.id as string;
    const zone = gridState.uiState.dragState.currentDndTarget.zone;

    let newPosition = filteredList.indexOf(targetId);

    if (zone === 'bellow') {
      newPosition++;
    }

    return newPosition;
  }

  private doChangePosition(draggedRows: Identifier[], newPosition: number): Observable<any> {
    return this.campaignViewService.changeItemsPosition(draggedRows, newPosition);
  }
}
