import {Injectable} from '@angular/core';
import {
  AddTestCaseResponse,
  AttachmentService,
  CampaignModel,
  CampaignPermissions,
  CampaignService,
  CustomDashboardService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  FavoriteDashboardValue,
  Identifier,
  Milestone,
  PartyPreferencesService,
  ProjectData,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {CampaignViewState, provideInitialView} from '../state/campaign-view.state';
import {CampaignState} from '../state/campaign.state';
import {Observable, of} from 'rxjs';
import {concatMap, map, switchMap, take, tap, withLatestFrom} from 'rxjs/operators';

@Injectable()
export class CampaignViewService extends EntityViewService<CampaignState, 'campaign', CampaignPermissions> {

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected customFieldValueService: CustomFieldValueService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService,
              private campaignService: CampaignService,
              private partyPreferenceService: PartyPreferencesService,
              private customDashboardService: CustomDashboardService) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper
    );
  }

  addSimplePermissions(projectData: ProjectData): CampaignPermissions {
    return new CampaignPermissions(projectData);
  }

  getInitialState(): CampaignViewState {
    return provideInitialView();
  }

  load(id: number) {
    this.restService.get<CampaignModel>(['campaign-view', id.toString()])
      .subscribe((campaignModel: CampaignModel) => {
        const campaign = this.initializeCampaignState(campaignModel);
        this.initializeEntityState(campaign);
      });
  }

  private initializeCampaignState(campaignModel: CampaignModel): CampaignState {
    const attachmentEntityState = this.initializeAttachmentState(campaignModel.attachmentList.attachments);
    const customFieldValueState = this.initializeCustomFieldValueState(campaignModel.customFieldValues);
    return {
      ...campaignModel,
      attachmentList: {id: campaignModel.attachmentList.id, attachments: attachmentEntityState},
      customFieldValues: customFieldValueState,
      uiState: {
        openTestCaseTreePicker: false
      }
    };
  }

  updateCampaignStatus(status: string): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap(state => this.restService.post(['campaign', state.campaign.id.toString(), 'status'], {status: status})),
      withLatestFrom(this.state$),
      map(([, state]: [any, CampaignViewState]) => {
        return {...state, campaign: {...state.campaign, campaignStatus: status}};
      }),
      tap((state: CampaignViewState) => this.store.commit(state)),
      tap(state => this.requireExternalUpdate(state.campaign.id, state))
    );
  }

  unbindMilestone(campaignId: number, milestoneId: number) {
    this.restService.delete<Milestone[]>(['campaign', campaignId.toString(), 'milestone', milestoneId.toString()]).pipe(
      withLatestFrom(this.store.state$),
      map(([milestones, state]: [Milestone[], CampaignViewState]) => {
        return this.unbindMilestoneToCampaign(state, milestoneId);
      })
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  bindMilestone(campaignId: number, milestone: Milestone) {
    this.restService.post(['campaign', campaignId.toString(), 'milestone', milestone.id.toString()]).pipe(
      withLatestFrom(this.store.state$),
      map(([boundMilestone, state]: [Milestone, CampaignViewState]) => {
        return this.bindMilestoneToCampaign(state, milestone);
      })
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  bindMilestoneToCampaign(state: CampaignViewState, milestone: Milestone): CampaignViewState {
    return {
      ...state,
      campaign: {
        ...state.campaign,
        milestones: [milestone]
      }
    };
  }

  unbindMilestoneToCampaign(state: CampaignViewState, milestoneId: number): CampaignViewState {
    return {
      ...state,
      campaign: {
        ...state.campaign,
        milestones: state.campaign.milestones.filter(milestone => milestone.id !== milestoneId)
      }
    };
  }

  updateScheduledStartDate(scheduledStartDate: Date): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: CampaignViewState) => this.campaignService.updateScheduledStartDate(state.campaign.id, scheduledStartDate)),
      withLatestFrom(this.state$),
      map(([, state]: [void, CampaignViewState]) => {
        return {...state, campaign: {...state.campaign, scheduledStartDate}};
      }),
      tap(state => this.store.commit(state))
    );
  }

  updateScheduledEndDate(scheduledEndDate: Date): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: CampaignViewState) => this.campaignService.updateScheduledEndDate(state.campaign.id, scheduledEndDate)),
      withLatestFrom(this.state$),
      map(([, state]: [void, CampaignViewState]) => {
        return {...state, campaign: {...state.campaign, scheduledEndDate}};
      }),
      tap(state => this.store.commit(state))
    );
  }

  updateActualStartDate(actualStartDate: Date): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: CampaignViewState) => this.campaignService.updateActualStartDate(state.campaign.id, actualStartDate)),
      withLatestFrom(this.state$),
      map(([, state]: [void, CampaignViewState]) => {
        return {...state, campaign: {...state.campaign, actualStartDate}};
      }),
      tap(state => this.store.commit(state))
    );
  }

  updateActualEndDate(actualEndDate: Date): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: CampaignViewState) => this.campaignService.updateActualEndDate(state.campaign.id, actualEndDate)),
      withLatestFrom(this.state$),
      map(([, state]: [void, CampaignViewState]) => {
        return {...state, campaign: {...state.campaign, actualEndDate}};
      }),
      tap(state => this.store.commit(state))
    );
  }

  updateActualStartAuto(actualStartAuto: boolean) {
    return this.state$.pipe(
      take(1),
      concatMap((state: CampaignViewState) => this.campaignService.updateActualStartAuto(state.campaign.id, actualStartAuto)),
      withLatestFrom(this.state$),
      map(([date, state]: [Date, CampaignViewState]) => {
        return {...state, campaign: {...state.campaign, actualStartAuto: actualStartAuto, actualStartDate: date}};
      }),
      tap(state => this.store.commit(state))
    );
  }

  updateActualEndAuto(actualEndAuto: boolean) {
    return this.state$.pipe(
      take(1),
      concatMap((state: CampaignViewState) => this.campaignService.updateActualEndAuto(state.campaign.id, actualEndAuto)),
      withLatestFrom(this.state$),
      map(([date, state]: [Date, CampaignViewState]) => {
        return {...state, campaign: {...state.campaign, actualEndAuto: actualEndAuto, actualEndDate: date}};
      }),
      tap(state => this.store.commit(state))
    );
  }

  updateAssignedUser(ctpiId: number[], userId: number) {
    return this.state$.pipe(
      take(1),
      concatMap(() => this.restService.post(
        ['test-plan-item', ctpiId.toString(), 'assign-user-to-ctpi'],
        {assignee: userId}))
    );
  }

  changeItemsPosition(itemsToMove: Identifier[], newPosition: number): Observable<any> {
    return this.state$.pipe(
      take(1),
      switchMap((state: CampaignViewState) => this.campaignService.changeItemsPosition(state.campaign.id, itemsToMove, newPosition)));
  }

  toggleTestCaseTreePicker() {
    this.state$.pipe(
      take(1),
      map((state: CampaignViewState) => this.doToggleTestCaseTreePicker(state))
    ).subscribe(state => this.commit(state));
  }

  private doToggleTestCaseTreePicker(state: CampaignViewState) {
    const pickerState = state.campaign.uiState.openTestCaseTreePicker;
    return {
      ...state,
      campaign: {
        ...state.campaign,
        uiState: {
          ...state.campaign.uiState,
          openTestCaseTreePicker: !pickerState
        }
      }
    };
  }

  addTestCaseIntoTestPlan(testCaseIds: number[]): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: CampaignViewState) => this.campaignService.addTestCase(testCaseIds, state.campaign.id)),
      withLatestFrom(this.store.state$),
      map(([response, state]) => this.updateStateAfterAddingTestCase(response, state)),
      tap(state => this.store.commit(state))
    );
  }

  private updateStateAfterAddingTestCase(response: AddTestCaseResponse, state: CampaignViewState): CampaignViewState {
    const nbTestPlanItems = state.campaign.nbTestPlanItems + response.totalNewTestPlanItemsNumber;
        if (!state.campaign.hasDatasets && response.hasDataSet) {
          return ({...state, campaign: {...state.campaign, hasDatasets: response.hasDataSet, nbTestPlanItems}});
        } else {
          return {...state, campaign: {...state.campaign, nbTestPlanItems}
          };
        }
  }

  refreshNbTestPlanItemsAfterDelete(rowIds: number[]) {
    return this.state$.pipe(
      take(1),
      map((state: CampaignViewState) => {
        const campaign = {...state.campaign, nbTestPlanItems: state.campaign.nbTestPlanItems - rowIds.length};
        return {...state, campaign};
      }),
      tap((state: CampaignViewState) => this.commit(state))
    );
  }

  refreshStats(): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: CampaignViewState) => this.campaignService.getCampaignStatistics(state.campaign.id).pipe(
        map(campaignStatisticsBundle => ({
          ...state,
          campaign: {...state.campaign, campaignStatisticsBundle}
        }))
      )),
      tap(state => this.commit(state))
    );
  }

  changeDashboardToDisplay(preferenceValue: FavoriteDashboardValue) {
    this.partyPreferenceService.changeCampaignWorkspaceFavoriteDashboard(preferenceValue).pipe(
      concatMap(() => {
        if (preferenceValue === 'default') {
          return this.refreshStats();
        } else {
          return this.refreshDashboard();
        }
      }),
      map(state => ({
        ...state,
        campaign: {...state.campaign, shouldShowFavoriteDashboard: preferenceValue === 'dashboard'}
      }))
    ).subscribe(state => this.commit(state));
  }

  refreshDashboard() {
    return this.state$.pipe(
      take(1),
      concatMap((initialState: CampaignViewState) => {
        if (initialState.campaign.canShowFavoriteDashboard) {
          return this.customDashboardService.getDashboardWithDynamicScope(initialState.campaign.favoriteDashboardId, {
            milestoneDashboard: false,
            workspaceName: 'CAMPAIGN', campaignIds: [initialState.campaign.id]
          }).pipe(
            withLatestFrom(this.state$),
            map(([dashboard, state]) => ({
              ...state,
              campaign: {
                ...state.campaign,
                dashboard: {...dashboard},
                generatedDashboardOn: new Date(),
                shouldShowFavoriteDashboard: true
              }
            }))
          );
        } else {
          return of({
            ...initialState,
            campaign: {...initialState.campaign, shouldShowFavoriteDashboard: true}
          });
        }
      })
    );
  }
}


