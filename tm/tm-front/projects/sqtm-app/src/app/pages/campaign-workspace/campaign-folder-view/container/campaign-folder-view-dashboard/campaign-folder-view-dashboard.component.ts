import {Component, OnInit, ChangeDetectionStrategy} from '@angular/core';
import {CampaignFolderViewComponentData} from '../campaign-folder-view/campaign-folder-view.component';
import {Observable} from 'rxjs';
import {
  CustomDashboardBinding,
  CustomDashboardModel,
  EntityRowReference,
  EntityScope, ExecutionStatusCount,
  SquashTmDataRowType
} from 'sqtm-core';
import {CampaignFolderViewService} from '../../services/campaign-folder-view.service';
import {CampaignFolderState} from '../../state/campaign-folder.state';
import {concatMap, take} from 'rxjs/operators';
import * as _ from 'lodash';

@Component({
  selector: 'sqtm-app-campaign-folder-view-dashboard',
  templateUrl: './campaign-folder-view-dashboard.component.html',
  styleUrls: ['./campaign-folder-view-dashboard.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CampaignFolderViewDashboardComponent implements OnInit {

  componentData$: Observable<CampaignFolderViewComponentData>;

  constructor(private campaignFolderViewService: CampaignFolderViewService) {
    this.componentData$ = this.campaignFolderViewService.componentData$;
  }

  ngOnInit(): void {
    this.componentData$.pipe(
      take(1),
      concatMap(() => this.campaignFolderViewService.refreshStats())
    ).subscribe();
  }

  campaignProgressionChartHasErrors(componentData: CampaignFolderViewComponentData) {
    const i18nErrors = componentData.campaignFolder.campaignFolderStatisticsBundle.campaignProgressionStatistics.errors;
    return i18nErrors && i18nErrors.length > 0;
  }


  getScope(campaignFolder: CampaignFolderState): EntityScope[] {
    const ref = new EntityRowReference(campaignFolder.id, SquashTmDataRowType.CampaignFolder).asString();
    return [
      {
        id: ref,
        label: campaignFolder.name,
        projectId: campaignFolder.projectId
      }
    ];
  }

  hasTestPlanItems(executionStatusCount: ExecutionStatusCount) {
    return Object.values(executionStatusCount).reduce(_.add) > 0;
  }

  displayFavoriteDashboard($event: MouseEvent) {
    $event.stopPropagation();
    this.campaignFolderViewService.changeDashboardToDisplay('dashboard');
  }

  displayDefaultDashboard($event: MouseEvent) {
    $event.stopPropagation();
    this.campaignFolderViewService.changeDashboardToDisplay('default');
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings] as CustomDashboardBinding[];
  }

}
