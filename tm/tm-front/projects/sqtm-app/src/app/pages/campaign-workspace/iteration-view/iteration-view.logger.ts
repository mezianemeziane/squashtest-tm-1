import {campaignWorkspaceLogger} from '../campaign-workspace.logger';

export const iterationViewLogger = campaignWorkspaceLogger.compose('iteration-view');
