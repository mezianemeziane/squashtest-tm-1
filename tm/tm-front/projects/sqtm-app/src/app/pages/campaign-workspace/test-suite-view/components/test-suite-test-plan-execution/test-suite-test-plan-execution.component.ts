import {APP_BASE_HREF, DatePipe} from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {Router} from '@angular/router';
import { Dictionary } from '@ngrx/entity/src/models';
import {BehaviorSubject, combineLatest, Observable, Subject} from 'rxjs';
import {
  catchError,
  concatMap,
  delayWhen,
  filter,
  finalize,
  map,
  switchMap,
  take,
  takeUntil,
  tap,
  withLatestFrom
} from 'rxjs/operators';
import {
  ActionErrorDisplayService,
  assigneeFilter,
  AutomatedSuitePreview,
  AutomatedSuiteService,
  buildFilters,
  centredTextColumn,
  DataRow,
  DateFilterComponent,
  DateFilterValueRendererComponent,
  DialogService,
  DRAG_AND_DROP_DATA,
  EntityReference,
  EntityRowReference,
  EntityType,
  executionModeColumn,
  ExecutionStatus,
  executionStatusFilter,
  Extendable,
  FilterBuilder,
  FilterOperation,
  Fixed,
  grid,
  GRID_PERSISTENCE_KEY,
  GridDefinition,
  GridDndData,
  GridFilter, GridFilterUtils,
  GridService,
  gridServiceFactory,
  GridWithStatePersistence,
  i18nEnumResearchFilter,
  indexColumn,
  InterWindowCommunicationService, InterWindowMessages,
  isDndDataFromTestCaseTreePicker,
  milestoneLabelColumn,
  parseDataRowId,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  serverBackedGridTextFilter,
  sortDate,
  SqtmDragEnterEvent,
  SqtmDragLeaveEvent,
  SqtmDropEvent,
  SquashTmDataRowType,
  StyleDefinitionBuilder,
  TEST_CASE_TREE_PICKER_ID,
  testCaseImportanceColumn,
  TestPlanResumeModel,
  TestSuiteService,
  textColumn,
  UserHistorySearchProvider,
  Workspaces,
  WorkspaceWithTreeComponent
} from 'sqtm-core';
import {
  EXECUTION_DIALOG_RUNNER_URL,
  EXECUTION_RUNNER_PROLOGUE_URL
} from '../../../../execution/execution-runner/execution-runner.constant';
import {dataSetColumn} from '../../../campaign-workspace/components/test-plan/dataset-cell-renderer/dataset-cell-renderer.component';
import {withProjectLinkColumn} from '../../../campaign-workspace/components/test-plan/project-link-cell/project-link-cell.component';
import {withTestCaseLinkColumn} from '../../../campaign-workspace/components/test-plan/test-case-link-cell/test-case-link-cell.component';
import {AutomatedTestsExecutionSupervisionDialogComponent} from '../../../iteration-view/components/automated-tests-execution-supervision-dialog/automated-tests-execution-supervision-dialog.component';
import {lastExecutionDateColumn} from '../../../iteration-view/components/cell-renderers/last-execution-date-cell/last-execution-date-cell.component';
import {startExecutionColumn} from '../../../iteration-view/components/cell-renderers/start-execution/start-execution.component';
import {SuccessRateComponent} from '../../../iteration-view/components/cell-renderers/success-rate/success-rate.component';
import {
  itpiLiteralConverter
} from '../../../iteration-view/components/iteration-test-plan-execution/iteration-test-plan-execution.component';
import {ItpiMultiEditDialogConfiguration} from '../../../iteration-view/components/itpi-multi-edit-dialog/itpi-multi-edit-dialog.configuration';
import {TestSuiteViewComponentData} from '../../containers/test-suite-view/test-suite-view.component';
import {TestSuiteTestPlanOperationHandler} from '../../services/test-suite-test-plan-operation-handler';
import {TestSuiteViewService} from '../../services/test-suite-view.service';
import {TestSuiteViewState} from '../../state/test-suite-view.state';
import {TEST_SUITE_TEST_PLAN_DROP_ZONE_ID, TSV_ITPE_TABLE, TSV_ITPE_TABLE_CONF} from '../../test-suite-view.constant';
import {testSuiteViewLogger} from '../../test-suite-view.logger';
import {deleteTestSuiteTestPlanItemColumn} from '../cell-renderers/delete-test-suite-test-plan-item/delete-test-suite-test-plan-item.component';
import {testSuiteAssignedUserColumn} from '../cell-renderers/test-suite-assigned-user-cell/test-suite-assigned-user-cell.component';
import {testSuiteExecutionStatusColumn} from '../cell-renderers/test-suite-execution-status-cell/test-suite-execution-status-cell.component';
import {DeleteTestSuiteTestPlanItemConfiguration} from '../delete-test-suite-test-plan-item-dialog/delete-test-suite-test-plan-item-configuration';
import {DeleteTestSuiteTestPlanItemDialogComponent} from '../delete-test-suite-test-plan-item-dialog/delete-test-suite-test-plan-item-dialog.component';
import {TestSuiteTestPlanDraggedContentComponent} from '../test-suite-test-plan-dragged-content/test-suite-test-plan-dragged-content.component';
import {TestSuiteTpiMultiEditDialogComponent} from '../test-suite-tpi-multi-edit-dialog/test-suite-tpi-multi-edit-dialog.component';
import {TestSuiteAssignableUserProvider} from './test-suite-assignable-user-provider';
import {ExecutionRunnerOpenerService} from '../../../../execution/execution-runner/services/execution-runner-opener.service';

export function tsvItpeTableDefinition(): GridDefinition {
  return grid('test-suite-test-plan')
    .withColumns([
      indexColumn()
        .enableDnd()
        .withViewport('leftViewport'),
      withProjectLinkColumn('projectName')
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1))
        .withAssociatedFilter(),
      milestoneLabelColumn('milestoneLabels')
        .changeWidthCalculationStrategy(new Fixed(180)),
      executionModeColumn('executionMode')
        .withI18nKey('sqtm-core.entity.execution.mode.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(80)),
      textColumn('testCaseReference')
        .withI18nKey('sqtm-core.entity.generic.reference.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(90)),
      withTestCaseLinkColumn('testCaseName')
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Extendable(150, 0.2)),
      testCaseImportanceColumn('importance')
        .withI18nKey('sqtm-core.entity.test-case.importance.label-short-dot')
        .withTitleI18nKey('sqtm-core.entity.test-case.importance.label')
        .isEditable(false)
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(80)),
      dataSetColumn('datasetName', {kind: 'dataset', itemIdType: 'itemTestPlanId'})
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withAssociatedFilter()
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .changeWidthCalculationStrategy(new Fixed(80)),
      testSuiteExecutionStatusColumn('executionStatus')
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(60)),
      centredTextColumn('successRate')
        .withRenderer(SuccessRateComponent)
        .withI18nKey('sqtm-core.entity.execution-plan.success-rate.label.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.success-rate.label.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      testSuiteAssignedUserColumn('user')
        .withI18nKey('sqtm-core.generic.label.user')
        .withAssociatedFilter('login')
        .changeWidthCalculationStrategy(new Extendable(90, 0.2)),
      lastExecutionDateColumn('lastExecutedOn')
        .withSortFunction(sortDate)
        .withI18nKey('sqtm-core.entity.execution-plan.last-execution.label.short-dot')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.last-execution.label.long')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(130)),
      startExecutionColumn('startExecution')
        .withLabel('')
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(50))
        .withViewport(('rightViewport')),
      deleteTestSuiteTestPlanItemColumn('delete')
        .withLabel('')
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(50))
        .withViewport('rightViewport')
    ]).server()
    .withRowConverter(itpiLiteralConverter)
    .disableRightToolBar()
    .withRowHeight(35)
    .withStyle(new StyleDefinitionBuilder()
      .enableInitialLoadAnimation()
      .showLines())
    .enableInternalDrop()
    .enableDrag()
    .withDraggedContentRenderer(TestSuiteTestPlanDraggedContentComponent)
    .build();
}

const logger = testSuiteViewLogger.compose('TestSuiteTestPlanExecutionComponent');

@Component({
  selector: 'sqtm-app-test-suite-test-plan-execution',
  templateUrl: './test-suite-test-plan-execution.component.html',
  styleUrls: ['./test-suite-test-plan-execution.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: TSV_ITPE_TABLE_CONF,
      useFactory: tsvItpeTableDefinition
    },
    TestSuiteTestPlanOperationHandler,
    {
      provide: TSV_ITPE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TSV_ITPE_TABLE_CONF, ReferentialDataService, TestSuiteTestPlanOperationHandler]
    },
    {provide: GridService, useExisting: TSV_ITPE_TABLE},
    {provide: UserHistorySearchProvider, useClass: TestSuiteAssignableUserProvider},
    {
      provide: TestSuiteTestPlanDraggedContentComponent,
      useClass: TestSuiteTestPlanDraggedContentComponent,
      deps: [DRAG_AND_DROP_DATA, TSV_ITPE_TABLE]
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'test-suite-test-plan-grid'
    },
    GridWithStatePersistence
  ]
})
export class TestSuiteTestPlanExecutionComponent implements OnInit, AfterViewInit, OnDestroy {

  dropZoneId = TEST_SUITE_TEST_PLAN_DROP_ZONE_ID;

  testCaseWorkspace = Workspaces['test-case-workspace'];

  @ViewChild('content', {read: ElementRef})
  content: ElementRef;

  launchAutomatedMenuVisible = false;

  unsub$ = new Subject<void>();

  hasSelectedRows$: Observable<boolean>;
  hasSelectedAutomatedRows$: Observable<boolean>;
  activeFilters$: Observable<GridFilter[]>;

  componentData$: Observable<TestSuiteViewComponentData>;

  private areAllItemsReady$: Observable<boolean>;
  private isOneItemReadyOrRunning$: Observable<boolean>;
  private isOneItemAutomated$: Observable<boolean>;

  isLaunchButtonVisible$: Observable<boolean>;
  isReLaunchButtonVisible$: Observable<boolean>;
  isResumeButtonVisible$: Observable<boolean>;
  isLaunchAutomatedButtonVisible$: Observable<boolean>;

  relaunchTestPlanButtonAsync$ = new BehaviorSubject<Boolean>(false);
  resumeTestPlanButtonAsync$ = new BehaviorSubject<Boolean>(false);

  canEditExecPlan$: Observable<boolean>;
  canDeleteInExecPlan$: Observable<boolean>;

  constructor(private gridService: GridService,
              private testSuiteViewService: TestSuiteViewService,
              private renderer: Renderer2,
              private interWindowCommunicationService: InterWindowCommunicationService,
              private dialogService: DialogService,
              private restService: RestService,
              private viewContainerRef: ViewContainerRef,
              private testSuiteService: TestSuiteService,
              private automatedSuiteService: AutomatedSuiteService,
              private workspaceWithTree: WorkspaceWithTreeComponent,
              private actionErrorDisplayService: ActionErrorDisplayService,
              private router: Router,
              private gridWithStatePersistence: GridWithStatePersistence,
              private executionRunnerOpenerService: ExecutionRunnerOpenerService,
              @Inject(APP_BASE_HREF) private baseUrl: string) {
  }

  ngOnInit(): void {
    this.componentData$ = this.testSuiteViewService.componentData$;
    this.initializeCommunicationWithExecutionDialog();
    this.initializeGridFilters();
    this.initializeStateObservables();
    this.enableDragInGridAccordingToPermissions();
  }

  ngAfterViewInit(): void {
    this.gridWithStatePersistence.popGridState().subscribe(() => {
      this.fetchTestPlan();
    });
    this.activeFilters$ = this.gridService.activeFilters$.pipe(
      takeUntil(this.unsub$),
      map((gridFilters: GridFilter[]) => gridFilters.filter(gridFilter => GridFilterUtils.mustIncludeFilter(gridFilter))),
    );
  }

  private enableDragInGridAccordingToPermissions() {
    this.componentData$.pipe(
      take(1)
    ).subscribe((componentData: TestSuiteViewComponentData) => {
      this.gridService.setEnableDrag(componentData.permissions.canLink);
    });
  }

  private initializeStateObservables() {
    this.hasSelectedRows$ = this.gridService.hasSelectedRows$.pipe(
      takeUntil(this.unsub$)
    );

    this.hasSelectedAutomatedRows$ = this.gridService.selectedRows$.pipe(
      takeUntil(this.unsub$),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) =>
        rows.filter((row: DataRow) => row.data['executionMode'] === 'AUTOMATED').length > 0));

    this.areAllItemsReady$ = this.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => Array.from(componentData.testSuite.executionStatusMap.values())
        .every(status => status === ExecutionStatus.READY.id))
    );

    this.isOneItemReadyOrRunning$ = this.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => Array.from(componentData.testSuite.executionStatusMap.values())
        .some(status => status === ExecutionStatus.READY.id || status === ExecutionStatus.RUNNING.id))
    );

    this.isOneItemAutomated$ = this.gridService.dataRows$.pipe(
      takeUntil(this.unsub$),
      map((rows: Dictionary<DataRow>) => Object.values(rows)),
      map(rows => rows.some(row => row.data['executionMode'] === 'AUTOMATED'))
    );

    this.isLaunchButtonVisible$ = this.areAllItemsReady$;
    this.isReLaunchButtonVisible$ = this.areAllItemsReady$.pipe(
      map(areAllItemsReady => !areAllItemsReady));
    this.isResumeButtonVisible$ = this.areAllItemsReady$.pipe(
      map(areAllItemsReady => !areAllItemsReady),
      withLatestFrom(this.isOneItemReadyOrRunning$),
      map(([notAllReady, oneReadyOrRunning]) => notAllReady && oneReadyOrRunning));
    this.isLaunchAutomatedButtonVisible$ = this.isOneItemAutomated$;
    this.canEditExecPlan$ = combineLatest([this.hasSelectedRows$, this.componentData$]).pipe(
      takeUntil(this.unsub$),
      map(([hasSelectedRow, componentData]: [boolean, TestSuiteViewComponentData]) => hasSelectedRow && componentData.permissions.canWrite)
    );
    this.canDeleteInExecPlan$ = combineLatest([this.hasSelectedRows$, this.componentData$]).pipe(
      takeUntil(this.unsub$),
      map(([hasSelectedRow, componentData]: [boolean, TestSuiteViewComponentData]) => hasSelectedRow && componentData.permissions.canLink)
    );
  }


  private initializeGridFilters() {
    this.gridService.addFilters(this.buildGridFilters());
  }

  private initializeCommunicationWithExecutionDialog() {
    this.interWindowCommunicationService.interWindowMessages$.pipe(
      takeUntil(this.unsub$),
      filter((message: InterWindowMessages) => message.isTypeOf('EXECUTION-STEP-CHANGED') || message.isTypeOf('MODIFICATION-DURING-EXECUTION')),
      switchMap(() => this.refreshComponentData()),
    ).subscribe(() => {
      this.gridService.refreshData();
    });
  }

  private fetchTestPlan() {
    this.testSuiteViewService.componentData$
      .pipe(
        takeUntil(this.unsub$),
        filter((componentData: TestSuiteViewComponentData) => Boolean(componentData.testSuite.id)),
        take(1),
        map(componentData => {
          const showMilestones = componentData.globalConfiguration.milestoneFeatureEnabled;

          this.gridService.setColumnVisibility('milestoneLabels', showMilestones);
          this.gridService.setColumnVisibility('hasExecutions', false);

          return componentData;
        })
      )
      .subscribe(componentData => {
        this.gridService.setServerUrl([`test-suite/${componentData.testSuite.id}/test-plan`]);
      });
  }

  private refreshComponentData() {
    return this.componentData$.pipe(
      take(1),
      concatMap((componentData: TestSuiteViewComponentData) => this.testSuiteViewService.load(componentData.testSuite.id)),
    );
  }

  private buildGridFilters(): GridFilter[] {
    return buildFilters(
      [
        serverBackedGridTextFilter('projectName'),
        serverBackedGridTextFilter('testCaseName'),
        serverBackedGridTextFilter('testCaseReference'),
        serverBackedGridTextFilter('testSuites'),
        serverBackedGridTextFilter('datasetName'),
        executionStatusFilter('executionStatus', ResearchColumnPrototype.EXECUTION_STATUS)
          .alwaysActive(),
        i18nEnumResearchFilter('importance', ResearchColumnPrototype.TEST_CASE_IMPORTANCE)
          .alwaysActive(),
        i18nEnumResearchFilter('executionMode', ResearchColumnPrototype.EXECUTION_EXECUTION_MODE)
          .alwaysActive(),
        assigneeFilter('login', ResearchColumnPrototype.ITEM_TEST_PLAN_TESTER)
          .alwaysActive(),
        this.buildExecutionDateFilter()
      ]
    );
  }

  private buildExecutionDateFilter(): FilterBuilder {
    return new FilterBuilder('lastExecutedOn')
      .alwaysActive()
      .withColumnPrototype(ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECON)
      .withAvailableOperations([FilterOperation.BETWEEN])
      .withOperations(FilterOperation.BETWEEN)
      .withInitialValue({kind: 'multiple-date-value', value: []})
      .withWidget(DateFilterComponent)
      .withValueRenderer(DateFilterValueRendererComponent);
  }

  ngOnDestroy(): void {
    this.relaunchTestPlanButtonAsync$.complete();
    this.resumeTestPlanButtonAsync$.complete();
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  showMassDeleteItpiDialog(componentData: TestSuiteViewComponentData) {
    this.gridService.selectedRows$.pipe(
      take(1),
      map((rows: DataRow[]) => this.filterDeletableRows(rows)),
      switchMap((rows: DataRow[]) => this.doOpenMassDeleteDialog(rows, componentData))
    ).subscribe(() => this.gridService.refreshData());
  }

  private doOpenMassDeleteDialog(selectedRows: DataRow[], componentData: TestSuiteViewComponentData)
    : Observable<boolean | { testSuiteId: number, rowIds: number[] }> {

    const itemTestPlanIds = selectedRows.map(row => row.data.itemTestPlanId);
    const hasExecutions = selectedRows.filter(row => row.data.lastExecutedOn != null).length > 0;
    const testSuiteId = componentData.testSuite.id;

    const dialogReference = this.getDialogReference(itemTestPlanIds, hasExecutions, testSuiteId);

    return dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$)
      );
  }

  filterDeletableRows(rows: DataRow[]): DataRow[] {
    if (rows.some(row => !row.simplePermissions.canExtendedDelete)) {
      return rows.filter(row => row.data.lastExecutedOn == null);
    }
    return rows;
  }

  getDialogReference(itemTestPlanIds: number[], hasExecutions: boolean, testSuiteId: number) {
    return this.dialogService.openDialog<DeleteTestSuiteTestPlanItemConfiguration, boolean>({
      id: 'delete-test-suite-tpi',
      component: DeleteTestSuiteTestPlanItemDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        id: 'delete-test-suite-tpi',
        titleKey: 'sqtm-core.campaign-workspace.test-suite.test-plan.title.plural',
        messageKey: hasExecutions ?
          'sqtm-core.campaign-workspace.test-suite.test-plan.message.has-executions.plural' :
          'sqtm-core.campaign-workspace.test-suite.test-plan.message.no-executions.plural',
        level: hasExecutions ? 'DANGER' : 'WARNING',
        testSuiteId,
        itemTestPlanIds
      },
      width: 600
    });
  }

  openMassEditDialog(): void {
    this.gridService.selectedRows$.pipe(
      take(1),
      switchMap((rows: DataRow[]) => this.doOpenMassEditDialog(rows)),
      filter(dialogResult => Boolean(dialogResult))
    ).subscribe(() => {
      this.gridService.refreshData();
      this.refreshTestSuiteNode();
    });
  }

  private doOpenMassEditDialog(rows: DataRow[]): Observable<void | undefined> {
    const dialogReference = this.dialogService.openDialog<ItpiMultiEditDialogConfiguration, void>({
      id: 'itpi-multi-edit',
      component: TestSuiteTpiMultiEditDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        id: 'itpi-multi-edit',
        titleKey: 'sqtm-core.search.generic.modify.selection',
        rows,
      }
    });

    return dialogReference.dialogClosed$;
  }

  private refreshTestSuiteNode(): void {
    this.testSuiteViewService.componentData$.pipe(
      take(1),
      map(({testSuite}) => new EntityRowReference(testSuite.id, SquashTmDataRowType.TestSuite).asString())
    ).subscribe((identifier) => this.workspaceWithTree.requireNodeRefresh([identifier]));
  }

  toggleTestCasePickerDrawer() {
    this.testSuiteViewService.toggleTestCaseTreePicker();
  }

  dropIntoTestPlan($event: SqtmDropEvent) {
    if ($event.dragAndDropData.origin === TEST_CASE_TREE_PICKER_ID) {
      this.dropTestCasesIntoTestPlan($event);
    }
  }

  dragEnter($event: SqtmDragEnterEvent) {
    if (isDndDataFromTestCaseTreePicker($event)) {
      this.markAsDropZone();
    }
  }

  dragLeave($event: SqtmDragLeaveEvent) {
    if (isDndDataFromTestCaseTreePicker($event)) {
      this.unmarkAsDropZone();
    }
  }

  dragCancel() {
    this.unmarkAsDropZone();
  }

  private markAsDropZone() {
    this.renderer.addClass(this.content.nativeElement, 'drop-test-case');
  }

  private unmarkAsDropZone() {
    this.renderer.removeClass(this.content.nativeElement, 'drop-test-case');
  }

  private dropTestCasesIntoTestPlan($event: SqtmDropEvent) {
    const data = $event.dragAndDropData.data as GridDndData;
    if (logger.isDebugEnabled()) {
      logger.debug(`Dropping test cases into test-suite test plan.`, [data]);
    }
    const testCaseIds = data.dataRows.map(row => parseDataRowId(row));
    this.testSuiteViewService.addTestCaseIntoTestPlan(testCaseIds)
      .subscribe((res: TestSuiteViewState) => {
        this.gridService.refreshData();
        this.unmarkAsDropZone();
      });
  }

  resumeExecution() {
    this.componentData$.pipe(
      take(1),
      map((data: TestSuiteViewComponentData) => data.testSuite.id),
      tap(() => this.resumeTestPlanButtonAsync$.next(true)),
      concatMap(id => this.testSuiteService.resume(id)),
      catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      finalize(() => this.resumeTestPlanButtonAsync$.next(false))
    ).subscribe((resume: TestPlanResumeModel) => this.navigateToTestPlanRunner(resume));
  }

  relaunchExecution() {
    this.dialogService.openDeletionConfirm({
      id: 'confirm-delete-all-execution',
      level: 'DANGER',
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.test-suite.mass-delete-execution',
      messageKey: 'sqtm-core.campaign-workspace.dialog.message.test-suite.mass-delete-execution'
    }).dialogClosed$.pipe(
      filter((confirm: boolean) => confirm),
      withLatestFrom(this.componentData$),
      tap(() => this.relaunchTestPlanButtonAsync$.next(true)),
      concatMap(([, componentData]) => this.testSuiteService.relaunch(componentData.testSuite.id)),
      catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      finalize(() => this.relaunchTestPlanButtonAsync$.next(false)),
      map((resume: TestPlanResumeModel) => this.navigateToTestPlanRunner(resume)),
      switchMap(() => this.refreshComponentData()),
    ).subscribe(() => this.gridService.refreshData());
  }

  private navigateToTestPlanRunner(resume: TestPlanResumeModel) {
    let baseUrl = `${this.baseUrl}${EXECUTION_DIALOG_RUNNER_URL}/test-suite/${resume.testSuiteId}/test-plan/${resume.testPlanItemId}/execution/${resume.executionId}`;
    if (resume.initialStepIndex && resume.initialStepIndex > 0) {
      baseUrl += `/step/${resume.initialStepIndex + 1}`;
    } else {
      baseUrl += `/${EXECUTION_RUNNER_PROLOGUE_URL}`;
    }
    const url = this.router.createUrlTree([baseUrl], {queryParams: {hasNextTestCase: resume.hasNextTestCase}}).toString();

    this.executionRunnerOpenerService.openExecutionWithProvidedUrl(url);
  }

  launchAllAutomatedTests() {
    this.componentData$.pipe(
      take(1),
      map(componentData => componentData.testSuite.id),
      delayWhen((testSuiteId: number) => this.automatedSuiteService.updateTaScriptsForTestSuite(testSuiteId)),
      switchMap((testSuiteId: number) => this.automatedSuiteService.generateAutomatedSuitePreview({
        context: new EntityReference(testSuiteId, EntityType.TEST_SUITE),
        testPlanSubsetIds: []
      }))
    ).subscribe((preview: AutomatedSuitePreview) => this.openAutomatedExecutionSupervisionDialog(preview));
  }

  launchSelectedAutomatedTests() {
    this.gridService.selectedRows$.pipe(
      take(1),
      map((rows: DataRow[]) =>
        rows.filter((row: DataRow) => row.data['executionMode'] === 'AUTOMATED')),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) => rows.map(row => row.id)),
      delayWhen((itemIds: number[]) => this.automatedSuiteService.updateTaScriptsForItems(itemIds)),
      withLatestFrom(this.componentData$),
      switchMap(([itemIds, componentData]) => this.automatedSuiteService.generateAutomatedSuitePreview({
        context: new EntityReference(componentData.testSuite.id, EntityType.TEST_SUITE),
        testPlanSubsetIds: itemIds
      }))
    ).subscribe((preview: AutomatedSuitePreview) => this.openAutomatedExecutionSupervisionDialog(preview));

  }

  private openAutomatedExecutionSupervisionDialog(data: AutomatedSuitePreview) {
    const automatedExecutionDialog = this.dialogService.openDialog({
      id: 'automated-tests-execution-supervision',
      viewContainerReference: this.viewContainerRef,
      component: AutomatedTestsExecutionSupervisionDialogComponent,
      data,
      height: 600,
      width: 800
    });
    automatedExecutionDialog.dialogClosed$.pipe(
      filter(result => Boolean(result)),
      switchMap(() => this.testSuiteViewService.incrementAutomatedSuiteCount())
    ).subscribe(() => this.gridService.refreshData());
  }

  openLaunchAutomatedMenu() {
    this.launchAutomatedMenuVisible = true;
  }

  shouldShowResetFilterLink(activeFilters: GridFilter[]): boolean {
    return activeFilters?.length > 0;
  }

  resetFilters() {
    this.gridService.resetFilters();
  }
}

