import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ScmServerInformationPanelComponent } from './scm-server-information-panel.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('ScmServerInformationPanelComponent', () => {
  let component: ScmServerInformationPanelComponent;
  let fixture: ComponentFixture<ScmServerInformationPanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ScmServerInformationPanelComponent ],
      imports: [HttpClientTestingModule, AppTestingUtilsModule],

      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmServerInformationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
