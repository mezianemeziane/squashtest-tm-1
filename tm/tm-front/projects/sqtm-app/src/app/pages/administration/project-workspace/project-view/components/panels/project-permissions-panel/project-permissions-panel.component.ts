import {ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {
  DataRow,
  deleteColumn,
  DialogService,
  Extendable,
  GridDefinition,
  GridService,
  indexColumn,
  RestService,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  textColumn
} from 'sqtm-core';
import {concatMap, filter, finalize, map, take, takeUntil, tap} from 'rxjs/operators';
import {
  buildPermissionProfile,
  permissionsProfileColumn
} from '../../cell-renderers/project-permissions-profile-cell/project-permissions-profile-cell.component';
import {AdminProjectViewComponentData} from '../../../containers/project-view/project-view.component';
import {getProjectViewState, ProjectViewService} from '../../../services/project-view.service';
import {
  buildPermissionType,
  permissionsTypeColumn
} from '../../cell-renderers/project-permissions-type-cell/project-permissions-type-cell.component';
import {AddPermissionDialogComponent} from '../../dialogs/add-permission-dialog/add-permission-dialog.component';
import {createSelector, select} from '@ngrx/store';
import {AdminProjectState} from '../../../state/admin-project-state';
import {DeleteProjectPermissionCellComponent} from '../../cell-renderers/delete-project-permission-cell/delete-project-permission-cell.component';
import {PROJECT_PERMISSIONS_TABLE} from '../../../project-view.constant';
import {UserLicenseInformationDialogService} from '../../../../../services/user-license-information-dialog.service';
import {TranslateService} from '@ngx-translate/core';

export function projectPermissionTableDefinition(translateService: TranslateService): GridDefinition {
  return smallGrid('project-permissions')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      textColumn('partyName')
        .withI18nKey('sqtm-core.administration-workspace.views.project.permissions.user-or-team.singular')
        .changeWidthCalculationStrategy(new Extendable(200, 0.5)),
      permissionsProfileColumn('permissionGroup')
        .withSortFunction(buildPermissionProfile(translateService))
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
      permissionsTypeColumn('team')
        .withSortFunction(buildPermissionType(translateService))
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
      deleteColumn(DeleteProjectPermissionCellComponent)
    ])
    .withInitialSortedColumns([{id: 'partyName', sort: Sort.ASC}])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-project-permissions-panel',
  template: `
    <sqtm-core-grid></sqtm-core-grid>`,
  styleUrls: ['./project-permissions-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: PROJECT_PERMISSIONS_TABLE
    },
    {
      provide: UserLicenseInformationDialogService,
      useClass: UserLicenseInformationDialogService,
    }
  ]
})
export class ProjectPermissionsPanelComponent implements OnInit, OnDestroy {
  private componentData$: Observable<AdminProjectViewComponentData>;

  private unsub$ = new Subject<void>();

  constructor(private projectViewService: ProjectViewService,
              private gridService: GridService,
              private dialogService: DialogService,
              private restService: RestService,
              private vcRef: ViewContainerRef,
              private userLicenseInformationDialogService: UserLicenseInformationDialogService) {
  }

  ngOnInit(): void {
    this.componentData$ = this.projectViewService.componentData$;
    this.initializeTable();
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  refreshGrid() {
    this.gridService.refreshData();
  }

  openAddUsersPermissionDialog() {
    if (!this.userLicenseInformationDialogService.isUserCreationAllowed()) {
      this.userLicenseInformationDialogService.openLicenseDialog();
    } else {
      const dialogRef = this.dialogService.openDialog({
        id: 'add-user-permission-dialog',
        component: AddPermissionDialogComponent,
        data: {
          permissionScope: 'users',
          i18nPartyTypeLabel: 'sqtm-core.administration-workspace.views.project.permissions.user.plural',
          i18nPlaceholder: 'sqtm-core.administration-workspace.views.project.permissions.user.placeholder'
        },
        viewContainerReference: this.vcRef,
        width: 720,
        top: '100px'
      });

      dialogRef.dialogClosed$.subscribe((result) => {
        if (result) {
          this.refreshGrid();
        }
      });
    }
  }

  openAddTeamsPermissionDialog() {
    if (!this.userLicenseInformationDialogService.isUserCreationAllowed()) {
      this.userLicenseInformationDialogService.openLicenseDialog();
    } else {
      const dialogRef = this.dialogService.openDialog({
        id: 'add-team-permission-dialog',
        component: AddPermissionDialogComponent,
        data: {
          permissionScope: 'teams',
          i18nPartyTypeLabel: 'sqtm-core.administration-workspace.views.project.permissions.team.plural',
          i18nPlaceholder: 'sqtm-core.administration-workspace.views.project.permissions.team.placeholder'
        },
        viewContainerReference: this.vcRef,
        width: 720,
        top: '100px'
      });

      dialogRef.dialogClosed$.subscribe((result) => {
        if (result) {
          this.refreshGrid();
        }
      });
    }
  }

  openDeletePermissionsDialog() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((selectedRows: DataRow[]) => selectedRows.length > 0),
      map((selectedRows: DataRow[]) => selectedRows.map(row => row.data.partyId)),
      concatMap((partyIds: number[]) => this.showConfirmDeletePermissionDialog(partyIds)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      concatMap(({partyIds}) => this.projectViewService.removePartyProjectPermissions(partyIds)),
      finalize(() => this.gridService.completeAsyncOperation())
    ).subscribe();
  }

  private showConfirmDeletePermissionDialog(partyIds): Observable<{ confirmDelete: boolean, partyIds: number[] }> {

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.projects.dialog.title.delete-project-permission.delete-many',
      messageKey: 'sqtm-core.administration-workspace.projects.dialog.message.delete-project-permission.delete-many',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, partyIds}))
    );
  }

  private initializeTable() {
    const permissionsTable = this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createSelector(getProjectViewState, (projectState: AdminProjectState) => projectState.partyProjectPermissions)),
    );

    this.gridService.connectToDatasource(permissionsTable, 'partyId');
  }
}
