import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CustomFieldInformationPanelComponent} from './custom-field-information-panel.component';
import {TranslateService} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {DialogService, RestService} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {OverlayModule} from '@angular/cdk/overlay';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {CustomFieldViewService} from '../../../services/custom-field-view.service';
import {of} from 'rxjs';

describe('CustomFieldInformationPanelComponent', () => {
  let component: CustomFieldInformationPanelComponent;
  let fixture: ComponentFixture<CustomFieldInformationPanelComponent>;
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, OverlayModule],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: DialogService,
          userValue: {}
        },
        {
          provide: CustomFieldViewService,
          useValue: {
            componentData$: of({})
          }
        },
      ],
      declarations: [CustomFieldInformationPanelComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomFieldInformationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
