import {Component, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService, ColumnDefinitionBuilder,
  DialogService,
  EditableTextFieldComponent,
  GridService
} from 'sqtm-core';
import {CustomFieldViewService} from '../../../services/custom-field-view.service';
import {catchError, finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-option-code-cell',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column">
        <sqtm-core-editable-text-field #editableTextField style="margin: auto 5px;"
                                       class="sqtm-grid-cell-txt-renderer"
                                       [showPlaceHolder]="false"
                                       [value]="row.data[columnDisplay.id]" [layout]="'no-buttons'"
                                       [size]="'small'"
                                       (confirmEvent)="updateValue($event)"
        ></sqtm-core-editable-text-field>
      </div>
    </ng-container>`,
  styleUrls: ['./option-code-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionCodeCellComponent extends AbstractCellRendererComponent {

  @ViewChild('editableTextField')
  editableTextField: EditableTextFieldComponent;

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              private dialogService: DialogService,
              private customFieldViewService: CustomFieldViewService,
              private actionErrorDisplayService: ActionErrorDisplayService) {
    super(grid, cdRef);
  }

  updateValue(newCode: string) {
    const currentLabel = this.row.data['label'];
    this.customFieldViewService.changeOptionCode(currentLabel, newCode).pipe(
      catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      finalize(() => this.editableTextField.endAsync())
    ).subscribe();
  }
}

export function optionCodeColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(OptionCodeCellComponent);
}
