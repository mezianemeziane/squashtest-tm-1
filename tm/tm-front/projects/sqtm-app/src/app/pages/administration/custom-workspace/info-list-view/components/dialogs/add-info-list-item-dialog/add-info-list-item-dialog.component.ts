import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {DialogReference, FieldValidationError, GridService, isInfoListItemCodePatternValid, RestService} from 'sqtm-core';
import {AbstractAdministrationCreationDialogDirective} from '../../../../../components/abstract-administration-creation-dialog';
import {Observable, of} from 'rxjs';
import {InfoListViewService, NewInfoListItem} from '../../../services/info-list-view.service';
import {finalize, switchMap, take} from 'rxjs/operators';
import {getInfoListIcons} from '../../../../info-list-icons.constant';

@Component({
  selector: 'sqtm-app-add-info-list-item-dialog',
  templateUrl: './add-info-list-item-dialog.component.html',
  styleUrls: ['./add-info-list-item-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddInfoListItemDialogComponent extends AbstractAdministrationCreationDialogDirective
  implements OnInit {


  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  color = '';
  icon: string = null;
  infoListIcons: string[];

  constructor(protected dialogReference: DialogReference,
              protected restService: RestService,
              protected cdr: ChangeDetectorRef,
              private fb: FormBuilder,
              private infoListViewService: InfoListViewService,
              private grid: GridService) {
    super('', dialogReference, restService, cdr);

    this.infoListIcons = getInfoListIcons();
  }

  ngOnInit(): void {
    this.initializeFormGroup();
  }

  addEntity(addAnother?: boolean) {
    if (this.formIsValid()) {
      this.beginAsync();
      this.grid.beginAsyncOperation();
      this.getRequestPayload().pipe(
        take(1),
        switchMap(payload => this.infoListViewService.addItem(payload)),
        finalize(() => this.grid.completeAsyncOperation())
      ).subscribe(result => {
          this.handleCreationSuccess(result, addAnother);
        },
        error => {
          this.handleCreationFailure(error);
        }
      );
    } else {
      this.showClientSideErrors();
    }
  }

  protected doResetForm() {
    this.resetFormControl('label', '');
    this.resetFormControl('code', '');
    this.color = '';
    this.icon = null;
  }

  protected getRequestPayload(): Observable<NewInfoListItem> {
    return of({
      label: this.getFormControlValue('label'),
      code: this.getFormControlValue('code'),
      colour: this.color || '',
      iconName: this.icon || '',
    });
  }

  get textFieldToFocus(): string {
    return 'label';
  }

  changeColor(newColor: string) {
    this.color = newColor;
  }

  changeIcon(newIcon: string) {
    this.icon = newIcon;
  }

  private initializeFormGroup(): void {
    this.formGroup = this.fb.group({
        label: this.fb.control('', [
          Validators.maxLength(100),
          Validators.required,
        ]),
        code: this.fb.control('', [
          Validators.maxLength(30),
          Validators.required,
          this.codeValidator,
        ]),
      }
    );
  }

  get codeValidator(): ValidatorFn {
    return function (formControl: AbstractControl): ValidationErrors {
      const isCodeValid = Boolean(isInfoListItemCodePatternValid(formControl.value));
      return isCodeValid ? null : {invalidCodePattern: true};
    };
  }
}
