import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {AbstractDeleteCellRenderer, DialogService, GridService} from 'sqtm-core';
import {TeamViewService} from '../../../services/team-view.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-remove-team-member-cell',
  template: `
    <sqtm-core-delete-icon
      [iconName]="getIcon()"
      (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
  `,
  styleUrls: ['./remove-team-member-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoveTeamMemberCellComponent extends AbstractDeleteCellRenderer implements OnDestroy {

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              private teamViewService: TeamViewService) {
    super(grid, cdr, dialogService);
  }

  getIcon(): string {
    return 'sqtm-core-generic:unlink';
  }

  protected doDelete() {
    this.grid.beginAsyncOperation();
    const partyId = this.row.data.partyId;
    this.teamViewService.removeTeamMember([partyId]).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe();
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.teams.dialog.title.remove-member.remove-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.teams.dialog.message.remove-member.remove-one';
  }
}
