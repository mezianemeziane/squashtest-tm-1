import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAutomatedSuitesAndExecutionDialogComponent } from './delete-automated-suites-and-execution-dialog.component';
import createSpyObj = jasmine.createSpyObj;
import {AutomationDeletionCount, DialogReference} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {SystemViewService} from '../../../services/system-view.service';
import {TranslateModule} from '@ngx-translate/core';
import {DeleteAutomatedSuitesAndExecutionConfiguration} from './delete-automated-suites-and-execution-configuration';

describe('DeleteAutomatedSuitesAndExecutionDialogComponent', () => {
  let component: DeleteAutomatedSuitesAndExecutionDialogComponent;
  let fixture: ComponentFixture<DeleteAutomatedSuitesAndExecutionDialogComponent>;

  const count: AutomationDeletionCount = {
    oldAutomatedSuiteCount: 6,
    oldAutomatedExecutionCount: 6
  };
  const overlayReference = jasmine.createSpyObj(['attachments']);
  overlayReference.attachments.and.returnValue(EMPTY);

  const dialogReference = new DialogReference<DeleteAutomatedSuitesAndExecutionConfiguration>(
    'delete-automated-suite-and-executions',
    null,
    overlayReference,
    {id: 'delete-automated-suite-and-executions',
      titleKey: 'sqtm-core.administration-workspace.system.cleaning.title.long',
      messageKey: 'sqtm-core.administration-workspace.system.cleaning.dialog.message',
      level: 'DANGER',
      automationDeletionCount: count});
  const systemViewService = jasmine.createSpyObj(['load']);
  systemViewService['componentData$'] = EMPTY;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteAutomatedSuitesAndExecutionDialogComponent ],
      imports: [TranslateModule.forRoot()],
      providers: [
        {provide: DialogReference, useValue: dialogReference},
        {provide: SystemViewService, useValue: systemViewService}
        ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAutomatedSuitesAndExecutionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
