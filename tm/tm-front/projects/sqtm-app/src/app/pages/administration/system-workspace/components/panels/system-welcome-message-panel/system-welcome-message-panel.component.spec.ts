import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {SystemWelcomeMessagePanelComponent} from './system-welcome-message-panel.component';
import {SystemViewService} from '../../../services/system-view.service';
import {of} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('SystemWelcomeMessagePanelComponent', () => {
  let component: SystemWelcomeMessagePanelComponent;
  let fixture: ComponentFixture<SystemWelcomeMessagePanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SystemWelcomeMessagePanelComponent],
      providers: [
        {
          provide: SystemViewService,
          useValue: {
            componentData$: of({welcomeMessage: 'HELLO'}),
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemWelcomeMessagePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
