import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, ViewChild, ViewContainerRef} from '@angular/core';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  DataRow,
  dateColumn,
  deleteColumn,
  DialogConfiguration,
  DialogReference,
  DialogService,
  Extendable,
  FilterOperation,
  Fixed,
  grid,
  GridDefinition,
  GridService,
  GridWithStatePersistence,
  indexColumn,
  MilestoneRange,
  MilestoneStatus,
  RestService,
  selectableTextColumn,
  SwitchFieldComponent,
  textColumn,
  TextResearchFieldComponent,
  WorkspaceWithGridComponent
} from 'sqtm-core';
import {milestoneStatusColumn} from '../../components/cell-renderers/milestone-status-cell-renderer/milestone-status-cell-renderer.component';
import {milestoneRangeColumn} from '../../components/cell-renderers/milestone-range-cell-renderer/milestone-range-cell-renderer.component';
import {Observable, of, Subject} from 'rxjs';
import {concatMap, filter, map, switchMap, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {MilestoneCreationDialogComponent} from '../../components/dialogs/milestone-creation-dialog/milestone-creation-dialog.component';
import {DeleteMilestoneCellRendererComponent} from '../../components/cell-renderers/delete-milestone-cell-renderer/delete-milestone-cell-renderer.component';
import {MilestoneDuplicationDialogComponent} from '../../components/dialogs/milestone-duplication-dialog/milestone-duplication-dialog.component';
import {
  MilestoneSynchronizationDialogComponent,
  MilestoneSynchronizationDialogData
} from '../../components/dialogs/milestone-synchronization-dialog/milestone-synchronization-dialog.component';
import {TranslateService} from '@ngx-translate/core';
import {milestoneOwnerColumn} from '../../components/cell-renderers/milestone-owner-cell-renderer/milestone-owner-cell-renderer.component';
import {MilestoneRowFields} from './milestone-workspace-grid.constants';

const DIALOG_TITLE_KEY_BASE = 'sqtm-core.administration-workspace.milestones.dialog.title.';
const DIALOG_MSG_KEY_BASE = 'sqtm-core.administration-workspace.milestones.dialog.message.';

export function adminMilestonesTableDefinition(): GridDefinition {
  return grid('milestones')
    .withColumns([
      indexColumn()
        .changeWidthCalculationStrategy(new Fixed(60))
        .withViewport('leftViewport'),
      selectableTextColumn(MilestoneRowFields.label)
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      milestoneStatusColumn(MilestoneRowFields.status)
        .withI18nKey('sqtm-core.entity.milestone.status.label')
        .changeWidthCalculationStrategy(new Fixed(160)),
      dateColumn('endDate')
        .withI18nKey('sqtm-core.entity.milestone.end-date.label')
        .changeWidthCalculationStrategy(new Fixed(140)),
      textColumn(MilestoneRowFields.projectCount)
        .withI18nKey('sqtm-core.entity.milestone.project-count.short')
        .withTitleI18nKey('sqtm-core.entity.milestone.project-count.full')
        .changeWidthCalculationStrategy(new Fixed(120)),
      milestoneRangeColumn(MilestoneRowFields.range)
        .withI18nKey('sqtm-core.entity.milestone.range.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      milestoneOwnerColumn('userSortColumn') // the 'userSortColumn' column is only used for server-side sorting
        .withI18nKey('sqtm-core.entity.milestone.owner.label')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      textColumn('description')
        .disableSort()
        .withI18nKey('sqtm-core.entity.milestone.description')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      dateColumn('createdOn')
        .withI18nKey('sqtm-core.entity.generic.created-on.masculine')
        .changeWidthCalculationStrategy(new Fixed(140)),
      textColumn(MilestoneRowFields.createdBy)
        .withI18nKey('sqtm-core.entity.generic.created-by.masculine')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      deleteColumn(DeleteMilestoneCellRendererComponent)
    ])
    .server().withServerUrl(['milestones'])
    .disableRightToolBar()
    .withRowHeight(35)
    .enableMultipleColumnsFiltering([
      MilestoneRowFields.label,
      MilestoneRowFields.ownerLogin,
      MilestoneRowFields.createdBy,
    ])
    .build();
}

@Component({
  selector: 'sqtm-app-milestone-workspace-grid',
  templateUrl: './milestone-workspace-grid.component.html',
  styleUrls: ['./milestone-workspace-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MilestoneWorkspaceGridComponent implements AfterViewInit, OnDestroy {
  @ViewChild(SwitchFieldComponent) enableMilestoneFeatureSwitch: SwitchFieldComponent;

  // i18n partial keys for multiple delete confirm dialog
  private readonly deleteManyTitleBase = `${DIALOG_TITLE_KEY_BASE}delete-many`;
  private readonly deleteManyMsgBase = `${DIALOG_MSG_KEY_BASE}delete-many`;
  private readonly withProjectAndAllRights = 'with-project.all-rights';
  private readonly withProjectAndPartialRights = 'with-project.partial-rights';
  private readonly withoutProjectAndAllRights = 'without-project.all-rights';
  private readonly withoutProjectAndPartialRights = 'without-project.partial-rights';

  private readonly cannotDeleteTitleKey = `${DIALOG_TITLE_KEY_BASE}cannot-delete`;
  private readonly cannotDeleteMessageKey = `${this.deleteManyMsgBase}.cannot-delete`;

  authenticatedUser$: Observable<AuthenticatedUser>;
  milestoneFeatureEnabled$: Observable<boolean>;
  milestoneSwitchVisible$: Observable<boolean>;
  isDeleteButtonActive$: Observable<boolean>;
  isDuplicateButtonActive$: Observable<boolean>;
  isSynchronizeButtonActive$: Observable<boolean>;

  unsub$ = new Subject<void>();

  protected readonly entityIdPositionInUrl = 2;

  @ViewChild(TextResearchFieldComponent)
  searchField: TextResearchFieldComponent;

  constructor(public gridService: GridService,
              private restService: RestService,
              private dialogService: DialogService,
              public adminReferentialDataService: AdminReferentialDataService,
              private viewContainerRef: ViewContainerRef,
              public workspaceWithGrid: WorkspaceWithGridComponent,
              public translateService: TranslateService,
              private gridWithStatePersistence: GridWithStatePersistence) {
    this.workspaceWithGrid.entityIdPositionInUrl = this.entityIdPositionInUrl;

    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
    this.milestoneFeatureEnabled$ = adminReferentialDataService.milestoneFeatureEnabled$;
    this.milestoneSwitchVisible$ = this.milestoneFeatureEnabled$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(adminReferentialDataService.loggedAsAdmin$),
      map(([milestoneFeatureEnabled, isAdmin]) => isAdmin && !milestoneFeatureEnabled)
    );
    this.checkIfDeleteButtonIsActive();
    this.checkIfDuplicateButtonIsActive();
    this.checkIfSynchronizeButtonIsActive();
  }


  ngAfterViewInit(): void {
    this.initializeGridFilters();
    this.gridWithStatePersistence.popGridState().subscribe((snapshot) => {
      GridWithStatePersistence.updateMultipleColumnSearchField(snapshot, this.searchField);
      this.gridService.refreshData();
    });

    this.milestoneFeatureEnabled$.pipe(takeUntil(this.unsub$))
      .subscribe((enabled) => {
        if (this.enableMilestoneFeatureSwitch != null) {
          this.enableMilestoneFeatureSwitch.value = enabled;
        }
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private checkIfDeleteButtonIsActive() {
    this.isDeleteButtonActive$ = this.gridService.hasSelectedRows$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.milestoneFeatureEnabled$),
      map(([hasSelectedRows, milestoneFeatureEnabled]) => milestoneFeatureEnabled && hasSelectedRows),
    );
  }

  private checkIfDuplicateButtonIsActive() {
    this.isDuplicateButtonActive$ = this.gridService.selectedRowIds$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.milestoneFeatureEnabled$),
      map(([selectedRowIds, milestoneFeatureEnabled]: [number[], boolean]) => milestoneFeatureEnabled && selectedRowIds.length === 1),
    );
  }

  private checkIfSynchronizeButtonIsActive() {
    this.isSynchronizeButtonActive$ = this.gridService.hasSelectedRows$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.milestoneFeatureEnabled$),
      map(([hasSelectedRows, milestoneFeatureEnabled]) => milestoneFeatureEnabled && hasSelectedRows),
    );
  }

  public initializeGridFilters(): void {
    this.addFilters();
  }

  private addFilters() {
    this.gridService.addFilters([
      {
        id: MilestoneRowFields.label,
        active: false,
        initialValue: {kind: 'single-string-value', value: ''},
        tiedToPerimeter: false,
        operation: FilterOperation.LIKE
      },
      {
        id: MilestoneRowFields.ownerLogin,
        active: false,
        initialValue: {kind: 'single-string-value', value: ''},
        tiedToPerimeter: false,
        operation: FilterOperation.LIKE
      },
      {
        id: MilestoneRowFields.createdBy,
        active: false,
        initialValue: {kind: 'single-string-value', value: ''},
        tiedToPerimeter: false,
        operation: FilterOperation.LIKE
      }
    ]);
  }

  openCreateDialog() {
    const dialogReference = this.dialogService.openDialog({
      component: MilestoneCreationDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        titleKey: DIALOG_TITLE_KEY_BASE + 'new-milestone',
      },
      id: 'milestone-dialog',
      width: 600
    });

    dialogReference.dialogResultChanged$.pipe(
      takeUntil(dialogReference.dialogClosed$),
      filter(confirm => Boolean(confirm))
    ).subscribe(() => {
      this.gridService.refreshData();
    });
  }

  openDeleteDialog($event: MouseEvent, user: AuthenticatedUser) {
    $event.stopPropagation();

    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0)
    ).subscribe((rows) => {
      const {titleKey, messageKey} = this.getTranslationKeysForMultipleDelete(user, rows);

      if (this.canDeleteSelection(user, rows)) {
        const dialogReference = this.dialogService.openDeletionConfirm({
          titleKey,
          messageKey,
          level: 'DANGER',
        });

        const pathVariable = this.getRowsWithDeletePermission(user, rows).map(row => row.data[MilestoneRowFields.milestoneId]).join(',');

        dialogReference.dialogClosed$.pipe(
          takeUntil(this.unsub$),
          filter(result => Boolean(result)),
          concatMap(() => this.restService.delete([`milestones`, pathVariable]))
        )
          .subscribe(() => this.gridService.refreshData());

      } else {
        this.dialogService.openAlert({
          level: 'INFO',
          messageKey,
          titleKey,
        });
      }
    });
  }

  canDeleteSelection(user: AuthenticatedUser, rows: DataRow[]): boolean {
    return user.admin || rows.map(row => row.data[MilestoneRowFields.ownerId]).includes(user.userId);
  }

  getRowsWithDeletePermission(user: AuthenticatedUser, rows: DataRow[]): DataRow[] {
    if (user.admin) {
      return rows;
    }

    return rows.filter(row => row.data[MilestoneRowFields.ownerId] === user.userId);
  }

  getTranslationKeysForMultipleDelete(user: AuthenticatedUser, rows: DataRow[]): { titleKey: string, messageKey: string } {
    const withProjects = rows.map(row => row.data[MilestoneRowFields.projectCount] > 0).includes(true);

    if (user.admin) {
      return {
        messageKey: `${this.deleteManyMsgBase}.${withProjects ? this.withProjectAndAllRights : this.withoutProjectAndAllRights}`,
        titleKey: this.deleteManyTitleBase,
      };
    } else {
      const rowsWithDeletePermission = this.getRowsWithDeletePermission(user, rows);
      const onlyNonOwnedMilestoneSelected = rowsWithDeletePermission.length === 0;
      const nonOwnedMilestoneSelected = rowsWithDeletePermission.length !== rows.length;

      if (onlyNonOwnedMilestoneSelected) {
        return {
          messageKey: this.cannotDeleteMessageKey,
          titleKey: this.cannotDeleteTitleKey,
        };
      } else if (nonOwnedMilestoneSelected) {
        return {
          messageKey: `${this.deleteManyMsgBase}.${withProjects ? this.withProjectAndPartialRights : this.withoutProjectAndPartialRights}`,
          titleKey: this.deleteManyTitleBase,
        };
      } else {
        return {
          messageKey: `${this.deleteManyMsgBase}.${withProjects ? this.withProjectAndAllRights : this.withoutProjectAndAllRights}`,
          titleKey: this.deleteManyTitleBase,
        };
      }
    }
  }

  handleMilestoneFeatureToggle(): void {
    this.milestoneFeatureEnabled$.pipe(
      take(1),
      switchMap((currentlyEnabled) => this.showConfirmToggleMilestoneFeatureDialog(!currentlyEnabled)),
      filter((confirm) => Boolean(confirm)),
      switchMap(() => this.toggleMilestoneFeature()),
    ).subscribe();
  }

  private showConfirmToggleMilestoneFeatureDialog(enable: boolean): Observable<boolean> {
    let dialogReference: DialogReference;

    if (enable) {
      const dialogId = 'confirm-enable-milestone-feature';
      dialogReference = this.dialogService.openConfirm({
        id: dialogId,
        titleKey: DIALOG_TITLE_KEY_BASE + dialogId,
        messageKey: DIALOG_MSG_KEY_BASE + dialogId,
        level: 'INFO',
      });
    } else {
      const dialogId = 'confirm-disable-milestone-feature';
      dialogReference = this.dialogService.openDeletionConfirm({
        id: dialogId,
        titleKey: DIALOG_TITLE_KEY_BASE + dialogId,
        messageKey: DIALOG_MSG_KEY_BASE + dialogId,
        level: 'DANGER',
      });
    }

    return dialogReference.dialogClosed$.pipe(
      tap((confirm) => {
        if (!confirm) {
          this.enableMilestoneFeatureSwitch.endAsyncMode();
        }
      }),
    );
  }

  private toggleMilestoneFeature(): Observable<any> {
    return this.milestoneFeatureEnabled$.pipe(
      take(1),
      switchMap((enabled) => this.adminReferentialDataService.setMilestoneFeatureEnabled(!enabled)),
      tap(() => this.gridService.refreshData()),
    );
  }

  duplicateMilestone() {
    this.gridService.selectedRows$.pipe(
      take(1),
    ).subscribe((rows: DataRow[]) => {
      if (rows[0].data[MilestoneRowFields.status] === MilestoneStatus.PLANNED.id) {
        this.openMilestoneStatusDoesNotAllowDuplicationAlert();
      } else {
        const milestoneId = Number(rows[0].id);
        this.openMilestoneDuplicationDialog(milestoneId);
      }
    });
  }

  private openMilestoneStatusDoesNotAllowDuplicationAlert(): void {
    const dialogId = 'milestone-status-does-not-allow-duplication';

    this.dialogService.openAlert({
      id: dialogId,
      titleKey: DIALOG_TITLE_KEY_BASE + 'duplicate-milestone',
      messageKey: DIALOG_MSG_KEY_BASE + dialogId,
      level: 'INFO',
    });
  }

  private openMilestoneDuplicationDialog(milestoneId: number) {
    const dialogReference = this.dialogService.openDialog({
      component: MilestoneDuplicationDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        milestoneId: milestoneId
      },
      id: 'duplicate-milestone-dialog',
      width: 600
    });

    dialogReference.dialogClosed$.pipe(
      filter(confirm => Boolean(confirm))
    ).subscribe(() => {
      this.gridService.refreshData();
    });
  }

  openSynchronizeMilestoneDialog() {
    this.gridService.selectedRows$.pipe(
      take(1),
      switchMap((selectedRows) => this.checkSelectionForSynchronization(selectedRows)),
      filter(([, isSelectionValid]) => Boolean(isSelectionValid)),
      switchMap(([selectedRows]) => this.doOpenSynchronizeMilestoneDialog(selectedRows)),
    ).subscribe(() => this.gridService.refreshData());
  }

  private doOpenSynchronizeMilestoneDialog(milestoneRows: DataRow[]): Observable<any> {
    return this.adminReferentialDataService.authenticatedUser$.pipe(
      take(1),
      switchMap((authenticatedUser: AuthenticatedUser) => {
        // Admin prevails (SQUASH-2740)
        const isProjectManager = authenticatedUser.projectManager && !authenticatedUser.admin;
        const dialogId = 'synchronize-milestones-dialog';
        const dialogConf: DialogConfiguration<MilestoneSynchronizationDialogData> = {
          id: dialogId,
          component: MilestoneSynchronizationDialogComponent,
          width: 800,
          data: {
            dialogId,
            titleKey: DIALOG_TITLE_KEY_BASE + 'synchronize-milestones',
            milestoneA: {
              id: milestoneRows[0].data[MilestoneRowFields.milestoneId],
              label: milestoneRows[0].data[MilestoneRowFields.label],
              locked: milestoneRows[0].data[MilestoneRowFields.status] === MilestoneStatus.LOCKED.id,
              owned: milestoneRows[0].data[MilestoneRowFields.ownerId] === authenticatedUser.userId,
              global: milestoneRows[0].data[MilestoneRowFields.range] === MilestoneRange.GLOBAL.id,
            },
            milestoneB: {
              id: milestoneRows[1].data[MilestoneRowFields.milestoneId],
              label: milestoneRows[1].data[MilestoneRowFields.label],
              locked: milestoneRows[1].data[MilestoneRowFields.status] === MilestoneStatus.LOCKED.id,
              owned: milestoneRows[1].data[MilestoneRowFields.ownerId] === authenticatedUser.userId,
              global: milestoneRows[1].data[MilestoneRowFields.range] === MilestoneRange.GLOBAL.id,
            },
            isProjectManager: isProjectManager,
          },
          viewContainerReference: this.viewContainerRef,
        };
        const dialogReference = this.dialogService.openDialog(dialogConf);
        return dialogReference.dialogClosed$;
      }));
  }

  private checkSelectionForSynchronization(selectedRows: DataRow[]): Observable<[DataRow[], boolean]> {
    // We use this weird stream with ternary conditions in order to bypass subsequent checks whenever
    // a check in the chain returned false. This ensures that there won't be more than one alert window
    // shown at the same time.
    return this.checkSelectionLengthForSynchronization(selectedRows.length).pipe(
      switchMap((checkLength) => checkLength ?
        this.checkSelectionStatusForSynchronization(selectedRows) : of(false)),
      switchMap((checkStatus) => checkStatus ?
        this.checkSelectionRangeForSynchronization(selectedRows) : of(false)),
      map(checksResult => [selectedRows, checksResult]));
  }

  private checkSelectionLengthForSynchronization(numSelected: number): Observable<boolean> {
    if (numSelected < 2) {
      this.openTooFewSelectedMilestonesAlert();
    } else if (numSelected > 2) {
      this.openTooManySelectedMilestonesAlert();
    }

    return of(numSelected === 2);
  }

  private openTooFewSelectedMilestonesAlert(): void {
    this.openSynchronizationInfoDialog('too-few-selected-milestones');
  }

  private openTooManySelectedMilestonesAlert(): void {
    this.openSynchronizationInfoDialog('too-many-selected-milestones');
  }

  private checkSelectionRangeForSynchronization(selectedMilestones: DataRow[]): Observable<boolean> {
    return this.adminReferentialDataService.authenticatedUser$.pipe(
      take(1),
      map((authenticatedUser) => {
        if (authenticatedUser.admin) {
          return true;
        } else {
          const selectionContainsOnlyGlobalMilestones = selectedMilestones
            .filter(dataRow => dataRow.data[MilestoneRowFields.range] !== MilestoneRange.GLOBAL.id)
            .length === 0;

          if (selectionContainsOnlyGlobalMilestones) {
            this.openOnlyGlobalMilestonesSelectedAlert();
            return false;
          } else {
            return true;
          }
        }
      }),
    );
  }

  private openOnlyGlobalMilestonesSelectedAlert(): void {
    this.openSynchronizationInfoDialog('selection-has-only-global-milestones');
  }

  private checkSelectionStatusForSynchronization(selectedMilestones: DataRow[]): Observable<boolean> {
    return this.adminReferentialDataService.authenticatedUser$.pipe(
      take(1),
      map((authenticatedUser) => {
        const selectionContainsNoPlannedMilestones = selectedMilestones
          .filter(dataRow => dataRow.data[MilestoneRowFields.status] === MilestoneStatus.PLANNED.id)
          .length === 0;

        if (authenticatedUser.admin) {
          const selectionContainsInProgressOrFinishedMilestones = selectedMilestones
            .filter(dataRow => dataRow.data[MilestoneRowFields.status] === MilestoneStatus.IN_PROGRESS.id
              || dataRow.data[MilestoneRowFields.status] === MilestoneStatus.FINISHED.id)
            .length > 0;

          return selectionContainsNoPlannedMilestones
            && selectionContainsInProgressOrFinishedMilestones;
        } else {
          const restrictedMilestones = selectedMilestones
            .filter(dataRow => dataRow.data[MilestoneRowFields.range] === MilestoneRange.RESTRICTED.id);
          const restrictedInProgressOrFinishedMilestones = restrictedMilestones
            .filter(dataRow => dataRow.data[MilestoneRowFields.status] === MilestoneStatus.IN_PROGRESS.id
              || dataRow.data[MilestoneRowFields.status] === MilestoneStatus.FINISHED.id);

          const selectionContainsRestrictedInProgressOrFinishedMilestones =
            restrictedMilestones.length === 0 || restrictedInProgressOrFinishedMilestones.length > 0;

          return selectionContainsNoPlannedMilestones
            && selectionContainsRestrictedInProgressOrFinishedMilestones;
        }
      }),
      tap((canSynchronize) => {
        if (!canSynchronize) {
          this.openWrongStatusForSynchronizationAlert();
        }
      })
    );
  }

  private openWrongStatusForSynchronizationAlert(): void {
    this.openSynchronizationInfoDialog('selected-milestone-status-forbid-synchronization');
  }

  private openSynchronizationInfoDialog(dialogId: string) {
    this.dialogService.openAlert({
      id: dialogId,
      titleKey: DIALOG_TITLE_KEY_BASE + 'synchronize-milestones',
      messageKey: DIALOG_MSG_KEY_BASE + 'synchronization.' + dialogId,
      level: 'INFO',
    });
  }


  filterMilestones($event: any) {
    this.gridService.applyMultiColumnsFilter($event);
  }
}
