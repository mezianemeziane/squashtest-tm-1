import {Component, ChangeDetectionStrategy, Input} from '@angular/core';
import {AdminInfoListViewComponentData} from '../../../containers/info-list-view/info-list-view.component';

@Component({
  selector: 'sqtm-app-info-list-information-panel',
  templateUrl: './info-list-information-panel.component.html',
  styleUrls: ['./info-list-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfoListInformationPanelComponent {

  @Input()
  componentData: AdminInfoListViewComponentData;

  constructor() {
  }

}
