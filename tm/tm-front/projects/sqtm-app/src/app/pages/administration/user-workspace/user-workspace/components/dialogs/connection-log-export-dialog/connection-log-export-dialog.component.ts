import {ChangeDetectionStrategy, Component, ElementRef, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DialogReference, FilterValueModel, RestService, TextFieldComponent} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {format} from 'date-fns';

@Component({
  selector: 'sqtm-app-connection-log-export-dialog',
  templateUrl: './connection-log-export-dialog.component.html',
  styleUrls: ['./connection-log-export-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConnectionLogExportDialogComponent {

  static DATE_FORMAT = 'yyyyMMdd_HHmmss';

  static readonly DIALOG_ID = 'export-connection-log';

  readonly formGroup: FormGroup;

  readonly data: ConnectionLogExportDialogData;

  @ViewChildren(TextFieldComponent)
  textFields: QueryList<TextFieldComponent>;

  @ViewChild('link')
  linkElement: ElementRef;

  get dialogId(): string {
    return ConnectionLogExportDialogComponent.DIALOG_ID;
  }

  constructor(public readonly formBuilder: FormBuilder,
              public readonly restService: RestService,
              public readonly dialogReference: DialogReference<ConnectionLogExportDialogData>,
              public readonly translateService: TranslateService) {
    this.formGroup = formBuilder.group({
      fileName: this.formBuilder.control(this.defaultFileName, Validators.required),
    });

    this.data = dialogReference.data;
  }

  get defaultFileName(): string {
    const prefixKey = 'sqtm-core.administration-workspace.users.connection-logs.dialog.export.file-name-prefix';
    const date = format(new Date(), ConnectionLogExportDialogComponent.DATE_FORMAT);
    return `${this.translateService.instant(prefixKey)}_${date}`;
  }

  export(): void {
    if (! this.formGroup.valid) {
      this.showErrors();
      return;
    }

    const body = {
      fileName: this.formGroup.controls.fileName.value,
      filterValues: this.data.filterValues,
    };

    this.restService.post<{export: string}>(['users/connection-logs/export'], body)
      .subscribe((result) => {
        downloadAsCSV(result.export, this.formGroup.controls.fileName.value);
        this.dialogReference.close();
      });
  }

  private showErrors(): void {
    if (this.textFields) {
      this.textFields.forEach((tf) => tf.showClientSideError());
    }
  }
}

export interface ConnectionLogExportDialogData {
  filterValues: FilterValueModel[];
}

function downloadAsCSV(content: string, fileName: string): void {
  const csvContent = 'data:text/csv;charset=utf-8,' + content;
  const encodedUri = encodeURI(csvContent);
  const link = document.createElement('a');
  link.setAttribute('href', encodedUri);
  link.setAttribute('download', fileName + '.csv');
  document.body.appendChild(link);
  link.click();
  link.remove();
}
