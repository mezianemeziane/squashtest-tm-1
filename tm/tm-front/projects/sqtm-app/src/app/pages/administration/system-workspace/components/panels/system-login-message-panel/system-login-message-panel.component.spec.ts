import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {SystemLoginMessagePanelComponent} from './system-login-message-panel.component';
import {SystemViewService} from '../../../services/system-view.service';
import {of} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('SystemLoginMessagePanelComponent', () => {
  let component: SystemLoginMessagePanelComponent;
  let fixture: ComponentFixture<SystemLoginMessagePanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SystemLoginMessagePanelComponent],
      providers: [
        {
          provide: SystemViewService,
          useValue: {
            componentData$: of({loginMessage: 'HELLO'}),
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemLoginMessagePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
