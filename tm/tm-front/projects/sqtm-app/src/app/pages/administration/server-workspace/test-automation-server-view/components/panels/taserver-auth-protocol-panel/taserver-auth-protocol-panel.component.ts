import {ChangeDetectionStrategy, Component, Input, ViewChild} from '@angular/core';
import {AdminTestAutomationServerViewComponentData} from '../../../containers/test-automation-server-view/test-automation-server-view.component';
import {AuthConfiguration, AuthenticationProtocol} from 'sqtm-core';
import {TestAutomationServerViewService} from '../../../services/test-automation-server-view.service';
import {AuthProtocolFormComponent} from '../../../../components/auth-protocol-form/auth-protocol-form.component';

@Component({
  selector: 'sqtm-app-taserver-auth-protocol-panel',
  templateUrl: './taserver-auth-protocol-panel.component.html',
  styleUrls: ['./taserver-auth-protocol-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TAServerAuthProtocolPanelComponent {

  @Input()
  componentData: AdminTestAutomationServerViewComponentData;

  @ViewChild(AuthProtocolFormComponent)
  authProtocolForm: AuthProtocolFormComponent;

  // authConfiguration is only used for OAuth protocol. No TAServer use OAuth credentials so far
  authConfiguration: AuthConfiguration = null;

  constructor(public readonly taServerViewService: TestAutomationServerViewService) {
  }

  handleProtocolChange(protocol: AuthenticationProtocol): void {
    this.taServerViewService.setAuthenticationProtocol(protocol)
      .subscribe(() => this.authProtocolForm.authProtocol = protocol);
  }
}
