import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {SystemViewState} from '../../states/system-view.state';
import {SystemViewService} from '../../services/system-view.service';
import {AdminReferentialDataService, AuthenticatedUser, GenericEntityViewService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-system-workspace',
  templateUrl: './system-workspace.component.html',
  styleUrls: ['./system-workspace.component.less'],
  providers: [
    {
      provide: SystemViewService,
      useClass: SystemViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: SystemViewService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemWorkspaceComponent implements OnInit, OnDestroy {
  componentData$: Observable<SystemViewState>;
  workspaceName = 'administration-workspace';
  titleKey = 'sqtm-core.administration-workspace.system.title';
  authenticatedUser$: Observable<AuthenticatedUser>;

  constructor(private readonly systemViewService: SystemViewService,
              public readonly adminReferentialDataService: AdminReferentialDataService) {
    adminReferentialDataService.refresh().subscribe();
    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
  }

  ngOnInit(): void {
    this.systemViewService.load();
  }

  ngOnDestroy(): void {
    // this.systemViewService.complete();
  }

}
