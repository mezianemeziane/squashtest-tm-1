import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InfoListViewComponent } from './info-list-view.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {EMPTY, of} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogService, GridService} from 'sqtm-core';
import {InfoListViewService} from '../../services/info-list-view.service';

describe('InfoListViewComponent', () => {
  let component: InfoListViewComponent;
  let fixture: ComponentFixture<InfoListViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, AppTestingUtilsModule],
      declarations: [ InfoListViewComponent ],
      providers: [
      {
        provide: TranslateService,
        useValue: jasmine.createSpyObj(['instant']),
      },
      {
        provide: ActivatedRoute,
        useValue: {
          params: of({id: 123}),
          paramMap: of(convertToParamMap({
            toto: 'tutu',
          })),
        }
      },
      {
        provide: DialogService,
        useValue: jasmine.createSpyObj(['openDeletionConfirm', 'openAlert'])
      },
      {
        provide: GridService,
        useValue: jasmine.createSpyObj(['refreshData'])
      },
       {
          provide: InfoListViewService,
          useValue: {
            componentData$: EMPTY
          }
       },
    ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
