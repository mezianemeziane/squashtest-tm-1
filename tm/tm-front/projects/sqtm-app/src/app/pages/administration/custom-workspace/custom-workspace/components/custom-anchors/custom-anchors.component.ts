import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {WorkspaceWithGridComponent} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-custom-anchors',
  templateUrl: './custom-anchors.component.html',
  styleUrls: ['./custom-anchors.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomAnchorsComponent implements OnInit {

  constructor(private workspaceWithGrid: WorkspaceWithGridComponent) {
  }

  ngOnInit(): void {
  }

  hideContextualContent() {
    this.workspaceWithGrid.switchToNoRowLayout();
  }
}
