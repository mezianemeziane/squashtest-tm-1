import {Component, OnInit, ChangeDetectionStrategy, OnDestroy} from '@angular/core';
import {ADMIN_WS_USER_TABLE, ADMIN_WS_USER_TABLE_CONFIG} from '../../../user-workspace.constant';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  GRID_PERSISTENCE_KEY,
  GridService,
  gridServiceFactory,
  GridWithStatePersistence,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {adminUserTableDefinition} from '../user-grid/user-grid.component';
import {Observable} from 'rxjs';

@Component({
  selector: 'sqtm-app-user-workspace',
  templateUrl: './user-workspace.component.html',
  styleUrls: ['./user-workspace.component.less'],
  providers: [
    {
      provide: ADMIN_WS_USER_TABLE_CONFIG,
      useFactory: adminUserTableDefinition,
      deps: []
    },
    {
      provide: ADMIN_WS_USER_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ADMIN_WS_USER_TABLE_CONFIG, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: ADMIN_WS_USER_TABLE
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'user-workspace-main-grid'
    },
    GridWithStatePersistence
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserWorkspaceComponent implements OnInit, OnDestroy {

  authenticatedAdmin$: Observable<AuthenticatedUser>;

  constructor(public readonly adminReferentialDataService: AdminReferentialDataService,
              private gridService: GridService) {
  }

  ngOnInit(): void {
    this.authenticatedAdmin$ = this.adminReferentialDataService.authenticatedUser$;
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }
}
