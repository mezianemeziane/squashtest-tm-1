import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestAutomationServerGridComponent} from './test-automation-server-grid.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DataRow, DialogModule, DialogService, GridService, GridTestingModule, RestService, WorkspaceWithGridComponent} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {adminMilestonesTableDefinition} from '../../../../milestone-workspace/milestone-workspace/containers/milestone-workspace-grid/milestone-workspace-grid.component';
import {ADMIN_WS_TEST_AUTOMATION_SERVER_TABLE, ADMIN_WS_TEST_AUTOMATION_SERVER_TABLE_CONFIG} from '../../../server-workspace.constant';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {mockClosableDialogService, mockGridService, mockRestService, mockRouter} from '../../../../../../utils/testing-utils/mocks.service';
import {of} from 'rxjs';
import {Router} from '@angular/router';

describe('TestAutomationServerGridComponent', () => {
  let component: TestAutomationServerGridComponent;
  let fixture: ComponentFixture<TestAutomationServerGridComponent>;

  const gridService = mockGridService();
  const dialogMock = mockClosableDialogService();
  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, GridTestingModule, DialogModule],
      declarations: [TestAutomationServerGridComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ADMIN_WS_TEST_AUTOMATION_SERVER_TABLE_CONFIG,
          useFactory: adminMilestonesTableDefinition,
        },
        {
          provide: ADMIN_WS_TEST_AUTOMATION_SERVER_TABLE,
          useValue: gridService,
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: DialogService,
          useValue: dialogMock.service,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: Router,
          useValue: mockRouter(),
        },
        {
          provide: WorkspaceWithGridComponent,
          useValue: {},
        },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestAutomationServerGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    dialogMock.resetCalls();
    dialogMock.resetSubjects();
    restService.delete.calls.reset();
    gridService.refreshData.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open creation dialog', () => {
    component.openCreationDialog();

    dialogMock.closeDialogsWithResult(true);

    expect(dialogMock.service.openDialog).toHaveBeenCalled();
    expect(gridService.refreshData).toHaveBeenCalled();
  });

  it('should delete servers', () => {
    gridService.selectedRows$ = of([{data: {executionCount: 1}} as unknown as DataRow]);

    component.deleteTestAutomationServers();

    dialogMock.closeDialogsWithResult(true);

    expect(dialogMock.service.openDeletionConfirm).toHaveBeenCalled();
    expect(gridService.refreshData).toHaveBeenCalled();
    expect(restService.delete).toHaveBeenCalled();
  });
});
