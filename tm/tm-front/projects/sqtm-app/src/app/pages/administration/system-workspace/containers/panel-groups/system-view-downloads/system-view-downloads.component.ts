import {ChangeDetectionStrategy, Component, OnDestroy} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {SystemViewState} from '../../../states/system-view.state';
import {SystemViewService} from '../../../services/system-view.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-system-view-downloads',
  templateUrl: './system-view-downloads.component.html',
  styleUrls: ['./system-view-downloads.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemViewDownloadsComponent implements OnDestroy {
  componentData$: Observable<SystemViewState>;

  unsub$ = new Subject<void>();

  constructor(public readonly systemViewService: SystemViewService) {
    this.componentData$ = this.systemViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
