import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractDeleteCellRenderer, DialogService, GridService} from 'sqtm-core';
import {Subject} from 'rxjs';
import {UserViewService} from '../../../services/user-view.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-remove-user-authorisation-cell',
  template: `
    <sqtm-core-delete-icon
      [iconName]="getIcon()"
      (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
  `,
  styleUrls: ['./remove-user-authorisation-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoveUserAuthorisationCellComponent extends AbstractDeleteCellRenderer {

  unsub$ = new Subject<void>();

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              dialogService: DialogService,
              private userViewService: UserViewService) {
    super(grid, cdr, dialogService);
  }

  getIcon(): string {
    return 'sqtm-core-generic:unlink';
  }

  protected doDelete() {
    this.grid.beginAsyncOperation();
    const projectId = this.row.data.projectId;
    this.userViewService.removeAuthorisation([projectId]).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe();
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.users.dialog.title.remove-authorisation.remove-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.users.dialog.message.remove-authorisation.remove-one';
  }
}
