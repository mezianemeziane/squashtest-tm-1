import {Component, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, DataRow, GridService, RestService} from 'sqtm-core';
import {tap, withLatestFrom} from 'rxjs/operators';
import {Dictionary} from '@ngrx/entity';


@Component({
  selector: 'sqtm-app-default-requirements-links-cell-renderer',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column radio-button-container">
            <input type="radio" [checked]="isDefault" (click)="select()">
      </div>
    </ng-container>`,
  styleUrls: ['./default-requirements-links.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DefaultRequirementsLinksComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef, public restService: RestService) {
    super(grid, cdRef);
  }

  get isDefault(): boolean {
    const isDefault = this.row.data[this.columnDisplay.id];
    return Boolean(isDefault);
  }


   select() {
    this.restService.post(['requirement-link-type', this.row.data['id'], 'default']).pipe(
    withLatestFrom(this.grid.dataRows$),
      tap(([, rows]: [void, Dictionary<DataRow>]) => {
        const dataRows = Object.values(rows);
        const defaultRow = dataRows.find(row => Boolean(row.data['default']) === true);
        this.grid.editRows([defaultRow.id], [{columnId: 'default', value: false}]);
        this.grid.editRows([this.row.id], [{columnId: 'default', value: true}]);
      })
      ).subscribe();
   }
}

export function defaultRequirementsLinksColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DefaultRequirementsLinksComponent);
}
