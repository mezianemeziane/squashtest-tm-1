import {TestBed} from '@angular/core/testing';

import {AddJobDialogService} from './add-job-dialog.service';
import {RestService, TestAutomationProject} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ProjectViewService} from './project-view.service';
import {of, throwError} from 'rxjs';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {take} from 'rxjs/operators';
import {mockRestService} from '../../../../../utils/testing-utils/mocks.service';

describe('AddJobDialogService', () => {
  const restService = mockRestService();

  let service: AddJobDialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: ProjectViewService,
          useClass: ProjectViewService
        }
      ]
    });

    service = TestBed.inject(AddJobDialogService);
  });

  it('should be created in "loading" state', async (done) => {
    // when
    expect(service).toBeTruthy();

    // then
    service.dataState$.subscribe(dataState => {
      expect(dataState).toBe('loading');
      done();
    });
  });

  it('should load project jobs', async (done) => {
    // given
    restService.get.and.returnValue(of({
      taProjects: taProjectsTestData()
    }));

    // when
    service.load(1);

    // then
    service.dataState$.subscribe(dataState => {
      expect(dataState).toBe('ready');

      service.taProjects$.subscribe((taProjects) => {
        expect(taProjects.length).toBe(2);
        done();
      });
    });
  });

  it('should handle loading errors', async (done) => {
    // given
    restService.get.and.returnValue(throwError({
      squashTMError: {
        error: 'boom!',
      }
    }));

    // when
    service.load(1);

    // then
    service.dataState$.subscribe(dataState => {
      expect(dataState).toBe('error');
      done();
    });
  });


  it('should unload jobs', async (done) => {
    // given
    restService.get.and.returnValue(of({
      taProjects: taProjectsTestData()
    }));

    service.load(1);

    // when
    service.unload();

    // then
    service.dataState$.subscribe(dataState => {
      expect(dataState).toBe('loading');

      service.taProjects$.subscribe((taProjects) => {
        expect(taProjects.length).toBe(0);
        done();
      });
    });
  });

  it('should set a job label', async (done) => {
    // given
    restService.get.and.returnValue(of({
      taProjects: taProjectsTestData()
    }));

    service.load(1);

    // when
    service.setJobLabel('remoteName2', 'label3');

    // then
    service.taProjects$.subscribe((taProjects) => {
      expect(taProjects.find(job => job.remoteName === 'remoteName2')?.label).toBe('label3');
      done();
    });
  });

  it('should set a unique job that can run BDD', async (done) => {
    // given
    restService.get.and.returnValue(of({
      taProjects: taProjectsTestData()
    }));

    service.load(1);

    // when
    service.setCanRunBdd('remoteName1', true);
    service.setCanRunBdd('remoteName2', true);

    // then
    service.state$.pipe(take(1)).subscribe((state) => {
      const firstJob = state.taProjects.find(job => job.remoteName === 'remoteName1');
      const secondJob = state.taProjects.find(job => job.remoteName === 'remoteName2');
      expect(firstJob.canRunBdd).toBeFalsy();
      expect(secondJob.canRunBdd).toBeTruthy();
      done();
    });
  });

  it('should toggle off a job that can run BDD', async (done) => {
    // given
    restService.get.and.returnValue(of({
      taProjects: taProjectsTestData()
    }));

    service.load(1);

    // when
    service.setCanRunBdd('remoteName1', true);
    service.setCanRunBdd('remoteName1', false);

    // then
    service.state$.pipe(take(1)).subscribe((state) => {
      const firstJob = state.taProjects.find(job => job.remoteName === 'remoteName1');
      expect(firstJob.canRunBdd).toBeFalsy();
      done();
    });
  });
});

function taProjectsTestData(): TestAutomationProject[] {
  return [
    {
      taProjectId: 1,
      tmProjectId: 1,
      remoteName: 'remoteName1',
      label: 'label1',
      serverId: 1,
      executionEnvironments: 'executionEnvironments1',
      canRunBdd: false,
    },
    {
      taProjectId: 2,
      tmProjectId: 1,
      remoteName: 'remoteName2',
      label: 'label2',
      serverId: 1,
      executionEnvironments: 'executionEnvironments2',
      canRunBdd: false,
    }
  ];
}
