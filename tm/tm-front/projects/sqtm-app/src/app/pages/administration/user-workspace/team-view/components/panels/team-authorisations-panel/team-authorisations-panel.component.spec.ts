import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {
  TEAM_AUTHORISATIONS_TABLE,
  TEAM_AUTHORISATIONS_TABLE_CONF,
  TeamAuthorisationsPanelComponent,
  teamAuthorisationsTableDefinition
} from './team-authorisations-panel.component';
import {DataRow, DialogService, GridService, RestService} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TeamViewService} from '../../../services/team-view.service';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {EMPTY, of} from 'rxjs';
import {mockAutoConfirmDialogService, mockGridService, mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';
import createSpyObj = jasmine.createSpyObj;

describe('TeamAuthorisationsPanelComponent', () => {
  let component: TeamAuthorisationsPanelComponent;
  let fixture: ComponentFixture<TeamAuthorisationsPanelComponent>;

  const restService = mockRestService();
  const gridService = mockGridService();
  const dialogService = mockAutoConfirmDialogService();
  const teamViewService = createSpyObj(['removeAuthorisation']);
  teamViewService.componentData$ = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      declarations: [TeamAuthorisationsPanelComponent],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: TeamViewService,
          useValue: teamViewService,
        },
        {
          provide: DialogService,
          useValue: {},
        },
        {
          provide: TEAM_AUTHORISATIONS_TABLE_CONF,
          useFactory: teamAuthorisationsTableDefinition,
        },
        {
          provide: TEAM_AUTHORISATIONS_TABLE,
          useValue: gridService,
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: DialogService,
          useValue: dialogService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamAuthorisationsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    gridService.refreshData.calls.reset();
    gridService.applyMultiColumnsFilter.calls.reset();
    dialogService.openDialog.calls.reset();
    teamViewService.removeAuthorisation.calls.reset();
    teamViewService.removeAuthorisation.and.returnValue(of({}));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('open dialog and refresh grid when confirmed', () => {
    component.openAddTeamAuthorisationDialog();

    expect(dialogService.openDialog).toHaveBeenCalled();
    expect(gridService.refreshData).toHaveBeenCalled();
  });

  it('remove selected permissions', () => {
    gridService.selectedRows$ = of([{data: {projectId: 123}} as unknown as DataRow]);
    component.removeAuthorisations();

    expect(dialogService.openDeletionConfirm).toHaveBeenCalled();
    expect(teamViewService.removeAuthorisation).toHaveBeenCalledWith([123]);
  });

  it('should filter in table with search input', () => {
    component.handleResearchInput('blabla');
    expect(gridService.applyMultiColumnsFilter).toHaveBeenCalledWith('blabla');
  });
});
