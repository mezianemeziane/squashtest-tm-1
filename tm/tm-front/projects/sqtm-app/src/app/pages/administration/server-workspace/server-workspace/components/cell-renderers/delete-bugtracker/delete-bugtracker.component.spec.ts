import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DeleteBugtrackerComponent} from './delete-bugtracker.component';
import {
  DialogService,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {OverlayModule} from '@angular/cdk/overlay';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';

describe('DeleteBugtrackerCellRendererComponent', () => {
  let component: DeleteBugtrackerComponent;
  let fixture: ComponentFixture<DeleteBugtrackerComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteBugtrackerComponent],
      imports: [AppTestingUtilsModule, OverlayModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: DialogService,
          userValue: {}
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteBugtrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
