import {Injectable} from '@angular/core';
import {
  AdminBugTrackerState,
  AttachmentService,
  AuthConfiguration,
  AuthenticationPolicy,
  AuthenticationProtocol,
  Credentials,
  EntityViewAttachmentHelperService,
  GenericEntityViewService,
  isBasicAuthCredentials,
  isOAuth1aCredentials,
  isTokenAuthCredentials,
  RestService
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {AdminBugTrackerViewState, provideInitialAdminBugTrackerView} from '../states/admin-bug-tracker-view-state';
import {Observable} from 'rxjs';
import {map, switchMap, take, tap, withLatestFrom} from 'rxjs/operators';

@Injectable()
export class BugTrackerViewService extends GenericEntityViewService<AdminBugTrackerState, 'bugTracker'> {
  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
  ) {
    super(
      restService,
      attachmentService,
      translateService,
      attachmentHelper
    );
  }

  public getInitialState(): AdminBugTrackerViewState {
    return provideInitialAdminBugTrackerView();
  }

  protected getRootUrl(initialState?): string {
    return 'bugtracker';
  }

  load(bugTrackerId: number) {
    return this.restService.get<AdminBugTrackerState>(['bugtracker-view', bugTrackerId.toString()])
      .subscribe((bugtracker) => this.initializeBugTracker(bugtracker));
  }

  private initializeBugTracker(bugTracker: AdminBugTrackerState): void {
    const btState: AdminBugTrackerState = {
      ...bugTracker,
      attachmentList: {id: null, attachments: null},
    };
    this.initializeEntityState(btState);
  }

  setAuthenticationConfiguration(authConf: AuthConfiguration): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((data: AdminBugTrackerViewState) => this.restService.post(
        [this.getRootUrl(), data.bugTracker.id.toString(), 'auth-protocol', 'configuration'], authConf)),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminBugTrackerViewState]) => ({
        ...state,
        bugTracker: {
          ...state.bugTracker,
          authConfiguration: authConf
        }
      })),
      tap((nextState: AdminBugTrackerViewState) => this.store.commit(nextState))
    );
  }

  setAuthPolicy(authPolicy: AuthenticationPolicy): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((data: AdminBugTrackerViewState) => this.restService.post(
        [this.getRootUrl(), data.bugTracker.id.toString(), 'auth-policy'], {authPolicy})),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminBugTrackerViewState]) => ({
        ...state,
        bugTracker: {
          ...state.bugTracker,
          authPolicy: authPolicy
        }
      })),
      tap((nextState: AdminBugTrackerViewState) => this.store.commit(nextState))
    );
  }

  setCredentials(credentials: Credentials): Observable<any> {
    if (isOAuth1aCredentials(credentials)) {
      return this.setOAuthCredentials(credentials.token, credentials.tokenSecret);
    } else if (isBasicAuthCredentials(credentials)) {
      return this.setBasicAuthCredentials(credentials.username, credentials.password);
    } else if (isTokenAuthCredentials(credentials)) {
      return this.setTokenAuthCredentials(credentials.token);
    }
  }

  setBasicAuthCredentials(username: string, password: string): Observable<any> {
    return this.doSetCredentials({
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      type: AuthenticationProtocol.BASIC_AUTH,
      username,
      password,
    });
  }

  setOAuthCredentials(token: string, tokenSecret: string): any {
    return this.doSetCredentials({
      implementedProtocol: AuthenticationProtocol.OAUTH_1A,
      type: AuthenticationProtocol.OAUTH_1A,
      token,
      tokenSecret,
    });
  }

  setTokenAuthCredentials(token: string): any {
    return this.doSetCredentials({
      implementedProtocol: AuthenticationProtocol.TOKEN_AUTH,
      type: AuthenticationProtocol.TOKEN_AUTH,
      token,
    });
  }

  private doSetCredentials(credentials: Credentials): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((data: AdminBugTrackerViewState) => this.restService.post(
        [this.getRootUrl(), data.bugTracker.id.toString(), 'credentials'], credentials)),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminBugTrackerViewState]) => {
        credentials.registered = true;
        return {
          ...state,
          bugTracker: {
            ...state.bugTracker,
            credentials,
          }
        };
      }),
      tap((nextState: AdminBugTrackerViewState) => this.store.commit(nextState))
    );
  }

  setAuthenticationProtocol(authProtocol: AuthenticationProtocol): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((data: AdminBugTrackerViewState) => this.restService.post(
        [this.getRootUrl(), data.bugTracker.id.toString(), 'auth-protocol'], {authProtocol})),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminBugTrackerViewState]) => ({
        ...state,
        bugTracker: {
          ...state.bugTracker,
          authenticationProtocol: authProtocol
        }
      })),
      tap((nextState: AdminBugTrackerViewState) => this.store.commit(nextState)),
      tap((state) => this.requireExternalUpdate(state.bugTracker.id, 'authProtocol', authProtocol))
    );
  }
}
