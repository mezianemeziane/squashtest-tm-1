import {Component, OnInit, ChangeDetectionStrategy, OnDestroy} from '@angular/core';
import {ADMIN_WS_TEAM_TABLE, ADMIN_WS_TEAM_TABLE_CONFIG} from '../../../user-workspace.constant';
import {
  AdminReferentialDataService,
  AuthenticatedUser, GRID_PERSISTENCE_KEY,
  GridService,
  gridServiceFactory, GridWithStatePersistence,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {adminTeamTableDefinition} from '../team-grid/team-grid.component';
import {Observable} from 'rxjs';

@Component({
  selector: 'sqtm-app-team-workspace',
  templateUrl: './team-workspace.component.html',
  styleUrls: ['./team-workspace.component.less'],  providers: [
    {
      provide: ADMIN_WS_TEAM_TABLE_CONFIG,
      useFactory: adminTeamTableDefinition,
      deps: []
    },
    {
      provide: ADMIN_WS_TEAM_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ADMIN_WS_TEAM_TABLE_CONFIG, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: ADMIN_WS_TEAM_TABLE
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'team-workspace-main-grid'
    },
    GridWithStatePersistence
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TeamWorkspaceComponent implements OnInit, OnDestroy  {

  authenticatedAdmin$: Observable<AuthenticatedUser>;

  constructor(public readonly adminReferentialDataService: AdminReferentialDataService,
              private gridService: GridService) {
  }

  ngOnInit(): void {
    this.authenticatedAdmin$ = this.adminReferentialDataService.authenticatedUser$;
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }
}
