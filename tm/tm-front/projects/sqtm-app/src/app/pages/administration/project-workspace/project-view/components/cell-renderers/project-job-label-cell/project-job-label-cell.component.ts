import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  DialogService,
  EditableTextFieldComponent,
  GridService,
  TableValueChange
} from 'sqtm-core';
import {ProjectViewService} from '../../../services/project-view.service';
import {catchError, finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-project-job-label-cell',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column">
        <sqtm-core-editable-text-field #editableTextField style="margin: auto 5px;"
                                       class="sqtm-grid-cell-txt-renderer"
                                       [showPlaceHolder]="false"
                                       [value]="row.data[columnDisplay.id]" [layout]="'no-buttons'"
                                       [size]="'small'"
                                       (confirmEvent)="updateValue($event)"
        ></sqtm-core-editable-text-field>
      </div>
    </ng-container>`,
  styleUrls: ['./project-job-label-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectJobLabelCellComponent extends AbstractCellRendererComponent implements OnInit {

  @ViewChild('editableTextField')
  editableTextField: EditableTextFieldComponent;

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              public projectViewService: ProjectViewService,
              public dialogService: DialogService,
              private actionErrorDisplayService: ActionErrorDisplayService) {
    super(grid, cdRef);
  }

  ngOnInit(): void {
  }

  updateValue(value: string) {
    this.projectViewService.setJobLabel(this.row.data.taProjectId, value).pipe(
      catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      finalize(() => this.editableTextField.endAsync())
    ).subscribe(() => {
      const tableValueChange: TableValueChange = {columnId: this.columnDisplay.id, value};
      this.grid.editRows([this.row.id], [tableValueChange]);
    });
  }
}

export function projectJobLabelColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ProjectJobLabelCellComponent);
}

