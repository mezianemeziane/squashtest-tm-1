import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AdminCustomFieldViewState} from '../../../states/admin-custom-field-view-state';
import {CustomFieldViewService} from '../../../services/custom-field-view.service';
import {
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {
  CustomFieldOptionsPanelComponent,
  OPTIONS_TABLE,
  OPTIONS_TABLE_CONF, optionsTableDefinition
} from '../../../components/panels/custom-field-options-panel/custom-field-options-panel.component';
import {takeUntil} from 'rxjs/operators';
import {OptionServerOperationHandler} from '../../../services/option-server-operation-handler';

@Component({
  selector: 'sqtm-app-custom-field-content',
  templateUrl: './custom-field-content.component.html',
  styleUrls: ['./custom-field-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: OPTIONS_TABLE_CONF,
      useFactory: optionsTableDefinition
    },
    {
      provide: OptionServerOperationHandler,
      useClass: OptionServerOperationHandler,
      deps: [CustomFieldViewService],
    },
    {
      provide: OPTIONS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, OPTIONS_TABLE_CONF, ReferentialDataService, OptionServerOperationHandler]
    }
  ]
})
export class CustomFieldContentComponent implements OnInit, OnDestroy {
  componentData$: Observable<AdminCustomFieldViewState>;
  unsub$ = new Subject<void>();

  @ViewChild(CustomFieldOptionsPanelComponent)
  private customFieldOptionsPanelComponent;

  constructor(private readonly customFieldViewService: CustomFieldViewService,
              @Inject(OPTIONS_TABLE) public optionsGridService: GridService) {
  }

  ngOnInit(): void {
    this.componentData$ = this.customFieldViewService.componentData$.pipe(
      takeUntil(this.unsub$)
    );
  }

  isDropdownList(componentData: AdminCustomFieldViewState): boolean {
    return componentData.customField.inputType === 'DROPDOWN_LIST';
  }

  addCustomFieldOptions($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.customFieldOptionsPanelComponent.openAddCustomFieldOptionDialog();
  }

  deleteCustomFieldOptions($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.customFieldOptionsPanelComponent.deleteCustomFieldOptions();
  }

  ngOnDestroy(): void {
    this.optionsGridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}
