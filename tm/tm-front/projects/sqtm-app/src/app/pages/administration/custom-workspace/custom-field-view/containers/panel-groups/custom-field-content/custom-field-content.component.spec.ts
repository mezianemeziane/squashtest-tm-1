import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CustomFieldContentComponent } from './custom-field-content.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateService} from '@ngx-translate/core';
import {CustomFieldViewService} from '../../../services/custom-field-view.service';
import {of} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {GridService} from 'sqtm-core';

describe('CustomFieldContentComponent', () => {
  let component: CustomFieldContentComponent;
  let fixture: ComponentFixture<CustomFieldContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, AppTestingUtilsModule],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: GridService,
          useValue: jasmine.createSpyObj(['refreshData'])
        },
        {
          provide: CustomFieldViewService,
          useValue: {
            componentData$: of({
              customField: {
                inputType: 'DROPDOWN_LIST'
              }
            })
          }
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [ CustomFieldContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomFieldContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
