import {Component, OnInit, ChangeDetectionStrategy, InjectionToken, ViewContainerRef, OnDestroy} from '@angular/core';
import {
  DataRow, deleteColumn,
  DialogService,
  Extendable,
  GridDefinition, GridFilter,
  GridService,
  indexColumn,
  smallGrid, Sort,
  StyleDefinitionBuilder
} from 'sqtm-core';
import {getTeamViewState, TeamViewService} from '../../../services/team-view.service';
import {concatMap, filter, finalize, map, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {createSelector, select} from '@ngrx/store';
import {Observable, Subject} from 'rxjs';
import {AdminTeamViewComponentData} from '../../../containers/team-view/team-view.component';
import {AdminTeamState} from '../../../states/admin-team-state';
import {teamAuthorisationProfileColumn} from '../../cell-renderers/team-authorisation-profile-cell/team-authorisation-profile-cell.component';
import {AddTeamAuthorisationDialogComponent} from '../../dialogs/add-team-authorisation-dialog/add-team-authorisation-dialog.component';
import {RemoveTeamAuthorisationCellComponent} from '../../cell-renderers/remove-team-authorisation-cell/remove-team-authorisation-cell.component';
import {clickableProjectNameColumn} from '../../../../../cell-renderer.builders';

export const TEAM_AUTHORISATIONS_TABLE_CONF = new InjectionToken('TEAM_AUTHORISATIONS_TABLE_CONF');
export const TEAM_AUTHORISATIONS_TABLE = new InjectionToken('TEAM_AUTHORISATIONS_TABLE');

export function teamAuthorisationsTableDefinition(): GridDefinition {
  return smallGrid('team-authorisations')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      clickableProjectNameColumn('projectName')
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Extendable(200, 0.5)),
      teamAuthorisationProfileColumn('permissionGroup')
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
      deleteColumn(RemoveTeamAuthorisationCellComponent),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .enableMultipleColumnsFiltering(['projectName'])
    .withInitialSortedColumns([{id: 'projectName', sort: Sort.ASC}])
    .build();
}

@Component({
  selector: 'sqtm-app-team-authorisations-panel',
  template: `
    <div class="m-t-10 m-l-10 m-b-15 flex-fixed-size" style="width: 200px">
        <sqtm-core-text-research-field (newResearchValue)="handleResearchInput($event)"></sqtm-core-text-research-field>
    </div>
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./team-authorisations-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: TEAM_AUTHORISATIONS_TABLE
    }
  ]
})
export class TeamAuthorisationsPanelComponent implements OnInit, OnDestroy {

  private componentData$: Observable<AdminTeamViewComponentData>;

  private unsub$ = new Subject<void>();

  constructor(private teamViewService: TeamViewService,
              public gridService: GridService,
              private dialogService: DialogService,
              private vcRef: ViewContainerRef) {
  }

  ngOnInit(): void {
    this.componentData$ = this.teamViewService.componentData$;
    this.initializeTable();
    this.initializeFilters();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  refreshGrid(): void {
    this.gridService.refreshData();
  }

  openAddTeamAuthorisationDialog(): void {
    const dialogRef = this.dialogService.openDialog({
      id: 'add-team-authorisation-dialog',
      component: AddTeamAuthorisationDialogComponent,
      viewContainerReference: this.vcRef,
      width: 720,
      top: '100px'
    });

    dialogRef.dialogClosed$.subscribe((result) => {
      if (result) {
        this.refreshGrid();
      }
    });
  }

  removeAuthorisations(): void {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) => rows.map(row => row.data.projectId)),
      concatMap((projectIds: number[]) => this.showConfirmDeleteProjectAuthorisationDialog(projectIds)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      switchMap(({projectIds}) => this.teamViewService.removeAuthorisation(projectIds)),
      finalize(() => this.gridService.completeAsyncOperation()),
    ).subscribe();
  }

  private showConfirmDeleteProjectAuthorisationDialog(projectIds): Observable<ConfirmDeleteDialogResult> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.teams.dialog.title.remove-authorisation.remove-many',
      messageKey: 'sqtm-core.administration-workspace.teams.dialog.message.remove-authorisation.remove-many',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, projectIds}))
    );
  }

  private initializeFilters(): void {
    const filters: GridFilter[] = [{
      id: 'projectName', active: false, initialValue: {kind: 'single-string-value', value: ''}, tiedToPerimeter: false
    }];
    this.gridService.addFilters(filters);
  }

  handleResearchInput($event: string): void {
    this.gridService.applyMultiColumnsFilter($event);
  }

  private initializeTable(): void {
    const permissionTable = this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createSelector(getTeamViewState, (teamState: AdminTeamState) => teamState.projectPermissions)),
    );

    this.gridService.connectToDatasource(permissionTable, 'projectId');
  }
}

interface ConfirmDeleteDialogResult {
  confirmDelete: boolean;
  projectIds: number[];
}
