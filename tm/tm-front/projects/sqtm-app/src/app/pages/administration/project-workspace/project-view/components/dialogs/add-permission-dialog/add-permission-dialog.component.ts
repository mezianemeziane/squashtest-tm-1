import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {
  AclGroup,
  DialogReference,
  DisplayOption,
  FieldValidationError,
  getAclGroupI18nKey,
  GroupedMultiListFieldComponent,
  ListItem,
  RestService,
  SelectFieldComponent
} from 'sqtm-core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProjectViewService} from '../../../services/project-view.service';
import {TranslateService} from '@ngx-translate/core';
import {switchMap, take} from 'rxjs/operators';
import {AddPermissionDialogConfiguration} from './add-permission-dialog-configuration';

@Component({
  selector: 'sqtm-app-add-permission-dialog',
  templateUrl: './add-permission-dialog.component.html',
  styleUrls: ['./add-permission-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddPermissionDialogComponent implements OnInit, AfterViewInit {

  public listItems: ListItem[] = [];
  public profileOptions: DisplayOption[];
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  errorsOnMultiListField: string[] = [];
  data: AddPermissionDialogConfiguration;

  @ViewChild(GroupedMultiListFieldComponent)
  partyList: GroupedMultiListFieldComponent;

  @ViewChild(SelectFieldComponent)
  profileSelectField: SelectFieldComponent;

  constructor(private dialogReference: DialogReference<AddPermissionDialogConfiguration>,
              private restService: RestService,
              private cdr: ChangeDetectorRef,
              private fb: FormBuilder,
              private projectViewService: ProjectViewService,
              private translateService: TranslateService) {
    this.data = this.dialogReference.data;
  }

  ngOnInit(): void {
    this.initializeFormGroup();
  }

  ngAfterViewInit(): void {
    this.prepareProfileSelectField();
    this.preparePartiesSelectField();
  }

  private preparePartiesSelectField() {
    this.projectViewService.componentData$.pipe(
      take(1),
      switchMap(componentData => {
        const projectId = componentData.project.id.toString();

        return this.restService.get<UnboundPartiesResponse>(
          ['project-view', projectId, 'unbound-parties']);
      })
    ).subscribe((response) => {
      const parties = this.retrievePartiesAsListItems(response);

      parties.sort((partyA, partyB) => partyA.label.localeCompare(partyB.label));

      this.listItems = [...parties];
      this.cdr.detectChanges();
    });
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      group: this.fb.control(null, [
        Validators.required,
      ]),
    });
  }

  private prepareProfileSelectField(): void {
    const options = this.retrieveProfileOptions();

    // Sort options by locale label
    options.sort((a, b) => {
      return a.label.localeCompare(b.label);
    });

    this.profileOptions = [...options];

    if (options.length > 0 && this.profileSelectField != null) {
      this.profileSelectField.disabled = false;
    }
  }

  private retrievePartiesAsListItems(response: UnboundPartiesResponse) {
    return response[this.data.permissionScope].map(party => {
      return {
        id: party.id,
        label: party.label,
        selected: false,
      };
    });
  }

  private retrieveProfileOptions() {
    return Object.keys(AclGroup)
      .map(key => AclGroup[key])
      .map((groupName) => {
        return {
          label: this.translateService.instant(getAclGroupI18nKey(groupName)),
          id: groupName,
        };
      });
  }

  selectedPartiesChanged($event: ListItem[]) {
    this.partyList.selectedItems = $event;
  }

  confirm() {
    if (this.formIsValid()) {
      this.doPost();
    } else {
      // logger.debug('client side errors when trying to add at ' + this.postUrl);
      this.showClientSideErrors();
    }
  }

  private getSelectedPartyIds(): number[] {
    return this.partyList.selectedItems.map(party => Number(party.id));
  }

  private doPost() {
    this.projectViewService.componentData$.pipe(
      take(1),
    ).subscribe(() => {
      const group = this.formGroup.controls['group'].value;
      this.projectViewService.setPartyProjectPermissions(this.getSelectedPartyIds(), group).subscribe();

      this.dialogReference.result = true;
      this.dialogReference.close();
    });
  }

  private formIsValid() {
    return this.formGroup.status === 'VALID' && this.partyList.selectedItems.length > 0;
  }

  private showClientSideErrors() {
    this.profileSelectField.showClientSideError();

    if (this.partyList.selectedItems.length === 0) {
      const requiredKey = 'sqtm-core.validation.errors.required';
      this.errorsOnMultiListField.push(requiredKey);
    }
  }
}

interface UnboundPartiesResponse {
  users: { id: string, label: string }[];
  teams: { id: string, label: string }[];
}
