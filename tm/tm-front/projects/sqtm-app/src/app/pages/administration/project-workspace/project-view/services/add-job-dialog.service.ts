import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {createStore, Identifier, RestService, Store, TestAutomationProject} from 'sqtm-core';
import {map, pluck, shareReplay, take} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AddJobDialogService {

  public readonly store: Store<AddJobDialogState>;
  public readonly state$: Observable<AddJobDialogState>;

  public readonly dataState$: Observable<AddJobDialogDataState>;
  public readonly taProjects$: Observable<TestAutomationProject[]>;

  constructor(private restService: RestService) {
    this.store = createStore<AddJobDialogState>(getInitialAddJobDialogState());
    this.state$ = this.store.state$;

    this.taProjects$ = this.store.state$.pipe(
      pluck('taProjects'),
      shareReplay(1));

    this.dataState$ = this.store.state$.pipe(
      pluck('dialogDataState'),
      shareReplay(1));
  }

  load(tmProjectId: number) {
    const urlParts = ['project-view', tmProjectId.toString(), 'available-ta-projects'];

    this.restService.get<{ taProjects?: TestAutomationProject[], squashTMError?: any }>(urlParts)
      .subscribe(
        (response) => {
          const taProjects = response.taProjects.map((project, index) => {
            project.taProjectId = index;
            return project;
          });

          this.store.commit({
            taProjects,
            dialogDataState: 'ready',
          });
        },
        () => {
          this.store.commit({
            taProjects: [],
            dialogDataState: 'error',
          });
        });
  }

  unload() {
    this.store.commit({
      taProjects: [],
      dialogDataState: 'loading',
    });
  }

  setJobLabel(remoteName: string, newValue: string) {
    this.state$.pipe(
      take(1),
      map(state => {
        const taProjects = state.taProjects.map((project) => ({
          ... project,
          label: project.remoteName === remoteName ? newValue : project.label
        }));

        return {...state, taProjects};
      })
    ).subscribe((newState) => {
      this.store.commit(newState);
    });
  }

  setCanRunBdd(remoteName: string, canRunBdd: boolean) {
    if (canRunBdd) {
      this.setUniqueBddJob(remoteName);
    } else {
      this.doSetCanRunBdd(remoteName, canRunBdd);
    }
  }

  private doSetCanRunBdd(remoteName: Identifier, canRunBdd: boolean) {
    this.state$.pipe(
      take(1),
      map(state => {
        const taProjects = state.taProjects.map((project) => {
          if (project.remoteName === remoteName) {
            project.canRunBdd = canRunBdd;
          }

          return project;
        });

        return { ...state, taProjects };
      })
    ).subscribe((newState) => {
      this.store.commit(newState);
    });
  }

  private setUniqueBddJob(remoteName: string) {
    this.state$.pipe(
      take(1),
      map(state => {
        const taProjects = state.taProjects.map((project) => ({
          ... project,
          canRunBdd: project.remoteName === remoteName
        }));

        return {...state, taProjects};
      })
    ).subscribe((newState) => {
      this.store.commit(newState);
    });
  }
}

interface AddJobDialogState {
  taProjects: TestAutomationProject[];
  dialogDataState: AddJobDialogDataState;
}

function getInitialAddJobDialogState(): AddJobDialogState {
  return {
    dialogDataState: 'loading',
    taProjects: [],
  };
}

export type AddJobDialogDataState = 'loading' | 'ready' | 'error';
