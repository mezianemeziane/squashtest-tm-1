import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DeleteInfoListComponent} from './delete-info-list.component';
import {
  DialogService,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {OverlayModule} from '@angular/cdk/overlay';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';

describe('DeleteInfoListCellRendererComponent', () => {
  let component: DeleteInfoListComponent;
  let fixture: ComponentFixture<DeleteInfoListComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteInfoListComponent],
      imports: [AppTestingUtilsModule, OverlayModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: DialogService,
          userValue: {}
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteInfoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
