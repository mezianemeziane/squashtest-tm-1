import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewContainerRef} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DialogService,
  GridService,
  InterWindowCommunicationService,
  InterWindowMessages
} from 'sqtm-core';
import {ConfigurePluginDialogComponent} from '../../dialogs/configure-plugin-dialog/configure-plugin-dialog.component';
import {ConfigurePluginDialogConfiguration} from '../../dialogs/configure-plugin-dialog/configure-plugin-dialog-configuration';
import {ProjectViewService} from '../../../services/project-view.service';
import {filter, map, switchMap, take, takeUntil} from 'rxjs/operators';
import {AdminProjectViewComponentData} from '../../../containers/project-view/project-view.component';
import {Subject} from 'rxjs';

@Component({
  selector: 'sqtm-app-plugin-configure-cell',
  template: `
    <ng-container *ngIf="row && hasConfigurationUrl">
      <div class="configure-button">
        <button class="flex-fixed-size"
                nz-button
                nzType="default"
                nzShape="circle"
                [nzSize]="'small'"
                [disabled]="!this.row.data['enabled']"
                [attr.data-test-button-id]="'configure-plugin'"
                (click)="configurePlugin()">
          <i nz-icon nzType="setting" class="icon table-icon-size"></i>
        </button>
      </div>
    </ng-container>
  `,
  styleUrls: ['./plugin-configure-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PluginConfigureCellComponent extends AbstractCellRendererComponent {

  unsub$ = new Subject<void>();

  constructor(public grid: GridService,
              public projectViewService: ProjectViewService,
              public cdRef: ChangeDetectorRef,
              private dialogService: DialogService,
              private vcr: ViewContainerRef,
              private interWindowCommunicationService: InterWindowCommunicationService) {
    super(grid, cdRef);
  }

  get hasConfigurationUrl() {
    return Boolean(this.row.data[this.columnDisplay.id]);
  }

  configurePlugin() {
    const url: string = this.row.data[this.columnDisplay.id];
    const dialogRef = this.dialogService.openDialog<ConfigurePluginDialogConfiguration, any>({
      id: 'configure-plugin-dialog',
      component: ConfigurePluginDialogComponent,
      data: {
        url
      },
      viewContainerReference: this.vcr,
      width: 1500,
      height: 768
    });

    this.interWindowCommunicationService.interWindowMessages$.pipe(
      takeUntil(this.unsub$),
      takeUntil(dialogRef.dialogClosed$),
      filter((messages: InterWindowMessages) => messages.isTypeOf('PLUGIN-CLOSE-DIALOG')),
    ).subscribe(() => {
      dialogRef.close();
    });

    dialogRef.dialogClosed$.pipe(
      take(1),
      switchMap(() => this.projectViewService.componentData$),
      take(1),
      map((componentData: AdminProjectViewComponentData) => componentData.project.id)
    ).subscribe(id => this.projectViewService.load(id));
  }
}

export function configurePluginColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(PluginConfigureCellComponent);
}
