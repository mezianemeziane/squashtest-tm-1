import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestAutomationServerCreationDialogComponent} from './test-automation-server-creation-dialog.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogReference} from 'sqtm-core';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CKEditorModule} from 'ckeditor4-angular';
import createSpyObj = jasmine.createSpyObj;
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';

describe('TestAutomationServerCreationDialogComponent', () => {
  let component: TestAutomationServerCreationDialogComponent;
  let fixture: ComponentFixture<TestAutomationServerCreationDialogComponent>;
  const dialogReference = createSpyObj(['close']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, ReactiveFormsModule, TranslateModule.forRoot(), HttpClientTestingModule, CKEditorModule],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference
        }],
      declarations: [TestAutomationServerCreationDialogComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestAutomationServerCreationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
