import {sqtmIconLib} from 'sqtm-core';

export const INFO_LIST_ICON_NAMESPACE = 'sqtm-core-infolist-item:';

export function getInfoListIcons(): string[] {
  return sqtmIconLib
    .filter(icon => icon.id.includes(INFO_LIST_ICON_NAMESPACE))
    .map(icon => icon.id);
}
