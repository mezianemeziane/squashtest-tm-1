import {
  AttachmentListState,
  BddImplementationTechnologyKeys,
  BddScriptLanguageKeys,
  Bindings,
  BugTracker,
  BugTrackerBinding,
  ExecutionStatusKeys,
  InfoList,
  MilestoneAdminProjectView,
  MilestoneBinding,
  ProjectPermissions, ProjectPlugin,
  ScmServer,
  SqtmGenericEntityState,
  TestAutomationProject,
  TestAutomationServer
} from 'sqtm-core';

export interface AdminProjectState extends SqtmGenericEntityState {
  id: number;
  uri: string;
  name: string;
  label: string;
  testCaseNatureId: number;
  testCaseTypeId: number;
  requirementCategoryId: number;
  allowAutomationWorkflow: boolean;
  customFieldBindings: Bindings;
  permissions: ProjectPermissions;
  availableBugtrackers: BugTracker[];
  bugTrackerBinding: BugTrackerBinding;
  bugtrackerProjectNames: string[];
  milestoneBindings: MilestoneBinding[];
  taServerId: number;
  automationWorkflowType: string;
  disabledExecutionStatus: ExecutionStatusKeys[];
  attachmentList: AttachmentListState;
  hasData: boolean;
  createdOn: string;
  createdBy: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  linkedTemplate: string;
  linkedTemplateId: number;
  description: string;
  allowTcModifDuringExec: boolean;
  allowedStatuses: AllowedStatusesState;
  statusesInUse: StatusesInUseState;
  useTreeStructureInScmRepo: boolean;
  scmRepositoryId?: number;
  infoLists: InfoList[];
  template: boolean;
  templateLinkedToProjects: boolean;
  partyProjectPermissions: PartyProjectPermission[];
  availableScmServers: ScmServer[];
  availableTestAutomationServers: TestAutomationServer[];
  boundTestAutomationProjects: TestAutomationProject[];
  boundMilestonesInformation: MilestoneAdminProjectView[];
  availablePlugins: ProjectPlugin[];
  automatedSuitesLifetime: number;
  bddImplementationTechnology: BddImplementationTechnologyKeys;
  bddScriptLanguage: BddScriptLanguageKeys;
  hasTemplateConfigurablePluginBinding: boolean;
}

export interface AllowedStatusesState {
  UNTESTABLE: boolean;
  SETTLED: boolean;
}

export interface StatusesInUseState {
  UNTESTABLE: boolean;
  SETTLED: boolean;
}

export interface PartyProjectPermission {
  projectId: number;
  partyName: string;
  partyId: number;
  permissionGroup: PermissionGroup;
  team: boolean;
}

export interface PermissionGroup {
  id: number;
  qualifiedName: string;
  simpleName: string;
}
