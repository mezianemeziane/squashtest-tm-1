import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  TemplateRef,
  ViewChild
} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {
  BindableEntity,
  CustomField,
  DialogReference,
  FieldValidationError,
  GroupedMultiListFieldComponent,
  ListItem
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import { NzSelectComponent } from 'ng-zorro-antd/select';
import {ProjectViewService} from '../../../services/project-view.service';
import {Subject} from 'rxjs';
import {take, takeUntil} from 'rxjs/operators';
import {AdminProjectViewComponentData} from '../../../containers/project-view/project-view.component';

@Component({
  selector: 'sqtm-app-bind-custom-field-dialog',
  templateUrl: './bind-custom-field-dialog.component.html',
  styleUrls: ['./bind-custom-field-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BindCustomFieldDialogComponent implements AfterViewInit, OnDestroy {

  @ViewChild('customFieldMultiList')
  customFieldMultiList: GroupedMultiListFieldComponent;

  @ViewChild('boundEntitySelectField')
  boundEntitySelectField: NzSelectComponent;

  @ViewChild('customFieldItemTemplate')
  customFieldItemTemplate: TemplateRef<any>;

  boundEntityOptionGroups: BoundEntityOptionGroup[];
  customFieldListItems: ListItem[] = [];
  selectedEntity: BindableEntity | null = null;
  data: BindCustomFieldDialogData;

  serverSideValidationErrors: FieldValidationError[] = [];
  errorsOnBoundEntity: string[] = [];
  errorsOnCustomFields: string[] = [];

  private unsub$ = new Subject<void>();

  constructor(public readonly fb: FormBuilder,
              public readonly translateService: TranslateService,
              public readonly dialogReference: DialogReference,
              public readonly projectViewService: ProjectViewService) {
    this.data = this.dialogReference.data;
  }

  ngAfterViewInit(): void {
    this.buildBoundEntityOptions();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  confirm() {
    if (this.checkValidity()) {
      this.dialogReference.result = {
        selectedEntity: this.selectedEntity,
        selectedCustomFields: this.selectedCustomFields
      };
      this.dialogReference.close();
    }
  }

  handleCustomFieldSelectionChange(newValues: ListItem[]) {
    this.customFieldMultiList.selectedItems = newValues;
  }

  getCustomField(id: any): CustomField {
    return this.data.customFields.filter(cuf => cuf.id === id)[0];
  }

  getCustomFieldType(id: any) {
    return this.getCustomField(id)?.inputType;
  }

  getCustomFieldTypeLabelKey(id: any) {
    return 'sqtm-core.entity.custom-field.' + this.getCustomFieldType(id);
  }


  private get selectedCustomFields(): CustomField[] | null {
    return this.customFieldMultiList?.selectedItems.map(item => this.getCustomField(Number(item.id)));
  }

  private buildBoundEntityOptions() {
    const makeBoundOptionKey = (entity: BindableEntity) => 'sqtm-core.entity.custom-field.bound-entity.' + entity;

    const makeBoundEntityOption = (entity: BindableEntity) => ({
      label: makeBoundOptionKey(entity),
      value: entity,
    });

    this.boundEntityOptionGroups = [
      {
        label: makeBoundOptionKey(BindableEntity.REQUIREMENT_VERSION),
        options: [
          makeBoundEntityOption(BindableEntity.REQUIREMENT_FOLDER),
          makeBoundEntityOption(BindableEntity.REQUIREMENT_VERSION),
        ],
      },
      {
        label: makeBoundOptionKey(BindableEntity.TEST_CASE),
        options: [
          makeBoundEntityOption(BindableEntity.TESTCASE_FOLDER),
          makeBoundEntityOption(BindableEntity.TEST_CASE),
          makeBoundEntityOption(BindableEntity.TEST_STEP),
        ],
      },
      {
        label: makeBoundOptionKey(BindableEntity.CAMPAIGN),
        options: [
          makeBoundEntityOption(BindableEntity.CAMPAIGN_FOLDER),
          makeBoundEntityOption(BindableEntity.CAMPAIGN),
          makeBoundEntityOption(BindableEntity.ITERATION),
          makeBoundEntityOption(BindableEntity.TEST_SUITE),
          makeBoundEntityOption(BindableEntity.EXECUTION),
          makeBoundEntityOption(BindableEntity.EXECUTION_STEP),
        ],
      },
    ];
  }

  buildCustomFieldMultiListItems() {
    this.projectViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      take(1),
    ).subscribe((componentData: AdminProjectViewComponentData) => {
      const boundCustomFieldIds = this.retrieveBoundCustomFieldIds(componentData);
      this.customFieldListItems = this.getBoundCustomFieldAsListItems(boundCustomFieldIds);
    });
  }

  private getBoundCustomFieldAsListItems(boundCustomFieldIds: number[]) {
    return this.data.customFields
      .filter(cuf => !boundCustomFieldIds.includes(cuf.id))
      .map(cuf => {
        return {
          selected: false,
          id: cuf.id,
          label: cuf.name,
          customTemplate: this.customFieldItemTemplate,
        };
      });
  }

  private retrieveBoundCustomFieldIds(componentData: AdminProjectViewComponentData) {
    return componentData.project.customFieldBindings[this.selectedEntity]
      .map(cufBinding => cufBinding.customFieldId);
  }

  private checkValidity(): boolean {
    let hasError = false;

    this.errorsOnBoundEntity = [];
    this.errorsOnCustomFields = [];

    const requiredKey = 'sqtm-core.validation.errors.required';

    if (this.selectedEntity == null) {
      this.errorsOnBoundEntity.push(requiredKey);
      hasError = true;
    }

    if (this.selectedEntity != null && this.selectedCustomFields == null || this.selectedCustomFields.length === 0) {
      this.errorsOnCustomFields.push(requiredKey);
      hasError = true;
    }

    return !hasError;
  }
}

interface BoundEntityOption {
  label: string;
  value: BindableEntity;
}

interface BoundEntityOptionGroup {
  label: string;
  options: BoundEntityOption[];
}

interface BindCustomFieldDialogData {
  customFields: CustomField[];
}

export interface SelectedCustomFieldsDialogResult {
  selectedEntity: BindableEntity;
  selectedCustomFields: CustomField[];
}
