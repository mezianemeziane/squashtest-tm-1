import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {CreationDialogData, DialogReference, FieldValidationError, RestService} from 'sqtm-core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {AbstractAdministrationCreationDialogDirective} from '../../../../../components/abstract-administration-creation-dialog';
import {of} from 'rxjs';

@Component({
  selector: 'sqtm-app-template-creation-dialog',
  templateUrl: './template-creation-dialog.component.html',
  styleUrls: ['./template-creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemplateCreationDialogComponent extends AbstractAdministrationCreationDialogDirective implements OnInit {

  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: CreationDialogData;

  constructor(private fb: FormBuilder,
              private translateService: TranslateService,
              dialogReference: DialogReference,
              restService: RestService,
              cdr: ChangeDetectorRef) {
    super('projects/new-template', dialogReference, restService, cdr);
    this.data = this.dialogReference.data;
  }

  get textFieldToFocus(): string {
    return 'name';
  }

  ngOnInit() {
    this.initializeFormGroup();
  }

  protected doResetForm(): void {
    this.resetFormControl('name', '');
    this.resetFormControl('description', '');
    this.resetFormControl('label', '');
  }

  protected getRequestPayload() {
    return of({
      name: this.getFormControlValue('name'),
      description: this.getFormControlValue('description'),
      label: this.getFormControlValue('label'),
    });
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern('(.|\\s)*\\S(.|\\s)*'),
        Validators.maxLength(255)
      ]),
      description: this.fb.control(''),
      label: this.fb.control('', [Validators.maxLength(255)]),
    });
  }
}
