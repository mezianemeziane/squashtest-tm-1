import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ChangeExecStatusUsedDialogComponent} from './change-exec-status-used-dialog.component';
import {CapitalizePipe, DialogReference, RestService} from 'sqtm-core';
import {ReactiveFormsModule} from '@angular/forms';
import {ProjectViewService} from '../../../services/project-view.service';
import {of} from 'rxjs';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AdminProjectState} from '../../../state/admin-project-state';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('ChangeExecutionStatusAlreadyInUseDialogComponent', () => {
  let component: ChangeExecStatusUsedDialogComponent;
  let fixture: ComponentFixture<ChangeExecStatusUsedDialogComponent>;
  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, ReactiveFormsModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: ProjectViewService,
          useValue: {
            componentData$: of({project: {id: -1} as AdminProjectState, type: 'project', uiState: null}),
          }
        },
        {
          provide: DialogReference,
          useValue: jasmine.createSpyObj(['close'])
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant'])
        },
        TranslateService,
        CapitalizePipe
      ],
      declarations: [ChangeExecStatusUsedDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    restService.get.and.returnValue(of({statuses: [{id: 'SUCCESS', label: 'SUCCESS'}]}));
    fixture = TestBed.createComponent(ChangeExecStatusUsedDialogComponent);
    component = fixture.componentInstance;
    component.data = {selectedStatus: 'UNTESTABLE'};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
