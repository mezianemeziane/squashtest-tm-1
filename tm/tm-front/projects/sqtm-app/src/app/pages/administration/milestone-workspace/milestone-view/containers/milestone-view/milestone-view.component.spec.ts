import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {MilestoneViewComponent} from './milestone-view.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {of} from 'rxjs';
import {DialogService, GridService} from 'sqtm-core';
import {MilestoneViewService} from '../../services/milestone-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('MilestoneViewComponent', () => {
  let component: MilestoneViewComponent;
  let fixture: ComponentFixture<MilestoneViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, AppTestingUtilsModule],
      declarations: [MilestoneViewComponent],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({id: 123}),
            paramMap: of(convertToParamMap({
              toto: 'tutu',
            })),
          }
        },
        {
          provide: DialogService,
          useValue: jasmine.createSpyObj(['openDeletionConfirm', 'openAlert'])
        },
        {
          provide: GridService,
          useValue: jasmine.createSpyObj(['refreshData'])
        },
        {
          provide: MilestoneViewService,
          useValue: {},
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilestoneViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
