import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InfoListCreationDialogComponent } from './info-list-creation-dialog.component';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CKEditorModule} from 'ckeditor4-angular';
import {DialogReference} from 'sqtm-core';
import {DropdownListOptionService} from '../../../services/dropdown-list-option.service';
import {of} from 'rxjs';
import createSpyObj = jasmine.createSpyObj;
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('InfoListCreationDialogComponent', () => {
  let component: InfoListCreationDialogComponent;
  let fixture: ComponentFixture<InfoListCreationDialogComponent>;
  const dialogReference = createSpyObj(['close']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoListCreationDialogComponent ],
      imports: [AppTestingUtilsModule, ReactiveFormsModule, TranslateModule.forRoot(), HttpClientTestingModule, CKEditorModule],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference
        },
        {
          provide: DropdownListOptionService,
          useValue: {
            state$: of(null),
            infoListOptions$: of([]),
            initialize: () => {},
          }
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoListCreationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
