import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CustomFieldWorkspaceComponent } from './custom-field-workspace.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('CustomFieldWorkspaceComponent', () => {
  let component: CustomFieldWorkspaceComponent;
  let fixture: ComponentFixture<CustomFieldWorkspaceComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomFieldWorkspaceComponent ],
      imports: [HttpClientTestingModule, RouterTestingModule, AppTestingUtilsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomFieldWorkspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
