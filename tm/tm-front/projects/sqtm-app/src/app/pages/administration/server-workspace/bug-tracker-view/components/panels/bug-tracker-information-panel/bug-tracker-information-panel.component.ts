import {Component, OnInit, ChangeDetectionStrategy, Input} from '@angular/core';
import {AdminBugTrackerViewState} from '../../../states/admin-bug-tracker-view-state';
import {Option} from 'sqtm-core';
import {BugTrackerViewService} from '../../../services/bug-tracker-view.service';

@Component({
  selector: 'sqtm-app-bug-tracker-information-panel',
  templateUrl: './bug-tracker-information-panel.component.html',
  styleUrls: ['./bug-tracker-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BugTrackerInformationPanelComponent implements OnInit {

  @Input()
  componentData: AdminBugTrackerViewState;

  kindOptions: Option[];

  constructor(public readonly bugTrackerViewService: BugTrackerViewService) {
  }

  ngOnInit(): void {
    this.kindOptions = this.componentData.bugTracker.bugTrackerKinds.map(kind => ({
      label: kind,
      value: kind,
      hide: false,
    }));

    // There may be cases where the current bugtracker kind used is not registered (because a plugin was removed
    // for example). We still show the value but we hide it from the dropdown options.
    const currentKind = this.componentData.bugTracker.kind;

    if (! this.kindOptions.map(option => option.value).includes(currentKind)) {
      this.kindOptions.push({
        label: currentKind,
        value: currentKind,
        hide: true,
      });
    }
  }

}
