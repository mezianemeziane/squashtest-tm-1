import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, ViewContainerRef} from '@angular/core';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  DataRow,
  dataUpdateTextColumn,
  deleteColumn,
  DialogService,
  Extendable, FilterOperation,
  Fixed,
  grid,
  GridDefinition,
  GridService,
  indexColumn,
  requirementRoleColumn, RequirementVersionLinkType,
  RestService
} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {concatMap, filter, map, take, takeUntil, tap} from 'rxjs/operators';
import {defaultRequirementsLinksColumn} from '../../components/cell-renderers/default-requirements-links/default-requirements-links.component';
import {RequirementsLinksCreationDialogComponent} from '../../components/dialogs/requirements-links-creation-dialog/requirements-links-creation-dialog.component';
import {DeleteRequirementsLinksComponent} from '../../components/cell-renderers/delete-requirements-links/delete-requirements-links.component';
import {TranslateService} from '@ngx-translate/core';
import {RequirementsLinkService} from '../../services/requirements-link.service';

export function adminRequirementsLinksTableDefinition(): GridDefinition {
  return grid('requirementsLinks')
    .withModificationUrl(['requirement-link-type'])
    .withColumns([
      indexColumn()
        .changeWidthCalculationStrategy(new Fixed(60))
        .withViewport('leftViewport'),
      requirementRoleColumn('role')
        .withI18nKey('sqtm-core.entity.requirement.requirement-version.link.role1.label')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2)),
      dataUpdateTextColumn('role1Code')
        .withI18nKey('sqtm-core.entity.requirement.requirement-version.link.role1-code.label')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2)),
      requirementRoleColumn('role2')
        .withI18nKey('sqtm-core.entity.requirement.requirement-version.link.role2.label')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      dataUpdateTextColumn('role2Code')
        .withI18nKey('sqtm-core.entity.requirement.requirement-version.link.role2-code.label')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      defaultRequirementsLinksColumn('default')
        .withI18nKey('sqtm-core.generic.label.default')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1))
        .withHeaderPosition('center')
        .withContentPosition('center'),
      deleteColumn(DeleteRequirementsLinksComponent),
    ])
    .disableRightToolBar()
    .withRowHeight(35)
    .enableMultipleColumnsFiltering(['role', 'role2'])
    .build();
}

@Component({
  selector: 'sqtm-app-requirements-link-grid',
  templateUrl: './requirements-link-grid.component.html',
  styleUrls: ['./requirements-link-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RequirementsLinkGridComponent implements AfterViewInit, OnDestroy {

  authenticatedUser$: Observable<AuthenticatedUser>;

  unsub$ = new Subject<void>();

  constructor(public gridService: GridService,
              private restService: RestService,
              private dialogService: DialogService,
              private adminReferentialDataService: AdminReferentialDataService,
              private viewContainerRef: ViewContainerRef,
              private translateService: TranslateService,
              private requirementLinksService: RequirementsLinkService
  ) {

    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
  }

  ngAfterViewInit() {
    this.initializeGrid();
    this.requirementLinksService.refresh$.pipe(
      takeUntil(this.unsub$)
    ).subscribe(() => this.initializeGrid());
  }

  private initializeGrid() {
    this.restService.post(['requirements-links']).pipe(
      takeUntil(this.unsub$),
      map((response: {requirementLinks: RequirementVersionLinkType[]}) => { return response.requirementLinks.map(linkType => {
        return this.transformLinkTypeToDataRow(linkType);
      });
      }),
      tap(dataRows => this.gridService.loadInitialDataRows(dataRows, dataRows.length))
    ).subscribe( () => this.addFilters());
  }

  private addFilters() {
    this.gridService.addFilters([
      {
        id: 'role',
        active: false,
        initialValue: {kind: 'single-string-value', value: ''},
        tiedToPerimeter: false,
        operation: FilterOperation.LIKE
      },
      {
        id: 'role2',
        active: false,
        initialValue: {kind: 'single-string-value', value: ''},
        tiedToPerimeter: false,
        operation: FilterOperation.LIKE
      }]);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openRequirementsLinksDialog() {

    const dialogReference = this.dialogService.openDialog({
      component: RequirementsLinksCreationDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        titleKey: 'sqtm-core.administration-workspace.requirements-link-types.dialog.title.new-requirements-link-type',
      },
      id: 'requirements-link-dialog',
      width: 600
    });

    dialogReference.dialogResultChanged$.pipe(
      takeUntil(dialogReference.dialogClosed$),
      filter(result => result != null)
    ).subscribe(() => {
      this.initializeGrid();
    });
  }

  deleteRequirementsLinks($event: MouseEvent) {
    $event.stopPropagation();

    this.showRequirementLinkIsDefaultAlertIfRequired();

    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0 && !this.rowIsDefault(rows)),
      concatMap((rows: DataRow[]) => this.showConfirmDeleteRequirementLinkDialog(rows)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      concatMap(({rows}) => this.deleteRequirementLinksServerSide(rows)),
      tap(() => this.gridService.completeAsyncOperation())
    ).subscribe(() => this.initializeGrid());
  }

  private showRequirementLinkIsDefaultAlertIfRequired() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0 && this.rowIsDefault(rows)),
    ).subscribe(() => this.openRequirementLinkIsDefaultAlert());
  }

  private rowIsDefault(rows: DataRow[]): boolean {
    return rows.map(row => row.data['default']).includes(true);
  }

  private openRequirementLinkIsDefaultAlert() {
    this.dialogService.openAlert({
      titleKey: 'sqtm-core.administration-workspace.requirements-link-types.dialog.title.delete-many',
      messageKey: 'sqtm-core.administration-workspace.requirements-link-types.dialog.message.delete-many-including-default-value',
      level: 'DANGER',
    });
  }

  private showConfirmDeleteRequirementLinkDialog(rows): Observable<{ confirmDelete: boolean, rows: string[] }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      messageKey: this.typeIsUsedByRequirementsLinks(rows) ?
        'sqtm-core.administration-workspace.requirements-link-types.dialog.message.delete-many-which-are-used' :
        'sqtm-core.administration-workspace.requirements-link-types.dialog.message.delete-many',
    });
    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, rows}))
    );
  }

  private typeIsUsedByRequirementsLinks(rows: DataRow[]) {
    return rows.map(row => row.data['linkCount'] > 0).includes(true);
  }

  private deleteRequirementLinksServerSide(rows): Observable<void> {
    const pathVariable = rows.map(row => row.data['id']).join(',');
    return this.restService.delete([`requirements-links`, pathVariable]);
  }

  filterRequirementLinks($event: any) {
    this.gridService.applyMultiColumnsFilter($event);
  }

  private transformLinkTypeToDataRow(linkType: RequirementVersionLinkType): DataRow {
    const transformedLinkType = {...linkType};
    const translatedRole = this.translateService.instant(this.getRoleKey(transformedLinkType.role));
    const translatedRole2 = this.translateService.instant(this.getRoleKey(transformedLinkType.role2));
    transformedLinkType.role = translatedRole;
    transformedLinkType.role2 = translatedRole2;
    return {id: transformedLinkType.id, data: {...transformedLinkType}} as unknown as DataRow;
  }

  private getRoleKey(role: string) {
    const isDefaultSystemRole = role.includes('requirement-version.link.type');
    if (isDefaultSystemRole) {
      return 'sqtm-core.entity.requirement.' + role;
    } else {
      return role;
    }
  }
}
