import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';


@Injectable()
export class RequirementsLinkService {

  private _refresh: Subject<void> = new Subject<void>();

  refresh$: Observable<void>;

  constructor() {
    this.refresh$ = this._refresh.asObservable();
  }

  refreshData() {
    this._refresh.next();
  }
}
