import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdminViewHeaderComponent } from './admin-view-header.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogService, GenericEntityViewService} from 'sqtm-core';
import {EMPTY} from 'rxjs';
import {mockDialogService} from '../../../../../utils/testing-utils/mocks.service';

describe('AdminViewHeaderComponent', () => {
  let component: AdminViewHeaderComponent<any, any>;
  let fixture: ComponentFixture<AdminViewHeaderComponent<any, any>>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewHeaderComponent ],
      providers: [
        { provide: GenericEntityViewService, useValue: { componentData$: EMPTY } },
        { provide: DialogService, useValue: mockDialogService() },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
