import {Component, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef} from '@angular/core';
import {AbstractDeleteCellRenderer, DialogService, GridService} from 'sqtm-core';
import {CustomFieldViewService} from '../../../services/custom-field-view.service';
import {finalize} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {AdminCustomFieldViewState} from '../../../states/admin-custom-field-view-state';

@Component({
  selector: 'sqtm-app-remove-option-cell',
  template: `
    <ng-container *ngIf="componentData$ | async as componentData">
    <sqtm-core-delete-icon
      *ngIf="componentData.customField.defaultValue !== row.data['label']"
      (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
    </ng-container>
  `,
  styleUrls: ['./remove-option-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoveOptionCellComponent extends AbstractDeleteCellRenderer implements OnDestroy {

  componentData$: Observable<AdminCustomFieldViewState>;

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              private customFieldViewService: CustomFieldViewService) {
    super(grid, cdr, dialogService);
    this.componentData$ = this.customFieldViewService.componentData$;
  }

  doDelete() {
    this.grid.beginAsyncOperation();
    const optionLabel = this.row.data['label'];
    this.customFieldViewService.deleteOptions([optionLabel]).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe();
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.custom-fields.dialog.title.delete-option.delete-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.custom-fields.dialog.message.delete-option.delete-one';
  }
}
