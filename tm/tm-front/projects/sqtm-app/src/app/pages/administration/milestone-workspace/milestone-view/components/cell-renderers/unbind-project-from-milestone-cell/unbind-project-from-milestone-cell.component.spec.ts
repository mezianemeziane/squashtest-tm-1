import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UnbindProjectFromMilestoneCellComponent } from './unbind-project-from-milestone-cell.component';
import {
  DialogService,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MilestoneViewService} from '../../../services/milestone-view.service';

describe('UnbindProjectFromMilestoneCellComponent', () => {
  let component: UnbindProjectFromMilestoneCellComponent;
  let fixture: ComponentFixture<UnbindProjectFromMilestoneCellComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UnbindProjectFromMilestoneCellComponent ],
      imports: [AppTestingUtilsModule, OverlayModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: DialogService,
          userValue: {}
        },
        {
          provide: TranslateService,
          useValue: {},
        },
        {
          provide: MilestoneViewService,
          useValue: jasmine.createSpyObj(['load', 'complete'])
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnbindProjectFromMilestoneCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
