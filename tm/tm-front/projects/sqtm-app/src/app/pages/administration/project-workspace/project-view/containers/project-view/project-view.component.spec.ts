import { waitForAsync } from '@angular/core/testing';

import {AdminProjectViewComponentData, ProjectViewComponent} from './project-view.component';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {of} from 'rxjs';
import {ChangeDetectorRef, ViewContainerRef} from '@angular/core';
import {Attachment, Project} from 'sqtm-core';
import {ProjectViewService} from '../../services/project-view.service';
import {
  mockActionErrorDisplayService,
  mockAdminReferentialDataService,
  mockAutoConfirmDialogService,
  mockRestService
} from '../../../../../../utils/testing-utils/mocks.service';
import {AdminProjectState} from '../../state/admin-project-state';
import createSpyObj = jasmine.createSpyObj;


describe('ProjectViewComponent', () => {
  let component: ProjectViewComponent;

  const restService = mockRestService();
  const dialogService = mockAutoConfirmDialogService();
  const projectViewService = jasmine.createSpyObj('ProjectViewService', [
    'coerceProjectIntoTemplate', 'disassociateFromTemplate', 'load', 'complete'
  ]);

  beforeEach(() => {
    // I won't use the TestBed here because it won't let me override the ProjectViewService as the providers in
    // the component's decorator seem to take precedence anyway. TestBed.overrideComponent didn't do the trick
    // neither and the official documentation is no help so let's just drop it and instantiate the component
    // class ourselves.

    const route = {
      params: of({projectId: 123}),
      paramMap: of(convertToParamMap({
        projectId: '123',
      })),
    } as unknown as ActivatedRoute;

    const cdRef = createSpyObj(['detectChanges']) as ChangeDetectorRef;
    const vcRef = {} as ViewContainerRef;

    component = new ProjectViewComponent(route, projectViewService, cdRef, dialogService,
      restService, mockAdminReferentialDataService(), vcRef, mockActionErrorDisplayService());

    restService.get.and.returnValue(of(makeProjectModel()));
    restService.delete.calls.reset();

    dialogService.openDeletionConfirm.calls.reset();
    dialogService.openAlert.calls.reset();
    dialogService.openConfirm.calls.reset();

    projectViewService.coerceProjectIntoTemplate.calls.reset();
    projectViewService.disassociateFromTemplate.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return attachments count', () => {
    expect(component.getAttachmentCount({
      entities: {
        1: {kind: 'persisted-attachment'} as Attachment,
        2: {kind: 'persisted-attachment'} as Attachment,
        3: {kind: 'persisted-attachment'} as Attachment,
      },
      ids: [1, 2, 3],
    })).toBe(3);
  });

  it('should delete a project', () => {
    projectViewService.componentData$ = of({
      ...makeComponentData(),
      project: {
        hasData: false,
      } as AdminProjectState,
    });
    component.handleDelete();

    expect(dialogService.openDeletionConfirm).toHaveBeenCalled();
    expect(restService.delete).toHaveBeenCalled();
  });

  it('should forbid to delete a project with data', waitForAsync(() => {
    projectViewService.componentData$ = of({
      ...makeComponentData(),
      project: {
        hasData: true,
      } as AdminProjectState,
    });
    component.handleDelete();

    expect(dialogService.openAlert).toHaveBeenCalled();
    expect(restService.delete).not.toHaveBeenCalled();
  }));

  it('should delete a template', () => {
    projectViewService.componentData$ = of({
      ...makeComponentData(),
      project: {
        template: true,
      } as AdminProjectState,
    });
    component.handleDelete();

    expect(dialogService.openDeletionConfirm).toHaveBeenCalled();
    expect(restService.delete).toHaveBeenCalled();
  });

  it('should coerce into template', () => {
    projectViewService.componentData$ = of({
      ...makeComponentData(),
      project: {
        hasData: false,
      } as AdminProjectState,
    });
    component.coerceIntoTemplate();

    expect(projectViewService.coerceProjectIntoTemplate).toHaveBeenCalled();
  });

  it('should forbid to coerce into template if project has data', waitForAsync(() => {
    projectViewService.componentData$ = of({
      ...makeComponentData(),
      project: {
        hasData: true,
      } as AdminProjectState,
    });
    component.coerceIntoTemplate();

    expect(dialogService.openAlert).toHaveBeenCalled();
    expect(projectViewService.coerceProjectIntoTemplate).not.toHaveBeenCalled();
  }));

  it('should open associate template dialog', () => {
    projectViewService.componentData$ = of({
      ...makeComponentData(),
      project: {
      } as AdminProjectState,
    });
    component.associateTemplate();

    expect(dialogService.openDialog).toHaveBeenCalled();
  });

  it('should open dissociate from template', () => {
    projectViewService.componentData$ = of({
      ...makeComponentData(),
      project: {
        linkedTemplateId: 12,
      } as AdminProjectState,
    });
    component.associateTemplate();

    expect(component['projectViewService']).toBe(projectViewService);

    expect(projectViewService.disassociateFromTemplate).toHaveBeenCalled();
  });
});

function makeComponentData(): AdminProjectViewComponentData {
  return {
    type: undefined, uiState: undefined,
    project: makeProjectState(),
  };
}

function makeProjectState(): AdminProjectState {
  return {
    id: 123,
    hasData: true,
    template: false,
    attachmentList: {
      id: 1,
      attachments: {
        entities: {
          1: {id: '1', kind: 'persisted-attachment'} as Attachment,
          2: {id: '2', kind: 'persisted-attachment'} as Attachment,
          3: {id: '3', kind: 'persisted-attachment'} as Attachment,
        },
        ids: [1, 2, 3],
      },
    }
  } as unknown as AdminProjectState;
}

function makeProjectModel(): Project {
  return {
    template: false,
    attachmentList: {
      id: 1,
      attachments: [
        {id: 1, name: '', addedOn: null, size: 1},
        {id: 2, name: '', addedOn: null, size: 1},
        {id: 3, name: '', addedOn: null, size: 1},
      ],
    }
  } as unknown as Project;
}
