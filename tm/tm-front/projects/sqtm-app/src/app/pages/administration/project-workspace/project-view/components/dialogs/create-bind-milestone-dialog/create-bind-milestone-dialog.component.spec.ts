import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CreateBindMilestoneDialogComponent } from './create-bind-milestone-dialog.component';
import {DialogReference} from 'sqtm-core';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CKEditorModule} from 'ckeditor4-angular';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';

describe('CreateBindMilestoneDialogComponent', () => {
  let component: CreateBindMilestoneDialogComponent;
  let fixture: ComponentFixture<CreateBindMilestoneDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, ReactiveFormsModule, TranslateModule.forRoot(), HttpClientTestingModule, CKEditorModule],
      providers: [
        {
          provide: DialogReference,
          useValue: jasmine.createSpyObj(['close'])
        }
      ],
      declarations: [ CreateBindMilestoneDialogComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBindMilestoneDialogComponent);
    component = fixture.componentInstance;
    component.data = {
      projectId: '-1',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
