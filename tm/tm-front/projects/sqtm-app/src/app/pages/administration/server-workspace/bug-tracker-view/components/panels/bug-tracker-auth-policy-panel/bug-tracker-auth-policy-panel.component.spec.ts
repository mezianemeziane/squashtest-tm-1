import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {BugTrackerAuthPolicyPanelComponent} from './bug-tracker-auth-policy-panel.component';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BugTrackerViewService} from '../../../services/bug-tracker-view.service';
import {
  ActionValidationError,
  AdminBugTrackerState,
  AuthenticationPolicy,
  AuthenticationProtocol,
  Credentials,
  GenericEntityViewService,
  RestService,
  WorkspaceCommonModule
} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AdminBugTrackerViewState} from '../../../states/admin-bug-tracker-view-state';
import {EMPTY, of, throwError} from 'rxjs';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import SpyObj = jasmine.SpyObj;
import {HttpErrorResponse} from '@angular/common/http';
import {makeHttpActionError} from '../../../../../../../utils/testing-utils/test-error-response-generator';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('BugTrackerAuthenticationPolicyPanelComponent', () => {
  let component: BugTrackerAuthPolicyPanelComponent;
  let fixture: ComponentFixture<BugTrackerAuthPolicyPanelComponent>;
  let viewService: SpyObj<BugTrackerViewService>;
  let restService: SpyObj<RestService>;

  beforeEach(waitForAsync(() => {
    viewService = jasmine.createSpyObj([
      'load', 'setBasicAuthCredentials', 'setOAuthCredentials', 'setAuthPolicy', 'setCredentials']);
    viewService['componentData$'] = of(makeComponentData(makeBugTracker()));
    viewService['externalRefreshRequired$'] = EMPTY;

    restService = mockRestService();

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AppTestingUtilsModule,
        FormsModule,
        ReactiveFormsModule,
        WorkspaceCommonModule,
        TranslateModule.forRoot()
      ],
      declarations: [BugTrackerAuthPolicyPanelComponent],
      providers: [
        {
          provide: BugTrackerViewService,
          useValue: viewService,
        },
        {
          provide: GenericEntityViewService,
          useValue: viewService,
        },
        {
          provide: RestService,
          useValue: restService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BugTrackerAuthPolicyPanelComponent);
    component = fixture.componentInstance;
  });

  it('should create', waitForAsync(() => {
    component.componentData = makeComponentData(makeBugTracker());
    fixture.detectChanges();

    expect(component).toBeTruthy();
  }));

  it('should change auth policy', waitForAsync(() => {
    component.componentData = makeComponentData(makeBugTracker());
    fixture.detectChanges();

    viewService.setAuthPolicy.and.returnValue(of({}));
    viewService.setAuthPolicy.calls.reset();

    component.handleAuthPolicyChange({
      id: AuthenticationPolicy.USER,
    });
    expect(viewService.setAuthPolicy).toHaveBeenCalledWith(AuthenticationPolicy.USER);
  }));

  it('should throw if given an unhandled auth policy', waitForAsync(() => {
    component.componentData = makeComponentData(makeBugTracker());
    fixture.detectChanges();

    viewService.setAuthPolicy.and.returnValue(of({}));
    viewService.setAuthPolicy.calls.reset();

    expect(() => component.handleAuthPolicyChange({
      id: 'NOT_A_POLICY',
    })).toThrow();
    expect(viewService.setAuthPolicy).not.toHaveBeenCalled();
  }));

  it('should send credentials form', waitForAsync(() => {
    component.componentData = makeComponentData(makeBugTracker());
    fixture.detectChanges();

    const credentials: Credentials = {
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      type: AuthenticationProtocol.BASIC_AUTH,
      password: 'password',
      username: 'username',
    };

    viewService.setCredentials.and.returnValue(of({}));
    viewService.setCredentials.calls.reset();

    component.sendCredentialsForm(credentials);

    expect(viewService.setCredentials).toHaveBeenCalledWith(credentials);
    expect(component.statusIcon).toBe('INFO');
  }));

  it('should show credentials action errors', waitForAsync(() => {
    component.componentData = makeComponentData(makeBugTracker());
    fixture.detectChanges();

    const credentials: Credentials = {
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      type: AuthenticationProtocol.BASIC_AUTH,
      password: 'password',
      username: 'username',
    };

    viewService.setCredentials.and.returnValue(throwError(
      makeHttpActionError({i18nKey: 'boom'} as ActionValidationError)));

    viewService.setCredentials.calls.reset();

    component.sendCredentialsForm(credentials);

    expect(viewService.setCredentials).toHaveBeenCalledWith(credentials);
    expect(component.statusIcon).toBe('DANGER');
  }));

  it('should log unhandled errors when sending form', () => {
    component.componentData = makeComponentData(makeBugTracker());
    fixture.detectChanges();

    spyOn(console, 'error');

    viewService.setCredentials.and.returnValue(
      throwError(new HttpErrorResponse({ status: 404,
        error: 'badaboom',
      }))
    );

    component.sendCredentialsForm(null);

    expect(console.error).toHaveBeenCalledTimes(1);
  });
});

function makeComponentData(bugTracker: AdminBugTrackerState): AdminBugTrackerViewState {
  return {
    bugTracker,
  } as AdminBugTrackerViewState;
}

function makeBugTracker(): AdminBugTrackerState {
  return {
    id: 1,
    kind: 'bugTracker',
    name: 'name',
    url: 'http://url.com',
    authPolicy: AuthenticationPolicy.APP_LEVEL,
    authProtocol: AuthenticationProtocol.OAUTH_1A,
    iframeFriendly: false,
    bugTrackerKinds: [],
    authConfiguration: null,
    credentials: null,
    supportedAuthenticationProtocols: [],
    attachmentList: {id: null, attachments: null},
  };
}
