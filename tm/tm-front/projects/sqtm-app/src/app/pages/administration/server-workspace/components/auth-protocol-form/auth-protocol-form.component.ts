import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChildren
} from '@angular/core';
import {
  AuthConfiguration,
  AuthenticationProtocol,
  DisplayOption,
  isKnownAuthenticationProtocol,
  Option,
  SignatureMethod, TextAreaFieldComponent,
  TextFieldComponent
} from 'sqtm-core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

const OAUTH_TRANSLATE_KEY_BASE = 'sqtm-core.administration-workspace.bugtrackers.oauth1a.';

@Component({
  selector: 'sqtm-app-auth-protocol-form',
  templateUrl: './auth-protocol-form.component.html',
  styleUrls: ['./auth-protocol-form.component.less'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class AuthProtocolFormComponent implements OnInit {

  @Input()
  set authProtocol(protocol: AuthenticationProtocol) {
    this._protocol = protocol;
    this.resetState();
  }

  get authProtocol(): AuthenticationProtocol {
    return this._protocol;
  }

  private _protocol: AuthenticationProtocol;

  @Input()
  supportedAuthenticationProtocols: string[];

  @Input()
  authConfiguration: AuthConfiguration;

  @Input()
  defaultUrl: string;

  @Output()
  configurationChanged = new EventEmitter<AuthConfiguration>();

  @Output()
  authProtocolChanged = new EventEmitter<AuthenticationProtocol>();

  @ViewChildren(TextFieldComponent)
  textFields: QueryList<TextFieldComponent>;

  @ViewChildren(TextAreaFieldComponent)
  textAreaFields: QueryList<TextAreaFieldComponent>;

  requestMethodOptions: DisplayOption[];
  signatureMethodOptions: DisplayOption[];

  formGroup: FormGroup;

  translateKeys = {
    consumerKey: OAUTH_TRANSLATE_KEY_BASE + 'consumer-key',
    authorize: OAUTH_TRANSLATE_KEY_BASE + 'authorize',
    accessTokens: OAUTH_TRANSLATE_KEY_BASE + 'access-tokens',
    requestTokens: OAUTH_TRANSLATE_KEY_BASE + 'request-tokens',
    secret: OAUTH_TRANSLATE_KEY_BASE + 'secret',
    signatureMethod: OAUTH_TRANSLATE_KEY_BASE + 'signature-method',

    unsavedChangesKey: 'sqtm-core.administration-workspace.servers.label.unsaved-changes',
    saveSuccessKey: 'sqtm-core.administration-workspace.servers.label.save-success',
    saveFailKey: 'sqtm-core.error.generic.label',
  };

  private readonly _authProtocolOptions: Option[];
  private hasServerError = false;
  private hasSaved = false;

  get canShowSelectField(): boolean {
    return this.authProtocol && this.supportedAuthenticationProtocols &&
      this.visibleProtocolOptions.map(opt => opt.value).includes(this.authProtocol);
  }

  constructor(public readonly translateService: TranslateService,
              private readonly formBuilder: FormBuilder,
              private readonly cdRef: ChangeDetectorRef) {
    this._authProtocolOptions = [
      {
        value: AuthenticationProtocol.BASIC_AUTH,
        label: translateService.instant('sqtm-core.entity.server.auth-protocol.basic-auth.label'),
      },
      {
        value: AuthenticationProtocol.OAUTH_1A,
        label: translateService.instant('sqtm-core.entity.server.auth-protocol.oauth_1a.label'),
      },
      {
        value: AuthenticationProtocol.TOKEN_AUTH,
        label: translateService.instant('sqtm-core.entity.server.auth-protocol.token-auth.label'),
      }
    ];
  }

  ngOnInit(): void {
    this.prepareOAuthForm();
  }

  private prepareOAuthForm(): void {
    this.requestMethodOptions = [
      {id: 'GET', label: 'GET'},
      {id: 'POST', label: 'POST'},
    ];

    this.signatureMethodOptions = [
      {id: SignatureMethod.HMAC_SHA1, label: 'HMAC-SHA1'},
      {id: SignatureMethod.RSA_SHA1, label: 'RSA-SHA1'},
    ];

    this.buildFormGroup();
  }

  private buildFormGroup(): void {
    const hasConfiguration = Boolean(this.authConfiguration);

    if (hasConfiguration) {
      const conf = this.authConfiguration;

      if (conf.implementedProtocol === AuthenticationProtocol.OAUTH_1A) {
        this.formGroup = this.buildFormFromConfiguration(conf);
      } else {
        throw new Error('Unknown authentication protocol ' + conf.implementedProtocol);
      }
    } else {
      this.buildDefaultFormGroup();
    }
  }

  private buildDefaultFormGroup(): void {
    const currentUrl = this.defaultUrl;

    this.formGroup = this.formBuilder.group({
      consumerKey: this.formBuilder.control('', [Validators.required]),
      requestTokenHttpMethod: this.requestMethodOptions[0].id,
      requestTokenUrl: this.formBuilder.control(currentUrl, [Validators.required]),
      accessTokenHttpMethod: this.requestMethodOptions[0].id,
      accessTokenUrl: this.formBuilder.control(currentUrl, [Validators.required]),
      userAuthorizationUrl: this.formBuilder.control(currentUrl, [Validators.required]),
      clientSecret: this.formBuilder.control('', [Validators.required]),
      signatureMethod: this.signatureMethodOptions[0].id,
    });
  }

  private buildFormFromConfiguration(conf: AuthConfiguration): FormGroup {
    const controlsConfig = {};
    Object.keys(conf).forEach(key => controlsConfig[key] = this.formBuilder.control(conf[key], [Validators.required]));
    return this.formBuilder.group(controlsConfig);
  }

  sendOAuthForm() {
    if (this.checkOAuthFormValidity()) {
      if (this.authProtocol === 'OAUTH_1A') {
        const authConf: AuthConfiguration = {
          implementedProtocol: this.authProtocol,
          type: this.authProtocol,
          consumerKey: this.formGroup.controls['consumerKey'].value,
          requestTokenHttpMethod: this.formGroup.controls['requestTokenHttpMethod'].value,
          requestTokenUrl: this.formGroup.controls['requestTokenUrl'].value,
          accessTokenHttpMethod: this.formGroup.controls['accessTokenHttpMethod'].value,
          accessTokenUrl: this.formGroup.controls['accessTokenUrl'].value,
          userAuthorizationUrl: this.formGroup.controls['userAuthorizationUrl'].value,
          clientSecret: this.formGroup.controls['clientSecret'].value,
          signatureMethod: this.formGroup.controls['signatureMethod'].value,
        };

        this.configurationChanged.emit(authConf);
      }
    }
  }

  handleServerSuccess(): void {
    this.refreshFieldErrors();
    this.hasServerError = false;
    this.hasSaved = true;
    this.formGroup.markAsPristine();
  }

  handleServerError(): void {
    this.refreshFieldErrors();
    this.hasServerError = true;
    this.hasSaved = false;
    this.cdRef.detectChanges();
  }

  public get visibleProtocolOptions(): Option[] {
    return this._authProtocolOptions
      .filter(option => this.supportedAuthenticationProtocols.includes(option.value));
  }

  public get isOAuth1a(): boolean {
    return this.authProtocol === AuthenticationProtocol.OAUTH_1A;
  }

  public get isOAuth1aAvailable(): boolean {
    return this.supportedAuthenticationProtocols.includes(AuthenticationProtocol.OAUTH_1A);
  }

  public get authConfigurationStatusMessage(): string {
    return this.hasServerError ? this.translateKeys.saveFailKey :
      (this.formGroup.dirty ? this.translateKeys.unsavedChangesKey :
        (this.hasSaved ? this.translateKeys.saveSuccessKey : ''));
  }

  private checkOAuthFormValidity(): boolean {
    if (this.formGroup.valid) {
      return true;
    } else {
      this.refreshFieldErrors();
      return false;
    }
  }

  private refreshFieldErrors(): void {
    this.textFields.forEach(textField => textField.showClientSideError());
    this.textAreaFields.forEach(textField => textField.showClientSideError());
  }

  confirmAuthProtocol(option: Option): void {
    if (isKnownAuthenticationProtocol(option.value)) {
      this.authProtocolChanged.emit(option.value);
    }
  }

  private resetState(): void {
    this.hasSaved = false;
    this.hasServerError = false;
  }
}
