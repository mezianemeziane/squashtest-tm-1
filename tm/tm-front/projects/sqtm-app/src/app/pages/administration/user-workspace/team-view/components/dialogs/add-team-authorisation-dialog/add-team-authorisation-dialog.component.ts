import {Component, OnInit, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef, AfterViewInit} from '@angular/core';
import {
  AclGroup,
  DialogReference,
  DisplayOption,
  FieldValidationError, getAclGroupI18nKey, GridService,
  GroupedMultiListFieldComponent,
  ListItem, RestService,
  SelectFieldComponent
} from 'sqtm-core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
// tslint:disable-next-line:max-line-length
import {TranslateService} from '@ngx-translate/core';
import {finalize, switchMap, take} from 'rxjs/operators';
import {TeamViewService} from '../../../services/team-view.service';

@Component({
  selector: 'sqtm-app-add-team-authorisation-dialog',
  templateUrl: './add-team-authorisation-dialog.component.html',
  styleUrls: ['./add-team-authorisation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddTeamAuthorisationDialogComponent implements OnInit, AfterViewInit {

  listItems: ListItem[] = [];
  profileOptions: DisplayOption[];
  formGroup: FormGroup;
  errorsOnMultiListField: string[] = [];
  serverSideValidationErrors: FieldValidationError[] = [];

  @ViewChild(GroupedMultiListFieldComponent)
  projectList: GroupedMultiListFieldComponent;

  @ViewChild(SelectFieldComponent)
  profileSelectField: SelectFieldComponent;

  constructor(private dialogReference: DialogReference,
              private restService: RestService,
              private cdr: ChangeDetectorRef,
              private fb: FormBuilder,
              private teamViewService: TeamViewService,
              private translateService: TranslateService,
              private grid: GridService) {
  }

  ngOnInit(): void {
    this.initializeFormGroup();
  }

  ngAfterViewInit(): void {
    this.prepareProfileSelectField();
    this.prepareProjectsMultiListField();
  }

  selectedProjectsChanged($event: ListItem[]) {
    this.projectList.selectedItems = $event;
  }

  confirm() {
    if (this.formIsValid()) {
      this.doPost();
    } else {
      this.showClientSideErrors();
    }
  }

  private prepareProfileSelectField(): void {
    const options = this.retrieveProfilesAsDisplayOption();

    // Sort options by locale label
    options.sort((a, b) => {
      return a.label.localeCompare(b.label);
    });

    this.profileOptions = [...options];

    if (options.length > 0 && this.profileSelectField != null) {
      this.profileSelectField.disabled = false;
    }
  }

  private retrieveProfilesAsDisplayOption(): DisplayOption[] {
    return Object.keys(AclGroup)
      .map(key => AclGroup[key])
      .map((groupName) => {
        return {
          label: this.translateService.instant(getAclGroupI18nKey(groupName)),
          id: groupName,
        };
      });
  }

  private prepareProjectsMultiListField() {
    this.teamViewService.componentData$.pipe(
      take(1),
      switchMap(componentData => {
        const userId = componentData.team.id.toString();

        return this.restService.get<ProjectWithoutPermission[]>(
          ['team-view', userId, 'projects-without-permission']);
      })
    ).subscribe((response) => {
      const projects = this.retrieveProjectsAsListItem(response);

      projects.sort((partyA, partyB) => partyA.label.localeCompare(partyB.label));

      this.listItems = [...projects];
      this.cdr.detectChanges();
    });
  }

  private retrieveProjectsAsListItem(response: ProjectWithoutPermission[]): ListItem[] {
    return response.map(project => {
      return {
        id: project.id,
        label: project.name,
        selected: false,
      };
    });
  }

  private getSelectedProjectIds(): number[] {
    return this.projectList.selectedItems.map(project => Number(project.id));
  }

  private doPost() {
    const profile = this.formGroup.controls['profile'].value;
    this.grid.beginAsyncOperation();
    this.teamViewService.setTeamAuthorisation(this.getSelectedProjectIds(), profile).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe();
    this.dialogReference.result = true;
    this.dialogReference.close();
  }

  private formIsValid() {
    return this.formGroup.status === 'VALID' && this.projectList.selectedItems.length > 0;
  }

  private showClientSideErrors() {
    this.profileSelectField.showClientSideError();

    if (this.projectList.selectedItems.length === 0) {
      const requiredKey = 'sqtm-core.validation.errors.required';
      this.errorsOnMultiListField.push(requiredKey);
    }
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      profile: this.fb.control(null, [
        Validators.required,
      ]),
    });
  }
}

interface ProjectWithoutPermission {
  id: string;
  name: string;
}


