import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ProjectCreationDialogComponent} from './project-creation-dialog.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogReference, RestService} from 'sqtm-core';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import createSpyObj = jasmine.createSpyObj;
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CKEditorModule} from 'ckeditor4-angular';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';
import {of} from 'rxjs';

describe('ProjectCreationDialogComponent', () => {
  let component: ProjectCreationDialogComponent;
  let fixture: ComponentFixture<ProjectCreationDialogComponent>;

  const dialogReference = createSpyObj(['close']);
  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, ReactiveFormsModule, TranslateModule.forRoot(), HttpClientTestingModule, CKEditorModule],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference
        },
        {
          provide: RestService,
          useValue: restService,
        }],
      declarations: [ ProjectCreationDialogComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectCreationDialogComponent);
    component = fixture.componentInstance;
    component.data = {
      id: '',
      titleKey: '',
    };

    restService.get.and.returnValue(of({ templates: [{id: 1, name: 'template'}]}));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should submit and reset form', () => {
    component.formGroup.controls['name'].setValue('new project');

    component.addAnother();

    expect(restService.post).toHaveBeenCalled();
    expect(component.formGroup.controls['name'].value).toBe('');
    expect(component.textFieldToFocus).toBe('name');
  });

  it('should enable additional fields when template binding is not kept', () => {
    const fields = [
      'keepCustomFields',
      'keepInfoLists',
      'keepAllowTcModificationsFromExecution',
      'keepOptionalExecutionStatuses',
    ];

    component.handleKeepTemplateBindingChange(false);
    fields.forEach((field) => expect(component.formGroup.controls[field].enabled).toBeTruthy());

    component.handleKeepTemplateBindingChange(true);
    fields.forEach((field) => expect(component.formGroup.controls[field].value).toBeTruthy());
    fields.forEach((field) => expect(component.formGroup.controls[field].enabled).toBeFalsy());
  });
});
