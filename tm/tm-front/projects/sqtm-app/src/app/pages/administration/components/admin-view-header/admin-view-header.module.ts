import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminViewHeaderComponent } from './admin-view-header/admin-view-header.component';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzIconModule } from 'ng-zorro-antd/icon';
import {WorkspaceCommonModule} from 'sqtm-core';



@NgModule({
  declarations: [AdminViewHeaderComponent],
  exports: [
    AdminViewHeaderComponent
  ],
  imports: [
    CommonModule,
    NzDropDownModule,
    WorkspaceCommonModule,
    NzBadgeModule,
    NzIconModule,
  ]
})
export class AdminViewHeaderModule { }
