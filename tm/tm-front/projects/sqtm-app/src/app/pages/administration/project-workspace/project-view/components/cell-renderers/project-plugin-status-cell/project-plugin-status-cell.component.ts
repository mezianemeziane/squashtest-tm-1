import {Component, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-project-plugin-status-cell',
  template: `
    <ng-container *ngIf="row">
      <div class="status-wrapper"
           [ngStyle]="getDisabledIconOpacity(row.disabled)">
        <div class="status" [ngClass]="statusColor"></div>
      </div>
    </ng-container>
  `,
  styleUrls: ['./project-plugin-status-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectPluginStatusCellComponent extends AbstractCellRendererComponent {


  constructor(public grid: GridService,
              public cdr: ChangeDetectorRef) {
    super(grid, cdr);
  }

  get statusColor(): string[] {
    const classes = ['status-indicator'];
    const status = this.row.data[this.columnDisplay.id];
    if (status === 'OK') {
      classes.push('activated');
    } else if (status === 'ERROR' && this.row.data['enabled'] === true) {
      classes.push('unconfigured');
    } else {
      classes.push('disabled');
    }

    return classes;
  }

  getDisabledIconOpacity(disabled: boolean) {
    if (disabled) {
      return {'opacity': '65%'};
    }
  }
}

export function pluginStatusColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ProjectPluginStatusCellComponent);
}
