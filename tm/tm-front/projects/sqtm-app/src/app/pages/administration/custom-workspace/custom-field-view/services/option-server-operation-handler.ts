import {Injectable} from '@angular/core';
import {AbstractAdminServerOperationHandler, Identifier} from 'sqtm-core';
import {CustomFieldViewService} from './custom-field-view.service';

@Injectable()
export class OptionServerOperationHandler extends AbstractAdminServerOperationHandler {

  constructor(private readonly customFieldViewService: CustomFieldViewService) {
    super();
  }

  doChangePosition(draggedRows: Identifier[], newPosition: number) {
    const optionLabels = draggedRows.map(row => row.toString());
    return this.customFieldViewService.changeOptionsPosition(optionLabels, newPosition);
  }
}
