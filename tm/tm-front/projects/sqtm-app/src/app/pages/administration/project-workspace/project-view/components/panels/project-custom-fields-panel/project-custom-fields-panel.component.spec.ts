import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ProjectCustomFieldsPanelComponent} from './project-custom-fields-panel.component';
import {DialogService, RestService} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {OverlayModule} from '@angular/cdk/overlay';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {PROJECT_CUSTOM_FIELDS_TABLE} from '../../../project-view.constant';
import {ProjectViewService} from '../../../services/project-view.service';
import {of} from 'rxjs';
import {mockAutoConfirmDialogService, mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('ProjectCustomFieldsPanelComponent', () => {
  let component: ProjectCustomFieldsPanelComponent;
  let fixture: ComponentFixture<ProjectCustomFieldsPanelComponent>;

  const restService = mockRestService();
  const dialogService = mockAutoConfirmDialogService();
  const projectViewService = jasmine.createSpyObj(['bindCustomFields', 'unbindCustomFields']);
  projectViewService.boundCustomFields$ = of([]);
  const cufTable = jasmine.createSpyObj(['connectToDatasource', 'complete', 'refreshData']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        AppTestingUtilsModule,
        HttpClientTestingModule,
        OverlayModule,
        TranslateModule.forRoot(),
        RouterTestingModule
      ],
      providers: [
        {
          provide: TranslateService,
          useValue: {},
        }, {
          provide: DialogService,
          useValue: dialogService,
        },
        {
          provide: PROJECT_CUSTOM_FIELDS_TABLE,
          useValue: cufTable,
        },
        {
          provide: ProjectViewService,
          useValue: projectViewService,
        },
        {
          provide: RestService,
          useValue: restService,
        }
      ],
      declarations: [ProjectCustomFieldsPanelComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectCustomFieldsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    projectViewService.bindCustomFields.and.returnValue(of({}));
    projectViewService.unbindCustomFields.and.returnValue(of({}));
    projectViewService.bindCustomFields.calls.reset();
    projectViewService.unbindCustomFields.calls.reset();
    cufTable.refreshData.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component['gridService']).toBe(cufTable);
  });

  it('should bind CUFs', waitForAsync(() => {
    restService.get.and.returnValue(of({
      customFields: [
        {name: 'b'},
        {name: 'a'},
      ]
    }));

    component.openBindCustomFieldDialog();

    expect(projectViewService.bindCustomFields).toHaveBeenCalled();
    expect(cufTable.refreshData).toHaveBeenCalled();
  }));

  it('should unbind CUFs', waitForAsync(() => {
    cufTable.selectedRowIds$ = of([1, 2, 3]);

    component.openUnbindCustomFieldsDialog();

    expect(projectViewService.unbindCustomFields).toHaveBeenCalledWith([1, 2, 3]);
    expect(cufTable.refreshData).toHaveBeenCalled();
  }));
});
