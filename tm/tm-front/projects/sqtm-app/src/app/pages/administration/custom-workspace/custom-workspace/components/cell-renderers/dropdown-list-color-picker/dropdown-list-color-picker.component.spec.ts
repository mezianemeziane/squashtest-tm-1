import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DropdownListColorPickerComponent } from './dropdown-list-color-picker.component';
import {
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DropdownListOptionService} from '../../../services/dropdown-list-option.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';

describe('DropdownListOptionColorPickerCellComponent', () => {
  let component: DropdownListColorPickerComponent;
  let fixture: ComponentFixture<DropdownListColorPickerComponent>;
  const gridConfig = grid('grid-test').build();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownListColorPickerComponent ],
      imports: [AppTestingUtilsModule,  HttpClientTestingModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        {
          provide: DropdownListOptionService,
          useValue: {}
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownListColorPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
