import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainUserWorkspaceComponent} from './user-workspace/containers/main-user-workspace/main-user-workspace.component';
import {UserGridComponent} from './user-workspace/containers/user-grid/user-grid.component';
import {TeamGridComponent} from './user-workspace/containers/team-grid/team-grid.component';
import {ConnectionLogGridComponent} from './user-workspace/containers/connection-log-grid/connection-log-grid.component';
import {
  AnchorModule,
  AttachmentModule,
  CellRendererCommonModule,
  DialogModule,
  GridModule,
  NavBarModule,
  UiManagerModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule
} from 'sqtm-core';
import {UserWorkspaceAnchorsComponent} from './user-workspace/components/user-workspace-anchors/user-workspace-anchors.component';
import {NzBadgeModule} from 'ng-zorro-antd/badge';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {NzCollapseModule} from 'ng-zorro-antd/collapse';
import {NzDividerModule} from 'ng-zorro-antd/divider';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzFormModule} from 'ng-zorro-antd/form';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzSpinModule} from 'ng-zorro-antd/spin';
import {NzSwitchModule} from 'ng-zorro-antd/switch';
import {NzTabsModule} from 'ng-zorro-antd/tabs';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {UserActiveCellRendererComponent} from './user-workspace/components/cell-renderers/user-active-cell-renderer/user-active-cell-renderer.component';
import {UserGroupCellRendererComponent} from './user-workspace/components/cell-renderers/user-group-cell-renderer/user-group-cell-renderer.component';
import {DeleteUserCellRendererComponent} from './user-workspace/components/cell-renderers/delete-user-cell-renderer/delete-user-cell-renderer.component';
import {DeleteTeamCellRendererComponent} from './user-workspace/components/cell-renderers/delete-team-cell-renderer/delete-team-cell-renderer.component';
import {TranslateModule} from '@ngx-translate/core';
import {TeamCreationDialogComponent} from './user-workspace/components/dialogs/team-creation-dialog/team-creation-dialog.component';
import {UserCreationDialogComponent} from './user-workspace/components/dialogs/user-creation-dialog/user-creation-dialog.component';
import {CKEditorModule} from 'ckeditor4-angular';
import {UserViewComponent} from './user-view/containers/user-view/user-view.component';
import {UserContentComponent} from './user-view/containers/panel-groups/user-content/user-content.component';
import {TeamViewComponent} from './team-view/containers/team-view/team-view.component';
import {TeamContentComponent} from './team-view/containers/panel-groups/team-content/team-content.component';
import {UserWorkspaceComponent} from './user-workspace/containers/user-workspace/user-workspace.component';
import {TeamWorkspaceComponent} from './user-workspace/containers/team-workspace/team-workspace.component';
import {ConnectionLogWorkspaceComponent} from './user-workspace/containers/connection-log-workspace/connection-log-workspace.component';
import {UserInformationPanelComponent} from './user-view/components/panels/user-information-panel/user-information-panel.component';
import {ResetPasswordDialogComponent} from './user-view/components/dialogs/reset-password-dialog/reset-password-dialog.component';
import {UserAuthorisationsPanelComponent} from './user-view/components/panels/user-authorisations-panel/user-authorisations-panel.component';
import {UserAuthorisationProfileCellComponent} from './user-view/components/cell-renderers/user-authorisation-profile-cell/user-authorisation-profile-cell.component';
import {RemoveUserAuthorisationCellComponent} from './user-view/components/cell-renderers/remove-user-authorisation-cell/remove-user-authorisation-cell.component';
import {AddUserAuthorisationDialogComponent} from './user-view/components/dialogs/add-user-authorisation-dialog/add-user-authorisation-dialog.component';
import {UserTeamsPanelComponent} from './user-view/components/panels/user-teams-panel/user-teams-panel.component';
import {RemoveUserAssociatedTeamCellComponent} from './user-view/components/cell-renderers/remove-user-associated-team-cell/remove-user-associated-team-cell.component';
import {AssociateTeamToUserDialogComponent} from './user-view/components/dialogs/associate-team-to-user-dialog/associate-team-to-user-dialog.component';
import {AdminViewHeaderModule} from '../components/admin-view-header/admin-view-header.module';
import {TeamInformationPanelComponent} from './team-view/components/panels/team-information-panel/team-information-panel.component';
import {TeamAuthorisationsPanelComponent} from './team-view/components/panels/team-authorisations-panel/team-authorisations-panel.component';
import {TeamAuthorisationProfileCellComponent} from './team-view/components/cell-renderers/team-authorisation-profile-cell/team-authorisation-profile-cell.component';
import {RemoveTeamAuthorisationCellComponent} from './team-view/components/cell-renderers/remove-team-authorisation-cell/remove-team-authorisation-cell.component';
import {AddTeamAuthorisationDialogComponent} from './team-view/components/dialogs/add-team-authorisation-dialog/add-team-authorisation-dialog.component';
import {TeamMembersPanelComponent} from './team-view/components/panels/team-member-panel/team-members-panel.component';
import {RemoveTeamMemberCellComponent} from './team-view/components/cell-renderers/remove-team-member-cell/remove-team-member-cell.component';
import {AddTeamMemberDialogComponent} from './team-view/components/dialogs/add-team-member-dialog/add-team-member-dialog.component';
import {UserViewDetailComponent} from './user-view/containers/user-view-detail/user-view-detail.component';
import {UserViewWithGridComponent} from './user-view/containers/user-view-with-grid/user-view-with-grid.component';
import {TeamViewDetailComponent} from './team-view/containers/team-view-detail/team-view-detail.component';
import {TeamViewWithGridComponent} from './team-view/containers/team-view-with-grid/team-view-with-grid.component';
import {ConnectionLogExportDialogComponent} from './user-workspace/components/dialogs/connection-log-export-dialog/connection-log-export-dialog.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'manage',
  },
  {
    path: '',
    component: MainUserWorkspaceComponent,
    children: [
      {
        path: 'manage',
        component: UserWorkspaceComponent,
        children: [
          {
            path: ':userId',
            component: UserViewWithGridComponent,
            children: [
              {
                path: '',
                redirectTo: 'content',
              },
              {
                path: 'content',
                component: UserContentComponent,
              },
            ]
          },
        ]
      },
      {
        path: 'teams',
        component: TeamWorkspaceComponent,
        children: [
          {
            path: ':teamId',
            component: TeamViewWithGridComponent,
            children: [
              {
                path: '',
                redirectTo: 'content',
              },
              {
                path: 'content',
                component: TeamContentComponent,
              },
            ]
          }
        ]
      },
      {
        path: 'history',
        component: ConnectionLogWorkspaceComponent,
      },
      {
        path: 'detail/user/:userId',
        component: UserViewDetailComponent,
        children: [
          {
            path: '',
            redirectTo: 'content',
          },
          {
            path: 'content',
            component: UserContentComponent,
          },
        ]
      },
      {
        path: 'detail/team/:teamId',
        component: TeamViewDetailComponent,
        children: [
          {
            path: '',
            redirectTo: 'content',
          },
          {
            path: 'content',
            component: TeamContentComponent,
          },
        ]
      },
    ],
  },
];

@NgModule({
  declarations: [
    MainUserWorkspaceComponent,
    UserGridComponent,
    TeamGridComponent,
    ConnectionLogGridComponent,
    UserWorkspaceAnchorsComponent,
    UserCreationDialogComponent,
    TeamCreationDialogComponent,
    UserActiveCellRendererComponent,
    UserGroupCellRendererComponent,
    DeleteUserCellRendererComponent,
    DeleteTeamCellRendererComponent,
    UserWorkspaceComponent,
    TeamWorkspaceComponent,
    ConnectionLogWorkspaceComponent,
    UserViewComponent,
    UserContentComponent,
    UserInformationPanelComponent,
    ResetPasswordDialogComponent,
    UserAuthorisationsPanelComponent,
    UserAuthorisationProfileCellComponent,
    RemoveUserAuthorisationCellComponent,
    AddUserAuthorisationDialogComponent,
    UserTeamsPanelComponent,
    RemoveUserAssociatedTeamCellComponent,
    AssociateTeamToUserDialogComponent,
    TeamViewComponent,
    TeamContentComponent,
    TeamInformationPanelComponent,
    TeamAuthorisationsPanelComponent,
    TeamAuthorisationProfileCellComponent,
    RemoveTeamAuthorisationCellComponent,
    AddTeamAuthorisationDialogComponent,
    TeamMembersPanelComponent,
    RemoveTeamMemberCellComponent,
    AddTeamMemberDialogComponent,
    UserViewDetailComponent,
    UserViewWithGridComponent,
    TeamViewDetailComponent,
    TeamViewWithGridComponent,
    ConnectionLogExportDialogComponent
  ],
  imports: [
    CommonModule,
    WorkspaceLayoutModule,
    NzDropDownModule,
    NzIconModule,
    GridModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    WorkspaceCommonModule,
    NzToolTipModule,
    AnchorModule,
    NzFormModule,
    NzDividerModule,
    DialogModule,
    CKEditorModule,
    CellRendererCommonModule,
    NavBarModule,
    AttachmentModule,
    NzBadgeModule,
    TranslateModule.forChild(),
    NzCheckboxModule,
    NzButtonModule,
    NzCollapseModule,
    UiManagerModule,
    NzSwitchModule,
    FormsModule,
    NzInputModule,
    NzSpinModule,
    NzSelectModule,
    NzTabsModule,
    AdminViewHeaderModule,
  ]
})
export class UserWorkspaceModule {
}
