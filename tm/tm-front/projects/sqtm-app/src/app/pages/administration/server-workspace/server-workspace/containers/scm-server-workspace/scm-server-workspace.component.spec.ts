import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ScmServerWorkspaceComponent } from './scm-server-workspace.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {GridService} from 'sqtm-core';

describe('ScmServerWorkspaceComponent', () => {
  let component: ScmServerWorkspaceComponent;
  let fixture: ComponentFixture<ScmServerWorkspaceComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ScmServerWorkspaceComponent ],
      imports: [HttpClientTestingModule, AppTestingUtilsModule, RouterTestingModule],
      providers: [
        {
          provide: GridService,
          useValue: jasmine.createSpyObj(['complete']),
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmServerWorkspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
