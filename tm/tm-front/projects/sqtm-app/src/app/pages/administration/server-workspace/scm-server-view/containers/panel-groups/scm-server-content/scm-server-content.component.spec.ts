import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ScmServerContentComponent } from './scm-server-content.component';
import {RestService} from 'sqtm-core';
import createSpyObj = jasmine.createSpyObj;
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateService} from '@ngx-translate/core';
import {ScmServerViewService} from '../../../services/scm-server-view.service';
import {EMPTY} from 'rxjs';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('ScmServerContentComponent', () => {
  let component: ScmServerContentComponent;
  let fixture: ComponentFixture<ScmServerContentComponent>;
  const restService = mockRestService();

  const viewService = createSpyObj(['complete', 'load']);
  viewService['componentData$'] = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [ ScmServerContentComponent ],
      providers: [
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: TranslateService,
          useValue: createSpyObj(['instant']),
        },
        {
          provide: ScmServerViewService,
          useValue: viewService,
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmServerContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
