import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ProjectExecutionPanelComponent} from './project-execution-panel.component';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DialogModule, DialogService} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {ProjectViewService} from '../../../services/project-view.service';
import {AdminProjectViewComponentData} from '../../../containers/project-view/project-view.component';
import {mockAutoConfirmDialogService} from '../../../../../../../utils/testing-utils/mocks.service';
import createSpyObj = jasmine.createSpyObj;

describe('ProjectExecutionPanelComponent', () => {
  let component: ProjectExecutionPanelComponent;
  let fixture: ComponentFixture<ProjectExecutionPanelComponent>;

  const projectViewService = createSpyObj(['changeAllowTcModifDuringExec', 'changeExecutionStatusOnProject']);
  const dialogService = mockAutoConfirmDialogService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        AppTestingUtilsModule,
        DialogModule
      ],
      providers: [
        {
          provide: ProjectViewService,
          useValue: projectViewService,
        },
        {
          provide: DialogService,
          useValue: dialogService,
        }
      ],
      declarations: [ProjectExecutionPanelComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectExecutionPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    projectViewService.changeAllowTcModifDuringExec.calls.reset();
    projectViewService.changeExecutionStatusOnProject.calls.reset();
    dialogService.openAlert.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change allowTcModifDuringExec', () => {
    component.adminProjectViewComponentData = {
      project: {
        linkedTemplateId: null,
        allowTcModifDuringExec: false,
      }
    } as unknown as AdminProjectViewComponentData;

    component.changeAllowTcModifDuringExec();

    expect(projectViewService.changeAllowTcModifDuringExec).toHaveBeenCalledWith(true);
  });

  it('should forbid changing allowTcModifDuringExec if linked to template', () => {
    component.adminProjectViewComponentData = {
      project: {
        linkedTemplateId: 159,
        allowTcModifDuringExec: false,
      }
    } as unknown as AdminProjectViewComponentData;

    component.changeAllowTcModifDuringExec();

    expect(dialogService.openAlert).toHaveBeenCalled();
    expect(projectViewService.changeAllowTcModifDuringExec).not.toHaveBeenCalled();
  });

  it('should change execution status usage', () => {
    component.adminProjectViewComponentData = {
      project: {
        linkedTemplateId: null,
        allowedStatuses: {
          STATUS: false,
        },
        statusesInUse: {
          STATUS: false,
        }
      }
    } as unknown as AdminProjectViewComponentData;

    component.changeExecutionStatusOnProject(null, 'STATUS');

    expect(projectViewService.changeExecutionStatusOnProject).toHaveBeenCalledWith(true, 'STATUS');
  });

  it('should show confirmation when changing used execution status usage', () => {
    component.adminProjectViewComponentData = {
      project: {
        linkedTemplateId: null,
        allowedStatuses: {
          STATUS: true,
        },
        statusesInUse: {
          STATUS: true,
        }
      }
    } as unknown as AdminProjectViewComponentData;

    component.changeExecutionStatusOnProject(null, 'STATUS');

    expect(dialogService.openDialog).toHaveBeenCalled();

    // TODO: check if the service should be called
    // expect(projectViewService.changeExecutionStatusOnProject).toHaveBeenCalledWith(false, 'STATUS');
  });

  it('should forbid changing execution status if linked to template', () => {
    component.adminProjectViewComponentData = {
      project: {
        linkedTemplateId: 159,
      }
    } as unknown as AdminProjectViewComponentData;

    component.changeExecutionStatusOnProject(null, 'STATUS');

    expect(dialogService.openAlert).toHaveBeenCalled();
    expect(projectViewService.changeExecutionStatusOnProject).not.toHaveBeenCalled();
  });
});
