import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UnbindCustomFieldFromProjectCellComponent } from './unbind-custom-field-from-project-cell.component';
import {
  DialogService,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {OverlayModule} from '@angular/cdk/overlay';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {ProjectViewService} from '../../../services/project-view.service';

describe('UnbindCustomFieldFromProjectCellComponent', () => {
  let component: UnbindCustomFieldFromProjectCellComponent;
  let fixture: ComponentFixture<UnbindCustomFieldFromProjectCellComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UnbindCustomFieldFromProjectCellComponent ],
      imports: [AppTestingUtilsModule, OverlayModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        },
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        {
          provide: DialogService,
          userValue: {}
        },
        {
          provide: TranslateService,
          useValue: {},
        },
        {
          provide: ProjectViewService,
          useValue: {}
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnbindCustomFieldFromProjectCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
