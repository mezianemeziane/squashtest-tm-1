import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddPermissionDialogComponent } from './add-permission-dialog.component';
import {DialogReference, RestService} from 'sqtm-core';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {ProjectViewService} from '../../../services/project-view.service';
import {of} from 'rxjs';
import {AdminProjectState} from '../../../state/admin-project-state';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('AddPermissionDialogComponent', () => {
  let component: AddPermissionDialogComponent;
  let fixture: ComponentFixture<AddPermissionDialogComponent>;
  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, ReactiveFormsModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: ProjectViewService,
          useValue: {
            componentData$: of({project: {id: -1} as AdminProjectState, type: 'project', uiState: null}),
          }
        },
        {
          provide: DialogReference,
          useValue: jasmine.createSpyObj(['close'])
        },
        TranslateService
      ],
      declarations: [ AddPermissionDialogComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    restService.get.and.returnValue(of({users: [], teams: []}));
    fixture = TestBed.createComponent(AddPermissionDialogComponent);
    component = fixture.componentInstance;
    component.data = {
      i18nPlaceholder: 'test',
      i18nPartyTypeLabel: 'test',
      permissionScope: 'users'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
