import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MilestoneContentComponent } from './milestone-content.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {DialogModule} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MilestoneViewService} from '../../../services/milestone-view.service';
import {EMPTY} from 'rxjs';

describe('MilestoneContentComponent', () => {
  let component: MilestoneContentComponent;
  let fixture: ComponentFixture<MilestoneContentComponent>;

  const viewService = jasmine.createSpyObj(['load', 'complete']);
  viewService['componentData$'] = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, DialogModule],
      declarations: [ MilestoneContentComponent ],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj('TranslateService', ['instant']),
        },
        {
          provide: MilestoneViewService,
          useValue: viewService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilestoneContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
