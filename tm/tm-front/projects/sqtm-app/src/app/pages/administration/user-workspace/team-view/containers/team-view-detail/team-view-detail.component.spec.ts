import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TeamViewDetailComponent} from './team-view-detail.component';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {of} from 'rxjs';
import {TeamViewService} from '../../services/team-view.service';
import {GenericEntityViewService, RestService} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {mockRestService} from '../../../../../../utils/testing-utils/mocks.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('TeamViewDetailComponent', () => {
  let component: TeamViewDetailComponent;
  let fixture: ComponentFixture<TeamViewDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [TeamViewDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({
              teamId: '123',
            })),
          }
        },
        {
          provide: GenericEntityViewService,
          useValue: {},
        },
        {
          provide: TeamViewService,
          useValue: {},
        },
        {
          provide: RestService,
          useValue: mockRestService(),
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamViewDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
