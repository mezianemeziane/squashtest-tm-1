import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {UserViewService} from '../../services/user-view.service';
import {map, takeUntil} from 'rxjs/operators';
import {AdminUserViewComponentData} from '../user-view/user-view.component';
import {GenericEntityViewService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-user-view-detail',
  templateUrl: './user-view-detail.component.html',
  styleUrls: ['./user-view-detail.component.less'],
  providers: [
    {
      provide: UserViewService,
      useClass: UserViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: UserViewService
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserViewDetailComponent implements OnInit, OnDestroy {

  public readonly componentData$: Observable<AdminUserViewComponentData>;
  private unsub$ = new Subject<void>();

  constructor(private route: ActivatedRoute,
              private userViewService: UserViewService) {
    this.componentData$ = this.userViewService.componentData$;
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('userId')),
      ).subscribe((id) => {
      this.userViewService.load(parseInt(id, 10));
    });
  }

  ngOnDestroy(): void {
    this.userViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  back(): void {
    history.back();
  }
}
