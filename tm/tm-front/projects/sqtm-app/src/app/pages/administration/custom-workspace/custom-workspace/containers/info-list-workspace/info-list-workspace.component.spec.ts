import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InfoListWorkspaceComponent } from './info-list-workspace.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('InfoListWorkspaceComponent', () => {
  let component: InfoListWorkspaceComponent;
  let fixture: ComponentFixture<InfoListWorkspaceComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoListWorkspaceComponent ],
      imports: [HttpClientTestingModule, AppTestingUtilsModule, RouterTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoListWorkspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
