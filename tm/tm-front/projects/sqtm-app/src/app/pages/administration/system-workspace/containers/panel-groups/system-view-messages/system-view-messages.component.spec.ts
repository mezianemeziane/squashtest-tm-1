import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SystemViewMessagesComponent } from './system-view-messages.component';
import {SystemViewService} from '../../../services/system-view.service';
import {EMPTY} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('SystemViewMessagesComponent', () => {
  let component: SystemViewMessagesComponent;
  let fixture: ComponentFixture<SystemViewMessagesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemViewMessagesComponent ],
      providers: [
        {
          provide: SystemViewService,
          useValue: {
            componentData$: EMPTY,
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemViewMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
