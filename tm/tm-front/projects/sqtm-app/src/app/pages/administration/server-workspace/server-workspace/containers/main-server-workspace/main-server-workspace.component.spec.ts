import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MainServerWorkspaceComponent } from './main-server-workspace.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('MainServerWorkspaceComponent', () => {
  let component: MainServerWorkspaceComponent;
  let fixture: ComponentFixture<MainServerWorkspaceComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MainServerWorkspaceComponent ],
      imports: [HttpClientTestingModule, AppTestingUtilsModule, RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainServerWorkspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
