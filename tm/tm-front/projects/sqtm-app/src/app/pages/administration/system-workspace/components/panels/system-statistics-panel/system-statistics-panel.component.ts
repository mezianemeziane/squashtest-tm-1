import {Component, OnInit, ChangeDetectionStrategy, Input} from '@angular/core';
import {SystemViewState} from '../../../states/system-view.state';

@Component({
  selector: 'sqtm-app-system-statistics-panel',
  templateUrl: './system-statistics-panel.component.html',
  styleUrls: [
    './system-statistics-panel.component.less',
    '../../../styles/system-workspace.common.less'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemStatisticsPanelComponent implements OnInit {

  @Input() componentData: SystemViewState;

  fields: StatisticField[];

  constructor() {
    this.fields = [
      makeField('projectsNumber', 'sqtm-core.entity.project.label.plural'),
      makeField('usersNumber', 'sqtm-core.entity.user.label.plural'),
      makeField('requirementsNumber', 'sqtm-core.entity.requirement.label.plural'),
      makeField('testCasesNumber', 'sqtm-core.entity.test-case.label.plural'),
      makeField('campaignsNumber', 'sqtm-core.entity.campaign.label.plural'),
      makeField('iterationsNumber', 'sqtm-core.entity.iteration.label.plural'),
      makeField('executionsNumber', 'sqtm-core.entity.execution.label.plural'),
      makeField('databaseSize',
        'sqtm-core.administration-workspace.system.information.statistics.database.label', ' Mo'),
    ];
  }

  ngOnInit(): void {
  }

}

interface StatisticField {
  i18nKey: string;
  name: string;
  suffix?: string;
}

function makeField(name: string, i18nKey: string, suffix = ''): StatisticField {
  return {
    name, i18nKey, suffix,
  };
}
