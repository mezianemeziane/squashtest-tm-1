import {ChangeDetectionStrategy, Component, InjectionToken, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {
  DataRow,
  deleteColumn,
  DialogService,
  Extendable,
  GridDefinition,
  GridFilter,
  GridService,
  indexColumn,
  smallGrid,
  StyleDefinitionBuilder
} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {concatMap, filter, finalize, map, take, takeUntil, tap} from 'rxjs/operators';
import {createSelector, select} from '@ngrx/store';
import {AdminTeamViewComponentData} from '../../../containers/team-view/team-view.component';
import {getTeamViewState, TeamViewService} from '../../../services/team-view.service';
import {AdminTeamState} from '../../../states/admin-team-state';
import {RemoveTeamMemberCellComponent} from '../../cell-renderers/remove-team-member-cell/remove-team-member-cell.component';
import {AddTeamMemberDialogComponent} from '../../dialogs/add-team-member-dialog/add-team-member-dialog.component';
import {clickableMemberColumn} from '../../../../../cell-renderer.builders';

export const TEAM_MEMBERS_TABLE_CONF = new InjectionToken('USER_AUTHORISATIONS_TABLE_CONF');
export const TEAM_MEMBERS_TABLE = new InjectionToken('USER_AUTHORISATIONS_TABLE');

export function teamMembersTableDefinition(): GridDefinition {
  return smallGrid('team-members')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      clickableMemberColumn('fullName')
        .withI18nKey('sqtm-core.entity.user.label.singular')
        .changeWidthCalculationStrategy(new Extendable(200, 0.5)),
      deleteColumn(RemoveTeamMemberCellComponent),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .enableMultipleColumnsFiltering(['fullName'])
    .build();
}

@Component({
  selector: 'sqtm-app-team-members-panel',
  template: `
    <div class="m-t-10 m-l-10 m-b-15 flex-fixed-size" style="width: 200px">
        <sqtm-core-text-research-field (newResearchValue)="handleResearchInput($event)"></sqtm-core-text-research-field>
    </div>
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./team-members-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: TEAM_MEMBERS_TABLE
    }
  ]
})
export class TeamMembersPanelComponent implements OnInit, OnDestroy {

  private componentData$: Observable<AdminTeamViewComponentData>;

  private unsub$ = new Subject<void>();

  constructor(private teamViewService: TeamViewService,
              public gridService: GridService,
              private dialogService: DialogService,
              private vcRef: ViewContainerRef) {
  }

  ngOnInit(): void {
    this.componentData$ = this.teamViewService.componentData$;
    this.initializeTable();
    this.initializeFilters();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  refreshGrid() {
    this.gridService.refreshData();
  }

  openAddTeamMemberDialog() {
    const dialogRef = this.dialogService.openDialog({
      id: 'add-team-member-dialog',
      component: AddTeamMemberDialogComponent,
      viewContainerReference: this.vcRef,
      width: 720,
      top: '100px'
    });

    dialogRef.dialogClosed$.subscribe((result) => {
      if (result) {
        this.refreshGrid();
      }
    });
  }

  removeTeamMembers() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) => rows.map(row => row.data.partyId)),
      concatMap((partyIds: number[]) => this.showConfirmDeleteTeamMemberDialog(partyIds)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      concatMap(({partyIds}) => this.teamViewService.removeTeamMember(partyIds)),
      finalize(() => this.gridService.completeAsyncOperation())
    ).subscribe();
  }

  private showConfirmDeleteTeamMemberDialog(partyIds): Observable<{ confirmDelete: boolean, partyIds: number[] }> {

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.teams.dialog.title.remove-member.remove-many',
      messageKey: 'sqtm-core.administration-workspace.teams.dialog.message.remove-member.remove-many',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, partyIds}))
    );
  }


  private initializeFilters() {
    const filters: GridFilter[] = [{
      id: 'fullName', active: false, initialValue: {kind: 'single-string-value', value: ''}, tiedToPerimeter: false
    }];
    this.gridService.addFilters(filters);
  }

  handleResearchInput($event: string) {
    this.gridService.applyMultiColumnsFilter($event);
  }

  private initializeTable() {
    const teamsTable = this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createSelector(getTeamViewState, (teamState: AdminTeamState) => teamState.members)),
    );

    this.gridService.connectToDatasource(teamsTable, 'partyId');
  }
}
