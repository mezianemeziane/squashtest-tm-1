import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProjectPluginEnabledCellComponent } from './project-plugin-enabled-cell.component';
import {
  DialogService,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {ProjectViewService} from '../../../services/project-view.service';
import {mockAutoConfirmDialogService} from '../../../../../../../utils/testing-utils/mocks.service';
import {EMPTY} from 'rxjs';

describe('ProjectPluginEnabledCellComponent', () => {
  let component: ProjectPluginEnabledCellComponent;
  let fixture: ComponentFixture<ProjectPluginEnabledCellComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectPluginEnabledCellComponent ],
      imports: [AppTestingUtilsModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        },
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        TranslateService,
        {
          provide: ProjectViewService,
          useValue: { componentData$: EMPTY }
        },
        {
          provide: DialogService,
          useValue: mockAutoConfirmDialogService(),
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectPluginEnabledCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
