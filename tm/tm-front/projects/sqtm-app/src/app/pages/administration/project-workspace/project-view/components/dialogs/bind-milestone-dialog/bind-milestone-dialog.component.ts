import {Component, OnInit, ChangeDetectionStrategy} from '@angular/core';
import {createStore, DialogReference, Milestone, RestService} from 'sqtm-core';
import {BindMilestoneDialogConfiguration} from './bind-milestone-dialog.configuration';
import {Observable} from 'rxjs';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-bind-milestone-dialog',
  templateUrl: './bind-milestone-dialog.component.html',
  styleUrls: ['./bind-milestone-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BindMilestoneDialogComponent implements OnInit {

  data: BindMilestoneDialogConfiguration;
  readonly componentData$: Observable<AvailableMilestoneState>;
  private store = createStore<AvailableMilestoneState>({...initialState});
  private selectedMilestones = new Set<Milestone>();

  constructor(private dialogReference: DialogReference<BindMilestoneDialogConfiguration, Milestone[]>,
              private restService: RestService) {
    this.data = dialogReference.data;
    this.componentData$ = this.store.state$.pipe(
      filter(state => state.loaded)
    );
  }

  ngOnInit(): void {
    this.loadAvailableMilestones();
  }

  addSelectedMilestone(milestones: Milestone[]) {
    this.selectedMilestones.clear();
    milestones
      .forEach(milestone => this.selectedMilestones.add(milestone));
  }

  confirm() {
    this.dialogReference.result = Array.from(this.selectedMilestones);
    this.dialogReference.close();
  }

  private loadAvailableMilestones() {
    this.restService.get<AvailableMilestoneModel>(['project-view', this.data.projectId.toString(), 'available-milestones'])
      .subscribe(model => this.store.commit({...model, loaded: true}));
  }
}

interface AvailableMilestoneState {
  globalMilestones: Milestone[];
  personalMilestones: Milestone[];
  otherMilestones: Milestone[];
  loaded: boolean;
}

const initialState: Readonly<AvailableMilestoneState> = {
  globalMilestones: [],
  otherMilestones: [],
  personalMilestones: [],
  loaded: false
};

type AvailableMilestoneModel = AvailableMilestoneState;
