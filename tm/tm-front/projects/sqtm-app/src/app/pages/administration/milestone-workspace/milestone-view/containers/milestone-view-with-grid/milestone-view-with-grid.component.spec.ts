import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {MilestoneViewWithGridComponent} from './milestone-view-with-grid.component';
import {GridService, WorkspaceCommonModule} from 'sqtm-core';
import {mockGridService} from '../../../../../../utils/testing-utils/mocks.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('MilestoneViewWithGridComponent', () => {
  let component: MilestoneViewWithGridComponent;
  let fixture: ComponentFixture<MilestoneViewWithGridComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [WorkspaceCommonModule],
      declarations: [MilestoneViewWithGridComponent],
      providers: [
        {
          provide: GridService,
          useValue: mockGridService(),
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilestoneViewWithGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
