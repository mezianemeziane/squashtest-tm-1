import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {
  DialogService,
  GenericEntityViewComponentData,
  GenericEntityViewService,
  GridService,
  RestService
} from 'sqtm-core';
import {CustomFieldViewService} from '../../services/custom-field-view.service';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {concatMap, filter, map, take, takeUntil} from 'rxjs/operators';
import {AdminCustomFieldViewState} from '../../states/admin-custom-field-view-state';
import {AdminCustomFieldState} from '../../states/admin-custom-field-state';

@Component({
  selector: 'sqtm-app-custom-field-view',
  templateUrl: './custom-field-view.component.html',
  styleUrls: ['./custom-field-view.component.less'],
  providers: [
    {
      provide: CustomFieldViewService,
      useClass: CustomFieldViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: CustomFieldViewService
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomFieldViewComponent implements OnInit, OnDestroy {


  componentData$: Observable<AdminCustomFieldViewState>;

  private unsub$ = new Subject<void>();

  constructor(private route: ActivatedRoute,
              private readonly customFieldViewService: CustomFieldViewService,
              private cdRef: ChangeDetectorRef,
              private restService: RestService,
              private gridService: GridService,
              private dialogService: DialogService) {
    this.componentData$ = this.customFieldViewService.componentData$;

    this.prepareGridRefreshOnEntityChanges();
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('customFieldId')),
      ).subscribe((id) => {
      this.customFieldViewService.load(parseInt(id, 10));
    });
  }

  ngOnDestroy(): void {
    this.customFieldViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  handleDelete(): void {
    this.componentData$.pipe(
      take(1),
      concatMap((componentData: AdminCustomFieldViewComponentData) => this.showConfirmDeleteCufDialog(componentData.customField.id)),
      filter(({confirmDelete}) => confirmDelete),
      concatMap(({cufId}) => this.deleteCustomFieldServerSide(cufId)),
    ).subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteCufDialog(cufId): Observable<{ confirmDelete: boolean, cufId: string }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.custom-fields.dialog.title.delete-one',
      messageKey: 'sqtm-core.administration-workspace.custom-fields.dialog.message.delete-one',
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, cufId}))
    );
  }

  private deleteCustomFieldServerSide(cufId): Observable<void> {
    return this.restService.delete(['custom-fields', cufId]);
  }

  private prepareGridRefreshOnEntityChanges(): void {
    this.customFieldViewService.simpleAttributeRequiringRefresh = [
      'name',
      'label',
      'code',
      'optional'];

    this.customFieldViewService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
    ).subscribe(() =>
      this.gridService.refreshDataAndKeepSelectedRows());
  }
}


export interface AdminCustomFieldViewComponentData extends GenericEntityViewComponentData<AdminCustomFieldState, 'customField'> {
}

