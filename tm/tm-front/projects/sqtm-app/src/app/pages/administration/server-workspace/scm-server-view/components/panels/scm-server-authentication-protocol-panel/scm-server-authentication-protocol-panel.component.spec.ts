import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationPolicy, AuthenticationProtocol } from 'sqtm-core';
import { AppTestingUtilsModule } from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import { ScmServerViewService } from '../../../services/scm-server-view.service';
import { AdminScmServerState } from '../../../states/admin-scm-server-state';
import { AdminScmServerViewState } from '../../../states/admin-scm-server-view-state';

import { ScmServerAuthenticationProtocolPanelComponent } from './scm-server-authentication-protocol-panel.component';
import SpyObj = jasmine.SpyObj;

describe('ScmServerAuthenticationProtocolPanelComponent', () => {
  let component: ScmServerAuthenticationProtocolPanelComponent;
  let fixture: ComponentFixture<ScmServerAuthenticationProtocolPanelComponent>;
  let viewService: SpyObj<ScmServerViewService>;

  beforeEach(waitForAsync(() => {
    viewService = jasmine.createSpyObj(['load', 'setAuthenticationProtocol']);

    TestBed.configureTestingModule({
      declarations: [ ScmServerAuthenticationProtocolPanelComponent ],
      imports: [AppTestingUtilsModule, FormsModule, ReactiveFormsModule],
      providers: [
      {
        provide: TranslateService,
        useValue: jasmine.createSpyObj(['instant']),
      },
      {
        provide: ScmServerViewService,
        useValue: viewService,
      }
    ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmServerAuthenticationProtocolPanelComponent);
    component = fixture.componentInstance;
    component.componentData = makeComponentData(makeScmServer());
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

function makeComponentData(scmServer: AdminScmServerState): AdminScmServerViewState {
  return {
    scmServer,
  } as AdminScmServerViewState;
}

function makeScmServer(): AdminScmServerState {
  return {
    id: 1,
    serverId: 1,
    kind: 'git',
    name: 'name',
    url: 'http://url.com',
    committerMail: '',
    repositories: [],
    authPolicy: AuthenticationPolicy.APP_LEVEL,
    authProtocol: AuthenticationProtocol.BASIC_AUTH,
    supportedAuthenticationProtocols: [AuthenticationProtocol.BASIC_AUTH, AuthenticationProtocol.TOKEN_AUTH],
    credentials: {
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      type: AuthenticationProtocol.BASIC_AUTH,
      username: 'a',
      password: 'b',
    },
    attachmentList: {id: null, attachments: null},
  };
}
