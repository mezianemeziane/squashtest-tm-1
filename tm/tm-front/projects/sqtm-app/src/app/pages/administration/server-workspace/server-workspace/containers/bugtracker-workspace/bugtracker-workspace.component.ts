import {Component, OnInit, ChangeDetectionStrategy, OnDestroy} from '@angular/core';
import {ADMIN_WS_BUGTRACKERS_TABLE, ADMIN_WS_BUGTRACKERS_TABLE_CONFIG} from '../../../server-workspace.constant';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {adminBugtrackersTableDefinition} from '../bugtracker-grid/bugtracker-grid.component';
import {Observable} from 'rxjs';


@Component({
  selector: 'sqtm-app-bugtracker-workspace',
  templateUrl: './bugtracker-workspace.component.html',
  styleUrls: ['./bugtracker-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ADMIN_WS_BUGTRACKERS_TABLE_CONFIG,
      useFactory: adminBugtrackersTableDefinition,
      deps: []
    },
    {
      provide: ADMIN_WS_BUGTRACKERS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ADMIN_WS_BUGTRACKERS_TABLE_CONFIG, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: ADMIN_WS_BUGTRACKERS_TABLE
    }
  ]
})
export class BugtrackerWorkspaceComponent implements OnInit, OnDestroy {

  authenticatedAdmin$: Observable<AuthenticatedUser>;

  constructor(public readonly adminReferentialDataService: AdminReferentialDataService,
              private gridService: GridService) {
  }

  ngOnInit(): void {
    this.authenticatedAdmin$ = this.adminReferentialDataService.authenticatedUser$;
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }

}
