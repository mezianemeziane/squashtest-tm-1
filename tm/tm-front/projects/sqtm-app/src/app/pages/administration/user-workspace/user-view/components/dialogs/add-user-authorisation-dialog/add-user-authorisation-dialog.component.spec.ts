import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddUserAuthorisationDialogComponent } from './add-user-authorisation-dialog.component';
import {
  DialogReference, grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {of} from 'rxjs';
import {UserViewService} from '../../../services/user-view.service';
import {AdminUserState} from '../../../states/admin-user-state';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('AddUserAuthorisationDialogComponent', () => {
  let component: AddUserAuthorisationDialogComponent;
  let fixture: ComponentFixture<AddUserAuthorisationDialogComponent>;
  const restService = mockRestService();
  const gridConfig = grid('grid-test').build();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUserAuthorisationDialogComponent ],
      imports: [AppTestingUtilsModule, ReactiveFormsModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: UserViewService,
          useValue: {
            componentData$: of({user: {id: -1} as AdminUserState, type: 'user', uiState: null}),
          }
        },
        {
          provide: DialogReference,
          useValue: jasmine.createSpyObj(['close'])
        },
        TranslateService
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    restService.get.and.returnValue(of([]));
    fixture = TestBed.createComponent(AddUserAuthorisationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
