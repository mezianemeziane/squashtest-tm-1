import {ChangeDetectionStrategy, Component} from '@angular/core';
import {DialogReference} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-deactivate-plugin-dialog',
  templateUrl: './deactivate-plugin-dialog.component.html',
  styleUrls: ['./deactivate-plugin-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeactivatePluginDialogComponent {

  configuration: DeactivatePluginDialogConfiguration;

  saveConf = true;

  constructor(private dialogReference: DialogReference<DeactivatePluginDialogConfiguration>) {
    this.configuration = dialogReference.data;
  }

  handleConfirm(): void {
    this.dialogReference.result = {
      confirm: true,
      saveConf: this.saveConf,
    };
    this.dialogReference.close();
  }
}

export interface DeactivatePluginDialogConfiguration {
  id: string;
  titleKey: string;
  pluginName: string;
  helpMessage: string;
}
