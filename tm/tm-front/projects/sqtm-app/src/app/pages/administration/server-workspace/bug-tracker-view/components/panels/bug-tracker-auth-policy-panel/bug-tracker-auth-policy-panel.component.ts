import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, ViewChild} from '@angular/core';
import {AdminBugTrackerViewState} from '../../../states/admin-bug-tracker-view-state';
import {FormBuilder} from '@angular/forms';
import {
  AuthenticationPolicy,
  Credentials,
  doesHttpErrorContainsSquashActionError,
  extractSquashActionError,
  ListItem,
  ThirdPartyCredentialsFormComponent
} from 'sqtm-core';
import {BugTrackerViewService} from '../../../services/bug-tracker-view.service';
import {TranslateService} from '@ngx-translate/core';

const TRANSLATE_KEYS_BASE = 'sqtm-core.administration-workspace.bugtrackers.authentication-policy.';

@Component({
  selector: 'sqtm-app-bug-tracker-authentication-policy-panel',
  templateUrl: './bug-tracker-auth-policy-panel.component.html',
  styleUrls: ['./bug-tracker-auth-policy-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BugTrackerAuthPolicyPanelComponent {
  @Input()
  componentData: AdminBugTrackerViewState;

  @ViewChild(ThirdPartyCredentialsFormComponent)
  credentialsForm: ThirdPartyCredentialsFormComponent;
  credentialsStatusMessage = '';
  statusIcon: 'DANGER' | 'INFO' = null;

  translateKeys = {
    userAuthenticationLabel: TRANSLATE_KEYS_BASE + 'user-authentication.label',
    userAuthenticationTooltip: TRANSLATE_KEYS_BASE + 'user-authentication.tooltip',
    appPolicy: TRANSLATE_KEYS_BASE + 'user-authentication.app-policy',
    usersPolicy: TRANSLATE_KEYS_BASE + 'user-authentication.users-policy',

    credentialsLabel: TRANSLATE_KEYS_BASE + 'credentials.label',
    credentialsTooltip: TRANSLATE_KEYS_BASE + 'credentials.tooltip',

    saveSuccessKey: TRANSLATE_KEYS_BASE + 'credentials.save-success',
    saveFailKey: TRANSLATE_KEYS_BASE + 'credentials.bad-credentials',
    connectionRefused: TRANSLATE_KEYS_BASE + 'credentials.connection-refused',
  };

  get radioItems(): ListItem[] {
    return [
      {
        id: AuthenticationPolicy.USER,
        i18nLabelKey: this.translateKeys.usersPolicy,
        selected: this.componentData.bugTracker.authPolicy === AuthenticationPolicy.USER,
      },
      {
        id: AuthenticationPolicy.APP_LEVEL,
        i18nLabelKey: this.translateKeys.appPolicy,
        selected: this.componentData.bugTracker.authPolicy === AuthenticationPolicy.APP_LEVEL,
      }
    ];
  }

  constructor(private readonly formBuilder: FormBuilder,
              public readonly bugTrackerViewService: BugTrackerViewService,
              private readonly cdRef: ChangeDetectorRef,
              private readonly translateService: TranslateService) {
    this.observeAuthProtocolChange();
  }

  private observeAuthProtocolChange(): void {
    this.bugTrackerViewService.externalRefreshRequired$.subscribe(update => {
      if (update.key === 'authProtocol') {
        this.credentialsForm.authenticationProtocol = update.value;
        this.clearMessage();
      }
    });
  }

  sendCredentialsForm(credentials: Credentials) {
    this.clearMessage();
    this.bugTrackerViewService.setCredentials(credentials)
      .subscribe(
      () => this.handleCredentialsSaveSuccess(),
      (err) => this.handleCredentialsError(err),
    );
  }

  private clearMessage(): void {
    this.credentialsStatusMessage = undefined;
    this.statusIcon = undefined;
  }

  private handleCredentialsSaveSuccess(): void {
    this.credentialsStatusMessage = this.translateKeys.saveSuccessKey;
    this.credentialsForm.formGroup.markAsPristine();
    this.credentialsForm.endAsync();
    this.statusIcon = 'INFO';
  }

  private handleCredentialsError(error: any): void {
    this.credentialsForm.endAsync();

    if (doesHttpErrorContainsSquashActionError(error)) {
      const squashError = extractSquashActionError(error);
      this.credentialsStatusMessage = this.translateService.instant(squashError.actionValidationError.i18nKey);
      this.cdRef.markForCheck();
      this.statusIcon = 'DANGER';
    } else {
      // Default error handling
      console.error(error);
    }
  }

  handleAuthPolicyChange($event: ListItem): void {
    const policy = $event.id.toString();
    if (policy === AuthenticationPolicy.APP_LEVEL || policy === AuthenticationPolicy.USER) {
      this.bugTrackerViewService.setAuthPolicy(policy).subscribe();
    } else {
      throw new Error('Unhandled authentication policy : ' + policy);
    }
  }
}
