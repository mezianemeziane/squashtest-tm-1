import {Component, ChangeDetectionStrategy, Input} from '@angular/core';
import {AdminTeamViewComponentData} from '../../../containers/team-view/team-view.component';


@Component({
  selector: 'sqtm-app-team-information-panel',
  templateUrl: './team-information-panel.component.html',
  styleUrls: ['./team-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TeamInformationPanelComponent {

  @Input()
  adminTeamViewComponentData: AdminTeamViewComponentData;

  constructor() {
  }
}
