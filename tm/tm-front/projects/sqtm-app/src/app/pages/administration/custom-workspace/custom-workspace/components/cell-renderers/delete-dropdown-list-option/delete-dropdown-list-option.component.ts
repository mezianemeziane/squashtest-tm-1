import {Component, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  DialogService,
  GridService
} from 'sqtm-core';
import {DropdownListOptionService} from '../../../services/dropdown-list-option.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-dropdown-list-option-cell-renderer',
  template: `
    <sqtm-core-delete-icon (delete)="doDelete()"></sqtm-core-delete-icon>
  `,
  styleUrls: ['./delete-dropdown-list-option.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteDropdownListOptionComponent extends AbstractDeleteCellRenderer {

  constructor(public grid: GridService,
              public cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              private dropdownListOptionService: DropdownListOptionService) {
    super(grid, cdr, dialogService);
  }

  doDelete() {
    this.grid.beginAsyncOperation();
    const optionName = this.row.data.optionName;
    this.dropdownListOptionService.removeOption(optionName).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe(() => this.grid.refreshData());
  }
}

