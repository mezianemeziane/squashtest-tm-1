import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, ViewContainerRef} from '@angular/core';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  basicExternalLinkColumn,
  DataRow,
  DialogService,
  DisplayOption,
  Extendable,
  FilterOperation,
  Fixed,
  grid,
  GridDefinition,
  GridService,
  indexColumn,
  RestService,
  selectableTextColumn,
  textColumn,
  WorkspaceWithGridComponent
} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {concatMap, filter, map, take, takeUntil, tap} from 'rxjs/operators';
import {deleteBugTrackerColumn} from '../../components/cell-renderers/delete-bugtracker/delete-bugtracker.component';
import {BugtrackerCreationDialogComponent} from '../../components/dialogs/bugtracker-creation-dialog/bugtracker-creation-dialog.component';

export function adminBugtrackersTableDefinition(): GridDefinition {
  return grid('bugtrackers')
    .withColumns([
      indexColumn()
        .changeWidthCalculationStrategy(new Fixed(60))
        .withViewport('leftViewport'),
      selectableTextColumn('name')
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(120, 0.2)),
      textColumn('kind')
        .withI18nKey('sqtm-core.entity.bugtracker.kind.label')
        .changeWidthCalculationStrategy(new Extendable(120, 0.2)),
      basicExternalLinkColumn('url')
        .withI18nKey('sqtm-core.entity.generic.url.label')
        .changeWidthCalculationStrategy(new Extendable(300, 0.1)),
      deleteBugTrackerColumn('delete')
    ]).server().withServerUrl(['bugtrackers'])
    .disableRightToolBar()
    .withRowHeight(35)
    .enableMultipleColumnsFiltering(['name'])
    .build();
}

@Component({
  selector: 'sqtm-app-bugtracker-grid',
  templateUrl: './bugtracker-grid.component.html',
  styleUrls: ['./bugtracker-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BugtrackerGridComponent implements AfterViewInit, OnDestroy {

  authenticatedUser$: Observable<AuthenticatedUser>;

  unsub$ = new Subject<void>();

  bugtrackerKinds: DisplayOption[];

  protected readonly entityIdPositionInUrl = 3;

  constructor(public gridService: GridService,
              private restService: RestService,
              private dialogService: DialogService,
              private adminReferentialDataService: AdminReferentialDataService,
              private viewContainerRef: ViewContainerRef,
              public workspaceWithGrid: WorkspaceWithGridComponent) {
    this.workspaceWithGrid.entityIdPositionInUrl = this.entityIdPositionInUrl;
    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
  }

  ngAfterViewInit() {
    this.getBugtrackerKinds();
    this.addFilters();
    this.gridService.refreshData();
  }

  private addFilters() {
    this.gridService.addFilters([{
      id: 'name',
      active: false,
      initialValue: {kind: 'single-string-value', value: ''},
      tiedToPerimeter: false,
      operation: FilterOperation.LIKE
    }]);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openBugtrackerDialog() {
    const dialogReference = this.dialogService.openDialog({
      component: BugtrackerCreationDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        titleKey: 'sqtm-core.administration-workspace.servers.bugtrackers.dialog.title.new-bugtracker',
        bugtrackerKinds: this.bugtrackerKinds
      },
      id: 'bugtracker-dialog',
      width: 600
    });

    dialogReference.dialogResultChanged$.pipe(
      takeUntil(dialogReference.dialogClosed$),
      filter(result => result != null)
    ).subscribe(() => {
      this.gridService.refreshData();
    });

  }

  getBugtrackerKinds() {
    this.restService.get<{ bugtrackerKinds: string[] }>(['bugtracker/get-bugtracker-kinds'])
      .subscribe(response => {
          this.bugtrackerKinds = response.bugtrackerKinds.map(bugtrackerKind => {
            return {id: bugtrackerKind, label: bugtrackerKind};
          });
        }
      );
  }

  deleteBugtrackers($event: MouseEvent) {
    $event.stopPropagation();

    this.showBugtrackerIsUsedBySynchronisationAlertIfRequired();

    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0 && !this.isUsedBySynchronization(rows)),
      concatMap((rows: DataRow[]) => this.showConfirmDeleteBugtrackerDialog(rows)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      concatMap(({rows}) => this.deleteBugtrackersServerSide(rows)),
      tap(() => this.gridService.completeAsyncOperation())
    ).subscribe(() => this.gridService.refreshData());
  }

  private showBugtrackerIsUsedBySynchronisationAlertIfRequired() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0 && this.isUsedBySynchronization(rows)),
    ).subscribe(() => this.openBugtrackerIsUsedBySynchronisationAlert());
  }

  private openBugtrackerIsUsedBySynchronisationAlert() {
    this.dialogService.openAlert({
      titleKey: 'sqtm-core.administration-workspace.servers.bugtrackers.dialog.title.delete-many-used-by-synchronization',
      messageKey: 'sqtm-core.administration-workspace.servers.bugtrackers.dialog.message.delete-many-used-by-synchronization',
      level: 'WARNING'
    });
  }

  private isUsedBySynchronization(rows: DataRow[]) {
    return rows.map(row => row.data['synchronisationCount'] > 0).includes(true);
  }

  private showConfirmDeleteBugtrackerDialog(rows): Observable<{ confirmDelete: boolean, rows: string[] }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.servers.bugtrackers.dialog.title.delete-many',
      messageKey: 'sqtm-core.administration-workspace.servers.bugtrackers.dialog.message.delete-many',
      level: 'DANGER'
    });
    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, rows}))
    );
  }

  private deleteBugtrackersServerSide(rows): Observable<void> {
    const pathVariable = rows.map(row => row.id).join(',');
    return this.restService.delete([`bugtracker`, pathVariable]);
  }

  filterBugTrackerName($event: any) {
    this.gridService.applyMultiColumnsFilter($event);
  }
}
