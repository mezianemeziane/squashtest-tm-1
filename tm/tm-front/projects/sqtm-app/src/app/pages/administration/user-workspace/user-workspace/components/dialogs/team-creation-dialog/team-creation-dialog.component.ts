import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  CreationDialogData,
  DialogReference,
  FieldValidationError,
  RestService,
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {AbstractAdministrationCreationDialogDirective} from '../../../../../components/abstract-administration-creation-dialog';
import {of} from 'rxjs';

@Component({
  selector: 'sqtm-app-team-creation-dialog',
  templateUrl: './team-creation-dialog.component.html',
  styleUrls: ['./team-creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TeamCreationDialogComponent extends AbstractAdministrationCreationDialogDirective implements OnInit {

  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: CreationDialogData;

  constructor(private fb: FormBuilder,
              private translateService: TranslateService,
              dialogReference: DialogReference,
              restService: RestService,
              cdr: ChangeDetectorRef) {
    super ('teams/new', dialogReference, restService, cdr);
  }

  get textFieldToFocus(): string {
    return 'name';
  }

  ngOnInit() {
    this.initializeFormGroup();
  }

  protected getRequestPayload() {
    return of({
      name: this.getFormControlValue('name'),
      description: this.getFormControlValue('description')
    });
  }

  protected doResetForm() {
    this.resetFormControl('name', '');
    this.resetFormControl('description', '');
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern('(.|\\s)*\\S(.|\\s)*'),
        Validators.maxLength(50)
      ]),
      description: this.fb.control(''),
    });
  }

}
