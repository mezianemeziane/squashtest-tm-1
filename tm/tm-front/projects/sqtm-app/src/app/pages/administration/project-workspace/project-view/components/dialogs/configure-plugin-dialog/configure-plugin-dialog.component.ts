import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {DialogReference} from 'sqtm-core';
import {ConfigurePluginDialogConfiguration} from './configure-plugin-dialog-configuration';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {APP_BASE_HREF} from '@angular/common';

@Component({
  selector: 'sqtm-app-configure-plugin-dialog',
  templateUrl: './configure-plugin-dialog.component.html',
  styleUrls: ['./configure-plugin-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfigurePluginDialogComponent implements OnInit {

  data: ConfigurePluginDialogConfiguration;
  pluginUrl: SafeUrl;

  constructor(private dialogReference: DialogReference<ConfigurePluginDialogConfiguration>,
              private sanitizer: DomSanitizer,
              @Inject(APP_BASE_HREF) private serverServletContext: string) {
    this.data = this.dialogReference.data;
    this.pluginUrl = sanitizer.bypassSecurityTrustResourceUrl(`${serverServletContext}${this.data.url}`);
  }

  ngOnInit(): void {
  }

}
