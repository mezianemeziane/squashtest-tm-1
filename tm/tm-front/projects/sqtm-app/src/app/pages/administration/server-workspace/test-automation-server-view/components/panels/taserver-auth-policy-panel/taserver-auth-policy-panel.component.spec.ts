import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TAServerAuthPolicyPanelComponent} from './taserver-auth-policy-panel.component';
import {ReactiveFormsModule} from '@angular/forms';
import {TestAutomationServerViewService} from '../../../services/test-automation-server-view.service';
import {TranslateService} from '@ngx-translate/core';
import {ActionValidationError, AuthenticationProtocol, GenericEntityViewService} from 'sqtm-core';
import {EMPTY, of, throwError} from 'rxjs';
import {makeHttpActionError} from '../../../../../../../utils/testing-utils/test-error-response-generator';
import {mockCredentialsForm} from '../../../../../../../utils/testing-utils/test-component-generator';
import createSpyObj = jasmine.createSpyObj;
import {mockPassThroughTranslateService} from '../../../../../../../utils/testing-utils/mocks.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import SpyObj = jasmine.SpyObj;

describe('TAServerAuthPolicyPanelComponent', () => {
  let component: TAServerAuthPolicyPanelComponent;
  let fixture: ComponentFixture<TAServerAuthPolicyPanelComponent>;
  let viewService: SpyObj<TestAutomationServerViewService>;

  beforeEach(waitForAsync(() => {
    viewService = createSpyObj(['setCredentials']);
    viewService['externalRefreshRequired$'] = EMPTY;

    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [TAServerAuthPolicyPanelComponent],
      providers: [
        {
          provide: TestAutomationServerViewService,
          useValue: viewService,
        },
        {
          provide: GenericEntityViewService,
          useValue: viewService,
        },
        {
          provide: TranslateService,
          useValue: mockPassThroughTranslateService(),
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TAServerAuthPolicyPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.credentialsForm = mockCredentialsForm();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should send form', () => {
    viewService.setCredentials.and.returnValue(of({}));

    component.sendCredentialsForm({
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      type: AuthenticationProtocol.BASIC_AUTH,
      username: 'username',
      password: 'password',
    });

    expect(component.statusIcon).toBe('INFO');
    expect(component.credentialsStatusMessage).toBe(component.translateKeys.saveSuccessKey);
  });

  it('should display action errors', () => {
    const actionError: ActionValidationError = {exception: '', i18nParams: [], i18nKey: 'BABOOUM'};
    viewService.setCredentials.and.returnValue(throwError(makeHttpActionError(actionError)));

    component.sendCredentialsForm({
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      type: AuthenticationProtocol.BASIC_AUTH,
      username: 'username',
      password: 'password',
    });

    expect(component.statusIcon).toBe('DANGER');
    expect(component.credentialsStatusMessage).toBe('BABOOUM');
  });

  it('should report unhandled errors to console', () => {
    spyOn(console, 'error');
    viewService.setCredentials.and.returnValue(throwError('BOOM'));

    component.sendCredentialsForm({
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      type: AuthenticationProtocol.BASIC_AUTH,
      username: 'username',
      password: 'password',
    });

    expect(console.error).toHaveBeenCalledWith('BOOM');
  });
});
