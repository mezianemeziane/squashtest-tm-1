import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DefaultRequirementsLinksComponent } from './default-requirements-links.component';
import {
  DialogService,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';

describe('DefaultRequirementsLinksCellRendererComponent', () => {
  let component: DefaultRequirementsLinksComponent;
  let fixture: ComponentFixture<DefaultRequirementsLinksComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultRequirementsLinksComponent ],
      imports: [AppTestingUtilsModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: DialogService,
          userValue: {}
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultRequirementsLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
