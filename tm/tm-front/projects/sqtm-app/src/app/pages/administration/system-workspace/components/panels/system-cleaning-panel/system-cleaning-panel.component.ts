import {ChangeDetectionStrategy, Component, Input, OnInit, ViewContainerRef} from '@angular/core';
import {SystemViewState} from '../../../states/system-view.state';
import {AutomationDeletionCount, DialogService} from 'sqtm-core';
import {switchMap, take, takeUntil} from 'rxjs/operators';
import {SystemViewService} from '../../../services/system-view.service';
import {Observable, Subject} from 'rxjs';
import {DeleteAutomatedSuitesAndExecutionDialogComponent} from '../../dialogs/delete-automated-suites-and-execution-dialog/delete-automated-suites-and-execution-dialog.component';
import {DeleteAutomatedSuitesAndExecutionConfiguration} from '../../dialogs/delete-automated-suites-and-execution-dialog/delete-automated-suites-and-execution-configuration';

@Component({
  selector: 'sqtm-app-system-cleaning-panel',
  templateUrl: './system-cleaning-panel.component.html',
  styleUrls: [
    './system-cleaning-panel.component.less',
    '../../../styles/system-workspace.common.less'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemCleaningPanelComponent implements OnInit {

  @Input() componentData: SystemViewState;

  unsub$ = new Subject<void>();

  constructor(private dialogService: DialogService, private systemViewService: SystemViewService,
              private vcr: ViewContainerRef) {
  }

  ngOnInit(): void {
  }

  showCleaningDialog() {
    this.systemViewService.getOldAutomatedSuitesAndExecutionsCount().pipe(
      take(1),
      switchMap((count: AutomationDeletionCount) => this.openConfirmDeletionDialog(count))
    ).subscribe();
  }

  private openConfirmDeletionDialog(count: AutomationDeletionCount): Observable<boolean> {
    const dialogReference = this.dialogService.openDialog<DeleteAutomatedSuitesAndExecutionConfiguration, boolean>({
      id: 'delete-automated-suite-and-executions',
      component: DeleteAutomatedSuitesAndExecutionDialogComponent,
      viewContainerReference: this.vcr,
      data: {
        id: 'delete-automated-suite-and-executions',
        titleKey: 'sqtm-core.administration-workspace.system.cleaning.title.long',
        messageKey: 'sqtm-core.administration-workspace.system.cleaning.dialog.message',
        level: 'DANGER',
        automationDeletionCount: count
      },
      width: 800
    });

    return dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
      );
  }
}
