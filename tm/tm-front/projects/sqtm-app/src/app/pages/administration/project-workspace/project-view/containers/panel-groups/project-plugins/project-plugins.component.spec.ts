import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ProjectPluginsComponent} from './project-plugins.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {ProjectViewService} from '../../../services/project-view.service';
import {EMPTY} from 'rxjs';

describe('ProjectPluginsComponent', () => {
  let component: ProjectPluginsComponent;
  let fixture: ComponentFixture<ProjectPluginsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj('TranslateService', ['instant']),
        },
        {
          provide: ProjectViewService,
          useValue: {
            componentData$: EMPTY,
            availablePlugins$: EMPTY,
          }
        },
      ],
      declarations: [ProjectPluginsComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectPluginsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
