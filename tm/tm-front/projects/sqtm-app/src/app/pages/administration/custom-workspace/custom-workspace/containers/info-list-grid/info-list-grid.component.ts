import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, ViewContainerRef} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {concatMap, filter, map, take, takeUntil, tap} from 'rxjs/operators';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  DataRow,
  dateColumn,
  deleteColumn,
  DialogService,
  Extendable,
  FilterOperation,
  Fixed,
  grid,
  GridDefinition,
  GridService,
  indexColumn,
  RestService,
  selectableTextColumn,
  textColumn,
  WorkspaceWithGridComponent
} from 'sqtm-core';
import {DeleteInfoListComponent} from '../../components/cell-renderers/delete-info-list/delete-info-list.component';
import {InfoListCreationDialogComponent} from '../../components/dialogs/info-list-creation-dialog/info-list-creation-dialog.component';

export function adminInfoListsTableDefinition(): GridDefinition {
  return grid('infoLists')
    .withColumns([
      indexColumn()
        .changeWidthCalculationStrategy(new Fixed(60))
        .withViewport('leftViewport'),
      selectableTextColumn('label')
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      textColumn('description')
        .disableSort()
        .withI18nKey('sqtm-core.entity.generic.description.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      textColumn('code')
        .withI18nKey('sqtm-core.entity.info-list.code.label')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      textColumn('defaultValue')
        .withI18nKey('sqtm-core.entity.info-list.default-value.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      dateColumn('createdOn')
        .withI18nKey('sqtm-core.entity.generic.created-on.feminine')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      textColumn('createdBy')
        .withI18nKey('sqtm-core.entity.generic.created-by.feminine')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      deleteColumn(DeleteInfoListComponent)
    ]).server().withServerUrl(['info-lists'])
    .disableRightToolBar()
    .withRowHeight(35)
    .enableMultipleColumnsFiltering(['label'])
    .build();
}

@Component({
  selector: 'sqtm-app-info-list-grid',
  templateUrl: './info-list-grid.component.html',
  styleUrls: ['./info-list-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoListGridComponent implements AfterViewInit, OnDestroy {

  authenticatedUser$: Observable<AuthenticatedUser>;
  unsub$ = new Subject<void>();

  private readonly entityIdPositionInUrl = 3;

  constructor(public gridService: GridService,
              private restService: RestService,
              private dialogService: DialogService,
              private adminReferentialDataService: AdminReferentialDataService,
              private viewContainerRef: ViewContainerRef,
              private workspaceWithGrid: WorkspaceWithGridComponent) {
    this.workspaceWithGrid.entityIdPositionInUrl = this.entityIdPositionInUrl;
    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
  }

  ngAfterViewInit() {
    this.addFilters();
    this.gridService.refreshData();
  }

  private addFilters() {
    this.gridService.addFilters([{
      id: 'label',
      active: false,
      initialValue: {kind: 'single-string-value', value: ''},
      tiedToPerimeter: false,
      operation: FilterOperation.LIKE
    }]);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openCreateDialog() {
    const dialogReference = this.dialogService.openDialog({
      component: InfoListCreationDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        titleKey: 'sqtm-core.administration-workspace.info-lists.dialog.title.new-info-list',
        addAnotherLabelKey: 'sqtm-core.generic.label.add-another.feminine'
      },
      id: 'create-info-list-dialog',
      width: 600
    });

    dialogReference.dialogResultChanged$.pipe(
      takeUntil(dialogReference.dialogClosed$),
      filter(result => result != null)
    ).subscribe(() => {
      this.gridService.refreshData();
    });
  }

  deleteInfoLists() {

    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      concatMap((rows: DataRow[]) => this.showConfirmDeleteInfoListDialog(rows)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      concatMap(({rows}) => this.deleteInfoListsServerSide(rows)),
      tap(() => this.gridService.completeAsyncOperation())
    ).subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteInfoListDialog(rows): Observable<{ confirmDelete: boolean, rows: string[] }> {
    const someRowHaveProjects = rows.filter(row => row.data['projectCount'] > 0).length > 0;

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.info-lists.dialog.title.delete-many',
      messageKey: someRowHaveProjects ?
        'sqtm-core.administration-workspace.info-lists.dialog.message.delete-many-with-project' :
        'sqtm-core.administration-workspace.info-lists.dialog.message.delete-many-without-project',
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, rows}))
    );
  }

  private deleteInfoListsServerSide(rows): Observable<void> {
    const pathVariable = rows.map(row => row.data['infoListId']).join(',');
    return this.restService.delete(['info-lists', pathVariable]);
  }

  filterInfoLists($event: any) {
    this.gridService.applyMultiColumnsFilter($event);
  }
}
