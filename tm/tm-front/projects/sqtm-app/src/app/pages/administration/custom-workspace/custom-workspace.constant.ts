import {InjectionToken} from '@angular/core';
import {GridDefinition, GridService} from 'sqtm-core';

/* tslint:disable:max-line-length */

export const ADMIN_WS_CUSTOM_FIELD_TABLE_CONFIG = new InjectionToken<GridDefinition>('Grid config instance for the table of custom fields in admin workspace');
export const ADMIN_WS_CUSTOM_FIELD_TABLE = new InjectionToken<GridService>('Grid service instance for the table of custom fields in admin workspace');

export const ADMIN_WS_INFO_LISTS_TABLE_CONFIG = new InjectionToken<GridDefinition>('Grid config instance for the table of info lists in admin workspace');
export const ADMIN_WS_INFO_LISTS_TABLE = new InjectionToken<GridService>('Grid service instance for the table of info requirements links in admin workspace');

export const ADMIN_WS_REQUIREMENTS_LINKS_TABLE_CONFIG = new InjectionToken<GridDefinition>('Grid config instance for the table of info lists in admin workspace');
export const ADMIN_WS_REQUIREMENTS_LINKS_TABLE = new InjectionToken<GridService>('Grid service instance for the table of requirements links in admin workspace');
