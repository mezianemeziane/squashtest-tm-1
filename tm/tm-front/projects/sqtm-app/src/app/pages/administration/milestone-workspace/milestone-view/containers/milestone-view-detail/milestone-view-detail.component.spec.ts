import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MilestoneViewDetailComponent } from './milestone-view-detail.component';
import {AdminReferentialDataService, RestService} from 'sqtm-core';
import {mockAdminReferentialDataService, mockRestService} from '../../../../../../utils/testing-utils/mocks.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EMPTY} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('MilestoneViewDetailComponent', () => {
  let component: MilestoneViewDetailComponent;
  let fixture: ComponentFixture<MilestoneViewDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MilestoneViewDetailComponent ],
      providers: [
        {
          provide: AdminReferentialDataService,
          useValue: mockAdminReferentialDataService(),
        },
        {
          provide: RestService,
          useValue: mockRestService(),
        },
        {
          provide: Router,
          useValue: {}
        },
        {
          provide: ActivatedRoute,
          useValue: { paramMap: EMPTY }
        },
        {
          provide: TranslateService,
          useValue: {}
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilestoneViewDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
