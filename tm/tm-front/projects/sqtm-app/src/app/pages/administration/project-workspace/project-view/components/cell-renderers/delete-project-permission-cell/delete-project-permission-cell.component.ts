import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  AuthenticatedUser,
  DialogService,
  GridService,
} from 'sqtm-core';
import {Observable} from 'rxjs';
import {finalize} from 'rxjs/operators';
import {ProjectViewService} from '../../../services/project-view.service';

@Component({
  selector: 'sqtm-app-delete-project-permission-cell',
  template: `
    <sqtm-core-delete-icon
      [iconName]="getIcon()"
      (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
  `,
  styleUrls: ['./delete-project-permission-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteProjectPermissionCellComponent extends AbstractDeleteCellRenderer implements OnDestroy {

  authenticatedUser$: Observable<AuthenticatedUser>;

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              private projectViewService: ProjectViewService) {
    super(grid, cdr, dialogService);
  }

  getIcon(): string {
    return 'sqtm-core-generic:unlink';
  }

  doDelete() {
    this.grid.beginAsyncOperation();
    const partyId = this.row.data.partyId;
    this.projectViewService.removePartyProjectPermissions([partyId]).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe();
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.projects.dialog.title.delete-project-permission.delete-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.projects.dialog.message.delete-project-permission.delete-one';
  }
}

