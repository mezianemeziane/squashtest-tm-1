import {Component, ChangeDetectionStrategy, AfterViewInit, ViewChild, ChangeDetectorRef} from '@angular/core';
import {DialogReference, GridService, GroupedMultiListFieldComponent, ListItem, Member, RestService} from 'sqtm-core';
import {TeamViewService} from '../../../services/team-view.service';
import {finalize, switchMap, take} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-add-team-member-dialog',
  templateUrl: './add-team-member-dialog.component.html',
  styleUrls: ['./add-team-member-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddTeamMemberDialogComponent implements AfterViewInit {

  listItems: ListItem[] = [];
  errorsOnMultiListField: string[] = [];

  @ViewChild(GroupedMultiListFieldComponent)
  membersList: GroupedMultiListFieldComponent;

  constructor(private dialogReference: DialogReference,
              private restService: RestService,
              private cdr: ChangeDetectorRef,
              private teamViewService: TeamViewService,
              private grid: GridService) {
  }

  ngAfterViewInit(): void {
    this.prepareMembersMultiListField();
  }

  selectedMembersChanged($event: ListItem[]) {
    this.membersList.selectedItems = $event;
  }

  confirm() {
    if (this.membersList.selectedItems.length === 0) {
      const requiredKey = 'sqtm-core.validation.errors.required';
      this.errorsOnMultiListField.push(requiredKey);
    } else {
      this.doPost();
    }
  }

  private prepareMembersMultiListField() {
    this.teamViewService.componentData$.pipe(
      take(1),
      switchMap(componentData => {
        const teamId = componentData.team.id.toString();

        return this.restService.get<Member[]>(
          ['team-view', teamId, 'non-members']);
      })
    ).subscribe((response) => {
      const members = this.retrieveMembersAsListItem(response);

      members.sort((partyA, partyB) => partyA.label.localeCompare(partyB.label));

      this.listItems = [...members];
      this.cdr.detectChanges();
    });
  }

  private retrieveMembersAsListItem(response: Member[]): ListItem[] {
    return response.map(member => {
      return {
        id: member.login,
        label: member.fullName,
        selected: false,
      };
    });
  }

  private getSelectedMemberLogins(): string[] {
    return this.membersList.selectedItems.map(listItem => String(listItem.id));
  }

  private doPost() {
    this.grid.beginAsyncOperation();
    this.teamViewService.addTeamMembers(this.getSelectedMemberLogins()).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe();
    this.dialogReference.result = true;
    this.dialogReference.close();
  }
}

