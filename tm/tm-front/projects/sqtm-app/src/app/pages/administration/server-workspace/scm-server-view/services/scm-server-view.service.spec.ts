import { TestBed } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { take, withLatestFrom } from 'rxjs/operators';
import {
  AttachmentService,
  AuthenticationPolicy,
  AuthenticationProtocol,
  EntityViewAttachmentHelperService,
  RestService
} from 'sqtm-core';
import { AppTestingUtilsModule } from '../../../../../utils/testing-utils/app-testing-utils.module';
import { mockRestService } from '../../../../../utils/testing-utils/mocks.service';
import { ScmRepositoryPayload } from '../components/dialogs/add-scm-repository-dialog/add-scm-repository-dialog.component';
import { AdminScmServerState } from '../states/admin-scm-server-state';
import { AdminScmServerViewState } from '../states/admin-scm-server-view-state';

import { ScmServerViewService } from './scm-server-view.service';
import createSpyObj = jasmine.createSpyObj;

describe('ScmServerViewService', () => {
  let service: ScmServerViewService;
  const restService = mockRestService();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      providers: [
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: TranslateService,
          useValue: createSpyObj(['instant']),
        },
        {
          provide: ScmServerViewService,
          useClass: ScmServerViewService,
          deps: [
            RestService,
            AttachmentService,
            TranslateService,
            EntityViewAttachmentHelperService,
          ]
        },
      ]
    });
    service = TestBed.inject(ScmServerViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load an scm server', async (done) => {
    const model = getInitialScmServer();
    restService.get.and.returnValue(of(model));
    service.load(1);

    service.componentData$.pipe(
      take(1)
    ).subscribe((componentData) => {
      expect(componentData.scmServer).toEqual(model);
      done();
    });
  });

  it('should change credentials', async (done) => {
    restService.get.and.returnValue(of(getInitialScmServer()));
    service.load(1);

    service.setBasicAuthCredentials('login', 'password').pipe(
      withLatestFrom(service.componentData$)
    ).subscribe(([, componentData]: [any, AdminScmServerViewState]) => {
      expect(componentData.scmServer.credentials).toEqual({
        type: AuthenticationProtocol.BASIC_AUTH,
        implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
        password: 'password',
        username: 'login',
        registered : true,
      });
      done();
    });
  });

  it('should add repository', async (done) => {
    restService.get.and.returnValue(of(getInitialScmServer()));
    service.load(1);

    restService.post.and.returnValue(of({
        repositories:
          [
            {
              scmRepositoryId: 1,
              serverId: 1,
              name: 'scm repo 1',
              repositoryPath: '/test/repo1',
              workingFolderPath: 'work/repo1',
              workingBranch: 'master'
            }
          ]
      }
    ));

    const payload: ScmRepositoryPayload = {
      scmRepository: {
        name: 'scm repo 1',
        repositoryPath: '/test/repo1',
        workingFolderPath: 'work/repo1',
        workingBranch: 'master',
      },
      cloneRepository: false
    };

    service.addRepository(payload).pipe(
      withLatestFrom(service.componentData$)
    ).subscribe(([, componentData]: [any, AdminScmServerViewState]) => {
      expect(componentData.scmServer.repositories).toEqual([
        {
          scmRepositoryId: 1,
          serverId: 1,
          name: 'scm repo 1',
          repositoryPath: '/test/repo1',
          workingFolderPath: 'work/repo1',
          workingBranch: 'master'
        }
      ]);
      done();
    });
  });

  it('should delete repository', async (done) => {
    const scmServer = getInitialScmServer();
    scmServer.repositories = [
      {
        scmRepositoryId: 1,
        serverId: 1,
        name: 'scm repo 1',
        repositoryPath: '/test/repo1',
        workingFolderPath: 'work/repo1',
        workingBranch: 'master'
      }
    ];

    restService.get.and.returnValue(of(scmServer));
    service.load(1);
    restService.delete.and.returnValue(of({}));
    service.deleteRepositories([1]).pipe(
      withLatestFrom(service.componentData$)
    ).subscribe(([, componentData]: [any, AdminScmServerViewState]) => {
      expect(componentData.scmServer.repositories).toEqual([]);
      done();
    });
  });

  it('should change repository branch', async (done) => {
    const scmServer = getInitialScmServer();
    scmServer.repositories = [
      {
        scmRepositoryId: 1,
        serverId: 1,
        name: 'scm repo 1',
        repositoryPath: '/test/repo1',
        workingFolderPath: 'work/repo1',
        workingBranch: 'master'
      }
    ];

    restService.get.and.returnValue(of(scmServer));
    service.load(1);

    restService.post.and.returnValue(of({
      repositories:
        [
          {
            scmRepositoryId: 1,
            serverId: 1,
            name: 'scm repo 1',
            repositoryPath: '/test/repo1',
            workingFolderPath: 'work/repo1',
            workingBranch: 'newBranch'
          }
        ]
    }));

    service.changeRepositoryWorkingBranch(1, 'newBranch').pipe(
      withLatestFrom(service.componentData$)
    ).subscribe(([, componentData]: [any, AdminScmServerViewState]) => {
      expect(componentData.scmServer.repositories).toEqual([
        {
          scmRepositoryId: 1,
          serverId: 1,
          name: 'scm repo 1',
          repositoryPath: '/test/repo1',
          workingFolderPath: 'work/repo1',
          workingBranch: 'newBranch'
        }
      ]);
      done();
    });
  });
});

function getInitialScmServer(): AdminScmServerState {
  return {
    id: 1,
    serverId: 1,
    kind: 'git',
    name: 'name',
    url: 'http://url.com',
    committerMail: '',
    repositories: [],
    authPolicy: AuthenticationPolicy.APP_LEVEL,
    authProtocol: AuthenticationProtocol.OAUTH_1A,
    supportedAuthenticationProtocols: [AuthenticationProtocol.BASIC_AUTH, AuthenticationProtocol.TOKEN_AUTH],
    credentials: null,
    attachmentList: {id: null, attachments: null},
  };
}
