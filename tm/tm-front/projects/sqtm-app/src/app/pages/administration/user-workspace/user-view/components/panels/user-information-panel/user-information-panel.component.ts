import {Component, ChangeDetectionStrategy, Input, ViewChild, ViewContainerRef, OnDestroy} from '@angular/core';
import {AdminUserViewComponentData} from '../../../containers/user-view/user-view.component';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe, formatDate} from '@angular/common';
import {UserViewService} from '../../../services/user-view.service';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  DialogService,
  EditableSelectFieldComponent,
  GridService,
  Option,
  UsersGroup, UsersGroupHelpers
} from 'sqtm-core';
import {filter, take, takeUntil} from 'rxjs/operators';
import {ResetPasswordDialogComponent} from '../../dialogs/reset-password-dialog/reset-password-dialog.component';
import {Observable, Subject} from 'rxjs';

@Component({
  selector: 'sqtm-app-user-information-panel',
  templateUrl: './user-information-panel.component.html',
  styleUrls: ['./user-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserInformationPanelComponent implements OnDestroy {

  unsub$ = new Subject<void>();
  authenticatedUser$: Observable<AuthenticatedUser>;

  @Input()
  adminUserViewComponentData: AdminUserViewComponentData;

  @ViewChild(EditableSelectFieldComponent)
  userGroupSelectField: EditableSelectFieldComponent;

  constructor(
    public  translateService: TranslateService,
    private userViewService: UserViewService,
    private datePipe: DatePipe,
    private readonly dialogService: DialogService,
    private readonly viewContainerRef: ViewContainerRef,
    private gridService: GridService,
    private adminReferentialDataService: AdminReferentialDataService) {
    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  confirmUserGroup(option: Option) {
    this.userGroupSelectField.beginAsync();
    const groupId = Number(option.value);
    this.userViewService.confirmUserGroup(groupId);
    this.userGroupSelectField.value = option.value;
    this.gridService.refreshDataAndKeepSelectedRows();
  }

  openResetPasswordDialog() {
    const dialogReference = this.dialogService.openDialog({
      id: 'project-custom-field-binding',
      component: ResetPasswordDialogComponent,
      viewContainerReference: this.viewContainerRef,
    });

    dialogReference.dialogClosed$.pipe(
      filter(result => Boolean(result)),
    ).subscribe((result: string) => {
      this.userViewService.resetPassword(this.adminUserViewComponentData.user.id, result);
    });
  }

  getUsersGroups(usersGroups: UsersGroup[]): Option[] {
    const options: Option[] = [];

    usersGroups.forEach((userGroup) => {
        const option = new Option();
        option.value = userGroup.id.toString();
        option.label = this.translateService.instant(UsersGroupHelpers.getI18nKey(userGroup));
        options.push(option);
      }
    );
    return options;
  }

  get lastConnectedOn(): string {
    if (this.adminUserViewComponentData.user.lastConnectedOn !== null) {
      return formatDate(this.adminUserViewComponentData.user.lastConnectedOn, 'short', this.translateService.getBrowserLang());

    } else {
      return this.translateService.instant('sqtm-core.generic.label.never');
    }
  }

  changeUserStatus() {
    const isActive = this.adminUserViewComponentData.user.active;
    this.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      take(1)
    ).subscribe(authenticatedUser => {
      const editingCurrentUser = authenticatedUser.userId.toString() === this.adminUserViewComponentData.user.id.toString();

      if (editingCurrentUser) {
        this.dialogService.openAlert({
          level: 'DANGER',
          messageKey: 'sqtm-core.administration-workspace.users.dialog.message.deactivate-current-user-alert',
          titleKey: 'sqtm-core.generic.label.error',
        });
      } else {
        Boolean(isActive) ? this.userViewService.deactivateUser() : this.userViewService.activateUser();
        this.gridService.refreshDataAndKeepSelectedRows();
      }
    });
  }


  get inactiveColor(): string {
    const isActive = this.adminUserViewComponentData.user.active;
    return Boolean(isActive) ?
      'disabled' : 'inactive';
  }

  get activeColor(): string {
    const isActive = this.adminUserViewComponentData.user.active;
    return Boolean(isActive) ?
      'active' : 'disabled';
  }

}
