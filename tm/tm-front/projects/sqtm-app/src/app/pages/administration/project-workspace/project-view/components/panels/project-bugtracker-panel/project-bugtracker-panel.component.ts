import {
  Component,
  ChangeDetectionStrategy,
  Input,
  ViewChild,
  ChangeDetectorRef,
  OnDestroy
} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {
  BugTracker,
  EditableSelectFieldComponent,
  FieldValidationError, Option,
  RestService
} from 'sqtm-core';
import {AdminProjectViewComponentData} from '../../../containers/project-view/project-view.component';
import {ProjectViewService} from '../../../services/project-view.service';
import {map, takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';

@Component({
  selector: 'sqtm-app-project-bugtracker-panel',
  templateUrl: './project-bugtracker-panel.component.html',
  styleUrls: ['./project-bugtracker-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectBugtrackerPanelComponent implements OnDestroy {

  serverSideValidationErrors: FieldValidationError[] = [];

  isProjectTemplate$: Observable<boolean>;

  private unsub$ = new Subject<void>();

  @ViewChild(EditableSelectFieldComponent)
  bugtrackerSelectField: EditableSelectFieldComponent;

  private readonly noBugtrackerOptionId: string = 'default';

  @Input()
  adminProjectViewComponentData: AdminProjectViewComponentData;

  constructor(
    public adminProjectViewService: ProjectViewService,
    public translateService: TranslateService,
    private restService: RestService,
    private cdr: ChangeDetectorRef
  ) {
    this.isProjectTemplate$ = this.adminProjectViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => componentData.project?.template));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getSelectedValue() {
    if (this.adminProjectViewComponentData.project.bugTrackerBinding != null) {
      return this.adminProjectViewComponentData.project.bugTrackerBinding.bugTrackerId.toString();
    } else {
      return this.noBugtrackerOptionId;
    }
  }

  public getBugtrackers(bugtrackers: BugTracker[]): Option[] {
      const options: Option[] = [];
      const defaultOption = new Option();

      defaultOption.value = this.noBugtrackerOptionId;
      defaultOption.label = this.translateService
        .instant('sqtm-core.administration-workspace.views.project.no-associated-bugtracker.label');

      options.push(defaultOption);

      const currentBtId = this.adminProjectViewComponentData.project.bugTrackerBinding?.bugTrackerId;

      if (currentBtId != null && ! bugtrackers.find(bt => bt.id === currentBtId)) {
        options.push({ value: currentBtId.toString(), label: currentBtId.toString(), hide: true });
      }

      bugtrackers.forEach((bugtracker) => {
        const option = new Option();

        option.value = bugtracker.id.toString();
        option.label = bugtracker.name;
        options.push(option);
      });

      return options;
  }

  get bugTrackerIsNotAvailableAnymore(): boolean {
    const currentBtId = this.adminProjectViewComponentData.project.bugTrackerBinding?.bugTrackerId;
    const bugTrackers = this.adminProjectViewComponentData.project.availableBugtrackers;
    return currentBtId != null && ! bugTrackers.find(bt => bt.id === currentBtId);
  }

  confirmBugtracker(option: Option) {

    this.bugtrackerSelectField.beginAsync();
    const bugtrackerId = option.value === 'default' ? null : Number(option.value);
    this.adminProjectViewService.confirmBugtracker(bugtrackerId);
    this.bugtrackerSelectField.value = option.value;

  }

  addProjectName(projectNames: string[]) {
    const projectId = this.adminProjectViewComponentData.project.id.toString();
    this.restService.post([this.getRootUrl(), projectId, 'bugtracker/projectNames'], projectNames).subscribe(() => {
      this.cdr.detectChanges();
    });
  }

  protected getRootUrl(): string {
    return 'generic-projects';
  }

}
