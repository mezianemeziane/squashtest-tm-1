import {InjectionToken} from '@angular/core';
import {GridDefinition, GridService} from 'sqtm-core';

/* tslint:disable:max-line-length */
export const ADMIN_WS_USER_TABLE_CONFIG = new InjectionToken<GridDefinition>('Grid config instance for the table of users in the admin workspace');
export const ADMIN_WS_USER_TABLE = new InjectionToken<GridService>('Grid service instance for the table of users in the admin workspace');

export const ADMIN_WS_CONNECTION_LOG_TABLE_CONFIG = new InjectionToken<GridDefinition>('Grid config instance for the table of connection logs in the admin workspace');
export const ADMIN_WS_CONNECTION_LOG_TABLE = new InjectionToken<GridService>('Grid service instance for the table of connection logs in the admin workspace');

export const ADMIN_WS_TEAM_TABLE_CONFIG = new InjectionToken<GridDefinition>('Grid config instance for the table of teams in the admin workspace');
export const ADMIN_WS_TEAM_TABLE = new InjectionToken<GridService>('Grid service instance for the table of teams in the admin workspace');
