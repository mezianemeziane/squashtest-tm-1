import {GenericEntityViewState, provideInitialGenericViewState} from 'sqtm-core';
import {AdminTeamState} from './admin-team-state';

export interface AdminTeamViewState extends GenericEntityViewState<AdminTeamState, 'team'> {
  team: AdminTeamState;
}

export function provideInitialAdminTeamView(): Readonly<AdminTeamViewState> {
  return provideInitialGenericViewState<AdminTeamState, 'team'>('team');
}
