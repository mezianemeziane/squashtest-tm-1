import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BindMilestoneDialogComponent } from './bind-milestone-dialog.component';
import {DialogReference} from 'sqtm-core';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CKEditorModule} from 'ckeditor4-angular';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';

describe('BindMilestoneDialogComponent', () => {
  let component: BindMilestoneDialogComponent;
  let fixture: ComponentFixture<BindMilestoneDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, ReactiveFormsModule, TranslateModule.forRoot(), HttpClientTestingModule, CKEditorModule],
      providers: [
        {
          provide: DialogReference,
          useValue: jasmine.createSpyObj(['close'])
        }
      ],
      declarations: [ BindMilestoneDialogComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindMilestoneDialogComponent);
    component = fixture.componentInstance;
    component.data = {
      projectId: -1
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
