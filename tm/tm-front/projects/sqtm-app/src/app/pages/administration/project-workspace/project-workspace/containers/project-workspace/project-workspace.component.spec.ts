import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ProjectWorkspaceComponent} from './project-workspace.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('ProjectWorkspaceComponent', () => {
  let component: ProjectWorkspaceComponent;
  let fixture: ComponentFixture<ProjectWorkspaceComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectWorkspaceComponent],
      imports: [HttpClientTestingModule, AppTestingUtilsModule, RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectWorkspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
