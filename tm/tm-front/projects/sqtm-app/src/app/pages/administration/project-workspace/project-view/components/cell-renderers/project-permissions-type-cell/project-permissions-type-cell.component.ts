import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-project-permissions-type-cell',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column">
        <span style="margin: auto 0;">
          {{cellText | translate}}
        </span>
      </div>
    </ng-container>`,
  styleUrls: ['./project-permissions-type-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectPermissionsTypeCellComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  get cellText(): string {
    const isUser = !this.row.data['team'];
    return isUser ? 'sqtm-core.entity.user.label.singular' : 'sqtm-core.entity.team.label';
  }
}

export function permissionsTypeColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ProjectPermissionsTypeCellComponent)
    .withI18nKey('sqtm-core.administration-workspace.views.project.permissions.type');
}

export function buildPermissionType(translateService: TranslateService) {
  return (keyA: any, keyB: any) => sortProfile(keyA, keyB, translateService);
}

function sortProfile(roleA: boolean, roleB: boolean, translateService: TranslateService) {
  const keyPermissionTypeA = roleA ? 'sqtm-core.entity.user.label.singular' : 'sqtm-core.entity.team.label';
  const keyPermissionTypeB = roleB ? 'sqtm-core.entity.user.label.singular' : 'sqtm-core.entity.team.label';
  const translatedRoleA = translateService.instant(keyPermissionTypeA);
  const translatedRoleB = translateService.instant(keyPermissionTypeB);
  return translatedRoleA.localeCompare(translatedRoleB);
}
