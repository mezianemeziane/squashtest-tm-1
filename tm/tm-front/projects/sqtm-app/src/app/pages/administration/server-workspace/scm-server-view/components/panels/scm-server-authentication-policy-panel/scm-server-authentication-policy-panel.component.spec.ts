import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ScmServerAuthenticationPolicyPanelComponent} from './scm-server-authentication-policy-panel.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ScmServerViewService} from '../../../services/scm-server-view.service';
import {AuthenticationPolicy, AuthenticationProtocol, GenericEntityViewService} from 'sqtm-core';
import {AdminScmServerState} from '../../../states/admin-scm-server-state';
import {AdminScmServerViewState} from '../../../states/admin-scm-server-view-state';
import {EMPTY, of} from 'rxjs';
import {TranslatePipe, TranslateService} from '@ngx-translate/core';
import {mockPassThroughTranslateService} from '../../../../../../../utils/testing-utils/mocks.service';
import {TranslateMockPipe} from '../../../../../../../utils/testing-utils/mocks.pipe';
import SpyObj = jasmine.SpyObj;

describe('ScmServerAuthenticationPolicyPanelComponent', () => {
  let component: ScmServerAuthenticationPolicyPanelComponent;
  let fixture: ComponentFixture<ScmServerAuthenticationPolicyPanelComponent>;
  let viewService: SpyObj<ScmServerViewService>;

  beforeEach(waitForAsync(() => {
    viewService = jasmine.createSpyObj(['load', 'setCredentials']);
    viewService['componentData$'] = of(makeComponentData(makeScmServer()));
    viewService.externalRefreshRequired$ = EMPTY;

    TestBed.configureTestingModule({
      declarations: [ScmServerAuthenticationPolicyPanelComponent],
      imports: [
        ReactiveFormsModule,
      ],
      providers: [
        {
          provide: ScmServerViewService,
          useValue: viewService,
        },
        {
          provide: GenericEntityViewService,
          useValue: viewService,
        },
        {
          provide: TranslateService,
          useValue: mockPassThroughTranslateService(),
        },
        {
          provide: TranslatePipe,
          useClass: TranslateMockPipe,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScmServerAuthenticationPolicyPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

function makeComponentData(scmServer: AdminScmServerState): AdminScmServerViewState {
  return {
    scmServer,
  } as AdminScmServerViewState;
}

function makeScmServer(): AdminScmServerState {
  return {
    id: 1,
    serverId: 1,
    kind: 'git',
    name: 'name',
    url: 'http://url.com',
    committerMail: '',
    repositories: [],
    authPolicy: AuthenticationPolicy.APP_LEVEL,
    authProtocol: AuthenticationProtocol.BASIC_AUTH,
    supportedAuthenticationProtocols: [AuthenticationProtocol.BASIC_AUTH, AuthenticationProtocol.TOKEN_AUTH],
    credentials: {
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      type: AuthenticationProtocol.BASIC_AUTH,
      username: 'a',
      password: 'b',
    },
    attachmentList: {id: null, attachments: null},
  };
}
