import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {adminRequirementsLinksTableDefinition, RequirementsLinkGridComponent} from './requirements-link-grid.component';
import {DataRow, DialogModule, DialogService, GridService, GridTestingModule, RestService} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ADMIN_WS_REQUIREMENTS_LINKS_TABLE, ADMIN_WS_REQUIREMENTS_LINKS_TABLE_CONFIG} from '../../../custom-workspace.constant';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {
  mockClosableDialogService,
  mockGridService,
  mockPassThroughTranslateService,
  mockRestService
} from '../../../../../../utils/testing-utils/mocks.service';
import {of} from 'rxjs';
import {mockMouseEvent} from '../../../../../../utils/testing-utils/test-component-generator';
import {TranslateService} from '@ngx-translate/core';
import {RequirementsLinkService} from '../../services/requirements-link.service';

describe('RequirementsLinkGridComponent', () => {
  let component: RequirementsLinkGridComponent;
  let fixture: ComponentFixture<RequirementsLinkGridComponent>;

  let dialogMock;
  let gridService;
  let restService;
  let translateService;

  beforeEach(waitForAsync(() => {
    dialogMock = mockClosableDialogService();
    gridService = mockGridService();
    restService = mockRestService();
    restService.post.and.returnValue(of({requirementLinks: []}));
    translateService = mockPassThroughTranslateService();

    TestBed.configureTestingModule({
      declarations: [RequirementsLinkGridComponent],
      imports: [GridTestingModule, DialogModule, AppTestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ADMIN_WS_REQUIREMENTS_LINKS_TABLE_CONFIG,
          useFactory: adminRequirementsLinksTableDefinition()
        },
        {
          provide: ADMIN_WS_REQUIREMENTS_LINKS_TABLE,
          useValue: gridService,
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: DialogService,
          useValue: dialogMock.service,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: TranslateService,
          useValue: translateService
        },
        {
          provide: RequirementsLinkService
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementsLinkGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    dialogMock.resetCalls();
    dialogMock.resetSubjects();
    gridService.refreshData.calls.reset();
    restService.delete.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open creation dialog', () => {
    component.openRequirementsLinksDialog();
    dialogMock.closeDialogsWithResult(true);

    expect(dialogMock.service.openDialog).toHaveBeenCalled();
  });

  it('should open delete dialog', () => {
    gridService.selectedRows$ = of([{data: {default: false}} as unknown as DataRow]);
    restService.delete.and.returnValue(of({}));

    component.deleteRequirementsLinks(mockMouseEvent());
    dialogMock.closeDialogsWithResult(true);

    expect(restService.delete).toHaveBeenCalled();
  });

  it('should forbid to delete default link type', () => {
    gridService.selectedRows$ = of([{data: {default: true}} as unknown as DataRow]);

    component.deleteRequirementsLinks(mockMouseEvent());

    expect(dialogMock.service.openAlert).toHaveBeenCalled();
    expect(restService.delete).not.toHaveBeenCalled();
    expect(gridService.refreshData).not.toHaveBeenCalled();
  });
});
