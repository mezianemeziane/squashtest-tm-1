import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationProtocol } from 'sqtm-core';
import { AuthProtocolFormComponent } from '../../../../components/auth-protocol-form/auth-protocol-form.component';
import { ScmServerViewService } from '../../../services/scm-server-view.service';
import { AdminScmServerViewState } from '../../../states/admin-scm-server-view-state';

@Component({
  selector: 'sqtm-app-scm-server-authentication-protocol-panel',
  templateUrl: './scm-server-authentication-protocol-panel.component.html',
  styleUrls: ['./scm-server-authentication-protocol-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScmServerAuthenticationProtocolPanelComponent implements OnInit {

  @Input()
  componentData: AdminScmServerViewState;

  @ViewChild(AuthProtocolFormComponent)
  authProtocolForm: AuthProtocolFormComponent;

  constructor(public readonly translateService: TranslateService,
              public readonly scmServerViewService: ScmServerViewService) {
  }

  ngOnInit(): void {
  }

  handleProtocolChange(protocol: AuthenticationProtocol): void {
    this.scmServerViewService.setAuthenticationProtocol(protocol)
      .subscribe(() => this.authProtocolForm.authProtocol = protocol);
  }

}
