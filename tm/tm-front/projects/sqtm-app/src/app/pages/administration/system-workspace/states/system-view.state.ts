import {SystemViewModel} from 'sqtm-core';

export type SystemViewState = SystemViewModel;
