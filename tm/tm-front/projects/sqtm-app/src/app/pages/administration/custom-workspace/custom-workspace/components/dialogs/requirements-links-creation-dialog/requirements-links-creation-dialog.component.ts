import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DialogReference, FieldValidationError, RestService} from 'sqtm-core';
import {AbstractAdministrationCreationDialogDirective} from '../../../../../components/abstract-administration-creation-dialog';
import {TranslateService} from '@ngx-translate/core';
import {of} from 'rxjs';

@Component({
  selector: 'sqtm-app-requirements-links-creation-dialog',
  templateUrl: './requirements-links-creation-dialog.component.html',
  styleUrls: ['./requirements-links-creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementsLinksCreationDialogComponent extends AbstractAdministrationCreationDialogDirective implements OnInit {

  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];

  constructor(private fb: FormBuilder,
              private translateService: TranslateService,
              dialogReference: DialogReference,
              restService: RestService,
              cdr: ChangeDetectorRef) {
    super ('requirements-links/new', dialogReference, restService, cdr);
  }

  get textFieldToFocus(): string {
    return 'role1';
  }

  ngOnInit(): void {
    this.initializeFormGroup();
  }

  protected getRequestPayload() {
    return of({
      role1: this.getFormControlValue('role1'),
      role1Code: this.getFormControlValue('role1Code'),
      role2: this.getFormControlValue('role2'),
      role2Code: this.getFormControlValue('role2Code')
    });
  }

  protected doResetForm() {
    this.resetFormControl('role1', '');
    this.resetFormControl('role1Code', '');
    this.resetFormControl('role2', '');
    this.resetFormControl('role2Code', '');
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      role1: this.fb.control('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      role1Code: this.fb.control('', [
        Validators.required,
        Validators.maxLength(30)
      ]),
      role2: this.fb.control('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      role2Code: this.fb.control('', [
        Validators.required,
        Validators.maxLength(30)
      ])
    });
  }
}
