import { Injectable } from '@angular/core';
import {createStore, Identifier, RestService, Store} from 'sqtm-core';
import {Observable} from 'rxjs';
import {map, pluck, switchMap, take, tap, withLatestFrom} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InfoListOptionService {

  public readonly store: Store<InfoListOptionState>;
  public readonly state$: Observable<InfoListOptionState>;
  public readonly infoListOptions$: Observable<InfoListOption[]>;

  constructor(protected restService: RestService) {
    this.store = createStore<InfoListOptionState>(getInitialInfoListOptionState());
    this.state$ = this.store.state$;
    this.infoListOptions$ = this.store.state$.pipe(
      pluck('infoListOptions')
    );
  }

  initialize() {
    this.store.commit(getInitialInfoListOptionState());
  }

  addOption(optionLabel: string, optionCode: string): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap(() => this.checkIfCodeAlreadyExistsServerSide(optionCode)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, InfoListOptionState]) => {
        const updateInfoListOptions: InfoListOption[] = [...state.infoListOptions];
        const isFirstOptionToAdd = updateInfoListOptions.length === 0;
        const option: InfoListOption = {
          id: optionCode,
          label: optionLabel,
          code: optionCode,
          isDefault: isFirstOptionToAdd,
          colour: '',
          iconName: null
        };

        updateInfoListOptions.push(option);

        return {
          ...state,
          infoListOptions: updateInfoListOptions
        };
      }),
      tap(state => this.store.commit(state))
    );
  }

  private checkIfCodeAlreadyExistsServerSide(optionCode: string) {

    const urlParts = [
      'info-lists',
      'check-if-item-code-already-exists',
       optionCode,
    ];
    return this.restService.get(urlParts);
  }

  removeOption(optionLabel: string): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      map((state: InfoListOptionState) => {
        const updateInfoListOptions = [...state.infoListOptions].filter(option => option.label !== optionLabel);

        return {
          ...state,
          infoListOptions: updateInfoListOptions
        };
      }),
      tap(state => this.store.commit(state))
    );
  }

  toggleDefault(optionLabel: string): void {
    this.store.state$.pipe(
      take(1),
      map((state: InfoListOptionState) => {
        const updateInfoListOptions = [...state.infoListOptions].map((option: InfoListOption) => {
          const isSelectedOption = option.label === optionLabel;
          const isCurrentDefault = option.isDefault;
          return {
            ...option,
            isDefault: isCurrentDefault ? false : isSelectedOption,
          };
        });

        return {
          ...state,
          infoListOptions: updateInfoListOptions
        };
      })
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  changeColor(optionLabel: string, newColor: string): void {
    this.store.state$.pipe(
      take(1),
      map((state: InfoListOptionState) => {
        const updateInfoListOptions = [...state.infoListOptions]
          .map((option: InfoListOption) => {
            const isSelected = option.label === optionLabel;
            return {
              ...option,
              colour: isSelected ? newColor : option.colour,
            };
          });
        return {
          ...state,
          infoListOptions: updateInfoListOptions
        };
      })
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  changeItemIcon(optionLabel: string, newIcon: string): void {
    this.store.state$.pipe(
      take(1),
      map((state: InfoListOptionState) => {
        const updateInfoListOptions = [...state.infoListOptions]
          .map((option: InfoListOption) => {
            const isSelected = option.label === optionLabel;
            return {
              ...option,
              iconName: isSelected ? newIcon : option.iconName,
            };
          });
        return {
          ...state,
          infoListOptions: updateInfoListOptions
        };
      })
    ).subscribe(state => {
      this.store.commit(state);
    });
  }
}

interface InfoListOptionState {
  infoListOptions: InfoListOption[];
}

export interface InfoListOption {
  id: Identifier;
  label: string;
  code: string;
  iconName: string;
  isDefault: boolean;
  colour: string;
}

function getInitialInfoListOptionState(): InfoListOptionState {
  return {
    infoListOptions: [],
  };
}
