import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestAutoServerInfoPanelComponent} from './test-auto-server-info-panel.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {DialogService, RestService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestAutomationServerViewService} from '../../../services/test-automation-server-view.service';

describe('TestAutomationServerInformationPanelComponent', () => {
  let component: TestAutoServerInfoPanelComponent;
  let fixture: ComponentFixture<TestAutoServerInfoPanelComponent>;
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TestAutoServerInfoPanelComponent],
      imports: [HttpClientTestingModule, AppTestingUtilsModule, OverlayModule],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: DialogService,
          userValue: {}
        },
        {
          provide: TestAutomationServerViewService,
          useValue: {},
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestAutoServerInfoPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
