import {AdminInfoListItem, SqtmGenericEntityState} from 'sqtm-core';

export interface AdminInfoListState extends SqtmGenericEntityState {
  id: number;
  label: string;
  code: string;
  description: string;
  createdBy: string;
  createdOn: string;
  lastModifiedBy: string;
  lastModifiedOn: string;
  items: AdminInfoListItem[];
}
