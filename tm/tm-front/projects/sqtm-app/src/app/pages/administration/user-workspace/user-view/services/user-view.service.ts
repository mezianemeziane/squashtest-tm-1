import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {
  GenericEntityViewService,
  AttachmentService,
  EntityViewAttachmentHelperService,
  RestService,
  User,
} from 'sqtm-core';

import {AdminUserViewState, provideInitialAdminUserView} from '../states/admin-user-view-state';
import {AdminUserState} from '../states/admin-user-state';
import {map, switchMap, take, tap, withLatestFrom} from 'rxjs/operators';
import {createFeatureSelector} from '@ngrx/store';
import {Observable} from 'rxjs';

@Injectable()
export class UserViewService extends GenericEntityViewService<AdminUserState, 'user'> {

  public readonly teamCount$: Observable<number>;
  public readonly authorisationCount$: Observable<number>;

  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
  ) {
    super(
      restService,
      attachmentService,
      translateService,
      attachmentHelper
    );

    this.teamCount$ = this.componentData$.pipe(
      map(componentData => componentData.user.teams.length)
    );

    this.authorisationCount$ = this.componentData$.pipe(
      map(componentData => componentData.user.projectPermissions.length)
    );
  }

  public getInitialState(): AdminUserViewState {
    return provideInitialAdminUserView();
  }

  public load(userId: number) {
    this.restService.get<User>(['user-view', userId.toString()])
      .subscribe((user: User) => {
        this.initializeUser(user);
      });
  }

  private initializeUser(user: User) {
    const userState: AdminUserState = {
      ...user,
      attachmentList: {id: null, attachments: null},
    };
    this.initializeEntityState(userState);
  }

  protected getRootUrl(initialState?): string {
    return 'users';
  }

  activateUser() {
    this.store.state$.pipe(
      take(1),
      switchMap((adminUserViewState: AdminUserViewState) => this.activateUserServerSide(adminUserViewState)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminUserViewState]) => this.updateStateForActivatedUser(state))
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  private activateUserServerSide(adminUserViewState: AdminUserViewState) {
    const userId = adminUserViewState.user.id.toString();
    return this.restService.post(
      [this.getRootUrl(), userId, 'activate']);
  }

  private updateStateForActivatedUser(state: AdminUserViewState) {
    return {
      ...state,
      user: {
        ...state.user,
        active: true,
      }
    };
  }

  deactivateUser() {
    this.store.state$.pipe(
      take(1),
      switchMap((adminUserViewState: AdminUserViewState) => this.deactivateUserServerSide(adminUserViewState)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminUserViewState]) => this.updateStateForDeactivatedUser(state))
    ).subscribe(state => {
      this.store.commit(state);
    });
  }


  private deactivateUserServerSide(adminUserViewState: AdminUserViewState) {
    const userId = adminUserViewState.user.id.toString();
    return this.restService.post(
      [this.getRootUrl(), userId, 'deactivate']);
  }

  private updateStateForDeactivatedUser(state: AdminUserViewState) {
    return {
      ...state,
      user: {
        ...state.user,
        active: false,
      }
    };
  }

  confirmUserGroup(groupId: number) {
    this.store.state$.pipe(
      take(1),
      switchMap((adminUserViewState: AdminUserViewState) => this.changeUserGroupServerSide(adminUserViewState, groupId)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminUserViewState]) => this.updateStateWithNewUserGroupBinding(state, groupId))
    ).subscribe(state => {
      this.store.commit(state);
      this.requireExternalUpdate(state.user.id, 'usersGroupBinding');
    });
  }

  private changeUserGroupServerSide(adminUserViewState: AdminUserViewState, groupId: number) {
    const userId = adminUserViewState.user.id.toString();
    return this.restService.post(
      [this.getRootUrl(), userId, 'change-group', groupId.toString()]);
  }

  private updateStateWithNewUserGroupBinding(state: AdminUserViewState, groupId: number) {
    return {
      ...state,
      user: {
        ...state.user,
        usersGroupBinding: groupId,
      }
    };
  }

  resetPassword(userId: number, newPassword: string) {
    this.restService.post([this.getRootUrl(), userId.toString(), 'reset-password'], {}, {params: {'password': newPassword}})
      .subscribe();
  }

  setUserAuthorisation(projectIds: number[], authorisedProfile: string): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: AdminUserViewState) => this.setUserAuthorisationsServerSide(state, projectIds, authorisedProfile)),
      withLatestFrom(this.store.state$),
      map(([response, state]: [any, AdminUserViewState]) => this.updateStateWithNewProjectPermissions(state, response)),
      tap(state => this.store.commit(state)),
      tap((state: AdminUserViewState) => this.requireExternalUpdate(state.user.id, 'projectPermissions'))
    );
  }

  private setUserAuthorisationsServerSide(state: AdminUserViewState, projectIds: number[], authorisedProfile: string) {
    const urlParts = [this.getRootUrl(), state.user.id.toString(), 'permissions', projectIds.join(',')];
    return this.restService.post(urlParts, {}, {params: {'permission': authorisedProfile}});
  }

  private updateStateWithNewProjectPermissions(state: AdminUserViewState, response) {
    return {
      ...state,
      user: {
        ...state.user,
        projectPermissions: response.projectPermissions,
      }
    };
  }

  removeAuthorisation(projectIds: number[]): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: AdminUserViewState) => this.removeUserAuthorisationsServerSide(state, projectIds)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminUserViewState]) => {
        return this.updateStateWithRemovedProjectPermissions(state, projectIds);
      }),
      tap((newState) => this.store.commit(newState)),
      tap((state: AdminUserViewState) => this.requireExternalUpdate(state.user.id, 'projectPermissions'))
    );
  }

  private removeUserAuthorisationsServerSide(adminUserViewState: AdminUserViewState, projectIds: number[]) {
    const urlParts = [this.getRootUrl(), adminUserViewState.user.id.toString(), 'permissions', projectIds.join(',')];
    return this.restService.delete(urlParts);
  }

  private updateStateWithRemovedProjectPermissions(state: AdminUserViewState, projectIds: number[]) {
    const updatedProjectPermissions = [...state.user.projectPermissions];
    const filteredProjectPerm = updatedProjectPermissions.filter(value => !projectIds.includes(value.projectId));
    return {
      ...state,
      user: {
        ...state.user,
        projectPermissions: filteredProjectPerm,
      }
    };
  }

  associateTeams(teamIds: number[]): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((adminUserViewState: AdminUserViewState) => this.associateTeamsServerSide(adminUserViewState, teamIds)),
      withLatestFrom(this.store.state$),
      map(([response, state]: [any, AdminUserViewState]) => this.updateStateWithNewTeamAssociations(state, response)),
      tap(state => this.store.commit(state)),
      tap((state: AdminUserViewState) => this.requireExternalUpdate(state.user.id, 'teams'))
    );
  }

  private associateTeamsServerSide(adminUserViewState: AdminUserViewState, teamIds: number[]) {
    const urlParts = [this.getRootUrl(), adminUserViewState.user.id.toString(), 'teams', teamIds.join(',')];
    return this.restService.post(urlParts);
  }

  private updateStateWithNewTeamAssociations(state: AdminUserViewState, response) {
    return {
      ...state,
      user: {
        ...state.user,
        teams: response.teams,
      }
    };
  }

  removeTeamAssociation(partyIds: number[]): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((adminUserViewState: AdminUserViewState) => this.removeTeamAssociationServerSide(adminUserViewState, partyIds)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminUserViewState]) => {
        return this.updateStateWithRemovedTeamAssociations(state, partyIds);
      }),
      tap((newState) => this.store.commit(newState)),
      tap((state: AdminUserViewState) => this.requireExternalUpdate(state.user.id, 'teams'))
    );
  }

  private removeTeamAssociationServerSide(adminUserViewState: AdminUserViewState, partyIds: number[]) {
    const urlParts = [this.getRootUrl(), adminUserViewState.user.id.toString(), 'teams', partyIds.join(',')];
    return this.restService.delete(urlParts);
  }

  private updateStateWithRemovedTeamAssociations(state: AdminUserViewState, partyIds: number[]) {
    const updatedTeams = [...state.user.teams];
    const filteredTeams = updatedTeams.filter(value => !partyIds.includes(value.partyId));
    return {
      ...state,
      user: {
        ...state.user,
        teams: filteredTeams,
      }
    };
  }
}

export const getUserViewState = createFeatureSelector<AdminUserViewState, AdminUserState>('user');
