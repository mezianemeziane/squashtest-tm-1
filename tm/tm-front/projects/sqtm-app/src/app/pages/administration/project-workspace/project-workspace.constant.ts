import {InjectionToken} from '@angular/core';
import {GridDefinition, GridService} from 'sqtm-core';

// tslint:disable-next-line:max-line-length
export const ADMIN_WS_PROJECT_TABLE_CONFIG = new InjectionToken<GridDefinition>('Grid config instance for the admin workspace project table');
export const ADMIN_WS_PROJECT_TABLE = new InjectionToken<GridService>('Grid service instance for the admin workspace project table');
