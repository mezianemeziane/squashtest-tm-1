import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {PluginConfigureCellComponent} from './plugin-configure-cell.component';
import {
  ColumnDisplay,
  DataRow,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {ProjectViewService} from '../../../services/project-view.service';
import {TranslateModule} from '@ngx-translate/core';

describe('PluginConfigureCellComponent', () => {
  let component: PluginConfigureCellComponent;
  let fixture: ComponentFixture<PluginConfigureCellComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PluginConfigureCellComponent],
      imports: [AppTestingUtilsModule, OverlayModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        },
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        {
          provide: ProjectViewService,
          useClass: ProjectViewService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PluginConfigureCellComponent);
    component = fixture.componentInstance;
    component.row = {data: {id: ''}} as unknown as DataRow;
    component.columnDisplay = {id: 'toto'} as ColumnDisplay;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
