import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TAServerAuthProtocolPanelComponent} from './taserver-auth-protocol-panel.component';
import {AdminTestAutomationServerState} from '../../../states/admin-test-automation-server-state';
import {AdminTestAutomationServerViewComponentData} from '../../../containers/test-automation-server-view/test-automation-server-view.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestAutomationServerViewService} from '../../../services/test-automation-server-view.service';
import {EMPTY} from 'rxjs';

describe('TAServerAuthProtocolPanelComponent', () => {
  let component: TAServerAuthProtocolPanelComponent;
  let fixture: ComponentFixture<TAServerAuthProtocolPanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TAServerAuthProtocolPanelComponent],
      providers: [{
        provide: TestAutomationServerViewService,
        useValue: { componentData$: EMPTY },
      }],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TAServerAuthProtocolPanelComponent);
    component = fixture.componentInstance;
    const testAutomationServer: AdminTestAutomationServerState = {
      id: 1,
      baseUrl: '',
      createdBy: '',
      createdOn: undefined,
      description: '',
      kind: '',
      lastModifiedBy: '',
      lastModifiedOn: undefined,
      manualSlaveSelection: false,
      name: '',
      credentials: null,
      authProtocol: undefined,
      supportedAuthenticationProtocols: [],
      attachmentList: undefined,
    };

    component.componentData = {testAutomationServer} as AdminTestAutomationServerViewComponentData;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
