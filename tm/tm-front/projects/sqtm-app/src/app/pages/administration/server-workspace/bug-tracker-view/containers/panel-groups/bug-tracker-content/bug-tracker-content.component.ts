import {Component, OnInit, ChangeDetectionStrategy, OnDestroy} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AdminBugTrackerViewState} from '../../../states/admin-bug-tracker-view-state';
import {BugTrackerViewService} from '../../../services/bug-tracker-view.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-bug-tracker-content',
  templateUrl: './bug-tracker-content.component.html',
  styleUrls: ['./bug-tracker-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BugTrackerContentComponent implements OnInit, OnDestroy {

  componentData$: Observable<AdminBugTrackerViewState>;

  unsub$ = new Subject<void>();

  constructor(public readonly bugTrackerViewService: BugTrackerViewService) {
    this.componentData$ = bugTrackerViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}
