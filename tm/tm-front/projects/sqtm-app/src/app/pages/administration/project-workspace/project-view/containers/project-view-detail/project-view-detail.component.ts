import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {AdminReferentialDataService, AuthenticatedUser, GenericEntityViewService, isAdminOrProjectManager} from 'sqtm-core';
import {Router} from '@angular/router';
import {filter, takeUntil} from 'rxjs/operators';
import {ProjectViewService} from '../../services/project-view.service';

@Component({
  selector: 'sqtm-app-project-view-detail',
  templateUrl: './project-view-detail.component.html',
  styleUrls: ['./project-view-detail.component.less'],
  providers: [
    {
      provide: ProjectViewService,
      useClass: ProjectViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: ProjectViewService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectViewDetailComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();

  constructor(public readonly adminReferentialDataService: AdminReferentialDataService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.adminReferentialDataService.refresh().subscribe();

    this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      filter((authUser: AuthenticatedUser) => !isAdminOrProjectManager(authUser)),
    ).subscribe(() => this.router.navigate(['home-workspace']));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  back(): void {
    history.back();
  }

  handleProjectDeleted(): void {
    this.back();
  }
}
