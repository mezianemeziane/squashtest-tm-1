import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DialogReference, FieldValidationError, TextFieldComponent} from 'sqtm-core';
import {EditProjectAutomationJobDialogConfiguration} from './edit-project-automation-job-dialog.configuration';
import {TranslateService} from '@ngx-translate/core';
import {HttpErrorResponse} from '@angular/common/http';
import {ProjectViewService, UpdatedAutomationJob} from '../../../services/project-view.service';

@Component({
  selector: 'sqtm-app-edit-project-automation-job-dialog',
  templateUrl: './edit-project-automation-job-dialog.component.html',
  styleUrls: ['./edit-project-automation-job-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditProjectAutomationJobDialogComponent implements OnInit {

  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: EditProjectAutomationJobDialogConfiguration;

  @ViewChildren(TextFieldComponent)
  textFields: QueryList<TextFieldComponent>;

  constructor(private fb: FormBuilder,
              private translateService: TranslateService,
              private dialogReference: DialogReference<EditProjectAutomationJobDialogConfiguration>,
              private projectViewService: ProjectViewService,
              private cdr: ChangeDetectorRef) {
    this.data = this.dialogReference.data;
  }

  ngOnInit(): void {
    this.initializeFormGroup();
  }

  confirm() {
    if (this.formGroup.valid) {
      const updatedAutomationJob: UpdatedAutomationJob = this.getUpdatedAutomationJob();

      this.projectViewService.updateTestAutomationProject(updatedAutomationJob).subscribe(
        () => {
          this.cdr.detectChanges();
          this.dialogReference.result = true;
          this.dialogReference.close();
        },
        error => {
          this.handleUpdateFailure(error);
        }
      );
    } else {
      this.textFields.forEach(textField => textField.showClientSideError());
    }
  }

  private getUpdatedAutomationJob(): UpdatedAutomationJob {
    return {
      canRunBdd: this.formGroup.controls['canRunBdd'].value,
      taProjectId: this.data.taProjectId,
      remoteName: this.data.remoteName,
      label: this.formGroup.controls['label'].value,
      executionEnvironment: this.formGroup.controls['executionEnvironment'].value || '',
    };
  }

  private handleUpdateFailure(error: HttpErrorResponse) {
    if (error.status === 412) {
      const squashError = error.error.squashTMError;
      if (squashError.kind === 'FIELD_VALIDATION_ERROR') {
        this.serverSideValidationErrors = squashError.fieldValidationErrors;
        this.cdr.markForCheck();
      }
    }
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      label: this.fb.control(this.data.label, [
        Validators.required,
      ]),
      canRunBdd: this.fb.control(this.data.canRunBdd),
      executionEnvironment: this.fb.control(this.data.executionEnvironment),
    });
  }
}

