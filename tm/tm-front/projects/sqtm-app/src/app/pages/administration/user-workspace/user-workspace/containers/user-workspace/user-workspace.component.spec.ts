import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UserWorkspaceComponent } from './user-workspace.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('UserWorkspaceComponent', () => {
  let component: UserWorkspaceComponent;
  let fixture: ComponentFixture<UserWorkspaceComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UserWorkspaceComponent ],
      imports: [HttpClientTestingModule, AppTestingUtilsModule, RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserWorkspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
