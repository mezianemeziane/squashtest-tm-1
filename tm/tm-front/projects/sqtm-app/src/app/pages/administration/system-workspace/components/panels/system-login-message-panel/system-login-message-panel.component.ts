import {ChangeDetectionStrategy, Component} from '@angular/core';
import {SystemViewService} from '../../../services/system-view.service';

@Component({
  selector: 'sqtm-app-system-login-message-panel',
  template: `
    <sqtm-core-editable-rich-text
        [value]="(systemViewService.componentData$|async).loginMessage"
        (confirmEvent)="changeMessage($event)"
        [attr.data-test-field-id]="'loginMessage'">
    </sqtm-core-editable-rich-text>
  `,
  styleUrls: ['./system-login-message-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemLoginMessagePanelComponent {

  constructor(public readonly systemViewService: SystemViewService) {
  }

  changeMessage($event: string): void {
    this.systemViewService.setLoginMessage($event).subscribe();
  }

}
