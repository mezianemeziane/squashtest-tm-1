import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DeleteRequirementsLinksComponent} from './delete-requirements-links.component';
import {
  DialogService,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {OverlayModule} from '@angular/cdk/overlay';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {RequirementsLinkService} from '../../../services/requirements-link.service';

describe('DeleteRequirementsLinksCellRendererComponent', () => {
  let component: DeleteRequirementsLinksComponent;
  let fixture: ComponentFixture<DeleteRequirementsLinksComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteRequirementsLinksComponent],
      imports: [AppTestingUtilsModule, OverlayModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: DialogService,
          userValue: {}
        }, {
          provide: RequirementsLinkService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteRequirementsLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
