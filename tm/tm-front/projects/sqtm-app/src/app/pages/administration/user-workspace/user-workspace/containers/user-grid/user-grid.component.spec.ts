import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {UserGridComponent} from './user-grid.component';
import {
  ActionErrorDisplayService,
  ActionValidationError,
  AdminReferentialDataService, AdminReferentialDataState,
  AuthenticatedUser,
  DialogModule,
  DialogService, GRID_PERSISTENCE_KEY,
  GridService,
  GridTestingModule, GridWithStatePersistence,
  Identifier,
  RestService,
  WorkspaceWithGridComponent
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {of, throwError} from 'rxjs';
import {makeHttpActionError} from '../../../../../../utils/testing-utils/test-error-response-generator';
import {
  mockAdminReferentialDataServiceWithUser,
  mockClosableDialogService,
  mockGridService,
  mockPassThroughTranslateService,
  mockRestService,
  mockRouter
} from '../../../../../../utils/testing-utils/mocks.service';
import {mockMouseEvent} from '../../../../../../utils/testing-utils/test-component-generator';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;


describe('UserGridComponent', () => {
  let component: UserGridComponent;
  let fixture: ComponentFixture<UserGridComponent>;

  let restService: SpyObj<RestService>;
  let gridService: SpyObj<GridService>;
  let adminReferentialDataService: SpyObj<AdminReferentialDataService>;
  let actionErrorDisplayService: SpyObj<ActionErrorDisplayService>;

  const dialogServiceMock = mockClosableDialogService();

  beforeEach(waitForAsync(() => {
    restService = mockRestService();
    gridService = mockGridService();
    adminReferentialDataService = mockAdminReferentialDataServiceWithUser({
      userId: 1,
      admin: true,
    } as AuthenticatedUser);
    actionErrorDisplayService = createSpyObj(['handleActionError']);

    TestBed.configureTestingModule({
      declarations: [UserGridComponent],
      imports: [RouterTestingModule, GridTestingModule, DialogModule, AppTestingUtilsModule, NzDropDownModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: DialogService,
          useValue: dialogServiceMock.service,
        },
        {
          provide: AdminReferentialDataService,
          useValue: adminReferentialDataService,
        },
        {
          provide: ActionErrorDisplayService,
          useValue: actionErrorDisplayService,
        },
        {
          provide: Router,
          useValue: mockRouter(),
        },
        {
          provide: WorkspaceWithGridComponent,
          useValue: {},
        },
        {
          provide: TranslateService,
          useValue: mockPassThroughTranslateService(),
        },
        {
          provide: GRID_PERSISTENCE_KEY,
          useValue: 'user-workspace-main-grid'
        },
        GridWithStatePersistence
      ]
    })
      .compileComponents();
  }));

  const prepareComponent = (selectedRowIds: Identifier[] = []) => {
    gridService.selectedRowIds$ = of(selectedRowIds);

    fixture = TestBed.createComponent(UserGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    dialogServiceMock.resetCalls();
    dialogServiceMock.resetSubjects();
  };

  it('should create', () => {
    prepareComponent();
    expect(component).toBeTruthy();
  });

  it('should open add user dialog', () => {
    restService.get.and.returnValue(of({
      usersGroups: [{
        id: 'blabla',
        qualifiedName: 'squashtest.authz.group.core.Admin',
      }]
    }));
    prepareComponent();

    adminReferentialDataService.adminReferentialData$ = of({ canManageLocalPassword: true } as AdminReferentialDataState);
    component.addUser();
    dialogServiceMock.closeDialogsWithResult(true);

    expect(gridService.refreshData).toHaveBeenCalled();
  });

  it('should delete users', () => {
    prepareComponent(['2']);

    restService.delete.and.returnValue(of(null));

    component.deleteUsers(mockMouseEvent());
    dialogServiceMock.closeDialogsWithResult(true);

    expect(gridService.refreshData).toHaveBeenCalled();
  });

  it('should show alert if trying to delete current user', () => {
    prepareComponent(['1']);

    component.deleteUsers(mockMouseEvent());

    expect(dialogServiceMock.service.openAlert).toHaveBeenCalled();
  });

  it('should show server-side errors for deletion', () => {
    prepareComponent(['2']);

    restService.delete.and.returnValue(throwError(makeHttpActionError({i18nKey: 'kaboom'} as ActionValidationError)));

    component.deleteUsers(mockMouseEvent());
    dialogServiceMock.closeDialogsWithResult(true);

    expect(actionErrorDisplayService.handleActionError).toHaveBeenCalled();
  });

  it('should activate users', () => {
    prepareComponent(['2']);

    component.activateUsers(mockMouseEvent());

    expect(gridService.refreshData).toHaveBeenCalled();
  });

  it('should deactivate users', () => {
    prepareComponent(['2']);

    component.deactivateUsers(mockMouseEvent());
    expect(gridService.refreshData).toHaveBeenCalled();
  });
});
