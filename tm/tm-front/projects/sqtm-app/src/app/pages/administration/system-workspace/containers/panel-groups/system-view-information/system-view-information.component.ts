import {Component, OnInit, ChangeDetectionStrategy, OnDestroy} from '@angular/core';
import {SystemViewState} from '../../../states/system-view.state';
import {Observable, Subject} from 'rxjs';
import {SystemViewService} from '../../../services/system-view.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-system-view-information',
  templateUrl: './system-view-information.component.html',
  styleUrls: ['./system-view-information.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemViewInformationComponent implements OnInit, OnDestroy {
  componentData$: Observable<SystemViewState>;

  unsub$ = new Subject<void>();

  constructor(public readonly systemViewService: SystemViewService) {
    this.componentData$ = this.systemViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
