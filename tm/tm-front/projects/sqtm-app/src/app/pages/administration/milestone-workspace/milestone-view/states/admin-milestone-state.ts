import {SqtmGenericEntityState, ProjectInfoForMilestoneAdminView} from 'sqtm-core';

export interface AdminMilestoneState extends SqtmGenericEntityState {
  id: number;
  label: string;
  description: string;
  endDate: string;
  status: string;
  range: string;
  ownerFistName: string;
  ownerLastName: string;
  ownerLogin: string;
  createdBy: string;
  createdOn: string;
  lastModifiedBy: string;
  lastModifiedOn: string;
  canEdit: boolean;
  boundProjectsInformation: ProjectInfoForMilestoneAdminView[];
}
