import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ProjectAutomationPanelComponent} from './project-automation-panel.component';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DialogService} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {ProjectViewService} from '../../../services/project-view.service';
import {of} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AdminProjectViewComponentData} from '../../../containers/project-view/project-view.component';
import {mockEditableSelectField} from '../../../../../../../utils/testing-utils/test-component-generator';
import {mockDialogService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('ProjectAutomationPanelComponent', () => {
  let component: ProjectAutomationPanelComponent;
  let fixture: ComponentFixture<ProjectAutomationPanelComponent>;

  const dialogService = mockDialogService();

  const projectViewService = jasmine.createSpyObj<ProjectViewService>([
    'unbindFromScmRepository',
    'bindToScmRepository',
    'bindToTestAutomationServer'
  ]);

  const componentData = {
    project: {
      taServerId: 1,
      availableTestAutomationServers: [
        {
          id: 1,
          name: 'ta server',
          baseUrl: 'baseUrl',
        }
      ],
      boundTestAutomationProjects: [{taProjectId: 1, name: 'p1'}],
      scmRepositoryId: 1,
      availableScmServers: [{serverId: 1, repositories: [{scmRepositoryId: 1, name: 'repo'}]}]
    }
  } as unknown as AdminProjectViewComponentData;
  projectViewService.componentData$ = of(componentData);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), HttpClientTestingModule, AppTestingUtilsModule],
      declarations: [ProjectAutomationPanelComponent],
      providers: [
        {
          provide: DialogService,
          useValue: dialogService,
        },
        {
          provide: ProjectViewService,
          useValue: projectViewService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectAutomationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.serverSelectField = mockEditableSelectField();

    projectViewService.unbindFromScmRepository.calls.reset();
    projectViewService.bindToScmRepository.calls.reset();
    projectViewService.bindToTestAutomationServer.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change SCM server', () => {
    component.handleScmServerConfirm({value: '1', label: 'scm server'});
    expect(projectViewService.unbindFromScmRepository).toHaveBeenCalled();
  });

  it('should change SCM repository', () => {
    component.handleScmRepositoryConfirm({value: '1', label: 'repo'});
    expect(projectViewService.bindToScmRepository).toHaveBeenCalled();
  });

  it('should unbind from SCM repository', () => {
    component.handleScmRepositoryConfirm({value: null, label: 'repo'});
    expect(projectViewService.unbindFromScmRepository).toHaveBeenCalled();
  });

  it('should change execution server', () => {
    dialogService.openDeletionConfirm.and.returnValue({
      dialogClosed$: of(true),
    }  as any);

    component.handleExecutionServerChange({
      value: '2',
      label: 'ta server',
    }, componentData);

    expect(projectViewService.bindToTestAutomationServer).toHaveBeenCalledWith(2);
  });
});
