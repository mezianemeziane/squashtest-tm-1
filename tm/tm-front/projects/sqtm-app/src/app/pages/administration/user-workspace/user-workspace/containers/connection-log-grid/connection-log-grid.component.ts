import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, ViewContainerRef} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  booleanColumn,
  buildFilters,
  dateTimeColumn,
  DialogService,
  Extendable,
  Fixed,
  grid,
  GridDefinition,
  GridFilter,
  GridFilterUtils,
  GridService,
  indexColumn,
  RestService,
  serverBackedGridDateRangeFilter,
  serverBackedGridTextFilter,
  textColumn
} from 'sqtm-core';
import {
  ConnectionLogExportDialogComponent,
  ConnectionLogExportDialogData
} from '../../components/dialogs/connection-log-export-dialog/connection-log-export-dialog.component';
import {map, take, takeUntil} from 'rxjs/operators';

export function adminConnectionLogTableDefinition(): GridDefinition {
  return grid('connectionLogs')
    .withColumns([
      indexColumn()
        .changeWidthCalculationStrategy(new Fixed(60))
        .withViewport('leftViewport'),
      textColumn('login')
        .withI18nKey('sqtm-core.entity.user.login.label')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2))
        .withAssociatedFilter(),
      dateTimeColumn('connectionDate')
        .withI18nKey('sqtm-core.entity.connection-logs.connection-date.label')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1))
        .withAssociatedFilter(),
      booleanColumn('success')
        .withI18nKey('sqtm-core.entity.connection-logs.success.label')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1))
        .withHeaderPosition('center')
        .withContentPosition('center')
    ]).server().withServerUrl(['users/connection-logs'])
    .disableRightToolBar()
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-connection-log-grid',
  templateUrl: './connection-log-grid.component.html',
  styleUrls: ['./connection-log-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConnectionLogGridComponent implements AfterViewInit, OnDestroy {
  authenticatedUser$: Observable<AuthenticatedUser>;

  unsub$ = new Subject<void>();
  activeFilters$: Observable<GridFilter[]>;

  constructor(private readonly gridService: GridService,
              private readonly restService: RestService,
              private readonly adminReferentialDataService: AdminReferentialDataService,
              private readonly viewContainerRef: ViewContainerRef,
              private readonly dialogService: DialogService) {

    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
    this.activeFilters$ = this.gridService.activeFilters$.pipe(
      takeUntil(this.unsub$),
      map((gridFilters: GridFilter[]) => gridFilters.filter(gridFilter => GridFilterUtils.mustIncludeFilter(gridFilter))),
    );
    this.gridService.addFilters(buildFilters([
      serverBackedGridTextFilter('login'),
      serverBackedGridDateRangeFilter('connectionDate')
    ]));
  }

  ngAfterViewInit() {
    this.gridService.refreshData();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openExportDialog(): void {
    this.gridService.activeFilters$
      .pipe(take(1))
      .subscribe((activeFilters: GridFilter[]) => {
        this.dialogService.openDialog<ConnectionLogExportDialogData, void>({
          viewContainerReference: this.viewContainerRef,
          component: ConnectionLogExportDialogComponent,
          id: ConnectionLogExportDialogComponent.DIALOG_ID,
          data: {
            filterValues: GridFilterUtils.createRequestFilters(activeFilters),
          }
        });
      });
  }

  shouldShowResetFilterLink(activeFilters: GridFilter[]): boolean {
    return activeFilters.length > 0;
  }

  resetFilters() {
    this.gridService.resetFilters();
  }
}
