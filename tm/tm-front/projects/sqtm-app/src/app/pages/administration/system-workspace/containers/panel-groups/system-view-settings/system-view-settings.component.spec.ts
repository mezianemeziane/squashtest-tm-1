import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SystemViewSettingsComponent } from './system-view-settings.component';
import {EMPTY} from 'rxjs';
import {SystemViewService} from '../../../services/system-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('SystemViewSettingsComponent', () => {
  let component: SystemViewSettingsComponent;
  let fixture: ComponentFixture<SystemViewSettingsComponent>;

  const systemViewService = jasmine.createSpyObj(['load']);
  systemViewService['componentData$'] = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemViewSettingsComponent ],
      providers: [
        {
          provide: SystemViewService,
          useValue: systemViewService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemViewSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
