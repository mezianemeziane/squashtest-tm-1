import {TestBed} from '@angular/core/testing';

import {ExecutionRunnerService} from './execution-runner.service';
import {
  AttachmentService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  ExecutionModel,
  ExecutionService,
  ExecutionStepService,
  InterWindowCommunicationService,
  NO_PERMISSIONS,
  ProjectData,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {of} from 'rxjs';
import SpyObj = jasmine.SpyObj;
import {EXECUTION_COVERAGE_TABLE} from '../../components/coverage-table/coverage-table.component';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';

const projectData: ProjectData = {
  id: 1,
  allowAutomationWorkflow: false,
  customFieldBinding: null,
  label: 'project1',
  name: 'project1',
  requirementCategory: null,
  testCaseNature: null,
  testCaseType: null,
  uri: '',
  permissions: NO_PERMISSIONS,
  bugTracker: null,
  milestones: [],
  automationWorkflowType: 'NATIVE',
  taServer: null,
  disabledExecutionStatus: [],
  keywords: [],
  bddScriptLanguage: 'ENGLISH',
  allowTcModifDuringExec: true,
  activatedPlugins: null
};

const executionModel: ExecutionModel = {
  projectId: 1,
  id: 1,
  customFieldValues: [],
  attachmentList: {id: 1, attachments: []},
  name: 'Test Case 1',
  prerequisite: '',
  executionOrder: 2,
  denormalizedCustomFieldValues: [],
  comment: '',
  tcDescription: '',
  tcStatus: 'WORK_IN_PROGRESS',
  tcNatLabel: 'sdv',
  tcNatIconName: '',
  tcImportance: 'LOW',
  tcTypeLabel: 'gsvdfv',
  tcTypeIconName: '',
  executionStepViews: [
    {
      id: 2,
      projectId: 1,
      order: 0,
      executionStatus: 'READY',
      customFieldValues: [],
      attachmentList: {id: 1, attachments: []},
      action: '',
      expectedResult: '',
      comment: '',
      denormalizedCustomFieldValues: [],
      lastExecutedOn: null,
      lastExecutedBy: 'admin'
    },
    {
      id: 1,
      projectId: 1,
      executionStatus: 'READY',
      order: 1,
      customFieldValues: [],
      attachmentList: {id: 1, attachments: []},
      action: '',
      expectedResult: '',
      comment: '',
      denormalizedCustomFieldValues: [],
      lastExecutedOn: null,
      lastExecutedBy: 'admin'
    },
    {
      id: 3,
      projectId: 1,
      executionStatus: 'READY',
      order: 2,
      customFieldValues: [],
      attachmentList: {id: 1, attachments: []},
      action: '',
      expectedResult: '',
      comment: '',
      denormalizedCustomFieldValues: [],
      lastExecutedOn: null,
      lastExecutedBy: 'admin'
    }
  ],
  coverages: [],
  executionMode: 'MANUAL',
  lastExecutedOn: null,
  lastExecutedBy: 'admin',
  executionStatus: 'READY',
  automatedJobUrl: null,
  testAutomationServerKind: null,
  automatedExecutionResultUrl: null,
  automatedExecutionResultSummary: null,
  nbIssues: 0,
  iterationId: -1,
  kind: 'STANDARD',
  milestones: []
};

describe('ExecutionRunnerService', () => {
  const executionServiceMock: SpyObj<ExecutionService> = jasmine.createSpyObj('ExecutionService', ['fetchExecutionData']);
  const executionStepServiceMock: SpyObj<ExecutionStepService> = jasmine.createSpyObj('ExecutionStepService', ['changeStatus']);
  const attachmentHelperMock = jasmine.createSpyObj('attachmentHelper', ['initializeAttachmentState', 'mapUploadEventToState']);
  const customFieldHelper = jasmine.createSpyObj('customFieldHelper', ['initializeCustomFieldValueState', 'updateCustomFieldValue']);
  const referentialDataServiceMock = jasmine.createSpyObj('referentialDataService', ['refresh', 'connectToProjectData']);
  const coverageTableMock = jasmine.createSpyObj('coverageTable', ['loadInitialData']);
  referentialDataServiceMock.globalConfiguration$ = of({
    milestoneFeatureEnabled: false,
    uploadFileExtensionWhitelist: ['txt'],
    uploadFileSizeLimit: 1000
  });

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
      AppTestingUtilsModule,
      TranslateModule.forRoot()
    ],
    providers: [
      {
        provide: ReferentialDataService,
        useValue: referentialDataServiceMock
      },
      {
        provide: ExecutionService,
        useValue: executionServiceMock
      },
      {
        provide: ExecutionStepService,
        useValue: executionStepServiceMock
      },
      {
        provide: EXECUTION_COVERAGE_TABLE,
        useValue: coverageTableMock
      },
      {
        provide: ExecutionRunnerService,
        useClass: ExecutionRunnerService,
        deps: [
          RestService,
          ReferentialDataService,
          AttachmentService,
          TranslateService,
          CustomFieldValueService,
          EntityViewAttachmentHelperService,
          EntityViewCustomFieldHelperService,
          ExecutionService,
          EXECUTION_COVERAGE_TABLE,
          InterWindowCommunicationService,
          ExecutionStepService,
        ]
      }
    ]
  }));

  it('it should load execution model', (done) => {
    referentialDataServiceMock.connectToProjectData.and.returnValue(of(projectData));
    executionServiceMock.fetchExecutionData.and.returnValue(of(executionModel));
    const service: ExecutionRunnerService = TestBed.inject(ExecutionRunnerService);
    service.componentData$.subscribe((data) => {
      expect(data.execution.id).toEqual(1);
      expect(data.execution.name).toEqual('Test Case 1');
      expect(data.execution.projectId).toEqual(1);
      expect(data.execution.executionOrder).toEqual(2);
      const ids = data.execution.executionSteps.ids;
      expect(ids).toEqual([2, 1, 3]);
      done();
    });
    service.load(1);
  });

  it('it should change status', (done) => {
    referentialDataServiceMock.connectToProjectData.and.returnValue(of(projectData));
    executionStepServiceMock.changeStatus.and.returnValue(of(null));
    executionServiceMock.fetchExecutionData.and.returnValue(of(executionModel));
    const service: ExecutionRunnerService = TestBed.inject(ExecutionRunnerService);
    service.load(1);
    service.changeExecutionStepStatus(1, 'SUCCESS', 1).subscribe();
    service.componentData$.subscribe((data) => {
      expect(data.execution.executionSteps.entities[1].executionStatus).toEqual('SUCCESS');
      done();
    });
  });

  describe('Navigate after changing status', () => {
    interface DataSet {
      initialStep: number;
      isLast: boolean;
      nextStepIndex: number;
    }

    const dataSets: DataSet[] = [
      {
        initialStep: 0,
        isLast: false,
        nextStepIndex: 1
      },
      {
        initialStep: 1,
        isLast: false,
        nextStepIndex: 2
      },
      {
        initialStep: 2,
        isLast: true,
        nextStepIndex: 3
      }
    ];

    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataSet, index: number) {
      it(`Dataset ${index} - It should return navigate command`, (done) => {
        referentialDataServiceMock.connectToProjectData.and.returnValue(of(projectData));
        executionStepServiceMock.changeStatus.and.returnValue(of(null));
        executionServiceMock.fetchExecutionData.and.returnValue(of(executionModel));
        const service: ExecutionRunnerService = TestBed.inject(ExecutionRunnerService);
        service.load(1);
        service.navigateToStep(data.initialStep);
        service.changeExecutionStepStatus(1, 'SUCCESS', 1).subscribe(navigateCommand => {
          expect(navigateCommand.isLastStep).toEqual(data.isLast);
          expect(navigateCommand.nextStepIndex).toEqual(data.nextStepIndex);
          done();
        });
      });
    }
  });
});
