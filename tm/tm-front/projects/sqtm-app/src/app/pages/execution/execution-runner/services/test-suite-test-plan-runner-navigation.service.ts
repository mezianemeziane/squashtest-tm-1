import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {DialogService, TestPlanResumeModel, TestSuiteService} from 'sqtm-core';
import {TestPlanRunnerNavigationService} from './test-plan-runner-navigation.service';

@Injectable()
export class TestSuiteTestPlanRunnerNavigationService extends TestPlanRunnerNavigationService {

  constructor(router: Router, dialogService: DialogService, private testSuiteService: TestSuiteService) {
    super(router, dialogService);
  }

  protected getUrlRoot(): string[] {
    return [
      'execution-runner',
      'test-suite', this.state.testPlanOwnerId.toString(),
      'test-plan', this.state.testPlanItemId.toString()
    ];
  }

  protected getNextExecution() {
    return this.testSuiteService.getNextExecution(this.state.testPlanOwnerId, this.state.testPlanItemId);
  }

  protected extractTestPlanOwnerId(testPlanResumeModel: TestPlanResumeModel) {
    return testPlanResumeModel.testSuiteId;
  }

}
