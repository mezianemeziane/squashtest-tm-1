import {TestBed} from '@angular/core/testing';

import {ExecutionRunnerOpenerService} from './execution-runner-opener.service';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('ExecutionRunnerOpenerService', () => {
  let service: ExecutionRunnerOpenerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule],
      providers: [ExecutionRunnerOpenerService]
    });
    service = TestBed.inject(ExecutionRunnerOpenerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
