import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ExecutionRunnerOptionalStatusComponent} from './execution-runner-optional-status.component';
import {ExecutionStatusKeys} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {OnPushComponentTester} from '../../../../../utils/testing-utils/on-push-component-tester';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

class ExecutionRunnerOptionalStatusTester extends OnPushComponentTester<ExecutionRunnerOptionalStatusComponent> {
  // [data-test-execution-status="${this.executionStatus}"]
  getStatusButton(executionStatus: ExecutionStatusKeys) {
    return this.element(`[data-test-execution-status="${executionStatus}"]`);
  }
}

describe('ExecutionRunnerOptionalStatusComponent', () => {
  let tester: ExecutionRunnerOptionalStatusTester;
  let fixture: ComponentFixture<ExecutionRunnerOptionalStatusComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [ExecutionRunnerOptionalStatusComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionRunnerOptionalStatusComponent);
    tester = new ExecutionRunnerOptionalStatusTester(fixture);
    tester.componentInstance.disabledExecutionStatus = ['SETTLED'];
    tester.detectChanges();
  });

  it('should show only required buttons', () => {
    expect(tester.componentInstance).toBeTruthy();
    expect(tester.getStatusButton('SETTLED')).toBeFalsy();
    expect(tester.getStatusButton('READY')).toBeTruthy();
    expect(tester.getStatusButton('UNTESTABLE')).toBeTruthy();
    tester.componentInstance.disabledExecutionStatus = ['SETTLED', 'UNTESTABLE'];
    tester.detectChanges();
    expect(tester.getStatusButton('SETTLED')).toBeFalsy();
    expect(tester.getStatusButton('READY')).toBeTruthy();
    expect(tester.getStatusButton('UNTESTABLE')).toBeFalsy();
    tester.componentInstance.disabledExecutionStatus = [];
    tester.detectChanges();
    expect(tester.getStatusButton('SETTLED')).toBeTruthy();
    expect(tester.getStatusButton('READY')).toBeTruthy();
    expect(tester.getStatusButton('UNTESTABLE')).toBeTruthy();
  });
});
