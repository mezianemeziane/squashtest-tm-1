import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ExecutionPageComponent} from './execution-page.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateService} from '@ngx-translate/core';
import {DialogService} from 'sqtm-core';
import {EMPTY} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ExecutionPageService} from '../../services/execution-page.service';
import {DatePipe} from '@angular/common';

describe('ExecutionPageComponent', () => {
  let component: ExecutionPageComponent;
  let fixture: ComponentFixture<ExecutionPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, RouterTestingModule],
      declarations: [ ExecutionPageComponent ],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: DialogService,
          useValue: jasmine.createSpyObj(['openDeletionConfirm', 'openAlert'])
        },
        {
          provide: ExecutionPageService,
          useValue: {
            componentData$: EMPTY,
            loaded$: EMPTY
          }
        },
        DatePipe
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
