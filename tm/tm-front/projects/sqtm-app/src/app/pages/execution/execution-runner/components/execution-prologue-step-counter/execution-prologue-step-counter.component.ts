import {Component, OnInit, ChangeDetectionStrategy, Input} from '@angular/core';

@Component({
  selector: 'sqtm-app-execution-prologue-step-counter',
  templateUrl: './execution-prologue-step-counter.component.html',
  styleUrls: ['./execution-prologue-step-counter.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionPrologueStepCounterComponent implements OnInit {

  @Input()
  stepCount: number;

  constructor() { }

  ngOnInit() {
  }

}
