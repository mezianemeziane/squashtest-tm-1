import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ViewChildren,
  QueryList,
  NgZone,
  Inject, ViewChild, ElementRef, AfterViewInit, OnDestroy, ChangeDetectorRef
} from '@angular/core';
import {ExecutionPageComponentData} from '../../../containers/abstract-execution-page.component';
import {ExecutionStepState} from '../../../../states/execution-state';
import {ExecutionStepComponent} from '../../execution-step/execution-step.component';
import {ExecutionPageService} from '../../../services/execution-page.service';
import {BehaviorSubject, fromEvent, Observable} from 'rxjs';
import {DOCUMENT} from '@angular/common';
import {take, tap} from 'rxjs/operators';
import {
  DataRow,
  IssuesService,
  RestService
} from 'sqtm-core';
import {GridResponse} from 'sqtm-core';


@Component({
  selector: 'sqtm-app-execution-page-scenario-panel',
  templateUrl: './execution-page-scenario-panel.component.html',
  styleUrls: ['./execution-page-scenario-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionPageScenarioPanelComponent implements OnInit, AfterViewInit, OnDestroy {

  get steps(): ExecutionStepState[] {
    return Object.values(this.executionPageComponentData.execution.executionSteps.entities);
  }

  @Input()
  executionPageComponentData: ExecutionPageComponentData;

  @Input()
  initialScrollTop: number;

  @Input()
  extendedPrerequisite: boolean;

  @ViewChildren(ExecutionStepComponent)
  executionSteps: QueryList<ExecutionStepComponent>;

  @ViewChild('scrollableStepList', {read: ElementRef, static: true})
  private scrollableStepList: ElementRef;

  issues: DataRow[] = [];

  private scrollTop = 0;
  public fraction = 1.3;
  private readonly _authenticated: BehaviorSubject<boolean>;
  public authenticated$: Observable<boolean>;

  constructor(private executionPageService: ExecutionPageService,
              @Inject(DOCUMENT) private document: Document,
              private ngZone: NgZone,
              private issuesService: IssuesService,
              private restService: RestService,
              private cdRef: ChangeDetectorRef) {
    this._authenticated = new BehaviorSubject<boolean>(false);
    this.authenticated$ = this._authenticated;
  }

  ngOnInit(): void {
    if (this.executionPageComponentData.projectData.bugTracker) {
      this.issuesService.loadModel(this.executionPageComponentData.execution.id, 'EXECUTION_TYPE').subscribe(issueModel => {
        const isAuthenticated = issueModel.modelLoaded && issueModel.bugTrackerStatus === 'AUTHENTICATED';
        this._authenticated.next(isAuthenticated);
        this.loadIssuesData(isAuthenticated);
      });
    }
  }

  loadIssuesData(authenticated: boolean) {
    if (authenticated) {
      const executionId = this.executionPageComponentData.execution.id;
      this.restService.post([`issues/execution/${executionId}/all-known-issues`]).pipe(
        take(1),
        tap((gridResponse: GridResponse) => {

          this.issues = gridResponse.dataRows;
          this.cdRef.markForCheck();
        }),
      ).subscribe();
    }
  }

  filterIssuesByStep(stepOrder: number) {
    return this.issues.filter(issue => issue.data['stepOrder'] === stepOrder.toString());
  }

  ngAfterViewInit() {
    this.ngZone.runOutsideAngular(() => {
      const scrollableDiv = this.scrollableStepList.nativeElement as HTMLDivElement;
      if (this.initialScrollTop) {
        scrollableDiv.scroll({top: this.initialScrollTop});
      }

      fromEvent(this.scrollableStepList.nativeElement, 'scroll')
        .subscribe(($event: MouseEvent) => {
          this.scrollTop = $event.target['scrollTop'];
        });
    });
  }

  ngOnDestroy() {
    this.executionPageService.updateStepViewScroll(this.scrollTop);
  }

  trackStepFn(index: number, step: ExecutionStepState) {
    return step.order;
  }

  togglePrerequisite() {
    this.executionPageService.togglePrerequisite();
  }

  collapseAllSteps() {
    this.executionPageService.collapseAllSteps();
  }

  expendAllSteps() {
    this.executionPageService.expendAllSteps();
  }

  resizeActionGrid(fraction: number) {
    this.fraction = fraction;
    this.executionSteps.forEach(executionStepComponent => executionStepComponent.setFaction(fraction));
  }

  getExecutionPrerequisite() {
    if (this.executionPageComponentData.execution.kind === 'GHERKIN') {
      const background = this.executionPageComponentData.execution.prerequisite;
      return background.startsWith('</br>') ? background.substring(5, background.length) : background;
    } else {
      return this.executionPageComponentData.execution.prerequisite;
    }
  }
}
