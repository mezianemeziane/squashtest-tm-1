import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {
  AttachmentService,
  CampaignPermissions,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewComponentData,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ExecutionService,
  ExecutionStepService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
  InterWindowCommunicationService,
  GenericEntityViewService
} from 'sqtm-core';
import {map, switchMap, take} from 'rxjs/operators';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ExecutionRunnerService} from '../../services/execution-runner.service';
import {
  EXECUTION_COVERAGE_TABLE,
  EXECUTION_COVERAGE_TABLE_CONF,
  executionCoverageTableDefinition
} from '../../../components/coverage-table/coverage-table.component';
import {ExecutionState} from '../../../states/execution-state';

@Component({
  selector: 'sqtm-app-execution-runner',
  templateUrl: './execution-runner.component.html',
  styleUrls: ['./execution-runner.component.less'],
  providers: [
    {
      provide: EXECUTION_COVERAGE_TABLE_CONF,
      useFactory: executionCoverageTableDefinition
    },
    {
      provide: EXECUTION_COVERAGE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, EXECUTION_COVERAGE_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: ExecutionRunnerService,
      useClass: ExecutionRunnerService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        CustomFieldValueService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        ExecutionService,
        EXECUTION_COVERAGE_TABLE,
        InterWindowCommunicationService,
        ExecutionStepService,
      ]
    },
    {
      provide: EntityViewService,
      useExisting: ExecutionRunnerService
    },
    {
      provide: GenericEntityViewService,
      useExisting: ExecutionRunnerService
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionRunnerComponent implements OnInit, OnDestroy {

  constructor(
    private route: ActivatedRoute,
    private executionRunnerService: ExecutionRunnerService,
    private referentialDataService: ReferentialDataService) {
  }

  ngOnDestroy(): void {
    this.executionRunnerService.complete();
    }

  ngOnInit() {
    this.referentialDataService.refresh().pipe(
      switchMap(() => this.route.paramMap),
      take(1),
      map((params: ParamMap) => params.get('executionId')),
    ).subscribe((executionIdParam: string) => {
      const executionId = parseInt(executionIdParam, 10);
      this.executionRunnerService.load(executionId);
    });
  }

}

export interface ExecutionRunnerComponentData extends EntityViewComponentData<ExecutionState, 'execution', CampaignPermissions> {
}
