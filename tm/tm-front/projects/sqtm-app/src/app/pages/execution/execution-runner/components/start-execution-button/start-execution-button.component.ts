import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ExecutionRunnerNavigationService} from '../../services/execution-runner-navigation.service';

@Component({
  selector: 'sqtm-app-start-execution-button',
  templateUrl: './start-execution-button.component.html',
  styleUrls: ['./start-execution-button.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StartExecutionButtonComponent implements OnInit {

  @Input()
  executionId: number;

  constructor(private navigationService: ExecutionRunnerNavigationService) {
  }

  ngOnInit() {
  }

  startExecution() {
    this.navigationService.startExecution(this.executionId);
  }

}
