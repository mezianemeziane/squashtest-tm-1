import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ExecutionRunnerNavigationService} from '../../services/execution-runner-navigation.service';
import {SingleExecutionRunnerNavigationService} from '../../services/single-execution-runner-navigation.service';

@Component({
  selector: 'sqtm-app-single-execution-runner',
  templateUrl: './single-execution-runner.component.html',
  styleUrls: ['./single-execution-runner.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ExecutionRunnerNavigationService,
      useClass: SingleExecutionRunnerNavigationService
    }
  ]
})
export class SingleExecutionRunnerComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
