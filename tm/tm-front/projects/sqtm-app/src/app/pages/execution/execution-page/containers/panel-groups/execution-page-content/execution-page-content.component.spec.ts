import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ExecutionPageContentComponent} from './execution-page-content.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateService} from '@ngx-translate/core';
import {EMPTY} from 'rxjs';
import {ExecutionPageService} from '../../../services/execution-page.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogService} from 'sqtm-core';
import {mockDialogService} from '../../../../../../utils/testing-utils/mocks.service';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';

describe('ExecutionPageContentComponent', () => {
  let component: ExecutionPageContentComponent;
  let fixture: ComponentFixture<ExecutionPageContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, NzDropDownModule],
      declarations: [ExecutionPageContentComponent],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj('TranslateService', ['instant']),
        },
        {
          provide: ExecutionPageService,
          useValue: {
            componentData$: EMPTY
          }
        },
        {
          provide: DialogService,
          useValue: mockDialogService(),
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionPageContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


