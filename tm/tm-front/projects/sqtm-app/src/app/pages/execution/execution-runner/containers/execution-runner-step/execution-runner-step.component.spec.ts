import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ExecutionRunnerStepComponent} from './execution-runner-step.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ExecutionRunnerService} from '../../services/execution-runner.service';
import {RouterTestingModule} from '@angular/router/testing';
import {EMPTY} from 'rxjs';
import SpyObj = jasmine.SpyObj;
import {TranslateModule} from '@ngx-translate/core';
import {DialogService} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {ExecutionRunnerNavigationService} from '../../services/execution-runner-navigation.service';
import {SingleExecutionRunnerNavigationService} from '../../services/single-execution-runner-navigation.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

const executionRunnerServiceMock: SpyObj<ExecutionRunnerService> =
  jasmine.createSpyObj('ExecutionRunnerService', ['load', 'navigateToStep']);
executionRunnerServiceMock.componentData$ = EMPTY;

describe('ExecutionRunnerStepComponent', () => {
  let component: ExecutionRunnerStepComponent;
  let fixture: ComponentFixture<ExecutionRunnerStepComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, RouterTestingModule, TranslateModule.forRoot()],
      declarations: [ExecutionRunnerStepComponent],
      providers: [
        {
          provide: ExecutionRunnerService,
          useValue: executionRunnerServiceMock
        },
        {
          provide: DialogService,
          useValue: jasmine.createSpyObj(['openConfirm']),
        },
        {
          provide: ExecutionRunnerNavigationService,
          useClass: SingleExecutionRunnerNavigationService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionRunnerStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
