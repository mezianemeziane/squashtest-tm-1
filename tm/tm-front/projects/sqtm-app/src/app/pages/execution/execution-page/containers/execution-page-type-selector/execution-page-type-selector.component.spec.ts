import { ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import { ExecutionPageTypeSelectorComponent } from './execution-page-type-selector.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {RouterTestingModule} from '@angular/router/testing';
import {ExecutionPageService} from '../../services/execution-page.service';
import {EMPTY} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';


describe('ExecutionPageTypeSelectorComponent', () => {
  let component: ExecutionPageTypeSelectorComponent;
  let fixture: ComponentFixture<ExecutionPageTypeSelectorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, RouterTestingModule],
      declarations: [ ExecutionPageTypeSelectorComponent ],
      providers: [
        {
          provide: ExecutionPageService,
          useValue: {
            componentData$: EMPTY,
            loaded$: EMPTY
          }
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionPageTypeSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
