import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {
  AbstractCellRendererComponent,
  GridService,
  DataRow,
  ColumnDefinitionBuilder,
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-issue-reported-in-cell',
  template: `
    <ng-container *ngIf="row">
      <span class="full-width full-height flex-column">
        <label class="sqtm-grid-cell-txt-renderer"
               style="margin-top: auto;
               margin-bottom: auto"
               [sqtmCoreLabelTooltip]="row.data[columnDisplay.id]"
               nz-tooltip [nzTooltipTitle]="" [nzTooltipPlacement]="'topLeft'">
          {{getLabel(row)}}
        </label>
      </span>
    </ng-container>`,
  styleUrls: ['./issue-reported-in-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueReportedInCellComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public girdService: GridService, public cdRef: ChangeDetectorRef, private translateService: TranslateService) {
    super(girdService, cdRef);
  }

  ngOnInit(): void {
  }

  getLabel(row: DataRow) {
    const stepOrder: string = row.data['stepOrder'];
    let label;
    if (stepOrder !== '') {
      const correctStepOrder = Number(stepOrder) + 1;
      label = `${this.translateService.instant('sqtm-core.campaign-workspace.execution-page.step.label')} ` + correctStepOrder;
    } else {
      label = `${this.translateService.instant('sqtm-core.campaign-workspace.execution-page.this-execution.label')}`;
    }
    return label;
  }

}

export function issueIssueReportedInColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(IssueReportedInCellComponent);
}
