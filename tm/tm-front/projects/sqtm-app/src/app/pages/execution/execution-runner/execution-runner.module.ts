import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ExecutionRunnerComponent} from './containers/execution-runner/execution-runner.component';
import {ExecutionPrologueComponent} from './containers/execution-prologue/execution-prologue.component';
import {RouterModule, Routes} from '@angular/router';
import {
  AttachmentModule,
  CellRendererCommonModule,
  CustomFieldModule,
  ExecutionUiModule,
  GridModule,
  IssuesModule,
  UiManagerModule,
  WorkspaceCommonModule
} from 'sqtm-core';
import {EXECUTION_RUNNER_PROLOGUE_URL} from './execution-runner.constant';
import {TranslateModule} from '@ngx-translate/core';
import {ExecutionPrologueStepCounterComponent} from './components/execution-prologue-step-counter/execution-prologue-step-counter.component';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCollapseModule} from 'ng-zorro-antd/collapse';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzPopoverModule} from 'ng-zorro-antd/popover';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {ExecutionRunnerStepComponent} from './containers/execution-runner-step/execution-runner-step.component';
import {StartExecutionButtonComponent} from './components/start-execution-button/start-execution-button.component';
import {ExecutionRunnerToolbarComponent} from './components/execution-runner-toolbar/execution-runner-toolbar.component';
import {ExecutionRunnerStatusButtonComponent} from './components/execution-runner-status-button/execution-runner-status-button.component';
import {ExecutionStepBodyComponent} from './components/execution-step-body/execution-step-body.component';
import {ExecutionModule} from '../../../components/execution/execution.module';
import {ExecutionPrologueBodyComponent} from './components/execution-prologue-body/execution-prologue-body.component';
import {CoverageTableComponent} from '../components/coverage-table/coverage-table.component';
import {ExecutionRunnerOptionalStatusComponent} from './components/execution-runner-optional-status/execution-runner-optional-status.component';
import {ExecutionStepIssuesComponent} from './components/execution-step-issues/execution-step-issues.component';
import {RemoteIssueModule} from '../../../components/remote-issue/remote-issue.module';
import {RemoveExecutionStepIssueCellComponent} from './components/remove-execution-step-issue-cell/remove-execution-step-issue-cell.component';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {IterationTestPlanRunnerComponent} from './containers/iteration-test-plan-runner/iteration-test-plan-runner.component';
import {SingleExecutionRunnerComponent} from './containers/single-execution-runner/single-execution-runner.component';
import {TestSuiteTestPlanRunnerComponent} from './containers/test-suite-test-plan-runner/test-suite-test-plan-runner.component';


export const routes: Routes = [
  {
    path: 'execution',
    component: SingleExecutionRunnerComponent,
    children: [
      {
        path: ':executionId',
        component: ExecutionRunnerComponent,
        children: [
          {
            path: `${EXECUTION_RUNNER_PROLOGUE_URL}`,
            component: ExecutionPrologueComponent
          },
          {
            path: `step/:stepIndex`,
            component: ExecutionRunnerStepComponent
          }
        ]
      },
    ]
  },
  {
    path: 'iteration/:iterationId/test-plan/:testPlanItemId',
    component: IterationTestPlanRunnerComponent,
    children: [
      {
        path: 'execution/:executionId',
        component: ExecutionRunnerComponent,
        children: [
          {
            path: `${EXECUTION_RUNNER_PROLOGUE_URL}`,
            component: ExecutionPrologueComponent
          },
          {
            path: `step/:stepIndex`,
            component: ExecutionRunnerStepComponent
          }
        ]
      }
    ]
  },
  {
    path: 'test-suite/:testSuiteId/test-plan/:testPlanItemId',
    component: TestSuiteTestPlanRunnerComponent,
    children: [
      {
        path: 'execution/:executionId',
        component: ExecutionRunnerComponent,
        children: [
          {
            path: `${EXECUTION_RUNNER_PROLOGUE_URL}`,
            component: ExecutionPrologueComponent
          },
          {
            path: `step/:stepIndex`,
            component: ExecutionRunnerStepComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  declarations: [
    ExecutionRunnerComponent,
    ExecutionPrologueComponent,
    ExecutionPrologueStepCounterComponent,
    ExecutionRunnerStepComponent,
    StartExecutionButtonComponent,
    ExecutionRunnerToolbarComponent,
    ExecutionRunnerStatusButtonComponent,
    ExecutionStepBodyComponent,
    ExecutionPrologueBodyComponent,
    CoverageTableComponent,
    ExecutionRunnerOptionalStatusComponent,
    ExecutionStepIssuesComponent,
    RemoveExecutionStepIssueCellComponent,
    IterationTestPlanRunnerComponent,
    SingleExecutionRunnerComponent,
    TestSuiteTestPlanRunnerComponent,
  ],
  exports: [
    ExecutionRunnerComponent,
    CoverageTableComponent
  ],
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),
    UiManagerModule,
    NzIconModule,
    NzToolTipModule,
    NzButtonModule,
    WorkspaceCommonModule,
    ExecutionUiModule,
    NzCollapseModule,
    ExecutionModule,
    CustomFieldModule,
    AttachmentModule,
    GridModule,
    CellRendererCommonModule,
    NzPopoverModule,
    IssuesModule,
    RemoteIssueModule,
    NzDropDownModule,
  ]
})
export class ExecutionRunnerModule {
}
