import { Injectable } from '@angular/core';
import {
  AttachmentService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  ExecutionService,
  ExecutionStatusKeys,
  ExecutionStepService,
  GridService,
  InterWindowCommunicationService,
  ReferentialDataService,
  RestService,
  StoreOptions
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {map, take, tap, withLatestFrom} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {AbstractExecutionService} from '../../service/abstract-execution.service';
import {ExecutionPageState, provideInitialPageState} from '../states/execution-page-state';
import {executionStepAdapter, ExecutionStepState} from '../../states/execution-state';
import {Update} from '@ngrx/entity';
import {UpdateStatusResponse} from 'sqtm-core';

const storeOptions: StoreOptions = {
  id: 'ExecutionPageStore',
  logDiff: 'detailed'
};

@Injectable()
export class ExecutionPageService  extends AbstractExecutionService {

  public readonly coverageCount$: Observable<number>;

  constructor(restService: RestService,
              referentialDataService: ReferentialDataService,
              attachmentService: AttachmentService,
              translateService: TranslateService,
              customFieldValueService: CustomFieldValueService,
              attachmentHelper: EntityViewAttachmentHelperService,
              customFieldHelper: EntityViewCustomFieldHelperService,
              executionService: ExecutionService,
              coverageTable: GridService,
              interWindowCommunicationService: InterWindowCommunicationService,
              executionStepService: ExecutionStepService,
) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
      executionService,
      coverageTable,
      storeOptions,
      interWindowCommunicationService,
      executionStepService
    );

    this.coverageCount$ = this.componentData$.pipe(
      map(componentData => componentData.execution.coverages.length)
    );
  }


  complete() {
    super.complete();
  }

  getInitialState(): ExecutionPageState {
    return provideInitialPageState();
  }

  removeIssues(issueIds: number[]): Observable<any> {
    return this.removeIssuesServerSide(issueIds).pipe(
      take(1),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, ExecutionPageState]) => {
        return this.updateStateWithRemovedIssues(state, issueIds);
      }),
      tap((newState) => this.store.commit(newState))
    );
  }

  private removeIssuesServerSide(issueIds: number[]) {
    const issuesIds = issueIds.join(',');
    return this.restService.delete(['issues', issuesIds]);
  }

  private updateStateWithRemovedIssues(state: ExecutionPageState, issueIds: number[]) {
    const updatedNbIssues = state.execution.nbIssues - issueIds.length;
    return {
      ...state,
      execution: {
        ...state.execution,
        nbIssues: updatedNbIssues,
      }
    };
  }

  updateStateAfterAddingIssue() {
    this.state$.pipe(
      take(1),
      map((state: ExecutionPageState) => {
        return {...state,
          execution:
            {...state.execution,
              nbIssues: state.execution.nbIssues + 1
            }
        };
      })
    ).subscribe(state => this.commit(state));
  }

  togglePrerequisite() {
    this.store.state$.pipe(
      take(1),
      map((state: ExecutionPageState) => {
        const updatedExtentedPrerequisite = !state.execution.extendedPrerequisite;
        return {
          ...state,
          execution: {
            ...state.execution,
            extendedPrerequisite: updatedExtentedPrerequisite,
          }
        };
      }),
    ).subscribe(state => this.commit(state));
  }

  expendAllSteps() {
    this.store.state$.pipe(
      take(1),
      map((state: ExecutionPageState) => {
        let executionSteps = {...state.execution.executionSteps};
        const changes: Update<ExecutionStepState>[] = (executionSteps.ids as number[]).map((id) => {
          return {id: id, changes: {extended: true}};
        });
        executionSteps = executionStepAdapter.updateMany(changes, executionSteps);
        return {...state,
          execution: {
          ...state.execution,
            extendedPrerequisite: true,
            executionSteps: executionSteps,
          }
        };
      }),
    ).subscribe(state => this.commit(state));
  }

  collapseAllSteps() {
    this.store.state$.pipe(
      take(1),
      map((state: ExecutionPageState) => {
        let executionSteps = {...state.execution.executionSteps};
        const changes: Update<ExecutionStepState>[] = (executionSteps.ids as number[]).map((id) => {
          return {id: id, changes: {extended: false}};
        });
        executionSteps = executionStepAdapter.updateMany(changes, executionSteps);
        return {...state,
          execution: {
            ...state.execution,
            extendedPrerequisite: false,
            executionSteps: executionSteps,
          }
        };
      }),
    ).subscribe(state => this.commit(state));
  }

  updateStepViewScroll(scrollTop: number) {
    this.state$.pipe(
      take(1),
      map((state:  ExecutionPageState) => {
        return {...state,
          execution: {
            ...state.execution,
            scrollTop: scrollTop
          }
        };
      })
    ).subscribe(state => this.commit(state));
  }

  toggleStep(id: number) {
    return this.store.state$.pipe(
      take(1),
      map((state: ExecutionPageState) => {
        let executionSteps = {...state.execution.executionSteps};
        const executionStep = executionSteps.entities[id];
        executionSteps = executionStepAdapter.updateOne({id: id, changes: {extended: !executionStep.extended}}, executionSteps);
        return {...state, execution: {...state.execution, executionSteps}};
      }),
    ).subscribe(state => this.commit(state));
  }

  changeExecutionStepStatus(executionStepId: number, executionStatus: ExecutionStatusKeys, executionId) {
    return this.executionStepService.changeStatus(executionStepId, executionStatus, executionId).pipe(
      withLatestFrom(this.state$),
      map(([response, state]: [UpdateStatusResponse, ExecutionPageState]) => {
        const executionSteps = executionStepAdapter.updateOne({
          id: executionStepId,
          changes: {
            executionStatus, lastExecutedOn: response.stepLastExecutedOn, lastExecutedBy: response.stepLastExecutedBy}
        }, state.execution.executionSteps);
        return {...state,
          execution:
            {...state.execution,
              executionStatus: response.executionStatus,
              lastExecutedOn:  response.lastExecutedOn,
              lastExecutedBy:  response.lastExecutedBy,
              executionSteps
            }
        };
      }),
    ).subscribe(state => this.commit(state));
  }
}
