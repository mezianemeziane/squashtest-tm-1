import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutionPageScenarioPanelComponent } from './execution-page-scenario-panel.component';
import {TranslateModule} from '@ngx-translate/core';
import {ExecutionPageService} from '../../../services/execution-page.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ExecutionRunnerService} from '../../../../execution-runner/services/execution-runner.service';
import SpyObj = jasmine.SpyObj;
import {ExecutionPageComponentData} from '../../../containers/abstract-execution-page.component';
import {IssuesService, RestService} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('ExecutionPageScenarioPanelComponent', () => {
  let component: ExecutionPageScenarioPanelComponent;
  let fixture: ComponentFixture<ExecutionPageScenarioPanelComponent>;
  const executionPageServiceMock: SpyObj<ExecutionRunnerService> = jasmine.createSpyObj(['load', 'updateStepViewScroll']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot()],
      declarations: [ ExecutionPageScenarioPanelComponent ],
      providers: [
        {
          provide: ExecutionPageService,
          useValue: executionPageServiceMock
        },
        RestService,
        IssuesService
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionPageScenarioPanelComponent);
    component = fixture.componentInstance;
    component.initialScrollTop = 0;
    component.extendedPrerequisite = true;

    component.executionPageComponentData = {
      execution: {
        extendedPrerequisite: true,
             executionSteps: {
          ids: [1],
          entities: {},
        },
      },
      projectData: {
        bugTracker: null
      }
    } as ExecutionPageComponentData;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
