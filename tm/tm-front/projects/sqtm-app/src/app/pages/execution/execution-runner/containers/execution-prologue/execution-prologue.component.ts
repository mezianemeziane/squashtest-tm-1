import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {ExecutionRunnerService} from '../../services/execution-runner.service';
import {ExecutionRunnerComponentData} from '../execution-runner/execution-runner.component';
import {Observable, Subject} from 'rxjs';
import {Router} from '@angular/router';
import {catchError, filter, map, take, takeUntil} from 'rxjs/operators';
import {select} from '@ngrx/store';
import {
  ActionErrorDisplayService,
  BindableEntity,
  createCustomFieldValueDataSelector,
  CustomFieldData,
  InterWindowCommunicationService,
  InterWindowMessages,
  ModifDuringExecService
} from 'sqtm-core';
import {APP_BASE_HREF, DOCUMENT} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {ExecutionState} from '../../../states/execution-state';
import {
  ExecutionRunnerOpenerService,
  RequireModifyDuringExecutionMessagePayload
} from '../../services/execution-runner-opener.service';

/** @dynamic */
@Component({
  selector: 'sqtm-app-execution-prologue',
  templateUrl: './execution-prologue.component.html',
  styleUrls: ['./execution-prologue.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionPrologueComponent implements OnInit, OnDestroy {

  componentData$: Observable<ExecutionRunnerComponentData>;

  customFieldData$: Observable<CustomFieldData[]>;

  unsub$ = new Subject<void>();

  constructor(
    public executionRunnerService: ExecutionRunnerService,
    private router: Router,
    private translateService: TranslateService,
    @Inject(DOCUMENT) private document: Document,
    @Inject(APP_BASE_HREF) private baseUrl: string,
    private interWindowCommunicationService: InterWindowCommunicationService,
    private executionRunnerOpenerService: ExecutionRunnerOpenerService,
    private modifDuringExecService: ModifDuringExecService,
    private actionErrorDisplayService: ActionErrorDisplayService,
  ) {
  }

  ngOnInit() {
    this.componentData$ = this.executionRunnerService.componentData$.pipe(
      takeUntil(this.unsub$)
    );

    this.customFieldData$ = this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createCustomFieldValueDataSelector(BindableEntity.EXECUTION))
    );

    this.componentData$.pipe(
      take(1)
    ).subscribe(data => {
      this.changeDocumentTitle(data.execution);
    });
  }


  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private changeDocumentTitle(executionState: ExecutionState) {
    // tslint:disable-next-line:max-line-length
    let title = `${this.translateService.instant('sqtm-core.entity.execution.label.short')} #${executionState.executionOrder + 1} - ${executionState.name}`;
    if (executionState.datasetLabel) {
      title = title.concat(` - ${executionState.datasetLabel}`);
    }
    title = title.concat(` (${this.translateService.instant('sqtm-core.entity.execution.preamble')})`);
    this.document.title = title;
  }

  addAttachment(files: File[], attachmentListId: number) {
    this.componentData$.pipe(
      take(1),
      filter(componentData => componentData.milestonesAllowModification),
      map(() => this.executionRunnerService.addAttachments(files, attachmentListId))
    ).subscribe();
  }

  modifyTestDuringExec(testCaseId: number, executionId: number) {
    this.modifDuringExecService.checkPermissionsOnPrologue(executionId).pipe(
      catchError(err => this.actionErrorDisplayService.handleActionError(err))
    ).subscribe(() => {
      if (this.interWindowCommunicationService.isAbleToSendMessagesToOpener()) {
        // we send back the command to initial opener so the dialog is NOT the opener of the modification during exec page
        this.modifyDuringExecInOpener(testCaseId, executionId);
      } else {
        // cannot reach the initial opener, probably closed by user after starting execution. Defaulting to a classic opening from dialog
        this.modifyDuringExec(testCaseId, executionId);
      }
      window.close();
    });
  }

  private modifyDuringExec(testCaseId: number, executionId: number) {
    this.executionRunnerOpenerService.openModifyRunningExecutionFromPrologue(testCaseId, executionId);
  }

  private modifyDuringExecInOpener(testCaseId: number, executionId: number) {
    const payload: RequireModifyDuringExecutionMessagePayload = {
      testCaseId,
      executionId
    };
    const message = new InterWindowMessages('REQUIRE-MODIFICATION-DURING-EXECUTION-PROLOGUE', payload);
    this.interWindowCommunicationService.sendMessage(message);
  }
}


