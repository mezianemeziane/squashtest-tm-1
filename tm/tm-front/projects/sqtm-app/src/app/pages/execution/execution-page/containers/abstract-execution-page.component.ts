import {ChangeDetectorRef, Directive, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {concatMap, filter, map, take, takeUntil} from 'rxjs/operators';
import {
  AttachmentDrawerComponent,
  AttachmentState,
  CampaignPermissions,
  DialogService,
  EntityViewComponentData,
  InterWindowCommunicationService,
  RestService,
  TestCaseExecutionMode
} from 'sqtm-core';
import {Dictionary} from '@ngrx/entity/src/models';
import {ExecutionPageService} from '../services/execution-page.service';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';
import {Observable, Subject} from 'rxjs';
import {ExecutionState, ExecutionStepState} from '../../states/execution-state';
import {ExecutionRunnerOpenerService} from '../../execution-runner/services/execution-runner-opener.service';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractExecutionPageComponent implements OnInit, OnDestroy {

  unsub$ = new Subject<void>();

  @Output()
  executionDeleted = new EventEmitter<void>();

  componentData$: Observable<ExecutionPageComponentData>;

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  protected constructor(public executionPageService: ExecutionPageService,
                        protected translateService: TranslateService,
                        protected datePipe: DatePipe,
                        protected interWindowCommunicationService: InterWindowCommunicationService,
                        protected baseUrl: string,
                        protected cdRef: ChangeDetectorRef,
                        protected dialogService: DialogService,
                        protected restService: RestService,
                        protected executionRunnerOpenerService: ExecutionRunnerOpenerService) {
    this.componentData$ = this.executionPageService.componentData$;

    // this one seem strange but is required because the observable emit when refreshing data
    // the refresh is often required when execution is in dialog and thus windows is not focused
    // angular seems to have some trouble to detect change when windows is not focused even if it's visible in background !
    this.executionPageService.loaded$.pipe(
      takeUntil(this.unsub$),
    ).subscribe(() => {
      this.cdRef.detectChanges();
    });
  }

  ngOnInit() {
    this.interWindowCommunicationService.interWindowMessages$.pipe(
      takeUntil(this.unsub$),
      filter(message => message.isTypeOf('EXECUTION-STEP-CHANGED')),
    ).subscribe(() => this.refreshData());
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private refreshData() {
    this.componentData$.pipe(
      take(1),
    ).subscribe(componentData => {
      this.executionPageService.load(componentData.execution.id);
    });
  }

  getAttachmentCount(attachmentState: AttachmentState): number {
    const attachments = Object.values(attachmentState.entities);
    return attachments.filter(attachment => attachment.kind === 'persisted-attachment').length;
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }

  getExecutionMode(statusKey: string) {
    const executionMode = TestCaseExecutionMode[statusKey];
    return {
      id: executionMode.id,
      icon: executionMode.icon,
      labelI18nKey: executionMode.i18nKey,
      titleI18nKey: 'sqtm-core.entity.execution.mode.label'
    };
  }

  getLastExecution(lastExecutedOn: Date, lastExecutedBy: string) {
    const date = this.datePipe.transform(lastExecutedOn, 'short', '', this.translateService.getBrowserLang());
    const fullLastExecInfo = date + ' (' + lastExecutedBy + ')';

    return {
      id: 'lastExecutionInfo',
      color: 'var(--container-border-color)',
      labelI18nKey: fullLastExecInfo,
    };
  }

  getStepsNumber(steps: Dictionary<ExecutionStepState>) {
    return Object.values(steps).length;
  }

  startExecutionInDialog(componentData: ExecutionPageComponentData) {
    if (componentData.milestonesAllowModification && componentData.permissions.canExecute) {
      const executionId = componentData.execution.id;
      const executionSteps = componentData.execution.executionSteps.entities;

      if (this.checkIfExecutionHasNoStep(executionSteps)) {
        this.dialogService.openAlert({
          id: 'execution-has-no-step',
          level: 'DANGER',
          titleKey: 'sqtm-core.generic.label.error',
          messageKey: 'sqtm-core.error.execution.has-no-test-step'
        });
      } else if (this.checkIfExecutionHasAlreadyBeenLaunched(componentData)) {
        this.resumeExecution(executionId, executionSteps);
      } else {
        this.openDialogWindow(executionId);
      }
    }
  }

  private checkIfExecutionHasNoStep(executionSteps: Dictionary<ExecutionStepState>) {
    return Object.values(executionSteps).length === 0;
  }

  private resumeExecution(executionId: number, executionSteps: Dictionary<ExecutionStepState>) {
    const firstStepNotInSuccess = Object.values(executionSteps).find(executionStep => executionStep.executionStatus !== 'SUCCESS');

    if (firstStepNotInSuccess) {
      const firstStepNotInSuccessOrder = firstStepNotInSuccess.order + 1;
      this.openDialogWindow(executionId, firstStepNotInSuccessOrder);
    } else {
      this.launchExecutionAtLastStep(executionId, executionSteps);
    }
  }

  private launchExecutionAtLastStep(executionId: number, executionSteps: Dictionary<ExecutionStepState>) {
    const lastStepOrder = Object.values(executionSteps).length;
    this.openDialogWindow(executionId, lastStepOrder);
  }

  private openDialogWindow(executionId: number, stepOrder?: number) {
    stepOrder ? this.executionRunnerOpenerService.openExecutionAtStep(executionId, stepOrder) :
                this.executionRunnerOpenerService.openExecutionPrologue(executionId);
  }

  checkIfExecutionHasAlreadyBeenLaunched(componentData: ExecutionPageComponentData) {
    const executionSteps = componentData.execution.executionSteps.entities;
    return Object.values(executionSteps).some(executionStep => executionStep.executionStatus !== 'READY');
  }

  getI18nLabelForDialogStart(componentData) {
    return this.checkIfExecutionHasAlreadyBeenLaunched(componentData) ?
      'sqtm-core.campaign-workspace.execution-buttons.resume' :
      'sqtm-core.campaign-workspace.execution-buttons.launch';
  }


  deleteExecution() {
    this.componentData$.pipe(
      take(1),
      filter((componentData: ExecutionPageComponentData) =>
        componentData.milestonesAllowModification && componentData.permissions.canDelete),
      concatMap((componentData: ExecutionPageComponentData) => this.showConfirmDeleteExecutionDialog(componentData)),
      filter(({confirmDelete}) => confirmDelete),
      concatMap(({componentData}) => this.deleteExecutionServerSide(componentData))
    ).subscribe(() => this.executionDeleted.emit());
  }

  private showConfirmDeleteExecutionDialog(componentData)
    : Observable<{ confirmDelete: boolean, componentData: ExecutionPageComponentData }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.remove-execution',
      messageKey: 'sqtm-core.campaign-workspace.dialog.message.remove-execution',
      level: 'DANGER'
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, componentData}))
    );
  }

  private deleteExecutionServerSide(componentData: ExecutionPageComponentData): Observable<void> {
    const iterationId = componentData.execution.iterationId.toString();
    const executionId = componentData.execution.id.toString();
    return this.restService.delete(['iteration', iterationId, 'test-plan/execution', executionId]);
  }

}


export interface ExecutionPageComponentData extends EntityViewComponentData<ExecutionState, 'execution', CampaignPermissions> {
}
