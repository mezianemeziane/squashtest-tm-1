import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ExecutionPageCommentsPanelComponent} from './execution-page-comments-panel.component';
import {ExecutionPageService} from '../../../services/execution-page.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {TranslateModule} from '@ngx-translate/core';
import {ExecutionPageComponentData} from '../../../containers/abstract-execution-page.component';

describe('ExecutionPageCommentsPanelComponent', () => {
  let component: ExecutionPageCommentsPanelComponent;
  let fixture: ComponentFixture<ExecutionPageCommentsPanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ExecutionPageCommentsPanelComponent],
      providers: [
        {
          provide: ExecutionPageService,
          useValue: EMPTY
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionPageCommentsPanelComponent);
    component = fixture.componentInstance;
    component.executionPageComponentData = {
      permissions: {canWrite: true}, milestonesAllowModification: true,
    } as ExecutionPageComponentData;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
