import {Logger} from 'sqtm-core';
import {sqtmAppLogger} from '../../../app-logger';

export const requirementWorkspaceLogger: Logger = sqtmAppLogger.compose('requirement-workspace');
