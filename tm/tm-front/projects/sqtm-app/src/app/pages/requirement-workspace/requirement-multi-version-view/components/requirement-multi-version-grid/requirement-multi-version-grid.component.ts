import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {GridService, ReferentialDataService, RequirementVersionService, WorkspaceWithGridComponent} from 'sqtm-core';
import {RequirementMultiVersionViewService} from '../../service/requirement-multi-version-view.service';
import {take} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {RequirementVersionViewService} from '../../../requirement-version-view/services/requirement-version-view.service';
import {REQ_VERSIONS_WS_GRID} from '../../requirement-multi-version-view.constant';

@Component({
  selector: 'sqtm-app-requirement-multi-version-grid',
  template: `
    <div class="full-width full-height flex-column overflow-hidden p-10">
      <div class="flex-row title-container">
        <sqtm-core-history-back-button class="back-button">
        </sqtm-core-history-back-button>
        <span class="title">
          {{"sqtm-core.requirement-workspace.multi-versions.title" | translate}}
        </span>
      </div>
      <div class="full-height full-width overflow-hidden sqtm-large-grid-container p-10 multi-version-content">
        <sqtm-core-grid></sqtm-core-grid>
      </div>
    </div>`,
  styleUrls: ['./requirement-multi-version-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {provide: GridService, useExisting: REQ_VERSIONS_WS_GRID}
  ]
})
export class RequirementMultiVersionGridComponent implements OnInit, OnDestroy {

  unsub$ = new Subject<void>();

  protected readonly entityIdPositionInUrl = 4;

  constructor(private gridService: GridService,
              private requirementMultiVersionService: RequirementMultiVersionViewService,
              private requirementVersionService: RequirementVersionService,
              private requirementVersionViewService: RequirementVersionViewService,
              private referentialDataService: ReferentialDataService,
              private workspaceWithGrid: WorkspaceWithGridComponent) {
    this.workspaceWithGrid.entityIdPositionInUrl = this.entityIdPositionInUrl;
  }

  ngOnInit(): void {
    this.initGrid();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private initGrid() {
    this.requirementMultiVersionService.componentData$.pipe(
      take(1)
    ).subscribe(componentData => {
      this.gridService.setServerUrl(['requirement-view', 'versions', componentData.requirementId.toString()]);
    });
    this.referentialDataService.globalConfiguration$.pipe(
      take(1)
    ).subscribe(globalConf => {
      this.gridService.setColumnVisibility('milestoneLabels', globalConf.milestoneFeatureEnabled);
    });
  }

}
