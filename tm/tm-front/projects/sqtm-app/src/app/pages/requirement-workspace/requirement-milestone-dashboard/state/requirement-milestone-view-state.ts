import {CustomDashboardModel, EntityScope, Milestone, RequirementStatistics} from 'sqtm-core';

export interface RequirementMilestoneViewState {
  milestone: Milestone;
  statistics: RequirementStatistics;
  scope: EntityScope[];
  dashboard: CustomDashboardModel;
  generatedDashboardOn: Date;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}

export function initialRequirementMilestoneViewState(): Readonly<RequirementMilestoneViewState> {
  return {
    milestone: null,
    statistics: null,
    scope: [],
    dashboard: null,
    generatedDashboardOn: null,
    shouldShowFavoriteDashboard: false,
    canShowFavoriteDashboard: false,
    favoriteDashboardId: null
  };
}
