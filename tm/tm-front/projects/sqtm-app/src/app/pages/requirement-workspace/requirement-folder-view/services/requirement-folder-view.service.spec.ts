import {TestBed} from '@angular/core/testing';

import {RequirementFolderViewService} from './requirement-folder-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {
  Permissions,
  ReferentialDataService,
  RequirementFolderModel,
  RequirementStatistics,
  RestService
} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {of} from 'rxjs';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {mockRestService} from '../../../../utils/testing-utils/mocks.service';

describe('TestCaseFolderViewService', () => {

  const restService = mockRestService();

  const referentialDataService = {} as ReferentialDataService;

  referentialDataService.connectToProjectData = jasmine.createSpy().and.returnValue(of({
    permissions: {TEST_CASE: [Permissions.WRITE]}
  }));

  referentialDataService.globalConfiguration$ = of(null);

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
    providers: [
      {
        provide: ReferentialDataService,
        useValue: referentialDataService
      },
      {
        provide: RestService,
        useValue: restService
      },
      {
        provide: RequirementFolderViewService,
        useClass: RequirementFolderViewService
      }
    ]
  }));

  it('should load requirement folder', (done) => {
    restService.get.and.returnValue(of(getInitialModel()));
    const service: RequirementFolderViewService = TestBed.inject(RequirementFolderViewService);
    service.componentData$.subscribe(data => {
      expect(data.requirementFolder.id).toEqual(1);
      done();
    });
    expect(service).toBeTruthy();
    service.load(1);
  });

});

function getInitialModel(): RequirementFolderModel {
  return {
    id: 1,
    name: 'folder-1',
    description: '',
    attachmentList: {id: 1, attachments: []},
    customFieldValues: [],
    projectId: 1,
    statistics: {
      selectedIds: [],
      generatedOn: new Date()
    } as unknown as RequirementStatistics,
    canShowFavoriteDashboard: false,
    dashboard: null,
    favoriteDashboardId: null,
    shouldShowFavoriteDashboard: false
  };

}
