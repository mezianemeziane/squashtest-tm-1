import {ChangeDetectionStrategy, Component, Input, OnInit, ViewChild} from '@angular/core';
// tslint:disable-next-line:max-line-length
import {RequirementVersionViewComponentData} from '../../containers/requirement-version-view/requirement-version-view.component';
import {
  ActionErrorDisplayService,
  CustomFieldData,
  EditableSelectLevelEnumFieldComponent,
  InfoList,
  ReferentialDataService,
  RequirementStatus,
  RequirementStatusLevelEnumItem
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';
import {RequirementVersionViewService} from '../../services/requirement-version-view.service';
import {Observable} from 'rxjs';
import {catchError, finalize, pluck, take} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-requirement-view-information',
  templateUrl: './requirement-version-view-information.component.html',
  styleUrls: ['./requirement-version-view-information.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class RequirementVersionViewInformationComponent implements OnInit {

  readonly SQTM_CORE_INFOLIST_ITEM_ICON_PREFIX = 'sqtm-core-infolist-item';

  @Input()
  set requirementVersionViewComponentData(requirementVersionViewComponentData: RequirementVersionViewComponentData) {
    this._requirementVersionViewComponentData = requirementVersionViewComponentData;
    // if (this._requirementVersionViewComponentData) {
      this.syncStatus = `sqtm-core.entity.requirement.synchronization-status.${this._requirementVersionViewComponentData.requirementVersion.syncStatus}`;
    // }
  }

  @Input()
  customFieldData: CustomFieldData[];

  @ViewChild('statusField')
  statusField: EditableSelectLevelEnumFieldComponent;

  milestoneFeatureEnabled$: Observable<boolean>;

  syncStatus: string;

  private _requirementVersionViewComponentData: RequirementVersionViewComponentData;

  get requirementVersionViewComponentData(): RequirementVersionViewComponentData {
   return this._requirementVersionViewComponentData;
  }

  constructor(private translateService: TranslateService,
              private datePipe: DatePipe,
              private requirementVersionViewService: RequirementVersionViewService,
              private referentialDataService: ReferentialDataService,
              private actionErrorDisplayService: ActionErrorDisplayService) {
  }

  ngOnInit(): void {
    this.milestoneFeatureEnabled$ = this.referentialDataService.globalConfiguration$.pipe(
      take(1),
      pluck('milestoneFeatureEnabled'));
  }

  getAuditableText(date: Date, userLogin: string) {
    if (date != null) {
      const formattedDate = this.formatDate(date);
      return `${formattedDate} (${userLogin})`;
    } else {
      return this.translateService.instant('sqtm-core.generic.label.never');
    }
  }

  private formatDate(date: Date): string {
    return this.datePipe.transform(date, 'short', this.translateService.getBrowserLang());
  }

  deleteMilestone(id: number, milestoneId: number) {
    this.requirementVersionViewService.unbindMilestone(id, milestoneId);
  }

  bindMilestones(id: number, milestoneIds: number[]) {
    this.requirementVersionViewService.bindMilestones(id, milestoneIds);
  }

  trackCfd(cfd: CustomFieldData) {
    return cfd.id;
  }

  getIcon(requirementCategory: InfoList, categoryId: number) {
    const items = requirementCategory.items;
    const category = items.find(item => item.id === categoryId);
    if (category.iconName !== '' && category.iconName !== 'noicon') {
      return `${this.SQTM_CORE_INFOLIST_ITEM_ICON_PREFIX}:${category.iconName}`;
    } else {
      return '';
    }
  }

  allowRequirementModification(componentData: RequirementVersionViewComponentData) {
    return RequirementStatus[componentData.requirementVersion.status].allowModifications && componentData.milestonesAllowModification;
  }

  updateStatus(status: RequirementStatusLevelEnumItem<any>) {
    this.requirementVersionViewService.updateRequirementStatus(status.id)
      .pipe(
        catchError(err => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => {
          this.statusField.endAsync();
          this.statusField.disableEditMode();
        })
      )
      .subscribe();
  }

  get remoteReqPerimeterStatus() {
    return `sqtm-core.entity.requirement.remote-req-perimeter-status.${this._requirementVersionViewComponentData.requirementVersion.remoteReqPerimeterStatus}`;
  }
}
