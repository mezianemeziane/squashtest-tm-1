import {sqtmAppLogger} from '../../../app-logger';

export const requirementFolderViewLogger = sqtmAppLogger.compose('requirement-folder-view');
