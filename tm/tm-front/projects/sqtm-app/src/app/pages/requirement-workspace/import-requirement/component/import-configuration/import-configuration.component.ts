import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ImportRequirementState, RequirementFileFormat, XLS_TYPE} from '../../state/import-requirement.state';
import {DisplayOption} from 'sqtm-core';
import {ImportRequirementService} from '../../services/import-requirement.service';

@Component({
  selector: 'sqtm-app-import-configuration',
  templateUrl: './import-configuration.component.html',
  styleUrls: ['./import-configuration.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportConfigurationComponent implements OnInit {

  @Input()
  wrongFileFormat: boolean;

  @Input()
  componentData: ImportRequirementState;

  constructor(private translateService: TranslateService,
              private importRequirementService: ImportRequirementService) {
  }

  ngOnInit(): void {
  }

  initializeImportFormat(): DisplayOption[] {
    const formatOptions: DisplayOption[] = [];
    for (const requirementImportFormat of Object.keys(RequirementFileFormat)) {
      const item = RequirementFileFormat[requirementImportFormat];
      formatOptions.push({
        id: item.id,
        label: this.translateService.instant(item.i18nKey)
      });
    }
    return formatOptions;
  }

  addAttachment(files: File[]) {
    this.importRequirementService.saveFile(files[0]);
  }

  getRestrictionFiles() {
    return XLS_TYPE;
  }

  getWrongFileKey() {
    return 'sqtm-core.requirement-workspace.dialog.import.error.xls-format';
  }
}
