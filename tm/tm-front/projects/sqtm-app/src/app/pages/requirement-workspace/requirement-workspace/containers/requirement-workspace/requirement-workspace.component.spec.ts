import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {RequirementWorkspaceComponent} from './requirement-workspace.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {WorkspaceCommonModule} from 'sqtm-core';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('RequirementWorkspaceComponent', () => {
  let component: RequirementWorkspaceComponent;
  let fixture: ComponentFixture<RequirementWorkspaceComponent>;

  // See administration-workspace.component.spec
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, WorkspaceCommonModule, RouterTestingModule],
      declarations: [RequirementWorkspaceComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(RequirementWorkspaceComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
