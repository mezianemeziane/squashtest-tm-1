import { TestBed } from '@angular/core/testing';

import { RequirementLibraryViewService } from './requirement-library-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';

describe('RequirementLibraryViewService', () => {
  let service: RequirementLibraryViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [RequirementLibraryViewService]
    });
    service = TestBed.inject(RequirementLibraryViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
