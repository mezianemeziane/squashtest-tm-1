import {EntityViewState, provideInitialViewState} from 'sqtm-core';
import {RequirementFolderState} from './requirement-folder.state';

export interface RequirementFolderViewState extends EntityViewState<RequirementFolderState, 'requirementFolder'> {
  requirementFolder: RequirementFolderState;
}

export function provideInitialRequirementFolderView(): Readonly<RequirementFolderViewState> {
  return provideInitialViewState<RequirementFolderState, 'requirementFolder'>('requirementFolder');
}
