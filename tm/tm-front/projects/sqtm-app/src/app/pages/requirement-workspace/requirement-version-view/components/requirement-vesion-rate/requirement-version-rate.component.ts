import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sqtm-app-requirement-version-rate',
  template: `
    <div style="display: grid; grid-template-columns: 1fr 1fr; column-gap: 10px; align-items: center;">
      <sqtm-core-label [attr.data-test-component-id]="'title'"
                       [text]="titleKey | translate"></sqtm-core-label>
      <div class="flex-row" style=" align-items: center;">
        <div class="m-r-5 rate-container flex flex-column percent"
             [class]="getCssClass()" [attr.data-test-component-id]="'percentage'">
          <span
          >{{getRate() |percent:'.0-2'}}</span>
        </div>
        <span style="font-weight: bold;" [attr.data-test-component-id]="'rate'">{{getRateString()}}</span>
      </div>
    </div>
  `,
  styleUrls: ['./requirement-version-rate.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementVersionRateComponent implements OnInit {

  @Input()
  matchingCount: number;

  @Input()
  total: number;

  @Input()
  titleKey: string;

  constructor() {
  }

  ngOnInit(): void {
  }

  getRate() {
    let rate = 0;
    if (this.total !== 0) {
      rate = this.matchingCount / this.total;
    }
    return rate;
  }

  getRateString() {
    return `(${this.matchingCount}/${this.total})`;
  }

  getCssClass() {
    const rate = this.getRate();
    if (rate === 1) {
      return 'percent-valid';
    } else if (1 > rate && rate >= 0.5) {
      return 'percent-warning';
    } else if (0.5 > rate && this.total !== 0) {
      return 'percent-danger';
    } else {
      return 'percent-undefined';
    }
  }
}

export interface RequirementVersionRate {
  fieldI18nKey: string;
  rate: number;
  total: number;
  count: number;
}
