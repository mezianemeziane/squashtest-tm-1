import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {RequirementVersionViewModificationHistoryComponent} from './requirement-version-view-modification-history.component';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RequirementVersionViewService} from '../../services/requirement-version-view.service';
import {EMPTY} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('RequirementVersionViewModificationHistoryComponent', () => {
  let component: RequirementVersionViewModificationHistoryComponent;
  let fixture: ComponentFixture<RequirementVersionViewModificationHistoryComponent>;

  const serviceMock = jasmine.createSpyObj(['load']);
  serviceMock.componentData$ = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), HttpClientTestingModule, AppTestingUtilsModule],
      declarations: [RequirementVersionViewModificationHistoryComponent],
      providers: [
        {
          provide: RequirementVersionViewService,
          useValue: serviceMock
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementVersionViewModificationHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
