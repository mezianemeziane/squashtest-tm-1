import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {
  AttachmentService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  Extendable,
  Fixed,
  GenericEntityViewService,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  indexColumn,
  infoListColumn,
  levelEnumColumn,
  milestoneLabelColumn,
  numericColumn,
  ReferentialDataService,
  RequirementCriticality,
  RequirementStatus,
  RequirementVersionService,
  RestService,
  selectableNumericColumn,
  selectableTextColumn,
  StyleDefinitionBuilder
} from 'sqtm-core';
import {REQ_VERSIONS_WS_GRID, REQ_VERSIONS_WS_GRID_CONFIG} from '../../requirement-multi-version-view.constant';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {map, takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {
  RVW_REQUIREMENT_VERSION_LINK_TABLE,
  RVW_REQUIREMENT_VERSION_LINK_TABLE_CONF,
  RVW_VERIFYING_TEST_CASE_TABLE,
  RVW_VERIFYING_TEST_CASE_TABLE_CONF
} from '../../../requirement-version-view/requirement-version-view.constant';
import {rvwVerifyingTCTableDefinition} from '../../../requirement-version-view/components/verifying-test-case-table/verifying-test-case-table.component';
import {rvwRequirementVersionLinkTableDefinition} from '../../../requirement-version-view/components/requirement-version-link-table/requirement-version-link-table.component';
import {TranslateService} from '@ngx-translate/core';
import {RequirementVersionViewService} from '../../../requirement-version-view/services/requirement-version-view.service';
import {RequirementMultiVersionViewState} from '../../state/requirement-multi-version-view.state';
import {RequirementMultiVersionViewService} from '../../service/requirement-multi-version-view.service';

export function requirementVersionsGrid(): GridDefinition {
  return grid('requirement-versions-grid')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      selectableNumericColumn('versionNumber').withI18nKey('sqtm-core.entity.requirement.version-number.short'),
      selectableTextColumn('reference').withI18nKey('sqtm-core.entity.generic.reference.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      selectableTextColumn('name').withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      levelEnumColumn('requirementStatus', RequirementStatus)
        .withTitleI18nKey('sqtm-core.entity.generic.status.label')
        .withI18nKey('sqtm-core.entity.generic.status.short')
        .isEditable(false)
        .changeWidthCalculationStrategy(new Fixed(40)),
      levelEnumColumn('criticality', RequirementCriticality)
        .withTitleI18nKey('sqtm-core.entity.generic.criticality.label')
        .withI18nKey('sqtm-core.entity.generic.criticality.short')
        .isEditable(false)
        .changeWidthCalculationStrategy(new Fixed(40)),
      infoListColumn('category', {
        kind: 'infoList',
        infolist: 'requirementCategory'
      }).withI18nKey('sqtm-core.entity.requirement.category.label'),
      milestoneLabelColumn('milestoneLabels').changeWidthCalculationStrategy(new Extendable(100, 1)),
      numericColumn('links')
        .withTitleI18nKey('sqtm-core.requirement-workspace.multi-versions.number-of-links.full')
        .withI18nKey('sqtm-core.requirement-workspace.multi-versions.number-of-links.short'),
      numericColumn('coverages')
        .withTitleI18nKey('sqtm-core.requirement-workspace.multi-versions.number-of-coverages.full')
        .withI18nKey('sqtm-core.requirement-workspace.multi-versions.number-of-coverages.short')

    ])
    .server()
    .disableRightToolBar()
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-requirement-multi-version-view',
  templateUrl: './requirement-multi-version-view.component.html',
  styleUrls: ['./requirement-multi-version-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: REQ_VERSIONS_WS_GRID_CONFIG,
      useFactory: requirementVersionsGrid,
      deps: []
    },
    {
      provide: REQ_VERSIONS_WS_GRID,
      useFactory: gridServiceFactory,
      deps: [RestService, REQ_VERSIONS_WS_GRID_CONFIG, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: REQ_VERSIONS_WS_GRID
    },
    {
      provide: RequirementMultiVersionViewService,
      useClass: RequirementMultiVersionViewService
    },
    {
      provide: RVW_VERIFYING_TEST_CASE_TABLE_CONF,
      useFactory: rvwVerifyingTCTableDefinition
    },
    {
      provide: RVW_VERIFYING_TEST_CASE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, RVW_VERIFYING_TEST_CASE_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: RVW_REQUIREMENT_VERSION_LINK_TABLE_CONF,
      useFactory: rvwRequirementVersionLinkTableDefinition,
      deps: [TranslateService]
    },
    {
      provide: RVW_REQUIREMENT_VERSION_LINK_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, RVW_REQUIREMENT_VERSION_LINK_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: RequirementVersionViewService,
      useClass: RequirementVersionViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        RequirementVersionService,
        CustomFieldValueService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        RVW_VERIFYING_TEST_CASE_TABLE,
        RVW_REQUIREMENT_VERSION_LINK_TABLE
      ]
    },
    {
      provide: EntityViewService,
      useExisting: RequirementVersionViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: RequirementVersionViewService
    }
  ]
})
export class RequirementMultiVersionViewComponent implements OnInit, OnDestroy {

  unsub$ = new Subject<void>();

  versionsComponentData$: Observable<RequirementMultiVersionViewState>;

  constructor(private router: Router,
              private route: ActivatedRoute,
              public readonly referentialDataService: ReferentialDataService,
              private requirementService: RequirementVersionService,
              private requirementVersionViewService: RequirementVersionViewService,
              private requirementMultiVersionViewService: RequirementMultiVersionViewService,
              @Inject(REQ_VERSIONS_WS_GRID) private gridService: GridService
  ) {
  }

  ngOnInit(): void {
    this.prepareGridRefreshOnEntityChanges();
    this.versionsComponentData$ = this.requirementMultiVersionViewService.componentData$;
    this.fetchData();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private fetchData() {
    this.referentialDataService.refresh().subscribe(
      () => this.loadData()
    );
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        map((params: ParamMap) => params.get('requirementId')),
      ).subscribe((id) => {
      this.requirementMultiVersionViewService.load(parseInt(id, 10));
    });
  }

  private prepareGridRefreshOnEntityChanges(): void {
    this.requirementVersionViewService.simpleAttributeRequiringRefresh = [
      'name',
      'reference',
      'status',
      'criticality',
      'category',
    ];

    this.requirementVersionViewService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
    ).subscribe(() => this.gridService.refreshDataAndKeepSelectedRows());
  }

}
