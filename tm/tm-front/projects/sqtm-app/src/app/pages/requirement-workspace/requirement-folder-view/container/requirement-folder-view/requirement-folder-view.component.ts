import {ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RequirementFolderViewService} from '../../services/requirement-folder-view.service';
import {requirementFolderViewLogger} from '../../requirement-folder-view.logger';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {filter, map, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {
  AttachmentDrawerComponent,
  AttachmentState,
  EntityRowReference,
  EntityViewComponentData,
  EntityViewService,
  GenericEntityViewService,
  Identifier,
  ReferentialDataService,
  RequirementPermissions,
  SquashTmDataRowType,
  WorkspaceWithTreeComponent
} from 'sqtm-core';
import {RequirementFolderState} from '../../state/requirement-folder.state';

const logger = requirementFolderViewLogger.compose('RequirementFolderViewComponent');

@Component({
  selector: 'sqtm-app-requirement-folder-view',
  templateUrl: './requirement-folder-view.component.html',
  styleUrls: ['./requirement-folder-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: RequirementFolderViewService,
      useClass: RequirementFolderViewService,
    },
    {
      provide: EntityViewService,
      useExisting: RequirementFolderViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: RequirementFolderViewService
    }
  ]
})
export class RequirementFolderViewComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  @ViewChild('content', {read: ElementRef})
  content: ElementRef;

  constructor(private route: ActivatedRoute,
              public readonly requirementFolderService: RequirementFolderViewService,
              private referentialDataService: ReferentialDataService,
              private workspaceWithTree: WorkspaceWithTreeComponent) {
    logger.debug('Instantiate RequirementFolderViewComponent');
  }

  ngOnInit() {
    this.referentialDataService.loaded$.pipe(
      takeUntil(this.unsub$),
      filter(loaded => loaded),
      take(1)
    ).subscribe(() => {
      logger.debug(`Loading RequirementFolderViewComponent Data by http request`);
      this.loadData();
      this.initializeTreeSynchronization();
    });
  }

  private initializeTreeSynchronization() {
    this.requirementFolderService.simpleAttributeRequiringRefresh = ['name'];
    this.requirementFolderService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.requirementFolderService.componentData$),
      map(([{}, componentData]: [{}, RequirementFolderViewComponentData]) =>
        new EntityRowReference(componentData.requirementFolder.id, SquashTmDataRowType.RequirementFolder).asString())
    ).subscribe((identifier: Identifier) => {
      this.workspaceWithTree.requireNodeRefresh([identifier]);
    });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('requirementFolderId')),
      ).subscribe((id) => {
      this.requirementFolderService.load(parseInt(id, 10));
    });
  }

  ngOnDestroy(): void {
    this.requirementFolderService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }


  getAttachmentCount(attachmentState: AttachmentState): number {
    const attachments = Object.values(attachmentState.entities);
    return attachments.filter(attachment => attachment.kind === 'persisted-attachment').length;
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }
}

export interface RequirementFolderViewComponentData
  extends EntityViewComponentData<RequirementFolderState, 'requirementFolder', RequirementPermissions> {
}

