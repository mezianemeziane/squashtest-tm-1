import {ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {take, takeUntil} from 'rxjs/operators';
import {
  RequirementVersionLinksData,
  RequirementVersionViewService
} from '../../services/requirement-version-view.service';
import {Observable, Subject} from 'rxjs';
// tslint:disable-next-line:max-line-length
import {RequirementVersionViewComponentData} from '../../containers/requirement-version-view/requirement-version-view.component';
import {select} from '@ngrx/store';
import {
  BindableEntity,
  createCustomFieldValueDataSelector,
  createRequirementVersionLinksMessageDialogConfiguration,
  createRequirementVersionVerifyingTestCasesMessageDialogConfiguration,
  CustomFieldData,
  DialogService,
  GridDndData,
  parseDataRowId,
  REQUIREMENT_TREE_PICKER_ID, RequirementPermissions,
  RequirementVersionLinkDialogComponent,
  RequirementVersionLinkDialogConfiguration,
  RequirementVersionLinkDialogResult,
  shouldShowCoverageMessageDialog,
  shouldShowRequirementLinksMessageDialog,
  SqtmDropEvent,
  TEST_CASE_TREE_PICKER_ID
} from 'sqtm-core';
import {requirementVersionViewContent} from '../../requirement-version-view.constant';
import {requirementVersionLogger} from '../../requirement-version-view.logger';

const logger = requirementVersionLogger.compose('RequirementVersionViewContentComponent');

@Component({
  selector: 'sqtm-app-requirement-view-content',
  templateUrl: './requirement-version-view-content.component.html',
  styleUrls: ['./requirement-version-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementVersionViewContentComponent implements OnInit, OnDestroy {

  componentData$: Observable<RequirementVersionViewComponentData>;

  unsub$ = new Subject<void>();

  customFieldData: CustomFieldData[] = [];
  dropZoneId = requirementVersionViewContent;

  constructor(public requirementViewService: RequirementVersionViewService,
              private dialogService: DialogService,
              private vcr: ViewContainerRef) {
    this.componentData$ = this.requirementViewService.componentData$;
  }

  ngOnInit(): void {

    this.initializeCufData();

  }

  private initializeCufData() {
    this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createCustomFieldValueDataSelector(BindableEntity.REQUIREMENT_VERSION))
    ).subscribe(customFieldData => {
      this.customFieldData = customFieldData;
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  removeVerifyingTestCases() {
    const dialogRef = this.dialogService.openDeletionConfirm({
      id: 'confirm-delete',
      level: 'WARNING',
      titleKey: 'sqtm-core.requirement-workspace.dialog.unlink-test-cases.title',
      messageKey: 'sqtm-core.requirement-workspace.dialog.unlink-test-cases.message'
    });

    dialogRef.dialogClosed$.pipe(
      take(1),
    ).subscribe(confirm => {
      if (confirm) {
        this.requirementViewService.removeVerifyingTestCases();
      }
    });
  }

  toggleTestCaseDrawer(event) {
    event.stopPropagation();
    this.requirementViewService.openTestCaseTreePicker();
  }

  toggleRequirementDrawer(event) {
    event.stopPropagation();
    this.requirementViewService.openRequirementTreePicker();
  }

  stopPropagation(mouseEvent: MouseEvent) {
    mouseEvent.stopPropagation();
  }

  dropIntoRequirementVersion($event: SqtmDropEvent, permissions: RequirementPermissions) {
    if (permissions.canLink) {
      this.handleDropIntoRequirementVersion($event);
    }
  }

  private handleDropIntoRequirementVersion($event: SqtmDropEvent) {
    const data = $event.dragAndDropData.data as GridDndData;
    if (logger.isDebugEnabled()) {
      logger.debug(`Dropping dataRows in requirement version view.`, [data]);
    }
    const entityIds = data.dataRows.map(row => parseDataRowId(row));
    if ($event.dragAndDropData.origin === TEST_CASE_TREE_PICKER_ID) {
      this.requirementViewService.addTestCases(entityIds).pipe(take(1)).subscribe(operationReport => {
        if (shouldShowCoverageMessageDialog(operationReport)) {
          this.dialogService.openDialog(createRequirementVersionVerifyingTestCasesMessageDialogConfiguration(operationReport));
        }
      });
    } else if ($event.dragAndDropData.origin === REQUIREMENT_TREE_PICKER_ID
      || $event.dragAndDropData.origin === 'requirement-workspace-main-tree') {
      this.addRequirementLinks(entityIds);
    }
  }

  addRequirementLinks(requirementIds: number[]) {
    this.requirementViewService.findRelatedRequirementVersionInformation(requirementIds).subscribe(requirementVersionLinksData => {
      const dialogRef = this.dialogService.openDialog<RequirementVersionLinkDialogConfiguration, RequirementVersionLinkDialogResult>({
        id: 'requirement-version-link',
        component: RequirementVersionLinkDialogComponent,
        viewContainerReference: this.vcr,
        data: this.createRequirementLinkConfiguration(requirementVersionLinksData),
        width: 600
      });
      dialogRef.dialogClosed$.subscribe(result => {
        if (result) {
          this.requirementViewService.addRequirementLinks(requirementIds, result.linkTypeId, result.linkDirection)
            .pipe(
              take(1)
            ).subscribe(operationReport => {
            if (shouldShowRequirementLinksMessageDialog(operationReport)) {
              this.dialogService.openDialog(createRequirementVersionLinksMessageDialogConfiguration(operationReport));
            }
          });
        }
      });
    });
  }

  private createRequirementLinkConfiguration(requirementVersionLinksData: RequirementVersionLinksData) {
    return {
      titleKey: 'sqtm-core.requirement-workspace.dialog.requirement-links.title',
      requirementVersionLinkTypes: requirementVersionLinksData.linkTypes,
      requirementVersionNodesName: requirementVersionLinksData.nodeNames,
      requirementVersionName: requirementVersionLinksData.reqVersionName
    };
  }

  removeRequirementVersionLinks() {
    const dialogRef = this.dialogService.openDeletionConfirm({
      id: 'confirm-delete',
      level: 'WARNING',
      titleKey: 'sqtm-core.requirement-workspace.dialog.unlink-requirement-version-links.title',
      messageKey: 'sqtm-core.requirement-workspace.dialog.unlink-requirement-version-links.message'
    });
    dialogRef.dialogClosed$.pipe(
      take(1)
    ).subscribe(confirm => {
      if (confirm) {
        this.requirementViewService.removeRequirementVersionLinks();
      }
    });
  }


}
