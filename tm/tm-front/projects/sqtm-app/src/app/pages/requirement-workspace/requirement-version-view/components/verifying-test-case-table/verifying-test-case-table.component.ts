import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {
  deleteColumn,
  Extendable,
  Fixed,
  GridDefinition,
  GridService,
  indexColumn,
  levelEnumColumn,
  milestoneLabelColumn,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  TestCaseStatus,
  TestCaseWeight,
  textColumn,
  withLinkColumn
} from 'sqtm-core';
import {RVW_VERIFYING_TEST_CASE_TABLE} from '../../requirement-version-view.constant';
import {DeleteVerifyingTestCaseComponent} from '../cell-renderers/delete-verifying-test-case/delete-verifying-test-case.component';

export function rvwVerifyingTCTableDefinition(): GridDefinition {
  return smallGrid('requirement-version-view-verifying-tc').withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      textColumn('projectName')
        .changeWidthCalculationStrategy(new Extendable(100))
        .withI18nKey('sqtm-core.entity.project.label.singular'),
      textColumn('reference').withI18nKey('sqtm-core.entity.generic.reference.label'),
      withLinkColumn('name', {kind: 'link', baseUrl: '/test-case-workspace/test-case/detail', columnParamId: 'id'})
        .changeWidthCalculationStrategy(new Extendable(100))
        .withI18nKey('sqtm-core.entity.test-case.label.singular'),
      milestoneLabelColumn('milestoneLabels')
        .changeWidthCalculationStrategy(new Extendable(150, 0.1)),
      levelEnumColumn('status', TestCaseStatus)
        .changeWidthCalculationStrategy(new Fixed(50))
        .withI18nKey('sqtm-core.search.test-case.grid.header.status.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.status.title'),
      levelEnumColumn('importance', TestCaseWeight)
        .changeWidthCalculationStrategy(new Fixed(50))
        .withI18nKey('sqtm-core.search.test-case.grid.header.weight.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.weight.title'),
      deleteColumn(DeleteVerifyingTestCaseComponent)
    ]
  ).withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withInitialSortedColumns([
      {id: 'importance', sort: Sort.ASC},
      {id: 'projectName', sort: Sort.ASC},
      {id: 'reference', sort: Sort.ASC},
      {id: 'name', sort: Sort.ASC}
    ])
    .build();
}

@Component({
  selector: 'sqtm-app-verifying-test-case-table',
  template: `
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./verifying-test-case-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: RVW_VERIFYING_TEST_CASE_TABLE
    }
  ]
})
export class VerifyingTestCaseTableComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
