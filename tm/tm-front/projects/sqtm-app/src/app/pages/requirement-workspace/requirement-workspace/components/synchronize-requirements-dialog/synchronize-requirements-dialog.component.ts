import {Component, OnInit, ChangeDetectionStrategy, Inject} from '@angular/core';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {SynchronizeRequirementsDialogConfiguration} from './synchronize-requirements-dialog-configuration';
import {DialogReference} from 'sqtm-core';
import {APP_BASE_HREF} from '@angular/common';

@Component({
  selector: 'sqtm-app-synchronize-requirements-dialog',
  templateUrl: './synchronize-requirements-dialog.component.html',
  styleUrls: ['./synchronize-requirements-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SynchronizeRequirementsDialogComponent implements OnInit {

  data: SynchronizeRequirementsDialogConfiguration;
  pluginUrl: SafeUrl;

  constructor(private dialogReference: DialogReference<SynchronizeRequirementsDialogConfiguration>,
              private sanitizer: DomSanitizer,
              @Inject(APP_BASE_HREF) private serverServletContext: string) {
    this.data = this.dialogReference.data;
    this.pluginUrl = sanitizer.bypassSecurityTrustResourceUrl(`${serverServletContext}${this.data.url}`);
  }

  ngOnInit(): void {
  }

}
