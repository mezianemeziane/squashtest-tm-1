import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  Renderer2,
  ViewContainerRef
} from '@angular/core';
import {AbstractRequirementVersionView} from '../abstract-requirement-version-view';
import {RequirementVersionViewService} from '../../services/requirement-version-view.service';
import {TranslateService} from '@ngx-translate/core';
import {DialogService, DragAndDropService, ReferentialDataService, RequirementVersionService} from 'sqtm-core';
import {ActivatedRoute} from '@angular/router';
import {concatMap, map} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-requirement-version-detail-view',
  templateUrl: './requirement-version-detail-view.component.html',
  styleUrls: ['./requirement-version-detail-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementVersionDetailViewComponent extends AbstractRequirementVersionView implements OnInit, AfterViewInit {

  constructor(requirementViewService: RequirementVersionViewService,
              translateService: TranslateService,
              dndService: DragAndDropService,
              renderer: Renderer2,
              referentialDataService: ReferentialDataService,
              dialogService: DialogService,
              vcr: ViewContainerRef,
              private activeRoute: ActivatedRoute,
              private requirementService: RequirementVersionService,
              private cdr: ChangeDetectorRef) {
    super(requirementViewService, translateService, dndService, renderer, referentialDataService, dialogService, vcr);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.activeRoute.paramMap.pipe(
      map(param => param.get('versionId')),
      concatMap(versionId => this.requirementService.loadVersion(parseInt(versionId, 10)))
    ).subscribe((versionModel) => {
      this.requirementViewService.load(versionModel);
      this.cdr.detectChanges();
    });
  }
}
