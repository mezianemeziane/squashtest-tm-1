import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {
  AttachmentService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  GenericEntityViewService,
  gridServiceFactory,
  ReferentialDataService,
  RequirementVersionModel,
  RequirementVersionService,
  RestService
} from 'sqtm-core';
import {Subject} from 'rxjs';
import {RequirementVersionViewService} from '../../requirement-version-view/services/requirement-version-view.service';
import {TranslateService} from '@ngx-translate/core';
import {concatMap, map, takeUntil} from 'rxjs/operators';
import {
  RVW_REQUIREMENT_VERSION_LINK_TABLE,
  RVW_REQUIREMENT_VERSION_LINK_TABLE_CONF,
  RVW_VERIFYING_TEST_CASE_TABLE,
  RVW_VERIFYING_TEST_CASE_TABLE_CONF
} from '../../requirement-version-view/requirement-version-view.constant';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {rvwVerifyingTCTableDefinition} from '../../requirement-version-view/components/verifying-test-case-table/verifying-test-case-table.component';
import {rvwRequirementVersionLinkTableDefinition} from '../../requirement-version-view/components/requirement-version-link-table/requirement-version-link-table.component';

@Component({
  selector: 'sqtm-app-requirement-version-level-two',
  templateUrl: './requirement-version-level-two.component.html',
  styleUrls: ['./requirement-version-level-two.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: RVW_VERIFYING_TEST_CASE_TABLE_CONF,
      useFactory: rvwVerifyingTCTableDefinition
    },
    {
      provide: RVW_VERIFYING_TEST_CASE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, RVW_VERIFYING_TEST_CASE_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: RVW_REQUIREMENT_VERSION_LINK_TABLE_CONF,
      useFactory: rvwRequirementVersionLinkTableDefinition,
      deps: [TranslateService]
    },
    {
      provide: RVW_REQUIREMENT_VERSION_LINK_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, RVW_REQUIREMENT_VERSION_LINK_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: RequirementVersionViewService,
      useClass: RequirementVersionViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        RequirementVersionService,
        CustomFieldValueService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        RVW_VERIFYING_TEST_CASE_TABLE,
        RVW_REQUIREMENT_VERSION_LINK_TABLE
      ]
    },
    {
      provide: EntityViewService,
      useExisting: RequirementVersionViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: RequirementVersionViewService
    }
  ]
})
export class RequirementVersionLevelTwoComponent implements OnInit, OnDestroy {
  unsub$ = new Subject<void>();

  constructor(private requirementVersionService: RequirementVersionService,
              private referentialDataService: ReferentialDataService,
              private requirementViewService: RequirementVersionViewService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.referentialDataService.refresh().subscribe(() =>
      this.loadData()
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('versionId')),
        concatMap((requirementId) =>
          this.requirementVersionService.loadVersion(parseInt(requirementId, 10)))
      ).subscribe((requirementVersionModel: RequirementVersionModel) => {
      this.requirementViewService.load(requirementVersionModel);
    });
  }

  back() {
    history.back();
  }
}
