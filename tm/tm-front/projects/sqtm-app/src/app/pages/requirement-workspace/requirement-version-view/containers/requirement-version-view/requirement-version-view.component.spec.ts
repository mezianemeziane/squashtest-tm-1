import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {RequirementVersionViewComponent} from './requirement-version-view.component';
import {RequirementVersionViewService} from '../../services/requirement-version-view.service';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {ReferentialDataService, SqtmDragAndDropModule} from 'sqtm-core';
import {OverlayModule} from '@angular/cdk/overlay';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';

describe('RequirementVersionViewComponent', () => {
  let component: RequirementVersionViewComponent;
  let fixture: ComponentFixture<RequirementVersionViewComponent>;
  const requirementVersionViewService = jasmine.createSpyObj('requirementVersionViewService', ['load', 'complete']);
  requirementVersionViewService.componentData$ = EMPTY;
  const referentialDataService = jasmine.createSpyObj(['load']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule,
        NzDropDownModule,
        TranslateModule.forRoot(),
        SqtmDragAndDropModule,
        OverlayModule
      ],
      declarations: [RequirementVersionViewComponent],
      providers: [
        {provide: RequirementVersionViewService, useValue: requirementVersionViewService},
        {provide: ReferentialDataService, useValue: referentialDataService}
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementVersionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
