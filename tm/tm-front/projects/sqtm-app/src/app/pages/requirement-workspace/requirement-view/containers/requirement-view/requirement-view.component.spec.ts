import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {RequirementViewComponent} from './requirement-view.component';
import {RequirementVersionService, WorkspaceWithTreeComponent} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('RequirementViewComponent', () => {
  let component: RequirementViewComponent;
  let fixture: ComponentFixture<RequirementViewComponent>;
  const requirementVersionService = jasmine.createSpyObj('requirementVersionService', ['load']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        AppTestingUtilsModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        RouterTestingModule
      ],
      declarations: [RequirementViewComponent],
      providers: [{
        provide: RequirementVersionService,
        useValue: requirementVersionService
      }, {provide: WorkspaceWithTreeComponent, useValue: WorkspaceWithTreeComponent}],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
