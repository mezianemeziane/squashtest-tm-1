import {Injectable} from '@angular/core';
import {
  createStore,
  CustomDashboardModel,
  DataRow,
  EntityScope,
  FavoriteDashboardValue,
  PartyPreferencesService,
  RequirementStatistics,
  RestService
} from 'sqtm-core';
import {provideInitialRequirementMultiView, RequirementMultiViewState} from '../state/requirement-multi-view.state';
import {Observable} from 'rxjs';
import {map, withLatestFrom} from 'rxjs/operators';


@Injectable()
export class RequirementMultiSelectionService {

  private store = createStore<RequirementMultiViewState>(provideInitialRequirementMultiView());

  componentData$: Observable<RequirementMultiViewState> = this.store.state$;

  constructor(private restService: RestService,
              private partyPreferencesService: PartyPreferencesService) {
  }

  init(rows: DataRow[]) {
    const references = rows.map(row => row.id);
    const scope: EntityScope[] = rows.map(row => ({
      id: row.id.toString(),
      label: row.data['NAME'],
      projectId: row.projectId
    }));
    this.restService.post(['requirement-workspace-multi-view'], {references})
      .pipe(
        withLatestFrom(this.store.state$),
        map(([response, state]: [RequirementMultiSelectionModel, RequirementMultiViewState]) => {
          return {...response, scope, generatedDashboardOn: new Date(), dashboardLoaded: true};
        })
      )
      .subscribe(state => {
        this.store.commit(state);
      });
  }

  complete() {
    this.store.complete();
  }

  changeDashboardToDisplay(preferenceValue: FavoriteDashboardValue): Observable<void> {
    return this.partyPreferencesService.changeRequirementWorkspaceFavoriteDashboard(preferenceValue);
  }
}

interface RequirementMultiSelectionModel {
  statistics: RequirementStatistics;
  dashboard: CustomDashboardModel;
  generatedDashboardOn: Date;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}
