import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RequirementVersionViewRatePanelComponent } from './requirement-version-view-rate-panel.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('RequirementVersionViewRatePanelComponent', () => {
  let component: RequirementVersionViewRatePanelComponent;
  let fixture: ComponentFixture<RequirementVersionViewRatePanelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RequirementVersionViewRatePanelComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementVersionViewRatePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
