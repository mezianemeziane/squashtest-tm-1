import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {RequirementVersionViewInformationComponent} from './requirement-version-view-information.component';
import {TranslateModule} from '@ngx-translate/core';
import {RequirementVersionViewService} from '../../services/requirement-version-view.service';
import {EntityViewService, ReferentialDataService, WorkspaceCommonModule} from 'sqtm-core';
import {EMPTY, of} from 'rxjs';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RequirementVersionViewComponentData} from '../../containers/requirement-version-view/requirement-version-view.component';


describe('RequirementViewInformationComponent', () => {
  let component: RequirementVersionViewInformationComponent;
  let fixture: ComponentFixture<RequirementVersionViewInformationComponent>;
  let requirementVersionViewService = jasmine.createSpyObj('requirementVersionViewService', ['load']);
  requirementVersionViewService = {...requirementVersionViewService, componentData$: of({requirementVersion: {syncStatus: 'FAILURE'}})};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), AppTestingUtilsModule, WorkspaceCommonModule],
      declarations: [RequirementVersionViewInformationComponent],
      providers: [
        {
          provide: RequirementVersionViewService,
          useValue: {requirementVersionViewService}
        },
        {
          provide: ReferentialDataService,
          useValue: {
            globalConfiguration$: of({})
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementVersionViewInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
