import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {RequirementMilestoneViewState} from '../../state/requirement-milestone-view-state';
import {RequirementMilestoneViewService} from '../../service/requirement-milestone-view.service';
import {ReferentialDataService} from 'sqtm-core';
import {filter, take, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-requirement-milestone-view',
  templateUrl: './requirement-milestone-view.component.html',
  styleUrls: ['./requirement-milestone-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [RequirementMilestoneViewService]
})
export class RequirementMilestoneViewComponent implements OnInit {

  public componentData$: Observable<Readonly<RequirementMilestoneViewState>>;

  private unsub$ = new Subject<void>();

  constructor(private viewService: RequirementMilestoneViewService,
              private referentialDataService: ReferentialDataService) { }

  ngOnInit(): void {
    this.referentialDataService.loaded$.pipe(
      takeUntil(this.unsub$),
      filter(loaded => loaded),
      take(1)
    ).subscribe(() => {
      this.componentData$ = this.viewService.componentData$;
      this.viewService.init();
    });
  }

}
