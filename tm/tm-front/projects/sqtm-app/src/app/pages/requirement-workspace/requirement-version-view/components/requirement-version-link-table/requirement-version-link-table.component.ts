import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {
  column,
  deleteColumn,
  Extendable,
  Fixed,
  GridDefinition,
  GridService,
  indexColumn,
  milestoneLabelColumn,
  requirementRoleColumn,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  textColumn,
  withLinkColumn
} from 'sqtm-core';
import {RVW_REQUIREMENT_VERSION_LINK_TABLE} from '../../requirement-version-view.constant';
import {DeleteRequirementLinkComponent} from '../cell-renderers/delete-requirement-link/delete-requirement-link.component';
import {ModifyRequirementLinkComponent} from '../cell-renderers/modify-requirement-link/modify-requirement-link.component';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-requirement-version-link-table',
  template: `
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./requirement-version-link-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: RVW_REQUIREMENT_VERSION_LINK_TABLE
    }
  ]
})
export class RequirementVersionLinkTableComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}

export function rvwRequirementVersionLinkTableDefinition(translateService: TranslateService): GridDefinition {
  return smallGrid('requirement-version-view-link').withColumns([
    indexColumn()
      .withViewport('leftViewport'),
    textColumn('projectName')
      .changeWidthCalculationStrategy(new Extendable(100))
      .withI18nKey('sqtm-core.entity.project.label.singular'),
    textColumn('reference')
      .withI18nKey('sqtm-core.entity.generic.reference.label'),
    withLinkColumn('name', {kind: 'link', baseUrl: '/requirement-workspace/requirement-version/detail', columnParamId: 'id'})
      .changeWidthCalculationStrategy(new Extendable(100))
      .withI18nKey('sqtm-core.entity.requirement.label.singular'),
    milestoneLabelColumn('milestoneLabels')
      .changeWidthCalculationStrategy(new Extendable(150, 0.1)),
    textColumn('versionNumber')
      .withI18nKey('sqtm-core.entity.requirement.version-number.short'),
    requirementRoleColumn('role')
      .isEditable(false)
      .withI18nKey('sqtm-core.entity.requirement.requirement-version.link.role.label')
      .withSortFunction(buildSortRole(translateService)),
    column('edit')
      .withRenderer(ModifyRequirementLinkComponent)
      .withLabel('')
      .changeWidthCalculationStrategy(new Fixed(50)),
    deleteColumn(DeleteRequirementLinkComponent)
  ]).withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withInitialSortedColumns([
      {id: 'role', sort: Sort.ASC}
    ])
    .build();
}

export function buildSortRole(translateService: TranslateService) {
  return (keyA: any, keyB: any) => sortRole(keyA, keyB, translateService);
}


function sortRole(roleA: string, roleB: string, translateService: TranslateService) {
  const translatedRoleA = getTranslatedRole(roleA, translateService);
  const translatedRoleB = getTranslatedRole(roleB, translateService);
  return translatedRoleA.localeCompare(translatedRoleB);
}

function getTranslatedRole(role: string, translateService: TranslateService): string {
  if (role.includes('requirement-version.link.type')) {
    return translateService.instant('sqtm-core.entity.requirement.' + role);
  } else {
    return role;
  }
}
