import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {RequirementMilestoneViewState} from '../../state/requirement-milestone-view-state';
import {RequirementMilestoneViewService} from '../../service/requirement-milestone-view.service';
import {TranslateService} from '@ngx-translate/core';
import {takeUntil, tap} from 'rxjs/operators';
import {CustomDashboardBinding, CustomDashboardModel, FavoriteDashboardValue, ReferentialDataService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-requirement-milestone-view-content',
  templateUrl: './requirement-milestone-view-content.component.html',
  styleUrls: ['./requirement-milestone-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementMilestoneViewContentComponent implements OnInit, OnDestroy {

  public componentData$: Observable<Readonly<RequirementMilestoneViewState>>;

  private readonly unsub$ = new Subject<void>();

  constructor(private viewService: RequirementMilestoneViewService,
              public readonly translateService: TranslateService,
              private referentialDataService: ReferentialDataService) {
  }

  ngOnInit(): void {
    this.componentData$ = this.viewService.componentData$;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  refreshStats($event: MouseEvent) {
    $event.stopPropagation();
    this.viewService.refreshStatistics();
  }

  getMilestoneModeData() {
    return this.referentialDataService.milestoneModeData$
      .pipe(takeUntil(this.unsub$));
  }

  changeDashboardToDisplay($event: MouseEvent, preferenceValue: FavoriteDashboardValue) {
    $event.stopPropagation();
    this.viewService.changeDashboardToDisplay(preferenceValue).pipe(
      tap(() => this.viewService.refreshStatistics())
    ).subscribe();
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings] as CustomDashboardBinding[];
  }
}
