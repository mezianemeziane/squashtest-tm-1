import {
  entitySelector,
  EntityViewState,
  Milestone,
  provideInitialViewState,
  RequirementCriticalityKeys,
  RequirementStatusKeys,
  RequirementVersionLink,
  RequirementVersionStatsBundle,
  SqtmEntityState,
  VerifyingTestCase,
  RemoteRequirementPerimeterStatus
} from 'sqtm-core';
import {createSelector} from '@ngrx/store';
import {verifyingTestCaseEntitySelectors, VerifyingTestCaseState} from './verifying-test-case.state';
import {requirementVersionLinkEntitySelectors, RequirementVersionLinkState} from './requirement-version-link.state';


export interface RequirementVersionViewState extends EntityViewState<RequirementVersionState, 'requirementVersion'> {
  requirementVersion: RequirementVersionState;
}

export function provideInitialRequirementViewState(): Readonly<RequirementVersionViewState> {
  return provideInitialViewState<RequirementVersionState, 'requirementVersion'>('requirementVersion');
}

export interface RequirementVersionState extends SqtmEntityState {
  name: string;
  reference: string;
  versionNumber: number;
  category: number;
  criticality: RequirementCriticalityKeys;
  status: RequirementStatusKeys;
  createdBy: string;
  createdOn: string;
  lastModifiedBy: string;
  lastModifiedOn: string;
  milestones: Milestone[];
  description: string;
  requirementId: number;
  uiState: RequirementVersionViewUiState;
  hasExtender: boolean;
  bindableMilestones: Milestone[];
  verifyingTestCases: VerifyingTestCaseState;
  requirementVersionLinks: RequirementVersionLinkState;
  requirementStats: RequirementVersionStatsBundle;
  statusAllowModification: boolean;
  nbIssues: number;
  remoteReqUrl: string;
  remoteReqId: string;
  syncStatus: string;
  remoteReqPerimeterStatus: RemoteRequirementPerimeterStatus;
}

export interface RequirementVersionViewUiState {
  openTestCaseTreePicker: boolean;
  openRequirementTreePicker: boolean;
}

export const verifyingTestCaseStateSelector = createSelector(entitySelector, (requirementVersion: RequirementVersionState) => {
  return requirementVersion.verifyingTestCases;
});

export const verifyingTestCaseSelector = createSelector(verifyingTestCaseStateSelector, verifyingTestCaseEntitySelectors.selectAll);

export const verifyingTestCaseCountSelect = createSelector(verifyingTestCaseSelector, (verifyingTestCases: VerifyingTestCase[]) => {
  return verifyingTestCases.length;
});

export const requirementVersionLinkStateSelector = createSelector(entitySelector, (requirementVersion: RequirementVersionState) => {
  return requirementVersion.requirementVersionLinks;
});

export const requirementVersionLinkSelector = createSelector(requirementVersionLinkStateSelector,
  requirementVersionLinkEntitySelectors.selectAll);

export const requirementVersionLinkCountSelector = createSelector(requirementVersionLinkSelector,
  (requirementVersionLinks: RequirementVersionLink[]) => {
    return requirementVersionLinks.length;
  });
