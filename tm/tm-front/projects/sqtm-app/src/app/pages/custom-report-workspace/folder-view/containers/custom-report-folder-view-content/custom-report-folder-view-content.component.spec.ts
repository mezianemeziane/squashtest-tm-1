import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomReportFolderViewContentComponent } from './custom-report-folder-view-content.component';
import {CustomReportFolderViewService} from '../../services/custom-report-folder-view.service';
import {EMPTY} from 'rxjs';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import SpyObj = jasmine.SpyObj;
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('CustomReportFolderViewContentComponent', () => {
  let component: CustomReportFolderViewContentComponent;
  let fixture: ComponentFixture<CustomReportFolderViewContentComponent>;

  // tslint:disable-next-line:max-line-length
  const customReportFolderViewService: SpyObj<CustomReportFolderViewService> = jasmine.createSpyObj('customReportFolderViewService', ['load']);
  customReportFolderViewService.componentData$ = EMPTY;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [ CustomReportFolderViewContentComponent ],
      providers: [
        {
          provide: CustomReportFolderViewService,
          useValue: customReportFolderViewService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomReportFolderViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
