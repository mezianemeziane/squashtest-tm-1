import {Injectable} from '@angular/core';
import {CustomExportWorkbenchData, Identifier, RestService} from 'sqtm-core';
import {Observable} from 'rxjs';
import {
  CraftedExport,
  craftedExportToNewCustomExportModel
} from '../../../../components/custom-export-workbench/state/custom-export-workbench.state';
import {pluck} from 'rxjs/operators';

@Injectable()
export class CreateCustomExportViewService {

  constructor(public readonly restService: RestService) {
  }

  fetchWorkbenchData(containerId: number): Observable<CustomExportWorkbenchData> {
    return this.restService.get<CustomExportWorkbenchData>(['custom-exports/workbench', containerId.toString()]);
  }

  createCustomExport(customExport: CraftedExport, containerId: Identifier): Observable<number> {
    const body = craftedExportToNewCustomExportModel(customExport);
    return this.restService.post<{ id: number }>(['custom-exports/new', containerId.toString()], body)
      .pipe(pluck('id'));
  }

  updateCustomExport(customExport: CraftedExport): Observable<number> {
    if (!customExport.existingNodeId) {
      throw new Error('Cannot update custom export without CRLN ID');
    }

    const body = craftedExportToNewCustomExportModel(customExport);
    return this.restService.post<{ id: number }>(['custom-exports/update', customExport.existingNodeId.toString()], body)
      .pipe(pluck('id'));
  }
}
