import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CustomReportWorkspaceTreeComponent} from './custom-report-workspace-tree.component';
import {EMPTY, of} from 'rxjs';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogService, GridService} from 'sqtm-core';
import {CR_WS_TREE} from '../../../custom-report-workspace.constant';

describe('CustomReportWorkspaceTreeComponent', () => {
  let component: CustomReportWorkspaceTreeComponent;
  let fixture: ComponentFixture<CustomReportWorkspaceTreeComponent>;
  const tableMock = jasmine.createSpyObj('tableMock', ['load', 'setColumnSorts', 'addFilters', 'inactivateFilter', 'activateFilter']);
  tableMock.selectedRows$ = EMPTY;
  tableMock.openedRowIds$ = EMPTY;
  tableMock.sortedColumns$ = EMPTY;
  tableMock.loaded$ = of(false);

  const dialogService = jasmine.createSpyObj('dialogService', ['create']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, NzDropDownModule, RouterTestingModule],
      declarations: [CustomReportWorkspaceTreeComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{
        provide: DialogService,
        useValue: dialogService
      }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    TestBed.overrideProvider(CR_WS_TREE, {useValue: tableMock});
    TestBed.overrideProvider(GridService, {useValue: tableMock});
    fixture = TestBed.createComponent(CustomReportWorkspaceTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
