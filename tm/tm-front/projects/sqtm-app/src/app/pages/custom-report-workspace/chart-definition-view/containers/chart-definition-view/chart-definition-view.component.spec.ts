import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChartDefinitionViewComponent } from './chart-definition-view.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import {WorkspaceWithTreeComponent} from 'sqtm-core';

describe('ChartDefinitionViewComponent', () => {
  let component: ChartDefinitionViewComponent;
  let fixture: ComponentFixture<ChartDefinitionViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule, NzDropDownModule],
      declarations: [ ChartDefinitionViewComponent ],
      providers: [
        {provide: WorkspaceWithTreeComponent, useValue: WorkspaceWithTreeComponent}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartDefinitionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
