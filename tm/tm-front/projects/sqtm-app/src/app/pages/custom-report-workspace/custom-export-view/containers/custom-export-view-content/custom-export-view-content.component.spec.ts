import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CustomExportViewContentComponent} from './custom-export-view-content.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {CustomExportViewService} from '../../services/custom-export-view.service';
import {RouterTestingModule} from '@angular/router/testing';
import {DialogService} from 'sqtm-core';

describe('CustomExportViewContentComponent', () => {
  let component: CustomExportViewContentComponent;
  let fixture: ComponentFixture<CustomExportViewContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [CustomExportViewContentComponent],
      providers: [
        {provide: CustomExportViewService, useValue: jasmine.createSpyObj(['load'])},
        {provide: DialogService, useValue: jasmine.createSpyObj(['openAlert'])},
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomExportViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
