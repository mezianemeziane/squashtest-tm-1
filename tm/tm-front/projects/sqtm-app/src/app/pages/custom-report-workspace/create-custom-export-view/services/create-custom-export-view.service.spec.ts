import { TestBed } from '@angular/core/testing';

import { CreateCustomExportViewService } from './create-custom-export-view.service';
import {mockRestService} from '../../../../utils/testing-utils/mocks.service';
import {RestService} from 'sqtm-core';

describe('CreateCustomExportViewService', () => {
  let service: CreateCustomExportViewService;

  beforeEach(() => {
    const restService = mockRestService();

    TestBed.configureTestingModule({
      providers: [
        {provide: RestService, useValue: restService},
        {
          provide: CreateCustomExportViewService,
          useClass: CreateCustomExportViewService,
          deps: [RestService],
        },
      ]
    });
    service = TestBed.inject(CreateCustomExportViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
