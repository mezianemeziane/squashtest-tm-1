import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CustomReportWorkspaceComponent} from './custom-report-workspace/containers/custom-report-workspace/custom-report-workspace.component';
import {RouterModule, Routes} from '@angular/router';
import {
  AnchorModule,
  GridModule,
  NavBarModule,
  SqtmDragAndDropModule,
  SvgModule,
  UiManagerModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule
} from 'sqtm-core';
import {CustomReportWorkspaceTreeComponent} from './custom-report-workspace/containers/custom-report-workspace-tree/custom-report-workspace-tree.component';
import {ChartDefinitionViewComponent} from './chart-definition-view/containers/chart-definition-view/chart-definition-view.component';
import {ChartDefinitionViewContentComponent} from './chart-definition-view/containers/chart-definition-view-content/chart-definition-view-content.component';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCollapseModule} from 'ng-zorro-antd/collapse';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {TranslateModule} from '@ngx-translate/core';
import {ChartsModule} from '../../components/charts/charts.module';
import {CreateChartViewComponent} from './create-chart-view/containers/create-chart-view/create-chart-view.component';
import {ChartWorkbenchModule} from '../../components/chart-workbench/chart-workbench.module';
import {DashboardViewComponent} from './dashboard-view/containers/dashboard-view/dashboard-view.component';
import {CustomDashboardModule} from '../../components/custom-dashboard/custom-dashboard.module';
import {DashboardViewContentComponent} from './dashboard-view/containers/dashboard-view-content/dashboard-view-content.component';
import {CustomReportLibraryViewComponent} from './library-view/containers/custom-report-library-view/custom-report-library-view.component';
import {CustomReportFolderViewComponent} from './folder-view/containers/custom-report-folder-view/custom-report-folder-view.component';
import {CustomReportLibraryViewContentComponent} from './library-view/containers/custom-report-library-view-content/custom-report-library-view-content.component';
import {CustomReportFolderViewContentComponent} from './folder-view/containers/custom-report-folder-view-content/custom-report-folder-view-content.component';
import {CreateReportViewComponent} from './create-report-view/create-report-view.component';
import {ReportWorkbenchModule} from '../../components/report-workbench/report-workbench.module';
import {CreateCustomExportViewComponent} from './create-custom-export-view/containers/create-custom-export-view/create-custom-export-view.component';
import {CustomExportWorkbenchModule} from '../../components/custom-export-workbench/custom-export-workbench.module';
import {CustomExportViewComponent} from './custom-export-view/containers/custom-export-view/custom-export-view.component';
import {CustomExportViewContentComponent} from './custom-export-view/containers/custom-export-view-content/custom-export-view-content.component';
import {CustomExportInformationPanelComponent} from './custom-export-view/components/custom-export-information-panel/custom-export-information-panel.component';
import {ModifyChartViewComponent} from './modify-chart-view/modify-chart-view.component';
import {ReportDefinitionViewComponent} from './report-definition-view/containers/report-definition-view/report-definition-view.component';
import {ReportDefinitionViewContentComponent} from './report-definition-view/containers/report-definition-view-content/report-definition-view-content.component';
import {ModifyReportViewComponent} from './modify-report-view/modify-report-view.component';
import {CustomReportMultipleViewComponent} from './custom-report-multiple-view/custom-report-multiple-view.component';
import {TestCaseWorkspaceMultiSelectViewComponent} from '../test-case-workspace/test-case-workspace-multi-select-view/containers/test-case-workspace-multi-select-view/test-case-workspace-multi-select-view.component';
import {TestCaseMultiViewContentComponent} from '../test-case-workspace/test-case-workspace-multi-select-view/containers/test-case-multi-view-content/test-case-multi-view-content.component';

export const routes: Routes = [
  {
    path: '',
    component: CustomReportWorkspaceComponent,
    children: [
      {
        path: 'custom-report-library/:customReportLibraryNodeId',
        component: CustomReportLibraryViewComponent,
        children: [
          {
            path: '',
            redirectTo: 'content',
          },
          {
            path: 'content',
            component: CustomReportLibraryViewContentComponent
          },
        ]
      },
      {
        path: 'custom-report-folder/:customReportLibraryNodeId',
        component: CustomReportFolderViewComponent,
        children: [
          {
            path: '',
            redirectTo: 'content',
          },
          {
            path: 'content',
            component: CustomReportFolderViewContentComponent
          },
        ]
      },
      {
        path: 'chart-definition/:customReportLibraryNodeId',
        component: ChartDefinitionViewComponent,
        children: [
          {
            path: 'content',
            component: ChartDefinitionViewContentComponent
          },
        ]
      },
      {
        path: 'custom-report-dashboard/:customReportLibraryNodeId',
        component: DashboardViewComponent,
        children: [
          {
            path: 'content',
            component: DashboardViewContentComponent
          },
        ]
      },
      {
        path: 'custom-report-custom-export/:customReportLibraryNodeId',
        component: CustomExportViewComponent,
        children: [
          {
            path: '',
            redirectTo: 'content',
          },
          {
            path: 'content',
            component: CustomExportViewContentComponent
          },
        ]
      },
      {
        path: 'report-definition/:customReportLibraryNodeId',
        component: ReportDefinitionViewComponent,
        children: [
          {
            path: 'content',
            component: ReportDefinitionViewContentComponent
          },
        ]
      },
      {
        path: 'custom-report-workspace-multi', component: CustomReportMultipleViewComponent
      },
    ]
  },
  {
    path: 'create-chart-definition/:customReportLibraryNodeId',
    component: CreateChartViewComponent
  },
  {
    path: 'modify-chart-definition/:customReportLibraryNodeId',
    component: ModifyChartViewComponent
  },
  {
    path: 'create-report-definition/:customReportLibraryNodeId',
    component: CreateReportViewComponent
  },
  {
    path: 'create-custom-export/:customReportLibraryNodeId',
    component: CreateCustomExportViewComponent
  },
  {
    path: 'modify-report-definition/:customReportLibraryNodeId',
    component: ModifyReportViewComponent
  }
];

@NgModule({
  declarations: [
    CustomReportWorkspaceComponent,
    CustomReportWorkspaceTreeComponent,
    ChartDefinitionViewComponent,
    ChartDefinitionViewContentComponent,
    CreateChartViewComponent,
    DashboardViewComponent,
    DashboardViewContentComponent,
    CustomReportLibraryViewComponent,
    CustomReportFolderViewComponent,
    CustomReportLibraryViewContentComponent,
    CustomReportFolderViewContentComponent,
    CreateReportViewComponent,
    CreateCustomExportViewComponent,
    ModifyChartViewComponent,
    CreateCustomExportViewComponent,
    CustomExportViewComponent,
    CustomExportViewContentComponent,
    CustomExportInformationPanelComponent,
    ReportDefinitionViewComponent,
    ReportDefinitionViewContentComponent,
    ModifyReportViewComponent,
    CustomReportMultipleViewComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule.forChild(routes),
    NavBarModule,
    SvgModule,
    WorkspaceCommonModule,
    UiManagerModule,
    GridModule,
    WorkspaceLayoutModule,
    AnchorModule,
    NzToolTipModule,
    NzDropDownModule,
    NzIconModule,
    NzCollapseModule,
    NzButtonModule,
    ChartsModule,
    ChartWorkbenchModule,
    CustomDashboardModule,
    SqtmDragAndDropModule,
    ReportWorkbenchModule,
    CustomExportWorkbenchModule,
  ]
})
export class CustomReportWorkspaceModule {
}
