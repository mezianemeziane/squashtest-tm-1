import {Injectable} from '@angular/core';
import {
  AttachmentService,
  CustomFieldValueService,
  CustomReportPermissions,
  EntityViewAttachmentHelperService,
  EntityViewComponentData,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ProjectData,
  ReferentialDataService,
  ReportDefinitionService,
  ReportDefinitionViewModel,
  RestService
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {map, take, tap, withLatestFrom} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {ReportDefinitionState} from '../state/report-definition-state.state';
import {
  provideInitialReportDefinitionViewState,
  ReportDefinitionViewState
} from '../state/report-definition-view.state';

@Injectable()
export class ReportDefinitionViewService extends EntityViewService<ReportDefinitionState, 'reportDefinition', CustomReportPermissions> {

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected customFieldValueService: CustomFieldValueService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService,
              private reportDefinitionService: ReportDefinitionService) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper
    );
  }

  addSimplePermissions(projectData: ProjectData) {
    return new CustomReportPermissions(projectData);
  }

  getInitialState(): ReportDefinitionViewState {
    return provideInitialReportDefinitionViewState();
  }

  load(customReportLibraryNodeId: number) {
    this.reportDefinitionService.getReportDefinitionViewModel(customReportLibraryNodeId)
      .pipe(withLatestFrom(this.state$))
      .subscribe(([reportDefinitionViewModel, state]: [ReportDefinitionViewModel, ReportDefinitionViewState]) => {
        const reportDefinitionState = {...state.reportDefinition, ...reportDefinitionViewModel};
        this.store.commit(this.updateEntity(reportDefinitionState, state));
      });
  }

  changeReportName(newChartName: string, customReportLibraryNodeId: number): void {
    this.changeReportNameServerSide(newChartName, customReportLibraryNodeId).pipe(
      take(1),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, ReportDefinitionViewState]) => this.updateStateWithNewChartName(state, newChartName)),
    ).subscribe(state => {
      this.store.commit(state);
      this.requireExternalUpdate(customReportLibraryNodeId, 'name');
    });
  }

  private changeReportNameServerSide(newChartName: string, customReportLibraryNodeId: number): Observable<any> {
    return this.restService.post(['report-definition', customReportLibraryNodeId.toString(), 'name'], {name: newChartName});
  }

  private updateStateWithNewChartName(state: ReportDefinitionViewState, newChartName: string): ReportDefinitionViewState {
    return {
      ...state,
      reportDefinition: {
        ...state.reportDefinition,
        name: newChartName
      }
    };
  }
}

export interface ReportDefinitionViewComponentData
  extends EntityViewComponentData<ReportDefinitionState, 'reportDefinition', CustomReportPermissions> {
}
