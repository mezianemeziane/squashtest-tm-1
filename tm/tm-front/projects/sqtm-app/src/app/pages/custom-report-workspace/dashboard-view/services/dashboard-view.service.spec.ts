import {TestBed} from '@angular/core/testing';

import {DashboardViewService} from './dashboard-view.service';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';

describe('DashboardViewService', () => {
  let service: DashboardViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [DashboardViewService]
    });
    service = TestBed.inject(DashboardViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
