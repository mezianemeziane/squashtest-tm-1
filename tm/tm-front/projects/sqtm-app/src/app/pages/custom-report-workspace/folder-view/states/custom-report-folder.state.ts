import {SqtmEntityState} from 'sqtm-core';

export interface CustomReportFolderState extends SqtmEntityState {
  name: string;
  description: string;
}

