import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomReportLibraryViewContentComponent } from './custom-report-library-view-content.component';
import {EMPTY} from 'rxjs';
import SpyObj = jasmine.SpyObj;

import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {CustomReportLibraryViewService} from '../../service/custom-report-library-view.service';

describe('CustomReportLibraryViewContentComponent', () => {
  let component: CustomReportLibraryViewContentComponent;
  let fixture: ComponentFixture<CustomReportLibraryViewContentComponent>;

  // tslint:disable-next-line:max-line-length
  const customReportLibraryViewService: SpyObj<CustomReportLibraryViewService> = jasmine.createSpyObj('customReportLibraryViewService', ['load']);
  customReportLibraryViewService.componentData$ = EMPTY;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [ CustomReportLibraryViewContentComponent ],
      providers: [
        {
          provide: CustomReportLibraryViewService,
          useValue: customReportLibraryViewService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomReportLibraryViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
