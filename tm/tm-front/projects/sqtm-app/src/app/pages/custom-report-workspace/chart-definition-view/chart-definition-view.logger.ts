import {sqtmAppLogger} from '../../../app-logger';

export const chartDefinitionViewLogger = sqtmAppLogger.compose('chart-definition-view');
