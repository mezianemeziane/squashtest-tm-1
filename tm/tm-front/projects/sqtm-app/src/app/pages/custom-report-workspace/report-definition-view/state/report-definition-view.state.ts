import {EntityViewState, provideInitialViewState} from 'sqtm-core';
import {ReportDefinitionState} from './report-definition-state.state';

export interface ReportDefinitionViewState extends EntityViewState<ReportDefinitionState, 'reportDefinition'> {
  reportDefinition: ReportDefinitionState;
}

export function provideInitialReportDefinitionViewState(): Readonly<ReportDefinitionViewState> {
  return provideInitialViewState<ReportDefinitionState, 'reportDefinition'>('reportDefinition');
}
