import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ChartDefinitionViewComponentData} from '../chart-definition-view/chart-definition-view.component';
import {ChartDefinitionViewService} from '../../services/chart-definition-view.service';
import {Observable, of} from 'rxjs';
import {ChartWorkbenchUtils} from '../../../../../components/chart-workbench/components/chart-workbench.utils';
import {
  AxisColumn,
  ChartColumnPrototype,
  ChartDataType,
  ChartDefinitionService,
  ChartFilter,
  ChartOperation,
  ChartScopeType,
  ChartType,
  EntityType,
  InfoList,
  isSystemInfoList,
  MeasureColumn,
  ProjectDataMap,
  ReferentialDataService,
  ResearchColumnPrototype,
  SearchColumnPrototypeUtils,
} from 'sqtm-core';
import {DetailedAxisRole} from '../../../../../components/chart-workbench/state/chart-workbench.state';
import {map, take} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe, formatDate} from '@angular/common';
import {Router} from '@angular/router';

@Component({
  selector: 'sqtm-app-chart-definition-view-content',
  templateUrl: './chart-definition-view-content.component.html',
  styleUrls: ['./chart-definition-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartDefinitionViewContentComponent implements OnInit {

  componentData$: Observable<ChartDefinitionViewComponentData>;
  projectsData$: Observable<ProjectDataMap>;
  infoLists$: Observable<InfoList[]>;

  detailedAxisRoles = DetailedAxisRole;

  private readonly yAxisIcon = 'sqtm-core-custom-report:up';
  private readonly xAxisIcon = 'sqtm-core-custom-report:right';

  constructor(private chartDefinitionViewService: ChartDefinitionViewService,
              private chartDefinitionService: ChartDefinitionService,
              private referentialDataService: ReferentialDataService,
              private translateService: TranslateService,
              private datePipe: DatePipe,
              private router: Router) {
  }

  ngOnInit(): void {
    this.componentData$ = this.chartDefinitionViewService.componentData$;
    this.projectsData$ = this.referentialDataService.projectDatas$;
    this.infoLists$ = this.referentialDataService.infoLists$;
  }

  refreshStats($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.chartDefinitionViewService.refresh();
  }

  modifyChart($event: MouseEvent, customReportLibraryNodeId: number) {
    $event.stopPropagation();
    $event.preventDefault();
    this.router.navigate(['custom-report-workspace', 'modify-chart-definition', customReportLibraryNodeId.toString()]);
  }


  downloadChart($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.chartDefinitionService.downloadChart();
  }

  getPerimeterLabel(componentData: ChartDefinitionViewComponentData, projectData: ProjectDataMap) {
    switch (componentData.chartDefinition.scopeType) {
      case (ChartScopeType.DEFAULT):
        return componentData.projectData.name;
      case (ChartScopeType.PROJECTS):
        const projectIds = componentData.chartDefinition.projectScope.map(id => Number(id));
        return this.getPojectNames(projectData, projectIds);
      case (ChartScopeType.CUSTOM):
        return this.getCustomPerimeter(componentData, projectData);
      default:
        throw Error(`Unable to find scope type for type ${componentData.chartDefinition.scopeType}`);
    }
  }

  private getPojectNames(projectDataMap: ProjectDataMap, projectIds: number[]) {
    const projects = Object.values(projectDataMap).filter((projectData) => projectIds.includes(projectData.id));
    const projectNames = projects.map(project => project.name);
    projectNames.sort((nameA, nameB) => nameA.localeCompare(nameB));
    return projectNames.join(', ');
  }

  private getCustomPerimeter(componentData: ChartDefinitionViewComponentData, projectData: ProjectDataMap) {
    switch (componentData.chartDefinition.scope[0].type) {
      case(EntityType.TEST_CASE):
      case(EntityType.TEST_CASE_LIBRARY):
      case(EntityType.TEST_CASE_FOLDER):
        return this.translateCustomPerimeter(EntityType.TEST_CASE, componentData, projectData);
      case(EntityType.CAMPAIGN):
      case(EntityType.CAMPAIGN_LIBRARY):
      case(EntityType.CAMPAIGN_FOLDER):
        return this.translateCustomPerimeter(EntityType.CAMPAIGN, componentData, projectData);
      case(EntityType.REQUIREMENT):
      case(EntityType.REQUIREMENT_LIBRARY):
      case(EntityType.REQUIREMENT_FOLDER):
        return this.translateCustomPerimeter(EntityType.REQUIREMENT, componentData, projectData);
      default:
        throw Error(`Unable to find entity type for type ${componentData.chartDefinition.scope[0].type}`);
    }
  }

  private translateCustomPerimeter(entityType, componentData: ChartDefinitionViewComponentData, projectData: ProjectDataMap) {
    return this.checkIfMulitpleProjectsInScope(componentData) ?
      this.getMultipleProjectCustomScope(entityType) :
      this.getSingleProjectCustomScope(entityType, componentData, projectData);
  }

  private getSingleProjectCustomScope(entityType, componentData: ChartDefinitionViewComponentData, projectData: ProjectDataMap) {
    // We know we have only one project in scope
    const projectId = componentData.chartDefinition.projectScope.map(id => Number(id))[0];
    const translateParam = {projectName: projectData[projectId]?.name || ''};
    return this.translateService.instant(`sqtm-core.custom-report-workspace.chart.perimeter.custom.${entityType}.single-project`,
      translateParam);
  }

  private getMultipleProjectCustomScope(entityType) {
    return this.translateService.instant(`sqtm-core.custom-report-workspace.chart.perimeter.custom.${entityType}.multiple-projects`);
  }

  private checkIfMulitpleProjectsInScope(componentData: ChartDefinitionViewComponentData) {
    return componentData.chartDefinition.projectScope.length > 1;
  }

  getGeneratedOnDisplayValue(generatedDate: Date) {
    const generatedOnLabel = this.translateService.instant('sqtm-core.generic.date.prefix.generated-on');
    const createdOnDate = formatDate(generatedDate, 'medium', this.translateService.getBrowserLang());
    return `${generatedOnLabel} ${createdOnDate}`;
  }

  get columnPrototypeBorderColor() {
    return 'var(--current-workspace-main-color)';
  }

  getColumnDisplayValue(column: AxisColumn | MeasureColumn | ChartFilter) {
    const entityType = this.translateService.instant(
      this.getEntityNameI18nKey(column.column.specializedType.entityType)
    );

    const operation = this.translateService.instant(this.getOperationI18nKey(column.operation));

    if (this.isCuf(column)) {
      return this.getCufColumnName(column).pipe(
        take(1),
        map(cufColumnName => this.translateService.instant(cufColumnName)),
        map(translatedCufColumnName => `${entityType} | ${translatedCufColumnName} - ${operation}`),
      );
    } else {
      const columnName =  this.translateService.instant(this.getColumnNameI18nKey(column));
      return of(`${entityType} | ${columnName} - ${operation}`);
    }
  }

  getFilterDisplayValue(filter: ChartFilter, infoLists: InfoList[]) {
    return this.getColumnDisplayValue(filter).pipe(
      take(1),
      map(displayValue => {
        const filterValues = this.convertFilterValues(filter, filter.values, infoLists);
        return `${displayValue} : ${filterValues}`;
      })
    );
  }

  private getEntityNameI18nKey(entityType: EntityType): string {
    return ChartWorkbenchUtils.getEntityNameI18nKey(entityType);
  }

  private getColumnNameI18nKey(column: MeasureColumn | AxisColumn | ChartFilter) {
    return `sqtm-core.custom-report-workspace.chart.columns.${column.column.label}`;
  }

  private getCufColumnName(column: MeasureColumn | AxisColumn | ChartFilter): Observable<string> {
    return this.referentialDataService.connectToCustomField(column.cufId).pipe(
      take(1),
      map(customField => customField.label)
    );
  }

  private getOperationI18nKey(chartOperation: ChartOperation) {
    return ChartWorkbenchUtils.getOperationI18nKey(chartOperation);
  }

  private convertFilterValues(filter: ChartFilter, rawValues: any[], infoLists: InfoList[]): string[] | string {
    switch (filter.column.dataType) {
      case ChartDataType.NUMERIC:
      case ChartDataType.STRING:
        return rawValues.join(', ');

      case ChartDataType.BOOLEAN_AS_STRING:
      case ChartDataType.BOOLEAN:
        if (!this.isCuf(filter)) {
          const convertedValues = rawValues.map(rawValue => {
            return SearchColumnPrototypeUtils.findEnum(ResearchColumnPrototype[filter.column.label])[rawValue];
          }).map(levelEnumItem => this.translateService.instant(levelEnumItem.i18nKey));
          return convertedValues.join(', ');
        } else {
          return rawValues.map(rawValue => this.translateService.instant(`sqtm-core.generic.label.${rawValue.toLowerCase()}`)).join((', '));
        }

      case ChartDataType.DATE:
      case ChartDataType.DATE_AS_STRING:
        return rawValues.map(rawValue => formatDate(rawValue, 'shortDate', this.translateService.getBrowserLang())).join((', '));

      case ChartDataType.INFO_LIST_ITEM:
        return rawValues.map(rawValue => this.convertInfoListItemFilterValue(rawValue, infoLists)).join((', '));

      case ChartDataType.LEVEL_ENUM:
      case ChartDataType.EXECUTION_STATUS:
      case ChartDataType.REQUIREMENT_STATUS:
      case ChartDataType.TAG:
      case ChartDataType.LIST:
        if (!this.isCuf(filter)) {
          const convertedValues = rawValues.map(rawValue => {
            return SearchColumnPrototypeUtils.findEnum(ResearchColumnPrototype[filter.column.label])[rawValue];
          }).map(levelEnumItem => this.translateService.instant(levelEnumItem.i18nKey));
          return convertedValues.join(', ');
        } else {
          return rawValues.join(', ');
        }
      default:
        throw Error(`Chart data type not found for type ${filter.column.dataType}`);
    }
  }

  private convertInfoListItemFilterValue(infoListItemCode: string, infoLists: InfoList[]) {
    const infoList = infoLists.find(list => list.items.find(item => item.code === infoListItemCode));
    if (infoList) {
      if (isSystemInfoList(infoList)) {
        const i18nKey = infoList.items.find(item => item.code === infoListItemCode).label;
        return this.translateService.instant(`sqtm-core.entity.${i18nKey}`);
      } else {
        return infoList.items.find(item => item.code === infoListItemCode).label;
      }
    } else {
      return infoListItemCode;
    }
  }

  getWorkspace(chartColumn: ChartColumnPrototype) {
    const entity = chartColumn.specializedType.entityType;
    return ChartWorkbenchUtils.getWorkspace(entity);
  }

  isCuf(column: MeasureColumn | AxisColumn | ChartFilter) {
    return column.cufId;
  }

  doesMeasureIsRequired(chartType: ChartType) {
    return chartType !== ChartType.PIE;
  }

  doesSeriesAreRequired(chartType: ChartType) {
    return chartType === ChartType.COMPARATIVE || chartType === ChartType.TREND;
  }

  getAxisI18nKey(detailedAxisRole: DetailedAxisRole) {
    return 'sqtm-core.custom-report-workspace.chart.axis.name.' + detailedAxisRole;
  }

  getAxisIcon(chartType: ChartType, detailedAxisRole: DetailedAxisRole) {
    switch (chartType) {
      case ChartType.PIE:
        return 'sqtm-core-custom-report:pie';
      case ChartType.BAR:
      case ChartType.CUMULATIVE:
      case ChartType.TREND:
        return this.getStandardChartIcons(detailedAxisRole);
      case ChartType.COMPARATIVE:
        return this.getReversedChartIcons(detailedAxisRole);

    }
  }

  private getStandardChartIcons(detailedAxisRole: DetailedAxisRole) {
    switch (detailedAxisRole) {
      case DetailedAxisRole.Measure:
        return this.yAxisIcon;
      case DetailedAxisRole.Axis:
        return this.xAxisIcon;
      case DetailedAxisRole.Series:
        return 'unordered-list';
    }
  }

  private getReversedChartIcons(detailedAxisRole: DetailedAxisRole) {
    switch (detailedAxisRole) {
      case DetailedAxisRole.Measure:
        return this.xAxisIcon;
      case DetailedAxisRole.Axis:
        return this.yAxisIcon;
      case DetailedAxisRole.Series:
        return 'unordered-list';
    }
  }

  getAuditableText(date: string, userLogin: string) {
    if (date != null) {
      const formattedDate = this.formatDate(date);
      return `${formattedDate} (${userLogin})`;
    } else {
      return this.translateService.instant('sqtm-core.generic.label.never');
    }
  }

  private formatDate(date: string): string {
    return formatDate(date, 'short', this.translateService.getBrowserLang());
  }

}


