import { Injectable } from '@angular/core';
import {
  AttachmentService,
  CustomFieldValueService,
  CustomReportPermissions,
  CustomReportLibraryModel,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ProjectData,
  ReferentialDataService,
  RestService,
  EntityViewComponentData,
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {
  CustomReportLibraryViewState,
  provideInitialCustomReportLibraryView
} from '../state/custom-report-library-view.state';
import {CustomReportLibraryState} from '../state/custom-report-library.state';


@Injectable()
export class CustomReportLibraryViewService
  extends EntityViewService<CustomReportLibraryState, 'customReportLibrary', CustomReportPermissions> {

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected customFieldValueService: CustomFieldValueService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper
    );
  }

  addSimplePermissions(projectData: ProjectData): CustomReportPermissions {
    return new CustomReportPermissions(projectData);
  }

  getInitialState(): CustomReportLibraryViewState {
    return provideInitialCustomReportLibraryView();
  }

  load(id: number) {
    this.restService.get<CustomReportLibraryModel>(['custom-report-library-view', id.toString()])
      .subscribe((customReportLibraryModel: CustomReportLibraryModel) => {
        const customReportLibrary = this.initializeCustomReportLibraryState(customReportLibraryModel);
        this.initializeEntityState(customReportLibrary);
      });
  }

  private initializeCustomReportLibraryState(customReportLibraryModel: CustomReportLibraryModel): CustomReportLibraryState {
    const customFieldValueState = this.initializeCustomFieldValueState(customReportLibraryModel.customFieldValues);
    return {
      ...customReportLibraryModel,
      attachmentList: {id: null, attachments: null},
      customFieldValues: customFieldValueState
    };
  }
}

export interface CustomReportLibraryViewComponentData
  extends EntityViewComponentData<CustomReportLibraryState, 'customReportLibrary', CustomReportPermissions> {
}
