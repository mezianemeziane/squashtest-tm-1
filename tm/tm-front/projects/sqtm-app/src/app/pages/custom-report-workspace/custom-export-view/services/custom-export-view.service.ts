import {Injectable} from '@angular/core';
import {
  AttachmentService,
  CustomExportModel,
  CustomFieldValueService,
  CustomReportPermissions,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ProjectData,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {CustomExportState} from '../state/custom-export.state';
import {CustomExportViewState, provideInitialCustomExportView} from '../state/custom-export-view.state';
import {map, pluck, withLatestFrom} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable()
export class CustomExportViewService extends EntityViewService<CustomExportState, 'customExport', CustomReportPermissions> {

  public readonly downloadUrl$: Observable<string>;

  private static downloadUrlBase = 'custom-exports/generate/';

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected customFieldValueService: CustomFieldValueService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper
    );

    this.downloadUrl$ = this.componentData$.pipe(
      pluck('customExport', 'customReportLibraryNodeId'),
      map(nodeId => this.restService.backendRootUrl + CustomExportViewService.downloadUrlBase + nodeId));
  }

  addSimplePermissions(projectData: ProjectData) {
    return new CustomReportPermissions(projectData);
  }

  getInitialState(): CustomExportViewState {
    return provideInitialCustomExportView();
  }

  load(id: number): void {
    this.restService.get<CustomExportModel>(['custom-report-custom-export-view', id.toString()])
      .pipe(withLatestFrom(this.state$))
      .subscribe(([model, state]: [CustomExportModel, CustomExportViewState]) => {
        const customExportState = {...state.customExport, ...model};
        this.store.commit(this.updateEntity(customExportState, state));
      });
  }
}
