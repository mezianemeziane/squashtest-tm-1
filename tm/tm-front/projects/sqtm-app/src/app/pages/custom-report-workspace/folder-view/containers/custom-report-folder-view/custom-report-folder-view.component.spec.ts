import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomReportFolderViewComponent } from './custom-report-folder-view.component';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {WorkspaceWithTreeComponent} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('CustomReportFolderViewComponent', () => {
  let component: CustomReportFolderViewComponent;
  let fixture: ComponentFixture<CustomReportFolderViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      declarations: [ CustomReportFolderViewComponent ],
      providers: [
        {provide: WorkspaceWithTreeComponent, useValue: WorkspaceWithTreeComponent}
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomReportFolderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
