import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyReportViewComponent } from './modify-report-view.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../utils/testing-utils/app-testing-utils.module';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('ModifyReportViewComponent', () => {
  let component: ModifyReportViewComponent;
  let fixture: ComponentFixture<ModifyReportViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, RouterTestingModule],
      declarations: [ ModifyReportViewComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyReportViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
