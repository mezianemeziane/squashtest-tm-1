import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CustomReportWorkspaceComponent} from './custom-report-workspace.component';
import {TranslateService} from '@ngx-translate/core';
import {EventEmitter, NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {of} from 'rxjs';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {DialogService} from 'sqtm-core';
import {OverlayModule} from '@angular/cdk/overlay';

describe('CustomReportWorkspaceComponent', () => {
  let component: CustomReportWorkspaceComponent;
  let fixture: ComponentFixture<CustomReportWorkspaceComponent>;

  let fakeTranslateService = jasmine.createSpyObj<TranslateService>('TranslateService', ['get']);

  // See administration-workspace.component.spec
  fakeTranslateService = {
    ...fakeTranslateService,
    onLangChange: new EventEmitter(),
    onTranslationChange: new EventEmitter(),
    onDefaultLangChange: new EventEmitter()
  } as jasmine.SpyObj<TranslateService>;
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  // See administration-workspace.component.spec
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule, AppTestingUtilsModule, OverlayModule],
      declarations: [CustomReportWorkspaceComponent],
      providers: [
        {provide: TranslateService, useValue: fakeTranslateService},
        {
          provide: DialogService,
          useValue: dialogService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents()
      .then(() => {
        fakeTranslateService.get.and.returnValue(of());
        fixture = TestBed.createComponent(CustomReportWorkspaceComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
