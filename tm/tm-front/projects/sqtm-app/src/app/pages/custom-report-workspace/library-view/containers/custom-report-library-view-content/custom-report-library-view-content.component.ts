import {Component, OnInit, ChangeDetectionStrategy, OnDestroy} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {
  CustomReportLibraryViewComponentData,
  CustomReportLibraryViewService
} from '../../service/custom-report-library-view.service';

@Component({
  selector: 'sqtm-app-custom-report-library-view-content',
  templateUrl: './custom-report-library-view-content.component.html',
  styleUrls: ['./custom-report-library-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomReportLibraryViewContentComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();
  componentData$: Observable<CustomReportLibraryViewComponentData>;

  constructor( private customReportLibraryViewService: CustomReportLibraryViewService) {
    this.componentData$ = customReportLibraryViewService.componentData$;
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}
