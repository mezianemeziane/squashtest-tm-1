import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CustomExportViewComponent} from './custom-export-view.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {EMPTY} from 'rxjs';
import {mockPassThroughTranslateService, mockRestService} from '../../../../../utils/testing-utils/mocks.service';
import {RestService, WorkspaceWithTreeComponent} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateService} from '@ngx-translate/core';

describe('CustomExportViewComponent', () => {
  let component: CustomExportViewComponent;
  let fixture: ComponentFixture<CustomExportViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [CustomExportViewComponent],
      providers: [
        {provide: ActivatedRoute, useValue: {paramMap: EMPTY}},
        {provide: RestService, useValue: mockRestService()},
        {provide: TranslateService, useValue: mockPassThroughTranslateService()},
        {provide: WorkspaceWithTreeComponent, useValue: jasmine.createSpyObj<WorkspaceWithTreeComponent>(['requireNodeRefresh'])},
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomExportViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
