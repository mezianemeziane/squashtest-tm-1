import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InformationPageComponent} from './containers/information-page/information-page.component';
import {NavBarModule, UiManagerModule} from 'sqtm-core';
import {RouterModule, Routes} from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    component: InformationPageComponent
  }
];

@NgModule({
  declarations: [InformationPageComponent],
  imports: [
    CommonModule,
    UiManagerModule,
    NavBarModule,
    RouterModule.forChild(routes),
  ]
})
export class InformationModule {
}
