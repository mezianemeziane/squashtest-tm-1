import {ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {
  EntityScope, FilterGroup,
  GridFilter,
  GridService,
  isMilestoneModeActivated,
  MilestoneModeData,
  MultiDiscreteFilterValue,
  ProjectData,
  ProjectReference,
  ReferentialDataService,
  Scope,
  SimpleFilter,
  SimpleScope
} from 'sqtm-core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {combineLatest, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {searchViewLogger} from '../../search-view.logger';
import {
  BACK_URL_PARAM,
  FILTER_QUERY_PARAM_KEY,
  MILESTONE_END_DATE,
  MILESTONE_FILTER,
  MILESTONE_STATUS
} from '../../../search-constants';

const logger = searchViewLogger.compose('SearchViewComponent');

@Component({
  selector: 'sqtm-app-search-view',
  templateUrl: './search-view.component.html',
  styleUrls: ['./search-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchViewComponent implements OnInit, OnDestroy {

  @Input()
  workspaceName: string;

  @Input()
  titleKey: string;

  @Input()
  resultTitleKey = 'sqtm-core.search.generic.results.title';

  @Input()
  filters: GridFilter[] = [];

  @Input()
  filterGroups: FilterGroup[] = [];

  private unsub$ = new Subject<void>();

  constructor(public gridService: GridService,
              private referentialDataService: ReferentialDataService,
              private activatedRoute: ActivatedRoute,
              public router: Router) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  toggleFilterPanel() {
    this.gridService.toggleFilterManager();
  }

  initializeResearch() {
    combineLatest([
      this.referentialDataService.filteredProjects$,
      this.referentialDataService.milestoneModeData$,
      this.activatedRoute.queryParamMap]
    ).pipe(
      takeUntil(this.unsub$)
    ).subscribe(([projects, milestoneModeData, paramMap]) => {
      logger.debug('Initialize research with param map ', [paramMap]);
      const filters = this.initializeFilters(paramMap, milestoneModeData);
      const scope = this.initializeScope(projects, paramMap);
      this.initializeGrid(filters, scope);
    });
  }

  private initializeScope(projects: ProjectData[], paramMap: ParamMap) {
    const projectScope = this.extractProjectScope(projects);
    const scope = this.initializeScopeFromUrl(paramMap, projectScope);
    logger.debug('Initialize research with perimeter ', [scope]);
    return scope;
  }

  private initializeFilters(paramMap: ParamMap, milestoneModeData: MilestoneModeData) {
    const filtersMap = this.buildFilterMap(this.filters);
    this.appendFiltersFromUrlParams(filtersMap, paramMap);
    const milestoneLabelFilter: GridFilter = filtersMap.get(MILESTONE_FILTER);

    if (isMilestoneModeActivated(milestoneModeData)) {
      const value: MultiDiscreteFilterValue = {
        kind: 'multiple-discrete-value',
        value: [{id: milestoneModeData.selectedMilestone.id, label: milestoneModeData.selectedMilestone.label}]
      };
      milestoneLabelFilter.value = value;
      milestoneLabelFilter.initialValue = value;
      milestoneLabelFilter.active = true;
      milestoneLabelFilter.alwaysActive = true;
      milestoneLabelFilter.preventOpening = true;
      milestoneLabelFilter.lockedValue = true;

      filtersMap.delete(MILESTONE_STATUS);
      filtersMap.delete(MILESTONE_END_DATE);

    } else {
      milestoneLabelFilter.value = {kind: 'multiple-discrete-value', value: []};
      milestoneLabelFilter.initialValue = {kind: 'multiple-discrete-value', value: []};
      milestoneLabelFilter.active = false;
      milestoneLabelFilter.alwaysActive = false;
      milestoneLabelFilter.lockedValue = false;
    }
    return Array.from(filtersMap.values());
  }

  private initializeScopeFromUrl(paramMap: ParamMap, projectScope: EntityScope[]) {
    let scope: Scope;
    if (paramMap.has('scope')) {
      const simpleScope = JSON.parse(paramMap.get('scope')) as SimpleScope;
      scope = {...simpleScope, active: true, initialValue: projectScope, initialKind: 'project'};
    } else {
      scope = {
        kind: 'project',
        value: projectScope,
        initialValue: projectScope,
        initialKind: 'project',
        active: true
      };
    }
    return scope;
  }

  private initializeGrid(filters: GridFilter[], scope: Scope) {
    this.gridService.replaceFilters(
      filters,
      scope,
      this.filterGroups);
  }

  private extractProjectScope(projects: ProjectData[]) {
    const projectScope: EntityScope[] = projects
      .map(p => {
        const ref = new ProjectReference(p.id).asString();
        return {id: ref, label: p.name, projectId: p.id};
      });
    return projectScope;
  }

  private appendFiltersFromUrlParams(filtersMap: Map<string, GridFilter>, paramMap: ParamMap) {
    if (paramMap.has(FILTER_QUERY_PARAM_KEY)) {
      const simpleFilters: SimpleFilter[] = JSON.parse(paramMap.get(FILTER_QUERY_PARAM_KEY));
      simpleFilters.forEach(simpleFilter => {
        const gridFilter: GridFilter = filtersMap.get(simpleFilter.id.toString());
        gridFilter.value = simpleFilter.value;
        gridFilter.operation = simpleFilter.operation;
        gridFilter.active = true;
        gridFilter.preventOpening = true;
      });
    }
  }

  private buildFilterMap(filters: GridFilter[]) {
    return filters.reduce((filterMap, f) => {
      filterMap.set(f.id, f);
      return filterMap;
    }, new Map());
  }

  navigateBack(): void {
    const backUrl = this.activatedRoute.snapshot.queryParamMap.get(BACK_URL_PARAM);
    this.router.navigate([backUrl]);

  }

  hasBackUrlParam(): boolean {
    return this.activatedRoute.snapshot.queryParamMap.get(BACK_URL_PARAM) != null;
  }
}
