import {
  BindableEntity,
  buildFilters,
  DataRow,
  dateFilter,
  Extendable,
  FilterGroup,
  FilterOperation,
  FilterTestCaseMilestoneLabelComponent,
  Fixed,
  fullTextResearchFilter,
  GridFilter,
  i18nEnumResearchFilter,
  i18nEnumSingleSelectionResearchFilter,
  iconLinkColumn,
  idListSearchFilter,
  indexColumn,
  infoListColumn,
  infoListResearchFilter,
  isRequirementEditable,
  isRequirementStatusEditable,
  levelEnumColumn,
  multipleListResearchFilter,
  numericColumn,
  numericResearchFilter,
  ProjectDataMap,
  Requirement,
  RequirementCriticality,
  RequirementCurrentVersionFilter,
  RequirementPermissions,
  RequirementStatus,
  ResearchColumnPrototype,
  ScopeDefinitionBuilder,
  searchGrid,
  Sort,
  SquashTmDataRow,
  StyleDefinitionBuilder,
  textColumn,
  textResearchFilter,
  userHistoryResearchFilter,
  WithOperationFilterValueRendererComponent
} from 'sqtm-core';
import {MILESTONE_END_DATE, MILESTONE_FILTER, MILESTONE_STATUS} from '../../search-constants';
import {FilterLinkTypeComponent} from '../components/filters/filter-link-type/filter-link-type.component';
import {requirementSearchEditableText} from '../components/cell-renderers/requirement-editable-cell-renderer/requirement-editable-cell-renderer.component';

export class RequirementSearchGridBuilders {

  public buildFilters(): GridFilter[] {
    return buildFilters([
      numericResearchFilter('id', ResearchColumnPrototype.REQUIREMENT_VERSION_ID)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.id.capitalize'),
      idListSearchFilter('idList', ResearchColumnPrototype.REQUIREMENT_ID)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.id-list.label'),
      textResearchFilter('reference', ResearchColumnPrototype.REQUIREMENT_VERSION_REFERENCE)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      textResearchFilter('name', ResearchColumnPrototype.REQUIREMENT_VERSION_NAME)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.name.label'),
      fullTextResearchFilter('description', ResearchColumnPrototype.REQUIREMENT_VERSION_DESCRIPTION)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.description.label'),
      dateFilter('createdOn', ResearchColumnPrototype.REQUIREMENT_VERSION_CREATED_ON)
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.created-on.feminine'),
      userHistoryResearchFilter('createdBy', ResearchColumnPrototype.REQUIREMENT_VERSION_CREATED_BY)
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.created-by.feminine'),
      dateFilter('modifiedOn', ResearchColumnPrototype.REQUIREMENT_VERSION_MODIFIED_ON)
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.last-modified-on.feminine'),
      userHistoryResearchFilter('modifiedBy', ResearchColumnPrototype.REQUIREMENT_VERSION_MODIFIED_BY)
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.last-modified-by.feminine'),
      multipleListResearchFilter(MILESTONE_FILTER,
        ResearchColumnPrototype.REQUIREMENT_VERSION_MILESTONE_ID,
        FilterTestCaseMilestoneLabelComponent)
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.search.generic.criteria.milestone.name'),
      i18nEnumResearchFilter(MILESTONE_STATUS, ResearchColumnPrototype.REQUIREMENT_VERSION_SEARCHABLE_MILESTONE_STATUS)
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.search.generic.criteria.milestone.status'),
      dateFilter(MILESTONE_END_DATE, ResearchColumnPrototype.REQUIREMENT_VERSION_MILESTONE_END_DATE)
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.entity.milestone.end-date.label'),
      i18nEnumSingleSelectionResearchFilter('requirementVersionCurrentVersion',
        ResearchColumnPrototype.REQUIREMENT_VERSION_CURRENT_VERSION,
        RequirementCurrentVersionFilter.ALL)
        .withI18nKey('sqtm-core.search.requirement.criteria.current-version.title')
        .alwaysActive(),
      i18nEnumResearchFilter('criticality', ResearchColumnPrototype.REQUIREMENT_VERSION_CRITICALITY)
        .withGroupId('attributes')
        .withI18nKey('sqtm-core.entity.requirement.criticality.label'),
      infoListResearchFilter('category', ResearchColumnPrototype.REQUIREMENT_VERSION_CATEGORY_ID)
        .withI18nKey('sqtm-core.entity.requirement.category.label'),
      i18nEnumResearchFilter('status', ResearchColumnPrototype.REQUIREMENT_VERSION_STATUS)
        .withGroupId('attributes')
        .withI18nKey('sqtm-core.entity.generic.status.label'),
      numericResearchFilter('attachmentCount', ResearchColumnPrototype.REQUIREMENT_VERSION_ATTCOUNT)
        .withGroupId('content')
        .withI18nKey('sqtm-core.search.generic.criteria.attachment-count'),
      i18nEnumSingleSelectionResearchFilter('hasDescription',
        ResearchColumnPrototype.REQUIREMENT_VERSION_HAS_DESCRIPTION)
        .withGroupId('content')
        .withI18nKey('sqtm-core.search.requirement.criteria.has-description'),
      numericResearchFilter('coveragesCount', ResearchColumnPrototype.REQUIREMENT_VERSION_TCCOUNT)
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.requirement.criteria.coverages'),
      i18nEnumSingleSelectionResearchFilter('hasChildren',
        ResearchColumnPrototype.REQUIREMENT_VERSION_HAS_CHILDREN)
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.requirement.criteria.has-children.label'),
      i18nEnumSingleSelectionResearchFilter('hasParent',
        ResearchColumnPrototype.REQUIREMENT_VERSION_HAS_PARENT)
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.requirement.criteria.has-parent.label'),
      multipleListResearchFilter('hasLinkType',
        ResearchColumnPrototype.REQUIREMENT_VERSION_HAS_LINK_TYPE,
        FilterLinkTypeComponent)
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.requirement.criteria.has-link.label')
        .withAvailableOperations([FilterOperation.AT_LEAST_ONE, FilterOperation.NONE])
        .withOperations(FilterOperation.AT_LEAST_ONE)
        .withValueRenderer(WithOperationFilterValueRendererComponent),
    ]);
  }

  public buildFilterGroups(): FilterGroup[] {
    return [{
      id: 'informations',
      i18nLabelKey: 'sqtm-core.generic.label.information.plural'
    }, {
      id: 'historical',
      i18nLabelKey: 'sqtm-core.search.generic.criteria.groups.historical'
    }, {
      id: 'attributes',
      i18nLabelKey: 'sqtm-core.entity.generic.attributes'
    }, {
      id: 'milestones',
      i18nLabelKey: 'sqtm-core.generic.label.milestone'
    }, {
      id: 'content',
      i18nLabelKey: 'sqtm-core.search.generic.criteria.groups.content'
    }, {
      id: 'associations',
      i18nLabelKey: 'sqtm-core.search.generic.criteria.groups.associations'
    }, {
      id: 'custom-fields',
      i18nLabelKey: 'sqtm-core.entity.custom-field.label.plural'
    }];
  }

}

export const searchRequirementGridConfigFactory = () => {
  return searchGrid('requirement-search')
    .server()
    .withServerUrl(['search/requirement'])
    .withModificationUrl(['requirement-version'])
    .withRowConverter(convertAllRowsInRequirements)
    .withFilterCufFrom(BindableEntity.REQUIREMENT_VERSION)
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      textColumn('projectName')
        .withI18nKey('sqtm-core.grid.header.project')
        .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_PROJECT_NAME)
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      textColumn('id')
        .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_ID)
        .withI18nKey('sqtm-core.grid.header.id')
        .changeWidthCalculationStrategy(new Fixed(60)),
      requirementSearchEditableText('reference')
        .isEditable(isRequirementEditable)
        .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_REFERENCE)
        .withI18nKey('sqtm-core.grid.header.reference')
        .changeWidthCalculationStrategy(new Fixed(100)),
      requirementSearchEditableText('name')
        .isEditable(isRequirementEditable)
        .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_NAME)
        .withI18nKey('sqtm-core.grid.header.name')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      levelEnumColumn('status', RequirementStatus)
        .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_STATUS)
        .withTitleI18nKey('sqtm-core.entity.generic.status.label')
        .withI18nKey('sqtm-core.entity.generic.status.short')
        .changeWidthCalculationStrategy(new Fixed(40))
        .isEditable(isRequirementStatusEditable),
      levelEnumColumn('criticality', RequirementCriticality)
        .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_CRITICALITY)
        .withTitleI18nKey('sqtm-core.entity.requirement.criticality.label')
        .withI18nKey('sqtm-core.entity.requirement.criticality.label-short-dot')
        .changeWidthCalculationStrategy(new Fixed(40))
        .isEditable(isRequirementEditable),
      infoListColumn('category', {kind: 'infoList', infolist: 'requirementCategory'})
        .withTitleI18nKey('sqtm-core.entity.requirement.category.label')
        .withI18nKey('sqtm-core.entity.requirement.category.label-short-dot')
        .isEditable(isRequirementEditable)
        .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_CATEGORY)
        .changeWidthCalculationStrategy(new Fixed(100)),
      textColumn('createdBy')
        .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_CREATED_BY)
        .withI18nKey('sqtm-core.grid.header.created-by.feminine')
        .changeWidthCalculationStrategy(new Fixed(100)),
      textColumn('lastModifiedBy')
        .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_MODIFIED_BY)
        .withI18nKey('sqtm-core.grid.header.modified-by.feminine')
        .changeWidthCalculationStrategy(new Fixed(100)),
      numericColumn('milestones')
        .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_MILCOUNT)
        .withI18nKey('sqtm-core.search.test-case.grid.header.milestones.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.milestones.title')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn('versionNumber')
        .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_VERS_NUM)
        .withI18nKey('sqtm-core.entity.requirement.version.label')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn('versionsCount')
        .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_NB_VERSIONS)
        .withI18nKey('sqtm-core.search.requirement.grid.header.version-count.label')
        .withTitleI18nKey('sqtm-core.search.requirement.grid.header.version-count.title')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn('attachments')
        .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_ATTCOUNT)
        .withI18nKey('sqtm-core.search.test-case.grid.header.attachments.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.attachments.title')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn('coverages')
        .withColumnPrototype(ResearchColumnPrototype.REQUIREMENT_VERSION_TCCOUNT)
        .withI18nKey('sqtm-core.search.requirement.grid.header.coverages.label')
        .withTitleI18nKey('sqtm-core.search.requirement.grid.header.coverages.title')
        .changeWidthCalculationStrategy(new Fixed(60)),
      iconLinkColumn('detail', {
        kind: 'iconLink',
        baseUrl: '/requirement-workspace/requirement-version/detail',
        columnParamId: 'id',
        iconName: 'sqtm-core-generic:edit'
      }),
      iconLinkColumn('folder', {
        kind: 'iconLink',
        baseUrl: '/requirement-workspace/requirement',
        columnParamId: 'requirementId',
        iconName: 'folder'
      })
        .disableHeader()
        .changeWidthCalculationStrategy(new Fixed(40))
    ])
    .withInitialSortedColumns([
      {id: 'projectName', sort: Sort.ASC},
      {id: 'reference', sort: Sort.ASC},
      {id: 'name', sort: Sort.ASC},
    ])
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation())
    .withScopeDefinition(new ScopeDefinitionBuilder().withCustomScope('requirement'))
    .build();
};


export function convertAllRowsInRequirements(literals: Partial<DataRow>[], projectsData: ProjectDataMap): SquashTmDataRow[] {
  return literals.map(literal => convertOneInRequirement(literal, projectsData));
}

export function convertOneInRequirement(literal: Partial<DataRow>, projectsData: ProjectDataMap): SquashTmDataRow {
  const dataRow: DataRow = new Requirement();
  dataRow.projectId = literal.projectId;
  const project = projectsData[dataRow.projectId];
  dataRow.simplePermissions = new RequirementPermissions(project);
  Object.assign(dataRow, literal);
  return dataRow;
}
