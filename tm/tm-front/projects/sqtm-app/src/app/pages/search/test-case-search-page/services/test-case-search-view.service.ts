import {Injectable} from '@angular/core';
import {
  createStore,
  ResearchColumnPrototype,
  RestService,
  Store,
  UserHistorySearchProvider,
  UserListElement
} from 'sqtm-core';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {initialTestCaseSearchState, TestCaseSearchModel, TestCaseSearchState} from './test-case-search-model';

type userTcColumnPrototype =
  ResearchColumnPrototype.TEST_CASE_CREATED_BY
  | ResearchColumnPrototype.TEST_CASE_MODIFIED_BY;

@Injectable()
export class TestCaseSearchViewService extends UserHistorySearchProvider {

  private store: Store<TestCaseSearchState> = createStore(initialTestCaseSearchState());
  public state$: Observable<TestCaseSearchState>;

  private columnProtoToUserList: { [K in userTcColumnPrototype]: keyof TestCaseSearchState } = {
    [ResearchColumnPrototype.TEST_CASE_CREATED_BY]: 'usersWhoCreatedTestCases',
    [ResearchColumnPrototype.TEST_CASE_MODIFIED_BY]: 'usersWhoModifiedTestCases'
  };

  constructor(private restService: RestService) {
    super();
    this.state$ = this.store.state$;
  }

  loadResearchData(): Observable<any> {
    return this.restService.get<TestCaseSearchModel>(['search', 'test-case'])
      .pipe(
        tap(model => this.store.commit(model))
      );
  }

  provideUserList(columnPrototype: ResearchColumnPrototype): Observable<UserListElement[]> {
    return this.state$.pipe(
      map((state) => state[this.columnProtoToUserList[columnPrototype]])
    );
  }
}
