import {
  buildFilters,
  CampaignPermissions,
  DataRow,
  dateFilter,
  dateTimeColumn,
  executionModeColumn,
  executionStatusColumn,
  executionStatusFilter,
  Extendable,
  FilterTestCaseMilestoneLabelComponent,
  Fixed,
  i18nEnumResearchFilter,
  iconLinkColumn,
  indexColumn,
  IterationTestPlanItemRow,
  multipleListResearchFilter,
  numericResearchFilter,
  ProjectDataMap,
  ResearchColumnPrototype,
  ScopeDefinitionBuilder,
  searchGrid,
  Sort,
  SquashTmDataRow,
  StyleDefinitionBuilder,
  testCaseImportanceColumn,
  testCaseSearchAutomatable,
  textColumn,
  textResearchFilter,
  userHistoryResearchFilter
} from 'sqtm-core';
import {InjectionToken} from '@angular/core';
import {MILESTONE_FILTER} from '../../search-constants';
import {startExecutionSearchColumn} from '../components/cell-renderers/start-execution-search/start-execution-search.component';

export class ItpiSearchGridBuilders {

  buildFilters() {
    return buildFilters([
      numericResearchFilter('id', ResearchColumnPrototype.TEST_CASE_ID)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.search.generic.criteria.test-case-id'),
      textResearchFilter('reference', ResearchColumnPrototype.TEST_CASE_REFERENCE)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      textResearchFilter('name', ResearchColumnPrototype.TEST_CASE_NAME)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.name.label'),
      i18nEnumResearchFilter('automatable', ResearchColumnPrototype.TEST_CASE_AUTOMATABLE)
        .withGroupId('automation')
        .withI18nKey('sqtm-core.generic.label.automation.indicator'),
      i18nEnumResearchFilter('automationRequestStatus', ResearchColumnPrototype.AUTOMATION_REQUEST_STATUS)
        .withGroupId('automation')
        .withI18nKey('sqtm-core.generic.label.automation.status'),
      multipleListResearchFilter(MILESTONE_FILTER,
        ResearchColumnPrototype.CAMPAIGN_MILESTONE_ID,
        FilterTestCaseMilestoneLabelComponent)
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.search.generic.criteria.milestone.name'),
      i18nEnumResearchFilter('milestoneStatus', ResearchColumnPrototype.CAMPAIGN_SEARCHABLE_MILESTONE_STATUS)
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.search.generic.criteria.milestone.status'),
      dateFilter('milestoneEndDate', ResearchColumnPrototype.CAMPAIGN_MILESTONE_END_DATE)
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.entity.milestone.end-date.label'),
      i18nEnumResearchFilter('importance', ResearchColumnPrototype.TEST_CASE_IMPORTANCE)
        .withGroupId('attributes')
        .withI18nKey('sqtm-core.entity.test-case.importance.label'),
      userHistoryResearchFilter('assignee', ResearchColumnPrototype.ITERATION_TEST_PLAN_ASSIGNED_USER_LOGIN)
        .withGroupId('attributes')
        .withI18nKey('sqtm-core.search.generic.criteria.assignee-user'),
      dateFilter('executionOn', ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECON)
        .withGroupId('execution')
        .withI18nKey('sqtm-core.search.generic.criteria.executed-on'),
      userHistoryResearchFilter('executedBy', ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECBY)
        .withGroupId('execution')
        .withI18nKey('sqtm-core.search.generic.criteria.executed-by'),
      executionStatusFilter('executionStatus', ResearchColumnPrototype.ITEM_TEST_PLAN_STATUS)
        .withGroupId('execution')
        .withI18nKey('sqtm-core.search.generic.criteria.execution-status'),
      i18nEnumResearchFilter('executionMode', ResearchColumnPrototype.EXECUTION_EXECUTION_MODE)
        .withGroupId('execution')
        .withI18nKey('sqtm-core.search.generic.criteria.execution-mode')
    ]);
  }

  public buildFilterGroups() {
    return [{
      id: 'informations',
      i18nLabelKey: 'sqtm-core.generic.label.information.plural'
    }, {
      id: 'automation',
      i18nLabelKey: 'sqtm-core.generic.label.automation.label'
    }, {
      id: 'milestones',
      i18nLabelKey: 'sqtm-core.generic.label.milestone'
    }, {
      id: 'attributes',
      i18nLabelKey: 'sqtm-core.entity.generic.attributes'
    }, {
      id: 'execution',
      i18nLabelKey: 'sqtm-core.search.generic.criteria.execution'
    }];
  }

}

export const ITPI_RESEARCH_GRID_CONFIG = new InjectionToken('Token for itpi grid research config');
export const ITPI_RESEARCH_GRID = new InjectionToken('Token for itpi grid research');

export const searchItpiGridConfigFactory = () => {
  return searchGrid('itpi-search')
    .server()
    .withServerUrl(['search/campaign'])
    .withRowConverter(convertAllRowsInCampaign)
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      textColumn('projectName')
        .withI18nKey('sqtm-core.grid.header.project')
        .withColumnPrototype(ResearchColumnPrototype.CAMPAIGN_PROJECT_NAME)
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      textColumn('campaignName')
        .withColumnPrototype(ResearchColumnPrototype.CAMPAIGN_NAME)
        .withI18nKey('sqtm-core.grid.header.campaign')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      textColumn('iterationName')
        .withColumnPrototype(ResearchColumnPrototype.ITERATION_NAME)
        .withI18nKey('sqtm-core.grid.header.iteration')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      textColumn('reference')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_REFERENCE)
        .withI18nKey('sqtm-core.grid.header.reference')
        .changeWidthCalculationStrategy(new Fixed(100)),
      textColumn('label')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_NAME)
        .withI18nKey('sqtm-core.grid.header.name')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      testCaseImportanceColumn('importance')
        .isEditable(false)
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_IMPORTANCE)
        .withI18nKey('sqtm-core.search.test-case.grid.header.weight.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.weight.title')
        .changeWidthCalculationStrategy(new Fixed(40)),
      textColumn('datasetName')
        .withColumnPrototype(ResearchColumnPrototype.DATASET_NAME)
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withTitleI18nKey('sqtm-core.grid.header.dataset')
        .changeWidthCalculationStrategy(new Fixed(100)),
      textColumn('testSuites')
        .withColumnPrototype(ResearchColumnPrototype.ITEM_TEST_PLAN_SUITECOUNT)
        .withI18nKey('sqtm-core.grid.header.test-suite')
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(100)),
      testCaseSearchAutomatable('automatable')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_AUTOMATABLE)
        .withI18nKey('sqtm-core.search.test-case.grid.header.automatable.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.automatable.title')
        .changeWidthCalculationStrategy(new Fixed(100)),
      executionModeColumn('executionExecutionMode')
        .withColumnPrototype(ResearchColumnPrototype.EXECUTION_EXECUTION_MODE)
        .withI18nKey('sqtm-core.grid.header.execution-mode')
        .changeWidthCalculationStrategy(new Fixed(50)),
      executionStatusColumn('executionStatus')
        .withI18nKey('sqtm-core.grid.header.execution-status')
        .withColumnPrototype(ResearchColumnPrototype.ITEM_TEST_PLAN_STATUS)
        .changeWidthCalculationStrategy(new Fixed(50)),
      textColumn('lastExecutedBy')
        .withColumnPrototype(ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECBY)
        .withI18nKey('sqtm-core.grid.header.executed-by')
        .changeWidthCalculationStrategy(new Fixed(100)),
      dateTimeColumn('lastExecutedOn')
        .withColumnPrototype(ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECON)
        .withI18nKey('sqtm-core.grid.header.executed-on')
        .changeWidthCalculationStrategy(new Fixed(100)),
      startExecutionSearchColumn('execution').disableHeader()
        .changeWidthCalculationStrategy(new Fixed(40)),
      iconLinkColumn('folder', {
        kind: 'iconLink',
        columnParamId: 'iterationId',
        iconName: 'folder',
        baseUrl: '/campaign-workspace/iteration'
      })
    ])
    .withInitialSortedColumns([
      {id: 'projectName', sort: Sort.ASC},
      {id: 'campaignName', sort: Sort.ASC},
      {id: 'iterationName', sort: Sort.ASC},
    ])
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation())
    .withScopeDefinition(new ScopeDefinitionBuilder().withCustomScope('campaign'))
    .build();
};

export function convertAllRowsInCampaign(literals: Partial<DataRow>[], projectsData: ProjectDataMap): SquashTmDataRow[] {
  return literals.map(literal => convertOneInCampaign(literal, projectsData));
}

export function convertOneInCampaign(literal: Partial<DataRow>, projectsData: ProjectDataMap): SquashTmDataRow {
  const dataRow: DataRow = new IterationTestPlanItemRow();
  dataRow.projectId = literal.projectId;
  const project = projectsData[dataRow.projectId];
  dataRow.simplePermissions = new CampaignPermissions(project);
  Object.assign(dataRow, literal);
  return dataRow;
}
