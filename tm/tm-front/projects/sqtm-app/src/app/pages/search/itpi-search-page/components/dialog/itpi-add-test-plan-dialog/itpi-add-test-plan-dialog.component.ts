import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  CampaignPickerComponent,
  DataRow,
  DialogReference,
  ReferentialDataService,
  RestService,
  SquashTmDataRowType,
  toEntityRowReference
} from 'sqtm-core';
import {concatMap, map, take, takeUntil} from 'rxjs/operators';
import {Observable, of, Subject} from 'rxjs';
import {ItpiAddTestPlanConfiguration} from './itpi-add-test-plan.configuration';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'sqtm-app-itpi-add-test-plan-dialog',
  templateUrl: './itpi-add-test-plan-dialog.component.html',
  styleUrls: ['./itpi-add-test-plan-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class ItpiAddTestPlanDialogComponent implements OnInit, AfterViewInit, OnDestroy {

  iterationIsSelected: Observable<boolean>;

  campaignIsSelected: Observable<boolean>;

  @ViewChild('campaignPicker')
  campaignPicker: CampaignPickerComponent;

  unsub$ = new Subject<void>();

  constructor(private dialogReference: DialogReference<ItpiAddTestPlanConfiguration, boolean>,
              private restService: RestService,
              private translateService: TranslateService,
              private datePipe: DatePipe,
              private referentialData: ReferentialDataService) {
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.iterationIsSelected = this.campaignPicker.tree.selectedRows$.pipe(
      takeUntil(this.unsub$),
      map(rows => this.canEditIteration(rows))
    );

    this.campaignIsSelected = this.campaignPicker.tree.selectedRows$.pipe(
      takeUntil(this.unsub$),
      concatMap(rows => {
        if (this.canEditCampaign(rows)) {
          return this.referentialData.connectToProjectData(rows[0].projectId).pipe(
            take(1),
            map(projectData => {
              const milestoneIds: number[] = rows[0].data['MILESTONES'];
              return !projectData.milestones.filter(m => milestoneIds.includes(m.id)).map(m => m.status).includes('LOCKED');
            })
          );
        } else {
          return of(false);
        }
      })
    );
  }

  private canEditCampaign(rows: DataRow[]) {
    return rows.length === 1 && rows[0].type === SquashTmDataRowType.Campaign && rows[0].simplePermissions.canWrite;
  }

  private canEditIteration(rows: DataRow[]) {
    return rows.length === 1
      && rows[0].type === SquashTmDataRowType.Iteration
      && rows[0].simplePermissions.canWrite
      && !rows[0].data['BOUND_TO_BLOCKING_MILESTONE'];
  }

  selectedRows() {

  }

  createIteration() {
    this.campaignPicker.tree.selectedRows$.pipe(
      take(1),
      concatMap(selectedRows => {
        const entityReference = toEntityRowReference(selectedRows[0].id);
        const date = this.datePipe.transform(new Date(), 'yyyy/MM/dd HH:mm:ss');
        const name = this.translateService.instant('sqtm-core.search.campaign.dialog.add-to-test-plan.generated-name');
        const description = this.translateService.instant('sqtm-core.search.campaign.dialog.add-to-test-plan.generated-description');
        const generatedName = `${name} ${date}`;
        return this.restService.post(['campaign-tree/campaign', entityReference.id.toString(), 'new-iteration-with-items'],
          {name: generatedName, description: description, itemTestPlanIds: this.dialogReference.data.itpiIds});
      })
    ).subscribe(() => {
      this.dialogReference.result = true;
      this.dialogReference.close();
    });

  }

  addToTestPlan() {
    this.campaignPicker.tree.selectedRows$.pipe(
      take(1),
      concatMap(selectedRows => {
        const entityReference = toEntityRowReference(selectedRows[0].id);
        return this.restService.post(['iteration', entityReference.id.toString(), 'test-plan/copy-items'],
          {itemTestPlanIds: this.dialogReference.data.itpiIds});
      })
    ).subscribe(() => {
      this.dialogReference.result = true;
      this.dialogReference.close();
    });


  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
