import {Injectable} from '@angular/core';
import {
  createStore,
  ResearchColumnPrototype,
  RestService,
  Store,
  UserHistorySearchProvider,
  UserListElement
} from 'sqtm-core';
import {Observable} from 'rxjs';
import {initialItpiSearchState, ItpiSearchModel, ItpiSearchState} from './itpi-search-model';
import {map, tap} from 'rxjs/operators';

type userCampaignColumnPrototype =
  ResearchColumnPrototype.ITERATION_TEST_PLAN_ASSIGNED_USER_LOGIN
  | ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECBY;

@Injectable()
export class ItpiSearchViewService extends UserHistorySearchProvider {

  private columnProtoToUserList: { [K in userCampaignColumnPrototype]: keyof ItpiSearchState } = {
    [ResearchColumnPrototype.ITERATION_TEST_PLAN_ASSIGNED_USER_LOGIN]: 'usersAssignedTo',
    [ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECBY]: 'usersExecutedItpi',
  };

  private store: Store<ItpiSearchState> = createStore(initialItpiSearchState());
  public state$: Observable<ItpiSearchState>;

  constructor(private restService: RestService) {
    super();
    this.state$ = this.store.state$;
  }

  loadResearchData(): Observable<any> {
    return this.restService.get<ItpiSearchModel>(['search', 'campaign'])
      .pipe(
        tap(model => this.store.commit(model))
      );
  }

  provideUserList(columnPrototype: ResearchColumnPrototype): Observable<UserListElement[]> {
    return this.state$.pipe(
      map((state) => state[this.columnProtoToUserList[columnPrototype]])
    );
  }
}
