import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  DataRow,
  DialogService,
  GridFilter,
  GridService,
  gridServiceFactory,
  Identifier,
  Milestone,
  MilestoneView,
  ReferentialDataService,
  RequirementCurrentVersionFilterKeys,
  RequirementPermissions,
  RequirementStatus,
  ResearchColumnPrototype,
  RestService,
  UserHistorySearchProvider
} from 'sqtm-core';
import {RequirementSearchViewService} from '../../services/requirement-search-view.service';
import {
  AbstractRequirementSearchComponent,
  REQUIREMENT_RESEARCH_GRID,
  REQUIREMENT_RESEARCH_GRID_CONFIG
} from '../abstract-requirement-search.component';
import {searchRequirementGridConfigFactory} from '../requirement-search-grid.builders';
import {concatMap, filter, map, take} from 'rxjs/operators';
import {combineLatest} from 'rxjs';
import {RequirementMultiEditDialogComponent} from '../../components/dialog/requirement-multi-edit-dialog/requirement-multi-edit-dialog.component';
import {RequirementMultiEditConfiguration} from '../../components/dialog/requirement-multi-edit-dialog/requirement-multi-edit.configuration';
import {RequirementSearchExportDialogConfiguration} from '../../components/dialog/requirement-export-dialog/requirement-search-export-dialog.configuration';
import {RequirementSearchExportDialogComponent} from '../../components/dialog/requirement-export-dialog/requirement-search-export-dialog.component';
import {SearchViewComponent} from '../../../search-view/containers/search-view/search-view.component';
import {MilestoneDialogConfiguration} from '../../../../../components/milestone/milestone-dialog/milestone.dialog.configuration';
import {MilestoneDialogComponent} from '../../../../../components/milestone/milestone-dialog/milestone-dialog.component';


/**
 * Main requirement search page.
 */
@Component({
  selector: 'sqtm-app-requirement-search-page',
  templateUrl: './requirement-search-page.component.html',
  styleUrls: ['./requirement-search-page.component.less'],
  providers: [
    {
      provide: REQUIREMENT_RESEARCH_GRID_CONFIG,
      useFactory: searchRequirementGridConfigFactory
    },
    {
      provide: REQUIREMENT_RESEARCH_GRID,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        REQUIREMENT_RESEARCH_GRID_CONFIG,
        ReferentialDataService
      ]
    }, {
      provide: GridService,
      useExisting: REQUIREMENT_RESEARCH_GRID
    },
    {
      provide: RequirementSearchViewService,
      useClass: RequirementSearchViewService,
      deps: [RestService]
    },
    {
      provide: UserHistorySearchProvider,
      useExisting: RequirementSearchViewService,
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementSearchPageComponent extends AbstractRequirementSearchComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild(SearchViewComponent)
  public searchViewComponent: SearchViewComponent;

  constructor(referentialDataService: ReferentialDataService,
              requirementSearchViewService: RequirementSearchViewService,
              gridService: GridService,
              private dialogService: DialogService,
              private  vcr: ViewContainerRef,
              private restService: RestService) {
    super(referentialDataService, requirementSearchViewService, gridService);
  }

  showExportDialog() {
    combineLatest([this.gridService.dataRows$, this.gridService.selectedRows$, this.gridService.filters$])
      .pipe(
        take(1),
        map(([rows, selectedRows, filters]) => this.getExportConfig(rows, selectedRows, filters))
      ).subscribe(exportedRowIds => {
      this.dialogService.openDialog<RequirementSearchExportDialogConfiguration, any>({
        id: 'requirement-export-dialog',
        component: RequirementSearchExportDialogComponent,
        viewContainerReference: this.vcr,
        data: {
          id: 'requirement-export-dialog',
          titleKey: 'sqtm-core.generic.label.export',
          requirementVersionIds: exportedRowIds,
        },
        width: 600
      });
    });
  }

  private getExportConfig(rows, selectedRows, filters) {
    return this.getExportedRequirementIds(rows, selectedRows);
  }

  massEdit() {
    this.gridService.selectedRows$.pipe(
      take(1),
    ).subscribe(dataRows => {
      const editableRows: DataRow[] = this.filterNonEditableRows(dataRows);
      if (editableRows.length > 0) {
        const writingRight = editableRows.length === dataRows.length;
        const projectIds = editableRows.map(dataRow => dataRow['projectId']);
        const canOnlyEditStatus = this.isApprovedOrObsolete(editableRows);
        this.showMassEditDialog(editableRows, projectIds, writingRight, canOnlyEditStatus);
      } else {
        this.showCantEditDialog('sqtm-core.search.generic.modify.no-line-with-writing-rights');
      }
    });
  }

  massBindMilestone() {
    this.gridService.selectedRows$.pipe(
      take(1),
      map((rows: DataRow[]) => rows.filter(row => {
        const reqPerm = row.simplePermissions as RequirementPermissions;
        return reqPerm.canLink;
      }))
    ).subscribe(rows => {
      if (rows.length > 0) {
        this.getBindableMilestones(rows);
      } else {
        this.showCantEditDialog('sqtm-core.search.generic.modify.no-permissions-on-any-line');
      }
    });
  }

  private getBindableMilestones(rows: DataRow[]) {
    const versionsShareRequirement = this.checkSharedRequirement(rows);
    const requirementVersionIds = rows.map(row => row.id);
    if (requirementVersionIds.length > 0) {
      this.restService.get<MilestoneMassEdit>(['search/milestones/requirement', requirementVersionIds.toString()]).pipe(
        map((massEditMilestone: MilestoneMassEdit) => {
          massEditMilestone.requirementVersionIds = requirementVersionIds;
          return massEditMilestone;
        }),
        concatMap((massBindingMilestone: MilestoneMassEdit) => {
          const milestoneIds = massBindingMilestone.milestoneIds;
          return this.referentialDataService.findMilestones(milestoneIds).pipe(
            take(1),
            map((milestones: Milestone[]) => {
              const milestoneMassBindingView: MilestoneMassBindingView = {
                ...massBindingMilestone,
                milestoneViews: this.convertToMilestoneViews(milestones, massBindingMilestone)
              };
              return milestoneMassBindingView;
            }));
        })).subscribe((bindableMilestoneView: MilestoneMassBindingView) => {
        if (bindableMilestoneView.milestoneViews.length < 1) {
          this.showCantEditDialog('sqtm-core.search.generic.modify.milestone.wrong-perimeter.requirement');
        } else if (versionsShareRequirement) {
          this.showCantEditDialog('sqtm-core.search.generic.modify.milestone.same-requirement');
        } else {
          this.showMassBindingMilestoneDialog(bindableMilestoneView);
        }
      });
    } else {
      this.showCantEditDialog('sqtm-core.search.generic.modify.milestone.req-version-already-bound');
    }
  }

  private showMassBindingMilestoneDialog(milestoneView: MilestoneMassBindingView) {
    const configuration: MilestoneDialogConfiguration = {
      id: 'search-milestone-dialog',
      titleKey: 'sqtm-core.search.generic.modify.milestone.title',
      checkedIds: milestoneView.checkedIds,
      milestoneViews: milestoneView.milestoneViews,
      samePerimeter: milestoneView.samePerimeter
    };

    const milestoneRef = this.dialogService.openDialog({
      id: 'research-milestone',
      component: MilestoneDialogComponent,
      data: {...configuration},
      height: 400,
      width: 800
    });

    milestoneRef.dialogClosed$.pipe(
      take(1),
      filter(result => result != null),
      concatMap((result: number[]) => {
        const data = {};
        data['milestoneIds'] = result;
        data['bindableObjectIds'] = milestoneView.requirementVersionIds;
        return this.restService.post(['search/milestones/requirement'], data);
      })
    ).subscribe((isOneReqVersionAlreadyBound: boolean) => {
      if (isOneReqVersionAlreadyBound) {
        this.showCantEditDialog('sqtm-core.search.generic.modify.milestone.req-version-already-bound');
      }
      this.gridService.refreshData();
    });
  }

  private filterNonEditableRows(dataRows: DataRow[]) {
    return dataRows.filter(row => row.simplePermissions.canWrite && row.data['reqMilestoneLocked'] !== 1);
  }


  private showMassEditDialog(editableRows: DataRow[], projectIds: number[], writingRight: boolean, canOnlyEditStatus: boolean) {
    // @ts-ignore
    const dialogReference = this.dialogService.openDialog<RequirementMultiEditConfiguration, boolean>({
      id: 'mass-edit',
      component: RequirementMultiEditDialogComponent,
      data: {
        id: 'mass-edit',
        titleKey: 'sqtm-core.search.generic.modify.selection',
        editableRowsIds: this.getRequirementVersionsIds(editableRows),
        projectIds: projectIds,
        writingRightOnLine: writingRight,
        canOnlyEditStatus: canOnlyEditStatus
      },
      width: 600
    });
    dialogReference.dialogClosed$.pipe(take(1)).subscribe(result => {
      if (result) {
        this.searchViewComponent.gridService.refreshData();
      }
    });
  }

  private showCantEditDialog(messageKey: string) {
    this.dialogService.openAlert({
      titleKey: 'sqtm-core.generic.label.information.singular',
      messageKey,
      level: 'INFO',
    });
  }

  private getExportedRequirementIds(rows, selectedRows) {
    if (this.hasSelection(selectedRows)) {
      return this.getRequirementVersionsIds(selectedRows);
    } else {
      const dataRows = Object.values(rows);
      return this.getRequirementVersionsIds(dataRows);
    }
  }

  private hasSelection(selectedRows) {
    return selectedRows.length > 0;
  }

  private getRequirementIds(selectedRows) {
    return selectedRows.map(dataRow => Number.parseInt(dataRow.data.requirementId.toString(), 10));
  }

  private getRequirementVersionsIds(selectedRows) {
    return selectedRows.map(dataRow => Number.parseInt(dataRow.data.id.toString(), 10));
  }

  private getRequirementVersionFilter(filters) {
    return filters.find(f => f.columnPrototype === ResearchColumnPrototype.REQUIREMENT_VERSION_CURRENT_VERSION);
  }

  private getRequirementVersionFilterKey(filters: GridFilter[]): RequirementCurrentVersionFilterKeys {
    const requirementVersionFilter = this.getRequirementVersionFilter(filters);
    return this.getRequirementVersionValue(requirementVersionFilter);
  }

  private getRequirementVersionValue(filtered) {
    return filtered.value.value[0].id;
  }

  private isApprovedOrObsolete(editableRows: DataRow[]) {
    for (const row of editableRows) {
      if (row.data['status'] === RequirementStatus.APPROVED.id || row.data['status'] === RequirementStatus.OBSOLETE.id) {
        return true;
      }
    }
    return false;
  }

  private convertToMilestoneViews(milestones: Milestone[], massEdit: MilestoneMassEdit): MilestoneView[] {
    const milestonesViews: MilestoneView[] = [];

    milestones.forEach(milestone => {
      const mv: MilestoneView = {...milestone, boundToObject: massEdit.checkedIds.includes(milestone.id)};
      milestonesViews.push(mv);
    });
    return milestonesViews;
  }

  private checkSharedRequirement(rows: DataRow[]): boolean {
    const requirements = [...new Set(rows.map(row => row.data.requirementId))];
    const versions = rows.map(row => row.id);
    return requirements.length !== versions.length;
  }

}

export interface MilestoneMassEdit {
  milestoneIds: number[];
  checkedIds: number[];
  samePerimeter: boolean;
  requirementVersionIds: Identifier[];
}

export interface MilestoneMassBindingView {
  checkedIds: number[];
  samePerimeter: boolean;
  milestoneViews: MilestoneView[];
  requirementVersionIds: Identifier[];
}
