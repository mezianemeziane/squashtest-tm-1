import {DataRow} from 'sqtm-core';

export class RequirementMultiEditConfiguration {
  id: string;
  titleKey: string;
  editableRowsIds: number[];
  projectIds: number[];
  writingRightOnLine: boolean;
  canOnlyEditStatus: boolean;
}
