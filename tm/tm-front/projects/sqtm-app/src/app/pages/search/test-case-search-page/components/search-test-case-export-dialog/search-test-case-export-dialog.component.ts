import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';
import {SearchTestCaseExportDialogConfiguration} from './search-test-case-export-dialog-configuration';
import {
  CORE_MODULE_CONFIGURATION,
  DialogReference,
  DisplayOption,
  RestService,
  SqtmCoreModuleConfiguration
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-search-test-case-export-dialog',
  templateUrl: './search-test-case-export-dialog.component.html',
  styleUrls: ['./search-test-case-export-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchTestCaseExportDialogComponent implements OnInit {

  data: SearchTestCaseExportDialogConfiguration;

  formGroup: FormGroup;

  constructor(public dialogReference: DialogReference<SearchTestCaseExportDialogConfiguration>,
              private translateService: TranslateService,
              private fb: FormBuilder,
              private datePipe: DatePipe,
              private restService: RestService,
              @Inject(CORE_MODULE_CONFIGURATION) private conf: SqtmCoreModuleConfiguration) {
    this.data = this.dialogReference.data;
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group(
      {
        type: this.fb.control('simple', [Validators.required]),
        fileName: this.fb.control(this.initFileName(), [Validators.required]),
        addCalledTC: this.fb.control(false, []),
        editableRichText: this.fb.control(true, [])
      }
    );
  }

  getType(): DisplayOption[] {
    return [{
      id: 'simple',
      label: this.translateService.instant('sqtm-core.test-case-workspace.dialog.field.simple-export.label')
    }, {
      id: 'full',
      label: this.translateService.instant('sqtm-core.test-case-workspace.dialog.field.full-export.label')
    }];
  }

  buildExportUrl() {

    const params = {
      nodes: this.data.nodes.toString(),
      filename: this.formGroup.controls.fileName.value,
      calls: this.formGroup.controls.addCalledTC.value,
      'keep-rte-format': this.formGroup.controls.editableRichText.value,
      type: this.formGroup.controls.type.value
    };

    return this.restService.buildExportUrlWithParams('test-case/export/searchExports', params);
  }

  initFileName() {
    const date = new Date();
    const newDate = this.datePipe.transform(date, 'yyyyMMdd_HHmmss');
    return `${this.translateService.instant('sqtm-core.test-case-workspace.dialog.export.file-name-value')}_${newDate}`;
  }

  getFileName() {
    return this.formGroup.controls.fileName.value + '.xls';
  }

  close() {
    this.dialogReference.close();
  }
}
