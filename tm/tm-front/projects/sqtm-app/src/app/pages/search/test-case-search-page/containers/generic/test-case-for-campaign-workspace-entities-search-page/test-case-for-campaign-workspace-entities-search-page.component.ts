import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {
  BindableEntity,
  Extendable,
  Fixed,
  GridService,
  gridServiceFactory,
  iconLinkColumn,
  indexColumn,
  infoListColumn,
  isTestCaseEditable,
  numericColumn,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  ScopeDefinitionBuilder,
  searchGrid,
  Sort,
  StyleDefinitionBuilder,
  testCaseImportanceColumn,
  testCaseSearchAutomatable,
  testCaseStatusColumn,
  textColumn
} from 'sqtm-core';
import {
  convertAllRowsInTestCase,
  TEST_CASE_RESEARCH_GRID,
  TEST_CASE_RESEARCH_GRID_CONFIG
} from '../../test-case-search-grid.builders';
import {TestCaseSearchViewService} from '../../../services/test-case-search-view.service';
import {AbstractTestCaseForCampaignWorkspaceEntitiesSearchPageComponent} from '../../abstract-test-case-for-campaign-workspace-entities-search-page.component';
import {ActivatedRoute, Router} from '@angular/router';
import {testCaseBoundToItemColumn} from '../../../components/cell-renderers/test-case-bound-to-item-checkbox-renderer/test-case-bound-to-item-renderer.component';
import {testCaseSearchEditableText} from '../../../components/cell-renderers/test-case-editable-text-renderer/test-case-editable-text-renderer.component';

export const searchTestCaseGridConfigFactory = () => {
  return searchGrid('test-case-search')
    .server()
    .withServerUrl(['search/test-case'])
    .withModificationUrl(['test-case'])
    .withRowConverter(convertAllRowsInTestCase)
    .withFilterCufFrom(BindableEntity.TEST_CASE)
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      testCaseBoundToItemColumn('testCaseBoundToItem'),
      textColumn('projectName')
        .withI18nKey('sqtm-core.grid.header.project')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_PROJECT_NAME)
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      textColumn('id')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_ID)
        .withI18nKey('sqtm-core.grid.header.id')
        .changeWidthCalculationStrategy(new Fixed(60)),
      testCaseSearchEditableText('reference')
        .isEditable(isTestCaseEditable)
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_REFERENCE)
        .withI18nKey('sqtm-core.grid.header.reference')
        .changeWidthCalculationStrategy(new Fixed(100)),
      testCaseSearchEditableText('name')
        .isEditable(isTestCaseEditable)
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_NAME)
        .withI18nKey('sqtm-core.grid.header.name')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      testCaseStatusColumn('status')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_STATUS)
        .withI18nKey('sqtm-core.search.test-case.grid.header.status.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.status.title')
        .changeWidthCalculationStrategy(new Fixed(40)),
      testCaseImportanceColumn('importance')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_IMPORTANCE)
        .withI18nKey('sqtm-core.search.test-case.grid.header.weight.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.weight.title')
        .changeWidthCalculationStrategy(new Fixed(40)),
      infoListColumn('nature', {kind: 'infoList', infolist: 'testCaseNature'})
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_NATURE)
        .withI18nKey('sqtm-core.search.test-case.grid.header.nature')
        .changeWidthCalculationStrategy(new Fixed(100))
        .isEditable(isTestCaseEditable),
      infoListColumn('type', {kind: 'infoList', infolist: 'testCaseType'})
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_TYPE)
        .withI18nKey('sqtm-core.search.test-case.grid.header.type')
        .changeWidthCalculationStrategy(new Fixed(100))
        .isEditable(isTestCaseEditable),
      testCaseSearchAutomatable('automatable')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_AUTOMATABLE)
        .isEditable(true)
        .withI18nKey('sqtm-core.search.test-case.grid.header.automatable.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.automatable.title')
        .changeWidthCalculationStrategy(new Fixed(100)),
      textColumn('createdBy')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_CREATED_BY)
        .withI18nKey('sqtm-core.grid.header.created-by.masculine')
        .changeWidthCalculationStrategy(new Fixed(100)),
      textColumn('lastModifiedBy')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_MODIFIED_BY)
        .withI18nKey('sqtm-core.grid.header.modified-by.masculine')
        .changeWidthCalculationStrategy(new Fixed(100)),
      numericColumn('milestones')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_MILCOUNT)
        .withI18nKey('sqtm-core.search.test-case.grid.header.milestones.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.milestones.title')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn('attachments')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_ATTCOUNT)
        .withI18nKey('sqtm-core.search.test-case.grid.header.attachments.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.attachments.title')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn('coverages')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_VERSCOUNT)
        .withI18nKey('sqtm-core.search.test-case.grid.header.coverages.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.coverages.title')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn('steps')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_STEPCOUNT)
        .withI18nKey('sqtm-core.search.test-case.grid.header.steps.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.steps.title')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn('iterations')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_ITERCOUNT)
        .withI18nKey('sqtm-core.search.test-case.grid.header.items.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.items.title')
        .changeWidthCalculationStrategy(new Fixed(60)),
      iconLinkColumn('detail', {
        kind: 'iconLink',
        columnParamId: 'id',
        iconName: 'sqtm-core-generic:edit',
        baseUrl: '/test-case-workspace/test-case/detail'
      }),
      iconLinkColumn('folder', {
        kind: 'iconLink',
        columnParamId: 'id',
        iconName: 'folder',
        baseUrl: '/test-case-workspace/test-case'
      })
    ])
    .withInitialSortedColumns([
      {id: 'projectName', sort: Sort.ASC},
      {id: 'reference', sort: Sort.ASC},
      {id: 'name', sort: Sort.ASC},
    ])
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation())
    .withScopeDefinition(new ScopeDefinitionBuilder().withCustomScope('test-case'))
    .build();
};

@Component({
  selector: 'sqtm-app-test-case-for-campaign-workspace-entities-search-page',
  templateUrl: './test-case-for-campaign-workspace-entities-search-page.component.html',
  styleUrls: ['./test-case-for-campaign-workspace-entities-search-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TEST_CASE_RESEARCH_GRID_CONFIG,
      useFactory: searchTestCaseGridConfigFactory
    }, {
      provide: TEST_CASE_RESEARCH_GRID,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        TEST_CASE_RESEARCH_GRID_CONFIG,
        ReferentialDataService
      ]
    }, {
      provide: GridService,
      useExisting: TEST_CASE_RESEARCH_GRID
    }, {
      provide: TestCaseSearchViewService,
      useClass: TestCaseSearchViewService,
      deps: [RestService]
    }
  ]
})
// tslint:disable-next-line:max-line-length
export class TestCaseForCampaignWorkspaceEntitiesSearchPageComponent extends AbstractTestCaseForCampaignWorkspaceEntitiesSearchPageComponent {

  @Input()
  workspaceName: string;

  @Input()
  titleKey: string;

  @Input()
  containerType: string;

  @Input()
  url: string;

  constructor(referentialDataService: ReferentialDataService,
              testCaseSearchViewService: TestCaseSearchViewService,
              gridService: GridService,
              activatedRoute: ActivatedRoute,
              router: Router,
              private restService: RestService) {
    super(referentialDataService, testCaseSearchViewService, gridService, activatedRoute, router);
  }

  protected persistTestCasesInEntity(testCaseIds: number[]) {
    const containerId = this.activatedRoute.snapshot.paramMap.get(this.containerType);
    const url = `${(this.url)}/${Number.parseInt(containerId, 10)}/test-plan-items`;
    return this.restService.post<void>([url], {testCaseIds});
  }

}
