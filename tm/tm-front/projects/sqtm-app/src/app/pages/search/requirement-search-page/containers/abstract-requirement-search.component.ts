import {AfterViewInit, Directive, InjectionToken, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RequirementSearchGridBuilders} from './requirement-search-grid.builders';
import {FilterGroup, GridFilter, GridService, ReferentialDataService} from 'sqtm-core';
import {RequirementSearchViewService} from '../services/requirement-search-view.service';
import {SearchViewComponent} from '../../search-view/containers/search-view/search-view.component';
import {Observable, Subject} from 'rxjs';
import {concatMap, pluck, takeUntil} from 'rxjs/operators';

export const REQUIREMENT_RESEARCH_GRID_CONFIG = new InjectionToken('Token for requirement grid research config');
export const REQUIREMENT_RESEARCH_GRID = new InjectionToken('Token for requirement grid research');


@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractRequirementSearchComponent implements OnInit, OnDestroy, AfterViewInit {
  filters: GridFilter[];

  filterGroups: FilterGroup[];

  @ViewChild(SearchViewComponent)
  protected searchViewComponent: SearchViewComponent;

  protected unsub$ = new Subject<void>();

  hasSelectedRows$: Observable<boolean>;

  protected constructor(protected referentialDataService: ReferentialDataService,
                        protected requirementSearchViewService: RequirementSearchViewService,
                        protected gridService: GridService) {
    this.observeMilestoneFeatureEnabled();
  }

  private observeMilestoneFeatureEnabled(): void {
    this.referentialDataService.globalConfiguration$.pipe(
      takeUntil(this.unsub$),
      pluck('milestoneFeatureEnabled')
    ).subscribe((milestoneFeatureEnabled) => {
      this.gridService.setColumnVisibility('milestones', milestoneFeatureEnabled);
      this.buildFilters(milestoneFeatureEnabled);
    });
  }

  private buildFilters(milestoneFeatureEnabled: boolean): void {
    const builders = new RequirementSearchGridBuilders();
    this.filters = builders.buildFilters();
    this.filterGroups = builders.buildFilterGroups();

    if (! milestoneFeatureEnabled) {
      this.filterGroups = this.filterGroups.filter(group => group.id !== 'milestones');
    }
  }

  ngOnInit(): void {
    this.hasSelectedRows$ = this.gridService.hasSelectedRows$.pipe(
      takeUntil(this.unsub$)
    );
  }

  ngAfterViewInit(): void {
    this.referentialDataService.refresh().pipe(
      concatMap(() => this.requirementSearchViewService.loadResearchData())
    ).subscribe(() => this.searchViewComponent.initializeResearch());
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

}
