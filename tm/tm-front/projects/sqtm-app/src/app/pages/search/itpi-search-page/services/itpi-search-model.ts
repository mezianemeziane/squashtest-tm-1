import {UserView} from 'sqtm-core';

export class ItpiSearchModel {
  usersAssignedTo: UserView[];
  usersExecutedItpi: UserView[];
}

export type ItpiSearchState = ItpiSearchModel;

export function initialItpiSearchState(): Readonly<ItpiSearchState> {
  return {usersAssignedTo: [], usersExecutedItpi: []};
}
