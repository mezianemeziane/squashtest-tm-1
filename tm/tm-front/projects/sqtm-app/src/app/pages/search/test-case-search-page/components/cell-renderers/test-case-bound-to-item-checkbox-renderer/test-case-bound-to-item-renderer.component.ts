import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, Fixed, GridService} from 'sqtm-core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'sqtm-app-test-case-bound-to-item-renderer',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column" *ngIf="isTestCaseAlreadyBound()">
        <i class="current-workspace-main-color vertical-center" style="margin:auto" nz-icon
           [nzType]="'sqtm-core-generic:confirm'"
           [nzTheme]="'fill'"></i>
      </div>
    </ng-container>
  `,
  styleUrls: ['./test-case-bound-to-item-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseBoundToItemRendererComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public grid: GridService,
              private cdr: ChangeDetectorRef,
              private activatedRoute: ActivatedRoute) {
    super(grid, cdr);
  }

  ngOnInit(): void {
  }

  isTestCaseAlreadyBound() {
    const campaignId = this.activatedRoute.snapshot.paramMap.get('campaignId');
    const campaignIds: string[] = this.row.data['campaignList'];

    if (campaignIds != null && campaignId != null) {
      return campaignIds.includes(campaignId);
    }

    const iterationId = this.activatedRoute.snapshot.paramMap.get('iterationId');
    const iterationIds: string[] = this.row.data['iterationList'];

    if (iterationIds != null && iterationId != null) {
      return iterationIds.includes(iterationId);
    }

    const testSuiteId = this.activatedRoute.snapshot.paramMap.get('testSuiteId');
    const testSuiteIds: string[] = this.row.data['testSuiteList'];

    if (testSuiteIds != null && testSuiteId != null) {
      return testSuiteIds.includes(testSuiteId);
    }
  }
}

export function testCaseBoundToItemColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(TestCaseBoundToItemRendererComponent)
    .disableHeader()
    .changeWidthCalculationStrategy(new Fixed(40));
}

