import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TestCaseBoundToItemRendererComponent } from './test-case-bound-to-item-renderer.component';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {GridTestingModule} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {of} from 'rxjs';

describe('TestCaseBoundToItemRendererComponent', () => {
  let component: TestCaseBoundToItemRendererComponent;
  let fixture: ComponentFixture<TestCaseBoundToItemRendererComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, GridTestingModule],
      declarations: [ TestCaseBoundToItemRendererComponent ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({id: 123}),
            paramMap: of(convertToParamMap({
              toto: 'tutu',
            })),
          }
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseBoundToItemRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
