import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'sqtm-app-test-case-for-test-suite-search-page',
  template: `
    <sqtm-app-test-case-for-campaign-workspace-entities-search-page [workspaceName]="'test-case-workspace'"
                                                                    [titleKey]="'sqtm-core.search.test-case.title'"
                                                                    [containerType]="'testSuiteId'"
                                                                    [url]="'test-suite'">
    </sqtm-app-test-case-for-campaign-workspace-entities-search-page>
  `,
  styleUrls: ['./test-case-for-test-suite-search-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseForTestSuiteSearchPageComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
