import {AfterViewInit, Directive, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FilterGroup, GridFilter, GridService, ReferentialDataService} from 'sqtm-core';
import {SearchViewComponent} from '../../search-view/containers/search-view/search-view.component';
import {Observable, Subject} from 'rxjs';
import {concatMap, pluck, takeUntil, tap} from 'rxjs/operators';
import {ItpiSearchGridBuilders} from './itpi-search-grid.builders';
import {ItpiSearchViewService} from '../services/itpi-search-view.service';


@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractItpiSearchComponent implements OnInit, OnDestroy, AfterViewInit {
  filters: GridFilter[];

  filterGroups: FilterGroup[];

  @ViewChild(SearchViewComponent)
  protected searchViewComponent: SearchViewComponent;

  protected unsub$ = new Subject<void>();

  hasSelectedRows$: Observable<boolean>;

  protected constructor(protected referentialDataService: ReferentialDataService,
                        protected itpiSearchViewService: ItpiSearchViewService,
                        protected gridService: GridService) {
    this.observeMilestoneFeatureEnabled();
  }

  private observeMilestoneFeatureEnabled(): void {
    this.referentialDataService.globalConfiguration$.pipe(
      takeUntil(this.unsub$),
      pluck('milestoneFeatureEnabled')
    ).subscribe((milestoneFeatureEnabled) => {
      this.buildFilters(milestoneFeatureEnabled);
    });
  }

  private buildFilters(milestoneFeatureEnabled: boolean): void {
    const builders = new ItpiSearchGridBuilders();
    this.filters = builders.buildFilters();
    this.filterGroups = builders.buildFilterGroups();

    if (!milestoneFeatureEnabled) {
      this.filterGroups = this.filterGroups.filter(group => group.id !== 'milestones');
    }
  }

  ngOnInit(): void {
    this.hasSelectedRows$ = this.gridService.hasSelectedRows$.pipe(
      takeUntil(this.unsub$)
    );
  }

  ngAfterViewInit(): void {
    this.referentialDataService.refresh().pipe(
      concatMap(() => this.itpiSearchViewService.loadResearchData())
    ).subscribe(() => this.searchViewComponent.initializeResearch());
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

}
