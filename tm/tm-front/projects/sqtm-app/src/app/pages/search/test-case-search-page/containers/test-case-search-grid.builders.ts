import {
  BindableEntity,
  buildFilters,
  DataRow,
  dateFilter,
  Extendable,
  FilterGroup,
  FilterTestCaseMilestoneLabelComponent,
  Fixed,
  fullTextResearchFilter,
  GridFilter,
  i18nEnumResearchFilter,
  iconLinkColumn,
  indexColumn,
  infoListColumn,
  infoListResearchFilter,
  isTestCaseEditable,
  multipleListResearchFilter,
  numericColumn,
  numericResearchFilter,
  ProjectDataMap,
  ResearchColumnPrototype,
  ScopeDefinitionBuilder,
  searchGrid,
  Sort,
  SquashTmDataRow,
  StyleDefinitionBuilder,
  TestCase,
  testCaseImportanceColumn,
  TestCasePermissions,
  testCaseSearchAutomatable,
  testCaseStatusColumn,
  textColumn,
  textResearchFilter,
  userHistoryResearchFilter
} from 'sqtm-core';
import {MILESTONE_FILTER} from '../../search-constants';
import {InjectionToken} from '@angular/core';
// tslint:disable-next-line:max-line-length
import {testCaseSearchEditableText} from '../components/cell-renderers/test-case-editable-text-renderer/test-case-editable-text-renderer.component';

export class TestCaseSearchGridBuilders {

  buildFilters(): GridFilter[] {
    return buildFilters([
      numericResearchFilter('id', ResearchColumnPrototype.TEST_CASE_ID)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.id.capitalize'),
      textResearchFilter('reference', ResearchColumnPrototype.TEST_CASE_REFERENCE)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      textResearchFilter('name', ResearchColumnPrototype.TEST_CASE_NAME)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.name.label'),
      fullTextResearchFilter('description', ResearchColumnPrototype.TEST_CASE_DESCRIPTION)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.generic.description.label'),
      fullTextResearchFilter('prerequisite', ResearchColumnPrototype.TEST_CASE_PREQUISITE)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.test-case.prerequisites.label'),
      i18nEnumResearchFilter('kind', ResearchColumnPrototype.TEST_CASE_KIND)
        .withGroupId('informations')
        .withI18nKey('sqtm-core.entity.test-case.kind.label'),
      dateFilter('createdOn', ResearchColumnPrototype.TEST_CASE_CREATED_ON)
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.created-on.masculine'),
      userHistoryResearchFilter('createdBy', ResearchColumnPrototype.TEST_CASE_CREATED_BY)
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.created-by.masculine'),
      dateFilter('modifiedOn', ResearchColumnPrototype.TEST_CASE_MODIFIED_ON)
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.last-modified-on.masculine'),
      userHistoryResearchFilter('modifiedBy', ResearchColumnPrototype.TEST_CASE_MODIFIED_BY)
        .withGroupId('historical')
        .withI18nKey('sqtm-core.entity.generic.last-modified-by.masculine'),
      i18nEnumResearchFilter('automatable', ResearchColumnPrototype.TEST_CASE_AUTOMATABLE)
        .withGroupId('automation')
        .withI18nKey('sqtm-core.generic.label.automation.indicator'),
      i18nEnumResearchFilter('automationRequestStatus', ResearchColumnPrototype.AUTOMATION_REQUEST_STATUS)
        .withGroupId('automation')
        .withI18nKey('sqtm-core.generic.label.automation.status'),
      multipleListResearchFilter(MILESTONE_FILTER,
        ResearchColumnPrototype.TEST_CASE_MILESTONE_ID,
        FilterTestCaseMilestoneLabelComponent)
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.search.generic.criteria.milestone.name'),
      i18nEnumResearchFilter('milestoneStatus', ResearchColumnPrototype.TEST_CASE_SEARCHABLE_MILESTONE_STATUS)
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.search.generic.criteria.milestone.status'),
      dateFilter('milestoneEndDate', ResearchColumnPrototype.TEST_CASE_MILESTONE_END_DATE)
        .withGroupId('milestones')
        .withI18nKey('sqtm-core.entity.milestone.end-date.label'),
      i18nEnumResearchFilter('status', ResearchColumnPrototype.TEST_CASE_STATUS)
        .withGroupId('attributes')
        .withI18nKey('sqtm-core.entity.generic.status.label'),
      i18nEnumResearchFilter('importance', ResearchColumnPrototype.TEST_CASE_IMPORTANCE)
        .withGroupId('attributes')
        .withI18nKey('sqtm-core.entity.test-case.importance.label'),
      infoListResearchFilter('nature', ResearchColumnPrototype.TEST_CASE_NATURE_ID)
        .withI18nKey('sqtm-core.entity.test-case.nature.label'),
      infoListResearchFilter('type', ResearchColumnPrototype.TEST_CASE_TYPE_ID)
        .withI18nKey('sqtm-core.entity.test-case.type.label'),
      numericResearchFilter('stepCount', ResearchColumnPrototype.TEST_CASE_STEPCOUNT)
        .withGroupId('content')
        .withI18nKey('sqtm-core.search.test-case.criteria.step-count'),
      numericResearchFilter('paramCount', ResearchColumnPrototype.TEST_CASE_PARAMCOUNT)
        .withGroupId('content')
        .withI18nKey('sqtm-core.search.test-case.criteria.param-count'),
      numericResearchFilter('datasetCount', ResearchColumnPrototype.TEST_CASE_DATASETCOUNT)
        .withGroupId('content')
        .withI18nKey('sqtm-core.search.test-case.criteria.dataset-count'),
      numericResearchFilter('callStepCount', ResearchColumnPrototype.TEST_CASE_CALLSTEPCOUNT)
        .withGroupId('content')
        .withI18nKey('sqtm-core.search.test-case.criteria.call-step-count'),
      numericResearchFilter('attachmentCount', ResearchColumnPrototype.TEST_CASE_ATTCOUNT)
        .withGroupId('content')
        .withI18nKey('sqtm-core.search.generic.criteria.attachment-count'),
      numericResearchFilter('coveragesCount', ResearchColumnPrototype.TEST_CASE_VERSCOUNT)
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.generic.criteria.coverage-count'),
      numericResearchFilter('iterationsCount', ResearchColumnPrototype.TEST_CASE_ITERCOUNT)
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.test-case.criteria.iteration-count'),
      numericResearchFilter('executionsCount', ResearchColumnPrototype.TEST_CASE_EXECOUNT)
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.test-case.criteria.exec-count'),
      numericResearchFilter('issuesCount', ResearchColumnPrototype.EXECUTION_ISSUECOUNT)
        .withGroupId('associations')
        .withI18nKey('sqtm-core.search.generic.criteria.issue-count')
    ]);
  }

  public buildFilterGroups(): FilterGroup[] {
    return [{
      id: 'informations',
      i18nLabelKey: 'sqtm-core.generic.label.information.plural'
    }, {
      id: 'historical',
      i18nLabelKey: 'sqtm-core.search.generic.criteria.groups.historical'
    }, {
      id: 'automation',
      i18nLabelKey: 'sqtm-core.generic.label.automation.label'
    }, {
      id: 'milestones',
      i18nLabelKey: 'sqtm-core.generic.label.milestone'
    }, {
      id: 'attributes',
      i18nLabelKey: 'sqtm-core.entity.generic.attributes'
    }, {
      id: 'content',
      i18nLabelKey: 'sqtm-core.search.generic.criteria.groups.content'
    }, {
      id: 'associations',
      i18nLabelKey: 'sqtm-core.search.generic.criteria.groups.associations'
    }, {
      id: 'custom-fields',
      i18nLabelKey: 'sqtm-core.entity.custom-field.label.plural'
    }];
  }

}

export const TEST_CASE_RESEARCH_GRID_CONFIG = new InjectionToken('Token for test case grid research config');
export const TEST_CASE_RESEARCH_GRID = new InjectionToken('Token for test case grid research');

export const searchTestCaseGridConfigFactory = () => {
  return searchGrid('test-case-search')
    .server()
    .withServerUrl(['search/test-case'])
    .withModificationUrl(['test-case'])
    .withRowConverter(convertAllRowsInTestCase)
    .withFilterCufFrom(BindableEntity.TEST_CASE)
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      textColumn('projectName')
        .withI18nKey('sqtm-core.grid.header.project')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_PROJECT_NAME)
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      textColumn('id')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_ID)
        .withI18nKey('sqtm-core.grid.header.id')
        .changeWidthCalculationStrategy(new Fixed(60)),
      testCaseSearchEditableText('reference')
        .isEditable(isTestCaseEditable)
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_REFERENCE)
        .withI18nKey('sqtm-core.grid.header.reference')
        .changeWidthCalculationStrategy(new Fixed(100)),
      testCaseSearchEditableText('name')
        .isEditable(isTestCaseEditable)
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_NAME)
        .withI18nKey('sqtm-core.grid.header.name')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      testCaseStatusColumn('status')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_STATUS)
        .withI18nKey('sqtm-core.search.test-case.grid.header.status.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.status.title')
        .changeWidthCalculationStrategy(new Fixed(40)),
      testCaseImportanceColumn('importance')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_IMPORTANCE)
        .withI18nKey('sqtm-core.search.test-case.grid.header.weight.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.weight.title')
        .changeWidthCalculationStrategy(new Fixed(40)),
      infoListColumn('nature', {kind: 'infoList', infolist: 'testCaseNature'})
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_NATURE)
        .withI18nKey('sqtm-core.search.test-case.grid.header.nature')
        .changeWidthCalculationStrategy(new Fixed(100))
        .isEditable(isTestCaseEditable),
      infoListColumn('type', {kind: 'infoList', infolist: 'testCaseType'})
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_TYPE)
        .withI18nKey('sqtm-core.search.test-case.grid.header.type')
        .changeWidthCalculationStrategy(new Fixed(100))
        .isEditable(isTestCaseEditable),
      testCaseSearchAutomatable('automatable')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_AUTOMATABLE)
        .isEditable(true)
        .withI18nKey('sqtm-core.search.test-case.grid.header.automatable.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.automatable.title')
        .changeWidthCalculationStrategy(new Fixed(100)),
      textColumn('createdBy')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_CREATED_BY)
        .withI18nKey('sqtm-core.grid.header.created-by.masculine')
        .changeWidthCalculationStrategy(new Fixed(100)),
      textColumn('lastModifiedBy')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_MODIFIED_BY)
        .withI18nKey('sqtm-core.grid.header.modified-by.masculine')
        .changeWidthCalculationStrategy(new Fixed(100)),
      numericColumn('milestones')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_MILCOUNT)
        .withI18nKey('sqtm-core.search.test-case.grid.header.milestones.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.milestones.title')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn('attachments')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_ATTCOUNT)
        .withI18nKey('sqtm-core.search.test-case.grid.header.attachments.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.attachments.title')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn('coverages')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_VERSCOUNT)
        .withI18nKey('sqtm-core.search.test-case.grid.header.coverages.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.coverages.title')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn('steps')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_STEPCOUNT)
        .withI18nKey('sqtm-core.search.test-case.grid.header.steps.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.steps.title')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn('iterations')
        .withColumnPrototype(ResearchColumnPrototype.TEST_CASE_ITERCOUNT)
        .withI18nKey('sqtm-core.search.test-case.grid.header.items.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.items.title')
        .changeWidthCalculationStrategy(new Fixed(60)),
      iconLinkColumn('detail', {
        kind: 'iconLink',
        columnParamId: 'id',
        iconName: 'sqtm-core-generic:edit',
        baseUrl: '/test-case-workspace/test-case/detail'
      }),
      iconLinkColumn('folder', {
        kind: 'iconLink',
        columnParamId: 'id',
        iconName: 'folder',
        baseUrl: '/test-case-workspace/test-case'
      })
    ])
    .withInitialSortedColumns([
      {id: 'projectName', sort: Sort.ASC},
      {id: 'reference', sort: Sort.ASC},
      {id: 'name', sort: Sort.ASC},
    ])
    .withStyle(new StyleDefinitionBuilder().enableInitialLoadAnimation())
    .withScopeDefinition(new ScopeDefinitionBuilder().withCustomScope('test-case'))
    .build();
};

export function convertAllRowsInTestCase(literals: Partial<DataRow>[], projectsData: ProjectDataMap): SquashTmDataRow[] {
  return literals.map(literal => convertOneInTestCase(literal, projectsData));
}

export function convertOneInTestCase(literal: Partial<DataRow>, projectsData: ProjectDataMap): SquashTmDataRow {
  const dataRow: DataRow = new TestCase();
  dataRow.projectId = literal.projectId;
  const project = projectsData[dataRow.projectId];
  dataRow.simplePermissions = new TestCasePermissions(project);
  Object.assign(dataRow, literal);
  return dataRow;
}
