import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FilterLinkTypeComponent } from './filter-link-type.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('FilterLinkTypeComponent', () => {
  let component: FilterLinkTypeComponent;
  let fixture: ComponentFixture<FilterLinkTypeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      declarations: [ FilterLinkTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterLinkTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
