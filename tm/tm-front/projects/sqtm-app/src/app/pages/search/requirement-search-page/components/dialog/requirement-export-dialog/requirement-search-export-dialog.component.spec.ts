import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {RequirementSearchExportDialogComponent} from './requirement-search-export-dialog.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogReference} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DatePipe} from '@angular/common';
import {EMPTY} from 'rxjs';
import {RequirementSearchExportDialogConfiguration} from './requirement-search-export-dialog.configuration';

describe('RequirementExportDialogComponent', () => {
  let component: RequirementSearchExportDialogComponent;
  let fixture: ComponentFixture<RequirementSearchExportDialogComponent>;
  const overlayReference = jasmine.createSpyObj(['attachments']);

  overlayReference.attachments.and.returnValue(EMPTY);

  const dialogReference: DialogReference = new DialogReference<RequirementSearchExportDialogConfiguration>(
    'edit',
    null,
    overlayReference,
    {requirementVersionIds: [], id: '', titleKey: ''},
  );
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), AppTestingUtilsModule, ReactiveFormsModule, HttpClientTestingModule],
      declarations: [RequirementSearchExportDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: DialogReference, useValue: dialogReference}, DatePipe
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementSearchExportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
