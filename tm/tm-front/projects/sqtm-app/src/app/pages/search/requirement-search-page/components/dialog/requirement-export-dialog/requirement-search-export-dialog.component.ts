import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {RequirementSearchExportDialogConfiguration} from './requirement-search-export-dialog.configuration';
import {DialogReference, RestService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'sqtm-app-requirement-search-export-dialog',
  templateUrl: './requirement-search-export-dialog.component.html',
  styleUrls: ['./requirement-search-export-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementSearchExportDialogComponent implements OnInit {

  data: RequirementSearchExportDialogConfiguration;
  formGroup: FormGroup;

  constructor(public dialogReference: DialogReference<RequirementSearchExportDialogConfiguration>,
              private translateService: TranslateService,
              private fb: FormBuilder,
              private datePipe: DatePipe,
              private restService: RestService) {
    this.data = this.dialogReference.data;
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group(
      {
        type: this.fb.control('simple', [Validators.required]),
        fileName: this.fb.control(this.initFileName(), [Validators.required]),
        editableRichText: this.fb.control(true, [])
      }
    );
  }

  buildExportUrl() {
    const params = {
      nodes: this.data.requirementVersionIds.toString(),
      filename: this.formGroup.controls.fileName.value,
      'keep-rte-format': this.formGroup.controls.editableRichText.value,
      type: this.formGroup.controls.type.value,
    };

    return this.restService.buildExportUrlWithParams('requirement/export/searchExports', params);
  }

  getFileName() {
    return this.formGroup.controls.fileName.value + '.xls';
  }

  initFileName() {
    const date = new Date();
    const newDate = this.datePipe.transform(date, 'yyyyMMdd_HHmmss');
    return `${this.translateService.instant('sqtm-core.requirement-workspace.dialog.export.file-name-value')}_${newDate}`;
  }

  getType() {
    return [{
      id: 'simple',
      label: this.translateService.instant('sqtm-core.requirement-workspace.dialog.export.field.simple-export.label')
    }, {
      id: 'full',
      label: this.translateService.instant('sqtm-core.requirement-workspace.dialog.export.field.full-export.label')
    }];
  }

  close() {
    this.dialogReference.close();
  }

}
