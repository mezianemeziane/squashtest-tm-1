import {ChangeDetectionStrategy, Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {
  DialogReference,
  DisplayOption,
  InfoList,
  InfoListItem,
  OptionalSelectFieldComponent,
  ProjectData,
  ProjectDataMap,
  ReferentialDataService,
  RequirementCriticality,
  RequirementStatus,
  RestService
} from 'sqtm-core';
import {RequirementMultiEditConfiguration} from './requirement-multi-edit.configuration';
import {TranslateService} from '@ngx-translate/core';
import {AbstractMassEditDialog} from '../../../../test-case-search-page/abstract-mass-edit-dialog';
import {Observable} from 'rxjs';

@Component({
  selector: 'sqtm-app-requirement-multi-edit-dialog',
  templateUrl: './requirement-multi-edit-dialog.component.html',
  styleUrls: ['./requirement-multi-edit-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementMultiEditDialogComponent extends AbstractMassEditDialog implements OnInit {

  data: RequirementMultiEditConfiguration;
  category: InfoList;
  projectDataMap$: Observable<ProjectDataMap>;

  @ViewChildren(OptionalSelectFieldComponent)
  selectFields: QueryList<OptionalSelectFieldComponent>;

  constructor(private dialogReference: DialogReference<RequirementMultiEditConfiguration, boolean>,
              translateService: TranslateService,
              referentialDataService: ReferentialDataService,
              restService: RestService) {
    super(translateService, referentialDataService, restService);
    this.data = dialogReference.data;
  }

  ngOnInit(): void {
    this.projectDataMap$ = this.referentialDataService.projectDatas$;
  }

  confirm() {
    const checkFields = this.selectFields.filter(field => field.check);
    if (checkFields.length === 0) {
      this.dialogReference.close();
    }
    const result = {};
    checkFields.forEach(checkField => {
      result[checkField.fieldName] = checkField.selectedValue;
    });
    const requirementVersionsIdsToParams = this.data.editableRowsIds.toString();
    this.restService.post(['search/requirement/mass-update', requirementVersionsIdsToParams], result).subscribe(
      () => {
        this.dialogReference.result = true;
        this.dialogReference.close();
      }
    );
  }

  getCriticalityValues() {
    return this.convertLevelEnumToDisplayOptions(RequirementCriticality);
  }

  getStatusValues() {
    return this.convertLevelEnumToDisplayOptions(RequirementStatus);
  }

  private getCategory(projectDataMap: ProjectDataMap) {
    const projectsData = this.getProjectsData(projectDataMap, this.data.projectIds);
    return projectsData[0].requirementCategory;
  }

  getDefaultCategory(projectDataMap: ProjectDataMap) {
    const category = this.getCategory(projectDataMap);
    return this.getDefaultItem(category.items);
  }

  private getDefaultItem(infoListItem: InfoListItem[]) {
    const systemItems = infoListItem.filter(item => item.system);

    let defaultValue;
    if (systemItems.length > 0) {
      defaultValue = infoListItem[0].code;
    } else {
      defaultValue = infoListItem.find(item => item.default).code;
    }
    return defaultValue;
  }

  cantEditCategory(projectDataMap: ProjectDataMap) {
    const projectsData = this.getProjectsData(projectDataMap, this.data.projectIds);
    const typeList = new Set(projectsData.map(projectData => projectData.requirementCategory.id));
    return typeList.size !== 1;
  }

  getCategoryValues(projectDataMap: ProjectDataMap) {
    const category = this.getCategory(projectDataMap);
    return this.convertCategoryInfoListItemsToDisplayOptions(category.items);
  }

  getProjectsData(projectDataMap: ProjectDataMap, ids: number[]) {
    const projectsData: ProjectData[] = [];
    ids.forEach(id => projectsData.push(projectDataMap[id]));
    return projectsData;
  }

  convertCategoryInfoListItemsToDisplayOptions(infoListItems: InfoListItem[]) {
    const displayOptions: DisplayOption[] = infoListItems.map(item => {
      return {
        id: item.code,
        label: item.system ? this.translateService.instant(`sqtm-core.entity.${item.label}`) : item.label
      };
    });
    return displayOptions;
  }

}
