import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {
  ChangeVerifyingTestCaseOperationReport,
  createRequirementVersionVerifyingTestCasesMessageDialogConfiguration,
  DataRow,
  DialogService,
  GridNode,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RequirementVersionService,
  RestService,
  shouldShowCoverageMessageDialog,
  UserHistorySearchProvider
} from 'sqtm-core';
import {ActivatedRoute, Router} from '@angular/router';
import {concatMap, map, take} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {BACK_URL_PARAM} from '../../../search-constants';
import {
  searchTestCaseGridConfigFactory,
  TEST_CASE_RESEARCH_GRID,
  TEST_CASE_RESEARCH_GRID_CONFIG
} from '../test-case-search-grid.builders';
import {TestCaseSearchViewService} from '../../services/test-case-search-view.service';
import {AbstractTestCaseSearchComponent} from '../abstract-test-case-search.component';

@Component({
  selector: 'sqtm-app-test-case-for-coverage-search-page',
  templateUrl: './test-case-for-coverage-search-page.component.html',
  styleUrls: ['./test-case-for-coverage-search-page.component.less'],
  providers: [
    {
      provide: TEST_CASE_RESEARCH_GRID_CONFIG,
      useFactory: searchTestCaseGridConfigFactory
    },
    {
      provide: TEST_CASE_RESEARCH_GRID,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        TEST_CASE_RESEARCH_GRID_CONFIG,
        ReferentialDataService
      ]
    }, {
      provide: GridService,
      useExisting: TEST_CASE_RESEARCH_GRID
    },
    {
      provide: TestCaseSearchViewService,
      useClass: TestCaseSearchViewService,
      deps: [RestService]
    },
    {
      provide: UserHistorySearchProvider,
      useExisting: TestCaseSearchViewService,
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseForCoverageSearchPageComponent extends AbstractTestCaseSearchComponent implements OnInit, OnDestroy, AfterViewInit {

  constructor(referentialDataService: ReferentialDataService,
              testCaseSearchViewService: TestCaseSearchViewService,
              gridService: GridService,
              private requirementVersionService: RequirementVersionService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private dialogService: DialogService) {
    super(referentialDataService, testCaseSearchViewService, gridService);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  linkSelection() {
    this.linkFromObservable(this.gridService.selectedRows$);
  }

  linkAll() {
    this.linkFromObservable(this.gridService.gridNodes$.pipe(
      map(nodes => this.getDataRows(nodes))));
  }

  private getDataRows(nodes: GridNode[]) {
    return nodes.map(node => node.dataRow);
  }

  linkFromObservable(obs: Observable<DataRow[]>) {
    obs.pipe(
      take(1),
      map(rows => this.extractTestCaseIds(rows)),
      concatMap(testCaseIds => this.persistNewCoverages(testCaseIds))
    ).subscribe(operationReport => {
      if (shouldShowCoverageMessageDialog(operationReport)) {
        this.showCoverageReport(operationReport);
      } else {
        this.navigateBack();
      }
    });
  }

  navigateBack() {
    const backUrl = this.activatedRoute.snapshot.queryParamMap.get(BACK_URL_PARAM) || 'requirement-workspace';
    this.router.navigate([backUrl]);
  }

  private showCoverageReport(operationReport: ChangeVerifyingTestCaseOperationReport) {
    this.dialogService.openDialog(createRequirementVersionVerifyingTestCasesMessageDialogConfiguration(operationReport));
  }

  private persistNewCoverages(requirementIds: number[]): Observable<ChangeVerifyingTestCaseOperationReport> {
    const requirementVersionId = this.activatedRoute.snapshot.paramMap.get('requirementVersionId');
    return this.requirementVersionService.persistVerifyingTestCases(Number.parseInt(requirementVersionId, 10), requirementIds);
  }

  private extractTestCaseIds(rows: DataRow[]) {
    return rows.map(row => row.data.id);
  }

}
