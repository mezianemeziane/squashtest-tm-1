import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {createStore, ReferentialDataService, RestService, SquashPlatformNavigationService} from 'sqtm-core';
import {map, withLatestFrom} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-logout-page',
  templateUrl: './logout-page.component.html',
  styleUrls: ['./logout-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LogoutPageComponent implements OnInit, OnDestroy {

  private readonly store = createStore<{ loggedOut: boolean }>({loggedOut: false});

  public componentData$ = this.store.state$;

  constructor(private restService: RestService,
              private navigationService: SquashPlatformNavigationService) {
  }

  ngOnInit(): void {
    this.restService.get(['logout']).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]) => ({...state, loggedOut: true}))
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  ngOnDestroy(): void {
    this.store.complete();
  }

  generateHref(): string {
    return this.navigationService.generateHref('login');
  }
}
