import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {Parameter} from 'sqtm-core';

export interface ParameterState extends EntityState<Parameter> {

}

export const parameterEntityAdapter = createEntityAdapter<Parameter>({
  selectId: (param) => param.id
});

export const parameterEntitySelectors = parameterEntityAdapter.getSelectors();
