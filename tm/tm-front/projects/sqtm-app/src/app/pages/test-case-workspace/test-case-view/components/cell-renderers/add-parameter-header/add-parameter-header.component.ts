import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewContainerRef
} from '@angular/core';
import {
  AbstractHeaderRendererComponent,
  DialogService,
  GridDisplay,
  GridService,
  TestCaseParameterOperationReport
} from 'sqtm-core';
import {ParameterDialogComponent} from '../../dialog/parameter-dialog/parameter-dialog.component';
import {takeUntil} from 'rxjs/operators';
import {TestCaseViewService} from '../../../service/test-case-view.service';
import {TestCaseViewComponentData} from '../../../containers/test-case-view/test-case-view.component';
import {Observable, Subject} from 'rxjs';

@Component({
  selector: 'sqtm-app-add-parameter-header',
  template: `
    <ng-container *ngIf="(grid.gridDisplay$|async) as gridDisplay">
      <ng-container *ngIf="componentData$ | async as componentData">
        <ng-container *ngIf="gridDisplay.allowModifications">
          <div [attr.data-test-button-id]="'add-param'" nz-tooltip
               [nzTooltipTitle]="'sqtm-core.test-case-workspace.dataset-table.parameter.add' |translate"
               (click)="openAddParameters(componentData.testCase.id)"
               class="full-width flex-row current-workspace-main-color"
               [style.height]="calculateRowHeight(gridDisplay)" style="cursor: pointer">
            <i nz-icon style="margin: auto" class="current-workspace-button table-icon-size" nzType="sqtm-core-generic:plus"
               [nzTheme]="'outline'"></i>
          </div>
        </ng-container>
      </ng-container>
    </ng-container>`,
  styleUrls: ['./add-parameter-header.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddParameterHeaderComponent extends AbstractHeaderRendererComponent implements OnInit, OnDestroy {

  componentData$: Observable<TestCaseViewComponentData>;

  unsub$ = new Subject<void>();


  constructor(public grid: GridService,
              public cdr: ChangeDetectorRef,
              private dialogService: DialogService,
              private testCaseViewService: TestCaseViewService,
              private viewContainerRef: ViewContainerRef
  ) {
    super(grid, cdr);
    this.componentData$ = this.testCaseViewService.componentData$.pipe(
      takeUntil(this.unsub$)
    );
  }

  ngOnInit(): void {
  }

  calculateRowHeight(gridDisplay: GridDisplay): string {
    return `${gridDisplay.rowHeight}px`;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }


  openAddParameters(testCaseId) {

    const dialogRef = this.dialogService.openDialog({
      id: 'add-parameters',
      viewContainerReference: this.viewContainerRef,
      data: {
        id: 'add-parameters',
        titleKey: 'sqtm-core.test-case-workspace.dataset-table.parameter.add',
        testCaseId: testCaseId
      },
      component: ParameterDialogComponent,
      width: 600
    });

    dialogRef.dialogResultChanged$.pipe(
      takeUntil(dialogRef.dialogClosed$)
    ).subscribe((result: TestCaseParameterOperationReport) => {
      this.testCaseViewService.addParameter(result);
    });
  }

}
