import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {TestCaseWorkspaceComponent} from './test-case-workspace.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {GridService} from 'sqtm-core';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {OverlayModule} from '@angular/cdk/overlay';
import {EMPTY} from 'rxjs';
import {TC_WS_TREE} from '../../../test-case-workspace.constant';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('TestCaseWorkspaceComponent', () => {
  let component: TestCaseWorkspaceComponent;
  let fixture: ComponentFixture<TestCaseWorkspaceComponent>;
  const tableMock = jasmine.createSpyObj('tableMock', ['load', 'setColumnSorts', 'complete']);
  tableMock.selectedRows$ = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, RouterTestingModule, OverlayModule],
      declarations: [TestCaseWorkspaceComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents()
      .then(() => {
        TestBed.overrideProvider(TC_WS_TREE, {useValue: tableMock});
        TestBed.overrideProvider(GridService, {useValue: tableMock});
        fixture = TestBed.createComponent(TestCaseWorkspaceComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));
});
