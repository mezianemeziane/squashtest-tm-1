import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ExecutionOrderComponent} from './execution-order.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {GridTestingModule} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('ExecutionOrderComponent', () => {
  let component: ExecutionOrderComponent;
  let fixture: ComponentFixture<ExecutionOrderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ExecutionOrderComponent],
      imports: [GridTestingModule, AppTestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
