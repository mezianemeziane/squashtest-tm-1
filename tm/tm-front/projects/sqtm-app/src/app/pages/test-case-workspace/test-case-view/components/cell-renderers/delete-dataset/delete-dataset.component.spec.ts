import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DeleteDatasetComponent} from './delete-dataset.component';
import {DialogService, GridDisplay, GridTestingModule} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {TestCaseViewService} from '../../../service/test-case-view.service';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('DeleteDatasetComponent', () => {
  let component: DeleteDatasetComponent;
  let fixture: ComponentFixture<DeleteDatasetComponent>;
  let testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  testCaseViewService = {...testCaseViewService, componentData$: EMPTY};
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, AppTestingUtilsModule],
      declarations: [DeleteDatasetComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: TestCaseViewService, useValue: testCaseViewService},
        {provide: DialogService, useValue: dialogService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteDatasetComponent);
    component = fixture.componentInstance;
    component.gridDisplay = {} as GridDisplay;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
