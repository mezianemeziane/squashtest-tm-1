import { OverlayModule } from '@angular/cdk/overlay';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActionErrorDisplayService } from 'sqtm-core';
import { TestCaseViewService } from '../../service/test-case-view.service';

import { KeywordTestStepsComponent } from './keyword-test-steps.component';

describe('KeywordTestStepsComponent', () => {
  let component: KeywordTestStepsComponent;
  let fixture: ComponentFixture<KeywordTestStepsComponent>;
  const testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  const actionErrorDisplayService = jasmine.createSpyObj('actionErrorDisplayService', ['handleActionError']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KeywordTestStepsComponent ],
      imports: [ ReactiveFormsModule, OverlayModule ],
      providers: [
        {
          provide: TestCaseViewService,
          useValue: testCaseViewService
        },
        {
          provide: ActionErrorDisplayService,
          useValue: actionErrorDisplayService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KeywordTestStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
