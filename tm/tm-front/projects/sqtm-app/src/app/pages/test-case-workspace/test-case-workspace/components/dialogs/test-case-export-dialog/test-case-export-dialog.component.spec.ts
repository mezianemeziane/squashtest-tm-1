import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestCaseExportDialogComponent} from './test-case-export-dialog.component';
import {TranslateModule} from '@ngx-translate/core';
import {DialogReference} from 'sqtm-core';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {EMPTY} from 'rxjs';
import {DatePipe} from '@angular/common';
import {TestCaseExportDialogConfiguration} from './test-case-export-dialog-configuration';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('TestCaseExportDialogComponent', () => {
  let component: TestCaseExportDialogComponent;
  let fixture: ComponentFixture<TestCaseExportDialogComponent>;
  const overlayReference = jasmine.createSpyObj(['attachments']);

  overlayReference.attachments.and.returnValue(EMPTY);

  const dialogReference: DialogReference = new DialogReference<TestCaseExportDialogConfiguration>(
    'edit',
    null,
    overlayReference,
    {id: '', nodes: [], libraries: []},
  );
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), AppTestingUtilsModule, ReactiveFormsModule, HttpClientTestingModule],
      declarations: [TestCaseExportDialogComponent],
      providers: [
        {provide: DialogReference, useValue: dialogReference},
        DatePipe
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseExportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
