import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TestCaseWorkspaceComponent} from './test-case-workspace/containers/test-case-workspace/test-case-workspace.component';
import {RouterModule, Routes} from '@angular/router';
import {
  AnchorModule,
  AttachmentModule,
  CellRendererCommonModule,
  CustomFieldModule,
  DialogModule,
  ExecutionUiModule,
  GridModule,
  IssuesModule,
  NavBarModule,
  PlatformNavigationModule,
  RequirementUiModule,
  SqtmDragAndDropModule,
  SvgModule,
  TaTestModule,
  UiManagerModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule
} from 'sqtm-core';
import {TestCaseWorkspaceTreeComponent} from './test-case-workspace/containers/test-case-workspace-tree/test-case-workspace-tree.component';
import {NzAnchorModule} from 'ng-zorro-antd/anchor';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {NzCollapseModule} from 'ng-zorro-antd/collapse';
import {NzDatePickerModule} from 'ng-zorro-antd/date-picker';
import {NzDividerModule} from 'ng-zorro-antd/divider';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzFormModule} from 'ng-zorro-antd/form';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzMenuModule} from 'ng-zorro-antd/menu';
import {NzModalModule} from 'ng-zorro-antd/modal';
import {NzPopoverModule} from 'ng-zorro-antd/popover';
import {NzRadioModule} from 'ng-zorro-antd/radio';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzSwitchModule} from 'ng-zorro-antd/switch';
import {NzTagModule} from 'ng-zorro-antd/tag';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {NzTypographyModule} from 'ng-zorro-antd/typography';
import {TestCaseViewComponent} from './test-case-view/containers/test-case-view/test-case-view.component';
import {TestCaseWorkspaceTreeMenuComponent} from './test-case-workspace/components/test-case-workspace-tree-menu/test-case-workspace-tree-menu.component';
import {TestCaseLibraryViewComponent} from './test-case-library-view/container/test-case-library-view/test-case-library-view.component';
import {TestCaseViewContentComponent} from './test-case-view/components/test-case-view-content/test-case-view-content.component';
import {TestStepsComponent} from './test-case-view/components/test-steps/test-steps.component';
import {IssuesComponent} from './test-case-view/components/issues/issues.component';
import {ExecutionsComponent} from './test-case-view/components/executions/executions.component';
import {ScriptComponent} from './test-case-view/components/script/script.component';
import {TestCaseFolderViewComponent} from './test-case-folder-view/container/test-case-folder-view/test-case-folder-view.component';
import {TestCaseFolderViewContentComponent} from './test-case-folder-view/container/test-case-folder-view-content/test-case-folder-view-content.component';
import {TestCaseLibraryViewContentComponent} from './test-case-library-view/container/test-case-library-view-content/test-case-library-view-content.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CKEditorModule} from 'ckeditor4-angular';
import {TranslateModule} from '@ngx-translate/core';
import {TestCaseWorkspaceMultiSelectViewComponent} from './test-case-workspace-multi-select-view/containers/test-case-workspace-multi-select-view/test-case-workspace-multi-select-view.component';
import {TestCaseMultiViewContentComponent} from './test-case-workspace-multi-select-view/containers/test-case-multi-view-content/test-case-multi-view-content.component';
import {TestCaseMilestoneViewComponent} from './test-case-milestone-dashboard/containers/test-case-milestone-view/test-case-milestone-view.component';
import {TestCaseMilestoneViewContentComponent} from './test-case-milestone-dashboard/containers/test-case-milestone-view-content/test-case-milestone-view-content.component';
import {ImportTestCaseModule} from './import-test-case/import-test-case.module';
import {TestCaseExportDialogComponent} from './test-case-workspace/components/dialogs/test-case-export-dialog/test-case-export-dialog.component';
import {TransmitResultDialogComponent} from './test-case-workspace/components/dialogs/transmit-result-dialog/transmit-result-dialog.component';
import {CreateTestCaseDialogComponent} from './test-case-workspace/components/dialogs/create-test-case-dialog/create-test-case-dialog.component';
import {CreateTestCaseFormComponent} from './test-case-workspace/components/dialogs/create-test-case-form/create-test-case-form.component';
import {CreateTestCaseFromRequirementDialogComponent} from './test-case-workspace/components/dialogs/create-test-case-from-requirement-dialog/create-test-case-from-requirement-dialog.component';
import {TestCaseStatisticsModule} from '../../components/statistics/test-case-statistics/test-case-statistics.module';
import {CoverageTableComponent} from './test-case-view/components/coverage-table/coverage-table.component';
import {TestCaseInformationPanelComponent} from './test-case-view/components/test-case-information-panel/test-case-information-panel.component';
import {DatasetsTableComponent} from './test-case-view/components/datasets-table/datasets-table.component';
import {ParameterHeaderComponent} from './test-case-view/components/cell-renderers/parameter-header/parameter-header.component';
import {ParameterSourceTestCaseComponent} from './test-case-view/components/cell-renderers/parameter-source-test-case/parameter-source-test-case.component';
import {DatasetDatasetParamValueComponent} from './test-case-view/components/cell-renderers/dataset-dataset-param-value/dataset-dataset-param-value.component';
import {DatasetHeaderRendererComponent} from './test-case-view/components/header-renderers/dataset-header-renderer/dataset-header-renderer.component';
import {ExecutionOrderComponent} from './test-case-view/components/cell-renderers/execution-order/execution-order.component';
import {ExecutionStatusComponent} from './test-case-view/components/cell-renderers/execution-status/execution-status.component';
import {ExecutionLastExecutedOnComponent} from './test-case-view/components/cell-renderers/execution-last-executed-on/execution-last-executed-on.component';
import {CalledTestCaseComponent} from './test-case-view/components/called-test-case/called-test-case.component';
import {CalledTestCaseOrderRendererComponent} from './test-case-view/components/cell-renderers/called-test-case-order-renderer/called-test-case-order-renderer.component';
import {CalledTestCaseNameComponent} from './test-case-view/components/cell-renderers/called-test-case-name/called-test-case-name.component';
import {TestCaseViewAutomationPanelComponent} from './test-case-view/components/test-case-view-automation-panel/test-case-view-automation-panel.component';
import {TestCaseWeightComponent} from './test-case-view/components/test-case-weight/test-case-weight.component';
import {DatasetNameComponent} from './test-case-view/components/cell-renderers/dataset-name/dataset-name.component';
import {DeleteDatasetComponent} from './test-case-view/components/cell-renderers/delete-dataset/delete-dataset.component';
import {AddParameterHeaderComponent} from './test-case-view/components/cell-renderers/add-parameter-header/add-parameter-header.component';
import {ParameterDialogComponent} from './test-case-view/components/dialog/parameter-dialog/parameter-dialog.component';
import {DatasetDialogComponent} from './test-case-view/components/dialog/dataset-dialog/dataset-dialog.component';
import {ParamInformationDialogComponent} from './test-case-view/components/dialog/param-information-dialog/param-information-dialog.component';
import {DeleteCoverageComponent} from './test-case-view/components/cell-renderers/delete-coverage/delete-coverage.component';
import {NewVersionDialogComponent} from './test-case-view/components/dialog/new-version-dialog/new-version-dialog.component';
import {TestStepsModule} from '../../components/test-steps/test-steps.module';
import {MilestoneModule} from '../../components/milestone/milestone.module';
import {TestCaseViewSubPageComponent} from './test-case-view-sub-page/test-case-view-sub-page.component';
import {TestCaseViewDetailComponent} from './test-case-view/containers/test-case-view-detail/test-case-view-detail.component';
import {GherkinAceEditorModule} from '../../components/gherkin-ace-editor/gherkin-ace-editor.module';
import {KeywordTestStepsComponent} from './test-case-view/components/keyword-test-steps/keyword-test-steps.component';
import {ScriptPreviewDialogComponent} from './test-case-view/components/dialog/script-preview-dialog/script-preview-dialog.component';
import {CustomDashboardModule} from '../../components/custom-dashboard/custom-dashboard.module';
import { TestCaseViewModifDuringExecComponent } from './test-case-view-modif-during-exec/test-case-view-modif-during-exec.component';
import { BddScriptExportDialogComponent } from './test-case-workspace/components/dialogs/bdd-script-export-dialog/bdd-script-export-dialog.component';
import { GherkinScriptExportDialogComponent } from './test-case-workspace/components/dialogs/gherkin-script-export-dialog/gherkin-script-export-dialog.component';
import { ScriptAutoComponent } from './test-case-view/components/script-auto/script-auto.component';


export const routes: Routes = [
  {
    path: '',
    component: TestCaseWorkspaceComponent,
    children: [
      {
        path: 'test-case/:testCaseId', component: TestCaseViewComponent, children: [
          {path: 'content', component: TestCaseViewContentComponent},
          {path: 'steps', component: TestStepsComponent},
          {path: 'keywordSteps', component: KeywordTestStepsComponent},
          {path: 'issues', component: IssuesComponent},
          {path: 'executions', component: ExecutionsComponent},
          {path: 'script', component: ScriptComponent},
        ]
      },
      {
        path: 'test-case-library/:testCaseLibraryId', component: TestCaseLibraryViewComponent, children: [
          {path: 'content', component: TestCaseLibraryViewContentComponent},
        ]
      },
      {
        path: 'test-case-folder/:testCaseFolderId', component: TestCaseFolderViewComponent, children: [
          {path: 'content', component: TestCaseFolderViewContentComponent},
        ]
      },
      {
        path: 'test-case-workspace-multi', component: TestCaseWorkspaceMultiSelectViewComponent, children: [
          {path: 'content', component: TestCaseMultiViewContentComponent},
        ]
      },
      {
        path: 'milestone-dashboard', component: TestCaseMilestoneViewComponent, children: [
          {path: 'content', component: TestCaseMilestoneViewContentComponent},
        ]
      },
    ]
  },
  {
    path: 'test-case/detail/:testCaseId', component: TestCaseViewSubPageComponent, children: [
      {path: '', redirectTo: 'content'},
      {path: 'content', component: TestCaseViewContentComponent},
      {path: 'steps', component: TestStepsComponent},
      {path: 'keywordSteps', component: KeywordTestStepsComponent},
      {path: 'issues', component: IssuesComponent},
      {path: 'executions', component: ExecutionsComponent},
      {path: 'script', component: ScriptComponent},
    ]
  },
  {
    path: 'test-case/modification-during-exec/:testCaseId/execution/:executionId',
    component: TestCaseViewModifDuringExecComponent, children: [
      {path: '', redirectTo: 'content'},
      {path: 'content', component: TestCaseViewContentComponent},
      {path: 'steps', component: TestStepsComponent},
      {path: 'keywordSteps', component: KeywordTestStepsComponent},
      {path: 'issues', component: IssuesComponent},
      {path: 'executions', component: ExecutionsComponent},
      {path: 'script', component: ScriptComponent},
    ]
  }
];

@NgModule({
  declarations: [
    TestCaseWorkspaceComponent,
    TestCaseWorkspaceTreeComponent,
    TestCaseWorkspaceTreeMenuComponent,
    TestCaseExportDialogComponent,
    TransmitResultDialogComponent,
    CreateTestCaseDialogComponent,
    CreateTestCaseFormComponent,
    CreateTestCaseFromRequirementDialogComponent,
    TestCaseFolderViewComponent,
    TestCaseFolderViewContentComponent,
    TestCaseLibraryViewComponent,
    TestCaseLibraryViewContentComponent,
    TestCaseMilestoneViewComponent,
    TestCaseMilestoneViewContentComponent,
    TestCaseViewComponent,
    CoverageTableComponent,
    TestCaseInformationPanelComponent,
    DatasetsTableComponent,
    ParameterHeaderComponent,
    ParameterSourceTestCaseComponent,
    DatasetDatasetParamValueComponent,
    DatasetHeaderRendererComponent,
    TestCaseViewContentComponent,
    TestStepsComponent,
    IssuesComponent,
    ExecutionsComponent,
    ScriptComponent,
    ExecutionOrderComponent,
    ExecutionStatusComponent,
    ExecutionLastExecutedOnComponent,
    CalledTestCaseComponent,
    CalledTestCaseOrderRendererComponent,
    CalledTestCaseNameComponent,
    TestCaseViewAutomationPanelComponent,
    TestCaseWeightComponent,
    DatasetNameComponent,
    DeleteDatasetComponent,
    AddParameterHeaderComponent,
    ParameterDialogComponent,
    DatasetDialogComponent,
    ParamInformationDialogComponent,
    DeleteCoverageComponent,
    NewVersionDialogComponent,
    TestCaseWorkspaceMultiSelectViewComponent,
    TestCaseMultiViewContentComponent,
    TestCaseViewSubPageComponent,
    TestCaseViewDetailComponent,
    KeywordTestStepsComponent,
    ScriptPreviewDialogComponent,
    TestCaseViewModifDuringExecComponent,
    BddScriptExportDialogComponent,
    GherkinScriptExportDialogComponent,
    ScriptAutoComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SvgModule,
    WorkspaceCommonModule,
    UiManagerModule,
    NavBarModule,
    NzMenuModule,
    GridModule,
    NzIconModule,
    NzToolTipModule,
    NzPopoverModule,
    NzInputModule,
    CellRendererCommonModule,
    DialogModule,
    WorkspaceLayoutModule,
    NzDropDownModule,
    ReactiveFormsModule,
    CKEditorModule,
    CustomFieldModule,
    TranslateModule.forChild(),
    NzButtonModule,
    PlatformNavigationModule,
    NzCheckboxModule,
    FormsModule,
    NzRadioModule,
    NzSelectModule,
    AttachmentModule,
    ImportTestCaseModule,
    AnchorModule,
    SqtmDragAndDropModule,
    NzCollapseModule,
    TestCaseStatisticsModule,
    NzAnchorModule,
    NzDatePickerModule,
    NzFormModule,
    NzSwitchModule,
    NzTagModule,
    NzInputModule,
    NzModalModule,
    TestStepsModule,
    RequirementUiModule,
    ExecutionUiModule,
    IssuesModule,
    MilestoneModule,
    NzTypographyModule,
    NzDividerModule,
    TaTestModule,
    GherkinAceEditorModule,
    CustomDashboardModule,
  ]
})
export class TestCaseWorkspaceModule {
}
