import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-order-renderer',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <span>
        {{getOrder()}}
        </span>
      </div>
    </ng-container>`,
  styleUrls: ['./called-test-case-order-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalledTestCaseOrderRendererComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(private cdr: ChangeDetectorRef, private gridService: GridService) {
    super(gridService, cdr);
  }

  ngOnInit() {
  }

  getOrder() {
    return this.row.data[this.columnDisplay.id] + 1;
  }

}

export function calledTestCaseStepOrder(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(CalledTestCaseOrderRendererComponent);
}
