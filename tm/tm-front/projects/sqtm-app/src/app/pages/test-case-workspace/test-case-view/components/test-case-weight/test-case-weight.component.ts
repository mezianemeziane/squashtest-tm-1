import {ChangeDetectionStrategy, Component, Input, ViewChild} from '@angular/core';
import {TestCaseViewComponentData} from '../../containers/test-case-view/test-case-view.component';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {EditableSelectLevelEnumFieldComponent, TestCaseWeight} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-test-case-weight',
  templateUrl: './test-case-weight.component.html',
  styleUrls: ['./test-case-weight.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseWeightComponent {

  @Input()
  testCaseViewComponentData: TestCaseViewComponentData;

  @ViewChild(EditableSelectLevelEnumFieldComponent)
  selectField: EditableSelectLevelEnumFieldComponent;

  get editable() {
    return this.testCaseViewComponentData.permissions.canWrite
      && this.testCaseViewComponentData.milestonesAllowModification;
  }

  public isSelectFullWidth: boolean;

  testCaseImportance = TestCaseWeight;

  constructor(private testCaseViewService: TestCaseViewService) {
  }

  public get selectFieldCssClass(): string {
    return this.isSelectFullWidth ? 'select-full-size' : '';
  }

  changeImportanceAuto(auto: boolean) {
    const testCaseId = this.testCaseViewComponentData.testCase.id;
    this.testCaseViewService.updateTestCaseImportanceAuto(testCaseId, auto);
  }

  get isImportanceEditable(): boolean {
    const canWrite = this.testCaseViewComponentData.permissions.canWrite;
    const importanceIsNotAuto = !this.testCaseViewComponentData.testCase.importanceAuto;
    return canWrite && importanceIsNotAuto;
  }

  onToggleEditMode($event: boolean) {
    this.isSelectFullWidth = $event;
  }
}
