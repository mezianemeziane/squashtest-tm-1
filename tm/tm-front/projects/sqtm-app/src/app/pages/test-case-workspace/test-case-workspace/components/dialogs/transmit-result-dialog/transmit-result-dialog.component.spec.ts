import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TransmitResultDialogComponent} from './transmit-result-dialog.component';
import {DialogReference} from 'sqtm-core';
import {EMPTY} from 'rxjs';
import {TransmitResult} from '../../../containers/test-case-workspace-tree/test-case-workspace-tree.component';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('TransmitResultDialogComponent', () => {
  let component: TransmitResultDialogComponent;
  let fixture: ComponentFixture<TransmitResultDialogComponent>;
  const overlayReference = jasmine.createSpyObj(['attachments']);

  overlayReference.attachments.and.returnValue(EMPTY);
  const dialogReference: DialogReference = new DialogReference<any>(
    'edit',
    null,
    overlayReference,
    {areAllEligible: false, eligibleTcIds: []} as TransmitResult,
  );
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [TransmitResultDialogComponent],
      providers: [{provide: DialogReference, useValue: dialogReference}],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransmitResultDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
