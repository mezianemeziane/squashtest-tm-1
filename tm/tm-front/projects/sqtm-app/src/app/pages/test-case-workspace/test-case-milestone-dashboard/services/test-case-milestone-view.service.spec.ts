import {TestBed} from '@angular/core/testing';

import {TestCaseMilestoneViewService} from './test-case-milestone-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';

describe('TestCaseMilestoneViewService', () => {
  let service: TestCaseMilestoneViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      providers: [TestCaseMilestoneViewService]
    });
    service = TestBed.inject(TestCaseMilestoneViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
