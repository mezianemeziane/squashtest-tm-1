import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslatePipe } from '@ngx-translate/core';
import { DialogReference } from 'sqtm-core';
import { AppTestingUtilsModule } from '../../../../../../utils/testing-utils/app-testing-utils.module';
import { TranslateMockPipe } from '../../../../../../utils/testing-utils/mocks.pipe';

import { ScriptPreviewDialogComponent } from './script-preview-dialog.component';

describe('ScriptPreviewDialogComponent', () => {
  let component: ScriptPreviewDialogComponent;
  let fixture: ComponentFixture<ScriptPreviewDialogComponent>;
  const dialogReference = {
    data: {
      content: 'script-preview-dialog-component'
    }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [AppTestingUtilsModule],
      declarations: [ScriptPreviewDialogComponent],
      providers: [{
        provide: DialogReference,
        useValue: dialogReference
      }, {
        provide: TranslatePipe,
        useClass: TranslateMockPipe
      }]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScriptPreviewDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
