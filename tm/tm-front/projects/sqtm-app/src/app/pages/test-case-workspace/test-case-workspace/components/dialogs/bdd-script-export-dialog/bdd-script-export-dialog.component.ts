import { DatePipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DialogReference, RestService } from 'sqtm-core';
import { TestCaseExportDialogConfiguration } from '../test-case-export-dialog/test-case-export-dialog-configuration';

@Component({
  selector: 'sqtm-app-bdd-script-export-dialog',
  templateUrl: './bdd-script-export-dialog.component.html',
  styleUrls: ['./bdd-script-export-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class BddScriptExportDialogComponent implements OnInit {

  data: TestCaseExportDialogConfiguration;

  formGroup: FormGroup;

  constructor(
    public dialogReference: DialogReference<TestCaseExportDialogConfiguration>,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    private translateService: TranslateService,
    private restService: RestService) {
    this.data = this.dialogReference.data;
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group(
      {
        fileName: this.fb.control(this.initFileName(), [Validators.required])
      }
    );
  }

  initFileName() {
    const date = new Date();
    const newDate = this.datePipe.transform(date, 'yyyyMMdd_HHmmss');
    return `${this.translateService.instant('sqtm-core.test-case-workspace.dialog.export.file-name-value')}_${newDate}`;
  }

  getFileName() {
    return this.formGroup.controls.fileName.value
  }

  doExport() {
    const params = {
      nodes: this.data.nodes.toString(),
      libraries: this.data.libraries.toString(),
      filename: this.formGroup.controls.fileName.value,
    };
    return this.restService.buildExportUrlWithParams(`test-case/export/content/keyword-scripts`, params);
  }

  close() {
    this.dialogReference.close();
  }
}
