import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ExecutionLastExecutedOnComponent} from './execution-last-executed-on.component';
import {GridTestingModule} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('ExecutionLastExecutedOnComponent', () => {
  let component: ExecutionLastExecutedOnComponent;
  let fixture: ComponentFixture<ExecutionLastExecutedOnComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ExecutionLastExecutedOnComponent],
      imports: [GridTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionLastExecutedOnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
