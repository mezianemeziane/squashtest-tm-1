import {ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TestCaseFolderViewService} from '../../services/test-case-folder-view.service';
import {testCaseFolderViewLogger} from '../../test-case-folder-view.logger';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {filter, map, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {
  AttachmentDrawerComponent,
  AttachmentState,
  EntityRowReference,
  EntityViewComponentData,
  EntityViewService,
  GenericEntityViewService,
  Identifier,
  ReferentialDataService,
  SquashTmDataRowType,
  TestCasePermissions,
  WorkspaceWithTreeComponent
} from 'sqtm-core';
import {TestCaseFolderState} from '../../state/test-case-folder.state';

const logger = testCaseFolderViewLogger.compose('TestCaseFolderViewComponent');

@Component({
  selector: 'sqtm-app-test-case-folder-view',
  templateUrl: './test-case-folder-view.component.html',
  styleUrls: ['./test-case-folder-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TestCaseFolderViewService,
      useClass: TestCaseFolderViewService,
    },
    {
      provide: EntityViewService,
      useExisting: TestCaseFolderViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: TestCaseFolderViewService
    }
  ]
})
export class TestCaseFolderViewComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  @ViewChild('content', {read: ElementRef})
  content: ElementRef;

  constructor(private route: ActivatedRoute,
              public readonly testCaseFolderService: TestCaseFolderViewService,
              private referentialDataService: ReferentialDataService,
              private workspaceWithTree: WorkspaceWithTreeComponent) {
    logger.debug('Instantiate TestCaseFolderViewComponent');
  }

  ngOnInit() {
    this.referentialDataService.loaded$.pipe(
      takeUntil(this.unsub$),
      filter(loaded => loaded),
      take(1)
    ).subscribe(() => {
      logger.debug(`Loading TestCaseView Data by http request`);
      this.loadData();
      this.initializeTreeSynchronization();
    });
  }

  private initializeTreeSynchronization() {
    this.testCaseFolderService.simpleAttributeRequiringRefresh = ['name'];
    this.testCaseFolderService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.testCaseFolderService.componentData$),
      map(([{}, componentData]: [{}, TestCaseFolderViewComponentData]) =>
        new EntityRowReference(componentData.testCaseFolder.id, SquashTmDataRowType.TestCaseFolder).asString())
    ).subscribe((identifier: Identifier) => {
      this.workspaceWithTree.requireNodeRefresh([identifier]);
    });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('testCaseFolderId')),
      ).subscribe((id) => {
      this.testCaseFolderService.load(parseInt(id, 10));
    });
  }

  ngOnDestroy(): void {
    this.testCaseFolderService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }


  getAttachmentCount(attachmentState: AttachmentState): number {
    const attachments = Object.values(attachmentState.entities);
    return attachments.filter(attachment => attachment.kind === 'persisted-attachment').length;
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }
}

export interface TestCaseFolderViewComponentData
  extends EntityViewComponentData<TestCaseFolderState, 'testCaseFolder', TestCasePermissions> {
}

