import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TestCaseViewModifDuringExecComponent} from './test-case-view-modif-during-exec.component';
import {AppTestingUtilsModule} from '../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {ExecutionRunnerOpenerService} from '../../execution/execution-runner/services/execution-runner-opener.service';
import {OverlayModule} from '@angular/cdk/overlay';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('TestCaseViewModifDuringExecComponent', () => {
  let component: TestCaseViewModifDuringExecComponent;
  let fixture: ComponentFixture<TestCaseViewModifDuringExecComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, RouterTestingModule, OverlayModule],
      declarations: [TestCaseViewModifDuringExecComponent],
      providers: [ExecutionRunnerOpenerService],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseViewModifDuringExecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
