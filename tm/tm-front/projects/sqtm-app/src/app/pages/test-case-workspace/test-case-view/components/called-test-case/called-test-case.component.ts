import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Extendable, Fixed, GridDefinition, GridService, indexColumn, Limited, smallGrid, textColumn} from 'sqtm-core';
import {TCW_CALLED_TC_TABLE} from '../../test-case-view.constant';
import {calledTestCaseStepOrder} from '../cell-renderers/called-test-case-order-renderer/called-test-case-order-renderer.component';
import {calledTestCaseNameColumn} from '../cell-renderers/called-test-case-name/called-test-case-name.component';

export function tcwCalledTCTableDefiniton(): GridDefinition {
  return smallGrid('test-case-view-called-test-case')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      textColumn('projectName')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .withI18nKey('sqtm-core.entity.project.label.singular'),
      textColumn('reference')
        .withI18nKey('sqtm-core.entity.generic.reference.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      calledTestCaseNameColumn('name')
        .changeWidthCalculationStrategy(new Extendable(100, 1.5))
        .withI18nKey('sqtm-core.entity.test-case.label.singular'),
      textColumn('datasetName')
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .changeWidthCalculationStrategy(new Limited(200)),
      calledTestCaseStepOrder('stepOrder')
        .withI18nKey('sqtm-core.entity.test-case.test-step.order.short')
        .withTitleI18nKey('sqtm-core.entity.test-case.test-step.order.label')
        .changeWidthCalculationStrategy(new Fixed(60)),
    ]).withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-called-test-case',
  template: `
    <sqtm-core-grid></sqtm-core-grid>`,
  styleUrls: ['./called-test-case.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: TCW_CALLED_TC_TABLE
    }
  ]
})
export class CalledTestCaseComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
