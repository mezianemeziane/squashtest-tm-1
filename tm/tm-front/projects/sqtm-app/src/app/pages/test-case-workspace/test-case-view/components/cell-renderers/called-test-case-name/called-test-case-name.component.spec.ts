import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CalledTestCaseNameComponent} from './called-test-case-name.component';
import {ColumnDisplay, DataRow, GridTestingModule} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('CalledTestCaseNameComponent', () => {
  let component: CalledTestCaseNameComponent;
  let fixture: ComponentFixture<CalledTestCaseNameComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, AppTestingUtilsModule],
      declarations: [CalledTestCaseNameComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalledTestCaseNameComponent);
    component = fixture.componentInstance;
    component.row = {
      id: 'TestCase-1',
      projectId: 1,
      data: {'NAME': 'TestCase 92'},
    } as unknown as DataRow;
    component.columnDisplay = {
      id: 'NAME'
    } as unknown as ColumnDisplay;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display name of calling test case', () => {
    const debugElement = fixture.debugElement;
    const name = debugElement.nativeElement.querySelector('a').innerText;
    expect(name).toBe('TestCase 92');
  });
});
