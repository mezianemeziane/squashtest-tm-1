import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {TestCaseViewService} from '../../service/test-case-view.service';

@Component({
  selector: 'sqtm-app-test-steps',
  template: `
      <ng-container *ngIf="testCaseViewService.componentData$ | async as componentData">
          <sqtm-app-test-step-list [steps]="testCaseViewService.testStep$ | async"
                                   [draggingStep]="testCaseViewService.stepDragging$ | async"
                                   [extendedPrerequisite]="componentData.testCase.testSteps.extendedPrerequisite"
                                   [prerequisite]="componentData.testCase.prerequisite"
                                   [draggingTestCase]="testCaseViewService.testCaseDragging$ | async"
                                   [stepCreationMode]="testCaseViewService.stepCreationMode$ | async"
                                   [stepCount]="testCaseViewService.stepCount$ | async"
                                   [customFieldBindingData]="componentData.projectData.customFieldBinding.TEST_STEP"
                                   [initialScrollTop]="componentData.testCase.testSteps.scrollTop"
                                   [editable]="componentData.permissions.canWrite &&  componentData.milestonesAllowModification"
                                    [linkable]="componentData.permissions.canLink">
          </sqtm-app-test-step-list>
      </ng-container>
  `,
  styleUrls: ['./test-steps.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestStepsComponent implements OnInit {

  constructor(public testCaseViewService: TestCaseViewService) {
  }

  ngOnInit() {
  }

}
