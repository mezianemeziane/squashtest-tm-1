import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {Dataset} from 'sqtm-core';

export interface DataSetState extends EntityState<Dataset> {

}

export const datasetEntityAdapter = createEntityAdapter<Dataset>({
  selectId: (dataset) => dataset.id
});

export const datasetEntitySelectors = datasetEntityAdapter.getSelectors();
