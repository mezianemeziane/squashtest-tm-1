import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-execution-status',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <sqtm-core-execution-status style="margin: auto; width: 80%" [status]="row.data[columnDisplay.id]"></sqtm-core-execution-status>
      </div>
    </ng-container>
  `,
  styleUrls: ['./execution-status.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionStatusComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public girdService: GridService, public cdRef: ChangeDetectorRef) {
    super(girdService, cdRef);
  }

  ngOnInit() {
  }

}

export function executionStatusColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ExecutionStatusComponent);
}
