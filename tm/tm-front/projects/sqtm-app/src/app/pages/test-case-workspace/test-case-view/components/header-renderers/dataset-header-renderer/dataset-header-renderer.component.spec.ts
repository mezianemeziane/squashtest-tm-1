import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DatasetHeaderRendererComponent} from './dataset-header-renderer.component';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {Fixed, GridDisplay, GridTestingModule} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('DatasetHeaderRendererComponent', () => {
  let component: DatasetHeaderRendererComponent;
  let fixture: ComponentFixture<DatasetHeaderRendererComponent>;
  const translateService = jasmine.createSpyObj('TranslateService', ['instant']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DatasetHeaderRendererComponent],
      imports: [GridTestingModule, AppTestingUtilsModule],
      providers: [{
        provide: TranslateService,
        useValue: translateService
      }],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasetHeaderRendererComponent);
    component = fixture.componentInstance;
    component.columnDisplay = {
      id: 'id',
      show: true,
      widthCalculationStrategy: new Fixed(100),
      label: 'mockLabel',
      headerPosition: 'left', contentPosition: 'left', showHeader: true,
      viewportName: 'mainViewport'
    };
    component.gridDisplay = {} as unknown as GridDisplay;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
