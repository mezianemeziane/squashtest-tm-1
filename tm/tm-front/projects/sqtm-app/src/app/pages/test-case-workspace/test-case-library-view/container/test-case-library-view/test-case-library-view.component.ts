import {ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {TestCaseLibraryViewService} from '../../service/test-case-library-view.service';
import {
  AttachmentDrawerComponent,
  AttachmentState,
  EntityViewComponentData,
  EntityViewService,
  GenericEntityViewService,
  ReferentialDataService,
  TestCasePermissions
} from 'sqtm-core';
import {filter, map, take, takeUntil} from 'rxjs/operators';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {testCaseFolderViewLogger} from '../../../test-case-folder-view/test-case-folder-view.logger';
import {TestCaseLibraryState} from '../../state/test-case-library.state';

const logger = testCaseFolderViewLogger.compose('TestCaseLibraryViewComponent');

@Component({
  selector: 'sqtm-app-test-case-library-view',
  templateUrl: './test-case-library-view.component.html',
  styleUrls: ['./test-case-library-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TestCaseLibraryViewService,
      useClass: TestCaseLibraryViewService,
    },
    {
      provide: EntityViewService,
      useExisting: TestCaseLibraryViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: TestCaseLibraryViewService
    }]
})
export class TestCaseLibraryViewComponent implements OnInit, OnDestroy {

  unsub$ = new Subject<void>();

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  @ViewChild('content', {read: ElementRef})
  content: ElementRef;

  constructor(public readonly testCaseLibraryService: TestCaseLibraryViewService,
              private referentialDataService: ReferentialDataService,
              private route: ActivatedRoute
  ) {
    logger.debug('Instantiate TestCaseLibraryViewComponent');
  }

  ngOnInit() {
    this.referentialDataService.loaded$.pipe(
      takeUntil(this.unsub$),
      filter(loaded => loaded),
      take(1)
    ).subscribe(() => {
      logger.debug(`Loading TestCaseLibraryViewComponent Data by http request`);
      this.loadData();
    });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('testCaseLibraryId')),
      ).subscribe((id) => {
      this.testCaseLibraryService.load(parseInt(id, 10));
    });
  }

  ngOnDestroy(): void {
    this.testCaseLibraryService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  getAttachmentCount(attachmentState: AttachmentState): number {
    const attachments = Object.values(attachmentState.entities);
    return attachments.filter(attachment => attachment.kind === 'persisted-attachment').length;
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }
}

export interface TestCaseLibraryViewComponentData
  extends EntityViewComponentData<TestCaseLibraryState, 'testCaseLibrary', TestCasePermissions> {
}

