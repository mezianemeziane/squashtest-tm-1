import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestCaseMultiViewContentComponent} from './test-case-multi-view-content.component';
import {TestCaseMultiSelectionService} from '../../services/test-case-multi-selection.service';
import {GridService} from 'sqtm-core';
import {of} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('TestCaseMultiViewContentComponent', () => {
  let component: TestCaseMultiViewContentComponent;
  let fixture: ComponentFixture<TestCaseMultiViewContentComponent>;

  const serviceMock = jasmine.createSpyObj<TestCaseMultiSelectionService>(['init']);
  const gridServiceMock = jasmine.createSpyObj(['load']);
  gridServiceMock.selectedRows$ = of([]);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TestCaseMultiViewContentComponent],
      providers: [
        {
          provide: TestCaseMultiSelectionService,
          useValue: serviceMock
        },
        {
          provide: GridService,
          useValue: gridServiceMock
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseMultiViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
