import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ImportTestCaseService} from '../../services/import-test-case.service';
import {
  FileEncoding,
  FileEncodingKeys,
  ImportTestCaseComponentData,
  TestCaseFileFormat,
  TestCaseImportFileFormatKeys,
  XLS_TYPE,
  ZIP_TYPE
} from '../../state/import-test-case.state';
import {DisplayOption} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-import-configuration',
  templateUrl: './import-configuration.component.html',
  styleUrls: ['./import-configuration.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportConfigurationComponent implements OnInit {

  @Input()
  componentData: ImportTestCaseComponentData;

  @Input()
  wrongFileFormat: boolean;

  @Output()
  fileFormatChanged = new EventEmitter<void>();

  constructor(private importTCService: ImportTestCaseService, private translateService: TranslateService) {
  }

  ngOnInit(): void {
  }

  initializeImportFormat(): DisplayOption[] {
    const formatOptions: DisplayOption[] = [];
    for (const testCaseImportFormat of Object.keys(TestCaseFileFormat)) {
      const item = TestCaseFileFormat[testCaseImportFormat];
      formatOptions.push({label: this.translateService.instant(item.i18nKey), id: item.id});
    }
    return formatOptions;
  }

  initializeEncoding(): DisplayOption[] {
    const encodingOptions: DisplayOption[] = [];
    for (const encoding of Object.keys(FileEncoding)) {
      const item = FileEncoding[encoding];
      encodingOptions.push({label: this.translateService.instant(item.i18nKey), id: item.id});
    }

    return encodingOptions;
  }

  changeFormat(value: TestCaseImportFileFormatKeys) {
    this.fileFormatChanged.emit();
    this.importTCService.changeFormat(value);
  }

  changeEncoding(value: FileEncodingKeys) {
    this.importTCService.changeEncoding(value);
  }

  changeSelectedProject(value: number) {
    this.importTCService.changeSelectedProject(value);
  }

  addAttachment(files: File[]) {
    this.importTCService.saveFile(files[0]);
  }

  getTemplateZip() {
    const browserLang = this.translateService.getBrowserLang();
    if (browserLang === 'fr') {
      return 'test-case_import_template_zip_fr.xls';
    } else if (browserLang === 'de') {
      return 'test-case_import_template_zip_de.xls';
    } else {
      return 'test-case_import_template_zip.xls';
    }
  }

  getRestrictionFiles(componentData: ImportTestCaseComponentData): string {
    if (componentData.format === 'XLS') {
      return XLS_TYPE;
    } else {
      return ZIP_TYPE;
    }
  }

  getWrongFileKey(componentData: ImportTestCaseComponentData) {
    if (componentData.format === 'XLS') {
      return 'sqtm-core.test-case-workspace.dialog.import.error.xls-format';
    } else {
      return 'sqtm-core.test-case-workspace.dialog.import.error.zip-format';
    }
  }
}
