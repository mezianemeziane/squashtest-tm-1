import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestStepsComponent} from './test-steps.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {TestStepListComponent} from '../../../../../components/test-steps/test-step-list/test-step-list.component';

describe('TestStepsComponent', () => {
  let component: TestStepsComponent;
  let fixture: ComponentFixture<TestStepsComponent>;
  const testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TestStepsComponent, TestStepListComponent],
      providers: [
        {
          provide: TestCaseViewService,
          useValue: testCaseViewService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
