import {AfterViewInit, ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {GridService} from 'sqtm-core';

// this is the contextual menu of the tree aka right click
@Component({
  selector: 'sqtm-app-test-case-workspace-tree-menu',
  template: `
    <div sqtmCoreCustomSubMenu class="full-width full-height">
      <ul nz-menu>
        <li nz-menu-item (click)="openDisplayConfigurationManager($event)">Display Configuration</li>
        <li nz-menu-item
            [sqtmCorePlatformLink]="'/plugin/proto-plugin-ng9/first-page'"
            [sqtmCorePlatformLinkPluginIdentifier]="'proto-plugin-ng9'">Go to proto plugin !!!
        </li>
        <li nz-submenu nzTooltipTitle="SubMenu Title">
          <ul>
            <li nz-menu-item>SubMenu Item 1</li>
            <li nz-menu-item>SubMenu Item 2</li>
            <li nz-menu-item>SubMenu Item 3</li>
          </ul>
        </li>
      </ul>
    </div>
  `,
  styleUrls: ['./test-case-workspace-tree-menu.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseWorkspaceTreeMenuComponent implements OnInit, AfterViewInit {

  constructor(private grid: GridService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
  }

  openDisplayConfigurationManager($event: MouseEvent) {
    this.grid.openDisplayConfigurationManager();
  }
}


