import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {IssuesComponent, tcwIssuesTableDefinition} from './issues.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {
  GridModule,
  gridServiceFactory,
  GridTestingModule,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';

import {TCW_ISSUE_TABLE, TCW_ISSUE_TABLE_CONF} from '../../test-case-view.constant';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {TranslateModule} from '@ngx-translate/core';
import {EMPTY} from 'rxjs';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('IssuesPanelComponent', () => {
  let component: IssuesComponent;
  let fixture: ComponentFixture<IssuesComponent>;
  let testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  testCaseViewService = {...testCaseViewService, componentData$: EMPTY};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, GridModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      declarations: [IssuesComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: TCW_ISSUE_TABLE_CONF,
          useFactory: tcwIssuesTableDefinition
        },
        {
          provide: TCW_ISSUE_TABLE,
          useFactory: gridServiceFactory,
          deps: [RestService, TCW_ISSUE_TABLE_CONF, ReferentialDataService]
        },
        {provide: TestCaseViewService, useValue: testCaseViewService}
      ],
    })
      .compileComponents();
  }));

  beforeEach(waitForAsync(() => {
    fixture = TestBed.createComponent(IssuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));
});
