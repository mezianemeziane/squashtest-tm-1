import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {AddParameterHeaderComponent} from './add-parameter-header.component';
import {DialogService, GridTestingModule, WorkspaceCommonModule} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {TestCaseViewService} from '../../../service/test-case-view.service';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('AddParameterHeaderComponent', () => {
  let component: AddParameterHeaderComponent;
  let fixture: ComponentFixture<AddParameterHeaderComponent>;
  let testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  testCaseViewService = {...testCaseViewService, componentData$: EMPTY};
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, AppTestingUtilsModule, WorkspaceCommonModule],
      declarations: [AddParameterHeaderComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: TestCaseViewService, useValue: testCaseViewService},
        {provide: DialogService, useValue: dialogService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddParameterHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
