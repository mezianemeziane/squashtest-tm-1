import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {TestCaseMultiSelectionService} from '../../services/test-case-multi-selection.service';
import {Observable} from 'rxjs';
import {TestCaseMultiViewState} from '../../state/test-case-multi-view.state';
import {filter, switchMap, take} from 'rxjs/operators';
import {CustomDashboardBinding, FavoriteDashboardValue, GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-test-case-multi-view-content',
  templateUrl: './test-case-multi-view-content.component.html',
  styleUrls: ['./test-case-multi-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseMultiViewContentComponent implements OnInit {

  componentData$: Observable<TestCaseMultiViewState>;

  constructor(private viewService: TestCaseMultiSelectionService, private tree: GridService) {
  }

  ngOnInit(): void {
    this.componentData$ = this.viewService.componentData$;
  }

  getStatisticScope(componentData: TestCaseMultiViewState) {
    return componentData.scope;
  }

  refreshStats($event: MouseEvent) {
    this.tree.selectedRows$.pipe(
      take(1),
      filter(rows => rows.length > 1),
    ).subscribe(rows => {
      this.viewService.init(rows);
    });
    $event.stopPropagation();
  }

  displayFavoriteDashboard($event: MouseEvent) {
    $event.stopPropagation();
    this.changeDashboardToDisplay('dashboard');
  }

  displayDefaultDashboard($event: MouseEvent) {
    $event.stopPropagation();
    this.changeDashboardToDisplay('default');
  }

  private changeDashboardToDisplay(preferenceValue: FavoriteDashboardValue) {
    this.viewService.changeDashboardToDisplay(preferenceValue)
      .pipe(
        take(1),
        switchMap(() => this.tree.selectedRows$),
        filter(rows => rows.length > 1),
      ).subscribe(rows => {
      this.viewService.init(rows);
    });
  }

  getChartBindings(dashboard: any) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings] as CustomDashboardBinding[];
  }
}
