import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {TestCaseFolderViewService} from '../../services/test-case-folder-view.service';
import {Observable, Subject} from 'rxjs';
import {TestCaseFolderViewComponentData} from '../test-case-folder-view/test-case-folder-view.component';
import {TestCaseFolderState} from '../../state/test-case-folder.state';
import {
  BindableEntity,
  createCustomFieldValueDataSelector, CustomDashboardBinding, CustomDashboardModel,
  CustomFieldData,
  EntityRowReference,
  EntityScope,
  SquashTmDataRowType
} from 'sqtm-core';
import {takeUntil} from 'rxjs/operators';
import {select} from '@ngrx/store';

@Component({
  selector: 'sqtm-app-test-case-folder-view-content',
  templateUrl: './test-case-folder-view-content.component.html',
  styleUrls: ['./test-case-folder-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseFolderViewContentComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();
  componentData$: Observable<TestCaseFolderViewComponentData>;

  customFieldData: CustomFieldData[];

  constructor(private testCaseFolderViewService: TestCaseFolderViewService) {
    this.componentData$ = testCaseFolderViewService.componentData$;
  }

  ngOnInit() {
    this.initCufs();
  }

  initCufs() {
    this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createCustomFieldValueDataSelector(BindableEntity.TESTCASE_FOLDER))
    ).subscribe(customFieldData => {
      this.customFieldData = customFieldData;
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  refreshStats($event: MouseEvent, componentData: TestCaseFolderViewComponentData) {
    $event.stopPropagation();
    if (componentData.testCaseFolder.statistics) {
      this.testCaseFolderViewService.refreshStatistics();
    }
    if (componentData.testCaseFolder.dashboard) {
      this.testCaseFolderViewService.refreshFolderDashboard();
    }
  }

  getStatisticScope(testCaseFolder: TestCaseFolderState): EntityScope[] {
    const ref = new EntityRowReference(testCaseFolder.id, SquashTmDataRowType.TestCaseFolder).asString();
    return [
      {
        id: ref,
        label: testCaseFolder.name,
        projectId: testCaseFolder.projectId
      }
    ];
  }

  trackCfd(cfd: CustomFieldData) {
    return cfd.id;
  }

  displayFavoriteDashboard($event: MouseEvent) {
    $event.stopPropagation();
    this.testCaseFolderViewService.changeDashboardToDisplay('dashboard');
  }

  displayDefaultDashboard($event: MouseEvent) {
    $event.stopPropagation();
    this.testCaseFolderViewService.changeDashboardToDisplay('default');
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings] as CustomDashboardBinding[];
  }
}
