import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestCaseMilestoneViewContentComponent} from './test-case-milestone-view-content.component';
import {TestCaseMilestoneViewService} from '../../services/test-case-milestone-view.service';
import {TranslateModule} from '@ngx-translate/core';

describe('TestCaseMilestoneViewContentComponent', () => {
  let component: TestCaseMilestoneViewContentComponent;
  let fixture: ComponentFixture<TestCaseMilestoneViewContentComponent>;
  const serviceMock = jasmine.createSpyObj<TestCaseMilestoneViewService>(['init']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ TestCaseMilestoneViewContentComponent ],
      providers: [
        {
          provide: TestCaseMilestoneViewService,
          useValue: serviceMock
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseMilestoneViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
