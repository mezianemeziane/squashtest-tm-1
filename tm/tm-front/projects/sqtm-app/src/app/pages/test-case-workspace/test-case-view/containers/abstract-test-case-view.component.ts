import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {TestCaseViewService} from '../service/test-case-view.service';
import {
  AttachmentDrawerComponent,
  AttachmentState,
  CapsuleInformationData,
  DialogService,
  DragAndDropService,
  ExecutionStatus,
  ExecutionStatusKeys,
  isDndDataFromRequirementTreePicker,
  MilestoneModeData,
  ReferentialDataService,
  SqtmDragEnterEvent,
  SqtmDragLeaveEvent,
  TestCaseImportanceKeys,
  TestCaseStatus,
  TestCaseStatusKeys,
  TestCaseWeight,
  Workspaces
} from 'sqtm-core';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Directive,
  ElementRef, Input,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {filter, map, take, takeUntil} from 'rxjs/operators';
import {testCaseViewContent} from '../test-case-view.constant';
import {Observable, Subject} from 'rxjs';
import {TestCaseViewComponentData} from './test-case-view/test-case-view.component';
import {testCaseViewLogger} from '../test-case-view.logger';

const logger = testCaseViewLogger.compose('TestCaseViewComponent');

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractTestCaseViewComponent implements OnInit, AfterViewInit, OnDestroy {

  requirementWorkspace = Workspaces['requirement-workspace'];
  testCaseWorkspace = Workspaces['test-case-workspace'];

  componentData$: Observable<TestCaseViewComponentData>;

  status = [];

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  @ViewChild('content', {read: ElementRef})
  content: ElementRef;

  @Input()
  backUrl: string;

  unsub$ = new Subject<void>();


  protected constructor(protected route: ActivatedRoute,
                        protected router: Router,
                        public testCaseViewService: TestCaseViewService,
                        protected referentialDataService: ReferentialDataService,
                        protected cdRef: ChangeDetectorRef,
                        public translateService: TranslateService,
                        protected dndService: DragAndDropService,
                        protected renderer: Renderer2,
                        protected vcr: ViewContainerRef,
                        protected dialogService: DialogService) {
    this.componentData$ = this.testCaseViewService.componentData$;
  }

  ngOnInit() {
    logger.debug(`ngOnInit TestCaseView`);
    this.referentialDataService.loaded$.pipe(
      takeUntil(this.unsub$),
      filter(loaded => loaded),
      take(1)
    ).subscribe(() => {
      logger.debug(`Loading TestCaseView Data by http request`);
      this.loadData();
      this.initializeDndFromRequirementTreePicker();
    });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('testCaseId')),
      ).subscribe((id) => {
      logger.debug(`Loading TestCaseView ${id}`);
      this.testCaseViewService.load(parseInt(id, 10), this.backUrl);
    });
  }

  private initializeDndFromRequirementTreePicker() {
    this.dndService.dragEnter$.pipe(
      takeUntil(this.unsub$),
      filter((dragEnterEvent: SqtmDragEnterEvent) => this.shouldShowDndEffect(dragEnterEvent))
    ).subscribe(() => {
      this.markAsDropZone();
    });

    this.dndService.dragLeave$.pipe(
      takeUntil(this.unsub$),
      filter((dragLeaveEvent: SqtmDragLeaveEvent) => this.shouldShowDndEffect(dragLeaveEvent))
    ).subscribe(() => {
      this.unmarkAsDropZone();
    });
  }

  private shouldShowDndEffect(dragEnterEvent: SqtmDragEnterEvent | SqtmDragLeaveEvent) {
    return dragEnterEvent.dropTargetId === testCaseViewContent && isDndDataFromRequirementTreePicker(dragEnterEvent);
  }

  ngAfterViewInit(): void {
    this.dndService.dragAndDrop$.pipe(
      takeUntil(this.unsub$),
      filter((dnd: boolean) => !dnd && Boolean(this.content))
    ).subscribe(() => this.unmarkAsDropZone());
  }

  ngOnDestroy(): void {
    this.testCaseViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }

  getAttachmentCount(attachmentState: AttachmentState): number {
    const attachments = Object.values(attachmentState.entities);
    return attachments.filter(attachment => attachment.kind === 'persisted-attachment').length;
  }

  private markAsDropZone() {
    this.renderer.addClass(this.content.nativeElement, 'drop-requirement');
  }

  private unmarkAsDropZone() {
    this.renderer.removeClass(this.content.nativeElement, 'drop-requirement');
  }

  closeRequirementPicker() {
    this.testCaseViewService.closeRequirementTreePicker();
  }


  closeTestCasePickerDrawer() {
    this.testCaseViewService.closeTestCaseTreePicker();
  }

  exportTestCase() {
    // todo
  }

  getStatusInformationData(statusKey: TestCaseStatusKeys) {
    const testCaseStatus = TestCaseStatus[statusKey];
    const informationDisplayData: CapsuleInformationData = {
      color: testCaseStatus.color,
      titleI18nKey: 'sqtm-core.entity.generic.status.label',
      icon: testCaseStatus.icon,
      id: testCaseStatus.id,
      labelI18nKey: testCaseStatus.i18nKey
    };
    return informationDisplayData;
  }

  getImportanceInformationData(importanceKey: TestCaseImportanceKeys) {
    const testCaseImportance = TestCaseWeight[importanceKey];
    const informationDisplayData: CapsuleInformationData = {
      color: testCaseImportance.color,
      titleI18nKey: 'sqtm-core.entity.test-case.importance.label',
      icon: testCaseImportance.icon,
      id: testCaseImportance.id,
      labelI18nKey: testCaseImportance.i18nKey
    };
    return informationDisplayData;
  }

  getLastExecutionInformationData(executionStatusKey: ExecutionStatusKeys): CapsuleInformationData {
    const executionStatus = ExecutionStatus[executionStatusKey];
    const executionInformation: CapsuleInformationData = {
      id: executionStatusKey,
      titleI18nKey: 'sqtm-core.entity.test-case.last-execution.label'
    };
    if (executionStatus != null) {
      executionInformation.labelI18nKey = executionStatus.i18nKey;
      executionInformation.color = executionStatus.color;
    } else {
      executionInformation.labelI18nKey = 'sqtm-core.generic.label.no-execution';
    }

    return executionInformation;
  }

  isMilestoneModeEnabled() {
    let isMilestoneModeEnabled = false;
    this.referentialDataService.milestoneModeData$.pipe(
      take(1)
    ).subscribe((milestoneModeData: MilestoneModeData) => {
      isMilestoneModeEnabled = milestoneModeData.milestoneModeEnabled;
    });
    return isMilestoneModeEnabled;
  }
}
