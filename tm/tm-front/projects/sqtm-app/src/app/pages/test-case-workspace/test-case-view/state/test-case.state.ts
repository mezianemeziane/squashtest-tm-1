import {
  AutomationRequest,
  entitySelector,
  ExecutionStatusKeys,
  Milestone,
  RequirementVersionCoverage,
  SqtmEntityState,
  TestCaseImportanceKeys,
  TestAutomation, TestCaseStatusKeys, TestCaseKindKeys
} from 'sqtm-core';
import {
  ActionStepState,
  ActionStepView,
  CallStepState,
  CallStepView,
  StepCoverageView,
  stepsEntityAdapter,
  TestStepsState,
  TestStepState,
} from './test-step.state';
import {createSelector, select} from '@ngrx/store';
import {createActionStepFormId} from '../test-case-view.constant';
import {coverageEntitySelectors, CoverageState} from './requirement-version-coverage.state';
import {ExecutionState} from './execution.state';
import {calledTestCaseEntitySelectors, CalledTestCaseState} from './called-test-case.state';
import {parameterEntitySelectors, ParameterState} from './parameter.state';
import {datasetEntitySelectors, DataSetState} from './dataset.state';
import {datasetParamValueEntitySelectors, DatasetParamValueState} from './dataset-param-value.state';

export interface TestCaseState extends SqtmEntityState {
  name: string;
  reference: string;
  importance: TestCaseImportanceKeys;
  description: string;
  status: TestCaseStatusKeys;
  nature: number;
  type: number;
  importanceAuto: boolean;
  automatable: string;
  prerequisite: string;
  testSteps: TestStepsState;
  coverages: CoverageState;
  milestones: Milestone[];
  automationRequest: AutomationRequest;
  uuid: string;
  kind: TestCaseKindKeys;
  executions: ExecutionState;
  nbIssues: number;
  uiState: TestCaseViewUiState;
  navigationState: TestCaseViewNavigationState;
  calledTestCases: CalledTestCaseState;
  lastModifiedOn: string;
  lastModifiedBy: string;
  createdOn: string;
  createdBy: string;
  parameters: ParameterState;
  datasets: DataSetState;
  datasetParamValues: DatasetParamValueState;
  lastExecutionStatus: ExecutionStatusKeys;
  nbExecutions: number;
  automatedTest: TestAutomation;
  script: string;
  actionWordLibraryActive: boolean;
  automatedTestTechnology?: number;
  automatedTestReference?: string;
  scmRepositoryId?: number;
  configuredRemoteFinalStatus?: string;
}

export interface TestCaseViewUiState {
  openTestCaseTreePicker: boolean;
  openRequirementTreePicker: boolean;
}

export interface TestCaseViewNavigationState {
  backUrl: string;
}

export interface TestCaseStepHolder {
  testCase: {
    id: number,
    kind: TestCaseKindKeys,
    testSteps: TestStepsState,
    coverages: CoverageState
  };
}

export interface TestStepHolder {
  testSteps: TestStepsState;
}


const stepEntitySelectors = {...stepsEntityAdapter.getSelectors()};

export const testStepStateSelector = createSelector(entitySelector, (testCase: TestStepHolder) => {
  return testCase.testSteps;
});

export const testCaseIdSelector = createSelector(entitySelector, (testCase: TestCaseState) => {
  return testCase.id;
});


export const testStepsSelector = createSelector(testStepStateSelector, stepEntitySelectors.selectAll);
export const selectedStepSelector = createSelector(testStepStateSelector, state => state.selectedStepIds);
export const draggingStepSelector = createSelector(testStepStateSelector, state => state.draggingSteps);
export const draggingRequirementSelector = createSelector(testStepStateSelector, state => state.draggingRequirement);
export const currentDndTargetSelector = createSelector(testStepStateSelector, state => state.currentDndTargetId);
export const draggingTestCaseSelector = createSelector(testStepStateSelector, state => state.draggingTestCase);
export const showPlaceHolderStepSelector = createSelector(testStepStateSelector, state => state.showPlaceHolder);
export const stepIdsSelector = createSelector(testStepStateSelector, stepEntitySelectors.selectIds);
export const stepCreationModeSelector = createSelector(stepIdsSelector, (stepIds) => {
  return (stepIds as number[]).includes(createActionStepFormId);
});

export const stepCountSelector = createSelector(stepIdsSelector, (stepIds) => {
  return (stepIds as number[]).filter((id) => id !== createActionStepFormId).length;
});

export const coverageStateSelector = createSelector(entitySelector, (testCase: TestCaseState) => {
  return testCase.coverages;
});


export const coveragesSelector = createSelector(coverageStateSelector, coverageEntitySelectors.selectAll);

export const coveragesCountSelect = createSelector(coveragesSelector, (coverages: RequirementVersionCoverage[]) => {
  return coverages.length;
});

export const executionsStateSelector = createSelector(entitySelector, (testCase: TestCaseState) => {
  return testCase.executions;
});

export const calledTestCaseStateSelector = createSelector(entitySelector, (testCase: TestCaseState) => {
  return testCase.calledTestCases;
});

export const calledTestCasesSelector = createSelector(calledTestCaseStateSelector, calledTestCaseEntitySelectors.selectAll);

export const stepCoverageSelector = createSelector(coveragesSelector, coverages => {
  return coverages.reduce((coveragesByStep, coverage) => {
    if (coverage.coverageStepInfos.length > 0) {
      for (const stepId of coverage.coverageStepInfos) {
        if (coveragesByStep.hasOwnProperty(stepId.id)) {
          coveragesByStep[stepId.id].push({...coverage});
        } else {
          coveragesByStep[stepId.id] = [coverage];
        }
      }
    }
    return coveragesByStep;
  }, {} as { [K in number]: StepCoverageView[] });
});

export const parameterStateSelector = createSelector(entitySelector, (testCase: TestCaseState) => {
  return testCase.parameters;
});

export const parametersSelector = createSelector(parameterStateSelector, parameterEntitySelectors.selectAll);

export const datasetsStateSelector = createSelector(entitySelector, (testCase: TestCaseState) => {
  return testCase.datasets;
});

export const datasetsSelector = createSelector(datasetsStateSelector, datasetEntitySelectors.selectAll);
export const datasetParamValueStateSelector = createSelector(entitySelector, (testCase: TestCaseState) => {
  return testCase.datasetParamValues;
});

export const datasetParamValueSelector = createSelector(datasetParamValueStateSelector, datasetParamValueEntitySelectors.selectAll);

export const datatestTest = select(datasetsSelector, datasetParamValueSelector);


export const stepsViewSelector =
  createSelector(
    testStepsSelector,
    selectedStepSelector,
    stepCoverageSelector,
    draggingStepSelector,
    showPlaceHolderStepSelector,
    testCaseIdSelector,
    draggingRequirementSelector,
    currentDndTargetSelector,
    (steps: TestStepState[],
     selectedSteps: number[],
     coverageViewByStep,
     draggingStep,
     showPlaceHolder,
     testCaseId,
     dragRequirement,
     dndTarget
    ) => {
      return steps.map(step => {
        const selected = selectedSteps.includes(step.id);
        const activeSelection = selectedSteps.length > 0;
        if (step.kind === 'call-step') {
          const callStepView: CallStepView = {
            ...step as CallStepState, testCaseId, selected, activeSelection, draggingStep, showPlaceHolder
          };
          return callStepView;
        } else {
          const coverages: StepCoverageView[] = coverageViewByStep[step.id] || [];
          const coverageDndTarget = dragRequirement && dndTarget === step.id;
          const actionStepView: ActionStepView = {
            ...step as ActionStepState,
            testCaseId,
            selected,
            coverages,
            activeSelection,
            draggingStep,
            showPlaceHolder,
            coverageDndTarget
          };
          return actionStepView;
        }
      });
    });
