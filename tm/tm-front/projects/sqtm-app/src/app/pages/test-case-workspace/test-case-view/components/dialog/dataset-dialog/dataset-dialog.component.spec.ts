import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DatasetDialogComponent} from './dataset-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogReference, RestService} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import createSpyObj = jasmine.createSpyObj;

describe('DatasetDialogComponent', () => {
  let component: DatasetDialogComponent;
  let fixture: ComponentFixture<DatasetDialogComponent>;
  const dialogReference = createSpyObj(['close']);
  dialogReference.data = {parameters: []};
  const restService = {};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, TranslateModule.forRoot()],
      declarations: [DatasetDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: DialogReference, useValue: dialogReference},
        {provide: RestService, useValue: restService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasetDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
