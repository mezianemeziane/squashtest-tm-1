import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import { ScriptAutoComponent } from './script-auto.component';
import {EMPTY} from "rxjs";
import {AppTestingUtilsModule} from "../../../../../utils/testing-utils/app-testing-utils.module";
import {TranslateModule} from "@ngx-translate/core";
import {TestCaseWeightComponent} from "../test-case-weight/test-case-weight.component";
import {TestCaseViewService} from "../../service/test-case-view.service";
import {NO_ERRORS_SCHEMA} from "@angular/core";
import {ActionErrorDisplayService} from "sqtm-core";

describe('ScriptAutoComponent', () => {
  let component: ScriptAutoComponent;
  let fixture: ComponentFixture<ScriptAutoComponent>;
  let testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);
  testCaseViewService = {...testCaseViewService, componentData$: EMPTY};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, TranslateModule.forRoot()],
      declarations: [ ScriptAutoComponent ],
      providers: [
        {
          provide: TestCaseViewService,
          useValue: testCaseViewService
        },
        {
          provide: ActionErrorDisplayService,
          useValue: ActionErrorDisplayService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScriptAutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
