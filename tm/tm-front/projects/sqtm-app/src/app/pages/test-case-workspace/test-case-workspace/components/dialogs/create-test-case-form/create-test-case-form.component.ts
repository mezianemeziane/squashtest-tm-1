import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {
  AbstractCreateEntityForm,
  DisplayOption,
  EntityFormModel,
  i18NEnumToOptions,
  ReferentialDataService,
  TestCaseKind
} from 'sqtm-core';
import {FormBuilder} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-create-test-case-form',
  templateUrl: './create-test-case-form.component.html',
  styleUrls: ['./create-test-case-form.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateTestCaseFormComponent extends AbstractCreateEntityForm implements OnInit {

  SCRIPT_LANGUAGE_FIELD_NAME = 'scriptLanguage';

  get kindOptions(): DisplayOption[] {
    return i18NEnumToOptions(TestCaseKind);
  }

  constructor(protected fb: FormBuilder,
              protected translateService: TranslateService,
              protected referentialDataService: ReferentialDataService,
              protected cdr: ChangeDetectorRef) {
    super(fb, translateService, referentialDataService, cdr);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  protected initializeAdditionalField() {
    this.formGroup.addControl(this.SCRIPT_LANGUAGE_FIELD_NAME, this.fb.control(TestCaseKind.STANDARD.id));
  }

  protected addAdditionalField(formModel: Partial<EntityFormModel>) {
    formModel[this.SCRIPT_LANGUAGE_FIELD_NAME] = this.formGroup.get(this.SCRIPT_LANGUAGE_FIELD_NAME).value;
  }

}
