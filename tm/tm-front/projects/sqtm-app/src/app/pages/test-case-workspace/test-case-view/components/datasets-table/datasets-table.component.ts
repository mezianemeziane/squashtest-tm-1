import {ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {
  ColumnWithFilter,
  Dataset,
  DialogService,
  Extendable,
  Fixed,
  GridDefinition,
  GridService,
  smallGrid,
  Sort,
  TestCaseParameterOperationReport
} from 'sqtm-core';
import {TCW_DATASET_TABLE} from '../../test-case-view.constant';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {dataSetNameColumn} from '../cell-renderers/dataset-name/dataset-name.component';
import {deleteDataSetColumn} from '../cell-renderers/delete-dataset/delete-dataset.component';
import {AddParameterHeaderComponent} from '../cell-renderers/add-parameter-header/add-parameter-header.component';
import {map, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {DatasetDialogComponent} from '../dialog/dataset-dialog/dataset-dialog.component';
import {Observable, Subject} from 'rxjs';
import {TestCaseViewComponentData} from '../../containers/test-case-view/test-case-view.component';
import {datasetDatasetParamValueColumn} from '../cell-renderers/dataset-dataset-param-value/dataset-dataset-param-value.component';
import {ParameterHeaderComponent} from '../cell-renderers/parameter-header/parameter-header.component';


export function tcwDatasetsTableDefinition(): GridDefinition {
  return smallGrid('test-case-view-datasets')
    .withColumns([
      dataSetNameColumn('name')
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Fixed(150)).withViewport('leftViewport'),
      deleteDataSetColumn().changeWidthCalculationStrategy(new Fixed(40))
        .withHeaderRenderer(AddParameterHeaderComponent).withViewport('rightViewport')
    ])
    .withRowHeight(35)
    .withInitialSortedColumns([{id: 'name', sort: Sort.ASC}])
    .build();
}

@Component({
  selector: 'sqtm-app-datasets-table',
  templateUrl: './datasets-table.component.html',
  styleUrls: ['./datasets-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {provide: GridService, useExisting: TCW_DATASET_TABLE}
  ]
})
export class DatasetsTableComponent implements OnInit, OnDestroy {

  PARAM_COLUMN_PREFIX = 'PARAM_COLUMN_';

  componentData$: Observable<TestCaseViewComponentData>;

  unsub$ = new Subject<void>();

  constructor(public grid: GridService,
              private dialogService: DialogService,
              private testCaseViewService: TestCaseViewService,
              private viewContainerRef: ViewContainerRef
  ) {
  }

  ngOnInit() {
    this.componentData$ = this.testCaseViewService.componentData$.pipe(
      takeUntil(this.unsub$)
    );
    this.initColumnsAndRows();
  }

  private initColumnsAndRows() {
    this.componentData$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.grid.columns$),
      tap(([component, columns]: [TestCaseViewComponentData, ColumnWithFilter[]]) => {
        const parametersIds = Object.values(component.testCase.parameters.ids);
        const paramColumnsIds = parametersIds.map(id => this.PARAM_COLUMN_PREFIX + id);
        const columnsIds = columns.filter(column => column.id.toString().startsWith(this.PARAM_COLUMN_PREFIX)).map(column => column.id);
        const idToDelete = columnsIds.filter(id => !paramColumnsIds.includes(id.toString()));
        this.grid.deleteColumns(idToDelete);
      }),
      map(([component, columns]) => {
        this.addParamColumns(component, columns);
        return component;
      }),
      tap((component: TestCaseViewComponentData) => {
        if (!component.permissions.canWrite || !component.milestonesAllowModification) {
          this.grid.lockGrid();
          this.grid.setColumnVisibility('delete', false);
        }
      }),
      tap((component: TestCaseViewComponentData) => {
        this.loadDataRows(component);
      })
    ).subscribe();
  }

  private addParamColumns(component: TestCaseViewComponentData, columns: ColumnWithFilter[]) {
    const parameters = Object.values(component.testCase.parameters.entities);
    const parameterColumns = parameters.map(parameter => {
      const paramColumnId = `${this.PARAM_COLUMN_PREFIX}${parameter.id}`;

      return datasetDatasetParamValueColumn(paramColumnId)
        .withLabel(parameter.name)
        .isEditable(component.permissions.canWrite)
        .changeWidthCalculationStrategy(new Extendable(150))
        .withHeaderRenderer(ParameterHeaderComponent).disableSort().build();
    });
    const columnNumber = columns.length;
    this.grid.addColumnAtIndex(parameterColumns, columnNumber);
  }

  private loadDataRows(component: TestCaseViewComponentData) {
    const paramValues = Object.values(component.testCase.datasetParamValues.entities);
    const dataSets = Object.values(component.testCase.datasets.entities);
    const dataRows = dataSets.map((dataset: Dataset) => {
      const data = {...dataset};
      paramValues.filter(paramValue => paramValue.datasetId === dataset.id).forEach(
        paramValue => {
          data[`${this.PARAM_COLUMN_PREFIX}${paramValue.parameterId}`] = paramValue;
        }
      );
      return {id: data.id, data: data};
    });
    this.grid.loadInitialDataRows(dataRows, dataRows.length);
  }

  userCanWriteTC(componentData: TestCaseViewComponentData) {
    return componentData.permissions.canWrite && componentData.milestonesAllowModification;
  }

  addDataset() {
    this.testCaseViewService.componentData$.pipe(
      take(1),
    ).subscribe(testCaseViewComponentData => {
      const dialogReference = this.dialogService.openDialog({
        id: 'add-dataset',
        viewContainerReference: this.viewContainerRef,
        data: {
          id: 'add-dataset',
          testCaseId: testCaseViewComponentData.testCase.id,
          parameters: Object.values(testCaseViewComponentData.testCase.parameters.entities),
          titleKey: 'sqtm-core.test-case-workspace.dataset-table.datasets.add'
        },
        component: DatasetDialogComponent
      });

      dialogReference.dialogResultChanged$
        .pipe(
          takeUntil(dialogReference.dialogClosed$)
        )
        .subscribe((result: TestCaseParameterOperationReport) => {
          this.testCaseViewService.addDataset(result);
        });
    });

  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
