import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {AbstractHeaderRendererComponent, EditableTextFieldComponent, GridDisplay, GridService} from 'sqtm-core';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'sqtm-app-dataset-header-renderer',
  template: `
    <ng-container *ngIf="gridDisplay">
      <div class="full-width flex-row" [style.height]="calculateRowHeight(gridDisplay)">
        <sqtm-core-editable-text-field [layout]="'no-buttons'" [size]="'small'" [formControl]="formControl"
                                       [editable]="editable"
                                       [value]="columnDisplay.label"
                                       (confirmEvent)="updateAndClose($event)"
                                       #editableTextField ngDefaultControl></sqtm-core-editable-text-field>
        <div class="full-height m-r-5 flex-fixed-size resize-handler">
        </div>
      </div>
    </ng-container>`,
  styleUrls: ['./dataset-header-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetHeaderRendererComponent extends AbstractHeaderRendererComponent implements OnInit {

  formControl: FormControl;

  @ViewChild('editableTextField')
  editableTextField: EditableTextFieldComponent;

  get editable() {
    return this.gridDisplay.allowModifications;
  }

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
    this.formControl = new FormControl('', [Validators.maxLength(255)]);
  }

  ngOnInit() {
  }

  calculateRowHeight(gridDisplay: GridDisplay): string {
    return `${gridDisplay.rowHeight}px`;
  }

  updateAndClose(currentValue: string) {
    // this.editableTextField.updateAndClose(currentValue);
  }

}
