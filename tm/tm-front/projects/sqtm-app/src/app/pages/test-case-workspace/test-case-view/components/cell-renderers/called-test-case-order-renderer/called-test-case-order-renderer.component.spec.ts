import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {CalledTestCaseOrderRendererComponent} from './called-test-case-order-renderer.component';
import {ColumnDisplay, DataRow, GridTestingModule} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

describe('OrderRendererComponent', () => {
  let component: CalledTestCaseOrderRendererComponent;
  let fixture: ComponentFixture<CalledTestCaseOrderRendererComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, AppTestingUtilsModule],
      declarations: [CalledTestCaseOrderRendererComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalledTestCaseOrderRendererComponent);
    component = fixture.componentInstance;
    component.row = {id: 'TestCase-92', data: {stepOrder: 5}} as unknown as DataRow;
    component.columnDisplay = {id: 'stepOrder'} as unknown as ColumnDisplay;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display order', () => {
    const expectValue = 6;
    expect(component.getOrder()).toBe(expectValue);
    const debugElement = fixture.debugElement;
    const divText = debugElement.nativeElement.querySelector('div').innerText;
    expect(divText).toBe(expectValue.toString());
  });
});
