import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {TestCaseMilestoneViewService} from '../../services/test-case-milestone-view.service';
import {TestCaseMilestoneViewState} from '../../state/test-case-milestone-view-state';
import {Observable, Subject} from 'rxjs';
import {ReferentialDataService} from 'sqtm-core';
import {filter, take, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-test-case-milestone-view',
  templateUrl: './test-case-milestone-view.component.html',
  styleUrls: ['./test-case-milestone-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [TestCaseMilestoneViewService]
})
export class TestCaseMilestoneViewComponent implements OnDestroy {

  public componentData$: Observable<Readonly<TestCaseMilestoneViewState>>;

  private unsub$ = new Subject<void>();

  constructor(private viewService: TestCaseMilestoneViewService,
              private referentialDataService: ReferentialDataService,
              private cdRef: ChangeDetectorRef) {
    this.referentialDataService.loaded$.pipe(
      takeUntil(this.unsub$),
      filter(loaded => loaded),
      take(1)
    ).subscribe(() => {
      this.componentData$ = this.viewService.componentData$;
      this.viewService.init();
      this.cdRef.detectChanges();
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}
