import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'sqtm-app-oauth-failure',
  template: `
    <div class="full-height full-width flex-column login-page neutral-background-color overflow-hidden"
         [sqtmCoreWorkspace]="'home-workspace'">
      <sqtm-core-horizontal-logout-bar
          class="flex-fixed-size full-width"
          [squashVersion]="null">
      </sqtm-core-horizontal-logout-bar>
      <div class="flex-fixed-size m-auto p-30 container-background-color container-border"
           style="max-width: 80%;">
        <h2>{{'sqtm-core.oauth-pages.failure.title'|translate}}</h2>
        <p>{{'sqtm-core.oauth-pages.failure.message'|translate}}</p>
      </div>
    </div>`,
  styleUrls: ['./oauth-failure.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OAuthFailureComponent {

  constructor() {
  }
}
