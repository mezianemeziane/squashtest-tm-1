import {Injectable} from '@angular/core';
import {
  AttachmentService,
  AuthenticationProtocol,
  Credentials,
  EntityViewAttachmentHelperService,
  GenericEntityViewService,
  RestService
} from 'sqtm-core';
import {provideInitialUserAccountView, UserAccountEntityState, UserAccountViewState} from '../state/user-account.model';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs';
import {map, tap, withLatestFrom} from 'rxjs/operators';

@Injectable()
export class UserAccountService extends GenericEntityViewService<UserAccountEntityState, 'userAccount'> {
  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
  ) {
    super(
      restService,
      attachmentService,
      translateService,
      attachmentHelper
    );
  }

  getInitialState(): UserAccountViewState {
    return provideInitialUserAccountView();
  }

  load(): void {
    this.restService.get<UserAccountEntityState>([this.getRootUrl()])
      .subscribe((userAccount: UserAccountEntityState) => {
        this.initializeEntityState(userAccount);
      });
  }

  protected getRootUrl(initialState?: any): string {
    return 'user-account';
  }

  changePassword(oldPassword: string, newPassword: string, initializing: boolean): Observable<any> {
    const body = {oldPassword, newPassword, initializing};
    return this.restService.post(['user-account/password'], body).pipe(
      withLatestFrom(this.componentData$),
      tap(([, state]: [any, UserAccountViewState]) => {
        const updatedState: UserAccountViewState = {
          ...state,
          userAccount: {
            ...state.userAccount,
            hasLocalPassword: true,
          }
        };
        this.store.commit(updatedState);
      })
    );
  }

  changeBugTrackerMode(isAutomatic: boolean): Observable<any> {
    const bugTrackerMode = isAutomatic ? 'Automatic' : 'Manual';
    return this.restService.post(['user-account/bug-tracker-mode'], {bugTrackerMode}).pipe(
      withLatestFrom(this.state$),
      map(([, state]) => ({
        ...state,
        userAccount: {
          ...state.userAccount,
          bugTrackerMode,
        }
      })),
      tap((newState) => this.commit(newState)));
  }

  changeBugTrackerCredentials(bugTrackerId: number, credentials: Credentials): Observable<any> {
    return this.restService.post(['user-account', 'bugtracker', bugTrackerId.toString(), 'credentials'], credentials).pipe(
      withLatestFrom(this.state$),
      map(([, state]: [any, UserAccountViewState]) => updateBugTrackerCredentials(state, bugTrackerId, credentials)),
      tap((newState) => this.commit(newState)));
  }

  deleteBugTrackerCredentials(bugTrackerId: number): Observable<any> {
    return this.restService.delete(['user-account', 'bugtracker', bugTrackerId.toString(), 'credentials']).pipe(
      withLatestFrom(this.state$),
      map(([, state]: [any, UserAccountViewState]) => updateBugTrackerCredentials(state, bugTrackerId, {
        implementedProtocol: AuthenticationProtocol.OAUTH_1A,
        type: AuthenticationProtocol.OAUTH_1A,
        token: '',
        registered: false,
        tokenSecret: '',
      })),
      tap((newState) => this.commit(newState)));
  }
}

function updateBugTrackerCredentials(state: UserAccountViewState, bugTrackerId: number, credentials?: Credentials): UserAccountViewState {
  const updatedCredentials = state.userAccount.bugTrackerCredentials.map((btCredentials) => {
    if (btCredentials.bugTracker.id === bugTrackerId) {
      return {
        ...btCredentials,
        credentials
      };
    } else {
      return {...btCredentials};
    }
  });

  return {
    ...state,
    userAccount: {
      ...state.userAccount,
      bugTrackerCredentials: updatedCredentials,
    }
  };
}
