import {ChangeDetectionStrategy, Component, InjectionToken, OnDestroy, OnInit} from '@angular/core';
import {
  AclGroup,
  Extendable,
  getAclGroupI18nKey,
  GridDefinition,
  GridService,
  gridServiceFactory,
  indexColumn,
  ProjectPermission,
  ReferentialDataService,
  RestService,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  textColumn
} from 'sqtm-core';
import {map, pluck, takeUntil} from 'rxjs/operators';
import {UserAccountService} from '../../../services/user-account.service';
import {Subject} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';

export const PERMISSIONS_TABLE_CONF = new InjectionToken('PERMISSIONS_TABLE_CONF');
export const PERMISSIONS_TABLE = new InjectionToken('PERMISSIONS_TABLE');

export function permissionsTableDefinition(): GridDefinition {
  return smallGrid('project-permissions')
    .withColumns([
      indexColumn(),
      textColumn('project')
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      textColumn('profile')
        .withI18nKey('sqtm-core.administration-workspace.views.project.permissions.profile.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
    ])
    .withInitialSortedColumns([{id: 'project', sort: Sort.ASC}])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-user-account-permissions-panel',
  template: `
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./user-account-permissions-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: PERMISSIONS_TABLE_CONF,
      useFactory: permissionsTableDefinition
    },
    {
      provide: PERMISSIONS_TABLE,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        PERMISSIONS_TABLE_CONF,
        ReferentialDataService
      ]
    },
    {
      provide: GridService,
      useExisting: PERMISSIONS_TABLE
    }
  ]
})
export class UserAccountPermissionsPanelComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();

  constructor(private readonly userAccountService: UserAccountService,
              private readonly gridService: GridService,
              private readonly translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.initializeTable();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private initializeTable() {
    const table = this.userAccountService.componentData$.pipe(
      takeUntil(this.unsub$),
      pluck('userAccount', 'projectPermissions'),
      map((permissions: ProjectPermission[]) => this.buildPermissionRows(permissions))
    );

    this.gridService.connectToDatasource(table, 'id');
  }

  private buildPermissionRows(projectPermissions: ProjectPermission[]): PermissionRow[] {
    return projectPermissions.map((perm, idx) => ({
      id: idx + 1,
      profile: this.translateService.instant(getAclGroupI18nKey(perm.permissionGroup.qualifiedName as AclGroup)),
      project: perm.projectName,
    }));
  }
}

interface PermissionRow {
  id: number;
  profile: string;
  project: string;
}

