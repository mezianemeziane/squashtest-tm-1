import {GenericEntityViewState, provideInitialGenericViewState, SqtmGenericEntityState, UserAccount} from 'sqtm-core';

export interface UserAccountEntityState extends UserAccount, SqtmGenericEntityState {
}

export interface UserAccountViewState extends GenericEntityViewState<UserAccountEntityState, 'userAccount'> {
  userAccount: UserAccountEntityState;
}

export function provideInitialUserAccountView(): Readonly<UserAccountViewState> {
  return provideInitialGenericViewState<UserAccountEntityState, 'userAccount'>('userAccount');
}
