import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChangeUserPasswordDialogComponent } from './change-user-password-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DialogReference, FieldValidationError, RestService} from 'sqtm-core';
import {UserAccountService} from '../../../services/user-account.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {of, throwError} from 'rxjs';
import {makeHttpFieldValidationError} from '../../../../../utils/testing-utils/test-error-response-generator';
import {mockTextField} from '../../../../../utils/testing-utils/test-component-generator';

describe('ChangeUserPasswordDialogComponent', () => {
  let component: ChangeUserPasswordDialogComponent;
  let fixture: ComponentFixture<ChangeUserPasswordDialogComponent>;

  const userAccountService = jasmine.createSpyObj(['load', 'changePassword']);

  const dialogRef = jasmine.createSpyObj(['close']);
  dialogRef.data = { hasLocalPassword: true};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule,
        AppTestingUtilsModule,
        TranslateModule.forRoot(),
      ],
      declarations: [ ChangeUserPasswordDialogComponent ],
      providers: [
        {
          provide: RestService,
          useValue: jasmine.createSpyObj(['get', 'post']),
        },
        {
          provide: UserAccountService,
          useValue: userAccountService,
        },
        {
          provide: DialogReference,
          useValue: dialogRef,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeUserPasswordDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    userAccountService.changePassword.and.returnValue(of({}));
    userAccountService.changePassword.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not change password if fields are empty', () => {
    const textField = mockTextField('oldPassword', component.formGroup);
    component.textFields = [textField];
    component.formGroup.get('oldPassword').setValue('');
    component.formGroup.get('newPassword').setValue('');
    component.formGroup.get('confirmPassword').setValue('');

    component.handleDialogConfirm();

    expect(userAccountService.changePassword).not.toHaveBeenCalled();
    expect(textField.showClientSideError).toHaveBeenCalled();

  });

  it('should not change password if passwords don\'t match', () => {
    component.formGroup.get('oldPassword').setValue('oldPassword');
    component.formGroup.get('newPassword').setValue('newPassword');
    component.formGroup.get('confirmPassword').setValue('confirmPassword');

    component.handleDialogConfirm();

    expect(userAccountService.changePassword).not.toHaveBeenCalled();
  });

  it('should change password', () => {
    component.formGroup.get('oldPassword').setValue('oldPassword');
    component.formGroup.get('newPassword').setValue('newPassword');
    component.formGroup.get('confirmPassword').setValue('newPassword');

    component.handleDialogConfirm();

    expect(userAccountService.changePassword).toHaveBeenCalled();
  });

  it('should show server-side errors', () => {
    component.formGroup.get('oldPassword').setValue('oldPassword');
    component.formGroup.get('newPassword').setValue('newPassword');
    component.formGroup.get('confirmPassword').setValue('newPassword');

    const fve = {fieldName: 'oldPassword', i18nKey: 'pas bon'} as FieldValidationError;

    userAccountService.changePassword.and.returnValue(throwError(
      makeHttpFieldValidationError([fve])
    ));

    component.handleDialogConfirm();

    expect(component.serverSideValidationErrors).toEqual([fve]);
  });
});
