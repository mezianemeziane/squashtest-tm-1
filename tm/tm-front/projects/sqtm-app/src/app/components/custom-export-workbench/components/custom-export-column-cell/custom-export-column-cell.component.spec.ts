import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomExportColumnCellComponent } from './custom-export-column-cell.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {mockGridService, mockPassThroughTranslateService} from '../../../../utils/testing-utils/mocks.service';
import {GridService} from 'sqtm-core';

describe('CustomExportColumnCellComponent', () => {
  let component: CustomExportColumnCellComponent;
  let fixture: ComponentFixture<CustomExportColumnCellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomExportColumnCellComponent ],
      providers: [
        { provide: TranslateService, useValue: mockPassThroughTranslateService() },
        { provide: GridService, useValue: mockGridService() },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomExportColumnCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
