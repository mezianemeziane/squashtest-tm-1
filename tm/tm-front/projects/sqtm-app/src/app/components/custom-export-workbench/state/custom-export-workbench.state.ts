import {
  CustomExportEntityType,
  getCustomExportColumnNamesByEntityType,
  Identifier,
  NewCustomExportColumn,
  NewCustomExportModel
} from 'sqtm-core';
import {createFeatureSelector, createSelector} from '@ngrx/store';

/** Front-end only type that represents a column in a transient custom export */
export interface IdentifiedExportColumn {
  id: string; // Temporary ID used in attribute tree
  label: string;
  cufId: number;
  entityType: CustomExportEntityType;
  columnName: string;
}

export type CustomExportIdentifiedColumnsByEntityType = { [entity in CustomExportEntityType]: IdentifiedExportColumn[] };

export interface CustomExportWorkbenchState {
  ui: CustomExportWorkbenchUiState;
  craftedExport: CraftedExport;
  availableColumns: CustomExportIdentifiedColumnsByEntityType;
  containerId: number;
}

export interface CraftedExport {
  existingNodeId?: number;
  name: string;
  scope: CustomExportScope;
  columns: IdentifiedExportColumn[];
}

export interface CustomExportScope {
  id: Identifier;
  name: string;
  projectId: number;
}

export interface CustomExportWorkbenchUiState {
  touched: boolean;
}

const emptyCustomExportIdentifiedColumnsByEntityType: Readonly<CustomExportIdentifiedColumnsByEntityType> = {
  [CustomExportEntityType.CAMPAIGN]: [],
  [CustomExportEntityType.TEST_SUITE]: [],
  [CustomExportEntityType.TEST_CASE]: [],
  [CustomExportEntityType.ITERATION]: [],
  [CustomExportEntityType.EXECUTION_STEP]: [],
  [CustomExportEntityType.EXECUTION]: [],
  [CustomExportEntityType.ISSUE]: [],
  [CustomExportEntityType.TEST_STEP]: [],
};

export function getIdentifiedColumnsByEntityType(withMilestoneColumns: boolean): CustomExportIdentifiedColumnsByEntityType {
  const columnsNames = getCustomExportColumnNamesByEntityType(withMilestoneColumns);
  const result = {} as CustomExportIdentifiedColumnsByEntityType;

  Object.entries(columnsNames)
    .forEach(([entity, columnNames]) => {
      result[entity] = columnNames.map((columnName) => ({
        id: columnName,
        label: columnName,
        columnName,
        cufId: undefined,
        entityType: entity,
      }));
    });

  return result;
}

export const initialCustomExportWorkbenchState: CustomExportWorkbenchState = {
  availableColumns: {...emptyCustomExportIdentifiedColumnsByEntityType},
  ui: {
    touched: false,
  },
  craftedExport: {
    name: '',
    scope: null,
    columns: [],
  },
  containerId: null,
};

const craftedExportSelector = createFeatureSelector<CraftedExport>('craftedExport');

export const exportColumnsSelector = createSelector(craftedExportSelector, (craftedExport) => craftedExport.columns);
export const availableColumnsSelector = createFeatureSelector<CustomExportIdentifiedColumnsByEntityType>('availableColumns');

export function craftedExportToNewCustomExportModel(craftedExport: CraftedExport): NewCustomExportModel {
  const columns: NewCustomExportColumn[] = craftedExport.columns.map(identifiedColumn => {
    if (Boolean(identifiedColumn.cufId)) {
      const label = identifiedColumn.entityType + '_CUF';
      return {label, cufId: identifiedColumn.cufId};
    } else {
      return {label: identifiedColumn.label, cufId: undefined};
    }
  });

  return {
    columns,
    name: craftedExport.name,
    scope: craftedExport.scope.id.toString(),
  };
}
