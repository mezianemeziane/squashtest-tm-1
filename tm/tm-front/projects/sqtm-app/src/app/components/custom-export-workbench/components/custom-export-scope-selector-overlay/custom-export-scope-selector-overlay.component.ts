import {ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {filter, map, take} from 'rxjs/operators';
import {CampaignLimitedPickerComponent, DataRow, SquashTmDataRowType} from 'sqtm-core';
import {Observable, of} from 'rxjs';
import {CustomExportScope} from '../../state/custom-export-workbench.state';

const ACCEPTED_ROW_TYPES: SquashTmDataRowType[] = [
  SquashTmDataRowType.Campaign,
  SquashTmDataRowType.Iteration,
  SquashTmDataRowType.TestSuite,
];

@Component({
  selector: 'sqtm-app-custom-export-scope-selector-overlay',
  templateUrl: './custom-export-scope-selector-overlay.component.html',
  styleUrls: ['./custom-export-scope-selector-overlay.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomExportScopeSelectorOverlayComponent implements OnInit {

  @Output()
  scopeChanged = new EventEmitter<CustomExportScope>();

  @ViewChild('picker')
  set picker(pickerElement: CampaignLimitedPickerComponent) {
    this._picker = pickerElement;

    if (pickerElement?.tree != null) {
      this.canConfirm$ = pickerElement.tree.selectedRows$.pipe(
        map((rows: DataRow[]) => this.filterOutIllegalRows(rows)),
        map((rows: DataRow[]) => rows.length > 0));
    }
  }

  get picker(): CampaignLimitedPickerComponent {
    return this._picker;
  }

  constructor() {
  }

  initialSelectedNodes = [];

  public canConfirm$: Observable<boolean> = of(false);

  @Output()
  closeRequired = new EventEmitter<void>();

  private _picker: CampaignLimitedPickerComponent;

  private filterOutIllegalRows(dataRows: DataRow[]): DataRow[] {
    if (dataRows.some(row => !ACCEPTED_ROW_TYPES.includes(row.type))) {
      this.picker.tree.unselectAllRows();
      return [];
    }

    return dataRows;
  }

  ngOnInit(): void {
  }

  handleConfirm(): void {
    if (this.picker != null) {
      // has to access directly to the tree as the picker EventEmitter can't give actual values, but just emit changes
      this.picker.tree.selectedRows$.pipe(
        take(1),
        filter((rows: DataRow[]) => rows.length > 0)
      ).subscribe((rows: DataRow[]) => this.emitScope(rows));
    }
  }

  private emitScope(rows: DataRow[]) {
    if (rows && rows.length > 0) {
      const name = rows[0].data.NAME;
      const projectId = rows[0].projectId;
      const id = rows[0].id;
      this.scopeChanged.emit({id, name, projectId});
    } else {
      this.scopeChanged.emit(null);
    }
  }

  handleClose() {
    this.closeRequired.emit();
  }
}
