import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {catchError, filter, map, take, takeUntil} from 'rxjs/operators';
import {
  ActionErrorDisplayService,
  DialogConfiguration, DialogReference,
  DialogService,
  KeywordStepFormModel,
  Option,
  SqtmDragLeaveEvent,
  SqtmDragOverEvent,
  SqtmDragStartEvent,
  SqtmDropEvent,
  AbstractActionWordFieldComponent
} from 'sqtm-core';
import { ScriptPreviewDialogComponent } from '../../../pages/test-case-workspace/test-case-view/components/dialog/script-preview-dialog/script-preview-dialog.component';
import { TestCaseViewComponentData } from '../../../pages/test-case-workspace/test-case-view/containers/test-case-view/test-case-view.component';
import { TestCaseViewService } from '../../../pages/test-case-workspace/test-case-view/service/test-case-view.service';
import {
  KeywordStepView,
  TestStepState
} from '../../../pages/test-case-workspace/test-case-view/state/test-step.state';
import { DraggedKeywordStepsComponent } from '../dragged-steps/dragged-keyword-steps/dragged-keyword-steps.component';
import { DuplicateActionWord } from '../duplicate-action-words-dialog/duplicate-action-word.model';
import { DuplicateActionWordsDialogComponent } from '../duplicate-action-words-dialog/duplicate-action-words-dialog.component';
import { KEYWORD_TEST_STEP_VIEW_ORIGIN, KeywordTestStepDndData } from '../keyword-step-dnd-data';
import { testStepsLogger } from '../test-steps.logger';
import {Observable, Subject} from 'rxjs';

const logger = testStepsLogger.compose('KeywordStepListComponent');

@Component({
  selector: 'sqtm-app-keyword-step-list',
  templateUrl: './keyword-step-list.component.html',
  styleUrls: ['./keyword-step-list.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class KeywordStepListComponent implements OnInit {

  @Input()
  steps: KeywordStepView[];

  @Input()
  keywordTestCaseId: number;

  @Input()
  keywords: Option[];

  @Input()
  actionWordLibraryActive: boolean;

  @Input()
  editable = false;

  @Input()
  linkable = false;

  @ViewChild('actionField')
  actionField: AbstractActionWordFieldComponent;

  addKeywordStepFormGroup: FormGroup;

  private unsub$ = new Subject<void>();
  hasSelectedSteps$: Observable<boolean>;

  matchingActionWords: string[] = [];

  public readonly draggedStepComponent = DraggedKeywordStepsComponent;

  readonly origin = KEYWORD_TEST_STEP_VIEW_ORIGIN;

  readonly lastPositionDropZone = 'LAST_POSITION_DROP_ZONE';

  constructor(private cdr: ChangeDetectorRef,
              private vcr: ViewContainerRef,
              public testCaseViewService: TestCaseViewService,
              private formBuilder: FormBuilder,
              private errorService: ActionErrorDisplayService,
              private dialogService: DialogService) {
  }

  ngOnInit(): void {
    logger.debug('Initialize KeywordStepListComponent...');
    this.initAddKeywordStepFormGroup();

    this.hasSelectedSteps$ = this.testCaseViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => componentData.testCase.testSteps.selectedStepIds.length > 0)
    );
  }

  getDisplayedKeyword() {
    return this.keywords.map(keyword => {
      return { id: keyword.value, label: keyword.label };
    });
  }

  initAddKeywordStepFormGroup() {
    this.addKeywordStepFormGroup = this.formBuilder
      .group({
        'keyword': this.formBuilder.control('GIVEN'),
        'action': this.formBuilder.control('', [Validators.required])
      });
  }

  filterKeywordSteps() {
    return this.steps.filter(step => step.kind === 'keyword-step');
  }

  addStep(event: KeyboardEvent) {
    event.stopPropagation();
    this.actionField.blurInput();
    if (this.addKeywordStepFormGroup.valid) {
      if (this.actionWordLibraryActive) {
        this.createStepCheckingActionWordDuplicate();
      } else {
        this.createStep();
      }
    } else {
      this.dialogService.openAlert({
        titleKey: 'sqtm-core.generic.label.error',
        messageKey: 'sqtm-core.error.action-word.input.value'
      });
    }
  }

  private createStepCheckingActionWordDuplicate() {
    this.testCaseViewService.getDuplicateActionWords(
      this.addKeywordStepFormGroup.get('action').value
    ).pipe(
      map(result => Object.keys(result)
        .map((key: string) => ({ projectName: key, actionWordId: result[key] }))),
      catchError(error => this.errorService.handleActionError(error)),
    ).subscribe((duplicateActionWords: DuplicateActionWord[]) => {
      if (duplicateActionWords.length > 0) {
        const duplicateActionDialog = this.dialogService.openDialog(
          this.getDuplicateActionDialogConfiguration(duplicateActionWords));
        duplicateActionDialog.dialogClosed$.pipe(
          take(1),
        ).subscribe((selectedActionWordId: number) => {
          if (selectedActionWordId !== undefined) {
            this.createStep(selectedActionWordId);
          } else {
            this.actionField.focusInput();
          }
        });
      } else {
        this.createStep();
      }
    });
  }

  private getDuplicateActionDialogConfiguration(duplicateActionWords: DuplicateActionWord[]): DialogConfiguration {
    return {
      id: 'duplicate-action-word',
      component: DuplicateActionWordsDialogComponent,
      viewContainerReference: this.vcr,
      data: { duplicateActionWords },
      width: 600
    };
  }

  private createStep(actionWordId?: number) {
    this.testCaseViewService.confirmAddKeywordStep(this.extractKeywordStepForm(actionWordId), this.keywordTestCaseId)
      .pipe(
        catchError(error => this.errorService.handleActionError(error))
      ).subscribe(
        () => {
          this.addKeywordStepFormGroup.get('keyword').reset('GIVEN');
          this.addKeywordStepFormGroup.get('action').reset('');
          this.matchingActionWords = [];
          this.actionField.focusInput();
        });
  }

  extractKeywordStepForm(actionWordId?: number): KeywordStepFormModel {
    const keywordStepFormModel: KeywordStepFormModel = {
      keyword: this.addKeywordStepFormGroup.get('keyword').value,
      actionWord: this.addKeywordStepFormGroup.get('action').value
    };
    const lastSelectedIndex: number = this.getLastSelectedStepIndex();
    if (lastSelectedIndex !== undefined) {
      keywordStepFormModel.index = lastSelectedIndex + 1;
    }
    if (actionWordId !== undefined) {
      keywordStepFormModel.actionWordId = actionWordId;
    }
    return keywordStepFormModel;
  }

  private getLastSelectedStepIndex(): number {
    const selectedIndexes = this.steps.filter(step => step.selected).map(step => step.stepOrder);
    if (selectedIndexes.length > 0) {
      return Math.max(...selectedIndexes);
    }
  }

  trackStepFn(index: number, step: TestStepState) {
    return step.id;
  }

  getMatchingActionWords(searchInput: string) {
    this.testCaseViewService.getMatchingActionWords(searchInput)
      .pipe(take(1))
      .subscribe((matchingActionWord: string[]) => {
        this.matchingActionWords = matchingActionWord;
        this.cdr.detectChanges();
      });
  }

  /* Drag & Drop */

  dragStart($event: SqtmDragStartEvent) {
    const data = $event.dragAndDropData.data as KeywordTestStepDndData;
    const draggedStepIds = data.draggedSteps.map(step => step.id);
    this.testCaseViewService.startDraggingSteps(draggedStepIds);
  }

  dragOver($event: SqtmDragOverEvent) {
    // No dnd event handled if no write permission or if a step creation is in progress
    const eventOrigin = $event.dragAndDropData.origin;
    // console.log(`detecting over in step component ${$event.dndTarget.id}. origin : ${eventOrigin}`);
    if (eventOrigin === KEYWORD_TEST_STEP_VIEW_ORIGIN) {
      this.dragStepOver($event);
    }
  }

  private dragStepOver($event: SqtmDragOverEvent) {
    this.testCaseViewService.componentData$.pipe(
      take(1),
      filter(() => this.editable),
      filter((componentData: TestCaseViewComponentData) => $event.dndTarget.id !== componentData.testCase.testSteps.currentDndTargetId)
    ).subscribe(() => {
      if ($event.dndTarget.id === this.lastPositionDropZone) {
        this.testCaseViewService.dragOverStep(null);
      } else {
        this.testCaseViewService.dragOverStep($event.dndTarget.id as number);
      }
    });
  }

  drop($event: SqtmDropEvent) {
    if (this.editable && $event && $event.dragAndDropData) {
      const eventOrigin = $event.dragAndDropData.origin;
      if (eventOrigin === KEYWORD_TEST_STEP_VIEW_ORIGIN) {
        this.testCaseViewService.dropSteps();
      }
    }
  }

  dragLeave($event: SqtmDragLeaveEvent) {
    const eventOrigin = $event.dragAndDropData.origin;
    if (eventOrigin === this.origin) {
      this.testCaseViewService.suspendStepDrag();
    }
  }

  dragCancel() {
    logger.debug('Canceling dnd operation');
    this.testCaseViewService.cancelDrag();
  }

  showScriptPreview() {
    this.testCaseViewService.getScriptPreview()
      .pipe(take(1))
      .subscribe(script => {
        const dialogConfiguration: DialogConfiguration = {
          id: 'script-preview-dialog',
          component: ScriptPreviewDialogComponent,
          viewContainerReference: this.vcr,
          data: { content: script }
        };
        this.dialogService.openDialog(dialogConfiguration);
      });
  }

  deleteTestSteps() {
    const deletedStepIds: number[] = this.steps.filter(step => step.selected).map(step => step.id);

    const dialogReference: DialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.test-case-workspace.dialog.title.remove-test-step.plural',
      messageKey: 'sqtm-core.test-case-workspace.dialog.message.remove-test-step.plural'
    });
    dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      take(1)
    ).subscribe((confirm) => {
      if (confirm) {
        this.testCaseViewService.deleteStep(deletedStepIds[0]);
      }
    });
  }
}
