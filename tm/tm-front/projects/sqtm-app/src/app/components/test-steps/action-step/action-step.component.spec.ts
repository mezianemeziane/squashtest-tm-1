import {TestBed, waitForAsync} from '@angular/core/testing';

import {ActionStepComponent} from './action-step.component';
import {CustomField, WorkspaceCommonModule, InputType} from 'sqtm-core';
import {ChangeDetectorRef, NO_ERRORS_SCHEMA} from '@angular/core';
import {TestCaseViewService} from '../../../pages/test-case-workspace/test-case-view/service/test-case-view.service';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {TranslateModule} from '@ngx-translate/core';
import {ComponentTester, speculoosMatchers} from 'ngx-speculoos';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';

class ActionStepComponentTester extends ComponentTester<ActionStepComponent> {
  constructor() {
    super(ActionStepComponent);
  }

  get actionField() {
    return this.element('.action');
  }

  get resultField() {
    return this.element('.result');
  }

  get customFields() {
    return this.element('.custom-fields');
  }

  get coverageSection() {
    return this.element('.coverages');
  }

  get coverageRows() {
    return this.coverageSection.elements('tr');
  }

  getCoverageRowCells(index: number) {
    return this.coverageRows[index].elements('td');
  }

  get attachmentSection() {
    return this.element('.attachments');
  }

  // fixing detection changes for onPush() components
  detectChanges(checkNoChanges?: boolean): void {
    this.fixture.componentRef.injector.get(ChangeDetectorRef).detectChanges();
  }
}

describe('ActionStepComponentComponent', () => {
  let tester: ActionStepComponentTester;
  const testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ActionStepComponent],
      imports: [
        AppTestingUtilsModule,
        HttpClientTestingModule,
        WorkspaceCommonModule,
        NzDropDownModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
        NzToolTipModule
      ],
      providers: [
        {provide: TestCaseViewService, useValue: testCaseViewService}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    tester = new ActionStepComponentTester();
    tester.componentInstance.actionStep = {
      kind: 'action-step',
      id: 1,
      testCaseId: 1,
      attachmentList: {
        id: 1,
        attachments: {entities: {}, ids: []}
      },
      action: '<p>action</p>',
      expectedResult: '<p>expected result</p>',
      customFieldValues: {entities: {}, ids: []},
      coverages: [],
      projectId: 1,
      selected: false,
      stepOrder: 0,
      activeSelection: false,
      draggingStep: false,
      showPlaceHolder: false,
      extended: true,
      coverageDndTarget: false
    };
    tester.componentInstance.customFields = [];
    tester.componentInstance.editable = true;
    tester.detectChanges();
    jasmine.addMatchers(speculoosMatchers);
  });


  it('should show simple test step', () => {
    expect(tester.actionField).toHaveText('action');
    expect(tester.resultField).toHaveText('expected result');
    expect(tester.customFields).toBeFalsy();
    expect(tester.coverageSection).toBeFalsy();
    expect(tester.attachmentSection).toBeFalsy();
    tester.componentInstance.actionStep.action = 'changedAction';
    tester.detectChanges();
    expect(tester.actionField).toHaveText('changedAction');
    tester.componentInstance.actionStep.extended = false;
    tester.detectChanges();
    expect(tester.actionField).toBeFalsy();
    expect(tester.resultField).toBeFalsy();
  });


  it('should show coverages', () => {
    tester.componentInstance.actionStep.coverages = [
      {
        requirementVersionId: 1,
        criticality: 'CRITICAL',
        name: 'req-1',
        projectName: 'project-1',
        reference: 'A'
      },
      {
        requirementVersionId: 2,
        criticality: 'MAJOR',
        name: 'req-2',
        projectName: 'project-1',
        reference: 'B'
      }
    ];
    tester.detectChanges();

    expect(tester.coverageSection).toBeTruthy();
    const coverageRows = tester.coverageRows;
    expect(coverageRows.length).toEqual(2);
    const coverageRowCells = tester.getCoverageRowCells(0);
    expect(coverageRowCells[0]).toHaveText('project-1');
    expect(coverageRowCells[1]).toHaveText('A');
    expect(coverageRowCells[2]).toHaveText('req-1');
    expect(coverageRowCells[3].element('sqtm-core-i18n-enum-icon')).toBeTruthy();
    expect(coverageRowCells[4].element('.delete-coverage-icon')).toBeTruthy();

    tester.componentInstance.editable = false;
    tester.detectChanges();
    const deactivatedDeleteCell = tester.getCoverageRowCells(0)[4];
    expect(deactivatedDeleteCell).toBeFalsy();

  });

  it('should show custom fields', () => {

    const cuf1: CustomField = {id: 1, inputType: InputType.PLAIN_TEXT} as CustomField;
    const cuf2: CustomField = {id: 2, inputType: InputType.DATE_PICKER} as CustomField;
    const cuf3: CustomField = {id: 3, inputType: InputType.TAG} as CustomField;
    tester.componentInstance.customFields = [cuf1, cuf2, cuf3];
    tester.detectChanges();

    expect(tester.customFields).toBeTruthy();
    expect(tester.customFields.elements('sqtm-core-custom-field-widget').length).toEqual(3);
  });

});
