import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestStepListComponent} from './test-step-list.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestCaseViewService} from '../../../pages/test-case-workspace/test-case-view/service/test-case-view.service';
import {OverlayModule} from '@angular/cdk/overlay';
import {AppTestingUtilsModule} from '../../../utils/testing-utils/app-testing-utils.module';
import {EMPTY} from 'rxjs';

describe('TestStepListComponent', () => {
  let component: TestStepListComponent;
  let fixture: ComponentFixture<TestStepListComponent>;

  const testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load', 'updateStepViewScroll']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TestStepListComponent],
      imports: [AppTestingUtilsModule, OverlayModule],
      providers: [
        {
          provide: TestCaseViewService,
          useValue: {...testCaseViewService, componentData$: EMPTY}
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestStepListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
