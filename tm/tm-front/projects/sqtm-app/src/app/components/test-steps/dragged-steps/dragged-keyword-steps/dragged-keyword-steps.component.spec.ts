import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DRAG_AND_DROP_DATA } from 'sqtm-core';
import { AppTestingUtilsModule } from '../../../../utils/testing-utils/app-testing-utils.module';

import { DraggedKeywordStepsComponent } from './dragged-keyword-steps.component';

describe('DraggedKeywordStepsComponent', () => {
  let component: DraggedKeywordStepsComponent;
  let fixture: ComponentFixture<DraggedKeywordStepsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ AppTestingUtilsModule ],
      declarations: [ DraggedKeywordStepsComponent ],
      providers: [
        { provide: DRAG_AND_DROP_DATA,
          useValue: {
            data: {}
          }
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DraggedKeywordStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
