import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component, ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild
} from '@angular/core';
import {ActionStepView, TestStepView} from '../../../pages/test-case-workspace/test-case-view/state/test-step.state';
import {TestCaseViewService} from '../../../pages/test-case-workspace/test-case-view/service/test-case-view.service';
import {CustomField, DraggableItemHandlerDirective, DraggableListItemDirective} from 'sqtm-core';
import {fromEvent, Subject} from 'rxjs';
import {switchMap, take, takeUntil} from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import {TestCaseViewComponentData} from '../../../pages/test-case-workspace/test-case-view/containers/test-case-view/test-case-view.component';
import {TestStepDndData} from '../test-step-dnd-data';
import {ActionStepComponent} from '../action-step/action-step.component';
import {CreateStepFormComponent} from '../create-step-form/create-step-form.component';

@Component({
  selector: 'sqtm-app-test-step',
  templateUrl: './test-step.component.html',
  styleUrls: ['./test-step.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestStepComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input()
  step: TestStepView;

  @Input()
  linkable = false;

  @ViewChild('dropFileZone', {read: ElementRef})
  private dropFileZone: ElementRef;

  private _customFields: CustomField[];

  get customFields(): CustomField[] {
    return this._customFields;
  }

  @Input()
  set customFields(value: CustomField[]) {
    // Setting cuf bindings only one time else it will instantiate dynamic component at each state emission !
    if (!Boolean(this._customFields)) {
      this._customFields = value;
    }
  }

  @Input()
  initialFraction: number;

  @Input()
  editable = false;

  @Input()
  stepCreationMode = false;

  @Input()
  stepCount = 0;

  @ViewChild(DraggableListItemDirective, {static: true})
  draggableListItem: DraggableListItemDirective;

  @ViewChild(DraggableItemHandlerDirective, {static: true})
  dragHandler: DraggableItemHandlerDirective;

  @ViewChild(ActionStepComponent)
  actionStep: ActionStepComponent;

  @ViewChild(CreateStepFormComponent)
  createStepFormComponent: CreateStepFormComponent;

  @Output()
  resizeActionStepColumn = new EventEmitter<number>();

  private draggedSteps = new Subject<TestStepDndData>();

  private unsub$ = new Subject<void>();

  constructor(private testCaseViewService: TestCaseViewService, private renderer: Renderer2) {
  }

  ngOnInit() {
    this.draggableListItem.connectDataSource(this.draggedSteps);
  }

  ngAfterViewInit(): void {
    // fixing dnd data when user mouse down on the drag handler in case of it could be a dnd.
    // we cannot just react to the dnd start, as data must be fixed at dnd init
    fromEvent(this.dragHandler.host.nativeElement, 'mousedown').pipe(
      takeUntil(this.unsub$),
      switchMap(() => this.testCaseViewService.componentData$.pipe(take(1)))
    ).subscribe((componentData: TestCaseViewComponentData) => {
      const testSteps = componentData.testCase.testSteps;
      const selectedStepIds = testSteps.selectedStepIds;
      let draggedStepIds = selectedStepIds;
      if (!selectedStepIds.includes(this.step.id)) {
        draggedStepIds = [this.step.id];
      }
      const draggedSteps = draggedStepIds.map(id => testSteps.entities[id]);
      this.draggedSteps.next(new TestStepDndData(draggedSteps));
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  addAttachment(files: File[], step: TestStepView) {
    if (step.kind === 'action-step') {
      const actionStep = step as ActionStepView;
      if (this.editable) {
        this.testCaseViewService.addAttachmentsToStep(files, actionStep.id, actionStep.attachmentList.id);
      }
      this.unmarkAsFileDropZone();
    }
  }

  showPlaceHolder(step: TestStepView) {
    // we show the placeholder if :
    // - dnd is running and user has hover over at least one step
    // - the step is selected OR the step is the dummy placeholder step used for dnd from test-case test to materialize call step.
    return step.showPlaceHolder && (step.selected || step.kind === 'dnd-call-step-placeholder');
  }

  showAsActionStep(step: TestStepView) {
    if (this.showPlaceHolder(step)) {
      return false;
    }
    return step.kind === 'action-step';
  }

  showAsCallStep(step: TestStepView) {
    if (this.showPlaceHolder(step)) {
      return false;
    }
    return step.kind === 'call-step';
  }

  disableControls(step: TestStepView, creationMode: boolean) {
    if (!step.selected && step.activeSelection) {
      return true;
    } else {
      return creationMode;
    }
  }

  selectStep($event: MouseEvent, id: number) {
    if ($event.ctrlKey) {
      this.testCaseViewService.toggleStepSelection(id);
    } else if ($event.shiftKey) {
      this.testCaseViewService.extendStepSelection(id);
    } else {
      this.testCaseViewService.selectStep(id);
    }
  }

  forwardFractionChangeToList(fraction: number) {
    this.resizeActionStepColumn.emit(fraction);
  }

  setFaction(fraction: number) {
    if (this.actionStep) {
      this.actionStep.setFaction(fraction);
    } else if (this.createStepFormComponent) {
      this.createStepFormComponent.setFaction(fraction);
    }
  }

  moveStepUp(step: TestStepView) {
    this.testCaseViewService.moveStepUp(step.id);
  }

  moveStepDown(step: TestStepView) {
    this.testCaseViewService.moveStepDown(step.id);
  }

  showRequirementIsOver(step: TestStepView) {
    return this.step.kind === 'action-step' && (step as ActionStepView).coverageDndTarget;
  }

  handleAttachmentEnter() {
    if (this.step.kind === 'action-step') {
      this.markAsFileDropZone();
    }
  }

  handleAttachmentLeave() {
    if (this.step.kind === 'action-step') {
      this.unmarkAsFileDropZone();
    }
  }

  private unmarkAsFileDropZone() {
    this.renderer.removeClass(this.dropFileZone.nativeElement, 'file-is-over');
  }

  private markAsFileDropZone() {
    this.renderer.addClass(this.dropFileZone.nativeElement, 'file-is-over');
  }
}
