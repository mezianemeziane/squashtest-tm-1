import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { DialogReference } from 'sqtm-core';
import { DuplicateActionWordDialogConfiguration } from './duplicate-action-word-dialog-configuration';

@Component({
  selector: 'sqtm-app-duplicate-action-words-dialog',
  templateUrl: './duplicate-action-words-dialog.component.html',
  styleUrls: ['./duplicate-action-words-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DuplicateActionWordsDialogComponent implements OnInit {

  selectedActionWordId: number;

  configuration: DuplicateActionWordDialogConfiguration;

  constructor(private dialogReference: DialogReference<DuplicateActionWordDialogConfiguration>) {
  }

  ngOnInit(): void {
    this.configuration = this.dialogReference.data;
    this.selectedActionWordId = this.configuration.duplicateActionWords[0].actionWordId;
  }

  handleConfirm() {
    this.dialogReference.result = this.selectedActionWordId;
    this.dialogReference.close();
  }
}
