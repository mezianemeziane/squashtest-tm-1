import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { DRAG_AND_DROP_DATA, DragAndDropData, Option } from 'sqtm-core';
import { KeywordTestStepDndData } from '../../keyword-step-dnd-data';
import { DraggedStepsComponent } from '../dragged-steps.component';

@Component({
  selector: 'sqtm-app-dragged-keyword-steps',
  templateUrl: './dragged-keyword-steps.component.html',
  styleUrls: ['./dragged-keyword-steps.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DraggedKeywordStepsComponent extends DraggedStepsComponent implements OnInit {

  public keywords: Option[];

  constructor(@Inject(DRAG_AND_DROP_DATA) public dragAnDropData: DragAndDropData) {
    super(dragAnDropData);
    this.keywords = (dragAnDropData.data as KeywordTestStepDndData).keywords;
  }

  ngOnInit(): void {
  }

  getOptionLabel(key: string) {
    const optionFound = this.keywords.find(option => option.value === key);
    if (Boolean(optionFound)) {
      return optionFound.label;
    } else {
      throw Error(`Unable to find Keyword ${key}.`);
    }
  }

}
