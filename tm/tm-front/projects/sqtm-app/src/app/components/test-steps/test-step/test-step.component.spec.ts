import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TestStepComponent} from './test-step.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {SqtmDragAndDropModule, DraggableListDirective} from 'sqtm-core';
import {TestCaseViewService} from '../../../pages/test-case-workspace/test-case-view/service/test-case-view.service';
import {ActionStepView} from '../../../pages/test-case-workspace/test-case-view/state/test-step.state';
import {OverlayModule} from '@angular/cdk/overlay';
import {AppTestingUtilsModule} from '../../../utils/testing-utils/app-testing-utils.module';

describe('TestStepComponent', () => {
  let component: TestStepComponent;
  let fixture: ComponentFixture<TestStepComponent>;

  const testCaseViewService = jasmine.createSpyObj('testCaseViewService', ['load']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, SqtmDragAndDropModule, OverlayModule],
      declarations: [TestStepComponent],
      providers: [
        {provide: TestCaseViewService, useValue: testCaseViewService},
        {provide: DraggableListDirective, useValue: {}},
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestStepComponent);
    component = fixture.componentInstance;
    component.step = {id: 1, kind: 'action-step'} as ActionStepView;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
