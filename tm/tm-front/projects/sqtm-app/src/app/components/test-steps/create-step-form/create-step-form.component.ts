import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {
  ActionErrorDisplayService,
  ActionStepFormModel,
  convertFormControlValues,
  CustomField,
  CustomFieldFormComponent,
  RichTextFieldComponent,
} from 'sqtm-core';
import {TestCaseViewService} from '../../../pages/test-case-workspace/test-case-view/service/test-case-view.service';
import {CreateActionStepFormView} from '../../../pages/test-case-workspace/test-case-view/state/test-step.state';
import {catchError} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-create-step-form',
  templateUrl: './create-step-form.component.html',
  styleUrls: ['./create-step-form.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateStepFormComponent implements OnInit, AfterViewInit {

  @Input()
  step: CreateActionStepFormView;

  @Input()
  customFields: CustomField[];

  @Input()
  initialFraction: number;

  @Input()
  stepCount = 0;

  @Output()
  resizeActionStepColumn = new EventEmitter<number>();

  @ViewChild('grid', {read: ElementRef, static: true})
  grid: ElementRef;

  @ViewChild('header', {read: ElementRef, static: true})
  headerAction: ElementRef;

  @ViewChild(CustomFieldFormComponent, {static: true})
  customFieldFormComponent: CustomFieldFormComponent;

  @ViewChild('actionField')
  actionField: RichTextFieldComponent;

  actionColumnWidth = 0;

  fraction = 1.3;
  actionStepFormGroup: FormGroup;

  constructor(private renderer: Renderer2,
              private formBuilder: FormBuilder,
              private translateService: TranslateService,
              private testCaseViewService: TestCaseViewService,
              private actionErrorDisplayService: ActionErrorDisplayService
  ) {
    this.actionStepFormGroup = this.formBuilder.group({
      action: new FormControl(''),
      expectedResult: new FormControl('')
    });
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.actionColumnWidth = (this.headerAction.nativeElement as HTMLDivElement).clientWidth;
    if (this.initialFraction) {
      this.fraction = this.initialFraction;
      this.setFaction(this.initialFraction);
    }
  }

  handleResize($event: number) {
    const gridWidth = (this.grid.nativeElement as HTMLDivElement).clientWidth;
    const divideFactor = gridWidth / 4;
    const nextFraction: number = this.fraction * (1 + ($event / divideFactor));
    if (nextFraction > 0.25 && nextFraction < 4) {
      this.resizeActionStepColumn.emit(nextFraction);
    }
  }

  setFaction(nextFraction: number) {
    this.fraction = nextFraction;
    this.renderer.setStyle(this.grid.nativeElement, 'grid-template-columns', `${this.fraction}fr 1fr 120px`);
  }

  addStep() {
    if (this.isFormValid()) {
      this.doAddStep();
    } else {
      this.customFieldFormComponent.showClientSideErrors();
    }
  }

  private doAddStep() {
    const actionStepFormModel = this.getFormValue();
    this.testCaseViewService.confirmAddActionStep(actionStepFormModel, this.step.testCaseId).pipe(
      catchError(err => this.actionErrorDisplayService.handleActionError(err))
    ).subscribe();
  }

  private isFormValid() {
    return this.actionStepFormGroup.status === 'VALID';
  }

  private getFormValue() {
    const customFieldsControls = this.actionStepFormGroup.controls['customFields'] as FormGroup;
    const rawValueMap = convertFormControlValues(this.customFields, customFieldsControls);
    const actionStepFormModel: ActionStepFormModel = {
      action: this.actionStepFormGroup.controls['action'].value,
      expectedResult: this.actionStepFormGroup.controls['expectedResult'].value,
      index: this.step.stepOrder,
      customFields: rawValueMap
    };
    return actionStepFormModel;
  }

  addAnotherStep() {
    if (this.isFormValid()) {
      this.doAddAnotherStep();
    } else {
      this.customFieldFormComponent.showClientSideErrors();
    }
  }

  private doAddAnotherStep() {
    const actionStepFormModel = this.getFormValue();
    this.refreshRichTextFieldFocus();
    this.resetForm();
    this.testCaseViewService.confirmAddAnotherActionStep(actionStepFormModel, this.step.testCaseId).pipe(
      catchError(err => this.actionErrorDisplayService.handleActionError(err))
    ).subscribe();
  }

  private refreshRichTextFieldFocus() {
    this.actionField.grabFocus();
  }

// This method is needed because Angular recycle the view when add another is triggered.
  private resetForm() {
    this.actionStepFormGroup.controls['action'].reset();
    this.actionStepFormGroup.controls['expectedResult'].reset();
    this.customFieldFormComponent.resetToDefaultValues();
  }

  cancel() {
    this.testCaseViewService.cancelAddTestStep();
  }
}
