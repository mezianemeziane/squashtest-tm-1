import { Option } from 'sqtm-core';
import { TestStepState } from '../../pages/test-case-workspace/test-case-view/state/test-step.state';
import { TestStepDndData } from './test-step-dnd-data';

export const KEYWORD_TEST_STEP_VIEW_ORIGIN = 'KEYWORD_TEST_STEP_VIEW_ORIGIN';

export class KeywordTestStepDndData extends TestStepDndData {
  constructor(
    public readonly draggedSteps: TestStepState[],
    public readonly keywords: Option[]) {
    super(draggedSteps);
  }
}
