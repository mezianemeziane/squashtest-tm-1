import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {TestStepState, TestStepView} from '../../../pages/test-case-workspace/test-case-view/state/test-step.state';
import {
  ActionErrorDisplayService, CopierService,
  createTestCaseCoverageMessageDialogConfiguration,
  CustomField,
  CustomFieldBindingData, DialogReference,
  DialogService,
  EditableRichTextComponent,
  GridDndData,
  parseDataRowId,
  REQUIREMENT_TREE_PICKER_ID,
  shouldShowCoverageMessageDialog,
  SqtmDragLeaveEvent,
  SqtmDragOverEvent,
  SqtmDragStartEvent,
  SqtmDropEvent,
  SquashTmDataRowType,
  TEST_CASE_TREE_PICKER_ID
} from 'sqtm-core';
import {TestCaseViewService} from '../../../pages/test-case-workspace/test-case-view/service/test-case-view.service';
import {fromEvent, Observable, Subject} from 'rxjs';
import {DOCUMENT} from '@angular/common';
import {TEST_STEP_VIEW_ORIGIN, TestStepDndData} from '../test-step-dnd-data';
import {testCaseWorkspaceTreeId} from '../../../pages/test-case-workspace/test-case-workspace.constant';
import {catchError, filter, map, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import {TestCaseViewComponentData} from '../../../pages/test-case-workspace/test-case-view/containers/test-case-view/test-case-view.component';
import {TestStepComponent} from '../test-step/test-step.component';
import {
  callStepDndPlaceholderId,
  createActionStepFormId
} from '../../../pages/test-case-workspace/test-case-view/test-case-view.constant';
import {DraggedStepsComponent} from '../dragged-steps/dragged-steps.component';

/** @dynamic */
@Component({
  selector: 'sqtm-app-test-step-list',
  templateUrl: './test-step-list.component.html',
  styleUrls: ['./test-step-list.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestStepListComponent implements OnInit, OnDestroy, AfterViewInit {

  private ERROR_LABEL = 'sqtm-core.generic.label.error';

  @Input()
  steps: TestStepView[];

  @Input()
  draggingStep: boolean;

  @Input()
  draggingTestCase: boolean;

  @Input()
  editable = false;

  @Input()
  linkable = false;

  @Input()
  stepCreationMode = false;

  @Input()
  stepCount = 0;

  @Input()
  extendedPrerequisite: boolean;

  @Input()
  prerequisite: string;

  @Input()
  initialScrollTop: number;

  public readonly draggedStepComponent = DraggedStepsComponent;

  private _customFieldBindingData: CustomFieldBindingData[];

  public fraction = 1.3;

  @ViewChild('scrollableStepList', {read: ElementRef, static: true})
  private scrollableStepList: ElementRef;

  private scrollTop = 0;

  get customFields(): CustomField[] {
    return this._customFieldBindingData.map(cufBindingData => cufBindingData.customField);
  }

  @Input()
  set customFieldBindingData(value: CustomFieldBindingData[]) {
    // Setting cuf bindings only one time else it will instantiate dynamic component at each state emission !
    // cuf bindings will not change during the lifespan of a test case view...
    if (!Boolean(this._customFieldBindingData)) {
      this._customFieldBindingData = value;
    }
  }

  @ViewChildren(TestStepComponent)
  testSteps: QueryList<TestStepComponent>;

  @ViewChild('prerequisiteField', {read: EditableRichTextComponent})
  prerequisiteField: EditableRichTextComponent;

  private unsub$ = new Subject<void>();

  hasCopiedSteps$: Observable<boolean>;
  selectedStepIds$: Observable<number[]>;
  hasSelectedSteps$: Observable<boolean>;
  canPaste$: Observable<boolean>;

  readonly origin = TEST_STEP_VIEW_ORIGIN;
  readonly lastPositionDropZone = 'LAST_POSITION_DROP_ZONE';

  constructor(
    private testCaseViewService: TestCaseViewService,
    @Inject(DOCUMENT) private document: Document,
    private ngZone: NgZone,
    private dialogService: DialogService,
    private actionErrorDisplayService: ActionErrorDisplayService,
    private copierService: CopierService) {
    this.initHasCopiedSteps();
  }

  initHasCopiedSteps() {
    this.hasCopiedSteps$ = this.copierService.copiedTestStep$.pipe(
      takeUntil((this.unsub$)),
      map(ids => ids?.length > 0)
    );
  }

  ngOnInit() {
    this.selectedStepIds$ = this.testCaseViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => componentData.testCase.testSteps.selectedStepIds)
    );

    this.hasSelectedSteps$ = this.selectedStepIds$.pipe(
      takeUntil(this.unsub$),
      map(stepIds => stepIds.length > 0 && !stepIds.includes(createActionStepFormId)),
    );

    this.canPaste$ = this.selectedStepIds$.pipe(
      takeUntil(this.unsub$),
      map(stepIds => stepIds.length < 2),
      withLatestFrom(this.hasCopiedSteps$),
      map(([hasOneRowSelected, hasCopiedSteps]: [boolean, boolean]) => hasOneRowSelected && hasCopiedSteps && this.editable)
    );
  }

  ngAfterViewInit() {
    this.ngZone.runOutsideAngular(() => {
      const scrollableDiv = this.scrollableStepList.nativeElement as HTMLDivElement;
      if (this.initialScrollTop) {
        scrollableDiv.scroll({top: this.initialScrollTop});
      }

      fromEvent(this.scrollableStepList.nativeElement, 'scroll')
        .subscribe(($event: MouseEvent) => {
          this.scrollTop = $event.target['scrollTop'];
        });
    });
  }


  ngOnDestroy(): void {
    this.testCaseViewService.updateStepViewScroll(this.scrollTop);
    this.unsub$.next();
    this.unsub$.complete();
  }

  trackStepFn(index: number, step: TestStepState) {
    return step.id;
  }

  resizeActionGrid(fraction: number) {
    this.fraction = fraction;
    this.testSteps.forEach(actionStepComponent => actionStepComponent.setFaction(fraction));
  }

  dragStart($event: SqtmDragStartEvent) {
    const data = $event.dragAndDropData.data as TestStepDndData;
    const draggedStepIds = data.draggedSteps.map(step => step.id);
    this.testCaseViewService.startDraggingSteps(draggedStepIds);
  }

  dragOver($event: SqtmDragOverEvent) {
    // No dnd event handled if no write permission or if a step creation is in progress
    const eventOrigin = $event.dragAndDropData.origin;
    // console.log(`detecting over in step component ${$event.dndTarget.id}. origin : ${eventOrigin}`);
    if (eventOrigin === TEST_STEP_VIEW_ORIGIN) {
      this.dragStepOver($event);
    } else if (eventOrigin === testCaseWorkspaceTreeId) {
      this.dragTestCaseOverStep($event);
    } else if (eventOrigin === REQUIREMENT_TREE_PICKER_ID) {
      this.dragRequirementOverStep($event);
    } else if (eventOrigin === TEST_CASE_TREE_PICKER_ID) {
      this.dragTestCaseOverStep($event);
    }
  }

  private dragTestCaseOverStep($event: SqtmDragOverEvent) {
    this.testCaseViewService.componentData$.pipe(
      take(1),
      filter(() => this.dragTestCaseAuthorized($event)),
    ).subscribe(() => {
      this.ngZone.run(() => {
        if ($event.dndTarget.id === this.lastPositionDropZone) {
          this.testCaseViewService.dragOverStepForCall(null);
        } else {
          this.testCaseViewService.dragOverStepForCall($event.dndTarget.id as number);
        }
      });
    });
  }

  private dragStepOver($event: SqtmDragOverEvent) {
    this.testCaseViewService.componentData$.pipe(
      take(1),
      filter(() => this.editable),
      filter((componentData: TestCaseViewComponentData) => $event.dndTarget.id !== componentData.testCase.testSteps.currentDndTargetId)
    ).subscribe(() => {
      if ($event.dndTarget.id === this.lastPositionDropZone) {
        this.testCaseViewService.dragOverStep(null);
      } else {
        this.testCaseViewService.dragOverStep($event.dndTarget.id as number);
      }
    });
  }

  drop($event: SqtmDropEvent) {
    if (this.editable && $event && $event.dragAndDropData) {
      const eventOrigin = $event.dragAndDropData.origin;
      if (eventOrigin === TEST_STEP_VIEW_ORIGIN) {
        this.testCaseViewService.dropSteps();
      } else if (eventOrigin === testCaseWorkspaceTreeId) {
        this.dropTestCaseForCall($event);
      } else if (eventOrigin === REQUIREMENT_TREE_PICKER_ID) {
        this.dropRequirementForCoverage($event);
      } else if (eventOrigin === TEST_CASE_TREE_PICKER_ID) {
        this.dropTestCaseForCall($event);
      }
    }
  }

  private dropRequirementForCoverage($event: SqtmDropEvent) {
    const data = $event.dragAndDropData.data as GridDndData;
    const dataRows = data.dataRows;
    const requirementIds: number[] = dataRows
      .filter(row => row.type === SquashTmDataRowType.Requirement || row.type === SquashTmDataRowType.RequirementFolder)
      .map(row => parseDataRowId(row));
    if (requirementIds.length > 0) {
      this.testCaseViewService.dropRequirementForCoverageInStep(requirementIds).pipe(take(1)).subscribe(operationReport => {
        if (shouldShowCoverageMessageDialog(operationReport)) {
          this.dialogService.openDialog(createTestCaseCoverageMessageDialogConfiguration(operationReport));
        }
      });
    }
  }

  private dropTestCaseForCall($event: SqtmDropEvent) {
    const data = $event.dragAndDropData.data as GridDndData;
    const dataRows = data.dataRows;
    const calledTestCaseIds: number[] = dataRows
      .filter(row => row.type === SquashTmDataRowType.TestCase || row.type === SquashTmDataRowType.TestCaseFolder)
      .map(row => parseDataRowId(row));

    if (calledTestCaseIds.length > 0) {
      this.testCaseViewService.dropTestCaseForCall(calledTestCaseIds).pipe(
        catchError(err => {
          this.testCaseViewService.cancelDrag();
          return this.actionErrorDisplayService.handleActionError(err);
        })).subscribe();
    }
  }

  dragLeave($event: SqtmDragLeaveEvent) {
    const eventOrigin = $event.dragAndDropData.origin;
    if (eventOrigin === testCaseWorkspaceTreeId) {
      this.testCaseViewService.suspendTestCaseDrag();
    } else if (eventOrigin === this.origin) {
      this.testCaseViewService.suspendStepDrag();
    }
  }

  dragCancel() {
    this.testCaseViewService.cancelDrag();
  }

  collapseAllSteps() {
    this.testCaseViewService.collapseAllSteps();
  }

  expendAllSteps() {
    this.testCaseViewService.expendAllSteps();
  }

  private dragTestCaseAuthorized($event: SqtmDragOverEvent): boolean {
    let authorized = true;
    if (!this.editable) {
      authorized = false;
    } else if ($event.dndTarget.id === callStepDndPlaceholderId) {
      authorized = false;
    } else if (this.stepCreationMode && $event.dndTarget.id !== this.lastPositionDropZone) {
      authorized = false;
    }
    return authorized;
  }

  togglePrerequisite() {
    this.testCaseViewService.togglePrerequisite();
  }

  editCollapsedPrerequisite() {
    this.testCaseViewService.togglePrerequisite();
    // setTimeout is required to allow ckEditor init after *ngIf is triggered by state change
    if (this.editable) {
      setTimeout(() => {
        this.prerequisiteField.enableEditMode();
      });
    }
  }

  private dragRequirementOverStep($event: SqtmDragOverEvent) {
    if (this.editable && !this.stepCreationMode) {
      this.testCaseViewService.dragRequirementOverStep($event.dndTarget.id as number);
    }
  }

  changePrerequisite(prerequisite: string) {
    this.testCaseViewService.changePrerequisite(prerequisite).pipe(
      take(1),
      catchError(err => {
        this.prerequisiteField.endAsync();
        return this.actionErrorDisplayService.handleActionError(err);
      })
    ).subscribe();
  }

  shouldShowStepList(): boolean {
    // If i have only one step, that is the creation form and i don't have rights to edit, hide the whole list...
    return !(this.hasOnlyCreationForm() && !this.editable);
  }

  private hasOnlyCreationForm() {
    return this.stepCreationMode && this.stepCount === 0;
  }

  copyTestSteps() {
    const copiedStepIds: number[] = this.steps.filter(step => step.selected).map(step => step.id);
    this.testCaseViewService.copySteps(copiedStepIds[0]);
  }

  pasteTestSteps() {
    const targetSteps = this.steps.filter(step => step.selected);
    const targetStepId = targetSteps.length === 0 ? null :
      targetSteps[0].id ===  createActionStepFormId ? null : targetSteps[0].id;
    const testCaseId = this.steps[0].testCaseId;
    this.testCaseViewService.pasteSteps(targetStepId, testCaseId);
  }

  deleteTestSteps() {
    const deletedStepIds: number[] = this.steps.filter(step => step.selected).map(step => step.id);

    const dialogReference: DialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.test-case-workspace.dialog.title.remove-test-step.plural',
      messageKey: 'sqtm-core.test-case-workspace.dialog.message.remove-test-step.plural'
    });
    dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      take(1)
    ).subscribe((confirm) => {
      if (confirm) {
        this.testCaseViewService.deleteStep(deletedStepIds[0]);
      }
    });
  }
}
