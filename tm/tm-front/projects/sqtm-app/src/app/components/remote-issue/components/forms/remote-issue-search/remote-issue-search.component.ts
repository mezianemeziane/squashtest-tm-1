import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'sqtm-app-remote-issue-search',
  templateUrl: './remote-issue-search.component.html',
  styleUrls: [
    './remote-issue-search.component.less',
    '../../../styles/remote-issue-dialog-commons.less',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoteIssueSearchComponent {

  searchText: string;

  loading: boolean;

  @Input()
  errorMessage: string;

  @Output()
  searchRequested = new EventEmitter<string>();

  constructor(private readonly cdr: ChangeDetectorRef) {
  }

  searchForIssue(): void {
    this.loading = true;
    this.searchRequested.emit(this.searchText);
  }

  endAsync(): void {
    this.loading = false;
    this.cdr.detectChanges();
  }
}
