import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoteIssueSearchComponent } from './remote-issue-search.component';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('RemoteIssueSearchComponent', () => {
  let component: RemoteIssueSearchComponent;
  let fixture: ComponentFixture<RemoteIssueSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [ RemoteIssueSearchComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteIssueSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
