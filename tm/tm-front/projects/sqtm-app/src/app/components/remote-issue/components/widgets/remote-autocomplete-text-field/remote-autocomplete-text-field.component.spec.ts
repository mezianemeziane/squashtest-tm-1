import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {RemoteAutocompleteTextFieldComponent} from './remote-autocomplete-text-field.component';
import {RemoteIssueService} from '../../../services/remote-issue.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {Field, FieldValue} from 'sqtm-core';
import SpyObj = jasmine.SpyObj;
import {of} from 'rxjs';
import {FormBuilder} from '@angular/forms';

describe('RemoteAutocompleteTextFieldComponent', () => {
  let component: RemoteAutocompleteTextFieldComponent;
  let fixture: ComponentFixture<RemoteAutocompleteTextFieldComponent>;
  let remoteIssueService: SpyObj<RemoteIssueService>;

  beforeEach(async () => {
    remoteIssueService = jasmine.createSpyObj(['sendDelegateCommand']);

    await TestBed.configureTestingModule({
      declarations: [RemoteAutocompleteTextFieldComponent],
      providers: [
        {provide: RemoteIssueService, useValue: remoteIssueService},
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteAutocompleteTextFieldComponent);
    component = fixture.componentInstance;
    component.formGroup = new FormBuilder().group({ field: '' });
    component.fieldName = 'field';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should send delegate command', () => {
    remoteIssueService.sendDelegateCommand.and.returnValue(of(assigneeAutocompleteReturn()));
    component.field = mockAssigneeField();

    component.onInput();
    expect(remoteIssueService.sendDelegateCommand).toHaveBeenCalledWith('assignee:', '');
    expect(component.visibleOptionLabels).toEqual(['A', 'B', 'C']);
  });

  it('should reset field', () => {
    spyOn(component.textInput.nativeElement, 'focus');
    expect(component.showClearButton).toBeFalse();

    component.formControl.setValue('w');
    expect(component.showClearButton).toBeTrue();

    component.clearInputAndShowList();
    expect(component.showClearButton).toBeFalse();
    expect(component.textInput.nativeElement.focus).toHaveBeenCalled();
  });

  it('should call onInput callback when text input value changes', fakeAsync(() => {
    spyOn(component, 'onInput');
    dispatchFakeInputEvent(component.textInput.nativeElement, 'w');

    tick(200);  // Simulate passage of time because event is debounced

    expect(component.onInput).toHaveBeenCalled();
  }));
});

function mockAssigneeField(): Field {
  return {
    label: 'Assignee',
    id: 'assignee',
    possibleValues: [],
    rendering: {
      required: false,
      operations: [],
      inputType: {
        configuration: {
          onchange: 'assignee:'
        },
        original: '',
        fieldSchemeSelector: false,
        dataType: 'string',
        name: 'assignee'
      },
    },
  };
}

function assigneeAutocompleteReturn(): FieldValue {
  return {
    id: '--',
    name: '--',
    typename: '',
    composite: [
      { name: 'A' } as FieldValue,
      { name: 'B' } as FieldValue,
      { name: 'C' } as FieldValue,
    ],
    scalar: '--',
    custom: null,
  };
}

function dispatchFakeInputEvent(nativeElement: HTMLInputElement, newValue: string): void {
  nativeElement.value = newValue;
  nativeElement.dispatchEvent(new Event('input', {bubbles: true, cancelable: true }));
}
