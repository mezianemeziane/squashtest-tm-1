import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';

import {NzAutocompleteModule} from 'ng-zorro-antd/auto-complete';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzRadioModule} from 'ng-zorro-antd/radio';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';

import {DialogModule, WorkspaceCommonModule} from 'sqtm-core';

import {MultiSelectFieldComponent} from './components/widgets/multi-select-field/multi-select-field.component';
import {NewAdvancedIssueFormComponent} from './components/forms/new-advanced-issue-form/new-advanced-issue-form.component';
import {NewOslcIssueDialogComponent} from './components/forms/new-oslc-issue-dialog/new-oslc-issue-dialog.component';
import {NewRemoteIssueFormComponent} from './components/forms/new-remote-issue-form/new-remote-issue-form.component';
import {RemoteAttachmentFieldComponent} from './components/widgets/remote-attachment-field/remote-attachment-field.component';
import {RemoteAutocompleteTextFieldComponent} from './components/widgets/remote-autocomplete-text-field/remote-autocomplete-text-field.component';
import {RemoteIssueDialogComponent} from './containers/remote-issue-dialog/remote-issue-dialog.component';
import {RemoteIssueSearchComponent} from './components/forms/remote-issue-search/remote-issue-search.component';
// tslint:disable-next-line:max-line-length
// import {CascadingSelectFieldComponent} from '../../../../../sqtm-core/src/lib/ui/workspace-common/components/forms/cascading-select-field/cascading-select-field.component';

@NgModule({
  declarations: [
    MultiSelectFieldComponent,
    NewAdvancedIssueFormComponent,
    NewOslcIssueDialogComponent,
    NewRemoteIssueFormComponent,
    RemoteAttachmentFieldComponent,
    RemoteAutocompleteTextFieldComponent,
    RemoteIssueDialogComponent,
    RemoteIssueSearchComponent,
    // CascadingSelectFieldComponent
  ],
  imports: [
    CommonModule,
    DialogModule,
    FormsModule,
    NzAutocompleteModule,
    NzButtonModule,
    NzCheckboxModule,
    NzIconModule,
    NzInputModule,
    NzRadioModule,
    NzSelectModule,
    NzToolTipModule,
    ReactiveFormsModule,
    TranslateModule,
    WorkspaceCommonModule,
  ],
  exports: [RemoteIssueDialogComponent],
})
export class RemoteIssueModule {
}
