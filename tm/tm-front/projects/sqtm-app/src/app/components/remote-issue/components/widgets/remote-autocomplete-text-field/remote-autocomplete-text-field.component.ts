import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, ViewChild} from '@angular/core';
import {AbstractFormField, Field, FieldValidationError, FieldValue} from 'sqtm-core';
import {FormGroup} from '@angular/forms';
import {RemoteIssueService} from '../../../services/remote-issue.service';
import {fromEvent} from 'rxjs';
import {auditTime, distinctUntilChanged} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-remote-autocomplete-text-field',
  templateUrl: './remote-autocomplete-text-field.component.html',
  styleUrls: ['./remote-autocomplete-text-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoteAutocompleteTextFieldComponent extends AbstractFormField {
  @Input() field: Field;
  @Input() formGroup: FormGroup;
  @Input() fieldName: string;
  @Input() size: 'small' | 'default' | 'large' = 'default';

  // Inherited but unused for now
  serverSideFieldValidationError: FieldValidationError[];

  @ViewChild('textInput') set textInput(elem: ElementRef) {
    this._textInput = elem;

    if (elem != null) {
      fromEvent(elem.nativeElement, 'input').pipe(
        auditTime(150),
        distinctUntilChanged()
      ).subscribe(() => this.onInput());
    }
  }

  get textInput(): ElementRef {
    return this._textInput;
  }

  private _textInput: ElementRef;

  visibleOptionLabels: string[] = [];

  constructor(public readonly remoteIssueService: RemoteIssueService,
              cdr: ChangeDetectorRef) {
    super(cdr);
  }

  get command(): string {
    return this.field?.rendering.inputType.configuration?.onchange;
  }

  get showSearchIcon(): boolean {
    return this.command != null;
  }

  get showClearButton(): boolean {
    return Boolean(this.formControl?.value);
  }

  get isNumeric(): boolean {
    return this.field?.rendering.inputType.dataType === 'number';
  }

  get inputType(): string {
    return this.isNumeric ? 'number' : 'text';
  }

  onInput(): void {
    if (this.command != null) {
      const value = this.textInput?.nativeElement.value;
      this.remoteIssueService.sendDelegateCommand<FieldValue>(this.command, value).subscribe((result) => {
        this.visibleOptionLabels = result?.composite?.map(v => v.name) ?? [];
        this.cdr.detectChanges();
      });
    }
  }

  clearInputAndShowList(): void {
    if (this.formControl) {
      this.formControl.setValue('');
    }

    if (this.textInput) {
      this.textInput.nativeElement.value = '';
      this.forceRefreshAutocompleteOptions();
    }
  }

  private forceRefreshAutocompleteOptions(): void {
    this.onInput();
    this.textInput?.nativeElement.focus();
  }
}
