import {Identifier, IssueBindableEntity, RemoteIssue} from 'sqtm-core';

export interface BoundEntityInfo {
  bindableEntity: IssueBindableEntity;
  boundEntityId: Identifier;
}

export interface RemoteIssueDialogState {
  // Are we attaching to an existing remote issue ?
  attachMode: boolean;

  // Identifier for the bugtracker used
  bugTrackerId: Identifier;

  // The entity this issue is getting bound to
  boundEntity: BoundEntityInfo;

  // Model used to build the creation form (NOT the transient state, which is held by NG forms)
  remoteIssue: RemoteIssue;

  // Available remote project names
  remoteProjectNames: string[];

  // The remote project we're adding to
  remoteProjectName: string;
}
