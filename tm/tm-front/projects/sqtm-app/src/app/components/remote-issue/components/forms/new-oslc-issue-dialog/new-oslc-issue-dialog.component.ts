import {ChangeDetectionStrategy, Component, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {DialogService, isOslcIssue, OslcIssue} from 'sqtm-core';
import {DomSanitizer} from '@angular/platform-browser';
import {RemoteIssueDialogState} from '../../../states/remote-issue-dialog.state';
import {FormBuilder} from '@angular/forms';
import {RemoteIssueService} from '../../../services/remote-issue.service';

@Component({
  selector: 'sqtm-app-new-oslc-issue-dialog',
  templateUrl: './new-oslc-issue-dialog.component.html',
  styleUrls: [
    './new-oslc-issue-dialog.component.less',
    '../../../styles/remote-issue-dialog-commons.less',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewOslcIssueDialogComponent {

  private _remoteIssueDialogState: RemoteIssueDialogState;

  @Input()
  set remoteIssueDialogState(state: RemoteIssueDialogState) {
    this._remoteIssueDialogState = state;

    if (!isOslcIssue(state.remoteIssue)) {
      throw new Error('Unexpected issue type. This component may not work as intended.');
    }
  }

  get remoteIssueDialogState(): RemoteIssueDialogState {
    return this._remoteIssueDialogState;
  }

  @Output()
  requireDialogClose = new EventEmitter<boolean>();

  constructor(private readonly domSanitizer: DomSanitizer,
              private readonly fb: FormBuilder,
              private readonly remoteIssueService: RemoteIssueService,
              private readonly dialogService: DialogService) {
  }

  @HostListener('window:message', ['$event'])
  handleWindowMessage(event: Event) {
    const data = event['data'];

    if (data != null && typeof data === 'string') {
      const HEADER = 'oslc-response:';
      if (data.indexOf(HEADER) === 0) {
        const issue = JSON.parse(data.substring(HEADER.length));
        const issueData = issue['oslc:results'][0];

        if (issueData != null) {
          const issueId = issueData['rdf:resource'].split('/').pop();
          this.remoteIssueService.submitOslcIssue(issueId)
            .subscribe(() => this.closeDialog(true),
              (err) => this.handleSubmitError(err));
        } else {
          this.closeDialog(false);
        }
      }
    }
  }

  getCreateUrl(): any {
    const url = (this._remoteIssueDialogState.remoteIssue as OslcIssue)?.createDialog + '#oslc-core-postMessage-1.0';
    return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
  }

  getSelectUrl(): any {
    const url = (this._remoteIssueDialogState.remoteIssue as OslcIssue)?.selectDialog + '#oslc-core-postMessage-1.0';
    return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
  }

  private closeDialog(success: boolean): void {
    this.requireDialogClose.emit(success);
  }

  private handleSubmitError(response: any): void {
    const message = response.fieldValidationErrors ? response.fieldValidationErrors[0].errorMessage
      : response.trace;

    this.dialogService.openAlert({
      id: 'report-oslc-error',
      level: 'DANGER',
      messageKey: message,
    });
  }
}
