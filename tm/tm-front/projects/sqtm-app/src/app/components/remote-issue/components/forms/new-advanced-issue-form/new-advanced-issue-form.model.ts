import {Field, FieldValue} from 'sqtm-core';
import {format} from 'date-fns';

/* NOTE :
 * Date time values SHOULD theoretically be ISO-8601 compliant but Jira Server date parsing is bugged :
 * https://jira.atlassian.com/browse/JRASERVER-61378
 * So we instead use the same format as in Squash 1.x
 */
const DATE_TIME_FORMAT = 'yyyy-MM-dd HH:mm';
const DATE_FORMAT = 'yyyy-MM-dd';

type ReadonlyField = Readonly<Field>;

export enum FieldInputTypeName {
  dropdown_list = 'dropdown_list',
  text_field = 'text_field',
  tag_list = 'tag_list',
  free_tag_list = 'free_tag_list',
  text_area = 'text_area',
  file_upload = 'file_upload',
  date_picker = 'date_picker',
  date_time = 'date_time',
  multi_select = 'multi_select',
  cascading_select = 'cascading_select',
  option_with_child = 'option-with-child',
  checkbox_list = 'checkbox_list',
  radio_button = 'radio_button',
  unknown = 'unknown',
}

export const SUPPORTED_INPUT_TYPES = [
  FieldInputTypeName.dropdown_list,
  FieldInputTypeName.text_field,
  FieldInputTypeName.tag_list,
  FieldInputTypeName.free_tag_list,
  FieldInputTypeName.text_area,
  FieldInputTypeName.file_upload,
  FieldInputTypeName.date_picker,
  FieldInputTypeName.date_time,
  FieldInputTypeName.multi_select,
  FieldInputTypeName.cascading_select,
  FieldInputTypeName.checkbox_list,
  FieldInputTypeName.radio_button,
  FieldInputTypeName.option_with_child,

  // fixme: really, having to support 'unknown' type is really not cool but some tickets have
  //  unknown required fields (e.g. Nom de l'épopée).
  //  We need to check both type.name and type.dataType to known if a field is supported or not...
  //  [unknown, string] is supported but [unknown, issuelinks-array] is not for example.
  FieldInputTypeName.unknown,
];

export const SUPPORTED_UNKNOWN_INPUT_DATA_TYPES = [
  'string',
];

export function isIssueFieldTypeSupported(typeName: string, dataType: string): boolean {
  if (typeName === FieldInputTypeName.unknown) {
    return SUPPORTED_UNKNOWN_INPUT_DATA_TYPES.includes(dataType);
  }

  return (SUPPORTED_INPUT_TYPES as string[]).includes(typeName);
}

export const COMPOSITE_INPUT_TYPES = [
  FieldInputTypeName.tag_list,
  FieldInputTypeName.free_tag_list,
];

export function isCompositeField(field: ReadonlyField): boolean {
  return (COMPOSITE_INPUT_TYPES as string[]).includes(field.rendering.inputType.name);
}

export const OPTION_WITH_CHILD_TYPES = [
  FieldInputTypeName.option_with_child,
  FieldInputTypeName.cascading_select,
];

export function isOptionWithChild(field: ReadonlyField): boolean {
  return (OPTION_WITH_CHILD_TYPES as string[]).includes(field.rendering.inputType.name);
}

export const MULTI_SCALAR_INPUT_TYPES = [
  FieldInputTypeName.checkbox_list,
];

export function isMultiScalarField(field: ReadonlyField): boolean {
  return (MULTI_SCALAR_INPUT_TYPES as string[]).includes(field.rendering.inputType.name);
}

export function isDropdownListField(field: ReadonlyField): boolean {
  return field.rendering.inputType.name === FieldInputTypeName.dropdown_list;
}

export function getPossibleValuesForField(field: ReadonlyField): FieldValue[] {
  return field?.possibleValues;
}

export function isMultiSelectField(field: ReadonlyField): boolean {
  return field.rendering.inputType.name === FieldInputTypeName.multi_select;
}

/**
 * Look into possible values of a field to find a value with matching provided ID.
 * May return undefined.
 *
 * @param field definition (with possible values if any)
 * @param valueId FieldValue ID to look for
 * @returns a FieldValue or undefined if no match found
 */
export function findFieldValueAmongPossibleValues(field: ReadonlyField, valueId: string): FieldValue {
  return getPossibleValuesForField(field)?.find(value => value.id === valueId);
}

/**
 * Obtain a FieldValue from a field and a transient widget value.
 *
 * @param field definition (with possible values if any)
 * @param value can be a primitive value (string, number) or an array of primitives, based on input type
 * @returns a FieldValue that can be either an identified FieldValue found among possible values, a composite
 * value (for composite value types such as tags) or a scalar (a wrapper around a primitive value with no ID).
 */
export function toFieldValue(field: ReadonlyField, value: any): FieldValue {
  if (isDate(field)) {
    return toDateValue(field, value);
  } else if (isDateTime(field)) {
    return toDateTimeValue(field, value);
  } else if (isOptionWithChild(field)) {
    return toOptionWithChildValue(field, value);
  } else if (isMultiScalarField(field)) {
    return toMultiScalarFieldValue(field, value);
  } else if (isMultiSelectField(field)) {
    return toMultiSelectFieldValue(field, value);
  } else if (isCompositeField(field)) {
    return toCompositeFieldValue(field, value);
  } else {
    const optionValue = findFieldValueAmongPossibleValues(field, value);

    if (optionValue) {
      // Radio button values are ignored if the typename is not set to 'select_option'
      optionValue.typename = field.rendering.inputType.dataType;
    }

    return optionValue ?? asScalar(field, value);
  }
}

function toCompositeFieldValue(field: ReadonlyField, value: any): FieldValue {
  if (Array.isArray(value)) {
    const compositeValues = value.map((compositeValue: any) => {
      const optionValue = findFieldValueAmongPossibleValues(field, compositeValue);
      return optionValue ?? asScalar(field, compositeValue);
    });

    return asComposite(field.id, compositeValues);
  } else if (value == null) {
    return asScalar(field, value);
  } else {
    throw new Error('Received invalid composite field value ' + value);
  }
}

function toOptionWithChildValue(field: ReadonlyField, value: any): FieldValue {
  if (Array.isArray(value)) {
    return asScalar(field, value.join(','));
  } else if (value == null) {
    return asScalar(field, value);
  } else {
    throw new Error('Received invalid option with child ' + value);
  }
}

function toMultiScalarFieldValue(field: ReadonlyField, value: any): FieldValue {
  if (Array.isArray(value)) {
    return asCheckboxListValue(field, value.join(','));
  } else if (value == null) {
    return asScalar(field, value);
  } else {
    throw new Error('Received invalid multi-scalar field value ' + value);
  }
}
function toMultiSelectFieldValue(field: ReadonlyField, value: any): FieldValue {
  if (Array.isArray(value)) {
    const fullValues = value.map(v => field.possibleValues.find(possible => possible.id === v)).filter(Boolean);
    return {
      id: field.id,
      composite: fullValues,
      custom: null,
      scalar: '',
      typename: field.rendering.inputType.dataType,
      name: '',
    };
  } else if (value == null) {
    return asScalar(field, value);
  } else {
    throw new Error('Received invalid multi-scalar field value ' + value);
  }
}

function asScalar(field: ReadonlyField, value: string): FieldValue {
  return {
    id: '--',
    scalar: value ?? '', // JiraCloud doesn't like null scalar values
    composite: [],
    custom: null,
    typename: field.rendering.inputType.dataType,
    name: undefined,
  };
}

function asCheckboxListValue(field: ReadonlyField, value: string): FieldValue {
  return { ...asScalar(field, value), typename: 'checkbox_list' };
}

function asComposite(id: string, values: any[]): FieldValue {
  return {
    id,
    scalar: '',
    composite: values,
    custom: null,
    typename: 'composite',
    name: undefined,
  };
}

function isDate(field: ReadonlyField): boolean {
  return field.rendering.inputType.name === FieldInputTypeName.date_picker
    || field.rendering.inputType.dataType === 'redmine.custom.date'; // TODO pck check if needed after 2.0.0 release
}

function toDateValue(field: ReadonlyField, value: any): FieldValue {
  if (value instanceof Date) {
    try {
      return asScalar(field, format(value, DATE_FORMAT));
    } catch (e) {
      console.error('Error when formatting date time value ' + value);
      console.error(e);
    }
  } else {
    return asScalar(field, value);
  }
}

function isDateTime(field: ReadonlyField): Boolean {
  return field.rendering.inputType.name === FieldInputTypeName.date_time
    || field.rendering.inputType.dataType === 'datetime'; // TODO pck check if needed after 2.0.0 release
}

function toDateTimeValue(field: ReadonlyField, value: any): FieldValue {
  if (value instanceof Date) {
    try {
      return asScalar(field, format(value, DATE_TIME_FORMAT));
    } catch (e) {
      console.error('Error when formatting date time value ' + value);
      console.error(e);
    }
  } else {
    return asScalar(field, value);
  }
}

export function isFileUpload(field: ReadonlyField): boolean {
  return field.rendering.inputType.name === FieldInputTypeName.file_upload;
}
