import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoteAttachmentFieldComponent } from './remote-attachment-field.component';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';

describe('RemoteAttachmentFieldComponent', () => {
  let component: RemoteAttachmentFieldComponent;
  let fixture: ComponentFixture<RemoteAttachmentFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [ RemoteAttachmentFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteAttachmentFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
