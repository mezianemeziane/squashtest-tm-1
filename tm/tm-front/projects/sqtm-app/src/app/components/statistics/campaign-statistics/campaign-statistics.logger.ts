import {sqtmAppLogger} from '../../../app-logger';

export const campaignStatisticsLogger = sqtmAppLogger.compose('campaign-statistics');
