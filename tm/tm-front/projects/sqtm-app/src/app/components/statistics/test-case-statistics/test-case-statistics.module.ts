import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {WorkspaceCommonModule} from 'sqtm-core';
import {RouterModule} from '@angular/router';
import {TestCaseStatisticPanelComponent} from './components/test-case-statistic-panel/test-case-statistic-panel.component';

@NgModule({
  declarations: [TestCaseStatisticPanelComponent],
  exports: [TestCaseStatisticPanelComponent],
  imports: [
    CommonModule,
    WorkspaceCommonModule,
    TranslateModule.forChild(),
    RouterModule
  ]
})
export class TestCaseStatisticsModule {
}
