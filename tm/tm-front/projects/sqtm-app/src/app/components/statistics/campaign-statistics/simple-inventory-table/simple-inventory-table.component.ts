import {Component, OnInit, ChangeDetectionStrategy, Input} from '@angular/core';
import {
  ExecutionStatus,
  ExecutionStatusKeys,
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {IterationAggregate} from '../campaign-inventory/campaign-inventory.component';
import {CampaignAggregate} from '../campaign-folder-inventory/campaign-folder-inventory.component';


@Component({
  selector: 'sqtm-app-simple-inventory-table',
  templateUrl: './simple-inventory-table.component.html',
  styleUrls: ['./simple-inventory-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpleInventoryTableComponent implements OnInit {

  ExecutionStatus = ExecutionStatus;
  @Input()
  inventory: GenericAggregate[];

  @Input()
  disabledExecutionStatus: ExecutionStatusKeys[];

  @Input()
  entityI18Key: string;


  constructor(protected readonly translateService: TranslateService) {
  }

  ngOnInit(): void {
  }

  getName(data: GenericAggregate) {
    if (isIterationAggregate(data)) {
      return data.iterationName;
    } else {
      return data.campaignName;
    }
  }
}

function isIterationAggregate(data: GenericAggregate): data is IterationAggregate {
  return data.hasOwnProperty('iterationName');
}

type GenericAggregate = IterationAggregate | CampaignAggregate;

