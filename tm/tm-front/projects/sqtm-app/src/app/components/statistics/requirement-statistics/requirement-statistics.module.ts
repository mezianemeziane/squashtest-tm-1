import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RequirementStatisticsPanelComponent} from './components/requirement-statistics-panel/requirement-statistics-panel.component';
import {TranslateModule} from '@ngx-translate/core';
import {RouterModule} from '@angular/router';
import {WorkspaceCommonModule} from 'sqtm-core';
import { NzIconModule } from 'ng-zorro-antd/icon';


@NgModule({
  declarations: [RequirementStatisticsPanelComponent],
  exports: [
    RequirementStatisticsPanelComponent
  ],
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule.forChild(),
        WorkspaceCommonModule,
        NzIconModule
    ]
})
export class RequirementStatisticsModule {
}
