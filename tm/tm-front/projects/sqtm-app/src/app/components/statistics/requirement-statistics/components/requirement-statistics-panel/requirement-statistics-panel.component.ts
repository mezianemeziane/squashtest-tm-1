import {ChangeDetectionStrategy, Component, ElementRef, Input, NgZone, Renderer2, ViewChild} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {
  compareLevelEumItems,
  ExecutionStatus,
  FilterOperation,
  FilterValueKind,
  I18nEnumItem,
  InconclusiveExecutionStatus,
  isMilestoneModeActivated,
  LevelEnumItem,
  Logger,
  MilestoneModeData,
  RequirementCriticality,
  RequirementCriticalityKeys,
  RequirementCriticalityLevelEnumItem,
  RequirementCurrentVersionFilter,
  RequirementCurrentVersionFilterKeys,
  RequirementStatistics,
  RequirementStatus,
  RequirementStatusKeys,
  RequirementValidationStatistics,
  RequirementVersionHasDescriptionFilter,
  RequirementVersionHasDescriptionFilterKey,
  RestService,
  SimpleFilter,
  getRequirementCriticalityChartColors,
  getRequirementStatusChartColors
} from 'sqtm-core';
import {requirementStatisticsLogger} from '../../requirement-statistics.logger';
import * as camelcase from 'camelcase';
import {Layout, newPlot} from 'plotly.js';
import {AbstractStatisticsPanelComponent} from '../../../abstract-statistics-panel.component';

const logger = requirementStatisticsLogger.compose('RequirementStatisticsPanelComponent');

@Component({
  selector: 'sqtm-app-requirement-statistics-panel',
  templateUrl: './requirement-statistics-panel.component.html',
  styleUrls: ['./requirement-statistics-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementStatisticsPanelComponent extends AbstractStatisticsPanelComponent {

  @ViewChild('coverageChart', {read: ElementRef})
  coverageChart: ElementRef;

  @ViewChild('statusChart', {read: ElementRef})
  statusChart: ElementRef;

  @ViewChild('criticalityChart', {read: ElementRef})
  criticalityChart: ElementRef;

  @ViewChild('descriptionChart', {read: ElementRef})
  descriptionChart: ElementRef;

  @ViewChild('coverageByCriticalityChart', {read: ElementRef})
  coverageByCriticalityChart: ElementRef;

  @ViewChild('coverageByCriticalityChartWrapper', {read: ElementRef})
  coverageByCriticalityChartWrapper: ElementRef;

  @ViewChild('coverageByCriticalityChartLegendWrapper', {read: ElementRef})
  coverageByCriticalityChartLegendWrapper: ElementRef;

  @ViewChild('validationByCriticalityChart', {read: ElementRef})
  validationByCriticalityChart: ElementRef;

  @ViewChild('validationByCriticalityChartWrapper', {read: ElementRef})
  validationByCriticalityChartWrapper: ElementRef;

  @ViewChild('validationByCriticalityChartLegendWrapper', {read: ElementRef})
  validationByCriticalityChartLegendWrapper: ElementRef;

  RequirementCriticality = RequirementCriticality;

  get statistics(): RequirementStatistics {
    return this._statistics;
  }

  @Input()
  set statistics(value: RequirementStatistics) {
    const hasPreviousSelection = this._statistics?.selectedIds?.length > 0;
    logger.debug('new value in charts :', [value]);
    this._statistics = value;
    this.updateAllCharts(hasPreviousSelection);
  }

  @Input()
  milestoneModeData: MilestoneModeData;

  private _statistics: RequirementStatistics;

  readonly SUCCESS_COLOR = '#006f57';
  readonly FAILURE_COLOR = '#cb1524';
  readonly UNDEFINED_COLOR = '#b8b8b8';

  constructor(protected translateService: TranslateService,
              protected router: Router,
              protected renderer: Renderer2,
              protected zone: NgZone,
              private restService: RestService) {
    super(translateService, router, renderer, zone);
  }

  protected buildAllCharts(): void {
    const versionFilter: SimpleFilter = this.createFilterOnCurrentVersion();
    this.buildCoverageChart(versionFilter);
    this.buildStatusChart(versionFilter);
    this.buildCriticalityChart(versionFilter);
    this.buildDescriptionChart(versionFilter);
    this.buildCoverageByCriticalityChart(versionFilter);
    this.buildValidationByCriticalityChart(versionFilter);
    this.forceResize();
  }

  protected getAllCharts(): ElementRef[] {
    return [this.coverageChart, this.statusChart, this.criticalityChart, this.descriptionChart];
  }

  protected get logger(): Logger {
    return logger;
  }

  protected get searchBaseUrl(): string {
    return 'requirement';
  }

  protected get totalCount(): number {
    return this.statistics.selectedIds.length;
  }

  private buildCoverageChart(versionFilter: SimpleFilter): void {
    const labels = [
      'sqtm-core.requirement-workspace.statistics.bound-tc.legend.no-tc',
      'sqtm-core.requirement-workspace.statistics.bound-tc.legend.one-tc',
      'sqtm-core.requirement-workspace.statistics.bound-tc.legend.many-tc'
    ].map(key => this.translateService.instant(key));

    const values = [
      this._statistics.boundTestCasesStatistics.zeroTestCases,
      this._statistics.boundTestCasesStatistics.oneTestCase,
      this._statistics.boundTestCasesStatistics.manyTestCases,
    ];

    const noTcFilter = this.createCoveragesCountFilter('0');
    const oneTcFilter = this.createCoveragesCountFilter('1');
    const manyTcFilter = this.createCoveragesCountFilter('1', FilterOperation.GREATER);


    const filters = [[noTcFilter, versionFilter], [oneTcFilter, versionFilter], [manyTcFilter, versionFilter]];

    this.plotPieChart({
      titleKey: 'sqtm-core.requirement-workspace.statistics.bound-tc.title',
      values,
      labels,
      elementRef: this.coverageChart,
      colors: ['#da4167', '#832161', '#3d2645'],
      customData: filters
    });
  }

  public createFilterOnCurrentVersion() {
    let enumItem: I18nEnumItem<RequirementCurrentVersionFilterKeys>;
    if (this.milestoneModeData && isMilestoneModeActivated(this.milestoneModeData)) {
      enumItem = RequirementCurrentVersionFilter.ALL;
    } else {
      enumItem = RequirementCurrentVersionFilter.CURRENT;
    }
    return {
      id: 'requirementVersionCurrentVersion',
      operation: FilterOperation.IN,
      value: {
        kind: 'multiple-discrete-value' as FilterValueKind,
        value: [{id: enumItem.id, i18nLabelKey: enumItem.i18nKey}]
      }
    };
  }

  private createCoveragesCountFilter(value, operation: FilterOperation = FilterOperation.EQUALS) {
    const filter: SimpleFilter = {
      id: 'coveragesCount',
      operation: operation,
      value: {
        kind: 'single-string-value',
        value: value
      }
    };
    return filter;
  }

  private buildStatusChart(versionFilter: SimpleFilter) {
    const orderedStatus: LevelEnumItem<RequirementStatusKeys>[] = Object.values(RequirementStatus)
      .sort(compareLevelEumItems);

    const labels = orderedStatus
      .map(status => this.translateService.instant(status.i18nKey));

    const values = orderedStatus
      .map(status => this.statistics.statusesStatistics[camelcase(status.id)]);

    const filters = this.buildStatusFilters(orderedStatus, versionFilter);

    this.plotPieChart({
      titleKey: 'sqtm-core.entity.generic.status.label',
      values,
      labels,
      elementRef: this.statusChart,
      colors: getRequirementStatusChartColors(),
      customData: filters
    });
  }

  private buildStatusFilters(orderedStatus: LevelEnumItem<RequirementStatusKeys>[], versionFilter: SimpleFilter): SimpleFilter[][] {
    return orderedStatus
      .map((status: LevelEnumItem<RequirementStatusKeys>) => ({
        id: 'status',
        operation: FilterOperation.IN,
        value: {
          kind: 'multiple-discrete-value' as FilterValueKind,
          value: [{
            id: status.id,
            i18nLabelKey: status.i18nKey
          }]
        }
      }))
      .map((filter) => ([filter, versionFilter]));
  }

  private buildCriticalityChart(versionFilter: SimpleFilter) {
    const orderedCriticality: LevelEnumItem<RequirementCriticalityKeys>[] = this.orderCriticalityEnum();
    const filters = this.buildCriticalityFilters(orderedCriticality, versionFilter);
    const labels = orderedCriticality
      .map(criticality => this.translateService.instant(criticality.i18nKey));
    const values = orderedCriticality
      .map(criticality => this.statistics.criticalityStatistics[camelcase(criticality.id)]);

    this.plotPieChart({
      titleKey: 'sqtm-core.entity.requirement.criticality.label',
      values,
      labels,
      elementRef: this.criticalityChart,
      colors: getRequirementCriticalityChartColors().reverse(),
      customData: filters
    });
  }

  private buildCriticalityFilters(orderedCriticality: LevelEnumItem<RequirementCriticalityKeys>[],
                                  versionFilter: SimpleFilter): SimpleFilter[][] {
    return orderedCriticality
      .map((criticality: LevelEnumItem<RequirementCriticalityKeys>) => this.buildOneCriticalityFilter(criticality))
      .map((filter) => ([filter, versionFilter]));
  }

  private buildOneCriticalityFilter(criticality: LevelEnumItem<RequirementCriticalityKeys>) {
    return {
      id: 'criticality',
      operation: FilterOperation.IN,
      value: {
        kind: 'multiple-discrete-value' as FilterValueKind,
        value: [{
          id: criticality.id,
          i18nLabelKey: criticality.i18nKey
        }]
      }
    };
  }

  private orderCriticalityEnum() {
    return Object.values(RequirementCriticality)
      .sort(compareLevelEumItems)
      .reverse();
  }

  private buildDescriptionChart(versionFilter: SimpleFilter) {
    const labels = [
      'sqtm-core.requirement-workspace.statistics.description.legend.with',
      'sqtm-core.requirement-workspace.statistics.description.legend.without'].map(key => this.translateService.instant(key));

    const values = [
      this._statistics.boundDescriptionStatistics.hasDescription,
      this._statistics.boundDescriptionStatistics.hasNoDescription,
    ];

    const withDescriptionFilter = this.createDescriptionFilter(RequirementVersionHasDescriptionFilter.HAS_DESCRIPTION);
    const withoutDescriptionFilter = this.createDescriptionFilter(RequirementVersionHasDescriptionFilter.NO_DESCRIPTION);

    const filters = [[withDescriptionFilter, versionFilter], [withoutDescriptionFilter, versionFilter]];

    this.plotPieChart({
      titleKey: 'sqtm-core.entity.generic.description.label',
      values,
      labels,
      elementRef: this.descriptionChart,
      colors: ['#e07a5f', '#3d405b'],
      customData: filters
    });
  }

  private createDescriptionFilter(
    hasDescriptionEnumItem: I18nEnumItem<RequirementVersionHasDescriptionFilterKey>,
    operation: FilterOperation = FilterOperation.EQUALS) {

    const value = [{
      id: hasDescriptionEnumItem.id,
      i18nLabelKey: hasDescriptionEnumItem.i18nKey
    }];

    const filter: SimpleFilter = {
      id: 'hasDescription',
      operation: operation,
      value: {
        kind: 'multiple-discrete-value',
        value: value
      }
    };
    return filter;
  }


  private buildCoverageByCriticalityChart(versionFilter: SimpleFilter) {
    const coverageStatistics = this.statistics.coverageStatistics;
    const data = [
      this.buildOneCoverageByCriticalityDomain(
        RequirementCriticality.CRITICAL,
        coverageStatistics.critical,
        coverageStatistics.totalCritical,
        0.81,
        0,
        versionFilter
      ),
      this.buildOneCoverageByCriticalityDomain(
        RequirementCriticality.MAJOR,
        coverageStatistics.major,
        coverageStatistics.totalMajor,
        0.73,
        0.105,
        versionFilter
      ),
      this.buildOneCoverageByCriticalityDomain(
        RequirementCriticality.MINOR,
        coverageStatistics.minor,
        coverageStatistics.totalMinor,
        0.62,
        0.22,
        versionFilter
      ),
      this.buildOneCoverageByCriticalityDomain(
        RequirementCriticality.UNDEFINED,
        coverageStatistics.undefined,
        coverageStatistics.totalUndefined,
        0.40,
        0.335,
        versionFilter
      )
    ] as any;

    const layout: Partial<Layout> = {...this.BASE_LAYOUT};
    const config: Partial<Plotly.Config> = {displayModeBar: false};
    newPlot(this.coverageByCriticalityChart.nativeElement, data, layout, config).then(chart => {
      this.initializePieZoneClick(chart);
    });

  }


  private buildOneCoverageByCriticalityDomain(
    criticality: RequirementCriticalityLevelEnumItem<RequirementCriticalityKeys>,
    covered: number,
    total: number,
    hole: number,
    domainShrink: number,
    currentVersionFilter: SimpleFilter
  ) {
    if (total > 0) {
      return this.buildOneFilledCoverageByCriticalityDomain(criticality, covered, total, domainShrink, hole, currentVersionFilter);
    } else {
      const criticalityLabel = this.translateService.instant(criticality.i18nKey);
      const emptySelectionLabel = this.translateService.instant(
        'sqtm-core.requirement-workspace.statistics.coverage-by-crit.empty',
        {criticalityLabel});
      return this.buildEmptyDomain(this.translateService.instant(criticality.i18nKey), domainShrink, hole, emptySelectionLabel);
    }
  }

  private buildOneFilledCoverageByCriticalityDomain(
    criticality: RequirementCriticalityLevelEnumItem<RequirementCriticalityKeys>,
    covered: number,
    total: number,
    domainShrink: number,
    hole: number,
    currentVersionFilter: SimpleFilter) {
    const name = this.translateService.instant(criticality.i18nKey);
    const coveredLabel = this.translateService.instant('sqtm-core.requirement-workspace.statistics.coverage-by-crit.covered');
    const uncoveredLabel = this.translateService.instant('sqtm-core.requirement-workspace.statistics.coverage-by-crit.uncovered');
    const values = [covered, total - covered];
    const labels = [coveredLabel, uncoveredLabel];
    const colors = [this.SUCCESS_COLOR, this.FAILURE_COLOR];
    const criticalityFilter = this.buildOneCriticalityFilter(criticality);
    const uncoveredFilter = this.createCoveragesCountFilter('0');
    const coveredFilter = this.createCoveragesCountFilter('1', FilterOperation.GREATER_EQUAL);
    const filters = [[criticalityFilter, coveredFilter, currentVersionFilter], [criticalityFilter, uncoveredFilter, currentVersionFilter]];
    return this.buildOneFilledDomain(values, labels, name, domainShrink, hole, colors, filters);
  }

  private buildValidationByCriticalityChart(versionFilter: SimpleFilter) {
    const validationStatistics: RequirementValidationStatistics = this.statistics.validationStatistics;
    const selectedIds = this.statistics.selectedIds;
    const data = [
      this.buildOneValidationByCriticalityDomain(
        RequirementCriticality.CRITICAL,
        validationStatistics.conclusiveCritical,
        validationStatistics.inconclusiveCritical,
        validationStatistics.undefinedCritical,
        0.81,
        0,
        selectedIds
      ),
      this.buildOneValidationByCriticalityDomain(
        RequirementCriticality.MAJOR,
        validationStatistics.conclusiveMajor,
        validationStatistics.inconclusiveMajor,
        validationStatistics.undefinedMajor,
        0.73,
        0.105,
        selectedIds
      ),
      this.buildOneValidationByCriticalityDomain(
        RequirementCriticality.MINOR,
        validationStatistics.conclusiveMinor,
        validationStatistics.inconclusiveMinor,
        validationStatistics.undefinedMinor,
        0.62,
        0.22,
        selectedIds
      ),
      this.buildOneValidationByCriticalityDomain(
        RequirementCriticality.UNDEFINED,
        validationStatistics.conclusiveUndefined,
        validationStatistics.inconclusiveUndefined,
        validationStatistics.undefinedUndefined,
        0.40,
        0.335,
        selectedIds
      )
    ] as any;

    const layout: Partial<Layout> = {...this.BASE_LAYOUT};

    const config: Partial<Plotly.Config> = {displayModeBar: false};

    newPlot(this.validationByCriticalityChart.nativeElement, data, layout, config).then(chart => {
      chart.on('plotly_click', ($event: Plotly.PlotMouseEvent) => {
        const customData = $event.points[0]?.customdata?.[0]; // Because slices may not contain custom data!
        if (Boolean(customData)) {
          this.searchOnValidationChart(customData, versionFilter);
        }
      });
    });
  }

  private buildOneValidationByCriticalityDomain(
    criticality: RequirementCriticalityLevelEnumItem<RequirementCriticalityKeys>,
    conclusive: number,
    inconclusive: number,
    undefinedResult: number,
    hole: number,
    domainShrink: number,
    selectedIds: number[]
  ) {
    const total = conclusive + inconclusive + undefinedResult;
    if (total > 0) {
      return this.buildOneFilledValidationByCriticalityDomain(
        criticality,
        conclusive,
        inconclusive,
        undefinedResult,
        domainShrink,
        hole,
        selectedIds);
    } else {
      const criticalityLabel = this.translateService.instant(criticality.i18nKey);
      const emptySelectionLabel = this.translateService.instant(
        'sqtm-core.requirement-workspace.statistics.validation-by-crit.empty',
        {criticalityLabel});
      return this.buildEmptyDomain(this.translateService.instant(criticality.i18nKey), domainShrink, hole, emptySelectionLabel);
    }
  }

  private buildOneFilledValidationByCriticalityDomain(
    criticality: RequirementCriticalityLevelEnumItem<RequirementCriticalityKeys>,
    conclusive: number,
    inconclusive: number,
    undefinedResult: number,
    domainShrink: number,
    hole: number,
    selectedIds: number[]) {
    const name = this.translateService.instant(criticality.i18nKey);
    const conclusiveLabel = this.translateService.instant('sqtm-core.requirement-workspace.statistics.validation-by-crit.conclusive');
    const inconclusiveLabel = this.translateService.instant('sqtm-core.requirement-workspace.statistics.validation-by-crit.inconclusive');
    const undefinedLabel = this.translateService.instant('sqtm-core.requirement-workspace.statistics.validation-by-crit.undefined');
    const values = [conclusive, inconclusive, undefinedResult];
    const labels = [conclusiveLabel, inconclusiveLabel, undefinedLabel];
    const colors = [this.SUCCESS_COLOR, this.FAILURE_COLOR, this.UNDEFINED_COLOR];
    const customData = [
      [selectedIds, criticality.id, [ExecutionStatus.SUCCESS.id]],
      [selectedIds, criticality.id, [ExecutionStatus.FAILURE.id]],
      [selectedIds, criticality.id, InconclusiveExecutionStatus.map(status => status.id)],
    ];
    return this.buildOneFilledDomain(values, labels, name, domainShrink, hole, colors, customData);
  }

  calculateCoverageGlobalCoveredRate() {
    const coverageStatistics = this.statistics.coverageStatistics;
    const totalSuccess = coverageStatistics.critical + coverageStatistics.major + coverageStatistics.minor + coverageStatistics.undefined;
    const total = coverageStatistics.totalCritical
      + coverageStatistics.totalMajor
      + coverageStatistics.totalMinor
      + coverageStatistics.totalUndefined;
    return Math.floor((totalSuccess / total) * 100);
  }

  calculateCoverageGlobalUncoveredRate() {
    return 100 - this.calculateCoverageGlobalCoveredRate();
  }

  calculateValidationGlobalConclusiveRate() {
    const validationStatistics: RequirementValidationStatistics = this.statistics.validationStatistics;
    const totalConclusive = validationStatistics.conclusiveCritical
      + validationStatistics.conclusiveMajor
      + validationStatistics.conclusiveMinor
      + validationStatistics.conclusiveUndefined;
    const total = this.calculateTotal(Object.values(validationStatistics));
    return totalConclusive === 0 ? totalConclusive : Math.floor((totalConclusive / total) * 100);
  }

  private calculateTotal(values: number[]): number {
    return values.reduce((acc, currentValue) => acc + currentValue, 0);
  }


  calculateValidationGlobalInconclusiveRate() {
    const validationStatistics = this.statistics.validationStatistics;
    const totalInconclusive = validationStatistics.inconclusiveCritical
      + validationStatistics.inconclusiveMajor
      + validationStatistics.inconclusiveMinor
      + validationStatistics.inconclusiveUndefined;
    const total = this.calculateTotal(Object.values(validationStatistics));
    // Inconclusive is rounded up because we doesn't want to miss test in failure...
    // Even if only one test among 10k is inconclusive, we can't show 0%
    return totalInconclusive === 0 ? totalInconclusive : Math.ceil((totalInconclusive / total) * 100);
  }

  protected getCustomCharts() {
    return [this.coverageByCriticalityChart, this.validationByCriticalityChart];
  }

  protected getAllCustomChartsLegends(): ElementRef[] {
    return [this.coverageByCriticalityChartLegendWrapper, this.validationByCriticalityChartLegendWrapper];
  }

  protected getAllCustomChartWrappers(): ElementRef[] {
    return [this.coverageByCriticalityChartWrapper, this.validationByCriticalityChartWrapper];
  }

  private searchOnValidationChart(customData: any[] | undefined, versionFilter: SimpleFilter): void {
    if (!Array.isArray(customData)) {
      return;
    }

    const selectedIds: number[] = customData[0];
    const criticality: RequirementCriticalityKeys = customData[1];
    const validation: string[] = customData[2];
    const body: ValidationStatisticRequest = {selectedIds, criticality, validation};

    this.restService.post<{ ids: number[] }>(['requirement-tree', 'validation-statistics'], body)
      .subscribe(({ids}) => {
        const idFilter = {
          id: 'idList',
          operation: FilterOperation.IN,
          value: {
            kind: 'multiple-numeric-value' as FilterValueKind,
            value: ids
          }
        };

        this.searchOnCriteria([idFilter, versionFilter]);
      });
  }
}

interface ValidationStatisticRequest {
  selectedIds: number[];
  criticality: RequirementCriticalityKeys;
  validation: string[];
}
