import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {
  AfterViewInit,
  Directive,
  ElementRef,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import {capitalizeString, EntityScope, Logger, ScopeKind, SimpleFilter, SimpleScope} from 'sqtm-core';
import * as CssElementQuery from 'css-element-queries';
import {ResizeSensor} from 'css-element-queries';
import {Subject} from 'rxjs';
import {shareReplay, take, takeUntil} from 'rxjs/operators';
import {BACK_URL_PARAM, FILTER_QUERY_PARAM_KEY, SCOPE_QUERY_PARAM_KEY} from '../../pages/search/search-constants';
import {Config, Layout, newPlot, relayout, restyle} from 'plotly.js';
import * as chroma from 'chroma-js';
import * as _ from 'lodash';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractStatisticsPanelComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input()
  scope: EntityScope[] = [];

  @Input()
  scopeKind: ScopeKind = 'custom';

  protected chartInitialized = false;

  protected resizeSensor: ResizeSensor;
  protected _resizeEvent = new Subject<ResizeEvent>();
  protected resizeEvent$ = this._resizeEvent.asObservable().pipe(shareReplay(1));

  protected unsub$ = new Subject<void>();

  readonly BASE_CHART_HEIGHT = 330;
  readonly TITLE_FONT = {
    family: 'Segoe UI, Roboto',
    size: 18,
    color: '#62737F'
  };

  @ViewChild('panel', {read: ElementRef, static: true})
  panel: ElementRef;

  readonly EMPTY_COLOR = 'rgb(219,219,219)';

  protected readonly BASE_MARGIN = {
    t: 70,
    b: 20,
    l: 10,
    r: 0
  };

  protected readonly CUSTOM_CHART_MARGIN = this.BASE_MARGIN;

  protected readonly BASE_LAYOUT = {
    height: 340,
    width: 564,
    hoverlabel: {bgcolor: 'black'},
    showlegend: false,
    margin: this.BASE_MARGIN
  };

  readonly LEGEND_WIDTH = 270;

  protected constructor(protected translateService: TranslateService,
                        protected router: Router,
                        protected renderer: Renderer2,
                        protected zone: NgZone) {
  }


  protected abstract get logger(): Logger;

  protected abstract get searchBaseUrl(): string;

  searchOnCriteria(filters?: SimpleFilter | SimpleFilter[]) {
    this.zone.run(() => {
      const queryParams = {
        [SCOPE_QUERY_PARAM_KEY]: this.getSerializedScope(),
        [BACK_URL_PARAM]: this.router.url,
      };
      if (filters) {
        const filterArray = Array.isArray(filters) ? filters : [filters];
        queryParams[FILTER_QUERY_PARAM_KEY] = JSON.stringify(filterArray);
      }
      this.router.navigate(['/', 'search', this.searchBaseUrl], {queryParams});
    });
  }

  getSerializedScope(): string {
    const scope = this.getSimpleScope();
    return JSON.stringify(scope);
  }

  getSimpleScope(): SimpleScope {
    return {
      kind: this.scopeKind,
      value: this.scope,
    };
  }

  protected plotPieChart(conf: PieChartConfiguration) {
    this.resizeEvent$.pipe(
      take(1)
    ).subscribe(resizeEvent => this.doPlot(conf, resizeEvent));
  }

  protected buildResizedLayout(resizeEvent: ResizeEvent) {
    let layout: any = {};
    if (resizeEvent.width > this.getSideLegendThreshold()) {
      layout = this.buildLayoutWithHorizontalLegend(resizeEvent);
    } else if (resizeEvent.width > this.getSingleColumnThreshold()) {
      layout = this.buildLayoutWithMarginBottom(resizeEvent.width / this.getChartByLineCount());
    } else {
      layout = this.buildLayoutWithMarginBottom(resizeEvent.width);
    }
    return layout;
  }

  protected getChartByLineCount() {
    return 2;
  }

  protected getSideLegendThreshold() {
    return 1080;
  }

  protected getSingleColumnThreshold() {
    return 570;
  }

  private buildLayoutWithHorizontalLegend(resizeEvent: ResizeEvent) {
    return {
      height: this.BASE_CHART_HEIGHT,
      width: resizeEvent.width / this.getChartByLineCount(),
      legend: {
        x: 1,
        y: 0.4
      },
      margin: {
        t: 70,
        b: 10,
        l: 10,
        r: 270
      }
    };
  }

  private buildInitialLayout(titleKey: string) {
    const title: string = capitalizeString(this.translateService.instant(titleKey));
    const layout: Partial<Layout> = {
      title: {
        text: `<b>${title}</b>`,
        font: this.TITLE_FONT,
        x: 1
      },
      height: this.BASE_CHART_HEIGHT,
      hoverlabel: {bgcolor: 'black'},
      legend: {
        x: 1,
        y: 0.2
      },
      showlegend: true,
      margin: {
        t: 70,
        b: 10,
        l: 10,
        r: 10
      }
    };
    return layout;
  }

  private buildLayoutWithMarginBottom(width: number) {
    return {
      height: 500,
      width,
      legend: {
        x: 0.2,
        y: -0.6
      },
      margin: {
        t: 70,
        b: 100,
        l: 10,
        r: 10
      }
    };
  }

  private doPlot(conf: PieChartConfiguration, resizeEvent: ResizeEvent) {
    const colors = conf.colors || chroma
      .scale(conf.brewerScale)
      .colors(conf.labels.length);


    const nativeElement = conf.elementRef.nativeElement;
    const data: any = this.buildBasicData(conf.values, conf.labels, colors, conf.customData);
    const layout = {...this.buildInitialLayout(conf.titleKey), ...this.buildResizedLayout(resizeEvent)};
    this.appendMessageIfNoData(conf, layout);
    const config: Partial<Config> = {displayModeBar: false};

    newPlot(nativeElement, data, layout, config).then(chart => {
      this.chartInitialized = true;
      this.initializePieZoneClick(chart);
      this.initializeLegendClick(chart);
      this.initializeHoverEvents(chart, colors);
    });
  }

  private appendMessageIfNoData(conf: PieChartConfiguration, layout) {
    const valueSum = _.sum(conf.values);
    if (valueSum === 0) {
      layout.annotations = [
        {
          xref: 'paper',
          yref: 'paper',
          x: 0.5,
          xanchor: 'left',
          y: 0.5,
          yanchor: 'bottom',
          text: this.translateService.instant('sqtm-core.generic.label.no-data'),
          showarrow: false
        }
      ];
    }
  }

  private initializeHoverEvents(chart, colors: string[]) {
    chart.on('plotly_hover', ($event: Plotly.PlotMouseEvent) => {
      const pointNumber = $event.points[0].pointNumber;
      const nextColors = [...colors];
      nextColors[pointNumber] = chroma.hex(colors[pointNumber]).brighten(0.2).hex();
      const dataUpdate = {marker: {colors: nextColors}};
      restyle(chart, dataUpdate);
    });
    chart.on('plotly_unhover', () => {
      const dataUpdate = {marker: {colors: colors}};
      restyle(chart, dataUpdate);
    });
  }

  private initializeLegendClick(chart) {
    chart.on('plotly_legendclick', ($event) => {
      const label = $event['label'];
      const allLabels: string[] = $event.data[0]['labels'];
      const clickedIndex = allLabels.indexOf(label);
      const filter = $event.data[0]['customdata'][clickedIndex];
      this.logger.debug(`Clicked on legend ${label}. All labels are ${allLabels}. Clicked index is ${clickedIndex}.`);
      this.logger.debug(`Associated filter should be ${filter}`);
      this.searchOnCriteria(filter);
      return false;
    });
  }

  protected initializePieZoneClick(chart) {
    chart.on('plotly_click', ($event: Plotly.PlotMouseEvent) => {
      this.searchOnCriteria($event.points[0].customdata[0]);
    });
  }

  private buildBasicData(values: number[], labels: any[], colors: string[], customdata?: any) {
    return [{
      values: values,
      labels: labels,
      type: 'pie',
      textposition: 'inside',
      texttemplate: '%{percent} (%{value})',
      marker: {
        colors
      },
      sort: false,
      customdata
    }];
  }

  protected abstract getAllCharts(): ElementRef[];

  protected abstract buildAllCharts(): void;

  protected abstract get totalCount(): number;

  handleResize(resizeEvent: ResizeEvent) {
    if (this.hasSelection()) {
      this.changeCssGrid(resizeEvent);
      if (this.chartInitialized) {
        const layout = this.buildResizedLayout(resizeEvent);
        this.getAllCharts().forEach(chart => relayout(chart.nativeElement, layout));
        this.handleResizeForCustomLegendCharts(resizeEvent);
      }
    }
  }

  hasSelection() {
    return this.totalCount > 0;
  }

  private changeCssGrid(resizeEvent: ResizeEvent) {
    if (resizeEvent.width > this.getSingleColumnThreshold()) {
      this.switchToMultipleColumnsLayout();
    } else {
      this.switchToOneColumnLayout();
    }
  }

  protected switchToMultipleColumnsLayout() {
    this.renderer.addClass(this.panel.nativeElement, 'grid-two-columns');
    this.renderer.removeClass(this.panel.nativeElement, 'grid-one-column');
  }


  protected switchToOneColumnLayout() {
    this.renderer.removeClass(this.panel.nativeElement, 'grid-two-columns');
    this.renderer.addClass(this.panel.nativeElement, 'grid-one-column');
  }

  protected handleResizeForCustomLegendCharts(resizeEvent: ResizeEvent) {
    let layout;
    if (resizeEvent.width > this.getSideLegendThreshold()) {
      layout = this.buildCustomChartLayoutWithSideLegend(resizeEvent.width / this.getChartByLineCount());
      this.switchCustomLegendToHorizontal();
    } else if (resizeEvent.width > this.getSingleColumnThreshold()) {
      layout = this.buildCustomChartLayoutWithLegendBellow(resizeEvent.width / this.getChartByLineCount());
      this.switchCustomLegendToBellow(resizeEvent.width / this.getChartByLineCount());
    } else {
      layout = this.buildCustomChartLayoutWithLegendBellow(resizeEvent.width);
      this.switchCustomLegendToBellow(resizeEvent.width);
    }
    this.getCustomCharts().forEach(chart => relayout(chart.nativeElement, layout));
  }

  protected buildOneFilledDomain(
    values: number[],
    labels: any[],
    name: string,
    domainShrink: number,
    hole: number,
    colors: string[],
    customdata: any[] = []) {
    return {
      values,
      labels,
      hovertemplate: `%{data.name} %{label}<br>%{value}<br>%{percent}<extra></extra>`,
      name,
      domain: {x: [domainShrink, 1 - domainShrink], y: [domainShrink, 1 - domainShrink]},
      hole,
      type: 'pie',
      textposition: 'inside',
      insidetextorientation: 'horizontal',
      sort: false,
      customdata,
      marker: {
        colors
      }
    };
  }

  protected buildEmptyDomain(
    name: string,
    domainShrink: number,
    hole: number,
    emptySelectionLabel: string) {
    return {
      values: [1],
      labels: [emptySelectionLabel],
      hovertemplate: '%{label}<extra></extra>',
      name,
      domain: {x: [domainShrink, 1 - domainShrink], y: [domainShrink, 1 - domainShrink]},
      hole,
      type: 'pie',
      textposition: 'none',
      sort: false,
      marker: {
        colors: [this.EMPTY_COLOR]
      }
    };
  }

  private buildCustomChartLayoutWithSideLegend(width: number) {
    return {
      height: 330,
      width: width - this.LEGEND_WIDTH,
      margin: this.CUSTOM_CHART_MARGIN
    };
  }

  protected getCustomCharts() {
    return [];
  }

  private buildCustomChartLayoutWithLegendBellow(width: number) {
    return {
      height: 330,
      width,
      margin: this.CUSTOM_CHART_MARGIN
    };
  }

  private switchCustomLegendToHorizontal() {
    this.getAllCustomChartWrappers().forEach(w => {
      this.renderer.addClass(w.nativeElement, 'flex-row');
      this.renderer.removeClass(w.nativeElement, 'flex-column');
    });

    this.getAllCustomChartsLegends().forEach(e => {
      this.renderer.addClass(e.nativeElement, 'legend_side');
      this.renderer.removeClass(e.nativeElement, 'legend_bellow');
      this.renderer.setStyle(e.nativeElement, 'width', `${this.LEGEND_WIDTH}px`);
    });
  }

  protected getAllCustomChartsLegends(): ElementRef[] {
    return [];
  }

  protected getAllCustomChartWrappers(): ElementRef[] {
    return [];
  }

  private switchCustomLegendToBellow(width: number) {
    this.getAllCustomChartWrappers().forEach(w => {
      this.renderer.addClass(w.nativeElement, 'flex-column');
      this.renderer.removeClass(w.nativeElement, 'flex-row');
    });

    this.getAllCustomChartsLegends().forEach(e => {
      this.renderer.addClass(e.nativeElement, 'legend_bellow');
      this.renderer.removeClass(e.nativeElement, 'legend_side');
      this.renderer.setStyle(e.nativeElement, 'width', `${width}px`);
    });
  }

  ngOnInit(): void {
    const that = this;
    this.zone.runOutsideAngular(() => {
      this.resizeSensor = new CssElementQuery.ResizeSensor(this.panel.nativeElement, function ($event: ResizeEvent) {
        that._resizeEvent.next($event);
      });
      this.resizeEvent$.pipe(
        takeUntil(this.unsub$)
      ).subscribe(resize => this.handleResize(resize));
    });
  }

  ngAfterViewInit(): void {
    if (this.hasSelection()) {
      this.buildAllCharts();
    } else {
      this.chartInitialized = true;
    }
  }

  ngOnDestroy(): void {
    this._resizeEvent.complete();
    this.unsub$.next();
    this.unsub$.complete();
    if (this.resizeSensor) {
      this.resizeSensor.detach();
    }
  }

  protected updateAllCharts(hasPreviousSelection: boolean) {
    if (this.chartInitialized) {
      if (!hasPreviousSelection) {
        setTimeout(() => this.buildAllCharts());
      } else {
        this.buildAllCharts();
      }
    }
  }

  protected forceResize() {
    this.resizeEvent$.pipe(
      take(1)
    ).subscribe(resize => this.handleResize(resize));
  }
}

export interface ResizeEvent {
  width: number;
  height: number;
}

export interface PieChartConfiguration {
  titleKey: string;
  values: number[];
  labels: string[];
  elementRef: ElementRef;
  brewerScale?: string;
  colors?: string[];
  customData?: any;
}
