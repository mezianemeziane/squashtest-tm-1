import {ChangeDetectionStrategy, Component} from '@angular/core';
import {ChartDefinitionService, ChartType, ReferentialDataService} from 'sqtm-core';
import {DataTitle, Layout, newPlot} from 'plotly.js';
import * as _ from 'lodash';
import {Dictionary} from 'lodash';
import {chartsLogger} from '../../../charts.logger';
import {ChartsTranslateService} from '../../../services/charts-translate.service';
import {AbstractCustomReportChart} from '../abstract-custom-report-chart';
import {TranslateService} from '@ngx-translate/core';

const logger = chartsLogger.compose('CustomReportComparativeChartComponent');

@Component({
  selector: 'sqtm-app-custom-report-comparative-chart',
  templateUrl: './custom-report-comparative-chart.component.html',
  styleUrls: ['./custom-report-comparative-chart.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomReportComparativeChartComponent extends AbstractCustomReportChart {

  constructor(protected chartsTranslateService: ChartsTranslateService,
              protected chartDefinitionService: ChartDefinitionService,
              protected referentialDataService: ReferentialDataService,
              protected translateService: TranslateService) {
    super(chartsTranslateService, chartDefinitionService, referentialDataService, translateService);
  }

  protected getHandledType(): ChartType {
    return ChartType.COMPARATIVE;
  }

  // In that kind of charts, labels are ordered like [[A,z],[A,y],[B,z] in abscissa attribute. values are set in series attributes.
  // So the abscissa attribute of chart definition is a mix between real y axis labels of the chart and series names
  // aka trace names in plotly.
  // In that example, to convert into Plotly traces we must extract A,B... as abscissa, x, y as trace names of the chart
  // the first column axis in ChartDefinition is the yAxis of plotly chart
  // the second column axis in ChartDefinition is the trace name of plotly chart
  // the values in series attributes are the xValues because our chart is plot horizontally
  // Values are flattened in this strange order, so we must take a great care to order.
  protected generateChart() {
    logger.debug(`Building chart ${this.chartDefinition.name} `);
    const traceNames = this.getTracesNames();
    logger.debug(`Unique trace names are `, [traceNames]);
    const traces: Dictionary<TraceData[]> = this.extractTraces();
    const data = this.buildPlotlyData(traces);
    const layout: Partial<Layout> = {
      title: this.chartTitle,
      showlegend: true,
      legend: {
        font: this.legendFont
      },
      barmode: 'stack',
      yaxis: {
        title: {
          text: this.getAxisText(),
          font: this.axisTitleFont,
          standoff: 40
        },
        fixedrange: true,
        type: 'category',
        tickfont: this.legendFont,
        automargin: true
      },
      xaxis: {
        title: {
          text: this.getMeasureText(),
          font: this.axisTitleFont
        },
        fixedrange: true,
        tickfont: this.legendFont,
      }
    };

    if (this.isCompactLayout()) {
      layout.margin = {
        t: 25,
        r: 0,
      };
    } else {
      layout.margin = {
        l: 200
      };
      (layout.yaxis.title as Partial<DataTitle>).standoff = 180;
    }

    const config = this.getDefaultConfig();

    newPlot(this.chartContainer.nativeElement, data, layout, config);
  }

  private buildPlotlyData(traces: Dictionary<TraceData[]>) {
    return Object.entries<TraceData[]>(traces).map(([key, serie], index) => {
      return this.buildOnePlotlyTrace(serie, index, key);
    });
  }

  private buildOnePlotlyTrace(serie: TraceData[], index: number, key: string) {
    const xValues = serie.map(s => s.xValue);
    const yLabels = serie.map(s => s.yLabel);
    let color: string;
    if (this.hasCustomColors()) {
      color = this.chartDefinition.colours[index];
    } else if (this.isColorConstrained(this.chartDefinition.axis[1])) {
      const rawLabel = serie[0].traceRawLabel?.toString();
      color = this.getColors([rawLabel], this.chartDefinition.axis[1])[0];
    } else {
      color = this.defaultColours[index];
    }

    let characterLength = 25;
    if (this.isCompactLayout()) {
      characterLength = 17;
    }

    /* [SQUASH-4158] Plotly doesn't currently have an option to do ellipsis on labels that are too long.
    / If we just truncate labels at a given string length, there can be duplicate truncated labels and plotly
    / concatenate the values of the duplicate truncated label before rendering chart. In order to keep integrity of
    / full label, we use a hack that consists of adding a span with css style 'display:none' to hide the extra characters
    / and avoid duplicate labels.
    */
    const truncatedLabels = yLabels.map(l => l.toString().length > characterLength ?
      [l.toString().slice(0, characterLength),
       '<span style="display:none">', l.toString().slice(characterLength), '</span>', '...'].join('') :
      l);

    const truncatedLegendLabel = key.length > characterLength ? [key.slice(0, characterLength), '...'].join('') : key;
    const serieData: any = {
      x: xValues,
      y: truncatedLabels,
      type: 'bar',
      name: truncatedLegendLabel,
      orientation: 'h',
      text: xValues,
      textposition: 'inside',
      insidetextanchor: 'middle',
      hoverinfo: 'none',
      textfont: this.legendFont,
      marker: {
        color
      },
    };
    return serieData;
  }

  private extractTraces(): Dictionary<TraceData[]> {
    const traceData = this.extractTraceData();
    const traces = _.groupBy<TraceData>(traceData, 'traceName');
    logger.debug('Series are regrouped and translated as :', [traces]);
    return traces;
  }

  private extractTraceData() {
    const yAxisDefinition = this.chartDefinition.axis[0];
    const yAxisConverter = this.chartsTranslateService.findConverter(yAxisDefinition, this.chartDefinition);
    logger.debug(`x axis definition is `, [yAxisDefinition]);
    const traceAxisDefinition = this.chartDefinition.axis[1];
    const traceAxisConverter = this.chartsTranslateService.findConverter(traceAxisDefinition, this.chartDefinition);
    logger.debug(`trace name definition is `, [traceAxisDefinition]);
    const values = Object.values(this.chartDefinition.series)[0];
    logger.debug(`values are `, [values]);
    const traceData: TraceData[] = this.chartDefinition.abscissa
      .map((tuple, index) => {
        return {
          traceName: tuple[1],
          yLabel: tuple[0],
          traceRawLabel: tuple[1],
          xValue: values[index]
        };
      })
      .map((value: TraceData) => ({
        traceName: traceAxisConverter.convertAxisLabels(traceAxisDefinition, [value.traceName])[0],
        yLabel: yAxisConverter.convertAxisLabels(yAxisDefinition, [value.yLabel])[0],
        traceRawLabel: value.traceRawLabel,
        xValue: value.xValue
      }));
    return traceData;
  }

  private getTracesNames(): string[] {
    const traceNames: string[] = this.chartDefinition.abscissa.map(tuple => tuple[0]);
    return _.uniq(traceNames);
  }
}

interface TraceData {
  traceName: string | Date;
  yLabel: string | Date;
  traceRawLabel: string | Date;
  xValue: number;
}
