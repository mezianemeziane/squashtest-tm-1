import {AxisLabelsConverter} from './axis-labels-converters';
import {AxisColumn} from 'sqtm-core';
import {Injectable} from '@angular/core';

@Injectable()
export class PassTroughAxisConverter implements AxisLabelsConverter {

  convertAxisLabels(axis: AxisColumn, rawLabels: any[]): string[] {
    return rawLabels.map(label => label != null ? label.toString() : '');
  }

  handleAxis(axis: AxisColumn): boolean {
    throw Error('Should not be called, PassTroughAxisConverter can handle everything and should be used as backup if no other converter can handle the data.');
  }

}
