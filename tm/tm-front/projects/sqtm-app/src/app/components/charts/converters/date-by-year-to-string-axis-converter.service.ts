import {AxisLabelsConverter} from './axis-labels-converters';
import {AxisColumn, ChartDataType, ChartOperation} from 'sqtm-core';
import {Injectable} from '@angular/core';
import {format} from 'date-fns';
import {TranslateService} from '@ngx-translate/core';
import {chartsLogger} from '../charts.logger';

const logger = chartsLogger.compose('DateByMonthToStringAxisConverter');

@Injectable()
export class DateByYearToStringAxisConverter implements AxisLabelsConverter {


  constructor(private translateService: TranslateService) {
  }

  static parseDateByYear(rawLabel): Date {
    const year = rawLabel.toString();
    return new Date(year);
  }

  convertAxisLabels(axis: AxisColumn, rawLabels: any[]): string[] {
    logger.debug(`Converting labels for axis ${axis.label}. Raw labels are `, [rawLabels]);
    return rawLabels.map(rawLabel => {
      if (rawLabel) {
        const date = DateByYearToStringAxisConverter.parseDateByYear(rawLabel);
        // date = setDay(date, 1);
        logger.debug('Formatted date', [date]);
        return format(date, 'yyyy');
      }
      return this.translateService.instant('sqtm-core.generic.label.never');
    });
  }

  handleAxis(axis: AxisColumn): boolean {
    // tslint:disable-next-line:max-line-length
    logger.debug(`Testing if DateByDayAxisConverter can handle ${axis.label}. Datatype : ${axis.column.dataType}. Operation : ${axis.operation}`, [axis]);
    return axis.column.dataType === ChartDataType.DATE
      && axis.operation === ChartOperation.BY_YEAR;
  }

}
