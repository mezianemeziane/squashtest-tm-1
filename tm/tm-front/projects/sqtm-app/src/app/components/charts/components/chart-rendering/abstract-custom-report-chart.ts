import {AfterViewInit, Directive, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  AxisColumn,
  ChartDataType,
  ChartDefinitionModel,
  ChartDefinitionService,
  ChartOperation,
  ChartType,
  CustomField,
  ExecutionStatus,
  MeasureColumn,
  ReferentialDataService,
  RequirementCriticality,
  RequirementStatus,
  TestCaseStatus,
  TestCaseWeight
} from 'sqtm-core';
import {ChartsTranslateService} from '../../services/charts-translate.service';
import * as CssElementQuery from 'css-element-queries';
import {ResizeSensor} from 'css-element-queries';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import * as Plotly from 'plotly.js';
import {Layout} from 'plotly.js';
import {DateByWeekToStringAxisConverter} from '../../converters/date-by-week-to-string-axis-converter.service';
import {DateByDayToStringAxisConverter} from '../../converters/date-by-day-to-string-axis-converter.service';
import {DateByMonthToStringAxisConverter} from '../../converters/date-by-month-to-string-axis-converter.service';
import {DateByYearToStringAxisConverter} from '../../converters/date-by-year-to-string-axis-converter.service';
import {TranslateService} from '@ngx-translate/core';
import {ResizeEvent} from '../../../statistics/abstract-statistics-panel.component';
import {CHART_COLUMN_PROTO_I18N_KEY} from '../../charts.constants';
import {registerPlotlyLocale} from '../../plotly.utils';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractCustomReportChart implements OnInit, AfterViewInit, OnDestroy {

  private _chartDefinition: ChartDefinitionModel;
  protected readonly locale: string;

  protected readonly defaultColours = ['277da1', 'f9c74f', 'f9844a', '43aa8b', '577590', 'f94144', '90be6d', 'f3722c', '4d908e', 'f8961e'];

  /**
   * These columns prototypes have colors hard coded into enums
   */
  protected readonly colorsConstrainedColumnPrototypes = [
    'REQUIREMENT_STATUS',
    'REQUIREMENT_VERSION_STATUS',
    'REQUIREMENT_CRITICALITY',
    'REQUIREMENT_VERSION_CRITICALITY',
    'TEST_CASE_STATUS',
    'TEST_CASE_IMPORTANCE',
    'ITEM_TEST_PLAN_STATUS',
    'EXECUTION_STATUS'
  ];

  get chartDefinition(): ChartDefinitionModel {
    return this._chartDefinition;
  }

  get chartTitle(): any {
    return {
      text: `<b>${this.chartDefinition.name}</b>`,
      font: {
        family: 'Segoe UI, Roboto',
        size: this.layout === 'compact' ? 16 : 18,
        color: '#62737F'
      }
    };
  }

  get legendFont() {
    return this.makeFont(12, 14);
  }

  get axisTitleFont() {
    return this.makeFont(13, 16);
  }

  get compactMargin() {
    return {
      t: 25,
      r: 0,
      l: 0,
      b: 0
    };
  }

  get baseLayout(): Partial<Layout> {
    const layout: Partial<Layout> = {
      title: this.chartTitle,
      legend: {
        font: this.legendFont
      },
    };
    if (this.isCompactLayout()) {
      layout.margin = this.compactMargin;
    }

    return layout;
  }

  @Input()
  set chartDefinition(value: ChartDefinitionModel) {
    if (value.type !== this.getHandledType()) {
      throw Error(`This chart handle only ${this.getHandledType()}`);
    }
    this._chartDefinition = value;
    if (this.chartInitialized) {
      this.generateChart();
    }
  }

  @Input()
  layout: 'full' | 'compact' = 'full';

  @ViewChild('chartContainer', {read: ElementRef})
  protected chartContainer: ElementRef;

  private chartInitialized = false;

  private customFields: CustomField[];

  protected resizeSensor: ResizeSensor;
  protected _resizeEvent = new Subject<ResizeEvent>();
  protected resizeEvent$ = this._resizeEvent.asObservable().pipe();

  protected unsub$ = new Subject<void>();

  protected constructor(protected chartsTranslateService: ChartsTranslateService,
                        protected chartDefinitionService: ChartDefinitionService,
                        protected referentialDataService: ReferentialDataService,
                        protected translateService: TranslateService) {
    this.locale = this.translateService.getBrowserLang();
    registerPlotlyLocale(this.locale);
  }

  ngOnInit(): void {
    this.chartDefinitionService.downloadChart$.pipe(
      takeUntil(this.unsub$)
    ).subscribe(() => this.downloadChart());

    this.referentialDataService.customFields$.pipe(
      takeUntil(this.unsub$)
    ).subscribe((customFields: CustomField[]) => this.customFields = customFields);
  }

  protected abstract getHandledType(): ChartType;

  protected abstract generateChart(): void;

  ngAfterViewInit(): void {
    this.generateChart();

    const that = this;
    this.resizeSensor = new CssElementQuery.ResizeSensor(this.chartContainer.nativeElement, function ($event: ResizeEvent) {
      that._resizeEvent.next($event);
    });
    this.resizeEvent$.pipe(
      takeUntil(this.unsub$)
    ).subscribe(resize => {
      Plotly.relayout(this.chartContainer.nativeElement, {height: resize.height, width: resize.width});
    });
    this.chartInitialized = true;
  }

  ngOnDestroy(): void {
    if (this.resizeSensor) {
      this.resizeSensor.detach();
    }
    this.unsub$.next();
    this.unsub$.complete();
  }

  protected downloadChart() {
    Plotly.downloadImage(this.chartContainer.nativeElement,
      {
        format: 'jpeg',
        filename: `${this.chartDefinition.name}`,
        height: this.chartContainer.nativeElement.height,
        width: this.chartContainer.nativeElement.width
      });
  }

  protected isDateChart() {
    const dataType = this.chartDefinition.axis[0].column.dataType;
    return dataType === ChartDataType.DATE || dataType === ChartDataType.DATE_AS_STRING;
  }

  protected convertDates(axisColumn: AxisColumn, rawLabels: number[]) {
    const operation = axisColumn.operation;
    switch (operation) {
      case ChartOperation.BY_DAY:
        return this.convertDayDates(rawLabels);
      case ChartOperation.BY_WEEK:
        return this.convertWeekDates(rawLabels);
      case ChartOperation.BY_MONTH:
        return this.convertMonthDates(rawLabels);
      case ChartOperation.BY_YEAR:
        return this.convertYearDates(rawLabels);
      default:
        throw Error('Unknown operation : ' + operation);
    }
  }

  private convertWeekDates(rawLabels: number[]) {
    return rawLabels.map(rawLabel => DateByWeekToStringAxisConverter.parseDateByWeek(rawLabel));
  }

  private convertDayDates(rawLabels: number[]) {
    return rawLabels.map(rawLabel => DateByDayToStringAxisConverter.parseDateByDay(rawLabel));
  }

  private convertMonthDates(rawLabels: number[]) {
    return rawLabels.map(rawLabel => DateByMonthToStringAxisConverter.parseDateByMonth(rawLabel));
  }

  private convertYearDates(rawLabels: number[]) {
    return rawLabels.map(rawLabel => DateByYearToStringAxisConverter.parseDateByYear(rawLabel));
  }

  protected getDefaultConfig(): Partial<Plotly.Config> {
    return {
      staticPlot: false,
      responsive: true,
      displayModeBar: false,
      editable: false,
      doubleClick: 'reset',
      displaylogo: false,
      scrollZoom: false,
      locale: this.locale,
      showTips: false
    };
  }

  protected getAxisText() {
    return this.isCuf(this.chartDefinition.axis[0]) ? this.findCufLabel(this.chartDefinition.axis[0].cufId) :
      this.translateService.instant(`${CHART_COLUMN_PROTO_I18N_KEY}${this.chartDefinition.axis[0].column.label}`);
  }

  protected getMeasureText() {
    return this.isCuf(this.chartDefinition.measures[0]) ? this.findCufLabel(this.chartDefinition.measures[0].cufId) :
      this.translateService.instant(`${CHART_COLUMN_PROTO_I18N_KEY}${this.chartDefinition.measures[0].column.label}`);
  }

  private findCufLabel(cufId: number) {
    const customField = this.customFields.find(cuf => cuf.id === cufId);
    return customField.label;
  }

  private isCuf(column: MeasureColumn | AxisColumn) {
    return column.cufId;
  }

  protected isCompactLayout() {
    return this.layout === 'compact';
  }

  private makeFont(compact: number, full: number) {
    return {
      family: 'Segoe UI, Roboto',
      size: this.layout === 'compact' ? compact : full,
    };
  }

  protected hasNeverDateValue(rawLabels: any[]): boolean {
    return rawLabels.filter(label => label === null).length > 0;
  }

  protected filterNeverDateValue(rawLabels: any[]): any[] {
    return rawLabels.filter(label => label !== null);
  }

  protected extractNeverDateValue(rawLabels: any[], yValues: any[]): any {
    const index = rawLabels.indexOf(null);
    return yValues[index];
  }

  protected getColors(rawLabels: string[], axis: AxisColumn): string[] {
    const userDefinedColours = this.chartDefinition.colours;
    if (userDefinedColours?.length > 0) {
      return userDefinedColours.map((color, index) => {
        if (color && color.length > 0) {
          return color;
        } else {
          return this.defaultColours[index];
        }
      });
    }
    const column = axis.column;
    const label = column.label;

    if (this.colorsConstrainedColumnPrototypes.includes(label)) {
      return this.getColorForConstrainedColumnPrototypes(label, rawLabels);
    } else {
      return this.buildDefaultColors(rawLabels.length);
    }
  }

  private buildDefaultColors(numberOfColorsRequired: number): string[] {
    const numberOfDefaultColors = this.defaultColours.length;
    const numberOfDefaultColorRepetition = Math.ceil(numberOfColorsRequired / numberOfDefaultColors);
    const colors = [];
    for (let i = 0; i < numberOfDefaultColorRepetition; i++) {
      colors.push(...this.defaultColours);
    }
    return colors;
  }

  private getColorForConstrainedColumnPrototypes(label: string, rawLabels: string[]) {
    switch (label) {
      case 'REQUIREMENT_STATUS':
      case 'REQUIREMENT_VERSION_STATUS':
        return rawLabels.map(rawLabel => RequirementStatus[rawLabel].chartColor);
      case 'REQUIREMENT_CRITICALITY':
      case 'REQUIREMENT_VERSION_CRITICALITY':
        return rawLabels.map(rawLabel => RequirementCriticality[rawLabel].chartColor);
      case 'TEST_CASE_STATUS':
        return rawLabels.map(rawLabel => TestCaseStatus[rawLabel].chartColor);
      case 'TEST_CASE_IMPORTANCE':
        return rawLabels.map(rawLabel => TestCaseWeight[rawLabel].chartColor);
      case 'ITEM_TEST_PLAN_STATUS':
      case 'EXECUTION_STATUS':
        return rawLabels.map(rawLabel => ExecutionStatus[rawLabel].color);
      default:
        throw Error('No colors for column prototype ' + label);
    }
  }

  protected isColorConstrained(axi: AxisColumn) {
    return this.colorsConstrainedColumnPrototypes.includes(axi.column.label);
  }

  protected hasCustomColors() {
    return this.chartDefinition.colours?.length > 0;
  }
}
