import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ChartDefinitionModel} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-custom-chart',
  templateUrl: './custom-chart.component.html',
  styleUrls: ['./custom-chart.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomChartComponent implements OnInit {

  @Input()
  chartDefinition: ChartDefinitionModel;

  @Input()
  layout: 'full' | 'compact' = 'full';

  constructor() {
  }

  ngOnInit(): void {
  }

}
