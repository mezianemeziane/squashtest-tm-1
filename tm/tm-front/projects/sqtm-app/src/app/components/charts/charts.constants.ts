import {InjectionToken} from '@angular/core';

export const AXIS_CONVERTERS = new InjectionToken('Injection token for axis chart converters');
export const CHART_ENTITY_I18N_KEY = `sqtm-core.custom-report-workspace.chart.entity.`;
export const CHART_COLUMN_PROTO_I18N_KEY = `sqtm-core.custom-report-workspace.chart.columns.`;
