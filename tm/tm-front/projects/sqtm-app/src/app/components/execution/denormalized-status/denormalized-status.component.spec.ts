import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DenormalizedStatusComponent } from './denormalized-status.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';

describe('DenormalizedStatusComponent', () => {
  let component: DenormalizedStatusComponent;
  let fixture: ComponentFixture<DenormalizedStatusComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ DenormalizedStatusComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenormalizedStatusComponent);
    component = fixture.componentInstance;
    component.status = 'WORK_IN_PROGRESS';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
