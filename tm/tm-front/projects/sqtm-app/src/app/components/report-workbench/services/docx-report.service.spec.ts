import {TestBed} from '@angular/core/testing';

import {DocxReportService} from './docx-report.service';
import {AppTestingUtilsModule} from '../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('DocxReportService', () => {
  let service: DocxReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppTestingUtilsModule,
        HttpClientTestingModule
      ],
      providers: [
        DocxReportService
      ]
    });
    service = TestBed.inject(DocxReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
