import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {DialogReference} from 'sqtm-core';
import {JasperReportDialogConfiguration} from './jasper-report-dialog.configuration';
import {JasperReportService} from '../../services/jasper-report.service';

@Component({
  selector: 'sqtm-app-jasper-report-dialog',
  templateUrl: './jasper-report-dialog.component.html',
  styleUrls: ['./jasper-report-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [JasperReportService]
})
export class JasperReportDialogComponent implements OnInit, OnDestroy {

  data: JasperReportDialogConfiguration;

  formats: string[];

  selectedFormat: string;

  selectedViewIndex = 0;

  constructor(private dialogReference: DialogReference<JasperReportDialogConfiguration, void>,
              public jasperReportService: JasperReportService) {
    this.data = dialogReference.data;
  }

  ngOnInit(): void {
    this.doUpdateFormat(0);
  }

  ngOnDestroy(): void {
    this.jasperReportService.complete();
  }

  handleDownload() {
    const reportView = this.data.report.views[this.selectedViewIndex];
    this.jasperReportService.downloadReport(
      this.data.report.id,
      this.selectedViewIndex,
      this.data.params,
      this.selectedFormat,
      reportView.reportFileName);
  }

  updateFormats(index: number) {
    this.doUpdateFormat(index);
  }

  private doUpdateFormat(reportViewIndex: number) {
    this.formats = this.data.report.views[reportViewIndex].formats;
    this.selectedFormat = this.formats[0];
  }
}
