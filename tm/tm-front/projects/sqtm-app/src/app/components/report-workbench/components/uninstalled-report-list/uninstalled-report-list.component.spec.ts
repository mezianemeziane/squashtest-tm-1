import {ComponentFixture, TestBed} from '@angular/core/testing';

import {UninstalledReportListComponent} from './uninstalled-report-list.component';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {DialogService} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('UninstalledReportListComponent', () => {
  let component: UninstalledReportListComponent;
  let fixture: ComponentFixture<UninstalledReportListComponent>;
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({

      imports: [AppTestingUtilsModule, TranslateModule.forRoot()],
      declarations: [UninstalledReportListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: DialogService, useValue: dialogService}
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UninstalledReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
