import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTestCaseTreePickerComponent } from './form-test-case-tree-picker.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('FormTestCaseTreePickerComponent', () => {
  let component: FormTestCaseTreePickerComponent;
  let fixture: ComponentFixture<FormTestCaseTreePickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormTestCaseTreePickerComponent ],
      imports: [OverlayModule, HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTestCaseTreePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
