import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ReportWorkbenchComponent} from './report-workbench.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DocxReportService} from '../../services/docx-report.service';
import {OverlayModule} from '@angular/cdk/overlay';
import {TranslateModule} from '@ngx-translate/core';

describe('ReportWorkbenchComponent', () => {
  let component: ReportWorkbenchComponent;
  let fixture: ComponentFixture<ReportWorkbenchComponent>;
  const docxMock = jasmine.createSpyObj(['generateReport']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, AppTestingUtilsModule,
        HttpClientTestingModule, RouterTestingModule,
        OverlayModule, TranslateModule.forRoot()],
      declarations: [ReportWorkbenchComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: DocxReportService,
          useValue: docxMock
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportWorkbenchComponent);
    component = fixture.componentInstance;
    component.availableReports = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
