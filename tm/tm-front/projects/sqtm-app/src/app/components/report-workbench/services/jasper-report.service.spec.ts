import {TestBed} from '@angular/core/testing';

import {JasperReportService} from './jasper-report.service';
import {AppTestingUtilsModule} from '../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('JasperReportService', () => {
  let service: JasperReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule],
      providers: [JasperReportService]
    });
    service = TestBed.inject(JasperReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
