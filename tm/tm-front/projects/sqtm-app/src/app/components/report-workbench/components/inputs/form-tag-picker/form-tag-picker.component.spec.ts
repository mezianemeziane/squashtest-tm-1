import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTagPickerComponent } from './form-tag-picker.component';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('FormTagPickerComponent', () => {
  let component: FormTagPickerComponent;
  let fixture: ComponentFixture<FormTagPickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, HttpClientTestingModule, NzSelectModule, NoopAnimationsModule],
      declarations: [ FormTagPickerComponent ],
      schemas:[NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTagPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
