import {Inject, Injectable, NgZone} from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';
import {CORE_MODULE_CONFIGURATION, Report, RestService, SqtmCoreModuleConfiguration} from 'sqtm-core';
import {HttpClient} from '@angular/common/http';
import {catchError, concatMap, map} from 'rxjs/operators';
import {customReportWorkspaceLogger} from '../../../pages/custom-report-workspace/custom-report-workspace.logger';
import Docxtemplater from 'docxtemplater';
import {BehaviorSubject, of} from 'rxjs';
import {saveAs} from 'file-saver';

declare const openXml: any;
const logger = customReportWorkspaceLogger.compose('DocxReportService');

@Injectable()
export class DocxReportService {
  private _generatingReportSubject = new BehaviorSubject(false);
  public generatingReport$ = this._generatingReportSubject.asObservable();

  _keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';


  constructor(@Inject(APP_BASE_HREF) private baseHref: string,
              @Inject(CORE_MODULE_CONFIGURATION) private sqtmCoreModuleConfiguration: SqtmCoreModuleConfiguration,
              private http: HttpClient,
              private ngZone: NgZone,
              private restService: RestService) {
  }

  generateReport(report: Report | string, params: any) {
    const reportId = typeof report === 'string' ? report : report.id;
    this.emitStartGeneratingReport();
    const templateUrl = `${this.baseHref}${this.sqtmCoreModuleConfiguration.backendRootUrl}/reports/${reportId}/views/0/docxtemplate`;
    // {responseType: 'arraybuffer' as any} is mandatory to properly handle binary data
    this.http.get<ReportPayload>(templateUrl, {responseType: 'arraybuffer' as any}).pipe(
      catchError(err => {
        this.emitStopGeneratingReport();
        return of(err);
      }),
      concatMap(template => this.restService.get(['reports', reportId, 'views', '0', 'data', 'docx'], params).pipe(
        map((data: any) => ({template, data})),
        catchError(err => {
          this.emitStopGeneratingReport();
          return of(err);
        })
      )),
    ).subscribe(({template, data}) => {
      logger.debug(`Data for template report ${reportId}`, [data]);
      const doc = new Docxtemplater(template);
      try {
        doc.setData(data.data).render();
      } catch (error) {
        this.logErrors(error);
        this.emitStopGeneratingReport();
        throw error;
      }
      const output = doc.getZip().generate({type: 'base64'}); // Output the document using Data-URI

      const html = data.html;

      const docx = new openXml.OpenXmlPackage(output);

      for (let i = 0; i < html.length; i++) {

        const alt_chunk_id = 'toto' + i;
        const alt_chunk_uri = '/word/' + i + '.html';
        // Add Alternative Format Import Part to document
        // we can't use btoa method to encode html string
        // because we have some errors when an user copy/paste "'" character
        // in description from Word [SQUASH-3407]. So we use old stuff of 1.2x of SquashTm.
        docx.addPart(alt_chunk_uri, 'text/html', 'base64', this.encode(html[i]));
        // Add Alternative Format Import Relationship to the document
        docx.mainDocumentPart().addRelationship(alt_chunk_id, openXml.relationshipTypes.alternativeFormatImport, alt_chunk_uri, 'Internal');
      }

      const theContent = docx.saveToBlob();
      saveAs(theContent, `${data.fileName}.${data.format}`);
      this.emitStopGeneratingReport();
    });
  }

  private encode(input) {
    let output = '';
    let chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    let i = 0;

    input = this.utf8_encode(input);

    while (i < input.length) {

      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      // tslint:disable-next-line:no-bitwise
      enc1 = chr1 >> 2;
      // tslint:disable-next-line:no-bitwise
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      // tslint:disable-next-line:no-bitwise
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      // tslint:disable-next-line:no-bitwise
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }

      output = output +
        this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
        this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

    }

    return output;
  }

  private utf8_encode(string) {
    string = string.replace(/\r\n/g, '\n');
    let utftext = '';

    for (let n = 0; n < string.length; n++) {

      const c = string.charCodeAt(n);

      if (c < 128) {
        utftext += String.fromCharCode(c);
      } else if ((c > 127) && (c < 2048)) {
        // tslint:disable-next-line:no-bitwise
        utftext += String.fromCharCode((c >> 6) | 192);
        // tslint:disable-next-line:no-bitwise
        utftext += String.fromCharCode((c & 63) | 128);
      } else {
        // tslint:disable-next-line:no-bitwise
        utftext += String.fromCharCode((c >> 12) | 224);
        // tslint:disable-next-line:no-bitwise
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        // tslint:disable-next-line:no-bitwise
        utftext += String.fromCharCode((c & 63) | 128);
      }

    }

    return utftext;
  }


  private logErrors(errors) {
    console.log(JSON.stringify({error: errors}, this.replaceErrors));
    if (errors.properties && errors.properties.errors instanceof Array) {
      const errorMessages = errors.properties.errors
        .map(function (error) {
          return error.properties.explanation;
        })
        .join('\n');
      console.log('errorMessages', errorMessages);
      // errorMessages is a humanly readable message looking like this :
      // 'The tag beginning with "foobar" is unopened'
    }
  }

  private replaceErrors(key, value) {
    if (value instanceof Error) {
      return Object.getOwnPropertyNames(value).reduce(function (
        error,
        key
        ) {
          error[key] = value[key];
          return error;
        },
        {});
    }
    return value;
  }

  private emitStartGeneratingReport() {
    this._generatingReportSubject.next(true);
  }

  private emitStopGeneratingReport() {
    this._generatingReportSubject.next(false);
  }

  complete() {
    this._generatingReportSubject.complete();
  }
}

interface ReportPayload {
  data: any;
  html: string[];
  fileName: string;
  format: string;
}
