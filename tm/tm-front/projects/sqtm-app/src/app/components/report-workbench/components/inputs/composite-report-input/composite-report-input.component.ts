import {ChangeDetectionStrategy, Component, Input, OnInit, ViewChild} from '@angular/core';
import {
  CustomField,
  ProjectData,
  ReferentialDataService,
  ReportInputType,
  ReportOption,
  ReportOptionGroup,
  InputType
} from 'sqtm-core';
import {FormGroup, ValidationErrors} from '@angular/forms';
import {FormProjectPickerComponent} from '../form-project-picker/form-project-picker.component';
import {concatMap, map, take} from 'rxjs/operators';
import {combineLatest, Observable, of} from 'rxjs';


@Component({
  selector: 'sqtm-app-composite-report-input',
  templateUrl: './composite-report-input.component.html',
  styleUrls: ['./composite-report-input.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompositeReportInputComponent implements OnInit {

  @Input()
  reportInput: ReportOptionGroup;

  @Input()
  subFormGroup: FormGroup;

  @Input()
  errors: ValidationErrors;

  @ViewChild(FormProjectPickerComponent)
  projectPicker: FormProjectPickerComponent;

  ReportInputType = ReportInputType;

  constructor(
    public referentialDataService: ReferentialDataService
  ) {
  }

  ngOnInit(): void {

  }

  handleOpen() {

  }

  getDebug() {
    return Object.keys(this.subFormGroup.controls);
  }

  getErrors() {
    return Object.keys(this.subFormGroup.errors || {});
  }

  showControl($event: any) {
    console.log($event);
  }

  optionIsDisabled(option: ReportOption) {
    if (option.disabledBy != null) {
      const disabledBy = option.disabledBy;
      const control = this.subFormGroup.controls[disabledBy];
      const value = control.value as boolean;
      return !value;
    }
    return false;
  }

  isTagPickerVisible(option: ReportOption) {
    if (option.contentType === ReportInputType.TAG_PICKER) {
      return this.referentialDataService.filteredProjects$.pipe(
        take(1),
        map((filteredProjects: ProjectData[]) => filteredProjects
          .map(proj => this.referentialDataService.findCustomFieldByProjectIdAndDomain(proj.id, option.bindableEntity))
        ),
        concatMap((obsArray: Observable<CustomField[]>[]) => combineLatest(obsArray)),
        map(doubleArray => doubleArray.flat()),
        map(cufs => cufs.some((cuf: CustomField) => cuf.inputType === InputType.TAG))
      );
    } else {
      return of(true);
    }
  }
}
