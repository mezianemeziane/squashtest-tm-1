import {ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {createStore, RequirementCriticalityKeys, RequirementStatusKeys, RestService} from 'sqtm-core';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'sqtm-app-requirement-version-details',
  templateUrl: './requirement-version-details.component.html',
  styleUrls: ['./requirement-version-details.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementVersionDetailsComponent implements OnInit, OnDestroy {

  @Input()
  requirementVersionId: number;

  private store = createStore<RequirementVersionDetailsComponentData>({
    requirementVersion: null,
    uiState: {loading: true}
  });

  componentData$ = this.store.state$;

  constructor(private readonly restService: RestService, private sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.restService
      .get<RequirementVersionDetails>(['detailed-test-step-view', 'requirement-version', this.requirementVersionId.toString()])
      .subscribe((requirementVersion) => {
        this.store.commit({
          requirementVersion,
          uiState: {loading: false}
        });
      });
  }

  ngOnDestroy(): void {
    this.store.complete();
  }

  getDescription(description: string) {
    return this.sanitizer.bypassSecurityTrustHtml(description);
  }

}

interface RequirementVersionDetailsComponentData {
  requirementVersion: RequirementVersionDetails;
  uiState: {
    loading: boolean;
  };
}

interface RequirementVersionDetails {
  criticality: RequirementCriticalityKeys;
  status: RequirementStatusKeys;
  category: number;
  description: string;
  versionNumber: number;
}
