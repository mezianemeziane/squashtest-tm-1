import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {AbstractCellRendererComponent, DataRow, DataRowOpenState, GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-open-close-cell-renderer',
  templateUrl: './open-close-cell-renderer.component.html',
  styleUrls: ['./open-close-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OpenCloseCellRendererComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  ngOnInit(): void {
  }

  iconName(dataRow: DataRow) {
    if (dataRow.state === DataRowOpenState.open) {
      return 'down';
    } else {
      return 'right';
    }
  }

  toggleRow($event, dataRow: DataRow) {
    if (dataRow.state === DataRowOpenState.closed) {
      this.grid.openRow(this.row.id);
    } else if (dataRow.state === DataRowOpenState.open) {
      this.grid.closeRow(this.row.id);
    }
  }

}
