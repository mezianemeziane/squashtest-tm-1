import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DeleteCoverageCellRendererComponent} from './delete-coverage-cell-renderer.component';
import {GridTestingModule} from 'sqtm-core';
import {DetailedTestStepViewService} from '../../../../../pages/detailed-views/detailed-test-step-view/services/detailed-test-step-view.service';
import {OverlayModule} from '@angular/cdk/overlay';
import {of} from 'rxjs';
import {DetailedTestStepViewComponentData} from '../../../../../pages/detailed-views/detailed-test-step-view/containers/detailed-test-step-view/detailed-test-step-view.component';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {STEP_VIEW_REQUIREMENT_LINK_SERVICE} from '../../../detailed-step-view.constant.ts.constant';

describe('DeleteCoverageCellRendererComponent', () => {
  let component: DeleteCoverageCellRendererComponent;
  let fixture: ComponentFixture<DeleteCoverageCellRendererComponent>;

  const viewService = jasmine.createSpyObj<DetailedTestStepViewService>(['load']);
  viewService.componentData$ = of({permissions: {canWrite: true}} as DetailedTestStepViewComponentData);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, OverlayModule, AppTestingUtilsModule],
      declarations: [DeleteCoverageCellRendererComponent],
      providers: [
        {
          provide: DetailedTestStepViewService,
          useValue: viewService
        },
        {
          provide: STEP_VIEW_REQUIREMENT_LINK_SERVICE,
          useExisting: DetailedTestStepViewService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteCoverageCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
