import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  Renderer2,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {GridsterComponent, GridsterConfig, GridsterItem} from 'angular-gridster2';
import {
  AbstractSqtmDragAndDropEvent,
  ChartBinding,
  CustomDashboardBinding,
  CustomDashboardService,
  CustomReportPermissions,
  DataRow,
  DraggableDropZoneDirective,
  DroppedNewItemInDashboard,
  EntityRowReference,
  isChartBinding,
  isReportBinding,
  ReadOnlyPermissions, ReportBinding,
  SqtmDragEnterEvent,
  SqtmDropEvent,
  SquashTmDataRowType,
  toEntityRowReference
} from 'sqtm-core';
import {customReportWorkspaceTreeId} from '../../../../pages/custom-report-workspace/custom-report-workspace.constant';
import {Observable, Subject} from 'rxjs';
import {debounceTime, finalize, map, takeUntil, tap} from 'rxjs/operators';
import {dashboardLogger} from '../dashboard.logger';
import * as CssElementQuery from 'css-element-queries';
import {ResizeSensor} from 'css-element-queries';

const logger = dashboardLogger.compose('CustomDashboardComponent');

/**
 * Architecture note. For this component we don't use the state normalized flow.
 * Aka we don't have a service containing the whole state in a behavior subject.
 * The gridster component isn't a good fit for this kind of flow, but it perfectly fit to a classic single component angular flow.
 * So we have a plain old mutable state and some cdRef ChangeDetectorRef ^^
 */
@Component({
  selector: 'sqtm-app-custom-dashboard',
  templateUrl: './custom-dashboard.component.html',
  styleUrls: ['./custom-dashboard.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomDashboardComponent implements OnInit, OnDestroy, AfterViewInit {

  public readonly DROP_ZONE_PREFIX = 'drop-zone-custom-dashboard-binding-';

  // the mutable dashboard used by gridster to track position of items
  dashboard: GridsterItem[];

  options: GridsterConfig;

  @ViewChild(GridsterComponent)
  gridster: GridsterComponent;

  @ViewChildren(DraggableDropZoneDirective)
  itemsDropZones: QueryList<DraggableDropZoneDirective>;

  @Input()
  permissions: CustomReportPermissions | ReadOnlyPermissions = new ReadOnlyPermissions();

  @Input()
  dashboardId: number;

  @Output()
  dashboardCleared = new EventEmitter<void>();

  private gridsterItemSubject = new Subject<GridsterItem[]>();
  private gridsterItemChanged$;

  private resizeSensor: ResizeSensor;
  private _resizeEvent = new Subject<ResizeEvent>();
  private resizeEvent$ = this._resizeEvent.asObservable().pipe();

  private unsub$ = new Subject<void>();

  // When an input come from the parent we override all gridster transient state.
  // Occurs at load time and after a first item is added in dashboard.
  @Input()
  set chartBindings(bindings: CustomDashboardBinding[]) {
    this.dashboard = bindings.map(binding => {
      const item: GridsterItem = this.convertToBaseGridsterItem(binding);
      if (isChartBinding(binding)) {
        this.appendChart(binding, item);
      } else if (isReportBinding(binding)) {
        this.appendReport(binding, item);
      }
      return item;
    });
    logger.debug(`setting gridster in dashboard`, [this.dashboard]);
  }

  @ViewChild('container', {read: ElementRef})
  private container: ElementRef;

  @ViewChild('loadingOverlay', {read: ElementRef})
  private loadingOverlay: ElementRef;

  constructor(private renderer: Renderer2, private dashboardService: CustomDashboardService, private cdRef: ChangeDetectorRef) {
  }

  ngOnDestroy(): void {
    this.gridsterItemSubject.complete();
    this.unsub$.next();
    this.unsub$.complete();
    if (this.resizeSensor) {
      this.resizeSensor.detach();
    }
  }

  ngOnInit() {
    this.options = {
      draggable: {
        enabled: this.permissions.canWrite,
        start: () => this.showGrid(),
        stop: () => this.hideGrid(),
      },
      resizable: {
        enabled: this.permissions.canWrite,
        start: () => this.showGrid(),
        stop: () => this.hideGrid(),
      },
      swap: this.permissions.canWrite,
      minRows: 3,
      maxRows: 3,
      minCols: 4,
      maxCols: 4,
      disableScrollVertical: true,
      disableScrollHorizontal: true,
      itemChangeCallback: () => this.itemChanged(),
    };
  }

  ngAfterViewInit(): void {
    this.initItemChanged();
    const that = this;
    this.resizeSensor = new CssElementQuery.ResizeSensor(this.container.nativeElement, function ($event: ResizeEvent) {
      that._resizeEvent.next($event);
    });
    this.resizeEvent$.pipe(
      takeUntil(this.unsub$),
    ).subscribe(() => this.gridster.resize());
  }

  private initItemChanged() {
    this.gridsterItemChanged$ = this.gridsterItemSubject.pipe(
      tap(() => this.beginAsync()),
      debounceTime(5),
      map(items => items.map(i => this.convertToBinding(i)))
    ).subscribe((dashboard: CustomDashboardBinding[]) => {
      logger.info('New bindings in dashboard ', [dashboard]);
      this.dashboardService.changeItemsPosition(dashboard).pipe(
        finalize(() => this.endAsync())
      ).subscribe();
    });
  }

  // gridster position start at 0 but in legacy squashTm dashboard position started at 1...
  // so we convert position and will convert back when adding/moving items
  private convertToBaseGridsterItem(binding: CustomDashboardBinding): GridsterItem {
    return {
      x: binding.col - 1,
      y: binding.row - 1,
      cols: binding.sizeX,
      rows: binding.sizeY,
      id: binding.id
    };
  }

  private appendChart(binding: ChartBinding, item: GridsterItem) {
    item.chartInstance = binding.chartInstance;
    item.chartDefinitionId = binding.chartDefinitionId;
  }

  private appendReport(binding: ReportBinding, item: GridsterItem) {
    item.reportDefinitionId = binding.reportDefinitionId;
    item.reportInstance = binding.reportInstance;
  }

  trackByBindingId(index: number, item: GridsterItem) {
    return item.id;
  }

  dropNewItem(dropEvent: SqtmDropEvent) {
    if (this.dragIsFromCustomReportTree(dropEvent)) {
      const dataRow: DataRow = this.extractFirstEligibleRow(dropEvent);
      if (dataRow && this.permissions && this.permissions.canWrite && this.gridsterHasAvailablePosition()) {
        const gridsterItem = this.findFirstPossiblePosition();
        const droppedItem = this.convertIntoSquashDashboardDroppedItem(dataRow, gridsterItem);
        if (this.dataRowIsChart(dataRow)) {
          this.addChart(droppedItem);
        } else if (this.dataRowIsReport(dataRow)) {
          this.addReport(droppedItem);
        }
      }
    }
  }

  private addChart(droppedItem: DroppedNewItemInDashboard) {
    this.beginAsync();
    this.dashboardService.addNewChartInDashboard(this.dashboardId, droppedItem).pipe(
      finalize(() => {
        this.unmarkAllDropZones();
        this.endAsync();
      })
    ).subscribe(
      (persistedBinding) => {
        const newItem = this.convertToBaseGridsterItem(persistedBinding);
        this.appendChart(persistedBinding, newItem);
        this.dashboard.push(newItem);
        this.cdRef.detectChanges();
      });
  }

  private addReport(droppedItem: DroppedNewItemInDashboard) {
    this.beginAsync();
    this.dashboardService.addNewReportInDashboard(this.dashboardId, droppedItem).pipe(
      finalize(() => {
        this.unmarkAllDropZones();
        this.endAsync();
      })
    ).subscribe(
      (persistedBinding) => {
        const newItem = this.convertToBaseGridsterItem(persistedBinding);
        this.appendReport(persistedBinding, newItem);
        this.dashboard.push(newItem);
        this.cdRef.detectChanges();
      });
  }


  private convertIntoSquashDashboardDroppedItem(dataRow: DataRow, gridsterItem: GridsterItem) {
    const entityRowReference: EntityRowReference = toEntityRowReference(dataRow.id);
    const droppedItem: DroppedNewItemInDashboard = {
      col: gridsterItem.x + 1,
      row: gridsterItem.y + 1,
      sizeX: 1,
      sizeY: 1,
      chartDefinitionNodeId: entityRowReference.entityType === SquashTmDataRowType.ChartDefinition ? entityRowReference.id : null,
      reportDefinitionNodeId: entityRowReference.entityType === SquashTmDataRowType.ReportDefinition ? entityRowReference.id : null,
    };
    return droppedItem;
  }

  private findFirstPossiblePosition(): GridsterItem {
    return this.gridster.getFirstPossiblePosition(CustomDashboardComponent.getBaseItem());
  }

  private gridsterHasAvailablePosition(): boolean {
    return this.gridster.getNextPossiblePosition(CustomDashboardComponent.getBaseItem());
  }

  private static getBaseItem() {
    return {
      x: 0,
      y: 0,
      rows: 1,
      cols: 1
    };
  }

  private extractFirstEligibleRow(dropEvent: AbstractSqtmDragAndDropEvent): DataRow | undefined {
    const dataRows: DataRow[] = dropEvent.dragAndDropData.data.dataRows;
    return dataRows.find(
      r => r.type === SquashTmDataRowType.ChartDefinition
        || r.type === SquashTmDataRowType.ReportDefinition);
  }

  private dragIsFromCustomReportTree(dropEvent: AbstractSqtmDragAndDropEvent) {
    return dropEvent.dragAndDropData.origin === customReportWorkspaceTreeId;
  }

  dragEnter(dragEnterEvent: AbstractSqtmDragAndDropEvent) {
    if (this.dragIsFromCustomReportTree(dragEnterEvent)) {
      const dataRow: DataRow = this.extractFirstEligibleRow(dragEnterEvent);
      if (dataRow && this.permissions && this.permissions.canWrite && this.gridsterHasAvailablePosition()) {
        this.markAsDroppable();
      }
    }
  }

  dragLeave() {
    this.unmarkAsDroppable();
  }

  private markAsDroppable() {
    this.renderer.addClass(this.container.nativeElement, '__hover_current_ws_border');
  }

  private unmarkAsDroppable() {
    this.renderer.removeClass(this.container.nativeElement, '__hover_current_ws_border');
  }

  private beginAsync() {
    this.renderer.setStyle(this.loadingOverlay.nativeElement, 'display', 'flex');
  }

  private endAsync() {
    this.renderer.setStyle(this.loadingOverlay.nativeElement, 'display', 'none');
  }

  private itemChanged() {
    this.gridsterItemSubject.next(this.dashboard);
  }

  private showGrid() {
    this.renderer.setStyle(this.gridster.el, 'background', 'darkgrey');
  }

  private hideGrid() {
    this.renderer.setStyle(this.gridster.el, 'background', 'none');
  }

  private convertToBinding(item: GridsterItem): ChartBinding | ReportBinding {
    return {
      id: item.id,
      dashboardId: this.dashboardId,
      chartDefinitionId: item.chartDefinitionId,
      reportDefinitionId: item.reportDefinitionId,
      row: item.y + 1,
      col: item.x + 1,
      sizeX: item.cols,
      sizeY: item.rows
    };
  }

  unbindItem($event: MouseEvent, item: GridsterItem) {
    $event.stopPropagation();
    if (item.chartDefinitionId) {
      this.unbindChart(item);
    } else if (item.reportDefinitionId) {
      this.unbindReport(item);
    }
  }

  private unbindReport(item: GridsterItem) {
    this.beginAsync();
    this.dashboardService.removeReportFromDashboard(item.id).pipe(
      finalize(() => this.endAsync())
    ).subscribe(() => this.removeGridsterItem(item));
  }

  private unbindChart(item: GridsterItem) {
    this.beginAsync();
    this.dashboardService.removeChartFromDashboard(item.id).pipe(
      finalize(() => this.endAsync())
    ).subscribe(() => this.removeGridsterItem(item));
  }

  private removeGridsterItem(item: GridsterItem) {
    this.dashboard.splice(this.dashboard.indexOf(item), 1);
    if (this.dashboard.length === 0) {
      this.dashboardCleared.emit();
    } else {
      this.cdRef.detectChanges();
    }
  }

  dragEnterInItem(item: GridsterItem, dragEnterEvent: SqtmDragEnterEvent) {
    if (this.dragIsFromCustomReportTree(dragEnterEvent)) {
      const dataRow: DataRow = this.extractFirstEligibleRow(dragEnterEvent);
      if (this.canSwap(dataRow, item)) {
        const dropZone = this.findItemDropZone(item);
        dropZone.markAsHovered();
        this.unmarkAsDroppable();
      }
    }
  }

  unmarkAllDropZones() {
    this.unmarkAllItems();
    this.unmarkAsDroppable();
  }

  private unmarkAllItems() {
    this.itemsDropZones.forEach((i) => i.unmarkAsHovered());
  }

  dragLeaveItem(item: GridsterItem, dragEnterEvent: AbstractSqtmDragAndDropEvent) {
    const dropZone = this.findItemDropZone(item);
    if (dropZone) {
      dropZone.unmarkAsHovered();
    }
    this.dragEnter(dragEnterEvent);
  }

  dropInItem(item: GridsterItem, sqtmDropEvent: SqtmDropEvent) {
    logger.debug(`drop item into chart binding ${item.id}`);
    if (this.dragIsFromCustomReportTree(sqtmDropEvent)) {
      const dataRow: DataRow = this.extractFirstEligibleRow(sqtmDropEvent);
      if (this.canSwap(dataRow, item)) {
        this.swapItem(dataRow, item);
      }
    }
  }

  private swapItem(dataRow: DataRow, item: GridsterItem) {
    logger.debug(`swapping chart binding ${item.id}`, [dataRow, item]);
    if (this.dataRowIsChart(dataRow)) {
      this.swapChart(dataRow, item);
    } else if (this.dataRowIsReport(dataRow)) {
      this.swapReport(dataRow, item);
    }
  }

  private swapChart(dataRow: DataRow, item: GridsterItem) {
    const entityRowReference = toEntityRowReference(dataRow);
    this.beginAsync();
    this.dashboardService.swapChart(item.id, entityRowReference.id).pipe(
      finalize(() => {
        this.unmarkAllDropZones();
        this.endAsync();
      })
    ).subscribe((binding: ChartBinding) => {
      this.appendChart(binding, item);
      this.cdRef.detectChanges();
    });
  }

  private swapReport(dataRow: DataRow, item: GridsterItem) {
    const entityRowReference = toEntityRowReference(dataRow);
    this.beginAsync();
    this.dashboardService.swapReport(item.id, entityRowReference.id).pipe(
      finalize(() => {
        this.unmarkAllDropZones();
        this.endAsync();
      })
    ).subscribe((binding: ReportBinding) => {
      this.appendReport(binding, item);
      this.cdRef.detectChanges();
    });
  }

  private dataRowIsReport(dataRow: DataRow) {
    return dataRow.type === SquashTmDataRowType.ReportDefinition;
  }

  private dataRowIsChart(dataRow: DataRow) {
    return dataRow.type === SquashTmDataRowType.ChartDefinition;
  }

  private canSwap(dataRow: DataRow, item: GridsterItem) {
    return dataRow && this.permissions && this.permissions.canWrite && this.typesAreCompatibles(dataRow, item);
  }

  private typesAreCompatibles(dataRow: DataRow, item: GridsterItem) {
    return (dataRow.type === SquashTmDataRowType.ChartDefinition && item.chartDefinitionId)
      || (dataRow.type === SquashTmDataRowType.ReportDefinition && item.reportDefinitionId);
  }

  private findItemDropZone(item: GridsterItem) {
    return this.itemsDropZones.find(i => i.id === `${this.DROP_ZONE_PREFIX}${item.id}`);
  }

  downloadReport($event: MouseEvent) {

  }
}

interface ResizeEvent {
  width: number;
  height: number;
}
