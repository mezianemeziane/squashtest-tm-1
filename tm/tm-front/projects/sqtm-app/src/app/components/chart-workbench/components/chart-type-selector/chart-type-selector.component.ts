import {Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter} from '@angular/core';
import {ChartType, filterAvailableChartOperations, Option} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-chart-type-selector',
  templateUrl: './chart-type-selector.component.html',
  styleUrls: ['./chart-type-selector.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartTypeSelectorComponent implements OnInit {

  selectedChartType: ChartType;

  @Input()
  set chartType(chartType: ChartType) {
    this.selectedChartType = chartType;
  }

  get availableChartTypes(): Option[] {
    return Object.values(ChartType).map(chartType => (
      {
        value: chartType,
        label: this.getChartTypeI18nKey(chartType),
        icon: this.getIcon(chartType)
      }));
  }

  @Output()
  chartTypeChanged = new EventEmitter<ChartType>();

  constructor() {
  }

  ngOnInit(): void {
  }

  getChartTypeI18nKey(chartType: ChartType): string {
    return `sqtm-core.custom-report-workspace.chart.types.${chartType}`;
  }

  getIcon(chartType: ChartType): string {
    return `sqtm-core-custom-report:${(chartType as string).toLowerCase()}`;
  }

  changeChartType() {
    this.chartTypeChanged.emit(this.selectedChartType);
  }
}
