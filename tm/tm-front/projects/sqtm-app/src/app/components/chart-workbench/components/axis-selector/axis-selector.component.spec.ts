import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AxisSelectorComponent } from './axis-selector.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('AxisSelectorComponent', () => {
  let component: AxisSelectorComponent;
  let fixture: ComponentFixture<AxisSelectorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule, TranslateModule.forRoot()],
      declarations: [ AxisSelectorComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AxisSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
