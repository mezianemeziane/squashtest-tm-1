import {
  ChangeDetectionStrategy,
  Component,
  ComponentRef,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  ChartScopeType,
  EntityReference,
  EntityType, Project, ProjectData,
  ProjectPickerComponent,
  ReferentialDataService
} from 'sqtm-core';
import {CompleteScope} from '../../state/chart-workbench.state';
import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {ComponentPortal} from '@angular/cdk/portal';
import {filter, map, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {Observable, race, Subject} from 'rxjs';
import {CustomScopeSelectorComponent} from '../custom-scope-selector/custom-scope-selector.component';

@Component({
  selector: 'sqtm-app-scope-selector',
  templateUrl: './scope-selector.component.html',
  styleUrls: ['./scope-selector.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScopeSelectorComponent implements OnInit {

  @Input()
  completeScope: CompleteScope;

  @Input()
  projectId: number;

  get selectedPerimeterType(): ChartScopeType {
    return this.completeScope.scopeType;
  }

  scopeTypes = ChartScopeType;

  @Output()
  scopeChanged = new EventEmitter<CompleteScope>();

  @ViewChild('scopeSelector')
  scopeSelector: ElementRef;

  private overlayRef: OverlayRef;

  private projectPickerComponentRef: ComponentRef<ProjectPickerComponent>;
  private customScopeSelectorComponentRef: ComponentRef<CustomScopeSelectorComponent>;

  private unsub$ = new Subject<void>();

  constructor(private overlay: Overlay, private vcr: ViewContainerRef, private referentialDataService: ReferentialDataService) {
  }

  ngOnInit(): void {
  }

  changePerimeterType(type: ChartScopeType) {
    if (type === ChartScopeType.PROJECTS) {
      this.openProjectSelector();
    } else if (type === ChartScopeType.CUSTOM) {
      this.openCustomScopeSelector();
    } else {
      this.scopeChanged.emit({
        scopeType: type,
        scope: [new EntityReference(this.projectId, EntityType.PROJECT)],
        projectScope: [this.projectId]
      });
    }
  }

  openProjectSelector() {
    this.referentialDataService.projects$.pipe(
      take(1),
      withLatestFrom(this.referentialDataService.filteredProjects$),
      map(([projects, filteredProjects]) => {
        const ids = filteredProjects.map(project => project.id);
        return projects.filter(
          project => ids.includes(project.id)
        );
      })
    ).subscribe(projects => {
      this.createOverlay();
      const projectPickerComponentPortal = new ComponentPortal(ProjectPickerComponent, this.vcr);
      this.projectPickerComponentRef = this.overlayRef.attach(projectPickerComponentPortal);
      this.projectPickerComponentRef.instance.projects = projects;
      this.projectPickerComponentRef.instance.initiallySelectedProjects = this.getInitiallySelectedProjects();
      this.projectPickerComponentRef.changeDetectorRef.detectChanges();
      this.projectPickerComponentRef.instance.selectedProjects.pipe(
        takeUntil(this.unsub$),
        take(1)
      ).subscribe(selectedProjects => {
        this.scopeChanged.emit({
          scopeType: ChartScopeType.PROJECTS,
          scope: selectedProjects.map(p => new EntityReference(p.id, EntityType.PROJECT)),
          projectScope: selectedProjects.map(p => p.id)
        });
        this.closeProjectSelector();
      });
      race([this.overlayRef.backdropClick(), this.projectPickerComponentRef.instance.cancelRequired])
        .pipe(
          takeUntil(this.unsub$),
          take(1)
        ).subscribe(() => this.closeProjectSelector());
    });
  }

  openCustomScopeSelector() {
    this.createOverlay();
    const customScopeSelectorPortal = new ComponentPortal(CustomScopeSelectorComponent, this.vcr);
    this.customScopeSelectorComponentRef = this.overlayRef.attach(customScopeSelectorPortal);
    this.customScopeSelectorComponentRef.instance.initiallySelectedNodes = this.completeScope.scope;
    // this.customScopeSelectorComponentRef.instance.initiallySelectedProjects = this.getInitiallySelectedProjects();
    // this.customScopeSelectorComponentRef.changeDetectorRef.detectChanges();
    this.customScopeSelectorComponentRef.instance.scopeChanged.pipe(
      take(1)
    ).subscribe((scope) => {
      this.scopeChanged.emit(scope);
      this.closeCustomScopeSelector();
    });
    race([this.overlayRef.backdropClick(), this.customScopeSelectorComponentRef.instance.closeRequired]).pipe(
      takeUntil(this.unsub$),
      take(1)
    ).subscribe(() => this.closeCustomScopeSelector());
  }

  private createOverlay() {
    const positionStrategy = this.overlay.position().flexibleConnectedTo(this.scopeSelector)
      .withPositions([
        {originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'top', offsetX: 20},
        {originX: 'start', overlayX: 'start', originY: 'center', overlayY: 'top'},
      ]);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
      width: 600,
      height: 800
    };
    this.overlayRef = this.overlay.create(overlayConfig);
  }

  private getInitiallySelectedProjects() {
    return this.completeScope.scope
      .filter(e => e.type === EntityType.PROJECT)
      .map(e => e.id);
  }

  closeProjectSelector() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.projectPickerComponentRef = null;
    }
  }

  closeCustomScopeSelector() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.customScopeSelectorComponentRef = null;
    }
  }

  getProjectScopeLabels(): Observable<ProjectScopeLabelsDisplay> {
    return this.referentialDataService.projects$.pipe(
      take(1),
      map(projects => projects.filter(project => this.completeScope.projectScope.includes(project.id))),
      map(filteredProjects => this.extractLabelsFromSelectedProjects(filteredProjects))
    );
  }

  private extractLabelsFromSelectedProjects(filteredProjects: Project[]): ProjectScopeLabelsDisplay {
    const projectNames: string[] = filteredProjects.map(project => project.name);
    projectNames.sort((nameA, nameB) => nameA.localeCompare(nameB));
    return {
      labels: projectNames.join((', ')),
      length: projectNames.length
    };
  }

}

export interface ProjectScopeLabelsDisplay {
  labels: string;
  length: number;
}
