import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChartTypeSelectorComponent } from './chart-type-selector.component';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('ChartTypeSelectorComponent', () => {
  let component: ChartTypeSelectorComponent;
  let fixture: ComponentFixture<ChartTypeSelectorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [ ChartTypeSelectorComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartTypeSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
