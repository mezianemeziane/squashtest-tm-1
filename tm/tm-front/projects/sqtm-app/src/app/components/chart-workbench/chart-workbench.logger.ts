import {sqtmAppLogger} from '../../app-logger';

export const chartWorkbenchLogger = sqtmAppLogger.compose('chart-workbench');
