import {ChangeDetectionStrategy, Component, InjectionToken, Input, OnDestroy, OnInit} from '@angular/core';
import {
  column,
  CustomChartEntityType,
  Extendable,
  GridDefinition,
  GridDefinitionBuilder,
  GridService,
  gridServiceFactory,
  GridType,
  pagination,
  ReferentialDataService,
  RestService,
  StyleDefinitionBuilder
} from 'sqtm-core';
import {ColumnProtoCellRendererComponent} from '../column-proto-cell-renderer/column-proto-cell-renderer.component';
import {EnhancedColumnPrototype} from '../../state/chart-workbench.state';

export const FIXED_COLUMN_PROTO_SELECTOR_GRID_CONFIG = new InjectionToken('Token to inject column proto grid config');


export function fixedColumnPrototypeSelectorGridConfig(): GridDefinition {
  return new GridDefinitionBuilder('fixed-column-prototype-selector', GridType.TREE)
    .withColumns([
      column('NAME')
        .enableDnd()
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(ColumnProtoCellRendererComponent)
    ])
    .withRowHeight(30)
    .disableHeaders()
    .enableMultipleColumnsFiltering(['NAME'])
    .disableRightToolBar()
    .withStyle(
      new StyleDefinitionBuilder()
        .switchOffSelectedRows()
        .switchOffHoveredRows()
        .enableInitialLoadAnimation())
    .withPagination(pagination().inactive())
    .enableDrag()
    .build();
}


@Component({
  selector: 'sqtm-app-fixed-column-prototype-selector',
  templateUrl: './fixed-column-prototype-selector.component.html',
  styleUrls: ['./fixed-column-prototype-selector.component.less'],
  providers: [
    {
      provide: FIXED_COLUMN_PROTO_SELECTOR_GRID_CONFIG,
      useFactory: fixedColumnPrototypeSelectorGridConfig
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, FIXED_COLUMN_PROTO_SELECTOR_GRID_CONFIG, ReferentialDataService]
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FixedColumnPrototypeSelectorComponent implements OnInit, OnDestroy {

  @Input()
  columnPrototypes: { [K in CustomChartEntityType]: EnhancedColumnPrototype[] };

  constructor(private gridService: GridService) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }

}
