import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import {
  ChangeFilteringAction,
  ChartBuildingBlocks,
  ChartColumnRole,
  ChartDefinitionModel,
  ChartDefinitionService,
  ChartOperation,
  ChartType,
  doesHttpErrorContainsSquashFieldError,
  extractSquashFirstFieldError,
  FieldValidationError,
  SqtmDropEvent
} from 'sqtm-core';
import {ChartWorkbenchService} from '../../services/chart-workbench.service';
import {select} from '@ngrx/store';
import {
  AxisColumns,
  ChartWorkbenchHintMessages,
  CompleteScope,
  CraftedChartPreview,
  DetailedAxisRole,
  RenderedCraftedChartFilter,
  selectAllowAttributeSelector,
  selectAxis,
  selectChartPreview,
  selectChartScope,
  selectChartType,
  selectFilters,
  selectMessages,
  selectOrderedColumnPrototypes,
  selectShowAttributeSelector
} from '../../state/chart-workbench.state';
import {BehaviorSubject, Observable, of, Subject, throwError} from 'rxjs';
import {
  catchError,
  concatMap,
  distinctUntilChanged,
  filter,
  finalize,
  map,
  share,
  shareReplay,
  take,
  takeUntil,
  tap
} from 'rxjs/operators';
import {ColumnPrototypeRow} from '../column-prototype-selector/column-prototype-row';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {chartWorkbenchLogger} from '../../chart-workbench.logger';
import * as CssElementQuery from 'css-element-queries';
import {ResizeSensor} from 'css-element-queries';
import {ResizeEvent} from '../../../statistics/abstract-statistics-panel.component';
import {TranslateService} from '@ngx-translate/core';

const logger = chartWorkbenchLogger.compose('CustomChartWorkbenchComponent');

@Component({
  selector: 'sqtm-app-custom-chart-workbench',
  templateUrl: './custom-chart-workbench.component.html',
  styleUrls: ['./custom-chart-workbench.component.less'],
  providers: [
    {
      provide: ChartWorkbenchService,
      useClass: ChartWorkbenchService
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomChartWorkbenchComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input()
  chartDefinition: ChartDefinitionModel;

  @Input()
  buildingBlocks: ChartBuildingBlocks;

  @Input()
  projectId: number;

  // The container that must be parent of the chart we are creating.
  @Input()
  containerId: number;

  @ViewChild('mainContainer')
  mainContainer: ElementRef;

  fieldValidationErrors: FieldValidationError[] = [];

  orderedColumnProto$ = this.chartWorkbenchService.state$.pipe(
    select(selectOrderedColumnPrototypes)
  );

  axes$: Observable<AxisColumns> = this.chartWorkbenchService.state$.pipe(
    select(selectAxis),
  );

  chartType$: Observable<ChartType> = this.chartWorkbenchService.state$.pipe(
    select(selectChartType),
  );

  scope$: Observable<CompleteScope> = this.chartWorkbenchService.state$.pipe(
    select(selectChartScope),
  );

  filters$: Observable<RenderedCraftedChartFilter[]> = this.chartWorkbenchService.state$.pipe(
    select(selectFilters),
  );

  allowAttributePanel$: Observable<boolean> = this.chartWorkbenchService.state$.pipe(
    select(selectAllowAttributeSelector),
    tap((show: boolean) => this.handleColumnCss(show))
  );

  showAttributePanel$: Observable<boolean> = this.chartWorkbenchService.state$.pipe(
    select(selectShowAttributeSelector),
    tap((show: boolean) => this.handleColumnCss(show))
  );

  showAttributePanelIcon$: Observable<string> = this.chartWorkbenchService.state$.pipe(
    select(selectShowAttributeSelector),
    map(show => show ? 'double-right' : 'double-left')
  );

  getAttributePanelIconTooltip$: Observable<string> = this.chartWorkbenchService.state$.pipe(
    select(selectShowAttributeSelector),
    map(show => show ? this.translateService.instant('sqtm-core.generic.label.collapse') : this.translateService.instant('sqtm-core.generic.label.expand'))
  );

  chartPreview$ = this.chartWorkbenchService.state$.pipe(
    select(selectChartPreview),
    // take care, this is an http request here if chart preview is valid
    concatMap((chartPreview: CraftedChartPreview) => {
      if (chartPreview.isValid) {
        this.loadingChartPreview$.next('async');
        return this.chartDefinitionService.getChartPreview(chartPreview.chartDefinition).pipe(
          map(chartDefinition => ({...chartPreview, chartDefinition})),
          finalize(() => this.loadingChartPreview$.next('not_async'))
        );
      } else {
        return of(chartPreview);
      }
    }),
    share() // share to prevent multiple http request for each subscription to this observable
  );

  showHintMessages$: Observable<ChartWorkbenchHintMessages> = this.chartWorkbenchService.state$.pipe(
    select(selectMessages),
  );

  formGroup: FormGroup;

  columnRoles = ChartColumnRole;

  detailedAxisRoles = DetailedAxisRole;

  loadingChartPreview$ = new BehaviorSubject<'async' | 'not_async'>('not_async');

  savingChart$ = new BehaviorSubject<'async' | 'not_async'>('not_async');

  protected resizeSensor: ResizeSensor;
  protected _resizeEvent = new Subject<ResizeEvent>();
  protected resizeEvent$ = this._resizeEvent.asObservable().pipe(shareReplay(1));
  protected unsub$ = new Subject<void>();

  private readonly MIN_SIZE_FOR_ATTRIBUTE_SELECTOR = 1400;

  toggleButtonTooltipIsVisible: boolean;

  constructor(private chartWorkbenchService: ChartWorkbenchService,
              private chartDefinitionService: ChartDefinitionService,
              private fb: FormBuilder,
              private router: Router,
              private cdRef: ChangeDetectorRef,
              private renderer: Renderer2,
              private zone: NgZone,
              private translateService: TranslateService) {
    this.formGroup = this.fb.group({
      'name': fb.control(this.translateService.instant('sqtm-core.custom-report-workspace.create-chart-view.new-chart'))
    });
    this.chartWorkbenchService.changeName(this.formGroup.get('name').value);
  }

  ngOnInit(): void {
    this.chartWorkbenchService.initializeBuildingBlocks(this.buildingBlocks.columnPrototypes, this.projectId);
  }

  ngAfterViewInit(): void {
    this.initializeResizeHook();
    if (this.chartDefinition) {
      this.chartWorkbenchService.loadExistingChart(this.chartDefinition);
      this.formGroup.get('name').setValue(this.chartDefinition.name);
    }
  }

  private initializeResizeHook() {
    this.zone.runOutsideAngular(() => {
      const that = this;
      this.resizeSensor = new CssElementQuery.ResizeSensor(this.mainContainer.nativeElement, function ($event: ResizeEvent) {
        that._resizeEvent.next($event);
      });
      this.resizeEvent$.pipe(
        takeUntil(this.unsub$),
        map(e => e.width > this.MIN_SIZE_FOR_ATTRIBUTE_SELECTOR),
        distinctUntilChanged()
      ).subscribe(allowAttributeSelector => this.allowAttributeSelector(allowAttributeSelector));
    });
  }

  private allowAttributeSelector(allowShowAttributeSelector: boolean) {
    this.zone.run(() => {
      this.chartWorkbenchService.allowAttributeSelector(allowShowAttributeSelector);
    });
  }

  refreshPreview() {

  }

  handleAdd() {
    this.chartWorkbenchService.state$.pipe(
      select(selectChartPreview),
      take(1),
      filter((chartPreview: CraftedChartPreview) => chartPreview.isValid),
      tap(() => this.savingChart$.next('async')),
      concatMap((chartPreview: CraftedChartPreview) =>
        this.chartDefinitionService.persistNewChartDefinition(chartPreview.chartDefinition, this.containerId)),
      catchError(err => {
        this.savingChart$.next('not_async');
        this.showFieldError(err);
        return throwError(err);
      })
    ).subscribe((createdEntityId) => {
      const url = `/custom-report-workspace/chart-definition/${createdEntityId.id}/content`;
      logger.debug(`Successful chart creation, navigate back to : ${url}`);
      this.router.navigate([url]);
    });
  }

  handleModify() {
    this.chartWorkbenchService.state$.pipe(
      select(selectChartPreview),
      take(1),
      filter((chartPreview: CraftedChartPreview) => chartPreview.isValid),
      tap(() => this.savingChart$.next('async')),
      concatMap((chartPreview: CraftedChartPreview) => {
        const newChartDefinition = {...chartPreview.chartDefinition};
        newChartDefinition.id = this.chartDefinition.id;
        return this.chartDefinitionService.updateChartDefinition(newChartDefinition,
          this.chartDefinition.customReportLibraryNodeId);
      }),
      catchError(err => {
        this.savingChart$.next('not_async');
        this.showFieldError(err);
        return throwError(err);
      })
    ).subscribe(() => {
      const url = `/custom-report-workspace/chart-definition/${this.chartDefinition.customReportLibraryNodeId}/content`;
      logger.debug(`Successful chart update, navigate back to : ${url}`);
      this.router.navigate([url]);
    });
  }

  private showFieldError(err) {
    if (doesHttpErrorContainsSquashFieldError(err)) {
      const squashFieldError = extractSquashFirstFieldError(err);
      this.fieldValidationErrors = squashFieldError.fieldValidationErrors;
      this.cdRef.detectChanges();
    }
  }

  handleDropAxis($event: SqtmDropEvent) {
    logger.debug(`Dropping column in axis `, [$event]);
    const columnPrototypeRows: ColumnPrototypeRow[] = $event.dragAndDropData.data.dataRows;
    const ids = columnPrototypeRows.map(proto => proto.id);
    this.chartWorkbenchService.dropColumns(ids);
  }

  handleDropFilters($event: SqtmDropEvent) {
    logger.debug(`Dropping column in filters  `, [$event]);
    const columnPrototypeRows: ColumnPrototypeRow[] = $event.dragAndDropData.data.dataRows;
    const ids = columnPrototypeRows.map(proto => proto.id);
    this.chartWorkbenchService.addFilters(ids);
  }

  changeAxis(id: string) {
    this.chartWorkbenchService.changeAxis(id);
  }

  changeName() {
    const value = this.formGroup.get('name').value || '';
    const name = value.trim();
    this.chartWorkbenchService.changeName(name);
    this.formGroup.get('name').setValue(name);
    this.fieldValidationErrors = [];
    this.cdRef.detectChanges();
  }

  handleCancel() {
    const url = `/custom-report-workspace`;
    logger.debug(`Cancelling chart creation, navigate back to : ${url}`);
    this.router.navigate([url]);
  }

  changeAxisOperation(operation: ChartOperation) {
    this.chartWorkbenchService.changeAxisOperation(operation);
  }

  changeChartType(chartType: ChartType) {
    this.chartWorkbenchService.changeType(chartType);
  }

  changeMeasure(id: string) {
    this.chartWorkbenchService.changeMeasure(id);
  }

  changeMeasureOperation(operation: ChartOperation) {
    this.chartWorkbenchService.changeMeasureOperation(operation);
  }

  changeSeries(id: string) {
    this.chartWorkbenchService.changeSeries(id);
  }

  changeSeriesOperation(operation: ChartOperation) {
    this.chartWorkbenchService.changeSeriesOperation(operation);
  }

  changeScope(completeScope: CompleteScope) {
    logger.debug('Changing scope to : ', [completeScope]);
    this.chartWorkbenchService.changeScope(completeScope);
  }

  getAxisI18nKey(detailedAxisRole: DetailedAxisRole) {
    return 'sqtm-core.custom-report-workspace.chart.axis.name.' + detailedAxisRole;
  }

  addFilter(id: string) {
    this.chartWorkbenchService.addFilter(id);
  }

  changeFilter(changeFilteringAction: ChangeFilteringAction) {
    this.chartWorkbenchService.changeFilter(changeFilteringAction);
  }

  removeFilter(id: string) {
    this.chartWorkbenchService.removeFilter(id);
  }

  getErrorMessage(chartDefinition: ChartDefinitionModel | Partial<ChartDefinitionModel>) {
    logger.debug('Missing elements to render chart ', [chartDefinition]);
    if (chartDefinition.measures.length === 0 && chartDefinition.axis.length === 0) {
      return 'sqtm-core.custom-report-workspace.create-chart-view.error.no-axes';
    }
    return 'sqtm-core.custom-report-workspace.create-chart-view.error.missing-axes';
  }

  toggleAttributeSelector() {
    this.chartWorkbenchService.toggleAttributeSelector();
    this.toggleButtonTooltipIsVisible = false;
  }

  private handleColumnCss(show: boolean) {
    if (show) {
      this.switchThreeColumnLayout();
    } else {
      this.switchTwoColumnLayout();
    }
  }

  private switchThreeColumnLayout() {
    if (this.mainContainer) {
      this.renderer.addClass(this.mainContainer.nativeElement, 'workbench-layout-3-cols');
      this.renderer.removeClass(this.mainContainer.nativeElement, 'workbench-layout-2-cols');
    }
  }

  private switchTwoColumnLayout() {
    if (this.mainContainer) {
      this.renderer.addClass(this.mainContainer.nativeElement, 'workbench-layout-2-cols');
      this.renderer.removeClass(this.mainContainer.nativeElement, 'workbench-layout-3-cols');
    }
  }

  ngOnDestroy(): void {
    this.detachResizeSensor();
    this.completeSubjects();
    this.completeService();
  }

  private completeSubjects() {
    this.unsub$.next();
    this.unsub$.complete();
    this._resizeEvent.complete();
    this.loadingChartPreview$.complete();
    this.savingChart$.complete();
  }

  private detachResizeSensor() {
    if (this.resizeSensor) {
      this.resizeSensor.detach();
    }
  }

  private completeService() {
    this.chartWorkbenchService.complete();
  }
}
