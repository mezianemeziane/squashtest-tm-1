import {ChartColumnPrototype, ChartOperation, EntityType, Workspaces} from 'sqtm-core';
import {EnhancedColumnPrototype} from '../state/chart-workbench.state';

export class ChartWorkbenchUtils {
  static getWorkspace(entityType: EntityType) {
    switch (entityType) {
      case EntityType.REQUIREMENT:
      case EntityType.REQUIREMENT_VERSION:
        return Workspaces['requirement-workspace'];
      case EntityType.TEST_CASE:
        return Workspaces['test-case-workspace'];
      case EntityType.CAMPAIGN:
      case EntityType.ITERATION:
      case EntityType.ITEM_TEST_PLAN:
      case EntityType.EXECUTION:
        return Workspaces['campaign-workspace'];
      default:
        throw Error(`Unable to find workspace theme for type ${entityType}`);
    }
  }

  static getColumnEntityNameI18nKey(column: ChartColumnPrototype): string {
    const entityType = column.specializedType.entityType;
    return this.getEntityNameI18nKey(entityType);
  }

  static getEntityNameI18nKey(entityType: EntityType) {
    return `sqtm-core.custom-report-workspace.chart.entity.${entityType}`;
  }

  static getColumnNameI18nKey(chartColumnPrototype: ChartColumnPrototype, cufId: number): string {
    const enhancedColumnPrototype = chartColumnPrototype as EnhancedColumnPrototype;
    return cufId ?
      enhancedColumnPrototype.cufLabel
      : `sqtm-core.custom-report-workspace.chart.columns.${enhancedColumnPrototype.label}`;
  }

  static getOperationI18nKey(operation: ChartOperation): string {
    return `sqtm-core.custom-report-workspace.chart.operations.${operation}`;
  }
}
