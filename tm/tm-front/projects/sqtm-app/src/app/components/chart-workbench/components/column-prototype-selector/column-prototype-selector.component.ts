import {ChangeDetectionStrategy, Component, Input, OnDestroy} from '@angular/core';
import {ChartColumnPrototype, CustomChartEntityType, DataRow, DataRowOpenState, GridService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {chartWorkbenchLogger} from '../../chart-workbench.logger';
import {Subject} from 'rxjs';
import {ColumnPrototypeRow, ENTITY_KEY, EntityRow} from './column-prototype-row';
import {EnhancedColumnPrototype} from '../../state/chart-workbench.state';
import {CHART_ENTITY_I18N_KEY} from '../../../charts/charts.constants';

const logger = chartWorkbenchLogger.compose('ColumnPrototypeSelectorComponent');

@Component({
  selector: 'sqtm-app-column-prototype-selector',
  template: `
    <sqtm-app-workbench-column-selector>
    </sqtm-app-workbench-column-selector>
  `,
  styleUrls: ['./column-prototype-selector.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColumnPrototypeSelectorComponent implements OnDestroy {

  dataRows$ = new Subject<DataRow[]>();

  protected _columnPrototypes: { [K in CustomChartEntityType]: ChartColumnPrototype[] };

  @Input()
  set columnPrototypes(nextColumnPrototypes: { [K in CustomChartEntityType]: EnhancedColumnPrototype[] }) {
    this._columnPrototypes = nextColumnPrototypes;
    const dataRows: DataRow[] = [];

    Object
      .entries(this._columnPrototypes)
      .forEach(([entityId, columnPrototypes]: [string, EnhancedColumnPrototype[]]) => {
        const columnPrototypeIds: string[] = columnPrototypes
          // .filter(ccp => ccp.columnType !== ChartColumnType.CUF)
          .map((ccp: EnhancedColumnPrototype) => ccp.generatedId);
        const entityRow = this.generateEntityRow(entityId, columnPrototypeIds);
        dataRows.push(entityRow);
        const ccpRows = columnPrototypes
          // .filter(ccp => ccp.columnType !== ChartColumnType.CUF)
          .map(ccp => {
            const ccpRow = new ColumnPrototypeRow();
            const name = ccp.cufId ? ccp.cufLabel :
              this.translateService.instant(`sqtm-core.custom-report-workspace.chart.columns.${ccp.label}`);
            ccpRow.id = ccp.generatedId;
            ccpRow.parentRowId = entityId;
            ccpRow.children = [];
            ccpRow.state = DataRowOpenState.leaf;
            ccpRow.data = {
              'NAME': name,
              [ENTITY_KEY]: entityId,
              cufId: ccp.cufId
            };
            return ccpRow;
          });
        dataRows.push(...ccpRows);
      });
    logger.debug(`Generated rows for attribute tree: `, [dataRows]);
    this.dataRows$.next(dataRows);
  }

  private generateEntityRow(entityId: string, columnPrototypeIds: string[]) {
    const entityRow = new EntityRow();
    entityRow.id = entityId;
    entityRow.children = columnPrototypeIds;
    entityRow.state = DataRowOpenState.open;
    entityRow.data = {
      'NAME': this.translateService.instant(`${CHART_ENTITY_I18N_KEY}${entityId}`),
      [ENTITY_KEY]: entityId
    };
    return entityRow;
  }

  constructor(private gridService: GridService, private translateService: TranslateService) {
    this.gridService.connectToDataRowSource(this.dataRows$);
  }

  ngOnDestroy() {
    this.gridService.complete();
  }
}
