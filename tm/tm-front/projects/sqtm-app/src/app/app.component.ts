import {ChangeDetectionStrategy, Component, Inject, NgZone, OnDestroy, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {fromEvent, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {DatePipe, DOCUMENT} from '@angular/common';
import {SessionPingService} from 'sqtm-core';
import {ExecutionRunnerOpenerService} from './pages/execution/execution-runner/services/execution-runner-opener.service';
import {WindowOpenerService} from './services/window-opener.service';

/** @dynamic */
@Component({
  selector: 'sqtm-app-root',
  template: `
    <sqtm-core-generic-error-display></sqtm-core-generic-error-display>
    <div class="full-height full-width" sqtmCoreDragAndDropDisableSelection>
      <router-outlet></router-outlet>
      <sqtm-core-svg-icon-definition></sqtm-core-svg-icon-definition>
    </div>
  `,
  styleUrls: ['./app.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe, ExecutionRunnerOpenerService]
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'sqtm-app';
  private unsub$ = new Subject<void>();

  constructor(translate: TranslateService,
              @Inject(DOCUMENT) private document: Document,
              private ngZone: NgZone,
              private executionRunnerOpenerService: ExecutionRunnerOpenerService,
              private windowOpenerService: WindowOpenerService,
              private sessionPingService: SessionPingService) {
    translate.setDefaultLang('en');
    translate.use(translate.getBrowserLang());
  }

  ngOnInit(): void {
    this.preventDefaultDragOver();
    this.executionRunnerOpenerService.initialize();
    this.windowOpenerService.initialize();
    this.sessionPingService.initialize();
  }

  private preventDefaultDragOver() {
    this.ngZone.runOutsideAngular(() => {
      // Preventing all default behavior on dragover component if files are inside the dnd data.
      // Prevent the app to be flushed out if user drop a file outside of a valid target...
      fromEvent(this.document.body, 'dragover').pipe(
        takeUntil(this.unsub$)
      ).subscribe(function ($event: DragEvent) {
        if ($event.dataTransfer.files.length > 0) {
          $event.preventDefault();
        }
      });
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  // yeah in app component, as it was case in legacy. If user navigate after opening his execution dialog and do
  // a modification during execution, it MUST get his new execution dialog
  // private initOpenExecutionAfterModificationDuringExec() {
  //   this.interWindowCommunicationService.interWindowMessages$.pipe(
  //     takeUntil(this.unsub$),
  //     filter(message => message.isTypeOf('MODIFICATION-DURING-EXECUTION'))
  //   ).subscribe(message => {
  //     console.log('MODIFICATION-DURING-EXECUTION');
  //     console.log(message);
  //   });
  // }
}
