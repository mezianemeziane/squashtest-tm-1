import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {
  BACKEND_CONTEXT_PATH,
  GenericErrorDisplayModule,
  getBaseLocation,
  httpInterceptorProviders,
  LoggerService,
  SQTM_MAIN_APP_IDENTIFIER,
  SqtmCoreModule,
  SqtmDragAndDropModule,
  SvgModule,
  UiManagerModule
} from 'sqtm-core';
import {RouteReuseStrategy, RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {appLoggerConfiguration} from '../environments/app-logger.configuration';
import {EXECUTION_DIALOG_RUNNER_URL} from './pages/execution/execution-runner/execution-runner.constant';
import {DETAILED_STEP_VIEW_URL} from './pages/detailed-views/detailed-test-step-view/detailed-test-step.constant';
import {APP_BASE_HREF} from '@angular/common';
import {chartServiceProviders} from './components/charts/chart-service-providers';
import {DocxReportService} from './components/report-workbench/services/docx-report.service';
import {MODIF_DURING_EXEC_STEP_VIEW_URL} from './pages/detailed-views/modif-during-exec-step-view/modif-during-exec-step.constant';
import {CustomRouteReuseStrategy} from './custom-route-reuse-strategy';
import {CoverageExternalLinkComponent} from './pages/execution/components/coverage-external-link/coverage-external-link.component';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {WindowOpenerService} from './services/window-opener.service';

const loggerService = LoggerService.getLoggerService();
loggerService.loadConfiguration(appLoggerConfiguration);

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/home-workspace',
    pathMatch: 'full'
  },
  {
    path: 'home-workspace',
    loadChildren: () => import('./pages/home-workspace/home-workspace/home-workspace.module')
      .then(m => m.HomeWorkspaceModule)
  },
  {path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule)},
  {path: 'logout', loadChildren: () => import('./pages/logout/logout.module').then(m => m.LogoutModule)},
  {
    path: 'information',
    loadChildren: () => import('./pages/information/information.module').then(m => m.InformationModule)
  },
  {path: 'oauth', loadChildren: () => import('./pages/oauth-pages/oauth-pages.module').then(m => m.OauthPagesModule)},
  {
    path: 'automation-workspace/automation-programmer-workspace',
    loadChildren: () => import('./pages/automation-workspace/automation-workspace/automation-workspace.module')
      .then(m => m.AutomationWorkspaceModule)
  },
  {
    path: 'campaign-workspace',
    loadChildren: () => import('./pages/campaign-workspace/campaign-workspace.module')
      .then(m => m.CampaignWorkspaceModule)
  },
  {
    path: 'custom-report-workspace',
    loadChildren: () => import('./pages/custom-report-workspace/custom-report-workspace.module')
      .then(m => m.CustomReportWorkspaceModule)
  },
  {
    path: 'requirement-workspace',
    loadChildren: () => import('./pages/requirement-workspace/requirement-workspace.module')
      .then(m => m.RequirementWorkspaceModule)
  },
  {
    path: 'test-case-workspace',
    loadChildren: () => import('./pages/test-case-workspace/test-case-workspace.module')
      .then(m => m.TestCaseWorkspaceModule)
  },
  {
    path: 'search/requirement',
    loadChildren: () => import('./pages/search/requirement-search-page/requirement-search-page.module')
      .then(m => m.RequirementSearchPageModule)
  },
  {
    path: 'search/test-case',
    loadChildren: () => import('./pages/search/test-case-search-page/test-case-search-page.module')
      .then(m => m.TestCaseSearchPageModule)
  },
  {
    path: `${DETAILED_STEP_VIEW_URL}/:testCaseId`,
    loadChildren: () => import('./pages/detailed-views/detailed-test-step-view/detailed-test-step-view.module')
      .then(m => m.DetailedTestStepViewModule)
  },
  {
    path: 'user-account',
    loadChildren: () => import('./pages/user-account/user-account.module').then(m => m.UserAccountModule)
  },
  {
    path: `${MODIF_DURING_EXEC_STEP_VIEW_URL}`,
    loadChildren: () => import('./pages/detailed-views/modif-during-exec-step-view/modif-during-exec-step-view.module')
      .then(m => m.ModifDuringExecStepViewModule)
  },
  {
    path: `${EXECUTION_DIALOG_RUNNER_URL}`,
    loadChildren: () => import('./pages/execution/execution-runner/execution-runner.module')
      .then(m => m.ExecutionRunnerModule)
  },
  {
    path: 'execution',
    loadChildren: () => import('./pages/execution/execution-page/execution-page.module').then(m => m.ExecutionPageModule)
  },
  {
    path: 'executions',
    redirectTo: 'execution'
  },
  {
    path: 'administration-workspace/projects',
    loadChildren: () => import('./pages/administration/project-workspace/project-workspace.module')
      .then(m => m.ProjectWorkspaceModule),
  },
  {
    path: 'administration-workspace/milestones',
    loadChildren: () => import('./pages/administration/milestone-workspace/milestone-workspace.module')
      .then(m => m.MilestoneWorkspaceModule),
  },
  {
    path: 'administration-workspace/users',
    loadChildren: () => import('./pages/administration/user-workspace/user-workspace.module')
      .then(m => m.UserWorkspaceModule),
  },
  {
    path: 'administration-workspace/entities-customization',
    loadChildren: () => import('./pages/administration/custom-workspace/custom-workspace.module')
      .then(m => m.CustomWorkspaceModule),
  },
  {
    path: 'administration-workspace/servers',
    loadChildren: () => import('./pages/administration/server-workspace/server-workspace.module')
      .then(m => m.ServerWorkspaceModule),
  },
  {
    path: 'administration-workspace/system',
    loadChildren: () => import('./pages/administration/system-workspace/system-workspace.module')
      .then(m => m.SystemWorkspaceModule),
  },
  {
    path: 'search/campaign',
    loadChildren: () => import('./pages/search/itpi-search-page/itpi-search-page.module')
      .then(m => m.ItpiSearchPageModule)
  },
  {
    path: 'automation-workspace/functional-tester-workspace',
    loadChildren: () => import('./pages/automation-workspace/functional-tester-workspace/functional-tester-workspace.module')
      .then(m => m.FunctionalTesterWorkspaceModule)
  },
];

@NgModule({
  declarations: [
    AppComponent,
    CoverageExternalLinkComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'}),
    HttpClientModule,
    SvgModule,
    SqtmCoreModule.forRoot({
      pluginIdentifier: SQTM_MAIN_APP_IDENTIFIER,
      ngxTranslateFiles: []
    }),
    UiManagerModule,
    SqtmDragAndDropModule,
    GenericErrorDisplayModule,
    NzToolTipModule,
  ],
  providers: [
    {
      provide: APP_BASE_HREF,
      useFactory: getBaseLocation
    },
    {
      provide: BACKEND_CONTEXT_PATH,
      useFactory: getBaseLocation
    },
    httpInterceptorProviders,
    {
      provide: RouteReuseStrategy,
      useClass: CustomRouteReuseStrategy
    },
    chartServiceProviders,
    DocxReportService,
    WindowOpenerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
