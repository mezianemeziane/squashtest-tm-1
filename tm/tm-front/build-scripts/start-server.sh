#!/bin/sh
# This script is used to start an http server to serve the app for cypress tests
# The script assumes :
#   node, yarn and all the needed dependencies are in $PWD/node_modules/.bin
#   the core lib and it's asset have been built (ie yarn build have been run previously)
# This is always the case if maven-front-en-plugin is used to build the app before calling this script

# Adding ./node_module/.bin to PATH
# Adding ./node/node to PATH
# Adding ./node/yarn/dist/bin to PATH
# The local installs from maven front end plugin must be found before any globally installed version of node or yarn
PATH=$PWD/node_modules/.bin:$PWD/node:$PWD/node/yarn/dist/bin:$PATH

PROJECT_HOME=$PWD
echo $PROJECT_HOME
PID_FILE=$PROJECT_HOME/target/ng-server.pid
echo $PID_FILE

export NODE_OPTIONS=--max_old_space_size=4096
nohup ng serve sqtm-app --prod &
NG_PID=$!
echo "STARTING ANGULAR SERVER WITH PID : $NG_PID"
echo $NG_PID > $PID_FILE
