// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands');

function buildCustomLangMock(locale) {
  const urlFr = `${Cypress.env('appBaseUrl')}/custom_translations_${locale}.json`
  cy.route(urlFr, {})
}

function buildPingMock() {
  const url = `${Cypress.env('appBaseUrl')}/ping`
  cy.route(url, {})
}

beforeEach(() => {
  cy.log('Executing global before each...')
  cy.log('Starting cy.server')
  cy.server();

  cy.log('Adding global custom lang files mocks')
  buildCustomLangMock('fr');
  buildCustomLangMock('en');

  cy.log('Adding global ping mock')
  buildPingMock();

  cy.log('Executed global before each.')
})

