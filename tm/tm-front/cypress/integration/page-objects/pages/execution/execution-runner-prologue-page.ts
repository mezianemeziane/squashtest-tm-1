import {Page} from '../page';
import {ReferentialData} from '../../../model/referential-data.model';
import {ReferentialDataProviderBuilder} from '../../../utils/referential/referential-data.provider';
import {ExecutionModel} from '../../../model/execution/execution.model';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {ExecutionRunnerStepPage} from './execution-runner-step-page';
import {InputType} from '../../../model/customfield/customfield.model';
import {AttachmentCompactListElement} from '../../elements/attachments/attachment-compact-list.element';
import {UploadSummary} from '../../../model/attachment/upload-summary.model';
import {GridElement} from '../../elements/grid/grid.element';
import {GridResponse} from '../../../model/grids/data-row.type';

export class ExecutionRunnerProloguePage extends Page {

  public attachmentCompactList: AttachmentCompactListElement;
  public coverageGrid: GridElement;


  constructor() {
    super('sqtm-app-execution-prologue');
    this.attachmentCompactList = new AttachmentCompactListElement();
    this.coverageGrid = new GridElement('execution-view-coverages');
  }

  public static initTestAtPage(executionId: number, executionModel?: ExecutionModel, referentialData?: ReferentialData) {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const executionModelMock = new HttpMockBuilder<ExecutionModel>(`execution/${executionId}`).responseBody(executionModel).build();
    cy.visit(`execution-runner/execution/${executionId}/prologue`);
    referentialDataProvider.wait();
    executionModelMock.wait();
    return new ExecutionRunnerProloguePage();
  }

  public static initTestInIterations(iterationId: number,
                                     testPlanItemId: number,
                                     executionId: number,
                                     hasNextTestCase: boolean,
                                     executionModel?: ExecutionModel,
                                     referentialData?: ReferentialData) {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const executionModelMock = new HttpMockBuilder<ExecutionModel>(`execution/${executionId}`).responseBody(executionModel).build();
    cy.visit(
      `execution-runner/iteration/${iterationId}/test-plan/${testPlanItemId}/execution/${executionId}/prologue`,
      {qs: {hasNextTestCase}}
    );
    referentialDataProvider.wait();
    executionModelMock.wait();
    return new ExecutionRunnerProloguePage();
  }

  checkModificationDuringExecButtonIsVisible() {
    cy.get('[data-test-component-id="modification-during-exec"]').should('be.visible');
  }

  checkTitle(expectedTitle: string) {
    cy.get('span[data-test-component-id="execution-order"]').should('contain.text', expectedTitle);
  }

  checkPrerequisite(expectedPrerequisite: string) {
    cy.get('div[data-test-component-id="execution-prerequisite"]').should('have.html', expectedPrerequisite);
  }

  checkStatus(expectedStatus: string) {
    cy.get('sqtm-app-denormalized-status').should('contain.text', expectedStatus);
  }

  checkImportance(expectedImportance: string) {
    cy.get('sqtm-app-denormalized-importance').should('contain.text', expectedImportance);
  }

  checkNature(expectedNature: string) {
    cy.get('span[data-test-component-id="execution-prologue-nature"]').should('contain.text', expectedNature);
  }

  checkNatureIcon(iconName: string): void {
    cy.get('span[data-test-component-id="execution-prologue-nature"]')
      .siblings('.sqtm-core-infolist-icon')
      .should('have.class', `anticon-sqtm-core-infolist-item:${iconName}`);
  }

  checkType(expectedType: string) {
    cy.get('span[data-test-component-id="execution-prologue-type"]').should('contain.text', expectedType);
  }

  checkTypeIcon(iconName: string): void {
    cy.get('span[data-test-component-id="execution-prologue-type"]')
      .siblings('.sqtm-core-infolist-icon')
      .should('have.class', `anticon-sqtm-core-infolist-item:${iconName}`);
  }

  checkDataSet(expectedDataSet: string) {
    cy.get('span[data-test-component-id="execution-prologue-dataset"]').should('contain.text', expectedDataSet);
  }

  checkTcDescription(expectedDescription: string) {
    cy.get('div[data-test-component-id="execution-prologue-tc-description"]').should('have.html', expectedDescription);
  }

  checkDenormalizedCustomField(index: number, expectedLabel: string, expectedValue: any, inputType?: InputType) {
    cy.get(`label[data-test-component-id="execution-prologue-dnz-label-${index}"]`).should('contain.text', expectedLabel);
    if (!Boolean(inputType)) {
      cy.get(`sqtm-app-denormalized-custom-field[data-test-component-id="execution-prologue-dnz-value-${index}"]`)
        .should('contain.text', expectedValue);
    } else if (inputType === InputType.CHECKBOX) {
      this.checkDenormalizedCheckBoxCustomField(index, expectedValue);
    } else if (inputType === InputType.RICH_TEXT) {
      cy.get(`sqtm-app-denormalized-custom-field[data-test-component-id="execution-prologue-dnz-value-${index}"]`)
        .should('contain.html', expectedValue);
    } else if (inputType === InputType.TAG) {
      this.checkDenormalizedTagCustomField(index, expectedValue);
    }
  }

  checkDocumentTitle(expectedTitle: string) {
    cy.title().should('include', expectedTitle);
  }

  startExecution(): ExecutionRunnerStepPage {
    cy.get('[data-test-component-id="start-execution-button"]').click();
    return new ExecutionRunnerStepPage();
  }

  startExecutionWithKnownIssues(gridResponse: GridResponse): ExecutionRunnerStepPage {
    const getIssuePanelMock = new HttpMockBuilder('issues/execution-step/*?frontEndErrorIsHandled=true')
      .responseBody(getAuthenticatedIssuePanelResponse())
      .build();

    const getKnownIssuesMock = new HttpMockBuilder('issues/execution-step/*/known-issues')
      .post()
      .responseBody(gridResponse)
      .build();

    const firstStepPage = this.startExecution();
    getIssuePanelMock.wait();
    getKnownIssuesMock.wait();

    return firstStepPage;
  }

  addAttachments(files: File[], attachmentListId?: number, uploadSummary?: UploadSummary[]) {
    const mock = new HttpMockBuilder(`attach-list/${attachmentListId || '*'}/attachments/upload`)
      .post()
      .responseBody(uploadSummary)
      .build();
    cy.get('sqtm-app-execution-prologue>div')
      .trigger('drop', {
        dataTransfer: {
          files,
          types: ['Files']
        }
      });
    mock.wait();
  }

  private checkDenormalizedCheckBoxCustomField(index: number, expectedValue: boolean) {
    const selector = `
    sqtm-app-denormalized-custom-field[data-test-component-id="execution-prologue-dnz-value-${index}"]
    .ant-checkbox
    `;
    if (expectedValue) {
      cy.get(selector).should('have.class', 'ant-checkbox-checked');
    } else {
      cy.get(selector).should('not.have.class', 'ant-checkbox-checked');
    }
  }

  private checkDenormalizedTagCustomField(cufIndex: number, expectedValues: string[]) {
    const selector = `
    sqtm-app-denormalized-custom-field[data-test-component-id="execution-prologue-dnz-value-${cufIndex}"]
    nz-tag
    `;
    expectedValues.forEach((expectedValue, index) => {
      cy.get(selector).then(elements => {
        cy.wrap(elements.eq(index)).should('contain.text', expectedValue);
      });
    });
  }
}


function getAuthenticatedIssuePanelResponse(): any {
  return {
    entityType: 'execution',
    bugTrackerStatus: 'AUTHENTICATED',
    projectName: '["PCK01"]',
    projectId: 1,
    delete: '',
    oslc: false,
  };
}
