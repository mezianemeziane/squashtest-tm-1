import {Page} from '../page';
import {ChangeExecutionStatusButton} from '../../elements/execution-runner/change-execution-status-button';
import {EditableRichTextFieldElement} from '../../elements/forms/editable-rich-text-field.element';
import {AttachmentCompactListElement} from '../../elements/attachments/attachment-compact-list.element';
import {UploadSummary} from '../../../model/attachment/upload-summary.model';
import {StepCounterElement} from '../../elements/execution-runner/step-counter.element';
import {AttachmentUtils} from '../../../utils/attachments/attachment.utils';
import {GridElement} from '../../elements/grid/grid.element';
import {ReportIssueDialogElement, ReportIssueDialogMocks} from '../../elements/issues/report-issue-dialog.element';
import {ToggleIconElement} from '../../elements/workspace-common/toggle-icon.element';
import {TestPlanResumeModel} from '../../../model/campaign/test-plan-resume.model';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {ExecutionModel} from '../../../model/execution/execution.model';

export class ExecutionRunnerStepPage extends Page {
  public successButton: ChangeExecutionStatusButton;
  public failureButton: ChangeExecutionStatusButton;
  public blockedButton: ChangeExecutionStatusButton;
  private editableCommentField: EditableRichTextFieldElement;
  public attachmentCompactList: AttachmentCompactListElement;
  private stepCounter: StepCounterElement;
  public readonly issuesPanelGrid: GridElement;
  private fastForwardButton = new ToggleIconElement('fast-forward');

  constructor(executionStepId?: number) {
    super('sqtm-app-execution-runner-step');
    this.successButton = new ChangeExecutionStatusButton('SUCCESS');
    this.failureButton = new ChangeExecutionStatusButton('FAILURE');
    this.blockedButton = new ChangeExecutionStatusButton('BLOCKED');
    this.stepCounter = new StepCounterElement();
    const commentUrl = `execution-step/${executionStepId || '*'}/comment`;
    this.editableCommentField = new EditableRichTextFieldElement('comment', commentUrl);
    this.attachmentCompactList = new AttachmentCompactListElement();
    this.issuesPanelGrid = GridElement.createGridElement('execution-step-issues');
  }

  checkExecutionButtons() {
    this.successButton.assertExist();
    this.failureButton.assertExist();
    this.blockedButton.assertExist();
  }

  checkDocumentTitle(expectedTitle: string) {
    cy.title().should('include', expectedTitle);
  }

  checkExecutionStepper(currentStep: number, maxStep: number) {
    this.stepCounter.checkExecutionStepper(currentStep, maxStep);
    // backward button is always active
    this.stepCounter.assertBackwardButtonIsActive();
    if (currentStep < maxStep) {
      this.stepCounter.assertForwardButtonIsActive();
    } else {
      this.stepCounter.assertForwardButtonIsDisabled();
    }
  }

  checkStepExecutionStatus(expected: string) {
    cy.get(
      `sqtm-app-execution-runner-toolbar
      .execution-status`
    )
      .should('have.length', 1)
      .should('contain.text', expected);
  }

  navigateForward(): ExecutionRunnerStepPage {
    this.stepCounter.navigateForward();
    return new ExecutionRunnerStepPage();
  }

  navigateBackward(): ExecutionRunnerStepPage {
    this.stepCounter.navigateBackward();
    return new ExecutionRunnerStepPage();
  }

  navigateToArbitraryStep(stepIndex: number): ExecutionRunnerStepPage {
    this.stepCounter.navigateToArbitraryStep(stepIndex);
    if (stepIndex > 0) {
      return new ExecutionRunnerStepPage();
    }
  }

  checkAction(expectedAction: string) {
    this.checkPanelTitle('action-panel', 'Action');
    this.checkPanelRichContent('action-panel', expectedAction);
  }

  checkExpectedResult(expectedResult: string) {
    this.checkPanelTitle('result-panel', 'Résultat attendu');
    this.checkPanelRichContent('result-panel', expectedResult);
  }

  checkComment(comment: string) {
    this.checkPanelTitle('comment-panel', 'Commentaires');
    this.checkCommentRichContent(comment);
  }

  checkModificationDuringExecutionIsVisible() {
    cy.get(this.selectByComponentId('modification-during-exec')).should('be.visible');
  }

  addAttachments(files: File[], attachmentListId?: number, uploadSummary?: UploadSummary[]) {
    AttachmentUtils.addAttachments('sqtm-app-execution-runner-step>div', files, attachmentListId, uploadSummary);
  }

  private checkPanelRichContent(panelId: string, expectedContent: string) {
    const panelSelector = this.getPanelSelector(panelId);
    cy.get(`
    ${panelSelector}
    .collapse-content
    `).should('contain.html', expectedContent);
  }

  private checkPanelTitle(panelId: string, expectedTitle: string) {
    const panelSelector = this.getPanelSelector(panelId);
    cy.get(`
    ${panelSelector}
    .collapse-title
    `).should('contain.text', expectedTitle);
    return panelSelector;
  }

  private getPanelSelector(panelId: string) {
    return `sqtm-core-compact-collapse-panel[data-test-component-id="${panelId}"]`;
  }


  updateComment(newComment: string) {
    this.editableCommentField.setAndConfirmValue(newComment);
  }

  private checkCommentRichContent(comment: string) {
    this.editableCommentField.checkHtmlContent(comment);
  }

  openReportIssueDialog(mocks: ReportIssueDialogMocks): ReportIssueDialogElement {
    const dialog = new ReportIssueDialogElement(mocks);
    cy.get('[data-test-button-id="report-issue"]').click();
    cy.get('[data-test-menu-item-id="create-issue"]').click();
    dialog.waitForInitialRequests();
    return dialog;
  }

  openAttachIssueDialog(mocks: ReportIssueDialogMocks): ReportIssueDialogElement {
    const dialog = new ReportIssueDialogElement(mocks);
    cy.get('[data-test-button-id="report-issue"]').click();
    cy.get('[data-test-menu-item-id="attach-issue"]').click();
    dialog.waitForInitialRequests();
    return dialog;
  }

  assertFastForwardButtonIsActive() {
    this.fastForwardButton.assertExist();
    this.fastForwardButton.assertIsActive();
  }

  public fastForward(iterationId = '*',
                     testPlanId = '*',
                     nextExecutionId = '*',
                     testPlanResume?: TestPlanResumeModel,
                     nextExecutionModel?: ExecutionModel) {
    const resumeMock = new HttpMockBuilder(`iteration/${iterationId}/test-plan/${testPlanId}/next-execution`)
      .post()
      .responseBody(testPlanResume)
      .build();
    const executionMock = new HttpMockBuilder(`execution/${nextExecutionId}`).responseBody(nextExecutionModel).build();
    this.fastForwardButton.click();
    resumeMock.wait();
    executionMock.wait();
  }

  assertFastForwardButtonIsInactive() {
    this.fastForwardButton.assertIsNotActive();
  }
}
