import {EntityViewPage} from '../../page';
import {RequirementVersionViewInformationPage} from './requirement-version-view-information.page';
import {GridElement, TreeElement} from '../../../elements/grid/grid.element';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {ChangeVerifyingTestCaseOperationReport} from '../../../../model/change-coverage-operation-report';
import {RemoveVerifyingTestCasesDialogElement} from '../../test-case-workspace/dialogs/remove-verifying-test-cases-dialog.element';
import {RemoveRequirementVersionLinksDialogElement} from '../dialogs/remove-requirement-version-links-dialog.element';
import {RequirementLinksTypeDialogElement} from '../dialogs/requirement-links-type-dialog.element';
import {GridResponse, Identifier} from '../../../../model/grids/data-row.type';
import {RequirementVersionViewModificationHistoryPage} from './requirement-version-view-modification-history.page';
import {RequirementVersionViewRatePage} from './requirement-version-view-rate.page';
import {ReferentialData} from '../../../../model/referential-data.model';
import {basicReferentialData} from '../../../../utils/referential/referential-data.provider';
import {navigateToTestCaseSearchForCoverage} from '../../test-case-workspace/research/search-page-utils';
import {TestCaseForCoverageSearchPage} from '../../test-case-workspace/research/test-case-for-coverage-search-page';
import {RequirementViewIssuesPage} from './requirement-view-issues.page';
import Chainable = Cypress.Chainable;

export class RequirementVersionViewPage extends EntityViewPage {

  constructor(public requirementVersionId: number | '*') {
    super('sqtm-app-requirement-version-view');
  }

  public checkDataFetched() {
    throw Error('RequirementVersion data should be fetch outside of this page');
  }

  clickAnchorLink<T>(linkId: RequirementVersionViewAnchorLinks, data?: any): T {
    let element: T;
    switch (linkId) {
      case 'information':
        element = this.showInformationPanel(linkId) as unknown as T;
        break;
      case 'history':
        element = this.showModificationHistoryPanel(linkId, data) as unknown as T;
        break;
      case 'rate':
        element = this.showRatePanel(linkId) as unknown as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    return element;
  }

  private showInformationPanel<T>(linkId: 'information'): RequirementVersionViewInformationPage {
    this.clickOnAnchorLink(linkId);
    return new RequirementVersionViewInformationPage(this);
  }

  private showModificationHistoryPanel<T>(linkId: 'history', data: any): RequirementVersionViewModificationHistoryPage {
    const page = RequirementVersionViewModificationHistoryPage.navigateTo(this.requirementVersionId, data);
    this.clickOnAnchorLink(linkId);
    page.grid.waitInitialDataFetch();
    return page;
  }

  private showRatePanel<T>(linkId: 'rate'): RequirementVersionViewRatePage {
    this.clickOnAnchorLink(linkId);
    return new RequirementVersionViewRatePage(this);
  }

  private clickOnAnchorLink(linkId: string) {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
  }

  get verifyingTestCaseTable() {
    return new GridElement('requirement-version-view-verifying-tc');
  }

  get requirementLinkTable() {
    return new GridElement('requirement-version-view-link');
  }

  navigateToSearchTestCaseForCoverage(referentialData: ReferentialData = basicReferentialData,
                                      initialRows: GridResponse = {dataRows: []}): TestCaseForCoverageSearchPage {
    return navigateToTestCaseSearchForCoverage(referentialData, initialRows);
  }

  openTestcaseDrawer(response?: any): TreeElement {
    const testcaseTree = TreeElement.createTreeElement('test-case-tree-picker', 'test-case-tree', response);
    cy.get('[data-test-icon-id=add-verifying-test-case]').click();
    testcaseTree.waitInitialDataFetch();
    return testcaseTree;
  }

  openRequirementDrawer(response?: any): TreeElement {
    const requirementTree = TreeElement.createTreeElement('requirement-tree-picker', 'requirement-tree', response);
    cy.get('[data-test-icon-id=add-requirement-links]').click();
    requirementTree.waitInitialDataFetch();
    cy.clickVoid();
    cy.removeNzTooltip();
    return requirementTree;
  }

  enterIntoRequirementVersion() {
    this.getDropZone().trigger('mouseenter', {force: true, buttons: 1});
  }

  private getDropZone(): Chainable<JQuery<HTMLDivElement>> {
    return cy.get(`[data-test-element-id=${REQUIREMENT_VERSION_DROP_ZONE_ID}]`);
  }

  dropTestCaseIntoRequirement(requirementVersionId: number,
                              refreshedCoverages?: ChangeVerifyingTestCaseOperationReport,
                              refreshNodeResponse?: GridResponse) {
    const url = `requirement-version/${requirementVersionId}/verifying-test-cases`;
    const mock = new HttpMockBuilder(url)
      .post()
      .responseBody(refreshedCoverages)
      .build();
    const refreshNodeMock = new HttpMockBuilder<any>(`requirement-tree/refresh`).post().responseBody(refreshNodeResponse).build();
    this.getDropZone().trigger('mouseup', {force: true});
    mock.wait();
    refreshNodeMock.wait();
  }

  dropRequirementIntoRequirement(isRelatedIdANodeId: boolean, versionNames?: { versionName: string }): RequirementLinksTypeDialogElement {
    const url = `requirement-version/*/linked-requirement-versions/*`;
    const mock = new HttpMockBuilder(url)
      .responseBody(versionNames)
      .build();
    this.getDropZone().trigger('mouseup', {force: true});
    mock.wait();
    return new RequirementLinksTypeDialogElement();
  }

  closeDrawer() {
    cy.get('[data-test-button-id="close-drawer"]').click();
  }

  showEditTypeDialog(rowId: Identifier): RequirementLinksTypeDialogElement {
    const row = this.requirementLinkTable.getRow(rowId);
    row.cell('edit').iconRenderer().click();
    return new RequirementLinksTypeDialogElement();
  }

  showDeleteConfirmTestCaseDialog(requirementVersionId: number, rowId: number) {
    const testCaseTable = this.verifyingTestCaseTable;
    const row = testCaseTable.getRow(rowId);
    const cell = row.cell('delete');
    cell.iconRenderer().click();
    return new RemoveVerifyingTestCasesDialogElement(requirementVersionId, [rowId]);
  }

  showDeleteConfirmTestCasesDialog(requirementVersionId: number, testCasesId: number[]) {
    cy.get(`[data-test-icon-id=remove-verifying-test-case]`).click();
    return new RemoveVerifyingTestCasesDialogElement(requirementVersionId, testCasesId);
  }

  showDeleteConfirmRequirementLinkDialog(requirementVersionId: number, rowId: number) {
    const requirementLinkTable = this.requirementLinkTable;
    const row = requirementLinkTable.getRow(rowId);
    const cell = row.cell('delete');
    cell.iconRenderer().click();
    return new RemoveRequirementVersionLinksDialogElement(requirementVersionId, [rowId]);
  }

  showDeleteConfirmRequirementLinksDialog(requirementVersionId: number, rowIds: number[]) {
    cy.get(`[data-test-icon-id=remove-requirement-links]`).click();
    return new RemoveRequirementVersionLinksDialogElement(requirementVersionId, rowIds);
  }

  showIssuesWithoutBindingToBugTracker(response?: any) {
    const httpMock = new HttpMockBuilder('issues/requirement-version/*?frontEndErrorIsHandled=true').responseBody(response).build();
    cy.get(`[anchor-link-id="issues"]`).click().trigger('mouseleave');
    httpMock.wait();
    cy.removeNzTooltip();
    return new RequirementViewIssuesPage();
  }

  showIssuesIfBindedToBugTracker(modelResponse?: any, gridResponse?: GridResponse): RequirementViewIssuesPage {
    const bugTrackerModel = new HttpMockBuilder('issues/requirement-version/*?frontEndErrorIsHandled=true')
      .responseBody(modelResponse).build();
    const issues = new HttpMockBuilder('issues/requirement-version/*/known-issues/all').responseBody(gridResponse).post().build();
    cy.get(`[anchor-link-id="issues"]`).click().trigger('mouseleave');
    bugTrackerModel.wait();
    issues.wait();
    cy.removeNzTooltip();
    return new RequirementViewIssuesPage();
  }

  openCreateNewVersionDialog() {

  }
}

export type RequirementVersionViewAnchorLinks = 'information' | 'history' | 'rate';
const REQUIREMENT_VERSION_DROP_ZONE_ID = 'requirement-version-view-content';
