import {Page, WorkspaceWithGridPage} from '../../page';
import {GridElement} from '../../../elements/grid/grid.element';

export class RequirementMultiVersionPage extends WorkspaceWithGridPage {

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-requirement-multi-version-view');
  }
}
