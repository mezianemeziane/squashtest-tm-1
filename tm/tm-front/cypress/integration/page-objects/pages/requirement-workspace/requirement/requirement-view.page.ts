import {EntityViewPage} from '../../page';
import {RequirementVersionViewPage} from './requirement-version-view.page';
import {apiBaseUrl} from '../../../../utils/mocks/request-mock';

export class RequirementViewPage extends EntityViewPage {

  private readonly requirementVersionPage: RequirementVersionViewPage;

  public get currentVersion() {
    return this.requirementVersionPage;
  }

  constructor(private requirementId: number | '*', private requirementVersionId: number | '*') {
    super('sqtm-app-requirement-view');
    this.requirementVersionPage = new RequirementVersionViewPage(requirementVersionId);
  }

  // The requirement page is responsible for fetching the current requirement version data
  public checkDataFetched() {
    const url = `${apiBaseUrl()}/requirement-view/current-version/${this.requirementId}`;
    cy.wait(`@${url}`);
  }

  clickAnchorLink<T>(linkId: any, data?: any): T {
    throw Error('This page must delegate to RequirementVersionPage');
  }
}
