import {EntityViewPage} from '../../page';
import {MilestoneInformationPanel} from '../../test-case-workspace/test-case/test-case-milestone-view.page';
import {RequirementStatisticPanelElement} from '../panels/requirement-statistic-panel.element';
import {selectByDataTestComponentId} from '../../../../utils/basic-selectors';
import {TestCaseFolderAnchorLinks} from '../../test-case-workspace/test-case-folder/test-case-folder-view.page';


export class RequirementMilestoneViewPage extends EntityViewPage {

  public constructor() {
    super('sqtm-app-requirement-milestone-view');
  }

  clickAnchorLink<T>(linkId: RequirementMilestoneViewAnchorLinks, data?: any): T {
    let element: T;
    switch (linkId) {
      case 'information':
        element = this.showInformationPanel(linkId) as unknown as T;
        break;
      case 'dashboard':
        element = this.showDashboard(linkId) as unknown as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    return element;
  }

  private showInformationPanel<T>(linkId: 'information'): MilestoneInformationPanel {
    this.clickOnAnchorLink(linkId);
    return new MilestoneInformationPanel();
  }

  private clickOnAnchorLink(linkId: TestCaseFolderAnchorLinks) {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
  }

  private showDashboard(linkId: 'dashboard') {
    this.clickOnAnchorLink(linkId);
    return new RequirementStatisticPanelElement();
  }

  assertNameEquals(expectedName: string) {
    cy.get(this.rootSelector)
      .find(selectByDataTestComponentId('milestone-label'))
      .should('contain.text', expectedName);
  }
}

export type RequirementMilestoneViewAnchorLinks = 'dashboard' | 'information';
