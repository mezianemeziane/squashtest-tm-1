import {Page} from '../../page';
import {RequirementVersionViewPage} from './requirement-version-view.page';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {EditableSelectFieldElement} from '../../../elements/forms/editable-select-field.element';
import {MilestonesTagFieldElement} from '../../../elements/forms/milestones-tag-field.element';
import {Milestone} from '../../../../model/milestone/milestone.model';
import {EditableRichTextFieldElement} from '../../../elements/forms/editable-rich-text-field.element';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {EditableTextFieldElement} from '../../../elements/forms/editable-text-field.element';
import {RequirementMultiVersionPage} from './requirement-multi-version.page';
import {GridElement} from '../../../elements/grid/grid.element';
import {ReferentialData} from '../../../../model/referential-data.model';


export class RequirementVersionViewInformationPage extends Page {

  private REFRESH_URL = 'requirement-tree/refresh';

  constructor(private parentPage: RequirementVersionViewPage) {
    super('sqtm-app-requirement-version-information-panel');
  }

  get requirementVersionId() {
    return this.parentPage.requirementVersionId;
  }

  get nameTextField(): EditableTextFieldElement {
    const url = `${this.getRootModificationUrl()}/name`;
    return new EditableTextFieldElement('entity-name', url);
  }

  get categoryField() {
    return new EditableSelectFieldElement('requirement-version-category');
  }

  get statusField() {
    return new EditableSelectFieldElement('requirement-version-status');
  }

  get criticalityField() {
    return new EditableSelectFieldElement('requirement-version-criticality');
  }

  get milestoneTagElement() {
    return new MilestonesTagFieldElement('requirement-version-milestones', 'requirement-version/*/milestones/*');
  }

  get descriptionElement() {
    return new EditableRichTextFieldElement('requirement-version-description');
  }

  rename(newValue: string, refreshedRow: GridResponse = {dataRows: []}) {
    const refreshMock = this.getRefreshTreeMock(refreshedRow);
    this.nameTextField.setValue(newValue);
    this.nameTextField.confirm();
    refreshMock.wait();
  }

  private getRefreshTreeMock(refreshedRow: GridResponse) {
    return new HttpMockBuilder(this.REFRESH_URL).post().responseBody(refreshedRow).build();
  }

  changeCategory(category: string, refreshedRow: GridResponse = {dataRows: []}) {
    const mock = new HttpMockBuilder(`${this.getRootModificationUrl()}/category`).post().build();
    const refreshMock = this.getRefreshTreeMock(refreshedRow);
    const categoryField = this.categoryField;
    categoryField.selectValue(category);
    categoryField.confirm();
    mock.wait();
    refreshMock.wait();
    categoryField.checkSelectedOption(category);
  }

  changeStatus(status: string, refreshedRow: GridResponse = {dataRows: []}) {
    const mock = new HttpMockBuilder(`${this.getRootModificationUrl()}/status`).post().build();
    const refreshMock = this.getRefreshTreeMock(refreshedRow);
    const statusField = this.statusField;
    statusField.selectValue(status);
    statusField.confirm();
    mock.wait();
    refreshMock.wait();
    statusField.checkSelectedOption(status);
  }

  changeCriticality(criticality: string, refreshedRow: GridResponse = {dataRows: []}) {
    const mock = new HttpMockBuilder(`${this.getRootModificationUrl()}/criticality`).post().build();
    const refreshMock = this.getRefreshTreeMock(refreshedRow);
    const criticalityField = this.criticalityField;
    criticalityField.selectValue(criticality);
    criticalityField.confirm();
    mock.wait();
    refreshMock.wait();
    criticalityField.checkSelectedOption(criticality);
  }

  bindMilestone(milestoneId: number, bindableMilestones?: Milestone[]) {
    const milestoneTagElement = this.milestoneTagElement;
    const milestonePickerDialog = milestoneTagElement.openMilestoneDialog();
    milestonePickerDialog.selectMilestone(milestoneId);
    const mock = new HttpMockBuilder('requirement-version/*/milestones/*').post().responseBody(bindableMilestones).build();
    milestonePickerDialog.confirm();
    mock.wait();
  }

  changeDescription(description: string, refreshedRow: GridResponse = {dataRows: []}) {
    const mock = new HttpMockBuilder(`${this.getRootModificationUrl()}/description`).post().build();
    const refreshMock = this.getRefreshTreeMock(refreshedRow);
    const descriptionElement = this.descriptionElement;
    descriptionElement.enableEditMode();
    descriptionElement.setValue(description);
    descriptionElement.confirm();
    mock.wait();
    refreshMock.wait();
  }

  clickOnVersionLink(response?: GridResponse, referentialData?: ReferentialData): RequirementMultiVersionPage {
    const mock = new HttpMockBuilder('referential').responseBody(referentialData).build();
    const grid = GridElement.createGridElement('requirement-versions-grid', 'requirement-view/versions/*', response);
    cy.get('[data-test-field-id="requirement-version-number"]').click();
    mock.wait();
    grid.waitInitialDataFetch();
    return new RequirementMultiVersionPage(grid);
  }

  private getRootModificationUrl() {
    return `requirement-version/${this.requirementVersionId}`;
  }

}
