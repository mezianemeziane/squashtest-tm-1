import {Page} from '../../page';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {ReferentialData} from '../../../../model/referential-data.model';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder
} from '../../../../utils/referential/referential-data.provider';
import {GridElement} from '../../../elements/grid/grid.element';
import {RequirementSearchModel} from './requirement-search-model';
import {buildRequirementSearchGrid, buildRequirementSearchPageMock} from './search-page-utils';
import {ToolbarElement} from '../../../elements/workspace-common/toolbar.element';
import {AlertDialogElement} from '../../../elements/dialog/alert-dialog.element';
import {MassEditRequirementDialog} from './dialogs/mass-edit-requirement.dialog';
import {EditMilestonesDialog} from '../../test-case-workspace/research/dialogs/edit-milestones-dialog';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {MilestoneMassEdit} from '../../../../model/milestone/milestone.model';
import {NoMilestoneSharedDialog} from '../dialogs/no-milestone-shared-dialog';
import {MilestoneAlreadyBoundDialog} from '../dialogs/MilestoneAlreadyBoundDialog';
import {NoPermissionToBindMilestoneDialog} from '../dialogs/NoPermissionToBindMilestoneDialog';
import {selectByDataTestToolbarButtonId} from '../../../../utils/basic-selectors';

export class RequirementSearchPage extends Page {

  constructor(public grid: GridElement) {
    super('sqtm-app-requirement-search-page');
  }

  public static initTestAtPage(
    referentialData: ReferentialData = basicReferentialData,
    initialRows: GridResponse = {dataRows: []},
    requirementSearchModel: RequirementSearchModel = {
      usersWhoCreatedRequirements: [],
      usersWhoModifiedRequirements: []
    },
    queryString: string = ''): RequirementSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = buildRequirementSearchPageMock(requirementSearchModel);
    const grid = buildRequirementSearchGrid(initialRows);
    // visit page
    cy.visit(`search/requirement?${queryString}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    pageDataMock.wait();
    // wait for initial tree data request to fire
    return new RequirementSearchPage(grid);
  }

  showExportDialog(): AlertDialogElement {
    const toolbarElement = new ToolbarElement('requirement-search-toolbar');
    toolbarElement.button('export-button').click();
    return new AlertDialogElement('requirement-export-dialog');
  }

  showMassEditDialog(): MassEditRequirementDialog {
    const toolbarElement = new ToolbarElement('requirement-search-toolbar');
    toolbarElement.button('mass-edit-button').clickWithoutSpan();
    return new MassEditRequirementDialog();
  }

  showMassBindingMilestoneDialog(milestoneMassEdit: MilestoneMassEdit): EditMilestonesDialog {
    const mock = new HttpMockBuilder('search/milestones/requirement/*').responseBody(milestoneMassEdit).build();
    const milestoneButton = cy.get(`${this.buildToolBarSelector()} ${selectByDataTestToolbarButtonId('edit-milestones-button')}`);
    milestoneButton.should('exist');
    milestoneButton.click();
    mock.wait();
    return new EditMilestonesDialog();
  }

  showNoPermissionsToEditMilestoneDialog(): AlertDialogElement {
    const toolbarElement = new ToolbarElement('requirement-search-toolbar');
    toolbarElement.button('edit-milestones-button').clickWithoutSpan();
    return new AlertDialogElement('alert');
  }

  getNoMilestoneSharedDialog(): NoMilestoneSharedDialog {
    return new NoMilestoneSharedDialog();
  }

  getNoPermissionToBindMilestoneDialog(): NoPermissionToBindMilestoneDialog {
    const toolbarElement = new ToolbarElement('requirement-search-toolbar');
    toolbarElement.button('edit-milestones-button').clickWithoutSpan();
    return new NoPermissionToBindMilestoneDialog();
  }

  getMilestoneAlreadyBoundDialog(): MilestoneAlreadyBoundDialog {
    return new MilestoneAlreadyBoundDialog();
  }

  foldFilterPanel() {
    cy.get(this.selectByComponentId('fold-filter-panel-button')).click();
  }

  buildToolBarSelector(): string {
    return '[data-test-toolbar-id=requirement-search-toolbar]';
  }

}
