import {RequirementSearchModel} from './requirement-search-model';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {GridElement} from '../../../elements/grid/grid.element';
import {ReferentialData} from '../../../../model/referential-data.model';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder
} from '../../../../utils/referential/referential-data.provider';
import {RequirementForCoverageSearchPage} from './requirement-for-coverage-search-page';

export function buildRequirementSearchPageMock
(requirementSearchModel: RequirementSearchModel = {usersWhoCreatedRequirements: [], usersWhoModifiedRequirements: []}) {
  return new HttpMockBuilder<RequirementSearchModel>('search/requirement').responseBody(requirementSearchModel).build();
}

export function buildRequirementSearchGrid(initialRows: GridResponse) {
  return GridElement.createGridElement('requirement-search', 'search/requirement', initialRows);
}

export function navigateToRequirementSearchForCoverage
(referentialData: ReferentialData = basicReferentialData, initialRows: GridResponse = {dataRows: []}) {
  const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
  const pageDataMock = buildRequirementSearchPageMock();
  const grid = buildRequirementSearchGrid(initialRows);
  // visit page
  cy.get('[data-test-icon-id=search-coverages]').click();
  // wait for ref data request to fire
  referentialDataProvider.wait();
  pageDataMock.wait();
  // wait for initial tree data request to fire
  return new RequirementForCoverageSearchPage(grid);
}
