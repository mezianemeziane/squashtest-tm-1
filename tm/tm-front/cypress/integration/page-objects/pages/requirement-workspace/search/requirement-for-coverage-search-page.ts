import {Page} from '../../page';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {ReferentialData} from '../../../../model/referential-data.model';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder
} from '../../../../utils/referential/referential-data.provider';
import {GridElement} from '../../../elements/grid/grid.element';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {RequirementSearchModel} from './requirement-search-model';
import {ToggleIconElement} from '../../../elements/workspace-common/toggle-icon.element';
import {ChangeCoverageOperationReport} from '../../../../model/change-coverage-operation-report';

export class RequirementForCoverageSearchPage extends Page {

  private linkSelectionButton = new ToggleIconElement('link-selection-button');
  private linkAllButton = new ToggleIconElement('link-all-button');
  private cancelButton = new ToggleIconElement('cancel-button');

  constructor(public grid: GridElement) {
    super('sqtm-app-requirement-for-coverage-search-page');
  }

  public static initTestAtPage(
    testCaseId: string,
    referentialData: ReferentialData = basicReferentialData,
    initialRows: GridResponse = {dataRows: []},
    requirementSearchModel: RequirementSearchModel = {
      usersWhoCreatedRequirements: [],
      usersWhoModifiedRequirements: []
    },
    queryString: string = ''): RequirementForCoverageSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = new HttpMockBuilder<RequirementSearchModel>('search/requirement').responseBody(requirementSearchModel).build();
    const grid = GridElement.createGridElement('requirement-search', 'search/requirement', initialRows);
    // visit page
    cy.visit(`search/requirement/coverage/${testCaseId}?${queryString}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    pageDataMock.wait();
    // wait for initial tree data request to fire
    return new RequirementForCoverageSearchPage(grid);
  }

  clickAddCriteria() {
    cy.get('div[data-test-component-id="grid-filter-manager-add-criteria"]')
      .click();
  }

  selectRowsWithMatchingCellContent(cellId: string, contents: string[]) {
    this.grid.selectRowsWithMatchingCellContent(cellId, contents);
  }

  selectRowWithMatchingCellContent(cellId: string, content: string) {
    this.grid.selectRowWithMatchingCellContent(cellId, content);
  }

  foldFilterPanel() {
    cy.get(this.selectByComponentId('fold-filter-panel-button')).click();
  }

  assertLinkSelectionButtonExist() {
    this.linkSelectionButton.assertExist();
  }

  assertLinkSelectionButtonIsActive() {
    this.linkSelectionButton.assertIsActive();
  }

  assertLinkSelectionButtonIsNotActive() {
    this.linkSelectionButton.assertIsNotActive();
  }

  assertLinkAllButtonExist() {
    this.linkAllButton.assertExist();
  }

  assertLinkAllButtonIsActive() {
    this.linkAllButton.assertIsActive();
  }

  assertNavigateBackButtonExist() {
    this.cancelButton.assertExist();
  }

  assertNavigateBackButtonIsActive() {
    this.cancelButton.assertIsActive();
  }

  linkSelection(
    testCaseId = '*',
    response: ChangeCoverageOperationReport = {
      coverages: [],
      summary: {}
    }) {
    const mock = this.buildLinkRequestMock(testCaseId, response);
    this.linkSelectionButton.click();
    mock.wait();
  }

  linkAll(
    testCaseId = '*',
    response: ChangeCoverageOperationReport = {
      coverages: [],
      summary: {}
    }) {
    const mock = this.buildLinkRequestMock(testCaseId, response);
    this.linkAllButton.click();
    mock.wait();
  }

  private buildLinkRequestMock(testCaseId: string, response: ChangeCoverageOperationReport) {
    return new HttpMockBuilder(`/test-cases/${testCaseId}/verified-requirements`).post()
      .responseBody(response)
      .build();
  }
}
