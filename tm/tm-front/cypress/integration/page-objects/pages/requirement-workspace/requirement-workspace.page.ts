import {TreeElement} from '../../elements/grid/grid.element';
import {ReferentialDataProviderBuilder} from '../../../utils/referential/referential-data.provider';
import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {ReferentialData} from '../../../model/referential-data.model';
import {GridResponse} from '../../../model/grids/data-row.type';
import {WorkspaceWithTreePage} from '../workspace-with-tree.page';
import {RequirementWorkspaceTreeMenu} from './tree/requirement-workspace-tree-menu';

export class RequirementWorkspacePage extends WorkspaceWithTreePage {
  public readonly navBar: NavBarElement = new NavBarElement();
  public readonly treeMenu: RequirementWorkspaceTreeMenu;

  public constructor(public readonly tree: TreeElement, rootSelector: string) {
    super(tree, rootSelector);
    this.treeMenu = new RequirementWorkspaceTreeMenu();
  }

  public static initTestAtPage
  (initialNodes: GridResponse = {dataRows: []}, referentialData?: ReferentialData): RequirementWorkspacePage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const tree = TreeElement.createTreeElement('requirement-workspace-main-tree', 'requirement-tree', initialNodes);
    // visit page
    cy.visit('requirement-workspace');
    // wait for ref data request to fire
    referentialDataProvider.wait();
    // wait for initial tree data request to fire
    tree.waitInitialDataFetch();
    return new RequirementWorkspacePage(tree, 'sqtm-app-requirement-workspace');
  }

}
