import {AlertDialogElement} from '../../../elements/dialog/alert-dialog.element';


export class NoPermissionToBindMilestoneDialog extends AlertDialogElement {
  constructor() {
    super('alert');
  }

  assertMessage() {
    this.assertHasMessage('Aucun des éléments sélectionnés ne peut être édité car vous ne disposez pas de ' +
      'droits suffisants.');
  }
}
