import {AlertDialogElement} from '../../../elements/dialog/alert-dialog.element';


export class NoMilestoneSharedDialog extends AlertDialogElement {
  constructor() {
    super('alert');
  }

  assertMessage() {
    this.assertHasMessage('La modification en masse des jalons est impossible car les projets auxquels ' +
      'appartiennent les versions d\'exigence séléctionnées n\'ont pas de jalon en commun.');
  }
}
