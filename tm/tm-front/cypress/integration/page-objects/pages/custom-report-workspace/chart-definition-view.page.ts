import {EntityViewPage} from '../page';
import {apiBaseUrl} from '../../../utils/mocks/request-mock';

class ChartInformationPanelElement {
  constructor(private customReportLibraryNodeId: number | '*') {
  }
}

class ChartPanelElement {
  constructor(customReportLibraryNodeId: number | '*') {
  }
}

export class ChartDefinitionViewPage extends EntityViewPage {

  constructor(private customReportLibraryNodeId: number | '*') {
    super('sqtm-app-chart-definition-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/chart-definition-view/${this.customReportLibraryNodeId}`;
    cy.wait(`@${url}`);
  }

  clickAnchorLink<T>(linkId: ChartDefinitionAnchorLinks, data?: any): T {
    let element: T;
    switch (linkId) {
      case 'chart':
        element = this.showChartPanel(linkId) as unknown as T;
        break;
      case 'information':
        element = this.showInformationPanel(linkId) as unknown as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    return element;
  }

  private showChartPanel<T>(linkId: 'chart'): ChartPanelElement {
    this.clickOnAnchorLink(linkId);
    return new ChartPanelElement(this.customReportLibraryNodeId);
  }

  private showInformationPanel<T>(linkId: 'information'): ChartInformationPanelElement {
    this.clickOnAnchorLink(linkId);
    return new ChartInformationPanelElement(this.customReportLibraryNodeId);
  }

  private clickOnAnchorLink(linkId: string) {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
  }

  foldTree() {
    cy.get(`[data-test-component-id="fold-tree-button"]`).click();
  }

  checkPerimeter(expectedPerimeter: string) {
    cy.get(`[data-test-field-id=perimeter-information]`).should('contain.text', expectedPerimeter);
  }

  checkAxisInfo(expectedAxis: string) {
    cy.get(`[data-test-field-id=axis-information]`).should('contain.text', expectedAxis);
  }

  checkMeasureInfo(expectedMeasure: string) {
    cy.get(`[data-test-field-id=measure-information]`).should('contain.text', expectedMeasure);
  }

  assertMeasureInfoNotExists() {
    cy.get(`[data-test-field-id=measure-information]`).should('not.exist');
  }

  checkSeriesInfo(expectedSeries: string) {
    cy.get(`[data-test-field-id=series-information]`).should('contain.text', expectedSeries);
  }

  assertSeriesInfoNotExists() {
    cy.get(`[data-test-field-id=series-information]`).should('not.exist');
  }

  checkFilterInfo(filterIndex: number, expectedFilter: string) {
    cy.get(`[data-test-field-id=filter-information-${filterIndex}]`).should('contain.text', expectedFilter);
  }

  assertFiltersInfoNotExists() {
    cy.get(`[data-test-field-id=filters-label]`).should('not.exist');
  }

}

export type ChartDefinitionAnchorLinks = 'chart' | 'information';
