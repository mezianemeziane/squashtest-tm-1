import {Page} from '../page';
import {ReferentialData} from '../../../model/referential-data.model';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder
} from '../../../utils/referential/referential-data.provider';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {
  NodeType,
  ReportDefinitionViewModel,
  ReportInputType,
  ReportWorkbenchData,
  StandardReportCategory
} from '../../../model/custom-report/report-definition.model';
import {ReportWidget} from './report-widget/report-widget';
import {selectByDataTestComponentId} from '../../../utils/basic-selectors';
import {BindableEntity} from '../../../model/bindable-entity.model';
import {TreeElement} from '../../elements/grid/grid.element';
import {CustomReportWorkspacePage} from './custom-report-workspace.page';
import {DataRow} from '../../../model/grids/data-row.type';
import {mockFieldValidationError} from '../../../data-mock/http-errors.data-mock';
import {TextFieldElement} from '../../elements/forms/TextFieldElement';
import {mockReportDefinitionViewModel} from '../../../data-mock/report-definition.data-mock';
import {RichTextFieldElement} from '../../elements/forms/RichTextFieldElement';

export class CreateReportViewPage extends Page {

  public descriptionField = new RichTextFieldElement('description');

  constructor() {
    super('sqtm-app-create-report-view');
  }

  assertPreparationPhaseReportListExist() {
    cy.get(this.selectByComponentId('preparation-phase-report-list')).should('exist');
  }

  assertPreparationPhaseReportExist(reportId: string, expectedLabel: string) {
    this.assertReportExist('preparation', reportId, expectedLabel);
  }

  assertExecutionPhaseReportExist(reportId: string, expectedLabel: string) {
    this.assertReportExist('execution', reportId, expectedLabel);
  }

  assertVariousReportExist(reportId: string, expectedLabel: string) {
    this.assertReportExist('various', reportId, expectedLabel);
  }

  private assertReportExist(phase: 'preparation' | 'execution' | 'various', reportId: string, expectedLabel: string) {
    cy.get(this.selectByComponentId(`${phase}-phase-report-list`))
      .find(this.selectByComponentId(reportId))
      .should('exist')
      .should('contain.text', expectedLabel);
  }

  assertExecutionPhaseReportListExist() {
    cy.get(this.selectByComponentId('execution-phase-report-list')).should('exist');
  }

  selectReport(reportId: string) {
    cy.get(this.selectByComponentId(reportId)).click();
  }

  assertNoReportIsSelected() {
    cy.get(this.selectByComponentId('selected-report-title')).should('not.exist');
  }

  assertReportIsSelected(reportId: string) {
    this.getSelectedReportPanel(reportId).should('be.visible');
  }

  private getSelectedReportPanel(reportId: string) {
    return cy.get(`[data-test-report-id="${reportId}"]`);
  }

  assertReportHasTitle(expectedTitle: string) {
    cy.get(this.selectByComponentId('selected-report-title')).should('contain.text', expectedTitle);
  }

  findCriteriaWidget(fieldId: string, fieldType: ReportInputType) {
    return ReportWidget.create(fieldId, fieldType);
  }

  clickOnDownload() {
    cy.get(this.selectByComponentId('download-report')).click();
  }

  toggleReportSelector() {
    cy.get(selectByDataTestComponentId('toggle-report-selector')).click();
  }

  public static initTestAtPage(
    referentialData: ReferentialData = basicReferentialData,
    reportWorkbenchData: ReportWorkbenchData = basicWorkbenchData,
    nodeReference: string = '*'): CreateReportViewPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const mock = new HttpMockBuilder(`report-workbench/${nodeReference}`).responseBody(reportWorkbenchData).build();
    // visit page
    cy.visit(`custom-report-workspace/create-report-definition/${nodeReference}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    mock.wait();
    return new CreateReportViewPage();
  }

  simulateDuplicateNodeFailure(containerId = '*'): void {
    const createReportMock = new HttpMockBuilder(`report-workbench/new/${containerId}`)
      .post()
      .status(412)
      .responseBody(mockFieldValidationError('name', 'sqtm-core.error.generic.duplicate-name'))
      .build();
    this.clickOnSaveButton();
    createReportMock.wait();
  }

  saveReport(containerId = '*',
             createdReportId?: number,
             initialNodes: DataRow[] = [],
             reportDefModel?: Partial<ReportDefinitionViewModel>,
             referentialData?: ReferentialData): CustomReportWorkspacePage {
    const createReportMock = new HttpMockBuilder(`report-workbench/new/${containerId}`).post().responseBody({id: createdReportId}).build();
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const tree = TreeElement.createTreeElement('custom-report-workspace-main-tree', 'custom-report-tree', {dataRows: initialNodes});
    const reportDef = new HttpMockBuilder(`report-definition-view/${createdReportId ? createdReportId : '*'}`)
      .responseBody(mockReportDefinitionViewModel(reportDefModel))
      .build();
    this.clickOnSaveButton();
    createReportMock.wait();
    referentialDataProvider.wait();
    tree.waitInitialDataFetch();
    reportDef.wait();
    return new CustomReportWorkspacePage(tree, 'sqtm-app-custom-report-workspace');
  }

  private clickOnSaveButton() {
    cy.get(selectByDataTestComponentId('save-report')).click();
  }

  assertDuplicateNodeNameErrorIsVisible() {
    new TextFieldElement('name').assertErrorExist('sqtm-core.error.generic.duplicate-name');
  }
}

export const requirementEditableReport = {
  'label': 'Cahier des exigences (format éditable)',
  'id': 'report.books.requirements.requirements.report.label',
  'description': 'Permet de générer un rapport contenant une sélection de fiches d\'exigences.',
  'category': StandardReportCategory.PREPARATION_PHASE,
  'inputs': [
    {
      'name': 'requirementsSelectionMode',
      'type': ReportInputType.RADIO_BUTTONS_GROUP,
      'disabledBy': null,
      'label': 'Périmètre du rapport',
      'required': true,
      'options': [
        {
          'name': 'requirementsSelectionMode',
          'label': 'Sélectionner le périmètre projet',
          'value': 'PROJECT_PICKER',
          'defaultSelected': true,
          'givesAccessTo': 'projectIds',
          'contentType': ReportInputType.PROJECT_PICKER,
          'composite': true
        },
        {
          'name': 'requirementsSelectionMode',
          'label': 'Sélectionner un ou plusieurs nœuds dans l\'arbre des exigences',
          'value': 'TREE_PICKER',
          'defaultSelected': false,
          'givesAccessTo': 'requirementsIds',
          'contentType': ReportInputType.TREE_PICKER,
          'composite': true,
          'nodeType': NodeType.REQUIREMENT
        },
        {
          'name': 'requirementsSelectionMode',
          'label': 'Sélectionner un jalon',
          'value': 'MILESTONE_PICKER',
          'defaultSelected': false,
          'givesAccessTo': 'milestones',
          'contentType': ReportInputType.MILESTONE_PICKER,
          'composite': true
        },
        {
          'name': 'requirementsSelectionMode',
          'label': 'Choisir des tags',
          'value': 'TAG_PICKER',
          'defaultSelected': false,
          'givesAccessTo': 'tags',
          'contentType': ReportInputType.TAG_PICKER,
          'composite': true,
          bindableEntity: BindableEntity.REQUIREMENT_VERSION
        }
      ]
    },
    {
      'name': 'reportOptions',
      'type': ReportInputType.CHECKBOXES_GROUP,
      'disabledBy': null,
      'label': 'Option d\'impression',
      'required': false,
      'options': [
        {
          'name': 'reportOptions',
          'label': 'Imprimer seulement la dernière version d\'exigence',
          'value': 'printOnlyLastVersion',
          'defaultSelected': true,
          'givesAccessTo': 'chk-printonlylastversion',
          'contentType': ReportInputType.CHECKBOX,
          'composite': true
        }
      ]
    }
  ]
};

export const anotherRequirementEditableReport = {
  ...requirementEditableReport,
  id: 'another.report.books.requirements.requirements.report.label',
  label: 'Un autre rapport'
};

export const executionAdvanceReport = {
  'label': 'Avancement de l\'exécution',
  'id': 'report.executionprogressfollowup.name',
  'description': 'Permet de générer un tableau de bord de suivi de l\'exécution des tests.',
  'category': StandardReportCategory.EXECUTION_PHASE,
  'inputs': [
    {
      'name': 'campaignSelectionMode',
      'type': ReportInputType.RADIO_BUTTONS_GROUP,
      'disabledBy': null,
      'label': 'Périmètre du rapport',
      'required': false,
      'options': [
        {
          'name': 'campaignSelectionMode',
          'label': 'Toutes les campagnes',
          'value': 'EVERYTHING',
          'defaultSelected': true,
          'givesAccessTo': 'none',
          'contentType': ReportInputType.CHECKBOX,
          'composite': true
        },
        {
          'name': 'campaignSelectionMode',
          'label': 'Les campagnes sélectionnées',
          'value': 'TREE_PICKER',
          'defaultSelected': false,
          'givesAccessTo': 'campaignIds',
          'contentType': ReportInputType.TREE_PICKER,
          'composite': true
        },
        {
          'name': 'campaignSelectionMode',
          'label': 'Toutes les campagnes du jalon sélectionné',
          'value': 'MILESTONE_PICKER',
          'defaultSelected': false,
          'givesAccessTo': 'milestones',
          'contentType': ReportInputType.MILESTONE_PICKER,
          'composite': true
        }
      ]
    },

    // null,
    // {
    //   'name': 'campaignStatus',
    //   'type': 'DROPDOWN_LIST',
    //   'disabledBy': null,
    //   'label': 'Statut de la campagne',
    //   'required': false,
    //   'options': [
    //     {
    //       'name': 'campaignStatus',
    //       'label': 'Tous',
    //       'value': 'CAMPAIGN_ALL',
    //       'defaultSelected': false,
    //       'givesAccessTo': 'none',
    //       'contentType': 'CHECKBOX',
    //       'composite': true
    //     },
    //     {
    //       'name': 'campaignStatus',
    //       'label': 'En cours',
    //       'value': 'CAMPAIGN_RUNNING',
    //       'defaultSelected': false,
    //       'givesAccessTo': 'none',
    //       'contentType': 'CHECKBOX',
    //       'composite': true
    //     },
    //     {
    //       'name': 'campaignStatus',
    //       'label': 'Terminé',
    //       'value': 'CAMPAIGN_OVER',
    //       'defaultSelected': false,
    //       'givesAccessTo': 'none',
    //       'contentType': 'CHECKBOX',
    //       'composite': true
    //     }
    //   ]
    // }
  ]
};

export const variousEditableReport = {
  ...requirementEditableReport,
  label: 'Rapport divers',
  id: 'various.report.books.requirements.requirements.report.label',
  description: 'Permet de générer un rapport divers contenant une sélection de fiches d\'exigences.',
  category: StandardReportCategory.VARIOUS,
};

const basicWorkbenchData: ReportWorkbenchData = {
  availableReports: [requirementEditableReport, anotherRequirementEditableReport, executionAdvanceReport, variousEditableReport],
  projectId: 1,
  reportDefinition: null
};
