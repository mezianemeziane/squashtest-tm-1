import {EntityViewPage, Page} from '../page';

export class CustomReportFolderViewPage extends EntityViewPage {

  constructor(private customReportLibraryNodeId: number | '*') {
    super('sqtm-app-custom-report-folder-view');
  }

  clickAnchorLink(linkId: string): Page {
    return undefined;
  }
}
