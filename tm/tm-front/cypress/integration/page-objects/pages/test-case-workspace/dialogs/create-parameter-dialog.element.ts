import {TextFieldElement} from '../../../elements/forms/TextFieldElement';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {TestCaseParameterOperationReport} from '../../../../model/test-case/change-dataset-param-operation.model';
import {RichTextFieldElement} from '../../../elements/forms/RichTextFieldElement';

export class CreateParameterDialogElement {

  nameField: TextFieldElement;
  descriptionField: RichTextFieldElement;

  constructor() {
    this.nameField = new TextFieldElement('name');
    this.descriptionField = new RichTextFieldElement('description');
  }

  fillName(value: string) {
    this.nameField.fill(value);
  }

  fillDescription(value: string) {
    this.descriptionField.fill(value);
  }

  addParam(operation?: TestCaseParameterOperationReport, another?: boolean) {

    const httpMock = new HttpMockBuilder('test-cases/*/parameters/new').post().responseBody(operation).build();
    if (another) {
      this.clickButton('add-another');
    } else {
      this.clickButton('add');
    }
    httpMock.wait();
  }

  addAnotherParam(operation?: TestCaseParameterOperationReport) {
    this.addParam(operation, true);
  }

  clickButton(buttonId: string) {
    cy.get(`button[data-test-dialog-button-id="${buttonId}"]`).click();
  }

  checkDialogButtons() {
    cy.get('button[data-test-dialog-button-id="add-another"]').should('exist');
    cy.get('button[data-test-dialog-button-id="add"]').should('exist');
    cy.get('button[data-test-dialog-button-id="cancel"]').should('exist');
  }
}
