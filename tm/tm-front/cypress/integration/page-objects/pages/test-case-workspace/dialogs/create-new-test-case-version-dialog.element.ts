import {TextFieldElement} from '../../../elements/forms/TextFieldElement';
import {RichTextFieldElement} from '../../../elements/forms/RichTextFieldElement';
import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseModel} from '../../../../model/test-case/test-case.model';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import Chainable = Cypress.Chainable;

export class CreateNewTestCaseVersionDialog {

  private readonly nameField: TextFieldElement;
  private readonly referenceField: TextFieldElement;
  private readonly descriptionField: RichTextFieldElement;

  constructor() {
    this.nameField = new TextFieldElement('name');
    this.referenceField = new TextFieldElement('reference');
    this.descriptionField = new RichTextFieldElement('description');
  }

  public fillName(name: string) {
    this.nameField.fill(name);
  }

  public checkNameContent(expectedContent: string) {
    this.nameField.checkContent(expectedContent);
  }

  public fillReference(reference: string) {
    this.referenceField.fill(reference);
  }

  public checkReferenceContent(expectedContent: string) {
    this.referenceField.checkContent(expectedContent);
  }

  public fillDescription(description: string) {
    this.descriptionField.fill(description);
  }

  public checkDescriptionContent(expectedContent: string) {
    this.descriptionField.checkHtmlTextContent(expectedContent);
  }

  public confirm(originalId = '*', refreshedParent = '*', refreshedNodes: DataRow[] = [], newVersionModel?: TestCaseModel) {
    const newVersionId = newVersionModel ? newVersionModel.id : null;
    const createVersionMock = new HttpMockBuilder(`test-case-tree/${originalId}/new-version`)
      .post()
      .responseBody({newVersionId})
      .build();

    const refreshNodeMock = new HttpMockBuilder<GridResponse>(`test-case-tree/refresh`)
      .post()
      .responseBody({dataRows: refreshedNodes, count: refreshedNodes.length})
      .build();

    const newVersionIdAsString = newVersionId ? newVersionId.toString() : '*';
    const selectNodeMock = new HttpMockBuilder<TestCaseModel>(`test-case-view/${newVersionIdAsString}`)
      .responseBody(newVersionModel)
      .build();

    this.getConfirmButton().click();
    createVersionMock.wait();
    refreshNodeMock.wait();
    selectNodeMock.wait();
  }

  checkCreateTestCaseDialogButtons() {
    const button = this.getConfirmButton();
    button.should('exist');
    cy.get('button[data-test-dialog-button-id="cancel"]').should('exist');
  }

  private getConfirmButton(): Chainable {
    return cy.get('button[data-test-dialog-button-id="confirm"]');
  }
}
