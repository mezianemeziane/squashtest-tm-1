import {SelectFieldElement} from '../../../elements/forms/select-field.element';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {DataRow, Identifier} from '../../../../model/grids/data-row.type';
import {CommonSelectors} from '../../../elements/forms/abstract-form-field.element';


export class AddTcFromRequirementDialogElement {

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  getField(fieldName: string): SelectFieldElement {
    return new SelectFieldElement(CommonSelectors.fieldName(fieldName));
  }

  confirm(destinationId: Identifier, tcKind: string, refreshedNodes: DataRow[]) {
    const httpMock = new HttpMockBuilder('test-case-tree/' + destinationId + '/content/paste-from-requirement/' + tcKind).post().build();
    const refreshUrl = `test-case-tree/refresh`;
    const httpRefreshMock = new HttpMockBuilder(refreshUrl).post().responseBody({dataRows: refreshedNodes}).build();
    this.clickButton('confirm');
    httpMock.wait();
    httpRefreshMock.wait();
  }


  buildSelector(): string {
    return '[data-test-dialog-id=add-tc-from-req]';
  }

  clickButton(buttonId: string) {
    const confirmButton = cy.get(`button[data-test-dialog-button-id=${buttonId}]`);
    confirmButton.should('exist');
    confirmButton.click();
  }
}
