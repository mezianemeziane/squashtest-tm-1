import Chainable = Cypress.Chainable;
import { DuplicateActionWord } from '../../../../model/test-case/duplicate-action-word.model';

export class DuplicateActionWordDialogElement {

  public readonly stringSelector: string = '[data-test-dialog-id="duplicate-action-word"]';

  constructor() {
  }

  get selector(): Chainable {
    return cy.get(this.stringSelector);
  }

  assertExist() {
    this.selector.should('exist');
  }

  checkContent(duplicateActionWords: DuplicateActionWord[]) {
    duplicateActionWords.forEach(duplicateActionWord => {
      this.selector.find('label span')
        .contains(duplicateActionWord.projectName)
        .should('exist');
    })
  }

  chooseProject(projectName: string) {
    this.selector.find('label span').contains(projectName).click();
  }

  confirm() {
    this.selector.find('[data-test-dialog-button-id="confirm"]').click();
  }

  cancel() {
    this.selector.find('[data-test-dialog-button-id="cancel"]').click();
  }
}
