export class ExportScriptDialog {

  private scriptType: ExportableScriptType;

  constructor(scriptType: ExportableScriptType) {
    this.scriptType = scriptType;
  }

  assertExist() {
    cy.get(`sqtm-app-${this.scriptType}-script-export-dialog`).should('exist');
  }

  assertNotExist() {
    cy.get(`sqtm-app-${this.scriptType}-script-export-dialog`).should('not.exist');
  }

  cancelScriptExport() {
    cy.get('[data-test-dialog-button-id="cancel"]').click();
  }

  assertButtonExist(buttonId: string) {
    cy.get(`[data-test-dialog-button-id="${buttonId}"]`).should('exist');
  }

  checkTitle(title: string) {
    cy.get('sqtm-core-dialog')
      .find(`sqtm-app-${this.scriptType}-script-export-dialog`)
      .contains('span.font-dialog-title', title);
  }

  checkExportScriptDialogButtons() {
    this.assertButtonExist('export');
    this.assertButtonExist('cancel');
  }

  checkDownloadFileName(fileName: string) {
    cy.get(`a[data-test-dialog-button-id="export"]`)
      .should('have.attr', 'download')
      .and('contain', fileName);
  }
}

export type ExportableScriptType = 'gherkin' | 'bdd';

