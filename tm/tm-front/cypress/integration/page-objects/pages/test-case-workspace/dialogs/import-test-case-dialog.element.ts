import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {XlsReport} from '../../../../model/test-case/import-test-case.model';
import {GridResponse} from '../../../../model/grids/data-row.type';

export class ImportTestCaseDialog {

  chooseImportFile(fileName: string, fileType: string) {
    cy.fixture(fileName, 'binary').as('importFile')
      .get('sqtm-core-attachment-file-selector')
      .find('input[type=file]').then(function (el) {
      const inputFile = el[0];
      const file = Cypress.Blob.binaryStringToBlob(this.importFile, fileType);

      const testFile = new File([file], fileName, {
        type: file.type
      });
      const dataTransfer = new DataTransfer();
      dataTransfer.items.add(testFile);
      (inputFile as HTMLInputElement).files = dataTransfer.files;
      inputFile.dispatchEvent(new Event('change'));

    });
  }


  clickImport() {
    cy.get('[data-test-dialog-button-id="import"]').click();
  }

  checkConfirmationMessage(fileName: string) {
    cy.get('[data-test-message-id="confirmation-message"')
      .should('contain.html', `<p>Vos données seront importées depuis le fichier <b>${fileName}</b>.</p>
<p>Cette opération ne peut être annulée. Confirmez-vous l'import ?</p>`);
  }

  confirmXlsImport(gridResponse: GridResponse = {} as GridResponse, response?: XlsReport) {
    const mockBuilder = new HttpMockBuilder('test-cases/importer/xls').responseBody(response).post().build();
    const mockTree = new HttpMockBuilder('test-case-tree/refresh').responseBody(gridResponse).post().build();
    cy.get('[data-test-dialog-button-id="confirm"]').click();
    mockBuilder.wait();
    mockTree.wait();
  }

  assertButtonExist(buttonId: string) {
    cy.get(`[data-test-dialog-button-id="${buttonId}"]`).should('exist');
  }

  assertReportExist(reportId: string) {
    cy.get(`[data-test-report-id="${reportId}"]`).should('exist');
  }

  checkTitle(title: string) {
    cy.get('sqtm-core-dialog')
      .find('sqtm-app-import-test-case')
      .contains('span.font-dialog-title', title);
  }

  chooseFormat(format: string) {
    cy.get('[data-test-field-name="format"]')
      .click();
    cy.get('nz-option-item')
      .contains('div', format)
      .click();
  }

  checkImportMessage(message: string) {
    cy.get('span')
      .contains(message)
      .should('exist');
  }

  checkDownloadTemplateLink() {
    cy.get('a[download="test-case_import_template_xls.xls"]')
      .contains('Télécharger le gabarit d\'import (.xls)');
  }

  checkFileSelectorLinkExists() {
    cy.get('sqtm-core-attachment-file-selector')
      .should('exist');
  }

  chooseEncodage(encodage: string) {
    cy.get('[data-test-field-name="encoding"]')
      .click();
    cy.get('nz-option-item')
      .contains('div', encodage)
      .click();
  }

  checkImportTestCaseDialogButtons() {
    this.assertButtonExist('simulate');
    this.assertButtonExist('import');
    this.assertButtonExist('cancel');
    // cy.get('button[data-test-dialog-button-id="simulate"]').should('exist');
    // cy.get('button[data-test-dialog-button-id="import"]').should('exist');
    // cy.get('button[data-test-dialog-button-id="cancel"]').should('exist');
  }
}

