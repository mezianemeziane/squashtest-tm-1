import {Page} from '../../page';
import {GridElement} from '../../../elements/grid/grid.element';
import {GridResponse} from '../../../../model/grids/data-row.type';

export class TestCaseViewExecutionsPage extends Page {
  constructor(rootSelector: string, public readonly grid: GridElement) {
    super(rootSelector);
  }

  static navigateTo(testCaseId: number | string, response: GridResponse): TestCaseViewExecutionsPage {
    const url = `test-case/${testCaseId}/executions`;
    const gridElement = GridElement.createGridElement('test-case-view-execution', url, response);
    return new TestCaseViewExecutionsPage('sqtm-app-executions', gridElement);
  }

  getTitle() {

  }
}
