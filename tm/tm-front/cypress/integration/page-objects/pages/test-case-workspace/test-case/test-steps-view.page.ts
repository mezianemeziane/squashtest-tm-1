import {Page} from '../../page';
import {ActionStepElement} from '../../../elements/test-steps/action-step.element';
import {CallStepElement} from '../../../elements/test-steps/call-step.element';
import {selectByDataTestComponentId} from '../../../../utils/basic-selectors';
import {EditableRichTextFieldElement} from '../../../elements/forms/editable-rich-text-field.element';
import {ParameterNameAlertDialogElement} from '../dialogs/parameter-name-alert-dialog-element';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import Chainable = Cypress.Chainable;

export class TestStepsViewPage extends Page {

  constructor() {
    super('sqtm-app-test-steps');
  }

  // check the order of steps by ids.
  checkOrder(expectedIds: number[]) {
    this.getSteps().then(stepComponents => {
      cy.wrap(stepComponents.length).should('equal', expectedIds.length);
      stepComponents.each((index, element) => {
        const stepId = element.getAttribute('data-test-test-step-id');
        cy.wrap(stepId).should('equal', expectedIds[index].toString());
      });
    });
  }

  collapseAll() {
    cy.get(`
      ${this.rootSelector}
       ${selectByDataTestComponentId('collapse-all-steps')}
    `).click();
  }

  expendAll() {
    cy.get(`
      ${this.rootSelector}
       ${selectByDataTestComponentId('expend-all-steps')}
    `).click();
  }

  private getSteps() {
    return cy.get(`
    ${this.rootSelector}
    [data-test-component-id="test-step"]
    `);
  }

  getActionStepByIndex(index: number) {
    return new ActionStepElement(null, index);
  }

  getCallStepByIndex(index: number) {
    return new CallStepElement(null, index);
  }

  checkPrerequisiteIsExpended() {
    this.getPrerequisiteToggleArrow().should('have.class', 'anticon-caret-down');
  }

  checkPrerequisiteIsCollapsed() {
    this.getPrerequisiteToggleArrow().should('have.class', 'anticon-caret-right');
  }

  assertIsEmpty() {
    return this.getSteps().should('have.length', 0);
  }

  private getPrerequisiteToggleArrow() {
    return cy.get(`
    ${this.rootSelector}
    .prerequisite
    .anticon
    `);
  }

  fillPrerequisiteFieldAndConfirm(prerequisite: string) {
    cy.get(`
    ${this.rootSelector}
    .prerequisite
    `).click().then(() => {
      const prerequisiteForm: EditableRichTextFieldElement = new EditableRichTextFieldElement('prerequisite');
      prerequisiteForm.setAndConfirmValue(prerequisite);
    });
  }

  fillPrerequisiteFieldAndCancel(prerequisite: string) {
    cy.get(`
    ${this.rootSelector}
    .prerequisite
    `).click().then(() => {
      const prerequisiteForm: EditableRichTextFieldElement = new EditableRichTextFieldElement('prerequisite');
      prerequisiteForm.setValue(prerequisite);
      prerequisiteForm.cancel();
      prerequisiteForm.checkDoesNotContainText(prerequisite);
    });
  }

  parameterNameAlertDialog(): ParameterNameAlertDialogElement {
    return new ParameterNameAlertDialogElement();
  }

  enterIntoTestCase() {
    this.getDropZone()
      .trigger('mouseenter', {force: true, buttons: 1})
      .trigger('mousemove', -20, -20, {force: true});
  }

  dropCalledTestCaseE2E(testCaseId: number, response?: any) {
    const url = `test-cases/${testCaseId}/steps/call-test-case`;
    const callMock = new HttpMockBuilder(url)
      .post()
      .responseBody(response)
      .build();

    this.getDropZone().trigger('mouseup', {force: true});
    callMock.wait();

  }

  dropCalledTestCase(testCaseId: number, response?: any, refreshNodes: DataRow[] = []) {
    const url = `test-cases/${testCaseId}/steps/call-test-case`;
    const callMock = new HttpMockBuilder(url)
      .post()
      .responseBody(response)
      .build();

    const refreshUrl = `test-case-tree/refresh`;
    const refreshTreeMock = new HttpMockBuilder<GridResponse>(refreshUrl)
      .post()
      .responseBody({dataRows: refreshNodes})
      .build();

    this.getDropZone().trigger('mouseup', {force: true});
    callMock.wait();
    refreshTreeMock.wait();
  }

  private getDropZone(): Chainable<JQuery<HTMLDivElement>> {
    return cy.get(`div.last-position-drop-zone`);
  }
}
