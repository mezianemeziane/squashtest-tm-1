import {EntityViewPage, Page} from '../../page';
import { KeywordTestStepsViewPage } from './keyword-test-step-view-page';
import {TestStepsViewPage} from './test-steps-view.page';
import {GridElement, TreeElement} from '../../../elements/grid/grid.element';
import {apiBaseUrl, HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {RemoveCoveragesDialogElement} from '../dialogs/remove-coverages-dialog.element';
import {TestCaseViewAutomationPage} from './test-case-view-automation.page';
import {TestCaseViewInformationPage} from './test-case-view-information.page';
import {TestCaseViewParametersPage} from './test-case-view-parameters.page';
import {ToolbarMenuButtonElement} from '../../../elements/workspace-common/toolbar.element';
import {CreateNewTestCaseVersionDialog} from '../dialogs/create-new-test-case-version-dialog.element';
import {TestCaseViewExecutionsPage} from './test-case-view-executions.page';
import {TestCaseViewIssuesPage} from './test-case-view-issues.page';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseViewCalledTestCasesPage} from './test-case-view-called-test-cases.page';
import {ChangeCoverageOperationReport} from '../../../../model/change-coverage-operation-report';
import {ReferentialData} from '../../../../model/referential-data.model';
import {basicReferentialData} from '../../../../utils/referential/referential-data.provider';
import {navigateToRequirementSearchForCoverage} from '../../requirement-workspace/search/search-page-utils';
import {RequirementForCoverageSearchPage} from '../../requirement-workspace/search/requirement-for-coverage-search-page';
import Chainable = Cypress.Chainable;
import {TestCaseViewScriptPage} from './test-case-view-script.page';

export class TestCaseViewPage extends EntityViewPage {
  constructor(public readonly testCaseId: number | '*') {
    super('sqtm-app-test-case-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/test-case-view/${this.testCaseId}`;
    cy.wait(`@${url}`);
  }

  clickAnchorLink<T extends Page>(linkId: TestCaseAnchorLinks, data?: any): T {
    let page: T;
    switch (linkId) {
      case 'information':
        page = this.showInformationPanel(linkId) as unknown as T;
        break;
      case 'steps':
        page = this.showTestSteps(linkId) as unknown as T;
        break;
      case 'keywordSteps':
        page = this.showKeywordTestSteps('keywordSteps') as unknown as T;
        break;
      case 'automation':
        page = this.showAutomationPanel(linkId) as unknown as T;
        break;
      case 'parameters':
        page = this.showParameterPanel(linkId) as unknown as T;
        break;
      case 'executions':
        page = this.showExecutions(data, linkId) as unknown as T;
        break;
      case 'called-test-cases':
        page = this.showCallingTestCases(data, linkId) as unknown as T;
        break;
      case 'script':
        page = this.showScript('script') as unknown as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    page.assertExist();
    return page;
  }

  private showInformationPanel<T>(linkId: 'information'): TestCaseViewInformationPage {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
    return new TestCaseViewInformationPage(this);
  }

  private showTestSteps<T>(linkId: 'steps'): TestStepsViewPage {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
    return new TestStepsViewPage();
  }

  private showKeywordTestSteps<T>(linkId: 'keywordSteps'): KeywordTestStepsViewPage {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
    return new KeywordTestStepsViewPage();
  }

  private showExecutions<T>(data: any, linkId: 'executions'): TestCaseViewExecutionsPage {
    const testCaseViewExecutionPage = TestCaseViewExecutionsPage.navigateTo(this.testCaseId, data);
    cy.get(`[anchor-link-id=${linkId}]`).click();
    testCaseViewExecutionPage.grid.waitInitialDataFetch();
    return testCaseViewExecutionPage;
  }

  private showCallingTestCases<T>(data: any, linkId: 'called-test-cases'): TestCaseViewCalledTestCasesPage {
    cy.get(`[anchor-link-id=${linkId}]`).click();
    return new TestCaseViewCalledTestCasesPage(this);
  }

  private showAutomationPanel<T>(linkId: 'automation'): TestCaseViewAutomationPage {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
    return new TestCaseViewAutomationPage(this);
  }

  private showParameterPanel<T>(linkId: 'parameters'): TestCaseViewParametersPage {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
    return new TestCaseViewParametersPage(this);
  }

  private showScript(linkId: 'script'): TestCaseViewScriptPage {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
    return new TestCaseViewScriptPage();
  }

  showIssuesIfBindedToBugTracker(modelResponse?: any, gridResponse?: GridResponse): TestCaseViewIssuesPage {
    const bugTrackerModel = new HttpMockBuilder('issues/test-case/*?frontEndErrorIsHandled=true').responseBody(modelResponse).build();
    const issues = new HttpMockBuilder('issues/test-case/*/known-issues').responseBody(gridResponse).post().build();
    cy.get(`[anchor-link-id="issues"]`).click().trigger('mouseleave');
    bugTrackerModel.wait();
    issues.wait();
    cy.removeNzTooltip();
    return new TestCaseViewIssuesPage();
  }

  showIssuesWithoutBindingToBugTracker(response?: any) {
    const httpMock = new HttpMockBuilder('issues/test-case/*?frontEndErrorIsHandled=true').responseBody(response).build();
    cy.get(`[anchor-link-id="issues"]`).click().trigger('mouseleave');
    httpMock.wait();
    cy.removeNzTooltip();
    return new TestCaseViewIssuesPage();
  }


  // Basic value check : only checks if a given string is present inside the field. This doesn't work on all fields
  // so a better alternative is to use field elements methods.
  checkData(fieldId: string, value: string) {
    cy.get(`[data-test-field-id=${fieldId}] span`).should('contain.text', value);
  }

  checkDescriptionData(fieldId: string, value: string) {
    cy.get(`[data-test-field-id=${fieldId}] > div > div p`).should('contain.text', value);
  }

  checkNameReferenceAndDescription(name: string, reference: string, description: string) {
    this.checkData('entity-name', name);
    this.checkData('entity-reference', reference);
    this.checkDescriptionData('test-case-description', description);
  }

  get coveragesTable(): GridElement {
    return new GridElement('test-case-view-coverages');
  }

  get calledTestCaseTable(): GridElement {
    return new GridElement('test-case-view-called-test-case');
  }

  get automationPanel(): TestCaseViewAutomationPage {
    return new TestCaseViewAutomationPage(this);
  }

  get informationPanel(): TestCaseViewInformationPage {
    return new TestCaseViewInformationPage(this);
  }

  get scriptPanel(): TestCaseViewScriptPage {
    return new TestCaseViewScriptPage();
  }

  showDeleteConfirmCoveragesDialog(testCaseId: number, requirementVersionIds: number[]): RemoveCoveragesDialogElement {
    cy.get(`[data-test-icon-id=remove-coverages]`).click();
    return new RemoveCoveragesDialogElement(testCaseId, requirementVersionIds);
  }

  showDeleteConfirmCoverageDialog(testCaseId: number, rowId: number) {
    const coverageTable = this.coveragesTable;
    const row = coverageTable.getRow(rowId);
    const cell = row.cell('delete');
    cell.iconRenderer().click();
    return new RemoveCoveragesDialogElement(testCaseId, [rowId]);
  }

  openRequirementDrawer(response?: any): TreeElement {
    const requirementTree = TreeElement.createTreeElement('requirement-tree-picker', 'requirement-tree', response);
    cy.get('[data-test-icon-id=add-coverages]').click();
    requirementTree.waitInitialDataFetch();
    return requirementTree;
  }

  navigateToSearchRequirementForCoverage(referentialData: ReferentialData = basicReferentialData,
                                         initialRows: GridResponse = {dataRows: []}): RequirementForCoverageSearchPage {
    return navigateToRequirementSearchForCoverage(referentialData, initialRows);
  }

  enterIntoTestCase() {
    this.getDropZone().trigger('mouseenter', {force: true, buttons: 1});
  }

  dropRequirementIntoTestCase(testCaseId: number, refreshedCoverages?: ChangeCoverageOperationReport) {
    const url = `test-cases/${testCaseId}/verified-requirements`;
    const mock = new HttpMockBuilder(url)
      .post()
      .responseBody(refreshedCoverages)
      .build();
    this.getDropZone().trigger('mouseup', {force: true});
    mock.wait();
  }

  closeRequirementDrawer() {
    cy.get('.ant-drawer-body .anticon-close').first().click();
  }

  assertAutomationBlockExist(exist: boolean) {
    const expression = exist ? 'exist' : 'not.exist';
    cy.get(`[anchor-link-id="automation"]`).should(expression);
  }

  openCreateNewVersion(): CreateNewTestCaseVersionDialog {
    const actionMenu = this.showActionMenu();
    actionMenu.assertExist();
    const menuElement = actionMenu.showMenu();
    menuElement.assertExist();
    const item = menuElement.item('create-test-case-version');
    item.assertExist();
    item.assertEnabled();
    item.click();
    return new CreateNewTestCaseVersionDialog();
  }

  private getDropZone(): Chainable<JQuery<HTMLDivElement>> {
    return cy.get(`[data-test-element-id=${TEST_CASE_DROP_ZONE_ID}]`);
  }

  private showActionMenu(): ToolbarMenuButtonElement {
    return new ToolbarMenuButtonElement('sqtm-core-entity-view-header', 'action-menu-button', 'action-menu');
  }

  assertMilestoneLockedWarningExist() {
    this.getMilestoneWarning()
      .should('exist');
  }

  private getMilestoneWarning() {
    return cy.get(this.rootSelector)
      .find(this.selectByComponentId('milestone-locked-warning'));
  }

  assertMilestoneLockedWarningNotExist() {
    this.getMilestoneWarning()
      .should('not.exist');
  }


}

export type TestCaseAnchorLinks =
  'steps'
  | 'keywordSteps'
  | 'automation'
  | 'information'
  | 'parameters'
  | 'executions'
  | 'called-test-cases'
  | 'script';
const TEST_CASE_DROP_ZONE_ID = 'test-case-view-content';
