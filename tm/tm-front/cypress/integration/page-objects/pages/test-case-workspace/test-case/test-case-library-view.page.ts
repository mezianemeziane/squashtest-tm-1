import {EntityViewPage} from '../../page';
import {FolderInformationPanelElement} from '../../../elements/panels/folder-information-panel.element';
import {TestCaseStatisticPanelElement} from '../panels/test-case-statistic-panel.element';
import {apiBaseUrl} from '../../../../utils/mocks/request-mock';

export class TestCaseLibraryViewPage extends EntityViewPage {

  constructor(private libraryId: number|'*') {
    super('sqtm-app-test-case-library-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/test-case-library-view/${this.libraryId}`;
    cy.wait(`@${url}`);
  }

  clickAnchorLink<T>(linkId: TestCaseLibraryAnchorLinks, data?: any): T {
    let element: T;
    switch (linkId) {
      case 'information':
        element = this.showInformationPanel(linkId) as unknown as T;
        break;
      case 'dashboard':
        element = this.showDashboard(linkId) as unknown as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    return element;
  }

  private showInformationPanel<T>(linkId: 'information'): FolderInformationPanelElement {
    this.clickOnAnchorLink(linkId);
    return new FolderInformationPanelElement(this.libraryId);
  }

  private clickOnAnchorLink(linkId: string) {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
  }

  private showDashboard(linkId: 'dashboard') {
    this.clickOnAnchorLink(linkId);
    return new TestCaseStatisticPanelElement();
  }

  checkNumberOfTestCases(number: number) {
    cy.get('div[data-test-component-id="footer"]> a')
      .should('have.text', number);
  }

}

export type TestCaseLibraryAnchorLinks = 'dashboard' | 'information';
