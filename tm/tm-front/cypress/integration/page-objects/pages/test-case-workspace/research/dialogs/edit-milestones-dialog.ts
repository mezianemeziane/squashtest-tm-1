import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {GridResponse} from '../../../../../model/grids/data-row.type';
import {GridElement} from '../../../../elements/grid/grid.element';

export class EditMilestonesDialog {

  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  getMilestoneGrid(): GridElement {
    return new GridElement('milestones-grid');
  }

  confirm(gridResponse?: GridResponse) {
    const httpMock = new HttpMockBuilder('search/milestones/test-case').post().build();
    const researchMock = new HttpMockBuilder('search/test-case').post().responseBody(gridResponse).build();
    this.clickButton('confirm');
    httpMock.waitResponseBody().then(() => {
      researchMock.wait();
    });
  }

  confirmForReq(gridResponse?: GridResponse, isAlreadyBound?: boolean) {
    // mock la requête pour update les liens
    const httpMock = new HttpMockBuilder('search/milestones/requirement').post().responseBody(isAlreadyBound).build();
    // mock la requête pour afficher les dataRows dans la page de recherche
    const researchMock = new HttpMockBuilder('search/requirement').post().responseBody(gridResponse).build();
    this.clickButton('confirm');
    httpMock.waitResponseBody().then(() => {
      researchMock.wait();
    });
  }

  showNoMilestoneSharedDialog() {

  }

  cancel() {
    this.clickButton('cancel');
  }

  clickButton(buttonId: string) {
    const confirmButton = cy.get(`button[data-test-dialog-button-id=${buttonId}]`);
    confirmButton.should('exist');
    confirmButton.click();
  }

  assertNoSamePerimeterMessage() {
    cy.get('span[data-test-dialog-message=no-same-perimeter]')
      .should('contain.text', 'La modification en masse des jalons porte uniquement sur le périmètre' +
        ' commun des objets sélectionnés.');

  }

  buildSelector(): string {
    return `[data-test-dialog-id=search-milestone-dialog]`;
  }
}
