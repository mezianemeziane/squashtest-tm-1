import {Page, PageFactory} from '../page';
import {GridElement, TreeElement} from '../../elements/grid/grid.element';
import {TestCaseWorkspaceTreeMenu} from './tree/test-case-workspace-tree-menu';
import {ReferentialDataProviderBuilder} from '../../../utils/referential/referential-data.provider';
import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {ReferentialData} from '../../../model/referential-data.model';
import {GridResponse} from '../../../model/grids/data-row.type';
import {TestCaseLibraryViewPage} from './test-case/test-case-library-view.page';
import {TestCaseViewPage} from './test-case/test-case-view.page';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {TestCaseSearchPage} from './research/test-case-search-page';
import {TestCaseFolderViewPage} from './test-case-folder/test-case-folder-view.page';
import {WorkspaceWithTreePage} from '../workspace-with-tree.page';

export enum TestCaseMenuItemIds {
  NEW_TEST_CASE = 'new-test-case',
  NEW_FOLDER = 'new-folder',
  IMPORT= 'import-test-case',
  ADD_TC_FROM_REQ= 'add-test-case'
}

export class TestCaseWorkspacePage extends WorkspaceWithTreePage {
  public readonly navBar: NavBarElement = new NavBarElement();
  public readonly treeMenu: TestCaseWorkspaceTreeMenu;

  public constructor(public readonly tree: TreeElement, rootSelector: string) {
    super(tree, rootSelector);
    this.treeMenu = new TestCaseWorkspaceTreeMenu();
  }

  public static initTestAtPage: PageFactory<TestCaseWorkspacePage> =
    (initialNodes: GridResponse = {dataRows: []}, referentialData?: ReferentialData) => {
      const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
      const tree = TreeElement.createTreeElement('test-case-workspace-main-tree', 'test-case-tree', initialNodes);
      // visit page
      cy.visit('test-case-workspace');
      // wait for ref data request to fire
      referentialDataProvider.wait();
      // wait for initial tree data request to fire
      tree.waitInitialDataFetch();
      return new TestCaseWorkspacePage(tree, 'sqtm-app-test-case-workspace');
    }

    selectTreeNodeByName(name: string, nodeType: string, isLeaf: boolean, contentResponse: any = null): Page {
    let page;

    switch (nodeType) {
      case 'project':
        page = new TestCaseLibraryViewPage('*');
        break;
      case 'testCase':
        page = new TestCaseViewPage('*');
        break;
      case 'folder':
        page = new TestCaseFolderViewPage('*');
        break;
      default :
        throw Error(`Unknown nodeType : ${nodeType}`);
    }

    let contentMock = null;

    // If the node being double clicked is not a leaf, it will open so we need to wait for its content
    if (! isLeaf) {
      const contentUrl = 'test-case-tree/*/content';
      contentMock = new HttpMockBuilder<GridResponse>(contentUrl).responseBody(contentResponse).build();
    }


    // cy.get('sqtm-core-test-case-tree-node > div > div ')
    //   .find('span.tree-node-name')
    //   .contains(name)
    //   .dblclick();

      cy.get('sqtm-core-tree-node-cell-renderer > div > div ')
        .find('span')
        .contains(name)
        .dblclick();


    if (contentMock !== null) {
      contentMock.wait();
    }

    page.assertExist();
    return page;
  }

  selectMultipleTreeNodesByName(names: Array<string>, nodeType: string, isLeaf: boolean, contentResponse: any = null) {

    let contentMock = null;

    // If the node being double clicked is not a leaf, it will open so we need to wait for its content
    if (!isLeaf) {
      const contentUrl = 'test-case-tree/*/content';
      contentMock = new HttpMockBuilder<GridResponse>(contentUrl).responseBody(contentResponse).build();
    }

    const firstName = names[0];
    const remainingNames = names.slice(1);

    this.selectTreeNodeByNameSimpleClick(firstName, nodeType, isLeaf);

    for (const name of remainingNames) {
      cy.get('body')
        .type('{ctrl}', {release: false})
        .get('sqtm-core-tree-node-cell-renderer > div > div ')
        .find('span')
        .contains(name)
        .click();

      if (contentMock !== null) {
        contentMock.wait();
      }
    }
  }
  selectTreeNodeByNameSimpleClick(name: string, nodeType: string, isLeaf: boolean, contentResponse: any = null): Page {
    let page;

    switch (nodeType) {
      case 'project':
        page = new TestCaseLibraryViewPage('*');
        break;
      case 'testCase':
        page = new TestCaseViewPage('*');
        break;
      case 'folder':
        page = new TestCaseFolderViewPage('*');
        break;
      default :
        throw Error(`Unknown nodeType : ${nodeType}`);
    }

    let contentMock = null;

    // If the node being double clicked is not a leaf, it will open so we need to wait for its content
    if (! isLeaf) {
      const contentUrl = 'test-case-tree/*/content';
      contentMock = new HttpMockBuilder<GridResponse>(contentUrl).responseBody(contentResponse).build();
    }


    // cy.get('sqtm-core-test-case-tree-node > div > div ')
    //   .find('span.tree-node-name')
    //   .contains(name)
    //   .click();
    cy.removeNzTooltip();
    cy.get('sqtm-core-tree-node-cell-renderer > div > div ')
      .find('span')
      .contains(name)
      .click();

    if (contentMock !== null) {
      contentMock.wait();
    }

    page.assertExist();
    // page.checkDataFetched();

    return page;
  }

  clickResearchButton(): TestCaseSearchPage {
    cy.get('i[data-test-toolbar-button-id="research-button"]')
      .click();
    const grid = GridElement.createGridElement('test-case-research', 'search/test-case');
    return new TestCaseSearchPage(grid);
  }

  clickDeleteButton(confirm: boolean) {
    cy.get('[data-test-toolbar-button-id="delete-button"]')
      .find('i')
      .click();
    if (confirm) {
      this.confirmDelete();
    } else {
      this.cancelDelete();
    }
  }

  private confirmDelete() {
    cy.get('sqtm-core-confirm-delete-dialog ' +
      'button[data-test-dialog-button-id="confirm"]')
      .should('exist')
      .click();

    cy.get('sqtm-core-confirm-delete-dialog ' +
      'button[data-test-dialog-button-id="confirm"]')
      .should('not.exist');
  }

  private cancelDelete() {
    cy.get('sqtm-core-confirm-delete-dialog ' +
      'button[data-test-dialog-button-id="cancel"]')
      .should('exist')
      .click();

    cy.get('sqtm-core-confirm-delete-dialog ' +
      'button[data-test-dialog-button-id="confirm"]')
      .should('not.exist');
  }

  clickCopyButton() {
    cy.get('[data-test-toolbar-button-id="copy-button"]')
      .find('i')
      .click();
  }

  clickPasteButton() {
    cy.get('[data-test-toolbar-button-id="paste-button"]')
      .find('i')
      .click();
  }
  pasteForSuccess() {
    const pasteUrl = `*/*/content/paste`;
    const pasteMock = new HttpMockBuilder(pasteUrl).post().build();
      cy.get('[data-test-toolbar-button-id="paste-button"]')
      .find('i')
      .click()
      .as('element');
    pasteMock.wait();
     }

}
