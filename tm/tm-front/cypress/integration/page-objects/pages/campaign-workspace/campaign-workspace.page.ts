import {ReferentialDataProviderBuilder} from '../../../utils/referential/referential-data.provider';
import {TreeElement} from '../../elements/grid/grid.element';
import {Page, PageFactory} from '../page';
import {CampaignWorkspaceTreeMenu} from './campaign-workspace-tree-menu';
import {GridResponse} from '../../../model/grids/data-row.type';
import {ReferentialData} from '../../../model/referential-data.model';
import {CreateCampaignDialog} from './dialogs/create-campaign-dialog.element';
import {CreateIterationDialog} from './dialogs/create-iteration-dialog.element';
import {HttpMock, HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {CampaignLibraryViewPage} from './campaign-library/campaign-library.page';
import {CampaignViewPage} from './campaign/campaign-view.page';
import {IterationViewPage} from './iteration/iteration-view.page';
import {WorkspaceWithTreePage} from '../workspace-with-tree.page';
import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {getDefaultCampaignFolderStatisticsBundle} from '../../../data-mock/campaign.data-mock';


export enum CampaignMenuItemIds {
  newCampaign = 'new-campaign',
  newIteration = 'new-iteration',
  newTestSuite = 'new-suite',
  newFolder = 'new-folder'
}

export class CampaignWorkspacePage extends WorkspaceWithTreePage {
  public readonly navBar: NavBarElement = new NavBarElement();
  public readonly treeMenu: CampaignWorkspaceTreeMenu;

  public constructor(public readonly tree: TreeElement) {
    super(tree, 'sqtm-app-campaign-workspace');
    this.treeMenu = new CampaignWorkspaceTreeMenu();

    this.declareStatisticsRoutes();
  }

  public static initTestAtPage: PageFactory<CampaignWorkspacePage> =
    (initialNodes: GridResponse = {dataRows: []}, referentialData?: ReferentialData) => {
      const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
      const tree = TreeElement.createTreeElement('campaign-workspace-main-tree', 'campaign-tree', initialNodes);
      cy.visit('campaign-workspace');
      referentialDataProvider.wait();
      tree.waitInitialDataFetch();
      return new CampaignWorkspacePage(tree);
    }


  selectTreeNodeByName(name: string, nodeType: string, isLeaf: boolean, contentResponse: any = null): Page {
    let page;
    let pageMock: HttpMock<any>;

    switch (nodeType) {
      case 'project':
        pageMock = new HttpMockBuilder(`campaign-library-view/*`).responseBody({}).build();
        page = new CampaignLibraryViewPage('*');
        break;
      case 'campaign':
        pageMock = new HttpMockBuilder(`campaign-view/*`).responseBody({}).build();
        page = new CampaignViewPage('*');
        break;
      case 'iteration':
        pageMock = new HttpMockBuilder(`iteration-view/*`).responseBody({}).build();
        page = new IterationViewPage('*');
        break;
      case 'folder':
        // TODO implement folder part
        break;
      default :
        throw Error(`Unknown nodeType : ${nodeType}`);
    }

    let contentMock = null;

    // If the node being double clicked is not a leaf, it will open so we need to wait for its content
    if (!isLeaf) {
      const contentUrl = 'campaign-tree/*/content';
      contentMock = new HttpMockBuilder<GridResponse>(contentUrl).responseBody(contentResponse || {}).build();
    }

    // cy.get('sqtm-core-test-case-tree-node > div > div ')
    //   .find('span.tree-node-name')
    //   .contains(name)
    //   .click();

    cy.get('sqtm-core-tree-node-cell-renderer > div > div ')
      .find('span')
      .contains(name)
      .dblclick();

    page.checkDataFetched();

    if (contentMock !== null) {
      contentMock.wait();
    }

    page.assertExist();

    return page;
  }

  mockCustomReportFolderStatisticsRequest() {
    return new HttpMockBuilder('campaign-folder-view/*/statistics').responseBody(getDefaultCampaignFolderStatisticsBundle()).build();
  }

  selectTreeNodeByNameSimpleClick(name: string, nodeType: string, isLeaf: boolean, contentResponse: any = null): Page {
    let page;
    let pageMock: HttpMock<any>;

    switch (nodeType) {
      case 'project':
        pageMock = new HttpMockBuilder(`campaign-library-view/*`).responseBody({}).build();
        page = new CampaignLibraryViewPage('*');
        break;
      case 'campaign':
        pageMock = new HttpMockBuilder(`campaign-view/*`).responseBody({}).build();
        page = new CampaignViewPage('*');
        break;
      case 'iteration':
        pageMock = new HttpMockBuilder(`iteration-view/*`).responseBody({}).build();
        page = new IterationViewPage('*');
        break;
      case 'folder':
        // TODO implement folder part
        break;
      default :
        throw Error(`Unknown nodeType : ${nodeType}`);
    }

    let contentMock = null;

    // If the node being double clicked is not a leaf, it will open so we need to wait for its content
    if (!isLeaf) {
      const contentUrl = 'campaign-tree/*/content';
      contentMock = new HttpMockBuilder<GridResponse>(contentUrl).responseBody(contentResponse || {}).build();
    }

    // cy.get('sqtm-core-test-case-tree-node > div > div ')
    //   .find('span.tree-node-name')
    //   .contains(name)
    //   .click();

    cy.get('sqtm-core-tree-node-cell-renderer > div > div ')
      .find('span')
      .contains(name)
      .dblclick();

    page.checkDataFetched();

    if (contentMock !== null) {
      contentMock.wait();
    }

    page.assertExist();

    return page;
  }

  openCreateCampaign(): CreateCampaignDialog {
    const createButton = cy.get('[data-test-toolbar-button-id="create-button"]');
    createButton.should('exist');
    createButton.click();
    cy.get('li[data-test-menu-item-id="new-campaign"]').should('exist').click();
    return new CreateCampaignDialog();
  }

  checkIfCreateIterationIsEnable(enable: boolean) {
    if (enable) {
      cy.get('li[data-test-menu-item-id="new-iteration"]').should('exist');
    } else {
      cy.get('li[data-test-menu-item-id="new-iteration"]').should('not.exist');
    }
  }

  openCreateIteration(): CreateIterationDialog {
    const createButton = cy.get('[data-test-toolbar-button-id="create-button"]');
    createButton.should('exist');
    createButton.click();
    cy.get('li[data-test-menu-item-id="new-iteration"]').should('exist').click();
    return new CreateIterationDialog();
  }

  openCreateTestSuite(): CreateIterationDialog {
    const createButton = cy.get('[data-test-toolbar-button-id="create-button"]');
    createButton.should('exist');
    createButton.click();
    cy.get('li[data-test-menu-item-id="new-suite"]').should('exist').click();
    return new CreateIterationDialog();
  }

  // Prevents missing stub errors. If you need to actually use these routes in your
  // tests, just redeclare them after the CampaignWorkspacePage was instantiated.
  private declareStatisticsRoutes(): void {
    [
      'campaign-view/*/statistics',
      'iteration-view/*/statistics',
    ].forEach((url) => new HttpMockBuilder(url).build());
  }
}
