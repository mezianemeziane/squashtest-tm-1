import {CreateEntityDialog} from '../../create-entity-dialog.element';
import {ProjectData} from '../../../../model/project/project-data.model';
import {BindableEntity} from '../../../../model/bindable-entity.model';


export class CreateCampaignFolderDialog extends CreateEntityDialog {

  constructor(project?: ProjectData, domain?: BindableEntity) {
    super({
      treePath: 'campaign-tree',
      viewPath: 'campaign-folder-view',
      newEntityPath: 'new-folder'
    }, project, domain);
  }
}
