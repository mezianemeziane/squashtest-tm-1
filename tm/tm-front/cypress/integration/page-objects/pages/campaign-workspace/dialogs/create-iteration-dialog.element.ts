import {CreateEntityDialog} from '../../create-entity-dialog.element';
import {ProjectData} from '../../../../model/project/project-data.model';
import {BindableEntity} from '../../../../model/bindable-entity.model';


export class CreateIterationDialog extends CreateEntityDialog {

  constructor(project?: ProjectData, domain?: BindableEntity) {
    super({
      treePath: 'campaign-tree',
      viewPath: 'iteration-view',
      newEntityPath: 'campaign/*/new-iteration'
    }, project, domain);
  }
}

