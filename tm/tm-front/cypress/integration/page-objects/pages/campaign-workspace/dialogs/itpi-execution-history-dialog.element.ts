import {GridElement} from '../../../elements/grid/grid.element';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';

export  class ExecutionHistoryDialog {
  public gridElement: GridElement;

  constructor(gridResponse: GridResponse) {
    this.gridElement = GridElement.createGridElement('iteration-test-plan-execution-history',
      `iteration/*/test-plan/*/executions`, gridResponse);
  }

  checkFirstIdInExecutionHistoryDialog(executionOrder: number, excutionId: number) {
    cy.get(`[data-test-cell-id='executionOrder']`)
      .find('a:first-child')
      .should('exist')
      .should('have.attr', 'href')
      .and('contain', excutionId);
  }

  deleteSelectedExecution(executionId: number, response?: any) {
    const selectedRow = this.gridElement.getRow(executionId);
    selectedRow.cell('delete').iconRenderer().click();

    const mockDeleteRequest = new HttpMockBuilder<{ nbIssues: number }>(`iteration/*/test-plan/execution/*`)
      .delete().responseBody({nbIssues: 0}).build();
    const mockRefreshResponse = new HttpMockBuilder(`iteration/*/test-plan/*/executions`).responseBody(response).post().build();

    cy.get(`[data-test-dialog-button-id="confirm"]`).click();

    mockDeleteRequest.waitResponseBody();
    mockRefreshResponse.waitResponseBody();
  }

  deleteMultiSelectedExecution(executionIds: number[], reponse?: any) {
    cy.get(`[data-test-button-id="show-confirm-mass-delete-execution-dialog"]>span`).should('have.class', 'label-color');
    this.gridElement.selectRows(executionIds, '#', 'leftViewport');
    cy.get(`[data-test-button-id="show-confirm-mass-delete-execution-dialog"]>span`).should('not.have.class', 'label-color');

    cy.get(`[data-test-button-id="show-confirm-mass-delete-execution-dialog"]`).click();

    const mockDeleteRequest = new HttpMockBuilder<{ nbIssues: number }>(`iteration/*/test-plan/execution/*`)
      .delete().responseBody({ nbIssues: 0 }).build();
    const mockRefreshResponse = new HttpMockBuilder(`iteration/*/test-plan/*/executions`).responseBody(reponse).post().build();

    cy.get(`[data-test-dialog-button-id="confirm"]`).click();

    mockDeleteRequest.waitResponseBody();
    mockRefreshResponse.waitResponseBody();
  }
}
