import {GroupedMultiListFieldElement} from '../../../elements/forms/grouped-multi-list-field.element';
import {SelectFieldElement} from '../../../elements/forms/select-field.element';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {DataRow} from '../../../../model/grids/data-row.type';
import {CommonSelectors} from '../../../elements/forms/abstract-form-field.element';

export class ItpiMultiEditDialogElement {
  public readonly dialogId = 'itpi-multi-edit';

  public readonly testSuiteMultiList: GroupedMultiListFieldElement;
  public readonly assigneeSelect: SelectFieldElement;
  public readonly statusSelect: SelectFieldElement;

  constructor() {
    this.testSuiteMultiList = new GroupedMultiListFieldElement('[data-test-component-id="test-suites"]');
    this.assigneeSelect = new SelectFieldElement(CommonSelectors.fieldName('assignee'));
    this.statusSelect = new SelectFieldElement(CommonSelectors.fieldName('status'));
  }

  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  confirm(gridRefreshResponse: DataRow[]): void {
    const updateMock = new HttpMockBuilder('iteration/test-plan/*/mass-update').post().build();
    const testPlanMock = new HttpMockBuilder('iteration/*/test-plan')
      .post().responseBody({dataRows: gridRefreshResponse})
      .build();

    const refreshTreeMock = new HttpMockBuilder('campaign-tree/refresh')
      .post()
      .responseBody({dataRows: []})
      .build();

    this.clickOnConfirmButton();

    updateMock.wait();
    testPlanMock.wait();
    refreshTreeMock.wait();
  }

  toggleTestSuiteEdition(): void {
    this.clickLabel('Suites de test');
  }

  selectTestSuites(testSuiteNames: string[]): void {
    this.testSuiteMultiList.showMultiList();
    this.testSuiteMultiList.multiList.assertExist();
    testSuiteNames.forEach(s => this.testSuiteMultiList.multiList.toggleOneItem(s));
    this.testSuiteMultiList.multiList.close();
  }

  toggleAssigneeEdition(): void {
    this.clickLabel('Assignation');
  }

  selectAssignee(assignee: string): void {
    this.assigneeSelect.selectValue(assignee);
  }

  toggleStatusEdition(): void {
    this.clickLabel('Statut');
  }

  selectStatus(status: string): void {
    this.statusSelect.selectValue(status);
  }

  private clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
    cy.get(this.buildSelector()).should('not.exist');
  }

  private clickLabel(labelContent: string): void {
    cy.get(this.buildSelector())
      .contains('span', labelContent)
      .click();
  }

  private buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }
}
