import {Page} from '../page';
import {selectByDataTestComponentId} from '../../../utils/basic-selectors';
import {NgZorroTable} from '../../elements/ngzorro-table/NgZorroTable';
import {ExecutionStatusCount} from '../../../model/campaign/campaign-model';
import {TestCaseImportanceKeys} from '../../../model/level-enums/level-enum';

export class CampaignWorkspaceDashboardPage extends Page {

  inventoryTable: NgZorroTable = new NgZorroTable('inventory-table');

  constructor(rootSelector: string) {
    super(rootSelector);
  }

  protected assertChartIsRendered(selector: string) {
    cy.get(this.rootSelector)
      .find(selector)
      .should('exist')
      .find('.plotly')
      .should('exist');
  }

  assertStatsChartAreRendered() {
    this.assertChartIsRendered(selectByDataTestComponentId('status-chart'));
    this.assertChartIsRendered(selectByDataTestComponentId('conclusiveness-chart'));
    this.assertChartIsRendered(selectByDataTestComponentId('importance-chart'));
  }

  assertErrorMessageOnEmptyTestPlanIsVisible() {
    cy.get(this.rootSelector)
      .find(this.selectByComponentId('empty-test-plan-error'))
      .should('be.visible');
  }

  checkStatusCount(rowIndex: number, expectedCounts: Partial<ExecutionStatusCount>) {
    Object.entries<number>(expectedCounts).forEach(([id, count]) => {
      this.inventoryTable.findRow(rowIndex).assertExist().getCell(id).assertContainsText(count.toString());
    });
  }

  checkImportanceCount(rowIndex: number, expectedCounts: Partial<{[K in TestCaseImportanceKeys]: number}>) {
    Object.entries<number>(expectedCounts).forEach(([id, count]) => {
      this.inventoryTable.findRow(rowIndex).assertExist().getCell(id).assertContainsText(count.toString());
    });
  }

  assertAdvancementChartIsRendered() {
    this.assertChartIsRendered('sqtm-app-campaign-advancement-chart');

    cy.get(this.rootSelector)
      .find(this.selectByComponentId('iteration-scheduled-dates-error'))
      .should('not.exist');
  }

  assertErrorMessageOnDateIsVisible() {
    cy.get(this.rootSelector)
      .should('exist')
      .find('sqtm-app-campaign-advancement-chart')
      .should('not.exist');

    cy.get(this.rootSelector)
      .find(this.selectByComponentId('iteration-scheduled-dates-error'))
      .should('be.visible');
  }
}
