import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {EditIterationsScheduledDatesDialog} from '../dialogs/edit-iterations-scheduled-dates-dialog';
import {IterationPlanning} from '../../../../model/campaign/campaign-model';
import {CampaignWorkspaceDashboardPage} from '../campaign-workspace-dashboard.page';


export class CampaignViewDashboardPage extends CampaignWorkspaceDashboardPage {

  constructor(private campaignId: string | number = '*') {
    super('sqtm-app-campaign-view-dashboard');
  }

  openIterationScheduledDatesDialog(scheduledIterations: IterationPlanning[] = []) {
    const mock = new HttpMockBuilder(`campaign/${this.campaignId}/iterations`).responseBody(scheduledIterations).build();
    cy.get(this.rootSelector)
      .find(this.selectByComponentId('iteration-scheduled-dates-button'))
      .click();
    mock.wait();
    const dialog = new EditIterationsScheduledDatesDialog(this.campaignId);
    dialog.assertIsOpened();
    return dialog;
  }

  assertInventoryTableExist() {
    cy.get(this.rootSelector)
      .find('sqtm-app-campaign-inventory')
      .should('exist');

    this.inventoryTable.assertExist();
  }

  assertIterationRowHasName(index: number, expectedName: string) {
    this.inventoryTable.findRow(index).assertExist().getCell(0).assertContainsText(expectedName);
  }

  checkTotalCount(rowIndex: number, expected: number) {
    this.inventoryTable.findRow(rowIndex).assertExist().getCell('total-count').assertContainsText(expected.toString());
  }

  checkExecutionRate(rowIndex: number, expected: number) {
    this.inventoryTable.findRow(rowIndex).assertExist().getCell('execution-rate').assertContainsText(`${expected} %`);
  }

  assertDateButtonIsVisible() {
    cy.get(this.rootSelector)
      .find(this.selectByComponentId('iteration-scheduled-dates-button'))
      .should('exist');
  }
}
