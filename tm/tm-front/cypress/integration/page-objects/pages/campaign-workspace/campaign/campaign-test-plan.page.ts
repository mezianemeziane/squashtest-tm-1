import {Page} from '../../page';
import {GridElement, TreeElement} from '../../../elements/grid/grid.element';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {VerifyItemTestPlanOptions} from '../iteration/iteration-test-plan.page';
import {RemoveCtpiDialogElement} from '../dialogs/remove-ctpi-dialog.element';
import {MassDeleteCtpiDialogElement} from '../dialogs/mass-delete-ctpi-dialog.element';
import {CtpiMultiEditDialogElement} from '../dialogs/ctpi-multi-edit-dialog.element';
import Chainable = Cypress.Chainable;

export class CampaignTestPlanPage extends Page {
  private constructor(rootSelector: string, public readonly testPlan: GridElement) {
    super(rootSelector);
  }

  static navigateTo(campaignId: number | string, testPlan: GridResponse): CampaignTestPlanPage {
    const url = `campaign/${campaignId}/test-plan`;
    const gridElement = GridElement.createGridElement('campaign-test-plan', url, testPlan);
    return new CampaignTestPlanPage('sqtm-app-campaign-test-plan-execution', gridElement);
  }

  openTestCaseDrawer(response?: GridResponse): TreeElement {
    const treeElement = TreeElement.createTreeElement('test-case-tree-picker', 'test-case-tree', response);
    cy.get('[data-test-button-id=show-test-case-picker]').click();
    treeElement.waitInitialDataFetch();
    return treeElement;
  }

  enterIntoTestPlan() {
    this.getDropZone()
      .trigger('mouseenter', 'left', {force: true, buttons: 1});
  }

  private getDropZone(): Chainable<JQuery<HTMLDivElement>> {
    return cy.get(`[data-test-element-id=${CAMPAIGN_TEST_PLAN_DROP_ZONE_ID}]`);
  }

  assertColoredBorderIsVisible() {
    this.getDropZone().should('have.class', 'drop-test-case');
  }

  dropIntoTestPlan(campaignId: number | string, createdTestPlanItemIds?: number[], refreshedTestPlan?: GridResponse): Chainable<number[]> {
    const url = `campaign/${campaignId}/test-plan-items`;
    const mock = new HttpMockBuilder<{ itemTestPlanIds: number[] }>(url)
      .post()
      .responseBody({itemTestPlanIds: createdTestPlanItemIds})
      .build();
    this.testPlan.declareRefreshData(refreshedTestPlan);
    this.getDropZone().trigger('mouseup', {force: true});
    return mock.waitResponseBody().then(responseBody => {
      this.testPlan.waitForRefresh();
      return cy.wrap(responseBody.itemTestPlanIds);
    });
  }

  verifyTestPlanItem(options: VerifyItemTestPlanOptions) {
    this.testPlan.assertRowExist(options.id);
    const row = this.testPlan.getRow(options.id);
    if (options.name) {
      if (options.showsAsLink) {
        row.cell('testCaseName').linkRenderer().assertContainText(options.name);
      } else {
        row.cell('testCaseName').textRenderer().assertContainText(options.name);
      }
    }
  }

  assertColoredBorderIsNotVisible() {
    this.getDropZone().should('not.have.class', 'drop-test-case');
  }

  closeTestCaseDrawer() {
    cy.get('.ant-drawer-body .anticon-close').click();
  }

  moveItemWithServerResponse(itemToMove: string, itemToDropOnto: string, res: any): void {
    const mock = new HttpMockBuilder('campaign/*/test-plan/*/position/*')
      .responseBody(res)
      .post().build();

    this.startDrag(itemToMove);
    this.getDraggedContent().should('be.visible')
      .find('[data-test-component-id="cannot-drag-icon"]')
      .should('not.exist');
    this.internalDropOnto(itemToDropOnto);

    mock.wait();
  }

  private startDrag(itemToDrag: string): void {
    this.testPlan.getRow(itemToDrag, 'leftViewport').cell('#').findCell()
      .find('span')
      .trigger('mousedown', {button: 0})
      .trigger('mousemove', -20, -20, {force: true})
      .trigger('mousemove', 50, 50, {force: true});
  }

  private getDraggedContent() {
    return cy.get('sqtm-app-campaign-test-plan-dragged-content');
  }

  private internalDropOnto(itemToDropOnto: string): void {
    this.testPlan.getRow(itemToDropOnto, 'leftViewport').cell('#').findCell()
      .trigger('mousemove', 'bottom', {force: true})
      .trigger('mouseup', {force: true});
  }

  assertCannotMoveItem(itemToMove: string): void {
    this.startDrag(itemToMove);
    this.getDraggedContent()
      .find('[data-test-component-id="cannot-drag-icon"]')
      .should('exist');
    this.internalDropOnto(itemToMove);
  }

  showDeleteConfirmDialog(campaignId: number, testPlanId: number) {
    cy.get(`[data-test-row-id=${testPlanId}] [data-test-cell-id="delete"] i`).click();
    return new RemoveCtpiDialogElement(testPlanId, campaignId);
  }

  showMassDeleteConfirmDialog(campaignId: number, testPlanIds: number[]) {
    cy.get('[data-test-button-id=show-confirm-mass-delete-dialog]').click();
    return new MassDeleteCtpiDialogElement(campaignId, testPlanIds);
  }

  openMassEditDialog(): CtpiMultiEditDialogElement {
    cy.get('[data-test-button-id=mass-edit]').click();
    return new CtpiMultiEditDialogElement();
  }

  changeDataset(itemId: string) {
    cy.get(`[data-test-cell-id=dataset-cell]`).click();
    const updatedDatasetMock = new HttpMockBuilder('test-plan-item/*/dataset-to-ctpi').post().build();
    cy.get(`[data-test-item-id=${itemId}]`).click();
    updatedDatasetMock.wait();
  }
}

const CAMPAIGN_TEST_PLAN_DROP_ZONE_ID = 'campaignTestPlanDropZone';
