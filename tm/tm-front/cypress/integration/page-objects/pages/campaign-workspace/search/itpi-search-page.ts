import {Page} from '../../page';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {ReferentialData} from '../../../../model/referential-data.model';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder
} from '../../../../utils/referential/referential-data.provider';
import {GridElement} from '../../../elements/grid/grid.element';
import {buildItpiSearchGrid, buildItpiSearchPageMock} from './search-page-utils';
import {ItpiSearchModel} from './itpi-search-model';

export class ItpiSearchPage extends Page {

  constructor(public grid: GridElement) {
    super('sqtm-app-itpi-search-page');
  }

  public static initTestAtPage(
    referentialData: ReferentialData = basicReferentialData,
    initialRows: GridResponse = {dataRows: []},
    itpiSearchModel: ItpiSearchModel = {
      usersExecutedItpi: [],
      usersAssignedTo: []
    },
    queryString: string = ''): ItpiSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = buildItpiSearchPageMock(itpiSearchModel);
    const grid = buildItpiSearchGrid(initialRows);
    // visit page
    cy.visit(`search/campaign?${queryString}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    pageDataMock.wait();
    // wait for initial tree data request to fire
    return new ItpiSearchPage(grid);
  }


  foldFilterPanel() {
    cy.get(this.selectByComponentId('fold-filter-panel-button')).click();
  }

  buildToolBarSelector(): string {
    return '[data-test-toolbar-id=campaign-search-toolbar]';
  }

}
