import {EntityViewPage, Page} from '../../page';
import {apiBaseUrl} from '../../../../utils/mocks/request-mock';

export class CampaignLibraryViewPage extends EntityViewPage {

  constructor(private campaignLibraryId: number | string) {
    super('sqtm-app-campaign-library-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/campaign-library-view/${this.campaignLibraryId}`;
    cy.wait(`@${url}`);
  }

  clickAnchorLink<T extends Page>(linkId: string, data?: any): T {
    throw Error('Implement me when needed !');
  }

}
