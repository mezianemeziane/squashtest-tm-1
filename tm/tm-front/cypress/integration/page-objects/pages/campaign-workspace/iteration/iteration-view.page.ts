import {EntityViewPage} from '../../page';
import {IterationTestPlanPage} from './iteration-test-plan.page';
import {EditableRichTextFieldElement} from '../../../elements/forms/editable-rich-text-field.element';
import {apiBaseUrl, HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {IterationViewInformationPage} from './iteration-view-information.page';
import {IterationViewStatisticsPage} from './iteration-view-statistics.page';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {IterationViewIssuesPage} from './iteration-view-issues.page';
import {IterationViewDashboardPage} from './iteration-view-dashboard.page';
import {IterationAutomatedSuitePage} from './iteration-automated-suite.page';

export class IterationViewPage extends EntityViewPage {

  constructor(public iterationId: number | string) {
    super('sqtm-app-iteration-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/iteration-view/${this.iterationId}`;
    cy.wait(`@${url}`);
  }

  clickAnchorLink<T extends IterationTestPlanPage>(linkId: IterationAnchorLinks, data?: any): T {
    let page: T;
    switch (linkId) {
      case 'dashboard':
        page = this.showDashboardPanel(data) as unknown as T;
        break;
      case 'plan-exec':
        page = this.showTestPlan(data, linkId) as T;
        break;
      case 'statistics':
        page = this.showStatisticsPanel(linkId) as unknown as T;
        break;
      case 'information':
        page = this.showInformationPanel(linkId) as unknown as T;
        break;
      case 'automated-suite':
        page = this.showAutomatedSuitePage(data, linkId) as unknown as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    page.assertExist();
    return page;
  }


  private showTestPlan<T>(data: any, linkId: 'plan-exec'): IterationTestPlanPage {
    const iterationTestPlanPage = IterationTestPlanPage.navigateTo(this.iterationId, data);
    cy.get(`[anchor-link-id=${linkId}]`).click();
    iterationTestPlanPage.testPlan.waitInitialDataFetch();
    return iterationTestPlanPage;
  }

  private showAutomatedSuitePage<T>(data: any, linkId: 'automated-suite'): IterationAutomatedSuitePage {
    const iterationAutomatedSuite = IterationAutomatedSuitePage.navigateTo(this.iterationId, data);
    cy.get(`[anchor-link-id=${linkId}]`).click();
    iterationAutomatedSuite.testPlan.waitInitialDataFetch();
    return iterationAutomatedSuite;
  }

  get descriptionRichField(): EditableRichTextFieldElement {
    return new EditableRichTextFieldElement('iteration-description');
  }

  checkDescription(value: string) {
    this.descriptionRichField.checkTextContent(value);
  }

  checkData(fieldId: string, value: string) {
    cy.get(`[data-test-field-id=${fieldId}] span`).should('contain.text', value);
  }

  get informationPanel(): IterationViewInformationPage {
    return new IterationViewInformationPage(this);
  }

  public clickOnAnchorLink(linkId: string) {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
  }

  public showDashboardPanel(model?): IterationViewDashboardPage {
    this.clickOnAnchorLink('dashboard');
    return new IterationViewDashboardPage(this);
  }

  public showStatisticsPanel(linkId: 'statistics'): IterationViewStatisticsPage {
    this.clickOnAnchorLink(linkId);
    return new IterationViewStatisticsPage(this);
  }

  public showInformationPanel(linkId: 'information'): IterationViewInformationPage {
    this.clickOnAnchorLink(linkId);
    return new IterationViewInformationPage(this);
  }

  showIssuesWithoutBindingToBugTracker(response?: any): IterationViewIssuesPage {
    const httpMock = new HttpMockBuilder('issues/iteration/*?frontEndErrorIsHandled=true').responseBody(response).build();
    cy.get(`[anchor-link-id="issues"]`).click().trigger('mouseleave');
    httpMock.wait();
    cy.removeNzTooltip();
    return new IterationViewIssuesPage();
  }

  showIssuesIfBindedToBugTracker(modelResponse?: any, gridResponse?: GridResponse): IterationViewIssuesPage {
    const bugTrackerModel = new HttpMockBuilder('issues/iteration/*?frontEndErrorIsHandled=true')
      .responseBody(modelResponse).build();
    const issues = new HttpMockBuilder('issues/iteration/*/known-issues').responseBody(gridResponse).post().build();
    cy.get(`[anchor-link-id="issues"]`).click().trigger('mouseleave');
    bugTrackerModel.wait();
    issues.wait();
    cy.removeNzTooltip();
    return new IterationViewIssuesPage();
  }
}

export type IterationAnchorLinks =
  'plan-exec'
  | 'statistics'
  | 'information'
  | 'planning'
  | 'dashboard'
  | 'automated-suite';
