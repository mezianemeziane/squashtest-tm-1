import {Page} from '../../page';
import {IterationViewPage} from './iteration-view.page';
import {EditableTextFieldElement} from '../../../elements/forms/editable-text-field.element';
import {EditableSelectFieldElement} from '../../../elements/forms/editable-select-field.element';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {EditableRichTextFieldElement} from '../../../elements/forms/editable-rich-text-field.element';


export class IterationViewInformationPage extends Page {
  constructor(private parentPage: IterationViewPage) {
    super('sqtm-app-iteration-information-panel');
  }

  get iterationId(): number | string {
    return this.parentPage.iterationId;
  }

  get nameTextField(): EditableTextFieldElement {
    const url = `iteration/${this.iterationId}/name`;
    return new EditableTextFieldElement('entity-name', url);
  }

  get referenceTextField(): EditableTextFieldElement {
    const url = `iteration/${this.iterationId}/reference`;
    return new EditableTextFieldElement('entity-reference', url);
  }

  get statusSelectField(): EditableSelectFieldElement {
    const url = `iteration/${this.iterationId}/iteration-status`;
    return new EditableSelectFieldElement('iteration-status', url);
  }

  get descriptionRichField(): EditableRichTextFieldElement {
    const url = `iteration/${this.iterationId}/description`;
    return new EditableRichTextFieldElement('iteration-description', url);
  }

  rename(newValue: string, refreshedRows?: GridResponse) {
    const refreshTreeMock = this.getRefreshTreeMock(refreshedRows);
    this.nameTextField.setAndConfirmValue(newValue);
    refreshTreeMock.wait();
  }

  changeReference(newValue: string, refreshedRows?: GridResponse) {
    const refreshTreeMock = this.getRefreshTreeMock(refreshedRows);
    this.nameTextField.setAndConfirmValue(newValue);
    refreshTreeMock.wait();
  }

  private getRefreshTreeMock(refreshedRows: GridResponse) {
    return new HttpMockBuilder(`campaign-tree/refresh`)
      .post()
      .responseBody(refreshedRows)
      .build();
  }
}
