import {IterationViewPage} from './iteration-view.page';
import {CampaignWorkspaceDashboardPage} from '../campaign-workspace-dashboard.page';


export class IterationViewDashboardPage extends CampaignWorkspaceDashboardPage {
  constructor(private parentPage: IterationViewPage) {
    super('sqtm-app-iteration-view-dashboard');
  }

  get iterationId() {
    return this.parentPage.iterationId;
  }

  assertInventoryTableExist() {
    cy.get(this.rootSelector)
      .find('sqtm-app-iteration-inventory')
      .should('exist');

    this.inventoryTable.assertExist();
  }

  assertTestSuiteRowHasName(index: number, expectedName: string) {
    this.inventoryTable.findRow(index).assertExist().getCell(0).assertContainsText(expectedName);
  }

  checkCell(rowIndex: number, cellId: string, expectedContent: string) {
    this.inventoryTable.findRow(rowIndex).assertExist().getCell(cellId).assertContainsText(expectedContent);
  }
}
