import {EntityViewPage} from '../../page';
import {EditableRichTextFieldElement} from '../../../elements/forms/editable-rich-text-field.element';
import {apiBaseUrl, HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {TestSuiteViewIssuesPage} from './test-suite-view-issues.page';
import {EditableTextFieldElement} from '../../../elements/forms/editable-text-field.element';
import {TestSuiteTestPlanPage} from './test-suite-test-plan.page';

export class TestSuiteViewPage extends EntityViewPage {

  constructor(public suiteId: number | string) {
    super('sqtm-app-test-suite-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/test-suite-view/${this.suiteId}`;
    cy.wait(`@${url}`);
  }

  get descriptionRichField(): EditableRichTextFieldElement {
    return new EditableRichTextFieldElement('test-suite-description');
  }

  checkDescription(value: string) {
    this.descriptionRichField.checkTextContent(value);
  }

  checkData(fieldId: string, value: string) {
    cy.get(`[data-test-field-id=${fieldId}] span`).should('contain.text', value);
  }

  private clickOnAnchorLink(linkId: string) {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
  }

  clickAnchorLink<T extends TestSuiteTestPlanPage>(linkId: TestSuiteAnchorLinks, data?: any): T {
    let page: T;
    switch (linkId) {
      case 'plan-exec':
        page = this.showTestPlan(data, linkId) as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    page.assertExist();
    return page;
  }

  showIssuesWithoutBindingToBugTracker(response?: any): TestSuiteViewIssuesPage {
    const httpMock = new HttpMockBuilder('issues/test-suite/*?frontEndErrorIsHandled=true').responseBody(response).build();
    cy.get(`[anchor-link-id="issues"]`).click().trigger('mouseleave');
    httpMock.wait();
    cy.removeNzTooltip();
    return new TestSuiteViewIssuesPage();
  }

  showIssuesIfBindedToBugTracker(modelResponse?: any, gridResponse?: GridResponse): TestSuiteViewIssuesPage {
    const bugTrackerModel = new HttpMockBuilder('issues/test-suite/*?frontEndErrorIsHandled=true')
      .responseBody(modelResponse).build();
    const issues = new HttpMockBuilder('issues/test-suite/*/known-issues').responseBody(gridResponse).post().build();
    cy.get(`[anchor-link-id="issues"]`).click().trigger('mouseleave');
    bugTrackerModel.wait();
    issues.wait();
    cy.removeNzTooltip();
    return new TestSuiteViewIssuesPage();
  }

  rename(newValue: string, refreshedRows?: GridResponse) {
    const refreshTreeMock = this.getRefreshTreeMock(refreshedRows);
    this.nameTextField.setAndConfirmValue(newValue);
    refreshTreeMock.wait();
  }

  get nameTextField(): EditableTextFieldElement {
    const url = `test-suite/${this.suiteId}/name`;
    return new EditableTextFieldElement('entity-name', url);
  }

  private getRefreshTreeMock(refreshedRows: GridResponse) {
    return new HttpMockBuilder(`campaign-tree/refresh`)
      .post()
      .responseBody(refreshedRows)
      .build();
  }

  private showTestPlan<T>(data: any, linkId: 'plan-exec'): TestSuiteTestPlanPage {
    const testSuiteTestPlanPage = TestSuiteTestPlanPage.navigateTo(this.suiteId, data);
    cy.get(`[anchor-link-id=${linkId}]`).click();
    testSuiteTestPlanPage.testPlan.waitInitialDataFetch();
    return testSuiteTestPlanPage;
  }
}

export type TestSuiteAnchorLinks = 'plan-exec' | 'statistics' | 'information';
