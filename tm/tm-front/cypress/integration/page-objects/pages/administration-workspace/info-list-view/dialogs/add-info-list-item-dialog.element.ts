import {TextFieldElement} from '../../../../elements/forms/TextFieldElement';
import {AbstractCreationDialogElement} from '../../../../elements/dialog/creation-dialog.element';
import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {IconFieldElement} from '../../../../elements/forms/icon-field-element';
import {AdminInfoList} from '../../../../../model/infolist/adminInfoList.model';

export class AddInfoListItemDialogElement extends AbstractCreationDialogElement {
  public readonly nameField: TextFieldElement;
  public readonly codeField: TextFieldElement;
  public readonly iconField: IconFieldElement;

  constructor() {
    super('add-info-list-item');
    this.nameField = new TextFieldElement('label');
    this.codeField = new TextFieldElement('code');
    this.iconField = new IconFieldElement('icon');
    // this.colourField = new TextFieldElement('code');
  }

  addItem(response: AdminInfoList) {
    const mock = new HttpMockBuilder('info-lists/*/items/new')
      .post().responseBody(response).build();

    this.clickOnAddButton();
    mock.wait();
  }
}

