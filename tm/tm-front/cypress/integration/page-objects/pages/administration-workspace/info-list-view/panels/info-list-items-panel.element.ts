import {GridElement} from '../../../../elements/grid/grid.element';
import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {CustomField} from '../../../../../model/customfield/customfield.model';
import {AddInfoListItemDialogElement} from '../dialogs/add-info-list-item-dialog.element';

export class InfoListItemsPanelElement {
  public readonly grid: GridElement;

  constructor() {
    this.grid = GridElement.createGridElement('infoListItems');
  }

  assertDeleteIconIsVisible(itemLabel: string): void {
    this.grid.findRowId('label', itemLabel)
      .then(id => {
        this.grid.getRow(id).cell('delete').iconRenderer().assertIsVisible();
      });
  }

  assertDeleteIconIsHidden(itemLabel: string): void {
    this.grid.findRowId('label', itemLabel)
      .then(id => this.grid.getRow(id).cell('delete').iconRenderer().assertNotExist());
  }

  setDefaultItem(itemLabel: string): void {
    const mock = new HttpMockBuilder('info-list-items/*/default').post().build();

    this.grid.findRowId('label', itemLabel)
      .then(id => {
        this.grid.getRow(id).cell('isDefault').radioRenderer().findRadio().click();
        mock.wait();
      });
  }

  moveItem(itemToMove: string, itemToDropOnto: string, response: CustomField): void {
    const mock = new HttpMockBuilder('info-lists/*/items/positions')
      .responseBody(response)
      .post().build();

    this.grid.getRow(itemToMove).cell('#').findCell()
      .trigger('mousedown', {button: 0})
      .trigger('mousemove', -20, -20, {force: true})
      .trigger('mousemove', 50, 50, {force: true});

    this.grid.getRow(itemToDropOnto).cell('#').findCell()
      .trigger('mousemove', 'bottom', {force: true})
      .trigger('mouseup', {force: true});

    mock.wait();
  }

  openAddItemDialog(): AddInfoListItemDialogElement {
    const dialog = new AddInfoListItemDialogElement();
    this.clickAddButton();
    return dialog;
  }

  clickAddButton(): void {
    cy.get('[data-test-button-id="add-item"]').click();
  }
}
