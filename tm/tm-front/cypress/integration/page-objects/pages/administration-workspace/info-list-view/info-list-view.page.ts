import {Page} from '../../page';
import {EditableTextFieldElement} from '../../../elements/forms/editable-text-field.element';
import {InfoListItemsPanelElement} from './panels/info-list-items-panel.element';
import {InfoListInformationPanelElement} from './panels/info-list-information-panel.element';

export class InfoListViewPage extends Page {

  readonly entityNameField: EditableTextFieldElement;
  readonly informationPanel = new InfoListInformationPanelElement();
  readonly itemsPanel = new InfoListItemsPanelElement();

  constructor() {
    super('sqtm-app-info-list-view');

    this.entityNameField = new EditableTextFieldElement('entity-name', 'info-lists/*/label');
  }

  waitInitialDataFetch() {
  }

  foldGrid() {
    cy.get(`[data-test-component-id="fold-tree-button"]`).click();
  }
}
