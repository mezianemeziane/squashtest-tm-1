import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {GridElement} from '../../elements/grid/grid.element';
import {AdministrationWorkspacePage} from './administration-workspace.page';
import {Page, PageFactory} from '../page';
import {GridResponse} from '../../../model/grids/data-row.type';
import {ReferentialData} from '../../../model/referential-data.model';
import {CreateCustomFieldDialog} from './dialogs/create-custom-field-dialog';
import {AdminWorkspaceInfoListsPage} from './admin-workspace-info-lists.page';
import {AdminWorkspaceRequirementsLinksPage} from './admin-workspace-requirements-links.page';
import {AdminReferentialDataProviderBuilder} from '../../../utils/referential/admin-referential-data.provider';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {CustomField} from '../../../model/customfield/customfield.model';
import {CustomFieldViewPage} from './custom-field-view/custom-field-view.page';

export class AdminWorkspaceCustomFieldsPage extends AdministrationWorkspacePage {

  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-main-custom-workspace');
  }

  public static initTestAtPageCustomFields: PageFactory<AdminWorkspaceCustomFieldsPage> =
    (initialNodes: GridResponse = {dataRows: []}, referentialData?: ReferentialData) => {
      return AdminWorkspaceCustomFieldsPage.initTestAtPage(initialNodes, 'customFields',
        'custom-fields', 'entities-customization/custom-fields', referentialData);
    }

  public static initTestAtPage: PageFactory<AdminWorkspaceCustomFieldsPage> =
    (initialNodes: GridResponse = {dataRows: []}, gridId: string, gridUrl: string, pageUrl: string,
     referentialData?: ReferentialData) => {
      const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(referentialData).build();
      const gridElement = GridElement.createGridElement(gridId, gridUrl, initialNodes);
      // visit page
      cy.visit(`administration-workspace/${pageUrl}`);
      // wait for ref data request to fire
      adminReferentialDataProvider.wait();
      // wait for initial tree data request to fire
      gridElement.waitInitialDataFetch();
      return new AdminWorkspaceCustomFieldsPage(gridElement);
    }

  protected getPageUrl(): string {
    return 'custom-fields';
  }

  openCreateCustomField(): CreateCustomFieldDialog {
    this.clickCreateButton();
    return new CreateCustomFieldDialog;
  }

  deleteCUFByName(name: string, confirm: boolean) {
    this.deleteRowWithMatchingCellContent('name', name, confirm);
  }

  protected getDeleteUrl(): string {
    return 'custom-fields';
  }

  clickAnchor(linkId: string, url: string): Page {
    let page;
    let gridElement: GridElement;
    cy.route('POST', `/squash/backend/${url}`).as(`${url}`);
    cy.get(`[anchor-link-id=${linkId}]`).click();
    cy.wait(`@${url}`);
    switch (linkId) {
      case 'info-lists':
        cy.get('[anchor-link-id="info-lists"]', { timeout: 10000 }).should('exist');
        gridElement = GridElement.createGridElement('infoLists', 'info-lists');
        page =  new AdminWorkspaceInfoListsPage(gridElement);
        page.assertExist();
        break;
      case 'custom-fields':
        cy.get('[anchor-link-id="custom-fields"]', { timeout: 10000 }).should('exist');
        gridElement = GridElement.createGridElement('customFields', 'custom-fields');
        page = new AdminWorkspaceCustomFieldsPage(gridElement);
        page.assertExist();
        break;
      case 'requirements-links':
        cy.get('[anchor-link-id="requirements-links"]', { timeout: 10000 }).should('exist');
        gridElement = GridElement.createGridElement('requirementsLinks', 'requirements-links');
        page = new AdminWorkspaceRequirementsLinksPage(gridElement);
        page.assertExist();
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }

    gridElement.assertExist();
    return page;
  }

  selectCustomFieldByName(name: string,
                          viewResponse?: CustomField): CustomFieldViewPage {

    const view = new CustomFieldViewPage();
    const mock = new HttpMockBuilder('custom-field-view/*')
      .responseBody(viewResponse)
      .build();

    this.selectRowWithMatchingCellContent('name', name);

    mock.waitResponseBody().then(() => {
      view.waitInitialDataFetch();
      view.assertExist();
    });

    return view;
  }

}
