import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {GridElement} from '../../elements/grid/grid.element';
import {AdministrationWorkspacePage} from './administration-workspace.page';
import {PageFactory} from '../page';
import {ReferentialData} from '../../../model/referential-data.model';
import {CreateRequirementsLinkDialog} from './dialogs/create-requirements-links-dialog.element';
import {AdminReferentialDataProviderBuilder} from '../../../utils/referential/admin-referential-data.provider';
import {RequirementVersionLinkType} from '../../../model/requirements/requirement-version-link-type.model';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';

export class AdminWorkspaceRequirementsLinksPage extends AdministrationWorkspacePage {

  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-main-custom-workspace');
  }

  public static initTestAtPageRequirementsLinks: PageFactory<AdminWorkspaceRequirementsLinksPage> =
    (requirementVersionLinkTypes: RequirementVersionLinkType[], referentialData?: ReferentialData) => {
      return AdminWorkspaceRequirementsLinksPage.initTestAtPage(requirementVersionLinkTypes, 'requirementsLinks',
        'entities-customization/requirements-links', referentialData);
    }

  public static initTestAtPage: PageFactory<AdminWorkspaceRequirementsLinksPage> =
    (requirementVersionLinkTypes: RequirementVersionLinkType[], gridId: string, pageUrl: string,
     referentialData?: ReferentialData) => {
      const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(referentialData).build();
      const reqLinksMock = new HttpMockBuilder('requirements-links').responseBody(
        {requirementLinks: requirementVersionLinkTypes}).post().build();
      const gridElement = GridElement.createGridElement(gridId);
      // visit page
      cy.visit(`administration-workspace/${pageUrl}`);
      // wait for ref data request to fire
      adminReferentialDataProvider.wait();
      // wait for requirement links data request to fire
      reqLinksMock.waitResponseBody();
      return new AdminWorkspaceRequirementsLinksPage(gridElement);
    }

  openCreateRequirementsLink(): CreateRequirementsLinkDialog {
    this.clickCreateButton();
    return new CreateRequirementsLinkDialog;
  }

  deleteLinkByName(cellId: string, content: string, confirm: boolean, error?: boolean, errorMessage?: string) {
    this.deleteRowWithMatchingCellContent(cellId, content, confirm, undefined,  error, errorMessage );
  }

  makeLinkDefault(cellId: string, content: string) {
    const button = cy.get(`sqtm-core-grid-cell[data-test-cell-id="${cellId}"]`)
      .contains('span', content)
      .closest('sqtm-core-grid-cell')
      .siblings('sqtm-core-grid-cell[data-test-cell-id="isDefault"]')
      .find('input')
      .check()
      .should('be.checked');
  }

  selectLinksByContent(cellId: string, names: string[]) {
    this.selectRowsWithMatchingCellContent(cellId, names);
  }

  assertExist() {
    super.assertExist();
  }

  protected getPageUrl(): string {
    return 'requirements-links';
  }

  protected getDeleteUrl(): string {
    return 'requirements-links';
  }
}
