import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {GridElement} from '../../elements/grid/grid.element';
import {PageFactory} from '../page';
import {GridResponse} from '../../../model/grids/data-row.type';
import {ReferentialData} from '../../../model/referential-data.model';
import {AdministrationWorkspacePage} from './administration-workspace.page';
import {CreateTestAutomationServerDialog} from './dialogs/create-test-automation-server-dialog.element';
import {AdminReferentialDataProviderBuilder} from '../../../utils/referential/admin-referential-data.provider';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {AdminTestAutomationServer} from '../../../model/test-automation/test-automation-server.model';
import {TestAutomationServerViewPage} from './test-automation-server-view/test-automation-server-view.page';
import {selectByDataTestToolbarButtonId} from '../../../utils/basic-selectors';

export class AdminWorkspaceAutomationServersPage extends AdministrationWorkspacePage {

  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-main-server-workspace');
  }

  public static initTestAtPageTestAutomationServers: PageFactory<AdminWorkspaceAutomationServersPage> =
    (initialNodes: GridResponse = {dataRows: []}, referentialData?: ReferentialData) => {
      return AdminWorkspaceAutomationServersPage.initTestAtPage(initialNodes, 'testAutomationServers', 'test-automation-servers',
        'servers/test-automation-servers', referentialData);
    };

  public static initTestAtPage: PageFactory<AdminWorkspaceAutomationServersPage> =
    (initialNodes: GridResponse = {dataRows: []}, gridId: string, gridUrl: string, pageUrl: string,
     referentialData?: ReferentialData) => {
      const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(referentialData).build();
      const gridElement = GridElement.createGridElement(gridId, gridUrl, initialNodes);
      // visit page
      cy.visit(`administration-workspace/${pageUrl}`);
      // wait for ref data request to fire
      adminReferentialDataProvider.wait();
      // wait for initial tree data request to fire
      gridElement.waitInitialDataFetch();
      return new AdminWorkspaceAutomationServersPage(gridElement);
    };

  openCreateTestAutomationServer(): CreateTestAutomationServerDialog {
    cy.get(`i${selectByDataTestToolbarButtonId('create-button')}`)
      .click();

    return new CreateTestAutomationServerDialog;
  }

  protected getPageUrl(): string {
    return 'test-automation-servers';
  }

  protected getDeleteUrl(): string {
    return 'test-automation-servers';
  }

  selectAutomationServerByName(names: string[],
                         viewResponse?: AdminTestAutomationServer): TestAutomationServerViewPage {

    const view = new TestAutomationServerViewPage();
    const mock = new HttpMockBuilder('test-automation-server-view/*')
      .responseBody(viewResponse)
      .build();

    this.selectRowsWithMatchingCellContent('name', names);

    mock.waitResponseBody().then(() => {
      view.waitInitialDataFetch();
    });

    return view;
  }
}
