import {RemoveAdministrationEntityDialog} from '../remove-administration-entity-dialog';

export class RemoveScmServerDialogElement extends RemoveAdministrationEntityDialog {
  constructor(public readonly serverIds: number[]) {
    super('scm-servers', 'scm-servers', serverIds);
  }

  assertMessageWhenUnused() {
    const expectedText = 'Le serveur de partage de code source sera supprimé, cette action ne peut être annulée. ' +
      'Confirmez-vous la suppression du serveur de partage de code source ?';

    cy.get(this.buildMessageSelector()).should('contain.text', expectedText);
  }

  assertMessageWhenScmServerBoundToProject() {
    const expectedText = 'Le serveur de partage de code source contient au moins un dépôt actuellement associé à un projet.';

    cy.get(this.buildMessageSelector()).should('contain.text', expectedText);
  }

  assertMessageWhenScmServersBoundToProject() {
    const expectedText = 'Au moins un des serveurs de partage de code source sélectionnés contient au moins un dépôt ' +
      'actuellement associé à un projet.';

    cy.get(this.buildMessageSelector()).should('contain.text', expectedText);
  }
}
