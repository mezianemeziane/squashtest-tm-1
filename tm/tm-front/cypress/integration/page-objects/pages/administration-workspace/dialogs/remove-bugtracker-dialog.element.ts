import {RemoveAdministrationEntityDialog} from '../remove-administration-entity-dialog';

export class RemoveBugtrackerDialogElement extends RemoveAdministrationEntityDialog {
  constructor(serverIds: number[]) {
    super('bugtracker', 'bugtrackers', serverIds);
  }

  assertMessageSingular() {
    const expectedText = 'Le serveur ainsi que les associations d\'anomalies présentes dans les blocs ' +
      'et ancres "Anomalies connues" seront supprimés.\nCette action ne peut être annulée.\nConfirmez-vous la suppression du serveur ?';


    cy.get(this.buildMessageSelector()).should('contain.text', expectedText);
  }
}
