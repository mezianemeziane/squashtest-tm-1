import {RemoveAdministrationEntityDialog} from '../remove-administration-entity-dialog';

export class RemoveMilestoneDialogElement extends RemoveAdministrationEntityDialog {
  constructor(public readonly milestoneIds: number[]) {
    super('milestones', 'milestones', milestoneIds);
  }
}
