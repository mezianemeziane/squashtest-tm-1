import {RemoveAdministrationEntityDialog} from '../remove-administration-entity-dialog';

export class RemoveCustomFieldDialogElement extends RemoveAdministrationEntityDialog {
  constructor(cufIds: number[]) {
    super('custom-fields', 'custom-fields', cufIds);
  }
}
