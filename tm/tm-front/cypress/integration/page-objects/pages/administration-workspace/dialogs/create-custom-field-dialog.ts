import {TextFieldElement} from '../../../elements/forms/TextFieldElement';
import {SelectFieldElement} from '../../../elements/forms/select-field.element';
import {CreateAdministrationEntityDialog} from '../create-administration-entity-dialog';
import {RichTextFieldElement} from '../../../elements/forms/RichTextFieldElement';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {HttpResponseStatus} from '../../../../utils/mocks/request-mock';
import {CheckBoxElement} from '../../../elements/forms/check-box.element';
import {GridElement} from '../../../elements/grid/grid.element';
import {CommonSelectors} from '../../../elements/forms/abstract-form-field.element';

export class CreateCustomFieldDialog extends CreateAdministrationEntityDialog {

  public readonly nameField: TextFieldElement;
  public readonly labelField: TextFieldElement;
  public readonly codeField: TextFieldElement;
  public readonly inputTypeField: SelectFieldElement;
  private readonly optionalField: CheckBoxElement;

  public readonly defaultTextField: TextFieldElement;
  public readonly defaultRichTextField: RichTextFieldElement;
  public readonly defaultNumericField: TextFieldElement;
  public readonly defaultTagsField: SelectFieldElement;
 // private readonly defaultDateField: EditableDateFieldElement;

  public readonly dropdownListOptionNameField: TextFieldElement;
  public readonly dropdownListOptionCodeField: TextFieldElement;
  public readonly dropdownListOptionsGrid: GridElement;


  constructor() {
    super('new-entity', 'custom-fields/new', 'custom-fields');
    this.nameField = new TextFieldElement('name');
    this.labelField = new TextFieldElement('label');
    this.codeField = new TextFieldElement('code');
    this.inputTypeField = new SelectFieldElement(CommonSelectors.fieldName('inputType'));
    this.defaultTextField = new TextFieldElement('defaultText');
    this.defaultRichTextField = new RichTextFieldElement('defaultRichText');
    this.defaultNumericField = new TextFieldElement('defaultNumeric');
   // this.defaultCheckedField = new CheckBoxElement('defaultChecked', 'checkBox');
    this.defaultTagsField = new SelectFieldElement(CommonSelectors.fieldName('defaultTags'));
    this.optionalField = new CheckBoxElement(CommonSelectors.fieldName('optional'));
   // this.defaultDateField = new EditableDateFieldElement('defaultDate', 'date');

    this.dropdownListOptionNameField = new TextFieldElement('dropdownListOptionName');
    this.dropdownListOptionCodeField = new TextFieldElement('dropdownListOptionCode');
    this.dropdownListOptionsGrid = GridElement.createGridElement('dropdown-list-options');
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  fillLabel(label: string) {
    this.labelField.fill(label);
  }

  fillCodeField(codeField: string) {
    this.codeField.fill(codeField);
  }

  fillDefaultTextField(defaultTextField: string) {
    this.defaultTextField.fill(defaultTextField);
  }

  fillDefaultRichTextField(defaultRichTextField: string) {
   this.defaultRichTextField.fill(defaultRichTextField);
  }


  fillDefaultNumericField(defaultNumericField: string) {
    this.defaultNumericField.fill(defaultNumericField);
  }

  selectTypeField(inputTypeField: string) {
    this.inputTypeField.selectValue(inputTypeField);
  }

  selectDefaultDateField() {
    cy.get('div[data-test-field-name="defaultDate"]').click();
    cy.get('.ant-picker-today-btn')
      .should('have.text', ' Aujourd\'hui ')
      .dblclick();
    cy.get('a')
      .contains(' Aujourd\'hui ')
      .should('not.exist');
  }

  assignDefaultCheckField(defaultValue: 'Vrai'|'Faux') {
    const checkedField = new SelectFieldElement(CommonSelectors.fieldName('defaultChecked'));
    checkedField.selectValue(defaultValue);
  }

  toggleOptionalCheck() {
    this.optionalField.toggleState();
  }


  assignDefaultTagsField(defaultTagsField: string ) {
    this.defaultTagsField.selector.find('span').click({force: true});
    this.defaultTagsField.selector.find('input').type(defaultTagsField, {force: true} );
    this.defaultTagsField.selector.find('input').type('{enter}');
    this.clickOnAddButton();
  }

  fillDropdownListOptionName(dropdownListOptionName: string) {
    this.dropdownListOptionNameField.fill(dropdownListOptionName);
  }

  fillDropdownListOptionCode(dropdownListOptionCode: string) {
    this.dropdownListOptionCodeField.fill(dropdownListOptionCode);
  }


  addDropdownListOption(dropdownListOptionName: string, dropdownListOptionCode: string) {
    this.fillDropdownListOptionName(dropdownListOptionName);
    this.fillDropdownListOptionCode(dropdownListOptionCode);
    cy.get('button[data-test-dialog-button-id="add-option"').click();
  }

  toggleOptionDefaultValue(optionName: string) {
    cy.get(`sqtm-core-grid-cell[data-test-cell-id="optionName"]`)
      .contains('span', optionName)
      .closest('sqtm-core-grid-cell')
      .siblings('sqtm-core-grid-cell[data-test-cell-id="default"]')
      .find('span')
      .first()
      .click();
  }

  checkIfOptionNameAlreadyExistErrorIsDisplayed() {
    const errorMessage = 'Le nom de l\'option existe déjà.';
    cy.get('span[data-test-error-key="sqtm-core.validation.errors.optionNameAlreadyExists"]').should('contain.text', errorMessage);
  }

  checkIfOptionCodeAlreadyExistErrorIsDisplayed() {
    const errorMessage = 'Le code de l\'option existe déjà.';
    cy.get('span[data-test-error-key="sqtm-core.validation.errors.optionCodeAlreadyExists"]').should('contain.text', errorMessage);
  }

  checkIfOptionDefaultValueRequiredErrorIsDisplayed() {
    const errorMessage = ' Une option par défaut doit être définie lorsque le champ personnalisé est obligatoire.';
    cy.get('span[data-test-error-key="default-option-required"]').should('contain.text', errorMessage);
  }

  checkIfInvalidCodePatternErrorIsDisplayed() {
    const errorMessage = 'Ne doit contenir que des lettres, nombres ou underscores.';
    cy.get('span[data-test-error-key="sqtm-core.validation.errors.invalidCodePattern"]').should('contain.text', errorMessage);
  }



  createOptionalCUF(name: string, label: string, codeField: string, inputTypeField: string, defaultValue: any = '*') {
    this.fillName(name);
    this.fillLabel(label);
    this.fillCodeField(codeField);
    this.selectTypeField(inputTypeField);

      switch (inputTypeField) {
        case'Texte simple' :
          this.fillDefaultTextField(defaultValue);
          this.addForSuccess(false, null, null, 201);
          break;
        case'Date' :
          this.selectDefaultDateField();
          this.addForSuccess(false, null, null, 201);
          break;
        case'Numérique' :
          this.fillDefaultNumericField(defaultValue);
          this.addForSuccess(false, null, null, 201);
          break;
        case'Tag' :
          this.assignDefaultTagsField(defaultValue);
          break;
        case'Texte riche' :
          this.fillDefaultRichTextField(defaultValue);
          this.addForSuccess(false, null, null, 201);
          break;
        case'Case à cocher' :
           this.assignDefaultCheckField(defaultValue);
          this.addForSuccess(false, null, null, 201);
          break;
      }

  //  this.addForSuccess(false, null, null, 201);
    cy.get('button[data-test-dialog-button-id="add"').should('not.exist');
  }

  createNonOptionalCUF(name: string, label: string, codeField: string, inputTypeField: string, defaultValue: any = '*') {
    this.fillName(name);
    this.fillLabel(label);
    this.fillCodeField(codeField);
    this.selectTypeField(inputTypeField);

    this.toggleOptionalCheck();
    // cy.get('span[data-test-field-name="optional"')
    //   .find('span')
    //   .should('have.class', 'ant-checkbox ant-checkbox-checked')
    //   .find('input')
    //   .click();
    //
    // cy.get('span[data-test-field-name="optional"')
    //   .find('span')
    //   .should('not.have.class', 'ant-checkbox ant-checkbox-checked');

    switch (inputTypeField) {
      case'Texte simple' :
        this.fillDefaultTextField(defaultValue);
        break;
      case'Date' :
        this.selectDefaultDateField();
        break;
      case'Numérique' :
        this.fillDefaultNumericField(defaultValue);
        break;
      case'Tag' :
        this.assignDefaultTagsField(defaultValue);
        break;
      case'Texte riche' :
        this.fillDefaultRichTextField(defaultValue);
        break;
      case'Case à cocher' :
        if (defaultValue) {
          this.assignDefaultCheckField(defaultValue);
        }
        break;
    }


    this.addForSuccess(false, null, null, 201);
    cy.get('button[data-test-dialog-button-id="add"').should('not.exist');


  }

  protected addForSuccess(addAnother: boolean,
                          createResponse?: any,
                          gridResponse?: GridResponse,
                          createResponseStatus?: HttpResponseStatus): Cypress.Chainable<any> {
    return super.addForSuccess(addAnother, createResponse, gridResponse, 201);
  }

}
