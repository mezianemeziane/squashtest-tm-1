import {TextFieldElement} from '../../../elements/forms/TextFieldElement';
import {RichTextFieldElement} from '../../../elements/forms/RichTextFieldElement';
import {CreateAdministrationEntityDialog} from '../create-administration-entity-dialog';
import {SelectFieldElement} from '../../../elements/forms/select-field.element';
import {CommonSelectors} from '../../../elements/forms/abstract-form-field.element';

export class CreateProjectDialog extends CreateAdministrationEntityDialog {

  private readonly nameField: TextFieldElement;
  private readonly labelField: TextFieldElement;
  private readonly descriptionField: RichTextFieldElement;
  private readonly templateField: SelectFieldElement;

  constructor() {
    super('new-entity', 'projects/new', 'generic-projects');

    this.nameField = new TextFieldElement('name');
    this.labelField = new TextFieldElement('label');
    this.descriptionField = new RichTextFieldElement('description');
    this.templateField = new SelectFieldElement(CommonSelectors.fieldName('template'));
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  fillLabel(label: string) {
    this.labelField.fill(label);
  }

  fillDescription(description: string) {
    this.descriptionField.fill(description);
  }

  createProject(name: string, label: string, description: string) {
    this.fillName(name);
    this.fillLabel(label);
    this.fillDescription(description);
    this.clickOnAddButton();
  }

  checkIfFormIsEmpty() {
    this.nameField.checkContent('');
    this.labelField.checkContent('');
    this.descriptionField.checkContent('');
  }

  selectTemplate(templateName: string) {
    this.templateField.selectValue(templateName);
  }

  getKeepTemplatePluginConfigurationBindingCheckbox(pluginId: string) {
    return cy.get(`[data-test-component-id="keepBinding_${pluginId}"]`);
  }

  getCopyTemplatePluginConfigurationCheckbox(pluginId: string) {
    return cy.get(`[data-test-component-id="copyConfiguration_${pluginId}"]`);
  }

  assertCopyTemplatePluginConfigurationCheckboxIsEnabled(pluginId: string): void {
    cy.get(`[data-test-component-id="copyConfiguration_${pluginId}"]`)
      .find('input')
      .should('not.have.attr', 'disabled');
  }

  assertCopyTemplatePluginConfigurationCheckboxIsDisabled(pluginId: string): void {
    cy.get(`[data-test-component-id="copyConfiguration_${pluginId}"]`)
      .find('input')
      .should('have.attr', 'disabled');
  }

  assertCopyTemplatePluginConfigurationCheckboxIsChecked(pluginId: string): void {
    cy.get(`[data-test-component-id="copyConfiguration_${pluginId}"]`)
      .find('input')
      .should('be.checked');
  }
}
