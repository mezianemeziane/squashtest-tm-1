import {TextFieldElement} from '../../../elements/forms/TextFieldElement';
import {CreateAdministrationEntityDialog} from '../create-administration-entity-dialog';
import {RichTextFieldElement} from '../../../elements/forms/RichTextFieldElement';

export class CreateTestAutomationServerDialog extends CreateAdministrationEntityDialog {


  private readonly nameField: TextFieldElement;
  private readonly urlField: TextFieldElement;
  private readonly descriptionField: RichTextFieldElement;

  constructor() {
    super('new-test-automation-server', 'test-automation-servers/new', 'test-automation-servers');

    this.nameField = new TextFieldElement('name');
    this.urlField = new TextFieldElement('baseUrl');
    this.descriptionField = new RichTextFieldElement('description');
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  fillUrl(baseUrl: string) {
    this.urlField.fill(baseUrl);
  }

  fillDescription(description: string) {
    this.descriptionField.fill(description);
  }

  openCreateTestAutomationServer(name: string, baseUrl: string, login: string, password: string, description: string) {
    this.fillName(name);
    this.fillUrl(baseUrl);
    this.fillDescription(description);
    this.addForSuccess(false);
  }

  checkIfFormIsEmpty() {
    this.nameField.checkContent('');
    this.urlField.checkContent('');
    this.descriptionField.checkContent('');
  }

}
