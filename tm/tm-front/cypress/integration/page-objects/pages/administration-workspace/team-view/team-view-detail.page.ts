import {TeamViewPage} from './team-view.page';

export class TeamViewDetailPage extends TeamViewPage {
  constructor() {
    super('sqtm-app-team-view-detail > div');
  }

  clickBackButton(): void {
    cy.get(this.rootSelector)
      .find('[data-test-button-id="back"]')
      .click();
  }
}
