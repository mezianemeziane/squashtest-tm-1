import {Page} from '../../page';
import {TeamMembersPanelElement} from './panels/team-members-panel.element';
import {TeamAuthorisationsPanelElement} from './panels/team-authorisations-panel.element';

export class TeamViewPage extends Page {

  public readonly teamAuthorisationsPanel: TeamAuthorisationsPanelElement;
  public readonly teamMembersPanel: TeamMembersPanelElement;

  constructor(selector: string = 'sqtm-app-team-view > div') {
    super(selector);
    this.teamAuthorisationsPanel = new TeamAuthorisationsPanelElement();
    this.teamMembersPanel = new TeamMembersPanelElement();
  }

  waitInitialDataFetch() {
    this.teamAuthorisationsPanel.waitInitialDataFetch();
    this.teamMembersPanel.waitInitialDataFetch();
  }

  foldGrid() {
    cy.get(`[data-test-component-id="fold-tree-button"]`).click();
  }
}
