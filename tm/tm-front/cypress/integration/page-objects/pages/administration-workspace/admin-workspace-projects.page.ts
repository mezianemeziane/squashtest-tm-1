import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {GridElement} from '../../elements/grid/grid.element';
import {CreateProjectDialog} from './dialogs/create-project-dialog.element';
import {AdministrationWorkspacePage} from './administration-workspace.page';
import {PageFactory} from '../page';
import {GridResponse} from '../../../model/grids/data-row.type';
import {ReferentialData} from '../../../model/referential-data.model';
import {ReferentialDataProviderBuilder} from '../../../utils/referential/referential-data.provider';
import {CreateTemplateDialog} from './dialogs/create-template-dialog.element';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {CreateTemplateFromProjectDialog} from './dialogs/create-template-from-project-dialog.element';
import {ProjectViewMockData, ProjectViewPage} from './project-view/project-view.page';
import {ProjectView} from '../../../model/project/project.model';
import {AdminReferentialDataProviderBuilder} from '../../../utils/referential/admin-referential-data.provider';
import {selectByDataTestMenuItemId, selectByDataTestToolbarButtonId} from '../../../utils/basic-selectors';

export class AdminWorkspaceProjectsPage extends AdministrationWorkspacePage {

  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-project-workspace');
  }

  public static initTestAtPageProjects: PageFactory<AdminWorkspaceProjectsPage> =
    (initialNodes: GridResponse = {dataRows: []}, referentialData?: ReferentialData) => {
      let page;
      page = AdminWorkspaceProjectsPage
        .initTestAtPage(initialNodes, 'projects', 'generic-projects', 'projects', referentialData);
      return page;
    };

  public static initTestAtPage: PageFactory<AdminWorkspaceProjectsPage> = (
        initialNodes: GridResponse = {dataRows: []},
        gridId: string,
        gridUrl: string,
        pageUrl: string,
        referentialData?: ReferentialData) => {
    new ReferentialDataProviderBuilder(referentialData).build();
    new AdminReferentialDataProviderBuilder(referentialData).build();
    const gridElement = GridElement.createGridElement(gridId, gridUrl, initialNodes);

    cy.visit(`administration-workspace/${pageUrl}`);
    gridElement.waitInitialDataFetch();
    return new AdminWorkspaceProjectsPage(gridElement);
  };

  selectProjectByName(name: string,
                      viewResponse?: ProjectView,
                      projectViewMockData?: ProjectViewMockData): ProjectViewPage {
    const projectView = new ProjectViewPage(projectViewMockData);

    const mock = new HttpMockBuilder('project-view/*').responseBody(viewResponse).build();
    this.selectRowWithMatchingCellContent('name', name);

    mock.wait();
    projectView.waitInitialDataFetch();
    projectView.assertExist();

    return projectView;
  }

  openCreateProject(templates: { id: number, name: string }[] = []): CreateProjectDialog {
    const templateListMock = new HttpMockBuilder('generic-projects/templates')
      .responseBody({templates})
      .build();

    cy.get(selectByDataTestToolbarButtonId('create-button'))
      .click()
      .get(selectByDataTestMenuItemId('new-project'))
      .click();

    templateListMock.wait();

    return new CreateProjectDialog();
  }

  openCreateTemplate(): CreateTemplateDialog {
    cy.get(selectByDataTestToolbarButtonId('create-button'))
      .click()
      .get(selectByDataTestMenuItemId('new-template'))
      .click();

    return new CreateTemplateDialog();
  }

  openCreateTemplateFromProject() {
    cy.get(selectByDataTestToolbarButtonId('create-button'))
      .click()
      .get(selectByDataTestMenuItemId('new-template-from-project'))
      .click()
      .should('not.be.visible');

    return new CreateTemplateFromProjectDialog();
  }

  // TODO this method uses external CSS ('ant-' prefixed classes) so it might break someday...
  checkIfCreateFromProjectDisabled(isDisabled: boolean) {
    cy.get('[data-test-toolbar-button-id="create-button"]')
      .should('exist')
      .click().then(() => {
      if (isDisabled) {
        cy.get('[data-test-menu-item-id="new-template-from-project"]')
          .should('have.class', 'ant-dropdown-menu-item-disabled');
      } else {
        cy.get('[data-test-menu-item-id="new-template-from-project"]')
          .should('have.not.class', 'ant-dropdown-menu-item-disabled');
      }
    });
  }

  checkIfEntryIsModel(name: string, isModel: boolean) {
    cy.get('sqtm-core-grid-cell[data-test-cell-id="name"]')
      .find('span')
      .contains(name)
      .closest('sqtm-core-grid-cell')
      .siblings('sqtm-core-grid-cell[data-test-cell-id="isTemplate"]')
      .as('element');
    if (isModel) {
      cy.get('@element')
        .find('svg')
        .should('exist');
    } else {
      expect(cy.get('@element')).not.to.have.descendants('svg');
    }

  }

  fillSearchInput(value: string, gridResponse?: GridResponse) {
    const mock = new HttpMockBuilder('generic-projects').post().responseBody(gridResponse).build();

    cy.get('[data-test-component-id="project-filter-field"').find('input').type(value);

    mock.wait();
  }

  protected getPageUrl(): string {
    return 'projects';
  }
}
