import {HttpMock, HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {GroupedMultiListElement} from '../../../../elements/filters/grouped-multi-list.element';
import {SelectFieldElement} from '../../../../elements/forms/select-field.element';
import {GridElement} from '../../../../elements/grid/grid.element';
import {CommonSelectors} from '../../../../elements/forms/abstract-form-field.element';


export interface ProjectWithoutPermission {
  id: string;
  name: string;
}


export class AddUserAuthorisationsDialog {

  public readonly profileSelectField: SelectFieldElement;

  private readonly dialogId = 'add-user-authorisation-dialog';

  private projectsWithoutPermissionMock: HttpMock<ProjectWithoutPermission[]>;

  constructor(private readonly permissionsGrid: GridElement, projectsWithoutPermission: ProjectWithoutPermission[]) {
    this.projectsWithoutPermissionMock = new HttpMockBuilder<ProjectWithoutPermission[]>('user-view/*/projects-without-permission')
      .responseBody(projectsWithoutPermission)
      .build();

    this.profileSelectField = new SelectFieldElement(CommonSelectors.fieldName('permission-profile'));
  }

  waitInitialDataFetch() {
    this.projectsWithoutPermissionMock.wait();
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  confirm(updatedPermissions?: ProjectWithoutPermission[]) {
    const mock = new HttpMockBuilder('users/*/permissions/*')
      .responseBody({projectPermissions: updatedPermissions})
      .post()
      .build();

    this.clickOnConfirmButton();

    mock.wait();
  }

  cancel() {
    const buttonSelector = this.buildButtonSelector('cancel');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }

  selectProjects(...projectNames: string[]) {
    cy.get('[data-test-component-id="grouped-multi-list-display-value"]').click();
    const multiList = new GroupedMultiListElement();
    projectNames.forEach(name => multiList.toggleOneItem(name));
    cy.clickVoid();
    multiList.assertNotExist();
  }

  selectProfile(profile: string) {
    this.profileSelectField.selectValue(profile);
  }
}
