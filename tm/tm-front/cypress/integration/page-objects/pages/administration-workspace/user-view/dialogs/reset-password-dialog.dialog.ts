import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {TextFieldElement} from '../../../../elements/forms/TextFieldElement';


export class ResetPasswordDialog {


  private readonly dialogId = 'reset-password-dialog';

  public readonly password: TextFieldElement;
  public readonly confirmPassword: TextFieldElement;

  constructor() {
    this.password = new TextFieldElement('password');
    this.confirmPassword = new TextFieldElement('confirmPassword');

  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  confirm(password: string) {
    const mock = new HttpMockBuilder(`users/*/reset-password?password=${password}`)
      .post()
      .build();

    this.clickOnConfirmButton();

    mock.wait();
  }

  cancel() {
    const buttonSelector = this.buildButtonSelector('cancel');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }

  fillPassword(password: string) {
    this.password.fill(password);
  }

  fillConfirmPassword(confirmPassword: string) {
    this.confirmPassword.fill(confirmPassword);
  }

}
