import {Page} from '../../../page';
import {TextFieldElement} from '../../../../elements/forms/TextFieldElement';
import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';

export class TestAutoServerAuthPolicyElement extends Page {
  usernameField: TextFieldElement;
  passwordField: TextFieldElement;

  constructor() {
    super('sqtm-app-taserver-auth-policy-panel');

    this.usernameField = new TextFieldElement('username');
    this.passwordField = new TextFieldElement('password');
  }

  get sendButtonSelector(): string {
    return this.rootSelector + ' [data-test-component-id="save-credentials-button"]';
  }

  get statusMessageSelector(): string {
    return this.rootSelector + ' [data-test-component-id="status-message"]';
  }


  assertSendButtonEnabled(): void {
    cy.get(this.sendButtonSelector).should('not.be.disabled');
  }

  assertSendButtonDisabled(): void {
    cy.get(this.sendButtonSelector).should('be.disabled');
  }

  sendCredentialsForm(): void {
    const mock = new HttpMockBuilder('test-automation-servers/*/credentials').post().build();
    cy.get(this.sendButtonSelector).click();
    mock.wait();
  }

  assertStatusMessageNotVisible(): void {
    cy.get(this.statusMessageSelector).should('not.be.visible');
  }

  assertSaveSuccessMessageVisible(): void {
    cy.get(this.statusMessageSelector).should('contain.text', 'Enregistré');
  }

  sendCredentialsFormWithServerError(errorMessage: string): void {
    const response = {
      squashTMError: {
        kind: 'ACTION_ERROR',
        actionValidationError: {
          i18nKey: errorMessage
        },
      },
    };

    const mock = new HttpMockBuilder('test-automation-server/*/credentials')
      .post()
      .status(412)
      .responseBody(response)
      .build();

    cy.get(this.sendButtonSelector).click();
    mock.wait();
  }

  assertServerErrorMessageVisible(errorMessage: string): void {
    cy.get(this.statusMessageSelector).should('contain.text', errorMessage);
  }
}
