import {Page} from '../../page';
import {EditableTextFieldElement} from '../../../elements/forms/editable-text-field.element';
import {TestAutoServerInfoPanelElement} from './panels/test-auto-server-info-panel.element';
import {TestAutoServerAuthPolicyElement} from './panels/test-auto-server-auth-policy.element';
import {TestAutoServerAuthProtocolElement} from './panels/test-auto-server-auth-protocol.element';

export class TestAutomationServerViewPage extends Page {

  readonly entityNameField: EditableTextFieldElement;

  readonly informationPanel = new TestAutoServerInfoPanelElement();
  readonly authPolicyPanel = new TestAutoServerAuthPolicyElement();
  readonly authProtocolPanel = new TestAutoServerAuthProtocolElement();

  constructor() {
    super('sqtm-app-bug-tracker-view');

    this.entityNameField = new EditableTextFieldElement('entity-name', 'test-automation-servers/*/name');
  }

  waitInitialDataFetch() {
  }

  foldGrid() {
    cy.get(`[data-test-component-id="fold-tree-button"]`).click();
  }
}
