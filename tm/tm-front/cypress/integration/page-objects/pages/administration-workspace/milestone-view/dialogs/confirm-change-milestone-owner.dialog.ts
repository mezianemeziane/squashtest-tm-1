import {ConfirmDialogElement} from '../../../../elements/dialog/confirm-dialog.element';

export class ConfirmChangeMilestoneOwnerDialog extends ConfirmDialogElement {
  constructor() {
    super('change-milestone-owner-warning');
  }

  assertMessage() {
    const message = 'Vous ne serez plus propriétaire du jalon et ne pourrez plus y accéder, souhaitez vous continuer ?';

    this.assertHasMessage(message);
  }
}
