import {Page} from '../../../page';
import {EditableSelectFieldElement} from '../../../../elements/forms/editable-select-field.element';
import {EditableDateFieldElement} from '../../../../elements/forms/editable-date-field.element';
import {EditableRichTextFieldElement} from '../../../../elements/forms/editable-rich-text-field.element';
import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {MilestoneAdminView} from '../../../../../model/milestone/milestone.model';
import {selectByDataTestFieldId} from '../../../../../utils/basic-selectors';

export class MilestoneInformationPanelElement extends Page {
  public readonly endDateField: EditableDateFieldElement;
  public readonly statusField: EditableSelectFieldElement;
  public readonly rangeField: EditableSelectFieldElement;
  public readonly descriptionField: EditableRichTextFieldElement;

  constructor() {
    super('sqtm-app-custom-field-information-panel');

    const urlPrefix = 'milestones/*/';
    this.endDateField = new EditableDateFieldElement('milestone-end-date', urlPrefix + 'end-date');
    this.statusField = new EditableSelectFieldElement('milestone-status', urlPrefix + 'status');
    this.rangeField = new EditableSelectFieldElement('milestone-range', urlPrefix + 'range');
    this.descriptionField = new EditableRichTextFieldElement('milestone-description', urlPrefix + 'description');
  }

  setOwner(searchString: string, responseBody: MilestoneAdminView): void {
    const changeOwnerMock = new HttpMockBuilder('milestones/*/owner')
      .post().responseBody(responseBody).build();

    this.setOwnerNoMock(searchString);

    changeOwnerMock.wait();
  }

  setOwnerNoMock(searchString: string): void {
    const fieldSelector = '[data-test-field-id="milestone-owner"]';

    cy.get(selectByDataTestFieldId('milestone-owner'))
      .click()
      .find('input')
      .clear()
      .type(searchString)
      .get(fieldSelector)
      .find('[data-test-button-id="confirm"]')
      .click();
  }

  checkOwner(expected: string): void {
    cy.get(selectByDataTestFieldId('milestone-owner'))
      .should('contain.text', expected);
  }

  assertOwnerIsNotEditable(): void {
    cy.get(selectByDataTestFieldId('milestone-owner'))
      .find('.editable')
      .should('not.exist');
  }

  assertOwnerIsEditable(): void {
    cy.get(selectByDataTestFieldId('milestone-owner'))
      .find('div').should('have.class', 'editable');
  }
}
