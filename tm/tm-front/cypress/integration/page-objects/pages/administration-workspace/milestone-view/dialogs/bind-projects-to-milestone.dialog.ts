import {HttpMock, HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {GridElement} from '../../../../elements/grid/grid.element';
import {ProjectInfoForMilestoneAdminView} from '../../../../../model/milestone/milestone.model';



export interface BindableProject {
  id: number;
  label: string;
  name: string;
  template: boolean;
}


export class BindProjectsToMilestoneDialog {


  private readonly dialogId = 'bind-projects-to-milestone';

  private bindableProjectsMock: HttpMock<{ bindableProjects: BindableProject[]}>;

  constructor(private readonly projectsGrid: GridElement, bindableProjects: BindableProject[]) {
    this.bindableProjectsMock = new HttpMockBuilder<{ bindableProjects: BindableProject[] }>('milestone-binding/*/bindable-projects')
      .responseBody({ bindableProjects: bindableProjects })
      .build();
  }

  waitInitialDataFetch() {
    this.bindableProjectsMock.wait();
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  confirm(updatedBoundProjects?: ProjectInfoForMilestoneAdminView[]) {
    const mock = new HttpMockBuilder('milestone-binding/*/bind-projects/*')
      .responseBody({boundProjectsInformation: updatedBoundProjects})
      .post()
      .build();

    this.clickOnConfirmButton();

    mock.wait();
  }

  cancel() {
    const buttonSelector = this.buildButtonSelector('cancel');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }

  selectProjects(...projectNames: string[]) {
    this.projectsGrid.checkBoxInRowsWithMatchingCellContentInPickerGrid('name', projectNames);
  }
}
