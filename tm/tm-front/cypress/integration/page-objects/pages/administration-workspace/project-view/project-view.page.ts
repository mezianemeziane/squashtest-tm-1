import {Page} from '../../page';
import {EditableTextFieldElement} from '../../../elements/forms/editable-text-field.element';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {TextFieldElement} from '../../../elements/forms/TextFieldElement';
import {BugTracker} from '../../../../model/bugtracker/bug-tracker.model';
import {EditableSelectFieldElement} from '../../../elements/forms/editable-select-field.element';
import {EditableDraggableTagFieldElement} from '../../../elements/forms/editable-draggable-tag-field-element';
import {EditableRichTextFieldElement} from '../../../elements/forms/editable-rich-text-field.element';
import {RemoveProjectDialog} from '../dialogs/remove-project-dialog.element';
import {PermissionsPanelElement} from './panels/project-permissions-panel.element';
import {ProjectAutomationPanelElement} from './panels/project-automation-panel.element';
import {ChangeExecutionStatusInUseDialog} from '../dialogs/change-execution-status-in-use-dialog.element';
import {CustomFieldsPanelElement} from './panels/project-custom-fields-panel.element';
import {InfoListsPanelElement} from './panels/project-info-lists-panel.element';
import {MilestonesPanelElement} from './panels/project-milestones-panel.element';
import {selectByDataTestFieldId} from '../../../../utils/basic-selectors';
import {AssociateProjectToTemplateDialog} from './dialogs/associate-project-to-template.dialog';
import {ProjectPluginsPanelElement} from './panels/project-plugins-panel.element';

export class ProjectViewPage extends Page {

  public readonly descriptionTextField: EditableRichTextFieldElement;
  public readonly labelTextField: EditableTextFieldElement;
  public readonly nameTextField: EditableTextFieldElement;
  public readonly linkedTemplateField: TextFieldElement;

  public readonly bugtrackerSelectField: EditableSelectFieldElement;
  public readonly projectNamesInBugtracker: EditableDraggableTagFieldElement;

  public readonly permissionsPanel: PermissionsPanelElement;
  public readonly customFieldsPanel: CustomFieldsPanelElement;
  public readonly infoListsPanel: InfoListsPanelElement;
  public readonly milestonesPanel: MilestonesPanelElement;
  public readonly automationPanel: ProjectAutomationPanelElement;
  public readonly pluginPanel: ProjectPluginsPanelElement;

  constructor(mockData: ProjectViewMockData = defaultMockData) {
    super('sqtm-app-project-view > div');

    // merge provided options with the default ones
    Object.keys(defaultMockData).forEach(key => {
      mockData[key] = mockData[key] || defaultMockData[key];
    });

    this.nameTextField = new EditableTextFieldElement('entity-name', 'generic-projects/*/name');
    this.labelTextField = new EditableTextFieldElement('project-label', 'generic-projects/*/label');
    this.descriptionTextField = new EditableRichTextFieldElement('project-description');
    this.linkedTemplateField = new TextFieldElement('linked-template');
    this.bugtrackerSelectField = new EditableSelectFieldElement('bugtracker-select-field', 'generic-projects/*/bugtracker');
    this.projectNamesInBugtracker = new EditableDraggableTagFieldElement('project-names-tag-field', 'generic-projects/*/bugtracker/projectNames');

    this.permissionsPanel = new PermissionsPanelElement();
    this.automationPanel = new ProjectAutomationPanelElement();
    this.customFieldsPanel = new CustomFieldsPanelElement();
    this.infoListsPanel = new InfoListsPanelElement();
    this.milestonesPanel = new MilestonesPanelElement();
    this.pluginPanel = new ProjectPluginsPanelElement();
  }

  waitInitialDataFetch() {
    this.permissionsPanel.waitInitialDataFetch();
    this.automationPanel.waitInitialDataFetch();
  }

  foldGrid() {
    cy.get(`[data-test-component-id="fold-tree-button"]`).click();
  }

  clickAnchorLink<T extends Page>(linkId: ProjectViewAnchorLinks): T {
    let page: T;
    switch (linkId) {
      case 'information':
        page = this.showInformationPanel(linkId) as unknown as T;
        break;
      case 'automation':
        page = this.showAutomationPanel(linkId) as unknown as T;
        break;
      case 'custom-fields':
        page = this.showCustomFieldsPanel(linkId) as unknown as T;
        break;
      case 'milestones':
        page = this.showMilestonesPanel(linkId) as unknown as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    page.assertExist();
    return page;
  }

  public clickOnCloseButton(): void {
    // Given
    cy.get(this.rootSelector + ' [data-test-component-id="close-view-button"]')
      // When
      .click()
      // Then
      .closest(this.rootSelector)
      .should('not.visible');
  }

  clickOnDeleteProjectMenuItem(): RemoveProjectDialog {
    cy.get('[data-test-button-id="menu-button"]')
      .should('exist')
      .click()
      .get('[data-test-menu-item-id="delete-project"]')
      .should('exist')
      .click();
    return new RemoveProjectDialog();
  }

  transformProjectIntoTemplate() {
    const mock = new HttpMockBuilder('projects/coerce-into-template')
      .post()
      .build();

    cy.get('[data-test-button-id="menu-button"]')
      .should('exist')
      .click()
      .get('[data-test-menu-item-id="transform-into-template"]')
      .should('exist')
      .click()
      .get('[data-test-dialog-button-id="confirm"]')
      .click();

    mock.wait();
  }

  openAssociateToTemplateDialog(templateNames: string[]): AssociateProjectToTemplateDialog {
    const dialog = new AssociateProjectToTemplateDialog(templateNames);

    cy.get('[data-test-button-id="menu-button"]')
      .should('exist')
      .click()
      .get('[data-test-menu-item-id="associate-template"]')
      .should('exist')
      .click()
      .then(() => {
        dialog.waitInitialMocks();
      });

    return dialog;
  }

  disassociateProjectFromTemplate() {
    const mock = new HttpMockBuilder('generic-projects/*/linked-template-id')
      .post()
      .responseBody({templateName: null})
      .build();

    cy.get('[data-test-button-id="menu-button"]')
      .should('exist')
      .click()
      .get('[data-test-menu-item-id="associate-template"]')
      .should('exist')
      .click()
      .get('[data-test-dialog-button-id="confirm"]')
      .click();

    mock.wait();
  }

  modifyData(fieldId: string, value: string) {
    switch (fieldId) {
      case 'entity-name':
        this.nameTextField.setAndConfirmValue(value);
        break;
      case 'project-description':
        cy.get(selectByDataTestFieldId('project-description')).click({force: true});
        this.descriptionTextField.setAndConfirmValue(value);
        break;
      case 'project-label':
        this.labelTextField.setAndConfirmValue(value);
        break;
      case undefined :
        cy.log('fieldId does not exist');
        break;
    }
  }

  checkData(fieldId: string, value: string) {
    cy.get(selectByDataTestFieldId(fieldId))
      .find('span')
      .should('contain.text', value);
  }

  checkDescriptionData(fieldId: string, value: string) {
    cy.get(`${selectByDataTestFieldId(fieldId)} > div > div`).should('contain.text', value);
  }

  checkModificationDate(isNever: boolean, isToday?: boolean) {
    if (isNever) {
      cy.get('[data-test-field-id="project-lastModified"]  span').should('have.text', 'jamais');
    } else {
      cy.get('[data-test-field-id="project-lastModified"]  span').should('not.have.text', 'jamais');
      if (isToday) {
        const today = new Date();
        const dd = String(today.getDate()).padStart(2, '0');
        const mm = String(today.getMonth() + 1).padStart(2, '0'); // January is 0!
        const yyyy = today.getFullYear();
        const todayDate = dd + '/' + mm + '/' + yyyy;
        cy.get('[data-test-field-id="project-lastModified"]  span').should('contain', todayDate);
      }
    }
  }

  delete(confirm: boolean) {
    const removeProjectDialog = this.clickOnDeleteProjectMenuItem();
    if (confirm) {
      removeProjectDialog.deleteForSuccess();
    }
  }

  changeBugtracker(bugtracker: string, confirm: boolean) {
    if (confirm) {
      const mockProjectNames = new HttpMockBuilder('generic-projects/*/bugtracker/projectNames').responseBody({
        projectNames: []
      }).build();

      this.bugtrackerSelectField.setAndConfirmValue(bugtracker);
      mockProjectNames.wait();
    } else {
      this.bugtrackerSelectField.setAndCancel(bugtracker);
    }
  }

  selectNoBugtracker() {
    const mockProjectNames = new HttpMockBuilder('generic-projects/*/bugtracker/projectNames').responseBody({
      projectNames: null
    }).build();

    this.bugtrackerSelectField.setAndConfirmValue('Pas de bugtracker');
    mockProjectNames.wait();
  }

  addUserPermission(user: string, profile: string, confirm: boolean) {
    const createProjectPermissionsDialog = this.permissionsPanel.clickOnAddUserPermissionButton();
    createProjectPermissionsDialog.selectParties(user + ' ' + user + ' ' + '(' + user + ')');
    createProjectPermissionsDialog.selectProfile(profile);
    if (confirm) {
      createProjectPermissionsDialog.clickOnConfirmButton();
    } else {
      createProjectPermissionsDialog.cancel();
    }
  }

  addTeamPermission(team: string, profile: string, confirm: boolean) {
    const createProjectPermissionsDialog = this.permissionsPanel.clickOnAddTeamPermissionButton();
    createProjectPermissionsDialog.selectParties(team + ' ' + team + ' ' + '(' + team + ')');
    createProjectPermissionsDialog.selectProfile(profile);
    if (confirm) {
      createProjectPermissionsDialog.clickOnConfirmButton();
    } else {
      createProjectPermissionsDialog.cancel();
    }
  }

  clickOnSwitchAllowTcModifDuringExec() {
    cy.get('[data-test-switch-id="allowTcModifDuringExec"]').find('button').click();
  }

  clickOnSwitchUntestableStatus() {
    cy.get('[data-test-switch-id="untestableStatus"]').find('button').click();
  }

  clickOnSwitchSettledStatus() {
    cy.get('[data-test-switch-id="settledStatus"]').find('button').click();
  }

  openDialogForStatusesInUseWithinProject(selectedStatus: 'UNTESTABLE' | 'SETTLED') {
    const mockStatuses = new HttpMockBuilder('generic-projects/*/get-enabled-execution-status/' + selectedStatus).responseBody({
      statuses: [
        {id: 'SUCCESS', label: 'execution.execution-status.SUCCESS'},
        {id: 'RUNNING', label: 'execution.execution-status.RUNNING'},
        {id: 'FAILURE', label: 'execution.execution-status.FAILURE'},
        {id: 'READY', label: 'execution.execution-status.READY'},
        {id: 'BLOCKED', label: 'execution.execution-status.BLOCKED'},
      ]
    }).build();

    if (selectedStatus === 'UNTESTABLE') {
      this.clickOnSwitchUntestableStatus();
    } else {
      this.clickOnSwitchSettledStatus();
    }

    mockStatuses.wait();

    return new ChangeExecutionStatusInUseDialog();
  }

  assertPermissionCount(expected: number): void {
    this.getAnchorCounter('project-view-permission-count').should('contain.text', expected);
  }

  assertCustomFieldCount(expected: number): void {
    this.getAnchorCounter('project-view-custom-field-count').should('contain.text', expected);
  }

  assertMilestoneCount(expected: number): void {
    this.getAnchorCounter('project-view-milestone-count').should('contain.text', expected);
  }

  assertPluginCount(expected: number): void {
    this.getAnchorCounter('project-view-plugin-count').should('contain.text', expected);
  }

  private getAnchorCounter(componentId: string): Cypress.Chainable {
    return cy.get(`[data-test-component-id="${componentId}"]`);
  }

  private showInformationPanel<T>(linkId: 'information'): this {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
    return this;
  }

  private showAutomationPanel<T>(linkId: 'automation'): ProjectAutomationPanelElement {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
    return this.automationPanel;
  }

  private showCustomFieldsPanel<T>(linkId: 'custom-fields'): CustomFieldsPanelElement {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
    return this.customFieldsPanel;
  }

  private showMilestonesPanel<T>(linkId: 'milestones'): MilestonesPanelElement {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
    return this.milestonesPanel;
  }
}

export interface ProjectViewMockData {
  availableBugtrackers?: BugTracker[];
  bugtrackerProjectNames?: string[];
}

const defaultMockData: ProjectViewMockData = {
  availableBugtrackers: [],
  bugtrackerProjectNames: [],
};

// tslint:disable-next-line:max-line-length
export type ProjectViewAnchorLinks =
  'information'
  | 'bugtracker'
  | 'permissions'
  | 'execution'
  | 'automation'
  | 'custom-fields'
  | 'info-lists'
  | 'milestones';
