import {TextFieldElement} from '../../../../elements/forms/TextFieldElement';
import {EditableDateFieldElement} from '../../../../elements/forms/editable-date-field.element';
import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {MilestoneAdminProjectView} from '../../../../../model/milestone/milestone-admin-project-view.model';
import {CommonSelectors} from '../../../../elements/forms/abstract-form-field.element';

export interface MilestoneCreationProjectViewResponse {
  milestone: MilestoneAdminProjectView;
}

export class CreateBindMilestoneToProjectDialog {

  private readonly dialogId = 'create-bind-milestone-dialog';
  private readonly nameField: TextFieldElement;
  private readonly deadlineDate: EditableDateFieldElement;

  constructor() {
    this.nameField = new TextFieldElement('label');
    this.deadlineDate = new EditableDateFieldElement(CommonSelectors.fieldName('endDate'));
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  setDeadlineDateToToday() {
    this.deadlineDate.setToTodayAndConfirm();
  }


  confirmCreateAndBindToProject(name: string, newMilestone: MilestoneCreationProjectViewResponse) {
    this.fillName(name);
    this.setDeadlineDateToToday();
    const mock = new HttpMockBuilder('milestone-binding/project/*/create-milestone-and-bind-to-project')
      .responseBody(newMilestone)
      .post()
      .build();

    const buttonSelector = this.buildButtonSelector('create-and-bind-to-project');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();

    mock.wait();
  }

  confirmCreateAndBindToProjectAndObjects(name: string, newMilestone: MilestoneCreationProjectViewResponse) {
    this.fillName(name);
    this.setDeadlineDateToToday();
    const mock = new HttpMockBuilder('milestone-binding/project/*/create-milestone-and-bind-to-project-and-objects')
      .responseBody(newMilestone)
      .post()
      .build();

    const buttonSelector = this.buildButtonSelector('create-and-bind-to-project-and-objects');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();

    mock.wait();
  }

  cancel() {
    const buttonSelector = this.buildButtonSelector('cancel');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }


  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }


  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }
}
