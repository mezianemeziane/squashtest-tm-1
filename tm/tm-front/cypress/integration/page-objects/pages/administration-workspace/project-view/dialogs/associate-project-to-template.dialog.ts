import {HttpMock, HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {SelectFieldElement} from '../../../../elements/forms/select-field.element';
import {CommonSelectors} from '../../../../elements/forms/abstract-form-field.element';

export class AssociateProjectToTemplateDialog {

  private templatesMock: HttpMock<any>;

  constructor(templateNames: string[]) {
    const templates = templateNames.map((templateName, index: number) => ({
      id: index + 1,
      name: templateName,
    }));

    this.templatesMock = new HttpMockBuilder('generic-projects/templates')
      .responseBody({templates})
      .build();
  }

  waitInitialMocks() {
    this.templatesMock.wait();
  }

  selectTemplateAndConfirm(templateName: string) {
    const selectField = new SelectFieldElement(CommonSelectors.fieldName('template'));
    selectField.setValue(templateName);

    const mock = new HttpMockBuilder('generic-projects/*/linked-template-id')
      .post()
      .responseBody({templateName})
      .build();

    cy.get('[data-test-dialog-button-id="confirm"]').click();

    mock.wait();
  }

  assertBindTemplatePluginConfigurationCheckboxExists(pluginId: string): void {
      cy.get(`[data-test-component-id="keepBinding_${pluginId}"]`)
        .should('exist');
  }
}
