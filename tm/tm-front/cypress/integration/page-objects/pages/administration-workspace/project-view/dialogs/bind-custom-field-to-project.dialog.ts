import {SelectFieldElement} from '../../../../elements/forms/select-field.element';
import {HttpMock, HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {GridElement} from '../../../../elements/grid/grid.element';
import {GroupedMultiListElement} from '../../../../elements/filters/grouped-multi-list.element';
import {CustomField} from '../../../../../model/customfield/customfield.model';
import {Bindings} from '../../../../../model/bindable-entity.model';
import {CommonSelectors} from '../../../../elements/forms/abstract-form-field.element';

export interface CustomFieldsResponse {
  customFields: CustomField[];
}

export class BindCustomFieldToProjectDialog {

  public readonly entitySelectField: SelectFieldElement;

  private readonly dialogId = 'project-custom-field-binding';

  private customFieldsMock: HttpMock<CustomFieldsResponse>;

  constructor(private readonly customFieldsGrid: GridElement, customFields: CustomFieldsResponse) {
    this.customFieldsMock = new HttpMockBuilder<CustomFieldsResponse>('custom-fields')
      .responseBody(customFields)
      .build();

    this.entitySelectField = new SelectFieldElement(CommonSelectors.fieldName('bound-entity-field'));
  }

  waitInitialDataFetch() {
    this.customFieldsMock.wait();
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  confirm(updatedCustomFieldsBinding?: Bindings) {
    const mock = new HttpMockBuilder('custom-field-binding/project/*/bind-custom-fields')
      .responseBody(updatedCustomFieldsBinding)
      .post()
      .build();

    this.clickOnConfirmButton();

    mock.wait();
  }

  cancel() {
    const buttonSelector = this.buildButtonSelector('cancel');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }

  selectCuf(...cufNames: string[]) {
    cy.get('[data-test-component-id="cuf-field"]').click();
    const multiList = new GroupedMultiListElement();
    cufNames.forEach(name => multiList.toggleOneItem(name));
    cy.clickVoid();
    multiList.assertNotExist();
  }

  selectEntity(entity: string) {
    this.entitySelectField.selectValue(entity);
  }
}
