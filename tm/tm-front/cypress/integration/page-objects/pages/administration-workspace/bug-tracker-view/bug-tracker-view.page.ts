import {Page} from '../../page';
import {EditableTextFieldElement} from '../../../elements/forms/editable-text-field.element';
import {BugTrackerInformationPanelElement} from './panels/bug-tracker-information-panel.element';
import {BugTrackerAuthenticationProtocolPanelElement} from './panels/bug-tracker-authentication-protocol-panel.element';
import {BugTrackerAuthenticationPolicyPanelElement} from './panels/bug-tracker-authentication-policy-panel.element';

export class BugTrackerViewPage extends Page {

  readonly entityNameField: EditableTextFieldElement;
  readonly informationPanel = new BugTrackerInformationPanelElement();
  readonly authProtocolPanel = new BugTrackerAuthenticationProtocolPanelElement();
  readonly authPolicyPanel = new BugTrackerAuthenticationPolicyPanelElement();

  constructor() {
    super('sqtm-app-bug-tracker-view');

    this.entityNameField = new EditableTextFieldElement('entity-name', 'bugtracker/*/name');
  }

  waitInitialDataFetch() {
  }

  foldGrid() {
    cy.get(`[data-test-component-id="fold-tree-button"]`).click();
  }
}
