import {GridElement} from '../../../../elements/grid/grid.element';
import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {AddScmRepositoryDialog} from '../dialogs/add-scm-repository.dialog';
import {selectByDataTestToolbarButtonId} from '../../../../../utils/basic-selectors';

export class ScmServerRepositoriesPanelElement {
  public readonly grid: GridElement;

  constructor() {
    this.grid = GridElement.createGridElement('scm-server-repositories');
  }

  waitInitialDataFetch() {
  }

  deleteOne(scmServerName: string) {
    const checkIfRepositoryBoundToProject = new HttpMockBuilder('scm-repositories/check-bound-to-project/*')
      .get()
      .build();

    this.grid.findRowId('name', scmServerName).then((id) => {
      this.grid
        .getRow(id)
        .cell('delete')
        .iconRenderer()
        .click();

      checkIfRepositoryBoundToProject.wait();

      const deleteMock = new HttpMockBuilder('scm-repositories/*')
        .delete()
        .build();

      this.clickConfirmDeleteButton();

      deleteMock.wait();
    });
  }

  deleteMultiple(scmServerNames: string[]) {
    this.grid.selectRowsWithMatchingCellContent('name', scmServerNames);

    const checkIfRepositoriesBoundToProject = new HttpMockBuilder('scm-repositories/check-bound-to-project/*')
      .get()
      .build();

    const deleteMock = new HttpMockBuilder('scm-repositories/*')
      .delete()
      .build();

    this.clickOnDeleteButton();
    checkIfRepositoriesBoundToProject.wait();
    this.clickConfirmDeleteButton();
    deleteMock.wait();
  }

  clickOnAddRepositoryButton(): AddScmRepositoryDialog {
    const dialog = new AddScmRepositoryDialog();

    cy.get('[data-test-button-id="add-repository"]')
      .should('exist')
      .click();
    dialog.assertExist();

    return dialog;
  }



  private clickOnDeleteButton() {
    cy.get(selectByDataTestToolbarButtonId('delete-repositories'))
      .should('exist')
      .click();
  }

  private clickConfirmDeleteButton() {
    cy.get('sqtm-core-confirm-delete-dialog')
      .find('[data-test-dialog-button-id="confirm"]')
      .click()
      // Then
      .get('sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }
}
