import {Page} from '../page';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';

export class LogoutPage extends Page {

  public static navigateTo(): LogoutPage {
    const httpMock = new HttpMockBuilder('logout').build();
    cy.visit('logout');
    httpMock.wait();
    return new LogoutPage();
  }

  constructor() {
    super('sqtm-app-logout-page');
  }

  assertLogoutMessageIsVisible() {
    cy.get(this.selectByComponentId('login-message')).should('be.visible');
  }
}
