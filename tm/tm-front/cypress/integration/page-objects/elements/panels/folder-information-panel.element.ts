import {EditableRichTextFieldElement} from '../forms/editable-rich-text-field.element';

export class FolderInformationPanelElement {
  constructor(private folderId: number|string) {
  }

  get descriptionRichField(): EditableRichTextFieldElement {
    const url = `test-case-folder/${this.folderId}/description`;
    return new EditableRichTextFieldElement('description', url);
  }
}
