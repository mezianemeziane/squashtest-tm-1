import Chainable = Cypress.Chainable;
import {HttpMock, HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {GridResponse} from '../../../model/grids/data-row.type';

export class GroupedMultiListElement {

  protected htmlTag = 'sqtm-core-grouped-multi-list';

  protected selectByComponentId(id: string) {
    return `[data-test-component-id="${id}"]`;
  }

  assertExist() {
    cy.get(this.htmlTag).should('exist');
  }

  assertNotExist() {
    cy.get(this.htmlTag).should('not.exist');
  }

  assertGroupExist(label: string) {
    this.selectGroup(label).should('exist');
  }

  assertGroupNotExist(label: string) {
    this.selectGroup(label).should('not.exist');
  }

  toggleOneItem(label: string) {
    this.selectItem(label).click();
  }

  assertGroupContain(groupLabel: string, items: string []) {
    items.forEach(item => {
      this.selectItemInGroup(groupLabel, item).should('exist');
    });
  }

  assertItemIsSelected(itemLabel: string) {
    this.selectItem(itemLabel).parent().find('.ant-checkbox-checked').should('exist');
  }

  assertItemIsNotSelected(itemLabel: string) {
    this.selectItem(itemLabel).parent().find('.ant-checkbox').should('exist');
    this.selectItem(itemLabel).parent().find('.ant-checkbox-checked').should('not.exist');
  }

  assertNoItemIsSelected() {
    cy.get(this.htmlTag).find('.ant-checkbox-checked').should('not.exist');
  }

  filterValues(str: string) {
    cy.get(this.selectByComponentId('input-filter')).type(str);
  }

  clearFilter() {
    cy.get(this.selectByComponentId('input-filter')).clear();
  }

  close() {
    cy.clickVoid();
  }

  private selectGroup(label: string): Chainable {
    const selector = `
    ${this.htmlTag}
    ${this.selectByComponentId('group')}
    `;

    return cy.get(selector).contains(label);
  }

  private selectItemInGroup(groupLabel: string, itemLabel: string) {

    if (groupLabel === 'ungrouped-items') {
      const groupSelector = ` ${this.htmlTag} ${this.selectByComponentId('ungrouped-items')}`;
      return cy.get(groupSelector)
        .parent()
        .find(this.selectByComponentId('item'))
        .contains(itemLabel);
    } else {
      const groupSelector = `${this.htmlTag} ${this.selectByComponentId('group')}`;
      return cy.get(groupSelector)
        .contains(groupLabel)
        .parent()
        .find(this.selectByComponentId('item'))
        .contains(itemLabel);
    }


  }

   selectItem(label: string): Chainable {
    const selector = `
    ${this.htmlTag}
    ${this.selectByComponentId('item')}
    `;

      return cy.get(selector).contains(label);

  }


}

export class AsyncGroupedMultiListElement extends GroupedMultiListElement {

  constructor(public url: string) {
    super();
  }

  toggleOneItem(label: string, response: GridResponse = {dataRows: []}) {
    let mock: HttpMock<GridResponse>;
    if (this.url) {
      mock = new HttpMockBuilder<GridResponse>(this.url).post().responseBody(response).build();
    }
    super.toggleOneItem(label);
    mock.wait();
  }
}

export class SearchOnTagElement extends AsyncGroupedMultiListElement {

  constructor(public url: string) {
    super(url);
  }

  changeOperation(label: string, response: GridResponse = {dataRows: []}) {
    let mock: HttpMock<GridResponse>;
    if (this.url) {
      mock = new HttpMockBuilder<GridResponse>(this.url).post().responseBody(response).build();
    }
    const selector = `
    ${this.htmlTag}
    ${this.selectByComponentId('operation')}
    nz-select
    `;

    // open the select menu
    cy.get(selector).click();

    // click on item
    cy.get('nz-option-item')
      .contains(label)
      .click();

    mock.wait();

    cy.get('nz-option-item')
      .contains(label)
      .should('not.exist');
  }

  assertOperationChosen(label: string) {
    const selector = `
    ${this.htmlTag}
    ${this.selectByComponentId('operation')}
    nz-select
    `;
    cy.get(selector).should('contain.text', label);
  }
}
