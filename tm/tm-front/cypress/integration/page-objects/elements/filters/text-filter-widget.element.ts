export class TextFilterWidgetElement {

  private selectBaseComponent() {
    return `
    sqtm-core-edit-filter-text
    [data-test-component-id="edit-filter-text"]
    `;
  }

  private selectByComponentId(id: string) {
    return `
    sqtm-core-edit-filter-text
    [data-test-component-id="edit-filter-text"]
    [data-test-component-id="${id}"]
    `;
  }

  constructor() {
  }

  assertExist() {
    cy.get(this.selectBaseComponent()).should('exist');
    cy.get(this.selectByComponentId('update-button')).should('exist');
    cy.get(this.selectByComponentId('update-button')).should('contain.text', 'Mettre à jour');
    cy.get(this.selectByComponentId('cancel-button')).should('exist');
    cy.get(this.selectByComponentId('cancel-button')).should('contain.text', 'Annuler');
  }

  typeAndConfirm(input: string) {
    cy.get(this.selectByComponentId('input')).type(input);
    cy.get(this.selectByComponentId('update-button')).click();
  }

  assertNotExist() {
    cy.get(this.selectBaseComponent()).should('not.exist');
  }

  cancel() {
    cy.get(this.selectByComponentId('cancel-button')).click();
  }

  private selectOperationSelector() {
    return `
    sqtm-core-operation-selector
    `;
  }

  changeOperation(label: string) {
    const selector = `
    ${this.selectOperationSelector()}
    nz-select
    `;

    // open the select menu
    cy.get(selector).click();

    // click on item
    cy.get('nz-option-item')
      .contains(label)
      .click();
  }

}
