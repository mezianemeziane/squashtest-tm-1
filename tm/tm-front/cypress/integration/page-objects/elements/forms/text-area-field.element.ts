import {TextFieldElement} from './TextFieldElement';

export class TextAreaFieldElement extends TextFieldElement {

  constructor(fieldName: string) {
    super(fieldName);
  }

  get selector(): string {
    return `
    sqtm-core-text-area-field
    [data-test-field-name="${this.fieldName}"]`;
  }

  get inputSelector() {
    return `
    ${this.selector}
    textarea
    `;
  }
}
