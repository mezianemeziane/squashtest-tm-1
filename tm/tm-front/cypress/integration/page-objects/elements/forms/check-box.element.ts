import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {AbstractFormFieldElement, ElementSelectorFactory} from './abstract-form-field.element';

export class CheckBoxElement extends AbstractFormFieldElement {

  constructor(selectorOrFieldId: ElementSelectorFactory | string, private readonly url?: string) {
    super(selectorOrFieldId);
  }

  checkState(expected: boolean) {
    this.selector.find('input').should(expected ? 'be.checked' : 'not.be.checked');
  }

  toggleState(response?: any) {
    if (this.url != null) {
      const mock = new HttpMockBuilder(this.url).post().responseBody(response).build();
      this.click();
      mock.wait();
    } else {
      this.click();
    }
  }

  click() {
    this.selector.click();
  }
}
