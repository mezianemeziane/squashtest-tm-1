import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {MultipleMilestonePickerDialogElement} from '../dialog/multiple-milestone-picker-dialog.element';
import {AbstractFormFieldElement, ElementSelectorFactory} from './abstract-form-field.element';

export class MilestonesTagFieldElement extends AbstractFormFieldElement {
  private openDialogSelector = '[data-test-button-id="open-milestone-dialog"]';
  private url: string;

  constructor(selectorOrFieldId: ElementSelectorFactory | string, url: string) {
    super(selectorOrFieldId);
    this.url = url;
  }

  openMilestoneDialog(): MultipleMilestonePickerDialogElement {
    this.selector.find(this.openDialogSelector).click();
    return new MultipleMilestonePickerDialogElement();
  }

  removeMilestone(milestoneName: string) {
    const mock = new HttpMockBuilder(this.url).post().build();
    this.selector.contains(`nz-tag`, milestoneName).find('i[nztype="close"]').click();
    mock.wait();
  }

  confirm() {
    const mock = new HttpMockBuilder(this.url).post().build();
    cy.get('sqtm-app-milestone-dialog [data-test-dialog-button-id="confirm"]').click();
    mock.wait();
  }

  cancel() {
    cy.get('sqtm-app-milestone-dialog [data-test-dialog-button-id="cancel"]').click();
  }

  // Checks the tags currently in the field
  checkMilestones(...values: string[]) {
    for (const value of values) {
      this.selector.contains(`nz-tag`, value).should('exist');
    }

    this.selector.find(`nz-tag`).should('have.length', values.length);
  }

}
