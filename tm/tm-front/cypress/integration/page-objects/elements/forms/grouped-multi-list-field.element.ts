import {GroupedMultiListElement} from '../filters/grouped-multi-list.element';

export class GroupedMultiListFieldElement {
  public readonly multiList: GroupedMultiListElement;

  constructor(public readonly customSelector: string) {
    this.multiList = new GroupedMultiListElement();
  }

  showMultiList(): void {
    cy.get(this.buildSelector())
      .click();
  }

  buildSelector(): string {
    return `sqtm-core-optional-grouped-multi-list${this.customSelector}`;
  }
}
