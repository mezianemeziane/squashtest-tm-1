import {AbstractFormFieldElement, ElementSelectorFactory} from './abstract-form-field.element';

export class CompactEditableNumericFieldElement extends AbstractFormFieldElement {

  constructor(selectorOrFieldId: ElementSelectorFactory | string) {
    super(selectorOrFieldId);
  }

  setValue(newValue: number) {
    this.selector.find('span').click();
    this.selector.find('input').clear();
    this.selector.find('input').type(newValue.toString());
  }

  confirm(): void {
    this.selector.find('input').type('{enter}');
  }
}
