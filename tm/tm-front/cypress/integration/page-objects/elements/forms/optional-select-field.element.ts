import Chainable = Cypress.Chainable;
import {selectByDataTestComponentId} from '../../../utils/basic-selectors';

export class OptionalSelectField {

  constructor(private fieldName: string, private parentSelector = '') {
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }

  check() {
    cy.get(`${this.buildSelector()} ${selectByDataTestComponentId('activate-checkbox')} input`).click();
  }

  private getOption(optionName: string): Chainable<any> {
    return cy.contains(`nz-option-item`, optionName);
  }

  // Shows the dropdown and click on an option
  selectValue(newValue: string) {
    cy.get(this.buildSelector()).find('nz-select').click();
    this.getOption(newValue).click();
  }

  buildSelector() {
    return `${this.parentSelector} [data-test-field-id=${this.fieldName}]`;
  }
}
