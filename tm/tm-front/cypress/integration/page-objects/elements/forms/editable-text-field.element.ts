import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {AbstractFormFieldElement, ElementSelectorFactory} from './abstract-form-field.element';

export class EditableTextFieldElement extends AbstractFormFieldElement {

  constructor(selectorOrFieldId: ElementSelectorFactory | string,
              private readonly url: string = '') {
    super(selectorOrFieldId);
  }

  setAndConfirmValue(newValue: string, response?) {
    this.setValue(newValue);
    this.confirm(newValue, response);
  }

  setValue(newValue: string) {
    this.selector.find('span').click();
    this.selector.find('input').clear();
    this.selector.find('input').type(newValue);
  }

  confirm(expectedContent?: string, response?): void {
    // Click on button
    if (this.url != null) {
      let mock;
      if (response) {
        mock = new HttpMockBuilder(this.url).post().responseBody(response).build();
      } else {
        mock = new HttpMockBuilder(this.url).post().build();
      }
      this.selector.find('button').first().click();
      mock.wait();
    } else {
      this.selector.find('button').first().click();
    }

    if (expectedContent != null) {
      this.checkContent(expectedContent);
    }
    this.checkEditMode(false);
  }

  cancel(): void {
    this.selector.find('button').eq(1).click();
    this.checkEditMode(false);
  }

  checkContent(value: string): void {
    this.selector.find('span').should('contain.text', value);
  }

  checkEditMode(shouldBeInEditMode: boolean) {
    const chainer = shouldBeInEditMode ? 'exist' : 'not.exist';
    this.selector.find('button').should(chainer);
  }

  checkPlaceholder() {
    this.checkContent('(Cliquer pour éditer...)');
  }

  assertIsEditable() {
    this.findDivWrapper()
      .should('have.class', 'editable');
  }

  assertIsNotEditable() {
    this.findDivWrapper()
      .should('not.have.class', 'editable');
  }

  private findDivWrapper() {
    return this.selector
      .find('div')
      .should('exist');
  }
}
