import {TreeElement} from '../grid/grid.element';
import {DataRow, GridResponse, Identifier} from '../../../model/grids/data-row.type';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';

export class ConfirmInterProjectMove {
  constructor(protected dialogId: string, private  url?: string) {
  }

  cancel() {
    const buttonSelector = this.buildButtonSelector('cancel');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  confirm(targetId: Identifier, rowsToRefresh: string = '*', dataRows: DataRow[] = []) {
    // two mocks are required, one for drop, one for refreshing the concerned part of the tree
    const dropUrl = `${this.url}/${targetId}/content/move`;
    const dropMock = new HttpMockBuilder<any>(dropUrl).post().build();
    const refreshUrl = `${this.url}/${rowsToRefresh}/refresh`;
    const refreshMock = new HttpMockBuilder<GridResponse>(refreshUrl).responseBody({
      dataRows,
      count: dataRows.length
    }).post().build();
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).click();
    dropMock.wait();
    refreshMock.wait();
  }

  protected buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }
}
