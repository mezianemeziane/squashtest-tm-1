import {GridResponse} from '../../../model/grids/data-row.type';

export abstract class DeleteConfirmDialogElement {
  protected constructor(protected dialogId: string) {
  }

  abstract deleteForSuccess(response?: GridResponse);

  abstract deleteForFailure(response: any);

  cancel() {
    const buttonSelector = this.buildButtonSelector('cancel');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  checkWarningMessages(expectedMessages: string[]) {
    cy.get(this.buildDeletionWarningSelector())
      .should('exist')
      .find('li')
      .then(warnings => {
        cy.wrap(warnings.length).should('equal', expectedMessages.length);
        warnings.each((index, warning) => {
          cy.wrap(warning).should('contain.text', expectedMessages[index]);
        });
      });
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  buildMessageSelector(): string {
    return `${this.buildSelector()} [data-test-dialog-message]`;
  }

  buildDeletionWarningSelector(): string {
    return `${this.buildSelector()} [data-test-component-id=deletion-warnings]`;
  }

  clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  protected buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }
}
