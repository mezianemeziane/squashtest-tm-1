import {TreeElement} from './grid.element';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {GridResponse} from '../../../model/grids/data-row.type';
import {selectByDataTestComponentId} from '../../../utils/basic-selectors';

export class GridTestCaseScopeElement {

  tree: TreeElement;

  constructor(nodes: GridResponse = {dataRows: []}, private researchGridUrl: string) {
    const mock = new HttpMockBuilder<GridResponse>('test-case-tree').post().responseBody(nodes).build();
    this.tree = new TreeElement('test-case-tree-picker', mock, 'test-case-tree');
  }

  confirm(rows: GridResponse = {dataRows: [], count: 0}) {
    const mock = new HttpMockBuilder<GridResponse>(this.researchGridUrl).post().responseBody(rows).build();
    cy.get(selectByDataTestComponentId('confirm-scope')).click();
    mock.wait();
    cy.clickVoid();
  }
}
