import {GridElement} from './grid.element';

export abstract class GridDialogPicker {

  public grid: GridElement;

  protected constructor(gridId: string) {
    this.grid = new GridElement(gridId);
  }

  fillSearchInput(value: string) {
    const searchInput = cy.get('nz-input-group input');
    searchInput.should('exist');
    searchInput.clear().type(value);
  }


  confirm() {
    this.clickButton('confirm');
    this.assertNotExist();
  }

  cancel() {
    this.clickButton('cancel');
    this.assertNotExist();
  }

  clickButton(buttonId: string) {
    cy.get(`[data-test-dialog-button-id=${buttonId}]`).click();
  }

  assertExist() {
    cy.get('sqtm-core-project-picker').should('exist');
    this.grid.assertExist();
  }

  assertNotExist() {
    cy.get('sqtm-core-project-picker').should('not.exist');
  }

  protected assertRowsAreSelected(ids: number[]) {
    ids.forEach(id => {
      this.grid.assertRowIsSelected(id);
    });
  }

  protected toggleRow(id: number) {
    this.grid.getCell(id, 'select-row-column').checkBoxRender().toggleState();
  }
}
