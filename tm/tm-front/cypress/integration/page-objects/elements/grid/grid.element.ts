import {
  buildCellSelector,
  GridCellSelectorBuilder,
  GridHeaderRowElement,
  GridRowElement,
  GridSelectorBuilder,
  TreeNodeRendererSelectorBuilder
} from '../../../utils/grid-selectors.builder';
import {Page} from '../../pages/page';
import {HttpMock, HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {TestCaseLibraryViewPage} from '../../pages/test-case-workspace/test-case/test-case-library-view.page';
import {IterationViewPage} from '../../pages/campaign-workspace/iteration/iteration-view.page';
import {CampaignLibraryViewPage} from '../../pages/campaign-workspace/campaign-library/campaign-library.page';
import {CampaignViewPage} from '../../pages/campaign-workspace/campaign/campaign-view.page';
import {CampaignFolderViewPage} from '../../pages/campaign-workspace/campaign-folder/campaign-folder.page';
import {
  DataRow,
  GridResponse,
  GridViewportName,
  Identifier,
  SquashTmDataRowType
} from '../../../model/grids/data-row.type';
import {TestCaseViewPage} from '../../pages/test-case-workspace/test-case/test-case-view.page';
import {GridFilterPanelElement} from './grid-filter-panel.element';
import {TestCaseFolderViewPage} from '../../pages/test-case-workspace/test-case-folder/test-case-folder-view.page';
import {selectByDataTestComponentId} from '../../../utils/basic-selectors';
import {ConfirmInterProjectMove} from '../dialog/confirm-inter-project-move';
import {RequirementVersionDetails} from '../../../model/requirements/requirement-version-details';
import {RequirementVersionDetailsPanel} from '../panels/requirement-version-details.panel';
import {RequirementVersionCoverage} from '../../../model/test-case/requirement-version-coverage-model';
import {SimpleDeleteConfirmDialogElement} from '../dialog/simple-delete-confirm-dialog.element';
import {RequirementViewPage} from '../../pages/requirement-workspace/requirement/requirement-view.page';
import {RequirementLibraryViewPage} from '../../pages/requirement-workspace/requirement-library/requirement-library-view.page';
import {RequirementFolderViewPage} from '../../pages/requirement-workspace/requirement-folder/requirement-folder-view.page';
import {ChartDefinitionViewPage} from '../../pages/custom-report-workspace/chart-definition-view.page';
import {TestSuiteViewPage} from '../../pages/campaign-workspace/test-suite/test-suite-view.page';
import {DashboardViewPage} from '../../pages/custom-report-workspace/dashboard-view.page';
import {CustomReportLibraryViewPage} from '../../pages/custom-report-workspace/custom-report-library-view.page';
import {CustomReportFolderViewPage} from '../../pages/custom-report-workspace/custom-report-folder-view.page';
import {mockIterationModel, mockTestSuiteModel} from '../../../data-mock/iteration.data-mock';
import {
  mockTestCaseFolderModel,
  mockTestCaseLibraryModel,
  mockTestCaseModel
} from '../../../data-mock/test-case.data-mock';
import {
  mockCampaignFolderModel,
  mockCampaignLibraryModel,
  mockCampaignModel
} from '../../../data-mock/campaign.data-mock';
import {
  mockRequirementFolderModel,
  mockRequirementLibraryModel,
  mockRequirementVersionModel
} from '../../../data-mock/requirements.data-mock';
import {
  mockChartDefinitionModel,
  mockCustomReportDashboardModel,
  mockCustomReportFolderModel,
  mockCustomReportLibraryModel
} from '../../../data-mock/custom-report.data-mock';
import PositionType = Cypress.PositionType;
import Chainable = Cypress.Chainable;

export class GridElement {

  protected selectorBuilder: GridSelectorBuilder;
  private currentRequestMock: HttpMock<GridResponse>;
  public filterPanel: GridFilterPanelElement;

  public static createGridElement(gridId: string, url?: string, initialServerData?: GridResponse) {

    let requestMock;
    if (url) {
      requestMock = new HttpMockBuilder<GridResponse>(url)
        .post()
        .responseBody(initialServerData)
        .build();
    }

    if (requestMock) {
      return new GridElement(gridId, requestMock, url);
    } else {
      return new GridElement(gridId);
    }
  }

  constructor(protected readonly gridId: string, protected readonly initialMock?: HttpMock<GridResponse>, public readonly url?: string) {
    this.selectorBuilder = new GridSelectorBuilder(gridId);
    this.filterPanel = new GridFilterPanelElement(gridId, url);
  }

  waitInitialDataFetch() {
    this.initialMock.wait();
  }

  declareRefreshData(gridResponse?: GridResponse) {
    this.currentRequestMock = new HttpMockBuilder<GridResponse>(this.url)
      .post()
      .responseBody(gridResponse)
      .build();
  }

  waitForRefresh() {
    this.currentRequestMock.wait();
  }

  assertExist() {
    cy.get(this.selectorBuilder.buildSelector()).should('have.length', 1);
  }

  assertRowCount(expectedCount: number, viewPortName: GridViewportName = 'mainViewport') {
    const selector = `sqtm-core-grid-row`;
    cy.get(`[data-test-grid-id="${this.gridId}"] [data-test-viewport-name="${viewPortName}"] ${selector}`)
      .should('have.length', expectedCount);
  }

  assertColumnCount(expectedCount: number) {
    const selector = 'div.sqtm-grid-header-cell';
    cy.get(`[data-test-grid-id="${this.gridId}"] ${selector}`).should('have.length', expectedCount);

  }

  assertRowExist(rowId: Identifier, viewportName: GridViewportName = 'mainViewport') {
    this.selectorBuilder.viewport(viewportName).row(rowId).assertExist();
  }

  assertRowNotExist(rowId: Identifier, viewportName: GridViewportName = 'mainViewport') {
    this.selectorBuilder.viewport(viewportName).row(rowId).assertNotExist();
  }

  assertRowHasParent(rowId: Identifier, expectedParent: Identifier, viewportName: GridViewportName = 'mainViewport') {
    this.selectorBuilder.viewport(viewportName).row(rowId).assertHasParent(expectedParent);
  }

  assertRowIsSelected(rowId: Identifier, viewportName: GridViewportName = 'mainViewport') {
    this.selectorBuilder.viewport(viewportName).row(rowId).assertIsSelected();
  }

  getRow(rowId: Identifier, viewportName: GridViewportName = 'mainViewport'): GridRowElement {
    return this.selectorBuilder.viewport(viewportName).row(rowId);
  }

  getCell(rowId: Identifier, cellId: string, viewportName: GridViewportName = 'mainViewport'): GridCellSelectorBuilder {
    return this.selectorBuilder.viewport(viewportName).row(rowId).cell(cellId);
  }

  getHeaderRow(viewportName: GridViewportName = 'headerMainViewport'): GridHeaderRowElement {
    return this.selectorBuilder.viewport(viewportName).headerRow();
  }

  findRowId(cellId: string,
            content: string, viewportName: GridViewportName = 'mainViewport'): Chainable {
    return this.selectorBuilder.viewport(viewportName).findRows()
      .find(buildCellSelector(cellId))
      .contains('span,a', content)
      .closest('sqtm-core-grid-row')
      .find('[data-test-row-id]')
      .invoke('attr', 'data-test-row-id');
  }

  findRowIdNoWithLink(cellId: string,
                      content: string, viewportName: GridViewportName = 'mainViewport'): Chainable {
    return this.selectorBuilder.viewport(viewportName).findRows()
      .find(buildCellSelector(cellId))
      .contains('a', content)
      .closest('sqtm-core-grid-row')
      .find('[data-test-row-id]')
      .invoke('attr', 'data-test-row-id');
  }

  findRowIdWithProjectName(cellId: string,
                           content: string, viewportName: GridViewportName = 'mainViewport', projectName?: string): Chainable {
    return this.selectorBuilder.viewport(viewportName).findRows()
      .find(buildCellSelector(cellId))
      .contains('span', projectName)
      .parents('sqtm-core-grid-row-wrapper')
      .next('sqtm-core-grid-row-wrapper')
      .find(buildCellSelector(cellId))
      .contains('span', content)
      .closest('sqtm-core-grid-row')
      .find('[data-test-row-id]')
      .invoke('attr', 'data-test-row-id');
  }

  selectRow(rowId: Identifier, cell: string = '#', viewportName: GridViewportName = 'mainViewport') {
    this.selectorBuilder.viewport(viewportName).row(rowId).cell(cell).indexRenderer().select();
  }

  toggleRow(rowId: Identifier, cell: string = '#', viewportName: GridViewportName = 'mainViewport') {
    this.selectRows([rowId], cell, viewportName);
  }

  expendSelectionToRow(rowId: Identifier, cell: string = '#', viewportName: GridViewportName = 'mainViewport') {
    this.expendSelectionToRows([rowId], cell, viewportName);
  }

  selectRowsWithStickyIndexColumn(rowIds: Identifier[]) {
    this.selectRows(rowIds, '#', 'leftViewport');
  }

  selectRows(rowIds: Identifier[], cell: string = '#', viewportName: GridViewportName = 'mainViewport') {
    rowIds.forEach(rowId => {
      this.selectorBuilder.viewport(viewportName).row(rowId).cell(cell).indexRenderer().toggle();
    });
  }

  protected expendSelectionToRows(rowIds: Identifier[], cell: string = '#', viewportName: GridViewportName = 'mainViewport') {
    rowIds.forEach(rowId => {
      this.selectorBuilder.viewport(viewportName).row(rowId).cell(cell).indexRenderer().clickWithShift();
    });
  }

  scrollPosition(position: PositionType, viewport: string) {
    cy.get(`${this.selectorBuilder.buildSelector()} [data-test-scroll-viewport-id=${viewport}]`).scrollTo(position);
  }

  /**
   * Looks for a cell knowing its id (same as columnId) and a contained string value, find the nearest selection ('#') column and click
   * on it, using the CSS styling update as a "done guard". Assumes the expected text is in a "span" element so it may not be compatible
   * with all cell renderers.
   *
   * Based on `cy.contains()` so the actual cell found may have more content that what's expected. Also only the first element found
   * will be selected.
   *
   * @param cellId same as columnId
   * @param content a string that should be contained in the cell
   * @param viewportName the viewport of index column
   */
  selectRowWithMatchingCellContent(cellId: string, content: string, viewportName: GridViewportName = 'leftViewport') {
    // Look for cell with provided content
    cy.get(`sqtm-core-grid-cell[data-test-cell-id="${cellId}"]`)
      .contains('span,a', content)
      .closest('sqtm-core-grid-row')
      .find('[data-test-row-id]')
      .invoke('attr', 'data-test-row-id')
      .then(rowId => {
        this.selectRow(rowId, '#', viewportName);
        this.assertRowIsSelected(rowId);
      });
  }

  selectRowWithMatchingLinkCellContent(cellId: string, content: string, viewportName: GridViewportName = 'leftViewport') {
    // Look for cell with provided content
    cy.get(`sqtm-core-grid-cell[data-test-cell-id="${cellId}"]`)
      .contains('a', content)
      .closest('sqtm-core-grid-row')
      .find('[data-test-row-id]')
      .invoke('attr', 'data-test-row-id')
      .then(rowId => {
        this.selectRow(rowId, '#', viewportName);
        this.assertRowIsSelected(rowId);
      });
  }

  /**
   * Iteratively select rows containing a cell that match the provided string contents.
   *
   * @see selectRowWithMatchingCellContent
   *
   * @param cellId same as columnId
   * @param contents an array of strings that should be contained in the cells
   */
  public selectRowsWithMatchingCellContent(cellId: string, contents: string[]): void {
    const first = contents.pop();

    // Select the first row
    this.selectRowWithMatchingCellContent(cellId, first);

    // Press the CTRL key
    cy.get('body').type('{ctrl}', {release: false});

    // Select other rows
    for (const content of contents) {
      this.selectRowWithMatchingCellContent(cellId, content);
    }
  }

  public selectRowsWithMatchingLinkCellContent(cellId: string, contents: string[]): void {
    const first = contents.pop();

    // Select the first row
    this.selectRowWithMatchingLinkCellContent(cellId, first);

    // Press the CTRL key
    cy.get('body').type('{ctrl}', {release: false});

    // Select other rows
    for (const content of contents) {
      this.selectRowWithMatchingLinkCellContent(cellId, content);
    }
  }

  checkBoxInRowWithMatchingCellContentInPickerGrid(cellId: string, content: string) {
    // Look for cell with provided content
    cy.get(`sqtm-core-grid-cell[data-test-cell-id="${cellId}"]`)
      .contains('span', content)
      .closest('sqtm-core-grid-cell')

      // Look for the selection cell for this row
      .siblings('sqtm-core-grid-cell[data-test-cell-id="select-row-column"]')
      .find('input')

      // Do click
      .click()

      // Assert that "selected" class was added
      .closest('sqtm-core-grid-row')
      .get('div')
      .should('have.class', 'selected');
  }

  checkBoxInRowsWithMatchingCellContentInPickerGrid(cellId: string, contents: string[]): void {
    const first = contents.pop();

    // Select the first row
    this.checkBoxInRowWithMatchingCellContentInPickerGrid(cellId, first);

    // Press the CTRL key
    cy.get('body').type('{ctrl}', {release: false});

    // Select other rows
    for (const content of contents) {
      this.checkBoxInRowWithMatchingCellContentInPickerGrid(cellId, content);
    }
  }

  /**
   * Filter by text column.
   * @param cell id of the column
   * @param filterValue the new filter value
   * @param gridResponse an optional grid response after filtering
   */
  filterByTextColumn(cell: string, filterValue: string, gridResponse?: GridResponse) {
    if (gridResponse) {
      this.declareRefreshData(gridResponse);
    }
    const widget = this.selectorBuilder.headerRow().header(cell).openTextFilter();
    widget.assertExist();
    widget.typeAndConfirm(filterValue);
    if (gridResponse) {
      this.waitForRefresh();
    }
  }
}

export class TreeElement extends GridElement {
  // Name of the column that is rendered with the node renderer
  private readonly treeNodeRendererColumnName = 'NAME';

  public static createTreeElement(gridId: string, url?: string, initialServerData?: GridResponse) {
    const mock = url ? new HttpMockBuilder<GridResponse>(url).post().responseBody(initialServerData).build() : null;
    return new TreeElement(gridId, mock, url);
  }

  constructor(gridId: string, mock?: HttpMock<GridResponse>, url?: string) {
    super(gridId, mock, url);
  }

  private getTreeNodeCell(rowId: Identifier, viewportName: GridViewportName = 'mainViewport'): TreeNodeRendererSelectorBuilder {
    return this.selectorBuilder.viewport(viewportName).row(rowId).cell('NAME').treeNodeRenderer();
  }

  openNodeIfClosed(rowId: Identifier) {
    const treeNodeRendererSelectorBuilder = this.getTreeNodeCell(rowId) as TreeNodeRendererSelectorBuilder;
    treeNodeRendererSelectorBuilder.getFoldClass().then(foldClass => {
      cy.log('fold ' + foldClass);
      const regexMinus = RegExp('.minus');
      const minus = regexMinus.test(foldClass.toString());
      console.log(regexMinus.test(foldClass.toString()));
      if (minus) {
        cy.log('node opened');
      } else {
        this.openNode(rowId);
      }
    });
  }

  openNode(rowId: Identifier, dataRows: DataRow[] = []) {
    const url = `${this.initialMock.url}/${rowId}/content`;
    const response: GridResponse = {
      count: dataRows.length,
      dataRows
    };
    const mock = new HttpMockBuilder<GridResponse>(url).responseBody(response).build();
    this.getTreeNodeCell(rowId).toggle();
    mock.wait();
  }

  closeNode(rowId: Identifier) {
    this.getTreeNodeCell(rowId).toggle();
  }

  assertNodeIsOpen(rowId: Identifier) {
    this.getTreeNodeCell(rowId).assertIsOpened();
  }

  assertNodeExist(rowId: Identifier, viewportName: GridViewportName = 'mainViewport') {
    return this.selectorBuilder.viewport(viewportName).row(rowId).cell('NAME').assertExist();
  }

  assertNodeNotExist(rowId: Identifier, viewportName: GridViewportName = 'mainViewport') {
    return this.selectorBuilder.viewport(viewportName).row(rowId).assertNotExist();
  }

  assertNodeTextContains(rowId: Identifier, expectedText: string) {
    this.getTreeNodeCell(rowId).assertContainText(expectedText);
  }

  assertNodeIsClosed(rowId: Identifier) {
    this.getTreeNodeCell(rowId).assertIsClosed();
  }

  assertNodeIsLeaf(rowId: Identifier) {
    this.getTreeNodeCell(rowId).assertIsLeaf();
  }

  assertNodeIsSelected(rowId: Identifier) {
    this.getRow(rowId).cell('NAME').treeNodeRenderer().assertIsSelected();
  }

  assertNodeIsNotSelected(rowId: Identifier) {
    this.getRow(rowId).cell('NAME').treeNodeRenderer().assertIsNotSelected();
  }


  assertNodeIsNotSelectable(rowId: Identifier, viewportName: GridViewportName = 'mainViewport') {
    return this.selectorBuilder.viewport(viewportName).row(rowId).cell('NAME').treeNodeRenderer().assertIsNotSelectable();
  }


  assertNodeOrderByName(expectedNames: string[]) {
    this.extractNamesOrdered().should('have.ordered.members', expectedNames);
  }

  beginDragAndDrop(rowId: Identifier) {
    this.getTreeNodeCell(rowId).beginDragAndDrop();
  }

  /**
   * Select a node in tree pickers, aka no page will open.
   * @param rowId id of the selected row
   * @param viewportName grid viewport identifier
   */
  pickNode(rowId: Identifier, viewportName: GridViewportName = 'mainViewport') {
    super.selectRow(rowId, this.treeNodeRendererColumnName, viewportName);
  }

  selectNode<T extends Page>(rowId: Identifier, model?: any, viewportName: GridViewportName = 'mainViewport') {
    let page: T;
    const entityReference = toEntityRowReference(rowId);
    const id = entityReference.id;

    model = model ?? getDefaultModel(entityReference.entityType);

    switch (entityReference.entityType) {
      case SquashTmDataRowType.Iteration:
        new HttpMockBuilder(`iteration-view/${id}`).responseBody(model).build();
        page = new IterationViewPage(id) as unknown as T;
        break;
      case SquashTmDataRowType.TestSuite:
        new HttpMockBuilder(`test-suite-view/${id}`).responseBody(model).build();
        page = new TestSuiteViewPage(id) as unknown as T;
        break;
      case SquashTmDataRowType.TestCaseLibrary:
        new HttpMockBuilder(`test-case-library-view/${id}`).responseBody(model).build();
        page = new TestCaseLibraryViewPage(id) as unknown as T;
        break;
      case SquashTmDataRowType.CampaignLibrary:
        new HttpMockBuilder(`campaign-library-view/${id}`).responseBody(model).build();
        page = new CampaignLibraryViewPage(id) as unknown as T;
        break;
      case SquashTmDataRowType.Campaign:
        new HttpMockBuilder(`campaign-view/${id}`).responseBody(model).build();
        page = new CampaignViewPage(id) as unknown as T;
        break;
      case SquashTmDataRowType.CampaignFolder:
        new HttpMockBuilder(`campaign-folder-view/${id}`).responseBody(model).build();
        page = new CampaignFolderViewPage(id) as unknown as T;
        break;
      case SquashTmDataRowType.TestCase:
        new HttpMockBuilder(`test-case-view/${id}`).responseBody(model).build();
        page = new TestCaseViewPage(id) as unknown as T;
        break;
      case SquashTmDataRowType.TestCaseFolder:
        new HttpMockBuilder(`test-case-folder-view/${id}`).responseBody(model).build();
        page = new TestCaseFolderViewPage(id) as unknown as T;
        break;
      case SquashTmDataRowType.Requirement:
        new HttpMockBuilder(`requirement-view/current-version/${id}`).responseBody(model).build();
        page = new RequirementViewPage(id, model.id) as unknown as T;
        break;
      case SquashTmDataRowType.RequirementLibrary:
        new HttpMockBuilder(`requirement-library-view/${id}`).responseBody(model).build();
        page = new RequirementLibraryViewPage(id) as unknown as T;
        break;
      case SquashTmDataRowType.RequirementFolder:
        new HttpMockBuilder(`requirement-folder-view/${id}`).responseBody(model).build();
        page = new RequirementFolderViewPage(id) as unknown as T;
        break;
      case SquashTmDataRowType.CustomReportLibrary:
        new HttpMockBuilder(`custom-report-library-view/${id}`).responseBody(model).build();
        page = new CustomReportLibraryViewPage(id) as unknown as T;
        break;
      case SquashTmDataRowType.CustomReportFolder:
        new HttpMockBuilder(`custom-report-folder-view/${id}`).responseBody(model).build();
        page = new CustomReportFolderViewPage(id) as unknown as T;
        break;
      case SquashTmDataRowType.ChartDefinition:
        new HttpMockBuilder(`chart-definition-view/${id}`).responseBody(model).build();
        page = new ChartDefinitionViewPage(id) as unknown as T;
        break;
      case SquashTmDataRowType.CustomReportDashboard:
        new HttpMockBuilder(`dashboard-view/${id}`).responseBody(model).build();
        page = new DashboardViewPage(id) as unknown as T;
        break;
      default:
        throw Error('Unable to find a page factory for type ' + entityReference.entityType);
    }
    super.selectRow(rowId, this.treeNodeRendererColumnName, viewportName);
    page.checkDataFetched();
    return page;
  }

  selectNodes<T extends Page>(rowIds: Identifier[], model?: any, viewportName: GridViewportName = 'mainViewport') {
    this.selectNode(rowIds[0], model, viewportName);
    super.selectRows(rowIds.slice(1), this.treeNodeRendererColumnName, viewportName);
  }

  addNodeToSelection<T extends Page>(rowId: Identifier, model?: any, viewportName: GridViewportName = 'mainViewport') {
    super.toggleRow(rowId, this.treeNodeRendererColumnName, viewportName);
  }

  // shift key held while selecting nodes
  expendSelectionToNode<T extends Page>(rowId: Identifier, model?: any, viewportName: GridViewportName = 'mainViewport') {
    super.expendSelectionToRow(rowId, this.treeNodeRendererColumnName, viewportName);
  }

  /**
   * Drag over the center of a given row. A dragAndDrop should have been initialized before.
   * @param rowId id of the row
   */
  dragOverCenter(rowId: Identifier) {
    this.getTreeNodeCell(rowId).dragOverCenter();
  }

  /**
   * Drag over the top part of a given row. A dragAndDrop should have been initialized before.
   * @param rowId id of the row
   */
  dragOverTopPart(rowId: Identifier) {
    this.getTreeNodeCell(rowId).dragOverTopPart();
  }

  /**
   * Drag over the bottom part of a given row. A dragAndDrop should have been initialized before.
   * @param rowId id of the row
   */
  dragOverBottomPart(rowId: Identifier) {
    this.getTreeNodeCell(rowId).dragOverBottomPart();
  }

  // drop mock url like :
  // /backend/test-case-tree/TestCaseFolder-2/content/move
  // refresh mock url like :
  // /backend/test-case-tree/TestCaseLibrary-1,TestCaseFolder-2/refresh
  drop(targetId: Identifier, rowsToRefresh: string = '*', dataRows: DataRow[] = []) {
    // two mocks are required, one for drop, one for refreshing the concerned part of the tree
    const dropUrl = `${this.initialMock.url}/${targetId}/content/move`;
    const dropMock = new HttpMockBuilder<any>(dropUrl).post().build();
    const refreshUrl = `${this.initialMock.url}/refresh`;
    const refreshMock = new HttpMockBuilder<GridResponse>(refreshUrl).responseBody({
      dataRows,
      count: dataRows.length
    }).post().build();
    this.getTreeNodeCell(targetId).drop();
    dropMock.wait();
    refreshMock.wait();
  }

  dropIntoOtherProject(targetId: Identifier): ConfirmInterProjectMove {
    this.getTreeNodeCell(targetId).drop();
    return new ConfirmInterProjectMove('confirm-inter-project-move', this.url);
  }

  cancelDnd() {
    cy.get('body').type('{esc}');
  }

  suspendDnd() {
    cy.get('body').trigger('mousemove', 'topLeft');
  }

  assertContainerIsDndTarget(rowId: Identifier) {
    this.getRow(rowId).assertIsDndTarget();
  }

  assertContainerIsNotDndTarget(rowId: Identifier) {
    this.getRow(rowId).assertIsNotDndTarget();
  }

  assertDragAndDropTargetIsVisible(expectedIndex: number) {
    const selector = selectByDataTestComponentId('grid-dnd-placeholder');
    cy.get(selector)
      .should('exist')
      .should('have.attr', 'data-test-index', expectedIndex.toString());
  }

  assertDragAndDropTargetIsNotVisible() {
    const selector = selectByDataTestComponentId('grid-dnd-placeholder');
    cy.get(selector).should('not.exist');
  }

  assertDndPlaceholderContains(rowId: string | number, expectedText: string) {
    cy.get(`[data-test-grid-row-dragged-content='${rowId}']`).should('contain.text', expectedText);
  }

  /**
   * Extract all the names of the nodes ordered by their flat order. Beware that it will ONLY extract nodes visible in
   * the virtualScroll.
   */
  private extractNamesOrdered(): Chainable<any> {
    return this.selectorBuilder.viewport().findRows()
      .find(buildCellSelector(this.treeNodeRendererColumnName))
      .find('span')
      .then(elements => {
        const names = [];
        elements.each((index, el) => {
          names.push(el.textContent);
        });
        return cy.wrap(names);
      });
  }

  assertSynchronizationStatusIconHasCorrectCssClass(rowId: Identifier, expectedCssClass: string) {
    return this.getTreeNodeCell(rowId).findRequirementFolderSynchronizationStatusIcon().should('have.class', expectedCssClass);
  }

  assertRemoteReqPerimeterStatusIconHasCorrectCssClass(rowId: Identifier, expectedCssClass: string) {
    return this.getTreeNodeCell(rowId).findRemoteReqPerimeterStatusIcon().should('have.class', expectedCssClass);
  }
}

export function toEntityRowReference(id: Identifier): EntityRowReference {
  if (typeof id === 'string') {
    const splitNode = id.split('-');
    const candidateType = splitNode[0];
    const typeElement = SquashTmDataRowType[candidateType];
    if (!typeElement) {
      throw Error(`Unable to parse id ${id}`);
    }
    return new EntityRowReference(Number.parseInt(splitNode[1], 10), typeElement);
  }
  throw Error(`Unable to parse id ${id}`);
}

export class EntityRowReference {

  constructor(public id: number, public entityType: SquashTmDataRowType) {
  }
}

export class CoverageTable extends GridElement {

  constructor() {
    super('detailed-test-step-view-coverages');
  }

  showRequirementVersionDetails(requirementVersionId: number, requirementVersionDetails?: RequirementVersionDetails) {
    const url = `detailed-test-step-view/requirement-version/${requirementVersionId}`;
    const mock = new HttpMockBuilder(url).responseBody(requirementVersionDetails).build();
    this.getRow(requirementVersionId).cell('open').openCloseRenderer().toggle();
    mock.wait();
    return new RequirementVersionDetailsPanel();
  }

  //  /backend/test-cases/3/verified-requirement-versions/1
  deleteRequirementVersion(requirementVersionId: number, testCaseId?: number, refreshedCoverages?: RequirementVersionCoverage[]) {
    const testCaseIdAsString = testCaseId ? testCaseId.toString() : '*';
    const url = `test-cases/${testCaseIdAsString}/verified-requirement-versions/${requirementVersionId}`;
    const mock = new HttpMockBuilder(url).delete().responseBody({coverages: refreshedCoverages}).build();
    this.getRow(requirementVersionId).cell('delete').iconRenderer().click();
    const dialog = new SimpleDeleteConfirmDialogElement();
    dialog.clickOnConfirmButton();
    mock.wait();
  }

}

export class CustomReportAttributeSelector extends TreeElement {
  constructor(id: string) {
    super(id);
  }

  openAllNodes() {
    cy.get(selectByDataTestComponentId('open-all-entity-types')).click();
  }

  closeAllNodes() {
    cy.get(selectByDataTestComponentId('close-all-entity-types')).click();
  }

  filterAttribute(value: string) {
    cy.get(selectByDataTestComponentId('input-filter')).type(value);
  }
}

function getDefaultModel(entityType: SquashTmDataRowType): any {
  switch (entityType) {
    case SquashTmDataRowType.RequirementLibrary:
      return mockRequirementLibraryModel();
    case SquashTmDataRowType.RequirementFolder:
      return mockRequirementFolderModel();
    case SquashTmDataRowType.Requirement:
      return mockRequirementVersionModel();

    case SquashTmDataRowType.TestCaseLibrary:
      return mockTestCaseLibraryModel();
    case SquashTmDataRowType.TestCaseFolder:
      return mockTestCaseFolderModel();
    case SquashTmDataRowType.TestCase:
      return mockTestCaseModel();

    case SquashTmDataRowType.CampaignLibrary:
      return mockCampaignLibraryModel();
    case SquashTmDataRowType.CampaignFolder:
      return mockCampaignFolderModel();
    case SquashTmDataRowType.Campaign:
      return mockCampaignModel();
    case SquashTmDataRowType.Iteration:
      return mockIterationModel();
    case SquashTmDataRowType.TestSuite:
      return mockTestSuiteModel();

    case SquashTmDataRowType.CustomReportLibrary:
      return mockCustomReportLibraryModel();
    case SquashTmDataRowType.CustomReportFolder:
      return mockCustomReportFolderModel();
    case SquashTmDataRowType.ChartDefinition:
      return mockChartDefinitionModel();
    case SquashTmDataRowType.CustomReportDashboard:
      return mockCustomReportDashboardModel();

    default:
      throw Error('Please provide a default model for ' + entityType);
  }
}
