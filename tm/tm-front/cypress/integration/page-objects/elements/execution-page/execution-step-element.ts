import {selectByDataTestComponentId} from '../../../utils/basic-selectors';
import {EditableRichTextFieldElement} from '../forms/editable-rich-text-field.element';
import {ExecutionStatusKeys} from '../../../model/level-enums/level-enum';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {GridResponse} from '../../../model/grids/data-row.type';

export class ExecutionStepElement {

  rootSelector: string;
  private editableCommentField: EditableRichTextFieldElement;

  constructor(protected stepId: string, protected stepIndex: number) {
    if (this.stepId) {
      this.rootSelector = `sqtm-app-execution-step
    div[data-test-component-id="execution-step"][data-test-execution-step-id="${stepId}"]
    `;
    } else {
      // step index + 1 because we have the prerequisite block just above the steps
      this.rootSelector = `sqtm-app-execution-step:nth-of-type(${this.stepIndex + 1})
      div[data-test-component-id="execution-step"]
    `;
    }

    const commentUrl = `execution-step/${stepId || '*'}/comment`;
    this.editableCommentField = new EditableRichTextFieldElement('comment', commentUrl);
  }

  checkIndexLabel() {
    this.getIndexLabel().should('contain.text', (this.stepIndex + 1).toString());
  }

  checkStepExecutionStatus(expectedStatus: string) {
    this.getExecutionStatusField().should('contain.text', expectedStatus);
  }

  checkStepHeaderContent(expectedHeader: string) {
    this.getStepHeader().should('contain.text', expectedHeader);
  }

  checkStepAction(expectedAction: string) {
    this.getStepActionField().should('contain.text', expectedAction);
  }

  checkStepExpectedResult(expectedResult: string) {
    this.getStepExpectedResultField().should('contain.text', expectedResult);
  }

  checkDnzCufLabel(dnzCufOrder: string, expectedLabel: string) {
    this.getStepDnzCufLabelByOrder(dnzCufOrder).should('contain.text', expectedLabel);
  }

  checkDnzCufValue(dnzCufOrder: string, expectedValue: string) {
    this.getStepDnzCufValueByOrder(dnzCufOrder).should('contain.text', expectedValue);
  }

  checkStepComment(expectedComment: string) {
    this.getStepCommentField().should('contain.text', expectedComment);
  }

  updateComment(newComment: string) {
    this.editableCommentField.setAndConfirmValue(newComment);
  }

  checkCommentRichContent(comment: string) {
    this.editableCommentField.checkHtmlContent(comment);
  }

  assertStepExpectedResultExists() {
    this.getStepExpectedResultField().should('exist');
  }

  assertStepExpectedResultNotExists() {
    this.getStepExpectedResultField().should('not.exist');
  }

  assertCommentFieldExists() {
    this.getStepCommentField().should('exist');
  }

  assertCommentFieldNotExists() {
    this.getStepCommentField().should('not.exist');
  }

  checkIsExtended() {
    this.getCollapseArrow().should('exist');
    this.getExpendArrow().should('not.exist');
  }

  checkIsCollapsed() {
    this.getExpendArrow().should('exist');
    this.getCollapseArrow().should('not.exist');
  }

  collapseStep() {
    this.getCollapseArrow().click();
    this.checkIsCollapsed();
  }

  extendStep() {
    this.getExpendArrow().click();
    this.checkIsExtended();
  }

  changeStepExecutionStatus(executionStatus: ExecutionStatusKeys, executionId: number) {
    const mock = new HttpMockBuilder(`execution-step/${this.stepId}/status?executionId=${executionId}`)
      .post()
      .responseBody({
        lastExecutedBy: 'admin',
        executionStatus: executionStatus,
        lastExecutedOn: new Date()
      })
      .build();
    this.getStatusButton(executionStatus).click();
    mock.wait();
  }

  checkIssueRemoteIdLabel(issueOrder: string, expectedId: string) {
    this.getIssueRemoteIdLabel(issueOrder).should('contain.text', expectedId);
  }

  checkIssueBtProjectLabel(issueOrder: string, expectedBtProject: string) {
    this.getIssueBtProjectLabel(issueOrder).should('contain.text', expectedBtProject);
  }

  checkIssueSummaryLabel(issueOrder: string, expectedSummary: string) {
    this.getIssueSummaryLabel(issueOrder).should('contain.text', expectedSummary);
  }

  checkIssuePriorityLabel(issueOrder: string, expectedPriority: string) {
    this.getIssuePriorityLabel(issueOrder).should('contain.text', expectedPriority);
  }

  unbindOneIssue(issueOrder: string, issuesGridResponse: GridResponse) {
    this.getIssueDeleteButton(issueOrder).click();

    const deleteMock = new HttpMockBuilder('issues/*')
      .delete()
      .build();

    const getKnownIssuesMock = new HttpMockBuilder('issues/execution/*/all-known-issues')
      .post()
      .responseBody(issuesGridResponse)
      .build();

    this.clickConfirmDeleteButton();

    deleteMock.wait();
    getKnownIssuesMock.wait();
  }

  private clickConfirmDeleteButton() {
    cy.get('sqtm-core-confirm-delete-dialog')
      .find('[data-test-dialog-button-id="confirm"]')
      .click()
      // Then
      .get('sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }

  private getCollapseArrow() {
    return cy.get(`
    ${this.rootSelector}
    ${selectByDataTestComponentId('collapse-step')}
    `);
  }

  private getExpendArrow() {
    return cy.get(`
    ${this.rootSelector}
    ${selectByDataTestComponentId('extend-step')}
    `);
  }

  private getIndexLabel() {
    return cy.get(`
    ${this.rootSelector}
    .index-typography
    `);
  }

  private getExecutionStatusField() {
    return cy.get(`
    ${this.rootSelector}
    .execution-step-info
    .step-execution-status
    `);
  }

  private getStepActionField() {
    return cy.get(`
    ${this.rootSelector}
    `).find('[data-test-component-id="execution-step-action"]');
  }

  private getStepHeader() {
    return cy.get(`
    ${this.rootSelector}
    .header-action
    `);
  }

  private getStepExpectedResultField() {
    return cy.get(`
    ${this.rootSelector}
    `).find('[data-test-component-id="execution-step-expected-result"]');
  }

  private getStepDnzCufLabelByOrder(dnzCufOrder: string) {
    return cy.get(`
    ${this.rootSelector}
    `).find(`[data-test-component-id="execution-dnz-label-${dnzCufOrder}"]`);
  }

  private getStepDnzCufValueByOrder(dnzCufOrder: string) {
    return cy.get(`
    ${this.rootSelector}
    `).find(`[data-test-component-id="execution-dnz-value-${dnzCufOrder}"]`);
  }

  private getStepCommentField() {
    return cy.get(`
    ${this.rootSelector}
    `).find(`[data-test-field-id="comment"]`);
  }

  private getStatusButton(executionStatus: ExecutionStatusKeys) {
    return cy.get(`
    ${this.rootSelector}
    `).find(`[data-test-execution-status="${executionStatus}"]`);
  }

  private getIssueRemoteIdLabel(issueOrder) {
    return this.getIssueDataRow(issueOrder).find(`[data-test-field-id="remoteId"]`);
  }

  private getIssueBtProjectLabel(issueOrder) {
    return this.getIssueDataRow(issueOrder).find(`[data-test-field-id="btProject"]`);
  }

  private getIssueSummaryLabel(issueOrder) {
    return this.getIssueDataRow(issueOrder).find(`[data-test-field-id="summary"]`);
  }

  private getIssuePriorityLabel(issueOrder) {
    return this.getIssueDataRow(issueOrder).find(`[data-test-field-id="priority"]`);
  }

  private getIssueDeleteButton(issueOrder) {
    return this.getIssueDataRow(issueOrder).find(`[data-test-field-id="delete-issue"]`);
  }


  private getIssueDataRow(issueOrder) {
    return cy.get(`
    ${this.rootSelector}
    `).find(`[data-test-component-id="issue-${issueOrder}"]`);
  }
}
