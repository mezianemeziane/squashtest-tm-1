import {AbstractStepElement} from './abstract-step.element';
import {selectByDataTestComponentId} from '../../../utils/basic-selectors';
import {LinkElement} from '../link-element';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {DelegateParameterDialogElement} from '../../pages/test-case-workspace/dialogs/delegate-parameter-dialog.element';

export class CallStepElement extends AbstractStepElement {

  constructor(stepId: string, stepIndex: number) {
    super(stepId, stepIndex);
  }

  checkIsExpended() {
    this.getToggleArrow().should('have.class', 'anticon-caret-down');
  }

  checkIsCollapsed() {
    this.getToggleArrow().should('have.class', 'anticon-caret-right');
  }

  getLinkElement(id: string) {
    return new LinkElement(id);
  }

  showDelegateParamDialog(response?: any): DelegateParameterDialogElement {
    const mock = new HttpMockBuilder('test-cases/*/datasets').responseBody(response).build();
    cy.get(`${this.rootSelector} [data-test-field-id="dataset"]`).click();
    mock.wait();
    return new DelegateParameterDialogElement();
  }

  private getToggleArrow() {
    return cy.get(`
    ${this.rootSelector}
    ${selectByDataTestComponentId('toggle-call-step')}
    `);
  }
}
