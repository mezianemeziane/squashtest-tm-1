import Chainable = Cypress.Chainable;

export class SimpleSelectFieldElement {

  constructor(protected fieldId: string) {}

  get selector(): Chainable<any> {
    return cy.get(`nz-select[data-test-field-name=${this.fieldId}]`);
  }

  setValue(newValue?: string) {
    this.selectValue(newValue);
  }

  // Show dropdown and check that the available options names match the specified string array
  checkAllOptions(options: string[]) {
    this.selector.click();
    cy.get(`nz-option-item`).each(function (value, index) {
      cy.wrap(value).should('contain.text', options[index]);
    });
    cy.clickVoid();
  }

  // Checks that the selected option matches the specified text
  checkSelectedOption(expected: string) {
    this.selector.should('contain.text', expected);
  }

  // Returns the Chainable for an option of the dropdown menu
  // This won't work if the dropdown isn't shown.
  private getOption(optionName: string): Chainable<any> {
    return cy.contains(`nz-option-item`, optionName);
  }

  // Shows the dropdown and click on an option
   selectValue(newValue: string) {
    this.selector
      .click();
    this.getOption(newValue).click();
  }
}
