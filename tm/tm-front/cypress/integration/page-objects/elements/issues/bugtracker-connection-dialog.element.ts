import {TextFieldElement} from '../forms/TextFieldElement';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';

export class BugTrackerConnectionDialog {

  userNameField: TextFieldElement;
  passwordField: TextFieldElement;

  constructor() {
    this.userNameField = new TextFieldElement('username');
    this.passwordField = new TextFieldElement('password');
  }

  fillUserName(userName: string) {
    cy.get(`input[data-test-field-name="username"]`).type(userName);
  }

  fillPassword(password: string) {
    cy.get(`input[data-test-field-name="password"]`).type(password);
  }

  connection(response?: any) {
    const mockConnection = new HttpMockBuilder('servers/*/authentication').post().responseBody(response).build();
    cy.get('[data-test-button-id="connection"]').click();
    mockConnection.wait();
  }

  selector() {
    return cy.get('[data-test-dialog-id="bugtracker-connection"]');
  }
}
