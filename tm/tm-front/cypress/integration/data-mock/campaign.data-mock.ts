import {CampaignModel, CampaignStatisticsBundle} from '../model/campaign/campaign-model';
import {addDays} from 'date-fns';
import {combineWithDefaultData} from './data-mocks.utils';
import {TestCaseLibraryModel} from '../model/test-case/test-case-library/test-case-library.model';
import {mockEmptyAttachmentListModel} from './generic-entity.data-mock';
import {CampaignLibraryModel} from '../model/campaign/campaign-library/campaign-library.model';
import {
  CampaignFolderModel,
  CampaignFolderStatisticsBundle
} from '../model/campaign/campaign-folder/campaign-folder.model';

export function getScheduledDate(addedDays: number): string {
  return addDays(new Date(), addedDays).toISOString();
}

export function getTodayScheduledDate(): string {
  return new Date().toISOString();
}

export function mockCampaignLibraryModel(customData: Partial<CampaignLibraryModel> = {}): CampaignLibraryModel {
  const defaultData: TestCaseLibraryModel = {
    id: 1,
    projectId: 1,
    attachmentList: mockEmptyAttachmentListModel(),
    canShowFavoriteDashboard: false,
    customFieldValues: [],
    dashboard: undefined,
    description: '',
    favoriteDashboardId: 0,
    name: '',
    shouldShowFavoriteDashboard: false,
  };

  return combineWithDefaultData(defaultData, customData);
}

export function mockCampaignFolderModel(customData: Partial<CampaignFolderModel> = {}): CampaignFolderModel {
  const defaulData: CampaignFolderModel = {
    id: 1,
    projectId: 1,
    attachmentList: mockEmptyAttachmentListModel(),
    customFieldValues: [],
    name: '',
    description: 'generic description'
  };

  return  combineWithDefaultData(defaulData, customData);
}

export function mockCampaignModel(customData: Partial<CampaignModel> = {}): CampaignModel {
  const defaultData: CampaignModel = {
    id: 1,
    projectId: 1,
    name: 'campaign',
    reference: '',
    description: '',
    createdOn: new Date(),
    createdBy: 'admin',
    lastModifiedOn: new Date(),
    lastModifiedBy: 'admin',
    campaignStatus: 'UNDEFINED',
    progressStatus: 'READY',
    attachmentList: {
      id: 1,
      attachments: []
    },
    milestones: [],
    testPlanStatistics: {
      nbTestCases: 2,
      progression: 0,
      status: 'READY',
      nbDone: 0,
      nbSuccess: 0,
      nbFailure: 0,
      nbSettled: 0,
      nbBlocked: 0,
      nbUntestable: 0,
      nbReady: 2,
      nbRunning: 0
    },
    customFieldValues: [],
    nbIssues: 0,
    hasDatasets: false,
    users: [
      {id: 1, login: 'raowl', firstName: 'Ra', lastName: 'Oul'},
      {id: 2, login: 'jawny', firstName: 'Joe', lastName: 'Ni'}
    ],
    testSuites: [
      {id: 1, name: 'suite01'},
      {id: 2, name: 'suite02'},
    ],
  };
  return combineWithDefaultData(defaultData, customData);
}

export function getDefaultCampaignFolderStatisticsBundle(): CampaignFolderStatisticsBundle {
  return {
    campaignTestInventoryStatisticsList: [
      {
        campaignName: 'Cp 1',
        statistics: {
          FAILURE: 1,
          BLOCKED: 2,
          READY: 10,
          RUNNING: 2,
          SETTLED: 3,
          SUCCESS: 4,
          UNTESTABLE: 1
        }
      },
      {
        campaignName: 'Cp 2',
        statistics: {
          FAILURE: 3,
          BLOCKED: 5,
          READY: 0,
          RUNNING: 4,
          SETTLED: 1,
          SUCCESS: 1,
          UNTESTABLE: 1
        }
      }
    ],
    campaignProgressionStatistics: {
      scheduledIterations: [
        {
          id: 1,
          name: 'Campaign-1',
          scheduledStart: new Date().toISOString(),
          scheduledEnd: getScheduledDate(2),
          testplanCount: 15,
          cumulativeTestsByDate: [
            [getTodayScheduledDate(), 5],
            [getScheduledDate(1), 10],
            [getScheduledDate(2), 15],
          ]
        },
        {
          id: 2,
          name: 'Campaign-2',
          scheduledStart: getScheduledDate(10),
          scheduledEnd: getScheduledDate(12),
          testplanCount: 12,
          cumulativeTestsByDate: [
            [getScheduledDate(10), 20],
            [getScheduledDate(11), 24],
            [getScheduledDate(12), 27],
          ]
        }
      ],
      cumulativeExecutionsPerDate: [
        [getTodayScheduledDate(), 3],
        [getScheduledDate(1), 9],
        [getScheduledDate(2), 12],
      ],
      errors: []
    },
    campaignTestCaseStatusStatistics: {
      FAILURE: 1,
      BLOCKED: 2,
      READY: 10,
      RUNNING: 2,
      SETTLED: 3,
      SUCCESS: 4,
      UNTESTABLE: 1
    },
    campaignTestCaseSuccessRateStatistics: {
      conclusiveness: {
        VERY_HIGH: {FAILURE: 0, NON_CONCLUSIVE: 0, SUCCESS: 2},
        HIGH: {FAILURE: 4, NON_CONCLUSIVE: 3, SUCCESS: 23},
        MEDIUM: {FAILURE: 0, NON_CONCLUSIVE: 0, SUCCESS: 1},
        LOW: {FAILURE: 0, NON_CONCLUSIVE: 0, SUCCESS: 2},
      }
    },
    campaignNonExecutedTestCaseImportanceStatistics: {
      VERY_HIGH: 2,
      HIGH: 2,
      MEDIUM: 7,
      LOW: 3,
    }
  };
}

export function getDefaultCampaignStatisticsBundle(): CampaignStatisticsBundle {
  return {
    iterationTestInventoryStatistics: [
      {
        iterationName: 'It 1',
        statistics: {
          FAILURE: 1,
          BLOCKED: 2,
          READY: 10,
          RUNNING: 2,
          SETTLED: 3,
          SUCCESS: 4,
          UNTESTABLE: 1
        }
      },
      {
        iterationName: 'It 2',
        statistics: {
          FAILURE: 3,
          BLOCKED: 5,
          READY: 0,
          RUNNING: 4,
          SETTLED: 1,
          SUCCESS: 1,
          UNTESTABLE: 1
        }
      }
    ],
    campaignProgressionStatistics: {
      scheduledIterations: [
        {
          id: 1,
          name: 'iteration-1',
          scheduledStart: new Date().toISOString(),
          scheduledEnd: getScheduledDate(2),
          testplanCount: 15,
          cumulativeTestsByDate: [
            [getTodayScheduledDate(), 5],
            [getScheduledDate(1), 10],
            [getScheduledDate(2), 15],
          ]
        },
        {
          id: 2,
          name: 'iteration-2',
          scheduledStart: getScheduledDate(10),
          scheduledEnd: getScheduledDate(12),
          testplanCount: 12,
          cumulativeTestsByDate: [
            [getScheduledDate(10), 20],
            [getScheduledDate(11), 24],
            [getScheduledDate(12), 27],
          ]
        }
      ],
      cumulativeExecutionsPerDate: [
        [getTodayScheduledDate(), 3],
        [getScheduledDate(1), 9],
        [getScheduledDate(2), 12],
      ],
      errors: []
    },
    campaignTestCaseStatusStatistics: {
      FAILURE: 1,
      BLOCKED: 2,
      READY: 10,
      RUNNING: 2,
      SETTLED: 3,
      SUCCESS: 4,
      UNTESTABLE: 1
    },
    campaignTestCaseSuccessRateStatistics: {
      conclusiveness: {
        VERY_HIGH: {FAILURE: 0, NON_CONCLUSIVE: 0, SUCCESS: 2},
        HIGH: {FAILURE: 4, NON_CONCLUSIVE: 3, SUCCESS: 23},
        MEDIUM: {FAILURE: 0, NON_CONCLUSIVE: 0, SUCCESS: 1},
        LOW: {FAILURE: 0, NON_CONCLUSIVE: 0, SUCCESS: 2},
      }
    },
    campaignNonExecutedTestCaseImportanceStatistics: {
      VERY_HIGH: 2,
      HIGH: 2,
      MEDIUM: 7,
      LOW: 3,
    }
  };
}

export function getEmptyCampaignStatisticsBundle(): CampaignStatisticsBundle {
  return {
    iterationTestInventoryStatistics: [],
    campaignProgressionStatistics: {
      scheduledIterations: [],
      cumulativeExecutionsPerDate: [],
      errors: ['error']
    },
    campaignTestCaseStatusStatistics: {
      FAILURE: 0,
      BLOCKED: 0,
      READY: 0,
      RUNNING: 0,
      SETTLED: 0,
      SUCCESS: 0,
      UNTESTABLE: 0
    },
    campaignTestCaseSuccessRateStatistics: {
      conclusiveness: {
        VERY_HIGH: {FAILURE: 0, NON_CONCLUSIVE: 0, SUCCESS: 0},
        HIGH: {FAILURE: 0, NON_CONCLUSIVE: 0, SUCCESS: 0},
        MEDIUM: {FAILURE: 0, NON_CONCLUSIVE: 0, SUCCESS: 0},
        LOW: {FAILURE: 0, NON_CONCLUSIVE: 0, SUCCESS: 0},
      }
    },
    campaignNonExecutedTestCaseImportanceStatistics: {
      VERY_HIGH: 0,
      HIGH: 0,
      MEDIUM: 0,
      LOW: 0,
    }
  };
}
