import {DataRow, DataRowOpenState, GridResponse, SquashTmDataRowType} from '../model/grids/data-row.type';

export function mockDataRow(customData: Partial<DataRow>): DataRow {
  const defaultRow: DataRow = {
    id: 1,
    projectId: 1,
    type: SquashTmDataRowType.Generic,
    data: {},
    allowedChildren: [],
    allowMoves: false,
    children: [],
    parentRowId: undefined,
    state: DataRowOpenState.leaf,
  };

  return {...defaultRow, ...customData};
}

export function mockGridResponse(idAttribute: string, dataRows: DataRow[]): GridResponse {
  return {
    dataRows,
    count: dataRows.length,
    idAttribute,
  };
}
