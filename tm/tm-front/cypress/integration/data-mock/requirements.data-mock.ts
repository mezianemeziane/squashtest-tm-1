import {RequirementVersionModel} from '../model/requirements/requirement-version.model';
import {mockEmptyAttachmentListModel} from './generic-entity.data-mock';
import {RequirementLibraryModel} from '../model/requirements/requirement-library/requirement-library.model';
import {RequirementFolderModel} from '../model/requirements/requirement-folder/requirement-folder.model';
import {combineWithDefaultData} from './data-mocks.utils';

export function mockRequirementVersionModel(customData: Partial<RequirementVersionModel> = {}): RequirementVersionModel {
  return combineWithDefaultData({
    id: 1,
    projectId: 1,
    attachmentList: mockEmptyAttachmentListModel(),
    bindableMilestones: [],
    category: 0,
    createdBy: '',
    createdOn: undefined,
    criticality: '',
    customFieldValues: [],
    description: '',
    lastModifiedBy: '',
    lastModifiedOn: undefined,
    milestones: [],
    name: '',
    reference: '',
    requirementId: 0,
    requirementStats: undefined,
    requirementVersionLinks: [],
    status: 'OBSOLETE',
    verifyingTestCases: [],
    versionNumber: 0,
    remoteReqPerimeterStatus: undefined
  }, customData);
}

export function mockRequirementLibraryModel(customData: Partial<RequirementFolderModel> = {}): RequirementLibraryModel {
  return combineWithDefaultData({
    id: 1,
    projectId: 1,
    name: '',
    attachmentList: mockEmptyAttachmentListModel(),
    canShowFavoriteDashboard: false,
    customFieldValues: [],
    dashboard: undefined,
    description: '',
    favoriteDashboardId: 0,
    shouldShowFavoriteDashboard: false,
    statistics: undefined
  }, customData);
}

export function mockRequirementFolderModel(customData: Partial<RequirementFolderModel> = {}): RequirementFolderModel {
  return combineWithDefaultData({
    id: 1,
    projectId: 1,
    name: '',
    attachmentList: mockEmptyAttachmentListModel(),
    canShowFavoriteDashboard: false,
    customFieldValues: [],
    dashboard: undefined,
    description: '',
    favoriteDashboardId: 0,
    shouldShowFavoriteDashboard: false,
    statistics: undefined
  }, customData);
}

