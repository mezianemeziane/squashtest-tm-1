import {IterationModel, IterationStatisticsBundle} from '../model/campaign/iteration-model';
import {getScheduledDate, getTodayScheduledDate} from './campaign.data-mock';
import {combineWithDefaultData} from './data-mocks.utils';
import {TestSuiteModel} from '../model/campaign/test-suite.model';

export function getEmptyIterationStatisticsBundle() {
  return {
    iterationTestCaseStatusStatistics: {},
    iterationTestCaseSuccessRateStatistics: {
      conclusiveness: {
        VERY_HIGH: {
          FAILURE: 0,
          SUCCESS: 0,
          NON_CONCLUSIVE: 0
        },
        HIGH: {
          FAILURE: 0,
          SUCCESS: 0,
          NON_CONCLUSIVE: 0
        },
        MEDIUM: {
          FAILURE: 0,
          SUCCESS: 0,
          NON_CONCLUSIVE: 0
        },
        LOW: {
          FAILURE: 0,
          SUCCESS: 0,
          NON_CONCLUSIVE: 0
        }
      }
    },
    iterationProgressionStatistics: {
      cumulativeExecutionsPerDate: [],
      errors: ['error'],
      scheduledIteration: {
        id: 1,
        name: 'iteration-1',
        scheduledStart: null,
        scheduledEnd: null,
        testplanCount: 0,
        cumulativeTestsByDate: []
      },
    },
    iterationNonExecutedTestCaseImportanceStatistics: {
      VERY_HIGH: 0,
      HIGH: 0,
      MEDIUM: 0,
      LOW: 0
    },
    testsuiteTestInventoryStatisticsList: []
  };
}

export function getDefaultIterationStatisticsBundle(): IterationStatisticsBundle {
  return {
    iterationTestCaseStatusStatistics: {
      SUCCESS: 6,
      FAILURE: 3
    },
    iterationTestCaseSuccessRateStatistics: {
      conclusiveness: {
        VERY_HIGH: {
          FAILURE: 2,
          SUCCESS: 4,
          NON_CONCLUSIVE: 2
        },
        HIGH: {
          FAILURE: 4,
          SUCCESS: 4,
          NON_CONCLUSIVE: 2
        },
        MEDIUM: {
          FAILURE: 2,
          SUCCESS: 0,
          NON_CONCLUSIVE: 0
        },
        LOW: {
          FAILURE: 0,
          SUCCESS: 0,
          NON_CONCLUSIVE: 0
        }
      }
    },
    iterationProgressionStatistics: {
      cumulativeExecutionsPerDate: [
        [getTodayScheduledDate(), 3],
        [getScheduledDate(1), 9],
        [getScheduledDate(2), 12],
      ],
      errors: [],
      scheduledIteration: {
        id: 1,
        name: 'iteration-1',
        scheduledStart: new Date().toISOString(),
        scheduledEnd: getScheduledDate(2),
        testplanCount: 15,
        cumulativeTestsByDate: [
          [getTodayScheduledDate(), 5],
          [getScheduledDate(1), 10],
          [getScheduledDate(2), 15],
        ]
      },
    },
    iterationNonExecutedTestCaseImportanceStatistics: {
      VERY_HIGH: 4,
      HIGH: 5,
      MEDIUM: 2,
      LOW: 2
    },
    testsuiteTestInventoryStatisticsList: [
      {
        testsuiteName: 'testSuite 1',
        importanceStatistics: {
          LOW: 2,
          MEDIUM: 3,
          HIGH: 5,
          VERY_HIGH: 5
        },
        statusStatistics: {
          FAILURE: 1,
          BLOCKED: 2,
          READY: 10,
          RUNNING: 2,
          SETTLED: 3,
          SUCCESS: 4,
          UNTESTABLE: 1
        },
        nbToExecute: 12,
        nbExecuted: 10,
        nbPrevToExecute: 10,
        nbTotal: 22,
        pcFailure: 12,
        pcPrevProgress: 25,
        pcProgress: 30,
        pcSuccess: 30,
        scheduledEnd: new Date().toISOString(),
        scheduledStart: new Date().toISOString()
      },
      {
        testsuiteName: null,
        importanceStatistics: {
          LOW: 4,
          MEDIUM: 5,
          HIGH: 2,
          VERY_HIGH: 1
        },
        statusStatistics: {
          FAILURE: 1,
          BLOCKED: 7,
          READY: 14,
          RUNNING: 1,
          SETTLED: 1,
          SUCCESS: 1,
          UNTESTABLE: 1
        },
        nbToExecute: 2,
        nbExecuted: 10,
        nbPrevToExecute: 3,
        nbTotal: 12,
        pcFailure: 23,
        pcPrevProgress: 45,
        pcProgress: 56,
        pcSuccess: 4,
        scheduledEnd: new Date().toISOString(),
        scheduledStart: new Date().toISOString()
      }
    ]
  };
}


export function mockIterationModel(customData: Partial<IterationModel> = {}): IterationModel {
  const defaultData: IterationModel = {
    id: 1,
    projectId: 1,
    name: 'iteration',
    description: '',
    reference: '',
    customFieldValues: [],
    attachmentList: {
      id: 1,
      attachments: []
    },
    itpi: [],
    hasDatasets: false,
    createdBy: 'toto',
    createdOn: new Date(),
    lastModifiedBy: 'gilbert',
    lastModifiedOn: new Date(),
    scheduledStartDate: new Date(),
    scheduledEndDate: new Date(),
    actualStartDate: new Date(),
    actualEndDate: new Date(),
    actualStartAuto: false,
    actualEndAuto: false,
    iterationStatus: 'PLANNED',
    testPlanStatistics: {
      status: 'DONE',
      progression: 100,
      nbTestCases: 3,
      nbDone: 3,
      nbReady: 0,
      nbRunning: 0,
      nbUntestable: 0,
      nbBlocked: 0,
      nbFailure: 0,
      nbSettled: 0,
      nbSuccess: 0
    },
    uuid: '12fe78',
    milestones: [],
    users: [],
    testSuites: [],
    executionStatusMap: new Map<number, string>(),
    iterationStatisticsBundle: getEmptyIterationStatisticsBundle(),
  };

  return combineWithDefaultData(defaultData, customData);
}

export function mockTestSuiteModel(customData: Partial<TestSuiteModel> = {}): TestSuiteModel {
  const defaultData: TestSuiteModel = {
    attachmentList: {
      id: 1,
      attachments: []
    },
    createdBy: 'toto',
    createdOn: new Date(),
    lastModifiedBy: 'gilbert',
    lastModifiedOn: new Date(),
    customFieldValues: [],
    description: '',
    executionStatus: 'PLANNED',
    executionStatusMap: {},
    hasDatasets: false,
    id: 1,
    name: '',
    projectId: 1,
    testPlanStatistics: {
      status: 'DONE',
      progression: 100,
      nbTestCases: 3,
      nbDone: 3,
      nbReady: 0,
      nbRunning: 0,
      nbUntestable: 0,
      nbBlocked: 0,
      nbFailure: 0,
      nbSettled: 0,
      nbSuccess: 0
    },
    users: [],
    uuid: '12fe78'
  };

  return combineWithDefaultData(defaultData, customData);
}
