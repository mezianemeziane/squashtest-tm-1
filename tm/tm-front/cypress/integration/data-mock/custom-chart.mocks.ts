import {
  ChartColumnType,
  ChartDataType,
  ChartDefinitionModel,
  ChartOperation,
  ChartScopeType,
  ChartType
} from '../model/custom-report/chart-definition.model';
import {EntityReference, EntityType} from '../model/entity.model';

export function getSimpleChartDefinition(type: ChartType): Partial<ChartDefinitionModel> {
  return {
    id: 1,
    customReportLibraryNodeId: 3,
    projectId: 1,
    name: 'Financial Breakdown',
    type: type,
    'scope': [{'type': EntityType.PROJECT, 'id': 1} as EntityReference],
    'measures': [{
      'cufId': null,
      'label': '',
      'column': {
        'columnType': ChartColumnType.ATTRIBUTE,
        'label': 'REQUIREMENT_VERSION_ID',
        'specializedType': {'entityType': 'REQUIREMENT_VERSION', 'entityRole': null},
        dataType: ChartDataType.NUMERIC
      },
      'operation': ChartOperation.COUNT
    }],
    'axis': [{
      'cufId': null,
      'label': '',
      'column': {
        'columnType': ChartColumnType.ATTRIBUTE,
        'label': 'REQUIREMENT_VERSION_CATEGORY',
        'specializedType': {'entityType': 'REQUIREMENT_VERSION', 'entityRole': null},
        'dataType': ChartDataType.INFO_LIST_ITEM
      },
      'operation': ChartOperation.NONE,

    }],
    'filters': [],
    abscissa: [
      ['requirement.category.CAT_BUSINESS'],
      ['requirement.category.CAT_NON_FUNCTIONAL'],
      ['requirement.category.CAT_SECURITY'],
      ['requirement.category.CAT_TECHNICAL'],
      ['requirement.category.CAT_TEST_REQUIREMENT'],
      ['requirement.category.CAT_UNDEFINED']],
    'series': {'': [2, 4, 1, 1, 1, 15]},
    'projectScope': [],
    'scopeType': ChartScopeType.DEFAULT
  };
}

export function getThreeAxisModel(type: ChartType): Partial<ChartDefinitionModel> {
  return {
    id: 1,
    customReportLibraryNodeId: 3,
    projectId: 1,
    name: 'Financial Breakdown',
    'type': type,
    'measures': [{
      'cufId': null,
      'label': '',
      'column': {
        'columnType': ChartColumnType.ATTRIBUTE,
        'label': 'REQUIREMENT_VERSION_ID',
        'specializedType': {'entityType': 'REQUIREMENT_VERSION', 'entityRole': null},
        dataType: ChartDataType.NUMERIC
      },
      'operation': ChartOperation.COUNT
    }],
    'axis': [{
      'cufId': null,
      'label': '',
      'column': {
        'columnType': ChartColumnType.ATTRIBUTE,
        'label': 'REQUIREMENT_VERSION_STATUS',
        'specializedType': {'entityType': 'REQUIREMENT_VERSION', 'entityRole': null},
        'dataType': ChartDataType.REQUIREMENT_STATUS
      },
      'operation': ChartOperation.NONE
    }, {
      'cufId': null,
      'label': '',
      'column': {
        'columnType': ChartColumnType.ATTRIBUTE,
        'label': 'REQUIREMENT_VERSION_CRITICALITY',
        'specializedType': {'entityType': 'REQUIREMENT_VERSION', 'entityRole': null},
        'dataType': ChartDataType.LEVEL_ENUM
      },
      'operation': ChartOperation.NONE
    }],
    'filters': [],
    'abscissa': [
      ['WORK_IN_PROGRESS', 'CRITICAL'],
      ['WORK_IN_PROGRESS', 'MAJOR'],
      ['WORK_IN_PROGRESS', 'MINOR'],
      ['WORK_IN_PROGRESS', 'UNDEFINED'],
      ['UNDER_REVIEW', 'CRITICAL'],
      ['UNDER_REVIEW', 'MAJOR'],
      ['UNDER_REVIEW', 'MINOR'],
      ['APPROVED', 'UNDEFINED'],
      ['OBSOLETE', 'MINOR'],
      ['OBSOLETE', 'UNDEFINED']],
    'series': {'': [1, 4, 5, 8, 1, 1, 1, 1, 1, 1]},
    'projectScope': [],
    'scopeType': ChartScopeType.DEFAULT
  };
}
