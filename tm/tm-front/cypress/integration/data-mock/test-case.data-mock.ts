import {TestCaseModel} from '../model/test-case/test-case.model';
import {TestCaseLibraryModel} from '../model/test-case/test-case-library/test-case-library.model';
import {mockEmptyAttachmentListModel} from './generic-entity.data-mock';
import {TestCaseFolderModel} from '../model/test-case/test-case-folder/test-case-folder.model';
import {combineWithDefaultData} from './data-mocks.utils';

export function mockTestCaseModel(customData: Partial<TestCaseModel> = {}): TestCaseModel {
  const defaultData: TestCaseModel = {
    id: 1,
    projectId: 1,
    customFieldValues: [],
    attachmentList: mockEmptyAttachmentListModel(),
    name: 'name',
    reference: 'ref',
    importance: 'LOW',
    description: 'description',
    status: 'WORK_IN_PROGRESS',
    nature: 12,
    type: 20,
    importanceAuto: false,
    automatable: 'M',
    prerequisite: '',
    testSteps: [],
    milestones: [],
    automationRequest: null,
    parameters: [],
    datasets: [],
    datasetParamValues: [],
    coverages: [],
    uuid: '44d63d7e-11dd-44b0-b584-565b6f791fa2',
    kind: 'STANDARD',
    executions: [],
    nbIssues: 0,
    calledTestCases: [],
    lastModifiedOn: null,
    lastModifiedBy: '',
    createdBy: 'cypress',
    createdOn: new Date(),
    lastExecutionStatus: 'SUCCESS',
    script: '',
    actionWordLibraryActive: false,
    scmRepositoryId: null,
    automatedTestReference: '',
    automatedTestTechnology: null,
  };

  return combineWithDefaultData(defaultData, customData);
}

export function mockTestCaseFolderModel(customData: Partial<TestCaseFolderModel> = {}): TestCaseFolderModel {
  const defaultData: TestCaseFolderModel = {
    id: 1,
    projectId: 1,
    attachmentList: mockEmptyAttachmentListModel(),
    canShowFavoriteDashboard: false,
    customFieldValues: [],
    dashboard: undefined,
    description: '',
    favoriteDashboardId: 1,
    name: '',
    shouldShowFavoriteDashboard: false,
    statistics: undefined,
  };

  return combineWithDefaultData(defaultData, customData);
}

export function mockTestCaseLibraryModel(customData: Partial<TestCaseLibraryModel> = {}): TestCaseLibraryModel {
  const defaultData: TestCaseLibraryModel = {
    id: 1,
    projectId: 1,
    attachmentList: mockEmptyAttachmentListModel(),
    canShowFavoriteDashboard: false,
    customFieldValues: [],
    dashboard: undefined,
    description: '',
    favoriteDashboardId: 0,
    name: '',
    shouldShowFavoriteDashboard: false,
  };

  return combineWithDefaultData(defaultData, customData);
}

