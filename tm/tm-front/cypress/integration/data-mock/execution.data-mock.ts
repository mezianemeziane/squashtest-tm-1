import {ExecutionModel, ExecutionStepModel} from '../model/execution/execution.model';
import {combineWithDefaultData} from './data-mocks.utils';

export function mockExecutionModel(customData: Partial<ExecutionModel>): ExecutionModel {
  const defaultData: ExecutionModel = {
    id: 1,
    projectId: 1,
    executionOrder: 2,
    name: 'NX - Test Case 1',
    prerequisite: '',
    attachmentList: {id: 1, attachments: []},
    customFieldValues: [],
    tcImportance: 'LOW',
    tcNatLabel: 'test-case.nature.NAT_BUSINESS_TESTING',
    tcNatIconName: '',
    tcStatus: 'APPROVED',
    tcTypeLabel: 'test-case.type.TYP_COMPLIANCE_TESTING',
    tcTypeIconName: '',
    tcDescription: 'description',
    comment: '',
    denormalizedCustomFieldValues: [],
    executionStepViews: [
      {
        id: 1,
        order: 0,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: [
            {id: 1, addedOn: new Date('25 Mar 2020 10:12:57'), name: 'attachment-1', size: 56789},
            {id: 2, addedOn: new Date('17 Mar 2019 20:40:00'), name: 'attachment-2', size: 1234567},
          ]
        },
        action: '',
        expectedResult: '',
        comment: '',
        customFieldValues: [],
        projectId: 1,
        lastExecutedBy: 'admin',
        lastExecutedOn: null
      },
      {
        id: 2,
        order: 1,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel,
      {
        id: 3,
        order: 2,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel
    ],
    coverages: [],
    executionMode: 'MANUAL',
    lastExecutedOn: null,
    lastExecutedBy: 'admin',
    executionStatus: 'READY',
    automatedJobUrl: null,
    testAutomationServerKind: null,
    automatedExecutionResultUrl: null,
    automatedExecutionResultSummary: null,
    nbIssues: 0,
    iterationId: -1,
    kind: 'STANDARD'
  };

  return combineWithDefaultData(defaultData, customData);
}
