import {ProjectView, WorkspaceTypeForPlugins} from '../model/project/project.model';
import {SystemViewModel} from '../model/system/system-view.model';
import {User} from '../model/user/user.model';
import {Team} from '../model/team/team.model';
import {MilestoneAdminView} from '../model/milestone/milestone.model';
import {AdminTestAutomationServer, TestAutomationServerKind} from '../model/test-automation/test-automation-server.model';
import {combineWithDefaultData} from './data-mocks.utils';
import {AuthenticationProtocol} from '../model/third-party-server/authentication.model';

export function makeProjectViewData(customData?: Partial<ProjectView>): ProjectView {
  const defaultData: ProjectView = {
    id: 4,
    name: 'Project1',
    label: null,
    testCaseNatureId: 2,
    testCaseTypeId: 3,
    requirementCategoryId: 4,
    allowAutomationWorkflow: true,
    customFieldBindings: {
      CAMPAIGN_FOLDER: [],
      TESTCASE_FOLDER: [],
      REQUIREMENT_VERSION: [],
      REQUIREMENT_FOLDER: [],
      TEST_SUITE: [],
      ITERATION: [],
      TEST_CASE: [],
      TEST_STEP: [],
      CAMPAIGN: [],
      EXECUTION: [],
      CUSTOM_REPORT_FOLDER: [],
      EXECUTION_STEP: []
    },
    permissions: null,
    availableBugtrackers: [],
    bugTrackerBinding: null,
    bugtrackerProjectNames: null,
    milestoneBindings: [],
    taServerId: null,
    automationWorkflowType: 'NATIVE',
    disabledExecutionStatus: [],
    attachmentList: {
      id: 519,
      attachments: [{'id': 7, 'name': 'demo_sprint_7.pdf', 'size': 152088, 'addedOn': new Date()}]
    },
    hasData: false,
    template: false,
    linkedTemplateId: 2,
    linkedTemplate: 'Template1',
    createdOn: new Date(),
    createdBy: 'admin',
    lastModifiedOn: new Date(),
    lastModifiedBy: 'admin',
    allowTcModifDuringExec: false,
    allowedStatuses: {SETTLED: false, UNTESTABLE: false},
    statusesInUse: {SETTLED: false, UNTESTABLE: false},
    description: '',
    uri: '',
    infoLists: null,
    useTreeStructureInScmRepo: false,
    templateLinkedToProjects: false,
    partyProjectPermissions: [],
    availableScmServers: [],
    availableTestAutomationServers: [],
    boundTestAutomationProjects: [],
    boundMilestonesInformation: [],
    availablePlugins: [],
    keywords: undefined,
    bddScriptLanguage: undefined,
    scmRepositoryId: undefined,
    activatedPlugins: {
      [WorkspaceTypeForPlugins.CAMPAIGN_WORKSPACE]: [],
      [WorkspaceTypeForPlugins.TEST_CASE_WORKSPACE]: [],
      [WorkspaceTypeForPlugins.REQUIREMENT_WORKSPACE]: [],
    }
  };

  return combineWithDefaultData(defaultData, customData);
}

export function makeSystemViewData(customData?: Partial<SystemViewModel>): SystemViewModel {
  const defaultData: SystemViewModel = {
    appVersion: 'VERSION',
    plugins: ['PLUGIN1', 'PLUGIN2'],
    statistics: {
      testcaseIndexingDate: null,
      requirementIndexingDate: null,
      campaignIndexingDate: null,
      projectsNumber: 1,
      usersNumber: 12,
      requirementsNumber: 123,
      testCasesNumber: 1234,
      campaignsNumber: 12345,
      iterationsNumber: 321,
      executionsNumber: 2,
      databaseSize: 10000,
    },
    callbackUrl: 'localhost:8080/squash',
    caseInsensitiveLogin: false,
    duplicateLogins: [],
    importSizeLimit: '10000',
    loginMessage: 'Login message',
    welcomeMessage: 'Welcome message',
    stackTraceFeatureIsEnabled: false,
    stackTracePanelIsVisible: true,
    uploadSizeLimit: '1000',
    whiteList: '',
    logFiles: [],
    autoconnectOnConnection: false,
  };

  return combineWithDefaultData(defaultData, customData);
}

export function makeUserData(customData?: Partial<User>): User {
  const defaultData: User = {
    id: 1,
    login: 'login',
    firstName: 'firstName',
    lastName: 'lastName',
    active: false,
    createdBy: '',
    createdOn: undefined,
    email: '',
    lastConnectedOn: undefined,
    lastModifiedBy: '',
    lastModifiedOn: undefined,
    projectPermissions: [],
    teams: [],
    usersGroupBinding: 0,
    usersGroups: [],
    canManageLocalPassword: true,
  };

  return combineWithDefaultData(defaultData, customData);
}

export function makeTeamData(customData?: Partial<Team>): Team {
  const defaultData: Team = {
    id: 1,
    name: 'name',
    createdBy: '',
    createdOn: undefined,
    description: '',
    lastModifiedBy: '',
    lastModifiedOn: undefined,
    members: [],
    projectPermissions: [],
  };

  return combineWithDefaultData(defaultData, customData);
}

export function makeMilestoneViewData(customData?: Partial<MilestoneAdminView>): MilestoneAdminView {
  const defaultData: MilestoneAdminView = {
    id: 1,
    createdBy: '',
    createdOn: undefined,
    description: '',
    endDate: undefined,
    label: '',
    lastModifiedBy: '',
    lastModifiedOn: undefined,
    ownerFistName: '',
    ownerLastName: '',
    ownerLogin: '',
    range: '',
    status: undefined,
    canEdit: false,
    boundProjectsInformation: [],
  };

  return combineWithDefaultData(defaultData, customData);
}

export function makeTAServerViewData(customData?: Partial<AdminTestAutomationServer>): AdminTestAutomationServer {
  const defaultData: AdminTestAutomationServer = {
    id: 1,
    name: 'TAServer',
    createdBy: '',
    createdOn: undefined,
    description: '',
    baseUrl: '',
    credentials: undefined,
    kind: TestAutomationServerKind.jenkins,
    lastModifiedBy: '',
    lastModifiedOn: undefined,
    manualSlaveSelection: false,
    authProtocol: AuthenticationProtocol.BASIC_AUTH,
    supportedAuthenticationProtocols: [AuthenticationProtocol.BASIC_AUTH],
  };

  return combineWithDefaultData(defaultData, customData);
}
