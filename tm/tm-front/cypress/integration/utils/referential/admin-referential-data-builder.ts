import {AuthenticatedUser} from '../../model/user/authenticated-user.model';

import {AdminReferentialData} from '../../model/admin-referential-data.model';
import {LicenseInformation} from '../../model/referential-data.model';
import {CustomField} from '../../model/customfield/customfield.model';
import {TestAutomationServerKind} from '../../model/test-automation/test-automation-server.model';
import {TemplateConfigurablePlugin} from '../../model/plugin/template-configurable-plugin.model';

export class AdminReferentialDataMockBuilder {

  private readonly _data: AdminReferentialData;

  constructor() {
    this._data = getDefaultAdminReferentialData();
  }

  public withUser(user: AuthenticatedUser): this {
    this._data.user = user;
    return this;
  }

  public withLicenseInformation(licenseInformation: LicenseInformation): this {
    this._data.licenseInformation = licenseInformation;
    return this;
  }

  public withCustomFields(customFields: CustomField[]): this {
    this._data.customFields = customFields;
    return this;
  }

  public withAvailableTestAutomationServerKinds(kinds: TestAutomationServerKind[]): this {
    this._data.availableTestAutomationServerKinds = kinds;
    return this;
  }

  public withTemplateConfigurablePlugins(templateConfigurablePlugins: TemplateConfigurablePlugin[]): this {
    this._data.templateConfigurablePlugins = templateConfigurablePlugins;
    return this;
  }

  public build(): AdminReferentialData {
     return {
      ...this._data
    };
  }
}

export function getDefaultAdminReferentialData() {
  const defaultAdminReferentialData: AdminReferentialData = {
    user: {
      userId: 1,
      username: 'admin',
      firstName: 'admin',
      lastName: 'admin',
      admin: true,
      projectManager: false,
      functionalTester: false,
      automationProgrammer: false,
    },
    globalConfiguration: {
      milestoneFeatureEnabled: true,
      uploadFileExtensionWhitelist: [],
      uploadFileSizeLimit: 0,
    },
    licenseInformation: {
      pluginLicenseExpiration: null,
      activatedUserExcess: null,
    },
    customFields: [],
    availableTestAutomationServerKinds: [],
    canManageLocalPassword: true,
    templateConfigurablePlugins: []
  };
  return defaultAdminReferentialData;
}

