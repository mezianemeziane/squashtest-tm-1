import {HTTP_RESPONSE_STATUS, HttpMock, HttpMockBuilder} from '../mocks/request-mock';
import {Project, WorkspaceTypeForPlugins} from '../../model/project/project.model';
import {BindableEntity} from '../../model/bindable-entity.model';
import {Permissions} from '../../model/permissions/permissions.model';
import {ReferentialData} from '../../model/referential-data.model';
import Chainable = Cypress.Chainable;
import {getSystemInfoLists} from './referential-data-builder';

const project1: Project = {
  id: 1,
  name: 'project 1',
  requirementCategoryId: 1,
  testCaseNatureId: 2,
  testCaseTypeId: 3,
  milestoneBindings: [],
  customFieldBindings: {
    [BindableEntity.REQUIREMENT_FOLDER]: [],
    [BindableEntity.REQUIREMENT_VERSION]: [],
    [BindableEntity.TESTCASE_FOLDER]: [],
    [BindableEntity.TEST_CASE]: [],
    [BindableEntity.TEST_STEP]: [],
    [BindableEntity.CAMPAIGN_FOLDER]: [],
    [BindableEntity.CAMPAIGN]: [],
    [BindableEntity.ITERATION]: [],
    [BindableEntity.TEST_SUITE]: [],
    [BindableEntity.EXECUTION]: [],
    [BindableEntity.EXECUTION_STEP]: [],
    [BindableEntity.CUSTOM_REPORT_FOLDER]: [],
  },
  permissions: {
    PROJECT: [Permissions.READ, Permissions.WRITE, Permissions.DELETE, Permissions.ADMIN, Permissions.MANAGEMENT, Permissions.ATTACH],
    PROJECT_TEMPLATE: [Permissions.READ, Permissions.WRITE, Permissions.DELETE, Permissions.ADMIN,
      Permissions.MANAGEMENT, Permissions.ATTACH],
    REQUIREMENT_LIBRARY: [Permissions.READ, Permissions.WRITE, Permissions.CREATE, Permissions.DELETE, Permissions.ATTACH],
    TEST_CASE_LIBRARY: [Permissions.READ, Permissions.WRITE, Permissions.CREATE, Permissions.DELETE, Permissions.ATTACH],
    CAMPAIGN_LIBRARY: [Permissions.READ, Permissions.WRITE, Permissions.CREATE, Permissions.DELETE, Permissions.ATTACH,
      Permissions.EXPORT, Permissions.EXECUTE, Permissions.LINK, Permissions.EXTENDED_DELETE, Permissions.READ_UNASSIGNED],
    CUSTOM_REPORT_LIBRARY: [Permissions.READ, Permissions.WRITE, Permissions.CREATE, Permissions.DELETE, Permissions.ATTACH],
    AUTOMATION_REQUEST_LIBRARY: [Permissions.READ, Permissions.WRITE, Permissions.CREATE, Permissions.DELETE, Permissions.ATTACH]
  },
  disabledExecutionStatus: [],
  keywords: [{label: 'Étant donné que', value: 'GIVEN'}, {label: 'Quand', value: 'WHEN'},
    {label: 'Alors', value: 'THEN'}, {label: 'Et', value: 'AND'}, {label: 'Mais', value: 'BUT'}],
  bddScriptLanguage: 'FRENCH',
  allowTcModifDuringExec: true,
  activatedPlugins: {
    [WorkspaceTypeForPlugins.CAMPAIGN_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.TEST_CASE_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.REQUIREMENT_WORKSPACE]: [],
  }
} as Project;

export class ReferentialDataProvider {
  constructor(private httpRequestMock: HttpMock<ReferentialData>) {

  }

  wait(): void {
    cy.wait(this.httpRequestMock.alias)
      .its('status')
      .should('eq', HTTP_RESPONSE_STATUS.SUCCESS);
    // .should('have.property', 'status', HTTP_RESPONSE_STATUS.SUCCESS);
  }

  waitForReferentialData(): Chainable<ReferentialData> {
    return cy.wait(this.httpRequestMock.alias)
      .then(xhr => xhr.responseBody as ReferentialData);
  }

}

export class ReferentialDataProviderBuilder {

  constructor(private referentialData: ReferentialData = basicReferentialData) {
  }

  build(): ReferentialDataProvider {
    const httpRequestMock = new HttpMockBuilder<ReferentialData>('referential').responseBody(this.referentialData).build();
    return new ReferentialDataProvider(httpRequestMock);
  }
}

export const basicReferentialData: ReferentialData = {
  projects: [
    project1
  ],
  customFields: [],
  filteredProjectIds: [],
  projectFilterStatus: false,
  infoLists: getSystemInfoLists(),
  globalConfiguration: {
    milestoneFeatureEnabled: false,
    uploadFileExtensionWhitelist: ['txt'],
    uploadFileSizeLimit: 50000
  },
  bugTrackers: [],
  milestones: [],
  automationServers: [],
  user: {
    username: 'admin',
    userId: 1,
    admin: true,
    projectManager: false,
    functionalTester: false,
    automationProgrammer: false,
    lastName: 'admin',
    firstName: '',
  },
  milestoneFilterState: {
    milestoneModeEnabled: false,
    selectedMilestoneId: null
  },
  requirementVersionLinkTypes: [],
  workspacePlugins: [],
  workspaceWizards: [],
  licenseInformation: {
    pluginLicenseExpiration: null,
    activatedUserExcess: null,
  },
  automatedTestTechnologies: [],
  availableTestAutomationServerKinds: [],
  scmServers: [],
  templateConfigurablePlugins: [],
};
