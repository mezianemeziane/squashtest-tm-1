import {Milestone} from '../../model/milestone/milestone.model';
import {GlobalConfiguration, LicenseInformation, ReferentialData} from '../../model/referential-data.model';
import {AuthenticatedUser} from '../../model/user/authenticated-user.model';
import {Project, WorkspaceTypeForPlugins} from '../../model/project/project.model';
import {BindableEntity} from '../../model/bindable-entity.model';
import {ADMIN_PERMISSIONS, ProjectPermissions} from '../../model/permissions/permissions.model';
import {CustomField, InputType} from '../../model/customfield/customfield.model';
import {CustomFieldBinding} from '../../model/customfield/custom-field-binding.model';
import {BugTrackerBinding} from '../../model/bugtracker/bug-tracker.binding';
import {CustomFieldOption} from '../../model/customfield/custom-field-option.model';
import {InfoListItem} from '../../model/infolist/infolistitem.model';
import {InfoList} from '../../model/infolist/infolist.model';
import {AuthenticationPolicy, AuthenticationProtocol, BugTracker} from '../../model/bugtracker/bug-tracker.model';
import Omit = Cypress.Omit;

function debug(...args: any[]): void {
  // console.log(args);
}

export type BoundMilestone = Omit<Partial<Milestone>, 'id'> & { boundProjectIndexes?: number[] };

/**
 * Allows to bind a CustomField to different entities and to different projects at once.
 * @see {@link BoundCustomField}
 */
interface CustomFieldQuickBindings {
  // Projects you want this custom field to be bounded to
  projectIndexes: number[];
  // A list a entities to bind to
  bindableEntities: BindableEntity[];
}

/**
 * A CustomFieldOption without id. The code is optional and during build it will be set to
 * the same value as the label if not provided.
 * @see {@link CustomFieldOption}
 */
export interface PartialCustomFieldOption {
  label: string;
  colour?: string;
  code?: string;
}

/**
 * A custom field without its id and with its bindings.
 */
export interface BoundCustomField {
  code: string;
  label: string;
  name: string;
  defaultValue?: string;
  largeDefaultValue?: string;
  numericDefaultValue?: number;
  inputType: InputType;
  optional: boolean;
  options?: PartialCustomFieldOption[];
  bindings: CustomFieldQuickBindings[];
}

/**
 * A partial {@link Project} without its id and without its bindings.
 */
interface PartialProject {
  uri?: string;
  name?: string;
  label?: string;
  testCaseNatureId?: number;
  testCaseTypeId?: number;
  requirementCategoryId?: number;
  allowAutomationWorkflow?: boolean;
  permissions?: ProjectPermissions;
  bugTrackerBinding?: BugTrackerBinding;
  taServerId?: number;
  automationWorkflowType?: string;
}

export interface BoundInfoList {
  uri?: string;
  code?: string;
  label?: string;
  description?: string;
  boundToProject: InfoListProjectBinding[];
}

export interface UninitializedItemsHolder {
  items: Partial<Omit<InfoListItem, 'id' | 'itemIndex'>>[];
}

export interface InitializedItemsHolder {
  items: InfoListItemWithId[];
}

export interface InfoListProjectBinding {
  projectIndex: number;
  role: InfoListBindingAttribute;
}

export type InfoListItemWithId = Partial<InfoListItem> & WithId;
export type InfoListWithIdAndItems = BoundInfoList & WithId & InitializedItemsHolder;
export type PartialList = BoundInfoList & UninitializedItemsHolder;

export type InfoListBindingAttribute = 'testCaseNature' | 'testCaseType' | 'requirementCategory';


// The ids are not exposed before the build phase. These types are only used inside the builder
export interface WithId {
  id: number;
}

export interface PartialBugTracker {
  name?: string;
  url?: string;
  kind?: string;
  authPolicy?: AuthenticationPolicy;
  authProtocol?: AuthenticationProtocol;
  iframeFriendly?: boolean;
}

function getDefaultAuthenticatedUser(): AuthenticatedUser {
  return {
    admin: false,
    automationProgrammer: false,
    firstName: '',
    functionalTester: false,
    lastName: '',
    projectManager: false,
    userId: 0,
    username: ''
  };
}

type BoundMilestoneWithId = BoundMilestone & WithId;
type BoundCustomFieldWithId = BoundCustomField & WithId;

/**
 * A mock builder for referential and projects data. It provides default values and shortcuts so that it's faster
 * to come with referential data that suits your test needs.
 *
 * The builder abstracts the ids away so when you need to bind entities to a project you just need to specify its
 * index based on the insertion order.
 *
 * @see ./default-referential-data.const.ts for a usage example.
 */
export class ReferentialDataMockBuilder {

  private readonly _data: ReferentialData;
  private _boundMilestones: BoundMilestoneWithId[] = [];
  private _boundCustomFields: BoundCustomFieldWithId[] = [];
  private _boundInfoLists: InfoListWithIdAndItems[] = [];

  constructor() {
    // new instance of default referential data to avoid propagation of mutations that may occurs afterward.
    this._data = getDefaultReferentialData();
  }

  public withUser(user: Partial<AuthenticatedUser>): this {
    this._data.user = {
      ...getDefaultAuthenticatedUser(),
      ...user,
    };
    return this;
  }

  public withMilestones(...milestones: BoundMilestone[]): this {
    this._boundMilestones = milestones.map((partial: BoundMilestone, id: number): BoundMilestoneWithId => {
      const defaultValues: Partial<Milestone> = {
        endDate: new Date(Date.now()),
        status: 'IN_PROGRESS',
        description: '',
        label: `milestone ${id + 1}`,
      };

      return {
        ...defaultValues,
        ...partial,
        id: id + 1, // Something along the way won't allow milestones to have id = 0
      };
    });

    return this;
  }

  public withProjects(...projects: PartialProject[]): this {
    debug('ADDING PROJECTS ');
    debug(this._data.projects);
    debug('DEFAULT PROJECT ');
    // debug(defaultProject); <-- this const was muted somewhere, maybe a test mute that ?
    // Anyway it shouldn't corrupt everything even if a test is badly written.
    this._data.projects = projects.map((partial: PartialProject, id: number): Project => {
      return {
        ...getEmptyProject(), // <-- calling function to obtain a clean empty project.
        // I don't know where but this thing is muted afterward, and it corrupt default project and thus all project generation.
        ...partial,
        id: id + 1,
      };
    });
    debug('ADDED PROJECTS ');
    debug(this._data.projects);

    return this;
  }

  public withCustomFields(...customFields: BoundCustomField[]): this {
    this._boundCustomFields = customFields.map((cf, id) => {
      return {
        options: [],
        ...cf,
        id
      };
    });
    return this;
  }

  public withInfoLists(...infoLists: PartialList[]): this {
    this._boundInfoLists = infoLists.map((partialList, index) => {
      const id = index + 4;
      const items: (Partial<InfoListItem> & WithId)[] = partialList.items.map((item, itemIndex) => {
        return {
          ...item,
          id: id * 100 + itemIndex,
          itemIndex,
        };
      });
      return {
        id: id, // <-- there is 3 default system infoList before custom ones,
        ...partialList,
        items,
      };
    });
    return this;
  }

  public withBugTrackers(...bugTrackers: PartialBugTracker[]) {
    this._data.bugTrackers = bugTrackers.map((partial: PartialBugTracker, id: number): BugTracker => {
      const newBugTracker = {} as BugTracker;
      return {...newBugTracker, ...partial, id: id + 1};
    });

    return this;
  }

  withLicenseInformation(licenseInformation: LicenseInformation): this {
    this._data.licenseInformation = licenseInformation;
    return this;
  }

  withGlobalConfiguration(globalConfiguration: GlobalConfiguration): this {
    this._data.globalConfiguration = globalConfiguration;
    return this;
  }

  public build(): ReferentialData {
    let milestoneBindingId = 1;
    debug('BUILDING PROJECTS');
    debug(this._data.projects);

    // Bind milestones to projects
    this._boundMilestones.forEach((boundMilestone) => {
      boundMilestone.boundProjectIndexes.forEach((projectIndex) => {
        this._data.projects[projectIndex].milestoneBindings.push({
          id: milestoneBindingId++,
          projectId: projectIndex + 1,
          milestoneId: boundMilestone.id,
        });
      });
    });

    debug(this._data.projects);

    // Bind custom fields
    let customFieldBindingId = 1;

    this._boundCustomFields.forEach((boundCustomField) => {
      boundCustomField.bindings.forEach((multipleBindings) => {
        multipleBindings.bindableEntities.forEach((entity) => {
          multipleBindings.projectIndexes.forEach((projectIndex) => {
            const binding: CustomFieldBinding = {
              bindableEntity: entity,
              boundProjectId: projectIndex + 1,
              customFieldId: boundCustomField.id,
              id: customFieldBindingId++,
              renderingLocations: [],
              position: -1,
              // Position is set later on
            };

            debug('Binding cuf to ' + projectIndex);
            debug('Cuf is  ' + boundCustomField.id);
            this._data.projects[projectIndex].customFieldBindings[entity].push(binding);
          });
        });
      });
    });

    debug(this._data.projects);

    // Reset CF binding positions
    this._data.projects.forEach((project) => {
      for (const entity of Object.keys(project.customFieldBindings)) {
        project.customFieldBindings[entity].forEach((customFieldBinding: CustomFieldBinding, index: number) => {
          customFieldBinding.position = index;
        });
      }
    });

    const customFields: CustomField[] = this._boundCustomFields.map((bound) => {
      const mappedOptions: CustomFieldOption[] = bound.options.map((boundOption, index) => {
        return {
          colour: '',
          code: boundOption.label,
          ...boundOption,
          id: index + 1,
          position: index,
          cfId: bound.id,
        };
      });

      return {
        ...bound,
        options: mappedOptions,
      };
    });

    // Build user custom info-list
    const infoLists = this.generateInfoLists();

    // concat with system info list
    this._data.infoLists = this._data.infoLists.concat(infoLists);

    // bind user custom info list to projects
    this.bindInfoListToProjects();

    debug(this._data.projects);

    return {
      ...this._data,
      milestones: [...this._boundMilestones] as Milestone[],
      customFields
    };
  }

  private generateInfoLists(): InfoList[] {
    return this._boundInfoLists.map(boundedList => {
      const items = this.generateInfoListItems(boundedList);
      return this.generateInfoList(boundedList, items);
    });
  }

  private generateInfoList(boundedList: InfoListWithIdAndItems, items: InfoListItem[]): InfoList {
    return {
      id: boundedList.id,
      label: boundedList.label ? boundedList.label : `User Info List ${boundedList.id}`,
      code: boundedList.code ? boundedList.code : `USR_LIST_GEN_CODE_${boundedList.id}`,
      uri: boundedList.uri ? boundedList.uri : '',
      description: boundedList.description ? boundedList.description : `Generated description for info list ${boundedList.id}`,
      items
    };
  }

  private generateInfoListItems(boundedList: InfoListWithIdAndItems): InfoListItem[] {
    return boundedList.items.map((boundedListItem: InfoListItemWithId) => {
      return {
        id: boundedListItem.id,
        uri: boundedListItem.uri ? boundedListItem.uri : '',
        code: boundedListItem.code ? boundedListItem.code : `USR_ITEM_GEN_CODE_${boundedListItem.id}`,
        label: boundedListItem.label ? boundedListItem.label : `User Item ${boundedListItem.id}`,
        friendlyLabel: boundedListItem.friendlyLabel ? boundedListItem.friendlyLabel : `User Item ${boundedListItem.id}`,
        iconName: boundedListItem.iconName ? boundedListItem.label : 'noicon',
        itemIndex: boundedListItem.itemIndex,
        default: boundedListItem.default,
        system: false,
        denormalized: false
      };
    });
  }

  private bindInfoListToProjects() {
    this._boundInfoLists.forEach(boundedList => {
      boundedList.boundToProject.forEach(binding => {
        const project = this._data.projects[binding.projectIndex];
        switch (binding.role) {
          case 'requirementCategory':
            project.requirementCategoryId = boundedList.id;
            break;
          case 'testCaseNature':
            project.testCaseNatureId = boundedList.id;
            break;
          case 'testCaseType':
            project.testCaseTypeId = boundedList.id;
            break;
        }
      });
    });
  }
}

// ============================================================

export const NO_PROJECT_PERMISSIONS: ProjectPermissions = {
  AUTOMATION_REQUEST_LIBRARY: [],
  CUSTOM_REPORT_LIBRARY: [],
  PROJECT: [],
  PROJECT_TEMPLATE: [],
  REQUIREMENT_LIBRARY: [],
  TEST_CASE_LIBRARY: [],
  CAMPAIGN_LIBRARY: [],
  ACTION_WORD_LIBRARY: [],
};

export const ALL_PROJECT_PERMISSIONS: ProjectPermissions = ADMIN_PERMISSIONS;

export const ALL_BINDABLE_ENTITIES: BindableEntity[] = Object.values(BindableEntity);

// ============================================================

export function getEmptyProject(): Project {
  return {
    id: 1,
    milestoneBindings: [],
    testCaseNatureId: 2,
    testCaseTypeId: 3,
    customFieldBindings: {
      [BindableEntity.REQUIREMENT_FOLDER]: [],
      [BindableEntity.REQUIREMENT_VERSION]: [],
      [BindableEntity.TESTCASE_FOLDER]: [],
      [BindableEntity.TEST_CASE]: [],
      [BindableEntity.TEST_STEP]: [],
      [BindableEntity.CAMPAIGN_FOLDER]: [],
      [BindableEntity.CAMPAIGN]: [],
      [BindableEntity.ITERATION]: [],
      [BindableEntity.TEST_SUITE]: [],
      [BindableEntity.EXECUTION]: [],
      [BindableEntity.EXECUTION_STEP]: [],
      [BindableEntity.CUSTOM_REPORT_FOLDER]: [],
    },
    permissions: NO_PROJECT_PERMISSIONS,
    taServerId: null,
    allowAutomationWorkflow: false,
    automationWorkflowType: 'NONE',
    bugTrackerBinding: null,
    label: '',
    name: 'Project',
    requirementCategoryId: 1,
    uri: null,
    description: '',
    disabledExecutionStatus: [],
    bddScriptLanguage: 'FRENCH',
    keywords: [{
      label: 'Étant donné que', value: 'GIVEN'
    }, {label: 'Quand', value: 'WHEN'},
      {label: 'Alors', value: 'THEN'}, {label: 'Et', value: 'AND'}, {label: 'Mais', value: 'BUT'}],
    allowTcModifDuringExec: false,
    activatedPlugins: {
      [WorkspaceTypeForPlugins.CAMPAIGN_WORKSPACE]: [],
      [WorkspaceTypeForPlugins.TEST_CASE_WORKSPACE]: [],
      [WorkspaceTypeForPlugins.REQUIREMENT_WORKSPACE]: [],
    },
    automatedSuitesLifetime: 0,
    bddImplementationTechnology: '',
  };
}

export function getSystemInfoLists() {
  return [
    {
      'id': 1,
      'uri': null,
      'code': 'DEF_REQ_CAT',
      'label': 'infolist.category.default',
      'description': '',
      'items': [
        {
          'id': 1,
          'uri': null,
          'code': 'CAT_FUNCTIONAL',
          'label': 'requirement.category.CAT_FUNCTIONAL',
          'friendlyLabel': null,
          'iconName': 'monitor',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 2,
          'uri': null,
          'code': 'CAT_NON_FUNCTIONAL',
          'label': 'requirement.category.CAT_NON_FUNCTIONAL',
          'friendlyLabel': null,
          'iconName': 'server',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 3,
          'uri': null,
          'code': 'CAT_USE_CASE',
          'label': 'requirement.category.CAT_USE_CASE',
          'friendlyLabel': null,
          'iconName': 'read',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 4,
          'uri': null,
          'code': 'CAT_BUSINESS',
          'label': 'requirement.category.CAT_BUSINESS',
          'friendlyLabel': null,
          'iconName': 'briefcase',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 5,
          'uri': null,
          'code': 'CAT_TEST_REQUIREMENT',
          'label': 'requirement.category.CAT_TEST_REQUIREMENT',
          'friendlyLabel': null,
          'iconName': 'checked_checkbox',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 6,
          'uri': null,
          'code': 'CAT_UNDEFINED',
          'label': 'requirement.category.CAT_UNDEFINED',
          'friendlyLabel': null,
          'iconName': 'indeterminate_checkbox_empty',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 7,
          'uri': null,
          'code': 'CAT_ERGONOMIC',
          'label': 'requirement.category.CAT_ERGONOMIC',
          'friendlyLabel': null,
          'iconName': 'puzzle',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 8,
          'uri': null,
          'code': 'CAT_PERFORMANCE',
          'label': 'requirement.category.CAT_PERFORMANCE',
          'friendlyLabel': null,
          'iconName': 'dashboard',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 9,
          'uri': null,
          'code': 'CAT_TECHNICAL',
          'label': 'requirement.category.CAT_TECHNICAL',
          'friendlyLabel': null,
          'iconName': 'key',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 10,
          'uri': null,
          'code': 'CAT_USER_STORY',
          'label': 'requirement.category.CAT_USER_STORY',
          'friendlyLabel': null,
          'iconName': 'bookmark',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 11,
          'uri': null,
          'code': 'CAT_SECURITY',
          'label': 'requirement.category.CAT_SECURITY',
          'friendlyLabel': null,
          'iconName': 'protect',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }
      ]
    }, {
      'id': 2,
      'uri': null,
      'code': 'DEF_TC_NAT',
      'label': 'infolist.nature.default',
      'description': '',
      'items': [
        {
          'id': 12,
          'uri': null,
          'code': 'NAT_UNDEFINED',
          'label': 'test-case.nature.NAT_UNDEFINED',
          'friendlyLabel': null,
          'iconName': 'indeterminate_checkbox_empty',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 13,
          'uri': null,
          'code': 'NAT_FUNCTIONAL_TESTING',
          'label': 'test-case.nature.NAT_FUNCTIONAL_TESTING',
          'friendlyLabel': null,
          'iconName': 'monitor',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 14,
          'uri': null,
          'code': 'NAT_BUSINESS_TESTING',
          'label': 'test-case.nature.NAT_BUSINESS_TESTING',
          'friendlyLabel': null,
          'iconName': 'briefcase',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 15,
          'uri': null,
          'code': 'NAT_USER_TESTING',
          'label': 'test-case.nature.NAT_USER_TESTING',
          'friendlyLabel': null,
          'iconName': 'user',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 16,
          'uri': null,
          'code': 'NAT_NON_FUNCTIONAL_TESTING',
          'label': 'test-case.nature.NAT_NON_FUNCTIONAL_TESTING',
          'friendlyLabel': null,
          'iconName': 'server',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 17,
          'uri': null,
          'code': 'NAT_PERFORMANCE_TESTING',
          'label': 'test-case.nature.NAT_PERFORMANCE_TESTING',
          'friendlyLabel': null,
          'iconName': 'dashboard',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 18,
          'uri': null,
          'code': 'NAT_SECURITY_TESTING',
          'label': 'test-case.nature.NAT_SECURITY_TESTING',
          'friendlyLabel': null,
          'iconName': 'protect',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 19,
          'uri': null,
          'code': 'NAT_ATDD',
          'label': 'test-case.nature.NAT_ATDD',
          'friendlyLabel': null,
          'iconName': 'circular_arrows',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }
      ]
    }, {
      'id': 3,
      'uri': null,
      'code': 'DEF_TC_TYP',
      'label': 'infolist.type.default',
      'description': '',
      'items': [
        {
          'id': 20,
          'uri': null,
          'code': 'TYP_UNDEFINED',
          'label': 'test-case.type.TYP_UNDEFINED',
          'friendlyLabel': null,
          'iconName': 'indeterminate_checkbox_empty',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 21,
          'uri': null,
          'code': 'TYP_COMPLIANCE_TESTING',
          'label': 'test-case.type.TYP_COMPLIANCE_TESTING',
          'friendlyLabel': null,
          'iconName': 'task_completed',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 22,
          'uri': null,
          'code': 'TYP_CORRECTION_TESTING',
          'label': 'test-case.type.TYP_CORRECTION_TESTING',
          'friendlyLabel': null,
          'iconName': 'bug',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 23,
          'uri': null,
          'code': 'TYP_EVOLUTION_TESTING',
          'label': 'test-case.type.TYP_EVOLUTION_TESTING',
          'friendlyLabel': null,
          'iconName': 'circled_up_right',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 24,
          'uri': null,
          'code': 'TYP_REGRESSION_TESTING',
          'label': 'test-case.type.TYP_REGRESSION_TESTING',
          'friendlyLabel': null,
          'iconName': 'circular_arrows',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 25,
          'uri': null,
          'code': 'TYP_END_TO_END_TESTING',
          'label': 'test-case.type.TYP_END_TO_END_TESTING',
          'friendlyLabel': null,
          'iconName': 'journey',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 26,
          'uri': null,
          'code': 'TYP_PARTNER_TESTING',
          'label': 'test-case.type.TYP_PARTNER_TESTING',
          'friendlyLabel': null,
          'iconName': 'handshake',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }
      ]
    }
  ];
}

export function getDefaultReferentialData() {
  const defaultReferentialData: ReferentialData = {
    projects: [],
    customFields: [],
    filteredProjectIds: [],
    user: {
      username: 'user',
      userId: 1,
      admin: false,
      projectManager: false,
      functionalTester: false,
      automationProgrammer: false,
      firstName: '',
      lastName: 'user',
    },
    projectFilterStatus: false,
    globalConfiguration: {
      milestoneFeatureEnabled: true,
      uploadFileExtensionWhitelist: ['txt'],
      uploadFileSizeLimit: 50000
    },
    bugTrackers: [],
    milestones: [
      {
        id: 1,
        label: 'Milestone 1',
        description: '',
        status: 'PLANNED',
        endDate: new Date(Date.now()),
        ownerFistName: 'admin',
        ownerLastName: 'admin',
        ownerLogin: 'admin',
        range: 'GLOBAL',
        lastModifiedBy: null,
        lastModifiedOn: null,
        createdOn: new Date(Date.now()),
        createdBy: 'cypress',
      },
      {
        id: 2,
        label: 'Milestone 2',
        description: '',
        status: 'IN_PROGRESS',
        endDate: new Date(Date.now()),
        ownerFistName: 'admin',
        ownerLastName: 'admin',
        ownerLogin: 'admin',
        range: 'GLOBAL',
        lastModifiedBy: null,
        lastModifiedOn: null,
        createdOn: new Date(Date.now()),
        createdBy: 'cypress',
      },
      {
        id: 3,
        label: 'Milestone 3',
        description: '',
        status: 'FINISHED',
        endDate: new Date(Date.now()),
        ownerFistName: 'admin',
        ownerLastName: 'admin',
        ownerLogin: 'admin',
        range: 'GLOBAL',
        lastModifiedBy: null,
        lastModifiedOn: null,
        createdOn: new Date(Date.now()),
        createdBy: 'cypress',
      },
      {
        id: 4,
        label: 'Milestone 4',
        description: '',
        status: 'LOCKED',
        endDate: new Date(Date.now()),
        ownerFistName: 'admin',
        ownerLastName: 'admin',
        ownerLogin: 'admin',
        range: 'GLOBAL',
        lastModifiedBy: null,
        lastModifiedOn: null,
        createdOn: new Date(Date.now()),
        createdBy: 'cypress',
      }
    ],
    automationServers: [],
    infoLists: getSystemInfoLists(),
    milestoneFilterState: {
      milestoneModeEnabled: false,
      selectedMilestoneId: null
    },
    requirementVersionLinkTypes: [
      {
        id: 1,
        role: 'requirement-version.link.type.related',
        role1Code: 'RELATED',
        role2: 'requirement-version.link.type.related',
        role2Code: 'RELATED',
        default: true,
        linkCount: 0,
      },
      {
        id: 2,
        role: 'requirement-version.link.type.parent',
        role1Code: 'PARENT',
        role2: 'requirement-version.link.type.child',
        role2Code: 'CHILD',
        default: false,
        linkCount: 0,
      },
      {
        id: 3,
        role: 'requirement-version.link.type.duplicate',
        role1Code: 'DUPLICATE',
        role2: 'requirement-version.link.type.duplicate',
        role2Code: 'DUPLICATE',
        default: false,
        linkCount: 0,
      },
    ],
    workspacePlugins: [],
    workspaceWizards: [],
    licenseInformation: {
      activatedUserExcess: null,
      pluginLicenseExpiration: null,
    },
    automatedTestTechnologies: [
      {id: 1, name: 'Robot Framework', actionProviderKey: 'robotframework/execute@v1'},
      {id: 2, name: 'Cypress', actionProviderKey: 'cypress/execute@v1'},
      {id: 3, name: 'JUnit', actionProviderKey: 'junit/execute@v1'},
      {id: 4, name: 'Cucumber', actionProviderKey: 'cucumber/execute@v1'},
    ],
    availableTestAutomationServerKinds: [],
    scmServers: [],
    templateConfigurablePlugins: [],
  };
  return defaultReferentialData;
}
