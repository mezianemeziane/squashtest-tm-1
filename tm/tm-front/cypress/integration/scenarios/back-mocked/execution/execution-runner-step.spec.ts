import {ExecutionRunnerProloguePage} from '../../../page-objects/pages/execution/execution-runner-prologue-page';
import {ExecutionModel, ExecutionStepModel} from '../../../model/execution/execution.model';
import {mockExecutionModel} from '../../../data-mock/execution.data-mock';


const action = `<p>a nice action</p>
<table><tbody><tr><th>Col1</th><th>Col2</th></tr><tr><td>Col1L1</td><td>Col2L1</td></tr></tbody></table>`;

const expectedResult = `<p>a nice result</p>
<table><tbody><tr><th>Col1</th><th>Col2</th></tr><tr><td>Col1L1</td><td>Col2L1</td></tr></tbody></table>`;

const comment = `<p>a nice comment</p>
<table><tbody><tr><th>Col1</th><th>Col2</th></tr><tr><td>Col1L1</td><td>Col2L1</td></tr></tbody></table>`;

function getExecutionModel(): ExecutionModel {
  return mockExecutionModel({
    executionStepViews: [
      {
        id: 1,
        order: 0,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: [
            {id: 1, addedOn: new Date('25 Mar 2020 10:12:57'), name: 'attachment-1', size: 56789},
            {id: 2, addedOn: new Date('17 Mar 2019 20:40:00'), name: 'attachment-2', size: 1234567},
          ]
        },
        action,
        expectedResult,
        comment,
        customFieldValues: [],
        projectId: 1,
        lastExecutedBy: 'admin',
        lastExecutedOn: null,
        denormalizedCustomFieldValues: []
      },
      {
        id: 2,
        order: 1,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel,
      {
        id: 3,
        order: 2,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel
    ],
  });
}

describe('Execution runner step', function () {
  it('it should display step', () => {
    const executionModel: ExecutionModel = getExecutionModel();
    const prologuePage = ExecutionRunnerProloguePage.initTestAtPage(1, executionModel);

    const firstStepPage = prologuePage.startExecution();
    firstStepPage.assertExist();
    firstStepPage.checkExecutionStepper(1, 3);
    firstStepPage.checkDocumentTitle('#3 - NX - Test Case 1 (1/3)');
    firstStepPage.checkAction(action);
    firstStepPage.checkExpectedResult(expectedResult);
    firstStepPage.checkComment(comment);
    firstStepPage.checkModificationDuringExecutionIsVisible();
  });

  it('it should update comment', () => {
    const executionModel: ExecutionModel = getExecutionModel();
    const prologuePage = ExecutionRunnerProloguePage.initTestAtPage(1, executionModel);
    const firstStepPage = prologuePage.startExecution();
    firstStepPage.assertExist();
    firstStepPage.updateComment('new comment');
  });

  it('it should show attachments information', () => {
    const executionModel: ExecutionModel = getExecutionModel();
    const prologuePage = ExecutionRunnerProloguePage.initTestAtPage(1, executionModel);
    const firstStepPage = prologuePage.startExecution();
    firstStepPage.attachmentCompactList.checkAttachmentData(0, 'attachment-1', '55,46 Ko', 'Ajouté le 25/03/2020 10:12');
    firstStepPage.attachmentCompactList.checkAttachmentData(1, 'attachment-2', '1,18 Mo', 'Ajouté le 17/03/2019 20:40');
  });

  it('it should do various operation on attachments ', () => {
    const executionModel: ExecutionModel = getExecutionModel();
    const prologuePage = ExecutionRunnerProloguePage.initTestAtPage(1, executionModel);
    const firstStepPage = prologuePage.startExecution();
    firstStepPage.attachmentCompactList.deleteAttachment(1);
    firstStepPage.attachmentCompactList.cancelDeleteAttachment(1);
    firstStepPage.attachmentCompactList.deleteAttachment(1);
    firstStepPage.attachmentCompactList.confirmDeleteAttachment(1, 0, 1, 2, 1);
    firstStepPage.attachmentCompactList.deleteAttachment(0);
    firstStepPage.attachmentCompactList.confirmDeleteAttachment(0, 0, 1, 1, 1);
    firstStepPage.addAttachments(
      [
        new File(['last flight'], 'STS_135.txt'),
        new File(['last flight'], 'STS_132.txt'),
        new File(['last flight'], 'STS_134.txt'),
      ],
      1,
      [
        {
          status: '',
          iStatus: 0,
          attachmentDto: {
            name: 'STS_135.txt',
            size: 123456,
            addedOn: new Date('25 Mar 2020 10:12:57'),
            id: 12,
          }
        },
        {
          status: '',
          iStatus: 0,
          attachmentDto: {
            name: 'STS_132.txt',
            size: 123456,
            addedOn: new Date('25 Mar 2020 10:12:57'),
            id: 13,
          }
        },
        {
          status: '',
          iStatus: 0,
          attachmentDto: {
            name: 'STS_134.txt',
            size: 123456,
            addedOn: new Date('25 Mar 2020 10:12:57'),
            id: 14,
          }
        }
      ]);
    firstStepPage.attachmentCompactList.checkAttachmentData(0, 'STS_135.txt', '120,56 Ko', 'Ajouté le 25/03/2020 10:12');
    firstStepPage.attachmentCompactList.checkAttachmentData(1,  'STS_132.txt', '120,56 Ko', 'Ajouté le 25/03/2020 10:12');
    firstStepPage.attachmentCompactList.checkAttachmentData(2,  'STS_134.txt', '120,56 Ko', 'Ajouté le 25/03/2020 10:12');
  });


});
