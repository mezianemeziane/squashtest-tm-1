import {AdvancedIssue, BTIssue} from '../../../model/issue/remote-issue.model';

export function mockBTIssueCreateModel(): BTIssue {
  return {
    'id': null,
    'summary': null,
    'project': {
      'id': '1',
      'name': 'MyProject',
      'priorities': [
        {'id': '10', 'name': 'feature', 'dummy': false},
        {'id': '20', 'name': 'trivial', 'dummy': false},
        {'id': '30', 'name': 'text', 'dummy': false},
        {'id': '40', 'name': 'tweak', 'dummy': false},
        {'id': '50', 'name': 'minor', 'dummy': false},
        {'id': '60', 'name': 'major', 'dummy': false},
        {'id': '70', 'name': 'crash', 'dummy': false},
        {'id': '80', 'name': 'block', 'dummy': false}
      ],
      'versions': [
        {'id': '3', 'name': '0.3', 'dummy': false},
        {'id': '2', 'name': '0.2', 'dummy': false},
        {'id': '1', 'name': '0.1', 'dummy': false}
      ],
      'users': [
        {
          'id': '1', 'name': 'administrator', 'permissions': [
            {'id': '10', 'name': 'viewer', 'dummy': false},
            {'id': '25', 'name': 'reporter', 'dummy': false},
            {'id': '40', 'name': 'updater', 'dummy': false},
            {'id': '55', 'name': 'developer', 'dummy': false},
            {'id': '70', 'name': 'manager', 'dummy': false},
            {'id': '90', 'name': 'administrator', 'dummy': false}
          ],
          'dummy': false
        },
        {
          'id': '2',
          'name': 'squash',
          'permissions': [
            {'id': '10', 'name': 'viewer', 'dummy': false},
            {'id': '25', 'name': 'reporter', 'dummy': false},
            {'id': '40', 'name': 'updater', 'dummy': false},
            {'id': '55', 'name': 'developer', 'dummy': false},
            {'id': '70', 'name': 'manager', 'dummy': false},
            {'id': '90', 'name': 'administrator', 'dummy': false}
          ],
          'dummy': false
        }
      ],
      'categories': [
        {'id': '0', 'name': 'Bug', 'dummy': false},
        {'id': '1', 'name': 'Feature', 'dummy': false},
        {'id': '2', 'name': 'General', 'dummy': false}
      ],
      'defaultIssuePriority': {'id': '50', 'name': 'minor', 'dummy': false},
      'dummy': false
    },
    'priority': {'id': '50', 'name': 'minor', 'dummy': false},
    'version': null,
    'reporter': null,
    'category': null,
    'assignee': null,
    'description': '# Cas de test: [] TEST CS\n# Exécution: http://localhost:4200/squash/executions/31\n\n# Description de l\'anomalie :\n',
    'comment': null,
    'createdOn': null,
    'status': null,
    'bugtracker': 'mantis-pcktest2',
  } as unknown as BTIssue;
}

export function mockExistingBTIssue(): BTIssue {
  return {
    'id': 'abc',
    'project': {id: 1, name: 'project'},
    'summary': 'sumary',
    'priority': {'id': '50', 'name': 'minor', 'dummy': false},
    'version': {id: 1, name: 'abc'},
    'reporter': {id: 1, name: 'abc'},
    'category': {id: 1, name: 'abc'},
    'assignee': {id: 1, name: 'abc'},
    'description': '# Cas de test: [] TEST CS\n# Exécution: http://localhost:4200/squash/executions/31\n\n# Description de l\'anomalie :\n',
    'comment': 'comment',
    'createdOn': null,
    'status': {id: 1, name: 'abc'},
    'bugtracker': 'mantis-pcktest2',
  } as unknown as BTIssue;
}

// Project's schemes are truncated : only 'issuetype:Bug' and 'issuetype:Nouvelle fonctionnalité' were kept.
// It means the dialog will totally fail if 'issuetype' is switched to any unsupported value.
export function mockAdvancedIssueCreateModel(): AdvancedIssue {
  return {
    'fieldValues': {
      'summary': null,
      'issuetype': null,
      'components': null,
      'description': {
        'id': 'description',
        'typename': 'description',
        'scalar': '# Cas de test: [] 123\n# Exécution: http://localhost:4200/squash/executions/26\n\n# Description de l\'anomalie :\n',
        'composite': [],
        'custom': null,
        'name': '# Cas de test: [] 123\n# Exécution: http://localhost:4200/squash/executions/26\n\n# Description de l\'anomalie :\n'
      },
      'reporter': {
        'id': 'reporter',
        'typename': null,
        'scalar': 'supportsquash',
        'composite': [],
        'custom': null,
        'name': 'supportsquash'
      },
      'fixVersions': null,
      'priority': null,
      'labels': null,
      'environment': null,
      'attachment': null,
      'versions': null,
      'issuelinks': null,
      'assignee': null,
    },
    'project': {
      'id': 'PCK01',
      'name': 'PCK01',
      'schemes': {
        'issuetype:Bug': [
          {
            'id': 'issuetype',
            'label': 'Type de ticket',
            'possibleValues': [{
              'id': '10005',
              'typename': 'issuetype',
              'scalar': 'Bug',
              'composite': [],
              'custom': null,
              'name': 'Bug'
            }, {
              'id': '10004',
              'typename': 'issuetype',
              'scalar': 'Nouvelle fonctionnalité',
              'composite': [],
              'custom': null,
              'name': 'Nouvelle fonctionnalité'
            }, {
              'id': '10002',
              'typename': 'issuetype',
              'scalar': 'Tâche',
              'composite': [],
              'custom': null,
              'name': 'Tâche'
            }, {
              'id': '10006',
              'typename': 'issuetype',
              'scalar': 'Story',
              'composite': [],
              'custom': null,
              'name': 'Story'
            }, {
              'id': '10001',
              'typename': 'issuetype',
              'scalar': 'Amélioration',
              'composite': [],
              'custom': null,
              'name': 'Amélioration'
            }, {
              'id': '10031',
              'typename': 'issuetype',
              'scalar': 'typeLEL',
              'composite': [],
              'custom': null,
              'name': 'typeLEL'
            }, {
              'id': '10003',
              'typename': 'issuetype',
              'scalar': 'Sous-tâche',
              'composite': [],
              'custom': null,
              'name': 'Sous-tâche'
            }, {
              'id': '10030',
              'typename': 'issuetype',
              'scalar': 'Anomalie_LEL',
              'composite': [],
              'custom': null,
              'name': 'Anomalie_LEL'
            }, {
              'id': '10026',
              'typename': 'issuetype',
              'scalar': 'Cuf_Dev',
              'composite': [],
              'custom': null,
              'name': 'Cuf_Dev'
            }, {
              'id': '10025',
              'typename': 'issuetype',
              'scalar': 'CUF SV',
              'composite': [],
              'custom': null,
              'name': 'CUF SV'
            }, {
              'id': '10000',
              'typename': 'issuetype',
              'scalar': 'Epic',
              'composite': [],
              'custom': null,
              'name': 'Epic'
            }, {
              'id': '10024',
              'typename': 'issuetype',
              'scalar': 'TypeFullCUF*',
              'composite': [],
              'custom': null,
              'name': 'TypeFullCUF*'
            }
            ],
            'rendering': {
              'operations': ['SET'],
              'inputType': {
                'name': 'dropdown_list',
                'original': 'issuetype',
                'dataType': null,
                'fieldSchemeSelector': true,
                'configuration': {}
              },
              'required': true
            }
          }, {
            'id': 'summary',
            'label': 'Résumé',
            'possibleValues': [],
            'rendering': {
              'operations': ['SET'],
              'inputType': {
                'name': 'text_field',
                'original': 'summary',
                'dataType': 'string',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': true
            }
          }, {
            'id': 'components',
            'label': 'Composants',
            'possibleValues': [],
            'rendering': {
              'operations': ['SET', 'ADD', 'REMOVE'],
              'inputType': {
                'name': 'tag_list',
                'original': 'components',
                'dataType': 'component-array',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'customfield_10091',
            'label': 'ALL_CUF_ChamTexteMultiligne',
            'possibleValues': [],
            'rendering': {
              'operations': ['SET'],
              'inputType': {
                'name': 'text_area',
                'original': 'com.atlassian.jira.plugin.system.customfieldtypes_textarea',
                'dataType': 'string',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'customfield_10095',
            'label': 'ALL_CUF_ListeCascade',

            'possibleValues': [{
              'id': '-1',
              'typename': 'string',
              'scalar': '',
              'composite': [],
              'custom': null,
              'name': ''
            }, {
              'id': '10165',
              'typename': null,
              'scalar': 'Option 1',
              'composite': [{
                'id': '10168',
                'typename': null,
                'scalar': 'A1',
                'composite': [],
                'custom': null,
                'name': 'A1'
              }, {
                'id': '10169',
                'typename': null,
                'scalar': 'A2',
                'composite': [],
                'custom': null,
                'name': 'A2'
              }
              ],
              'custom': null,
              'name': 'Option 1'
            }, {
              'id': '10166',
              'typename': null,
              'scalar': 'Option 2',
              'composite': [{
                'id': '10170',
                'typename': null,
                'scalar': 'B1',
                'composite': [],
                'custom': null,
                'name': 'B1'
              }, {
                'id': '10171',
                'typename': null,
                'scalar': 'B2',
                'composite': [],
                'custom': null,
                'name': 'B2'
              }
              ],
              'custom': null,
              'name': 'Option 2'
            }, {
              'id': '10167',
              'typename': null,
              'scalar': 'Option 3',
              'composite': [{
                'id': '10172',
                'typename': null,
                'scalar': 'C1',
                'composite': [],
                'custom': null,
                'name': 'C1'
              }, {
                'id': '10173',
                'typename': null,
                'scalar': 'C2',
                'composite': [],
                'custom': null,
                'name': 'C2'
              }
              ],
              'custom': null,
              'name': 'Option 3'
            }
            ],
            'rendering': {
              'operations': ['SET'],
              'inputType': {
                'name': 'cascading_select',
                'original': 'com.atlassian.jira.plugin.system.customfieldtypes_cascadingselect',
                'dataType': 'option-with-child',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'customfield_10096',
            'label': 'ALL_CUF_ListePlusieursChoix',
            'possibleValues': [{
              'id': '-1',
              'typename': 'string',
              'scalar': '',
              'composite': [],
              'custom': null,
              'name': ''
            }, {
              'id': '10115',
              'typename': null,
              'scalar': 'Suzuki',
              'composite': [],
              'custom': null,
              'name': 'Suzuki'
            }, {
              'id': '10116',
              'typename': null,
              'scalar': 'BMW',
              'composite': [],
              'custom': null,
              'name': 'BMW'
            }, {
              'id': '10117',
              'typename': null,
              'scalar': 'Kawasaki',
              'composite': [],
              'custom': null,
              'name': 'Kawasaki'
            }, {
              'id': '10118',
              'typename': null,
              'scalar': 'Peugeot',
              'composite': [],
              'custom': null,
              'name': 'Peugeot'
            }, {
              'id': '10119',
              'typename': null,
              'scalar': 'Harley Davidson',
              'composite': [],
              'custom': null,
              'name': 'Harley Davidson'
            }, {
              'id': '10120',
              'typename': null,
              'scalar': 'KMT',
              'composite': [],
              'custom': null,
              'name': 'KMT'
            }, {
              'id': '10121',
              'typename': null,
              'scalar': 'Triumph',
              'composite': [],
              'custom': null,
              'name': 'Triumph'
            }
            ],
            'rendering': {
              'operations': ['SET', 'ADD', 'REMOVE'],
              'inputType': {
                'name': 'multi_select',
                'original': 'com.atlassian.jira.plugin.system.customfieldtypes_multiselect',
                'dataType': 'option-array',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'description',
            'label': 'Description',
            'possibleValues': [],
            'rendering': {
              'operations': ['SET'],
              'inputType': {
                'name': 'text_area',
                'original': 'description',
                'dataType': 'string',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'customfield_10020',
            'label': 'Sprint',
            'possibleValues': [{
              'id': '-1',
              'typename': 'string',
              'scalar': '',
              'composite': [],
              'custom': null,
              'name': ''
            }, {
              'id': '44',
              'typename': 'string',
              'scalar': 'Connexio Sprint 3',
              'composite': [],
              'custom': null,
              'name': 'Connexio Sprint 3'
            }, {
              'id': '1',
              'typename': 'string',
              'scalar': 'Connexion Sprint 1 modif',
              'composite': [],
              'custom': null,
              'name': 'Connexion Sprint 1 modif'
            }, {
              'id': '10',
              'typename': 'string',
              'scalar': 'Dossier Sync tableau 2',
              'composite': [],
              'custom': null,
              'name': 'Dossier Sync tableau 2'
            }, {
              'id': '26',
              'typename': 'string',
              'scalar': 'Sample Sprint 2',
              'composite': [],
              'custom': null,
              'name': 'Sample Sprint 2'
            }, {
              'id': '29',
              'typename': 'string',
              'scalar': 'Sample Sprint 3',
              'composite': [],
              'custom': null,
              'name': 'Sample Sprint 3'
            }, {
              'id': '32',
              'typename': 'string',
              'scalar': 'Sample Sprint 4',
              'composite': [],
              'custom': null,
              'name': 'Sample Sprint 4'
            }, {
              'id': '33',
              'typename': 'string',
              'scalar': 'Sample Sprint 5',
              'composite': [],
              'custom': null,
              'name': 'Sample Sprint 5'
            }, {
              'id': '61',
              'typename': 'string',
              'scalar': 'Sprint 1',
              'composite': [],
              'custom': null,
              'name': 'Sprint 1'
            }, {
              'id': '54',
              'typename': 'string',
              'scalar': 'Sprint 2',
              'composite': [],
              'custom': null,
              'name': 'Sprint 2'
            }
            ],
            'rendering': {
              'operations': ['SET'],
              'inputType': {
                'name': 'dropdown_list',
                'original': 'com.pyxis.greenhopper.jira_gh-sprint',
                'dataType': 'json-array',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'customfield_10075',
            'label': 'Bouton-radio',
            'possibleValues': [{
              'id': '-1',
              'typename': 'string',
              'scalar': '',
              'composite': [],
              'custom': null,
              'name': ''
            }, {
              'id': '10032',
              'typename': null,
              'scalar': 'Alabama',
              'composite': [],
              'custom': null,
              'name': 'Alabama'
            }, {
              'id': '10033',
              'typename': null,
              'scalar': 'Connecticut',
              'composite': [],
              'custom': null,
              'name': 'Connecticut'
            }, {
              'id': '10034',
              'typename': null,
              'scalar': 'Delaware',
              'composite': [],
              'custom': null,
              'name': 'Delaware'
            }, {
              'id': '10035',
              'typename': null,
              'scalar': 'Iowa',
              'composite': [],
              'custom': null,
              'name': 'Iowa'
            }
            ],
            'rendering': {
              'operations': ['SET'],
              'inputType': {
                'name': 'dropdown_list',
                'original': 'com.atlassian.jira.plugin.system.customfieldtypes_radiobuttons',
                'dataType': 'select_option',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'reporter',
            'label': 'Rapporteur',
            'possibleValues': [],
            'rendering': {
              'operations': ['SET'],
              'inputType': {
                'name': 'text_field',
                'original': 'reporter',
                'dataType': 'user',
                'fieldSchemeSelector': false,
                'configuration': {
                  'onchange': 'searchreporter:'
                }
              },
              'required': true
            }
          }, {
            'id': 'customfield_10087',
            'label': 'ALL_CUF_BtnRadio',
            'possibleValues': [{
              'id': '-1',
              'typename': 'string',
              'scalar': '',
              'composite': [],
              'custom': null,
              'name': ''
            }, {
              'id': '10092',
              'typename': null,
              'scalar': 'Porsche',
              'composite': [],
              'custom': null,
              'name': 'Porsche'
            }, {
              'id': '10093',
              'typename': null,
              'scalar': 'Ferrari',
              'composite': [],
              'custom': null,
              'name': 'Ferrari'
            }, {
              'id': '10094',
              'typename': null,
              'scalar': 'Lamborghini',
              'composite': [],
              'custom': null,
              'name': 'Lamborghini'
            }, {
              'id': '10095',
              'typename': null,
              'scalar': 'Peugeot',
              'composite': [],
              'custom': null,
              'name': 'Peugeot'
            }, {
              'id': '10096',
              'typename': null,
              'scalar': 'Citroën',
              'composite': [],
              'custom': null,
              'name': 'Citroën'
            }, {
              'id': '10097',
              'typename': null,
              'scalar': 'Renault',
              'composite': [],
              'custom': null,
              'name': 'Renault'
            }, {
              'id': '10098',
              'typename': null,
              'scalar': 'Audi',
              'composite': [],
              'custom': null,
              'name': 'Audi'
            }, {
              'id': '10099',
              'typename': null,
              'scalar': 'BMW',
              'composite': [],
              'custom': null,
              'name': 'BMW'
            }, {
              'id': '10100',
              'typename': null,
              'scalar': 'Volkswagen',
              'composite': [],
              'custom': null,
              'name': 'Volkswagen'
            }, {
              'id': '10101',
              'typename': null,
              'scalar': 'Mercedes-Benz',
              'composite': [],
              'custom': null,
              'name': 'Mercedes-Benz'
            }, {
              'id': '10102',
              'typename': null,
              'scalar': 'Toyota',
              'composite': [],
              'custom': null,
              'name': 'Toyota'
            }, {
              'id': '10103',
              'typename': null,
              'scalar': 'FIAT',
              'composite': [],
              'custom': null,
              'name': 'FIAT'
            }, {
              'id': '10104',
              'typename': null,
              'scalar': 'Ford',
              'composite': [],
              'custom': null,
              'name': 'Ford'
            }, {
              'id': '10105',
              'typename': null,
              'scalar': 'Chevrolet',
              'composite': [],
              'custom': null,
              'name': 'Chevrolet'
            }
            ],
            'rendering': {
              'operations': ['SET'],
              'inputType': {
                'name': 'dropdown_list',
                'original': 'com.atlassian.jira.plugin.system.customfieldtypes_radiobuttons',
                'dataType': 'select_option',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'fixVersions',
            'label': 'Versions corrigées',
            'possibleValues': [{
              'id': '10020',
              'typename': null,
              'scalar': 'v0.0',
              'composite': [],
              'custom': null,
              'name': 'v0.0'
            }, {
              'id': '10021',
              'typename': null,
              'scalar': 'v0.1',
              'composite': [],
              'custom': null,
              'name': 'v0.1'
            }
            ],
            'rendering': {
              'operations': ['SET', 'ADD', 'REMOVE'],
              'inputType': {
                'name': 'tag_list',
                'original': 'fixVersions',
                'dataType': 'version-array',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'customfield_10088',
            'label': 'ALL_CUF_Calendrier',
            'possibleValues': [],
            'rendering': {
              'operations': ['SET'],
              'inputType': {
                'name': 'date_picker',
                'original': 'com.atlassian.jira.plugin.system.customfieldtypes_datepicker',
                'dataType': 'date',
                'fieldSchemeSelector': false,
                'configuration': {
                  'format': 'yyyy-MM-dd'
                }
              },
              'required': false
            }
          }, {
            'id': 'priority',
            'label': 'Priorité',
            'possibleValues': [{
              'id': '-1',
              'typename': 'string',
              'scalar': '',
              'composite': [],
              'custom': null,
              'name': ''
            }, {
              'id': '1',
              'typename': null,
              'scalar': 'Highest',
              'composite': [],
              'custom': null,
              'name': 'Highest'
            }, {
              'id': '2',
              'typename': null,
              'scalar': 'High',
              'composite': [],
              'custom': null,
              'name': 'High'
            }, {
              'id': '3',
              'typename': null,
              'scalar': 'Medium',
              'composite': [],
              'custom': null,
              'name': 'Medium'
            }, {
              'id': '4',
              'typename': null,
              'scalar': 'Low',
              'composite': [],
              'custom': null,
              'name': 'Low'
            }, {
              'id': '5',
              'typename': null,
              'scalar': 'Lowest',
              'composite': [],
              'custom': null,
              'name': 'Lowest'
            }
            ],
            'rendering': {
              'operations': ['SET'],
              'inputType': {
                'name': 'dropdown_list',
                'original': 'priority',
                'dataType': 'priority',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'customfield_10089',
            'label': 'ALL_CUF_CasesCocher',
            'possibleValues': [{
              'id': '10106',
              'typename': null,
              'scalar': 'Sedan',
              'composite': [],
              'custom': null,
              'name': 'Sedan'
            }, {
              'id': '10107',
              'typename': null,
              'scalar': 'Break',
              'composite': [],
              'custom': null,
              'name': 'Break'
            }, {
              'id': '10108',
              'typename': null,
              'scalar': 'Coupé',
              'composite': [],
              'custom': null,
              'name': 'Coupé'
            }, {
              'id': '10109',
              'typename': null,
              'scalar': 'Cabriolet',
              'composite': [],
              'custom': null,
              'name': 'Cabriolet'
            }, {
              'id': '10110',
              'typename': null,
              'scalar': 'Pick-up',
              'composite': [],
              'custom': null,
              'name': 'Pick-up'
            }, {
              'id': '10111',
              'typename': null,
              'scalar': 'Camion',
              'composite': [],
              'custom': null,
              'name': 'Camion'
            }, {
              'id': '10112',
              'typename': null,
              'scalar': 'Supercar',
              'composite': [],
              'custom': null,
              'name': 'Supercar'
            }, {
              'id': '10113',
              'typename': null,
              'scalar': 'Hypercar',
              'composite': [],
              'custom': null,
              'name': 'Hypercar'
            }, {
              'id': '10114',
              'typename': null,
              'scalar': 'Camionette',
              'composite': [],
              'custom': null,
              'name': 'Camionette'
            }
            ],
            'rendering': {
              'operations': ['SET', 'ADD', 'REMOVE'],
              'inputType': {
                'name': 'checkbox_list',
                'original': 'com.atlassian.jira.plugin.system.customfieldtypes_multicheckboxes',
                'dataType': 'option-array',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'customfield_10014',
            'label': 'Epic Link',
            'possibleValues': [],
            'rendering': {
              'operations': ['SET'],
              'inputType': {
                'name': 'text_field',
                'original': 'com.pyxis.greenhopper.jira_gh-epic-link',
                'dataType': 'any',
                'fieldSchemeSelector': false,
                'configuration': {
                  'onchange': 'searchepic:PCK01'
                }
              },
              'required': false
            }
          }, {
            'id': 'customfield_10179',
            'label': 'PCK_date_time',
            'possibleValues': [],
            'rendering': {
              'operations': ['SET'],
              'inputType': {
                'name': 'date_time',
                'original': 'com.atlassian.jira.plugin.system.customfieldtypes_datetime',
                'dataType': 'datetime',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'labels',
            'label': 'Étiquettes',
            'possibleValues': [],
            'rendering': {
              'operations': ['SET', 'ADD', 'REMOVE'],
              'inputType': {
                'name': 'free_tag_list',
                'original': 'labels',
                'dataType': 'free_tag_list',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'environment',
            'label': 'Environnement',
            'possibleValues': [],
            'rendering': {
              'operations': ['SET'],
              'inputType': {
                'name': 'text_area',
                'original': 'environment',
                'dataType': 'string',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'attachment',
            'label': 'Pièce jointe',
            'possibleValues': [],
            'rendering': {
              'operations': [],
              'inputType': {
                'name': 'file_upload',
                'original': 'attachment',
                'dataType': 'attachment-array',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'versions',
            'label': 'Versions affectées',
            'possibleValues': [],
            'rendering': {
              'operations': ['SET', 'ADD', 'REMOVE'],
              'inputType': {
                'name': 'tag_list',
                'original': 'versions',
                'dataType': 'version-array',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'issuelinks',
            'label': 'Tickets liés',
            'possibleValues': [],
            'rendering': {
              'operations': ['ADD'],
              'inputType': {
                'name': 'unknown',
                'original': 'issuelinks',
                'dataType': 'issuelinks-array',
                'fieldSchemeSelector': false,
                'configuration': {}
              },
              'required': false
            }
          }, {
            'id': 'assignee',
            'label': 'Responsable',
            'possibleValues': [],
            'rendering': {
              'operations': ['SET'],
              'inputType': {
                'name': 'text_field',
                'original': 'assignee',
                'dataType': 'user',
                'fieldSchemeSelector': false,
                'configuration': {
                  'onchange': 'searchassignable:PCK01'
                }
              },
              'required': false
            }
          }
        ],
        'issuetype:Nouvelle fonctionnalité': [{
          'id': 'issuetype',
          'label': 'Type de ticket',
          'possibleValues': [{
            'id': '10005',
            'typename': 'issuetype',
            'scalar': 'Bug',
            'composite': [],
            'custom': null,
            'name': 'Bug'
          }, {
            'id': '10004',
            'typename': 'issuetype',
            'scalar': 'Nouvelle fonctionnalité',
            'composite': [],
            'custom': null,
            'name': 'Nouvelle fonctionnalité'
          }, {
            'id': '10002',
            'typename': 'issuetype',
            'scalar': 'Tâche',
            'composite': [],
            'custom': null,
            'name': 'Tâche'
          }, {
            'id': '10006',
            'typename': 'issuetype',
            'scalar': 'Story',
            'composite': [],
            'custom': null,
            'name': 'Story'
          }, {
            'id': '10001',
            'typename': 'issuetype',
            'scalar': 'Amélioration',
            'composite': [],
            'custom': null,
            'name': 'Amélioration'
          }, {
            'id': '10031',
            'typename': 'issuetype',
            'scalar': 'typeLEL',
            'composite': [],
            'custom': null,
            'name': 'typeLEL'
          }, {
            'id': '10003',
            'typename': 'issuetype',
            'scalar': 'Sous-tâche',
            'composite': [],
            'custom': null,
            'name': 'Sous-tâche'
          }, {
            'id': '10030',
            'typename': 'issuetype',
            'scalar': 'Anomalie_LEL',
            'composite': [],
            'custom': null,
            'name': 'Anomalie_LEL'
          }, {
            'id': '10026',
            'typename': 'issuetype',
            'scalar': 'Cuf_Dev',
            'composite': [],
            'custom': null,
            'name': 'Cuf_Dev'
          }, {
            'id': '10025',
            'typename': 'issuetype',
            'scalar': 'CUF SV',
            'composite': [],
            'custom': null,
            'name': 'CUF SV'
          }, {
            'id': '10000',
            'typename': 'issuetype',
            'scalar': 'Epic',
            'composite': [],
            'custom': null,
            'name': 'Epic'
          }, {
            'id': '10024',
            'typename': 'issuetype',
            'scalar': 'TypeFullCUF*',
            'composite': [],
            'custom': null,
            'name': 'TypeFullCUF*'
          }
          ],
          'rendering': {
            'operations': ['SET'],
            'inputType': {
              'name': 'dropdown_list',
              'original': 'issuetype',
              'dataType': null,
              'fieldSchemeSelector': true,
              'configuration': {}
            },
            'required': true
          }
        }, {
          'id': 'summary',
          'label': 'Résumé',
          'possibleValues': [],
          'rendering': {
            'operations': ['SET'],
            'inputType': {
              'name': 'text_field',
              'original': 'summary',
              'dataType': 'string',
              'fieldSchemeSelector': false,
              'configuration': {}
            },
            'required': true
          }
        }, {
          'id': 'components',
          'label': 'Composants',
          'possibleValues': [],
          'rendering': {
            'operations': ['SET', 'ADD', 'REMOVE'],
            'inputType': {
              'name': 'tag_list',
              'original': 'components',
              'dataType': 'component-array',
              'fieldSchemeSelector': false,
              'configuration': {}
            },
            'required': false
          }
        }, {
          'id': 'description',
          'label': 'Description',
          'possibleValues': [],
          'rendering': {
            'operations': ['SET'],
            'inputType': {
              'name': 'text_area',
              'original': 'description',
              'dataType': 'string',
              'fieldSchemeSelector': false,
              'configuration': {}
            },
            'required': false
          }
        }, {
          'id': 'customfield_10020',
          'label': 'Sprint',
          'possibleValues': [{
            'id': '-1',
            'typename': 'string',
            'scalar': '',
            'composite': [],
            'custom': null,
            'name': ''
          }, {
            'id': '44',
            'typename': 'string',
            'scalar': 'Connexio Sprint 3',
            'composite': [],
            'custom': null,
            'name': 'Connexio Sprint 3'
          }, {
            'id': '1',
            'typename': 'string',
            'scalar': 'Connexion Sprint 1 modif',
            'composite': [],
            'custom': null,
            'name': 'Connexion Sprint 1 modif'
          }, {
            'id': '10',
            'typename': 'string',
            'scalar': 'Dossier Sync tableau 2',
            'composite': [],
            'custom': null,
            'name': 'Dossier Sync tableau 2'
          }, {
            'id': '26',
            'typename': 'string',
            'scalar': 'Sample Sprint 2',
            'composite': [],
            'custom': null,
            'name': 'Sample Sprint 2'
          }, {
            'id': '29',
            'typename': 'string',
            'scalar': 'Sample Sprint 3',
            'composite': [],
            'custom': null,
            'name': 'Sample Sprint 3'
          }, {
            'id': '32',
            'typename': 'string',
            'scalar': 'Sample Sprint 4',
            'composite': [],
            'custom': null,
            'name': 'Sample Sprint 4'
          }, {
            'id': '33',
            'typename': 'string',
            'scalar': 'Sample Sprint 5',
            'composite': [],
            'custom': null,
            'name': 'Sample Sprint 5'
          }, {
            'id': '61',
            'typename': 'string',
            'scalar': 'Sprint 1',
            'composite': [],
            'custom': null,
            'name': 'Sprint 1'
          }, {
            'id': '54',
            'typename': 'string',
            'scalar': 'Sprint 2',
            'composite': [],
            'custom': null,
            'name': 'Sprint 2'
          }
          ],
          'rendering': {
            'operations': ['SET'],
            'inputType': {
              'name': 'dropdown_list',
              'original': 'com.pyxis.greenhopper.jira_gh-sprint',
              'dataType': 'json-array',
              'fieldSchemeSelector': false,
              'configuration': {}
            },
            'required': false
          }
        }, {
          'id': 'reporter',
          'label': 'Rapporteur',
          'possibleValues': [],
          'rendering': {
            'operations': ['SET'],
            'inputType': {
              'name': 'text_field',
              'original': 'reporter',
              'dataType': 'user',
              'fieldSchemeSelector': false,
              'configuration': {
                'onchange': 'searchreporter:'
              }
            },
            'required': true
          }
        }, {
          'id': 'fixVersions',
          'label': 'Versions corrigées',
          'possibleValues': [{
            'id': '10020',
            'typename': null,
            'scalar': 'v0.0',
            'composite': [],
            'custom': null,
            'name': 'v0.0'
          }, {
            'id': '10021',
            'typename': null,
            'scalar': 'v0.1',
            'composite': [],
            'custom': null,
            'name': 'v0.1'
          }
          ],
          'rendering': {
            'operations': ['SET', 'REMOVE', 'ADD'],
            'inputType': {
              'name': 'tag_list',
              'original': 'fixVersions',
              'dataType': 'version-array',
              'fieldSchemeSelector': false,
              'configuration': {}
            },
            'required': false
          }
        }, {
          'id': 'priority',
          'label': 'Priorité',
          'possibleValues': [{
            'id': '-1',
            'typename': 'string',
            'scalar': '',
            'composite': [],
            'custom': null,
            'name': ''
          }, {
            'id': '1',
            'typename': null,
            'scalar': 'Highest',
            'composite': [],
            'custom': null,
            'name': 'Highest'
          }, {
            'id': '2',
            'typename': null,
            'scalar': 'High',
            'composite': [],
            'custom': null,
            'name': 'High'
          }, {
            'id': '3',
            'typename': null,
            'scalar': 'Medium',
            'composite': [],
            'custom': null,
            'name': 'Medium'
          }, {
            'id': '4',
            'typename': null,
            'scalar': 'Low',
            'composite': [],
            'custom': null,
            'name': 'Low'
          }, {
            'id': '5',
            'typename': null,
            'scalar': 'Lowest',
            'composite': [],
            'custom': null,
            'name': 'Lowest'
          }
          ],
          'rendering': {
            'operations': ['SET'],
            'inputType': {
              'name': 'dropdown_list',
              'original': 'priority',
              'dataType': 'priority',
              'fieldSchemeSelector': false,
              'configuration': {}
            },
            'required': false
          }
        }, {
          'id': 'customfield_10014',
          'label': 'Epic Link',
          'possibleValues': [],
          'rendering': {
            'operations': ['SET'],
            'inputType': {
              'name': 'text_field',
              'original': 'com.pyxis.greenhopper.jira_gh-epic-link',
              'dataType': 'any',
              'fieldSchemeSelector': false,
              'configuration': {
                'onchange': 'searchepic:PCK01'
              }
            },
            'required': false
          }
        }, {
          'id': 'labels',
          'label': 'Étiquettes',
          'possibleValues': [],
          'rendering': {
            'operations': ['SET', 'ADD', 'REMOVE'],
            'inputType': {
              'name': 'free_tag_list',
              'original': 'labels',
              'dataType': 'free_tag_list',
              'fieldSchemeSelector': false,
              'configuration': {}
            },
            'required': false
          }
        }, {
          'id': 'attachment',
          'label': 'Pièce jointe',
          'possibleValues': [],
          'rendering': {
            'operations': [],
            'inputType': {
              'name': 'file_upload',
              'original': 'attachment',
              'dataType': 'attachment-array',
              'fieldSchemeSelector': false,
              'configuration': {}
            },
            'required': false
          }
        }, {
          'id': 'issuelinks',
          'label': 'Tickets liés',
          'possibleValues': [],
          'rendering': {
            'operations': ['ADD'],
            'inputType': {
              'name': 'unknown',
              'original': 'issuelinks',
              'dataType': 'issuelinks-array',
              'fieldSchemeSelector': false,
              'configuration': {}
            },
            'required': false
          }
        }, {
          'id': 'assignee',
          'label': 'Responsable',
          'possibleValues': [],
          'rendering': {
            'operations': ['SET'],
            'inputType': {
              'name': 'text_field',
              'original': 'assignee',
              'dataType': 'user',
              'fieldSchemeSelector': false,
              'configuration': {
                'onchange': 'searchassignable:PCK01'
              }
            },
            'required': false
          }
        }
        ]
      }
    },
    'id': null,
    'currentScheme': 'issuetype:Bug',
    'comment': '',
    'description': '# Cas de test: [] 123\n# Exécution: http://localhost:4200/squash/executions/26\n\n# Description de l\'anomalie :\n',
    'priority': {
      'name': '--',
      'id': '--'
    },
    'version': {
      'name': '--',
      'id': '--'
    },
    'category': {
      'name': '--',
      'id': '--'
    },
    'summary': '',
    'bugtracker': 'plugin01',
    'assignee': {
      'name': '--',
      'id': '--'
    },
    btName: '',
    hasBlankId: false,
    state: undefined,
    getNewKey(): string {
      return '';
    }
  };
}
