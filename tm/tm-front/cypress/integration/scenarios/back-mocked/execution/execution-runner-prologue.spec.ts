import {ExecutionRunnerProloguePage} from '../../../page-objects/pages/execution/execution-runner-prologue-page';
import {ExecutionModel, ExecutionStepModel} from '../../../model/execution/execution.model';
import {InputType} from '../../../model/customfield/customfield.model';

const prerequisite = `<p>a nice prerequisite</p>
<table><tbody><tr><th>Col1</th><th>Col2</th></tr><tr><td>Col1L1</td><td>Col2L1</td></tr></tbody></table>`;

const tcDescription = `<p>a nice description</p>
<table><tbody><tr><th>Col8</th><th>Col2</th></tr><tr><td>Col8L1</td><td>Col2L1</td></tr></tbody></table>`;

const tcRtf = `<p>a nice rich text field</p>
<table><tbody><tr><th>Col4</th><th>Col2</th></tr><tr><td>Col4L1</td><td>Col2L1</td></tr></tbody></table>`;

function getExecutionModel(): ExecutionModel {
  return {
    id: 1,
    projectId: 1,
    executionOrder: 2,
    name: 'NX - Test Case 1',
    prerequisite,
    attachmentList: {
      id: 1, attachments: [
        {id: 1, addedOn: new Date('25 Mar 2020 10:12:57'), name: 'attachment-1', size: 56789},
        {id: 2, addedOn: new Date('17 Mar 2019 20:40:00'), name: 'attachment-2', size: 1234567},
      ]
    },
    customFieldValues: [],
    tcImportance: 'LOW',
    tcNatLabel: 'test-case.nature.NAT_BUSINESS_TESTING',
    tcNatIconName: 'indeterminate_checkbox',
    tcStatus: 'APPROVED',
    datasetLabel: 'JDD-1',
    tcTypeLabel: 'a custom type',
    tcTypeIconName: 'indeterminate_checkbox',
    tcDescription,
    comment: '',
    denormalizedCustomFieldValues: [
      {
        id: 1,
        label: 'dnz-cuf-1',
        inputType: InputType.PLAIN_TEXT,
        denormalizedFieldHolderId: 1,
        fieldType: 'CF',
        value: 'dnz cuf value 1'
      },
      {
        id: 2,
        label: 'dnz-cuf-2',
        inputType: InputType.DATE_PICKER,
        denormalizedFieldHolderId: 1,
        fieldType: 'CF',
        value: '2020-02-25'
      },
      {
        id: 3,
        label: 'dnz-cuf-3',
        inputType: InputType.DROPDOWN_LIST,
        denormalizedFieldHolderId: 1,
        fieldType: 'SSF',
        value: 'Option-1'
      },
      {
        id: 4,
        label: 'dnz-cuf-4',
        inputType: InputType.NUMERIC,
        denormalizedFieldHolderId: 1,
        fieldType: 'CF',
        value: '12'
      },
      {
        id: 5,
        label: 'dnz-cuf-5',
        inputType: InputType.CHECKBOX,
        denormalizedFieldHolderId: 1,
        fieldType: 'CF',
        value: 'true'
      },
      {
        id: 6,
        label: 'dnz-cuf-6',
        inputType: InputType.CHECKBOX,
        denormalizedFieldHolderId: 1,
        fieldType: 'CF',
        value: 'false'
      },
      {
        id: 7,
        label: 'dnz-cuf-7',
        inputType: InputType.RICH_TEXT,
        denormalizedFieldHolderId: 1,
        fieldType: 'RTF',
        value: tcRtf
      },
      {
        id: 8,
        label: 'dnz-cuf-8',
        inputType: InputType.TAG,
        denormalizedFieldHolderId: 1,
        fieldType: 'MSF',
        value: 'hello|world|squash'
      }
    ],
    executionStepViews: [
      {
        id: 1,
        order: 0,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel,
      {
        id: 2,
        order: 1,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel,
      {
        id: 3,
        order: 2,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel
    ],
    coverages: [
      {
        criticality: 'CRITICAL',
        directlyVerified: true,
        name: 'Requirement 1',
        projectName: 'Project 1',
        reference: 'REF-1',
        requirementVersionId: 1,
        status: 'WORK_IN_PROGRESS',
        stepIndex: 0,
        unDirectlyVerified: false,
        verifiedBy: 'admin',
        verifyingCalledTestCaseIds: [12],
        coverageStepInfos: [{id: 11, index: 0}],
        verifyingTestCaseId: 3
      },
      {
        criticality: 'MAJOR',
        directlyVerified: true,
        name: 'Requirement 2',
        projectName: 'Project 1',
        reference: '',
        requirementVersionId: 2,
        status: 'APPROVED',
        stepIndex: 2,
        unDirectlyVerified: true,
        verifiedBy: 'admin',
        verifyingCalledTestCaseIds: [12],
        coverageStepInfos: [{id: 11, index: 2}],
        verifyingTestCaseId: 3
      }
    ],
    executionMode: 'MANUAL',
    lastExecutedOn: null,
    lastExecutedBy: 'admin',
    executionStatus: 'READY',
    automatedJobUrl: null,
    testAutomationServerKind: null,
    automatedExecutionResultUrl: null,
    automatedExecutionResultSummary: null,
    nbIssues: 0,
    iterationId: -1,
    kind: 'STANDARD',
    milestones: []
  };
}

describe('Execution runner', function () {
  it('it should show correct information', () => {
    const executionModel: ExecutionModel = getExecutionModel();
    const prologuePage = ExecutionRunnerProloguePage.initTestAtPage(1, executionModel);
    prologuePage.assertExist();
    prologuePage.checkTitle('#3 : NX - Test Case 1');
    prologuePage.checkPrerequisite(prerequisite);
    prologuePage.checkStatus('Approuvé');
    prologuePage.checkImportance('Faible');
    prologuePage.checkNature('Métier');
    prologuePage.checkNatureIcon('indeterminate_checkbox');
    prologuePage.checkType('a custom type');
    prologuePage.checkTypeIcon('indeterminate_checkbox');
    prologuePage.checkDataSet('JDD-1');
    prologuePage.checkTcDescription(tcDescription);
    prologuePage.checkDenormalizedCustomField(0, 'dnz-cuf-1', 'dnz cuf value 1');
    prologuePage.checkDenormalizedCustomField(1, 'dnz-cuf-2', '25/02/2020');
    prologuePage.checkDenormalizedCustomField(2, 'dnz-cuf-3', 'Option-1');
    prologuePage.checkDenormalizedCustomField(3, 'dnz-cuf-4', '12');
    prologuePage.checkDenormalizedCustomField(4, 'dnz-cuf-5', true, InputType.CHECKBOX);
    prologuePage.checkDenormalizedCustomField(5, 'dnz-cuf-6', false, InputType.CHECKBOX);
    prologuePage.checkDenormalizedCustomField(6, 'dnz-cuf-7', tcRtf, InputType.RICH_TEXT);
    prologuePage.checkDenormalizedCustomField(7, 'dnz-cuf-8', ['hello', 'world', 'squash'], InputType.TAG);
    prologuePage.checkDocumentTitle('Exéc #3 - NX - Test Case 1 - JDD-1 (Préambule)');
    prologuePage.checkModificationDuringExecButtonIsVisible();
  });


  it('it should show attachments information', () => {
    const executionModel: ExecutionModel = getExecutionModel();
    const prologuePage = ExecutionRunnerProloguePage.initTestAtPage(1, executionModel);
    prologuePage.attachmentCompactList.checkAttachmentData(0, 'attachment-1', '55,46 Ko', 'Ajouté le 25/03/2020 10:12');
    prologuePage.attachmentCompactList.checkAttachmentData(1, 'attachment-2', '1,18 Mo', 'Ajouté le 17/03/2019 20:40');
  });

  it('it should do various operation on attachments ', () => {
    const executionModel: ExecutionModel = getExecutionModel();
    const prologuePage = ExecutionRunnerProloguePage.initTestAtPage(1, executionModel);
    prologuePage.attachmentCompactList.deleteAttachment(1);
    prologuePage.attachmentCompactList.cancelDeleteAttachment(1);
    prologuePage.attachmentCompactList.deleteAttachment(1);
    prologuePage.attachmentCompactList.confirmDeleteAttachment(1, 0, 1, 2, 1);
    prologuePage.attachmentCompactList.deleteAttachment(0);
    prologuePage.attachmentCompactList.confirmDeleteAttachment(0, 0, 1, 1, 1);
    prologuePage.addAttachments(
      [
        new File(['last flight'], 'STS_135.txt'),
        new File(['last flight'], 'STS_132.txt'),
        new File(['last flight'], 'STS_134.txt'),
      ],
      1,
      [
        {
          status: '',
          iStatus: 0,
          attachmentDto: {
            name: 'STS_135.txt',
            size: 123456,
            addedOn: new Date('25 Mar 2020 10:12:57'),
            id: 12,
          }
        },
        {
          status: '',
          iStatus: 0,
          attachmentDto: {
            name: 'STS_132.txt',
            size: 123456,
            addedOn: new Date('25 Mar 2020 10:12:57'),
            id: 13,
          }
        },
        {
          status: '',
          iStatus: 0,
          attachmentDto: {
            name: 'STS_134.txt',
            size: 123456,
            addedOn: new Date('25 Mar 2020 10:12:57'),
            id: 14,
          }
        }
      ]);
    prologuePage.attachmentCompactList.checkAttachmentData(0, 'STS_135.txt', '120,56 Ko', 'Ajouté le 25/03/2020 10:12');
    prologuePage.attachmentCompactList.checkAttachmentData(1, 'STS_132.txt', '120,56 Ko', 'Ajouté le 25/03/2020 10:12');
    prologuePage.attachmentCompactList.checkAttachmentData(2, 'STS_134.txt', '120,56 Ko', 'Ajouté le 25/03/2020 10:12');
  });

  it('it should show coverages ', () => {
    const executionModel: ExecutionModel = getExecutionModel();
    const prologuePage = ExecutionRunnerProloguePage.initTestAtPage(1, executionModel);
    const row = prologuePage.coverageGrid.getRow(1);
    row.cell('projectName').textRenderer().assertContainText('Project 1');
    row.cell('reference').textRenderer().assertContainText('REF-1');
    row.cell('name').linkRenderer().assertContainText('Requirement 1');
    row.cell('criticality').iconRenderer().assertContainIcon('anticon-sqtm-core-requirement:double_up');
    prologuePage.coverageGrid.assertRowExist(2);
  });
});

