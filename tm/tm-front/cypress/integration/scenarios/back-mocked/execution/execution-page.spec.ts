import {ExecutionModel, ExecutionStepModel} from '../../../model/execution/execution.model';
import {InputType} from '../../../model/customfield/customfield.model';
import {ExecutionPage} from '../../../page-objects/pages/execution/execution-page';
import {mockDataRow, mockGridResponse} from '../../../data-mock/grid.data-mock';
import {ReferentialDataMockBuilder} from '../../../utils/referential/referential-data-builder';
import {DataRow, GridResponse, Identifier} from '../../../model/grids/data-row.type';
import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {ADMIN_PERMISSIONS} from '../../../model/permissions/permissions.model';

describe('Execution page', function () {
  it('it should display correct information', () => {
    const executionModel: ExecutionModel = getExecutionModel();
    const executionPage = ExecutionPage.initTestAtPage(1, executionModel);
    executionPage.assertExist();
    executionPage.checkName('#3 : NX - Test Case 1');
    executionPage.checkStatus('En cours');
    executionPage.checkImportance('Haute');
    executionPage.checkNature('Métier');
    executionPage.checkNatureIcon('indeterminate_checkbox');
    executionPage.checkType('default');
    executionPage.checkTypeIcon('indeterminate_checkbox');
    executionPage.checkDataSet('JDD-1');
    executionPage.checkTcDescription('this is a description');
    executionPage.checkDenormalizedCustomField(0, 'dnz-cuf-1', 'dnz cuf value 1');
    executionPage.checkDenormalizedCustomField(1, 'dnz-cuf-2', '25/02/2020');
  });

  it('it should show coverages ', () => {
    const executionModel: ExecutionModel = getExecutionModel();
    const executionPage = ExecutionPage.initTestAtPage(1, executionModel);
    const row = executionPage.coverageGrid.getRow(1);
    row.cell('projectName').textRenderer().assertContainText('P1');
    row.cell('reference').textRenderer().assertContainText('This is a reference');
    row.cell('name').linkRenderer().assertContainText('Requirement 1');
    row.cell('criticality').iconRenderer().assertContainIcon('anticon-sqtm-core-requirement:double_up');
    executionPage.coverageGrid.assertRowExist(2);
  });

  it('it should display and correctly change comment', () => {
    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel());
    executionPage.commentField.checkTextContent('this is a comment');
    executionPage.commentField.setAndConfirmValue('new comment');
    executionPage.commentField.checkTextContent('new comment');
  });

  it('it should correctly display issue grid data', () => {
    const refData = mockRefDataWithBugtracker();

    const issueGridResponse = mockIssueGridResponse([
      mockIssueDataRow(1, 12, ''),
      mockIssueDataRow(2, 13, '0'),
    ]);

    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel(), issueGridResponse, refData);
    executionPage.issueGrid.assertExist();
    const row1 = executionPage.issueGrid.getRow('12');
    row1.cell('remoteId').linkRenderer().assertContainText('12');
    row1.cell('btProject').textRenderer().assertContainText('P1');
    row1.cell('summary').textRenderer().assertContainText('summary');
    row1.cell('priority').textRenderer().assertContainText('minor');
    row1.cell('status').textRenderer().assertContainText('assigned');
    row1.cell('assignee').textRenderer().assertContainText('admin');
    row1.cell('reportedIn').textRenderer().assertContainText('Cette exécution');

    const row2 = executionPage.issueGrid.getRow('13');
    row2.cell('reportedIn').textRenderer().assertContainText('Pas 1');
  });

  it('should unbind issue', () => {
    const refData = mockRefDataWithBugtracker();

    const issueGridResponse = mockIssueGridResponse([
      mockIssueDataRow(1, 12, ''),
      mockIssueDataRow(2, 13, '0'),
    ]);

    const issueGridResponseAfterDelete = mockIssueGridResponse([
      mockIssueDataRow(2, 13, '0'),
    ]);

    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel(), issueGridResponse, refData);
    executionPage.issueGrid.assertExist();
    new NavBarElement().toggle();
    executionPage.unbindOneIssue('12', issueGridResponseAfterDelete);
    executionPage.issueGrid.assertRowCount(1);
  });

  it('should unbind multiple issues', () => {
    const refData = mockRefDataWithBugtracker();

    const issueGridResponse = mockIssueGridResponse([
      mockIssueDataRow(1, 12, ''),
      mockIssueDataRow(2, 13, '0'),
    ]);

    const issueGridResponseAfterDelete = mockIssueGridResponse([]);

    const executionPage = ExecutionPage.initTestAtPage(1, getExecutionModel(), issueGridResponse, refData);
    executionPage.issueGrid.assertExist();
    new NavBarElement().toggle();
    executionPage.unbindMultipleIssues(['12', '13'], issueGridResponseAfterDelete);
    executionPage.issueGrid.assertRowCount(0);
  });
});


function getExecutionModel(): ExecutionModel {
  return {
    id: 1,
    projectId: 1,
    executionOrder: 2,
    name: 'NX - Test Case 1',
    prerequisite: '',
    attachmentList: {
      id: 1, attachments: [
        {id: 1, addedOn: new Date('25 Mar 2020 10:12:57'), name: 'attachment-1', size: 56789},
        {id: 2, addedOn: new Date('17 Mar 2019 20:40:00'), name: 'attachment-2', size: 1234567},
      ]
    },
    customFieldValues: [],
    tcImportance: 'HIGH',
    tcNatLabel: 'test-case.nature.NAT_BUSINESS_TESTING',
    tcNatIconName: 'indeterminate_checkbox',
    tcStatus: 'WORK_IN_PROGRESS',
    datasetLabel: 'JDD-1',
    tcTypeLabel: 'default',
    tcTypeIconName: 'indeterminate_checkbox',
    tcDescription: 'this is a description',
    comment: 'this is a comment',
    denormalizedCustomFieldValues: [
      {
        id: 1,
        label: 'dnz-cuf-1',
        inputType: InputType.PLAIN_TEXT,
        denormalizedFieldHolderId: 1,
        fieldType: 'CF',
        value: 'dnz cuf value 1'
      },
      {
        id: 2,
        label: 'dnz-cuf-2',
        inputType: InputType.DATE_PICKER,
        denormalizedFieldHolderId: 1,
        fieldType: 'CF',
        value: '2020-02-25'
      }
    ],
    executionStepViews: [
      {
        id: 1,
        order: 0,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel,
      {
        id: 2,
        order: 1,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel,
      {
        id: 3,
        order: 2,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel
    ],
    coverages: [
      {
        criticality: 'CRITICAL',
        directlyVerified: true,
        name: 'Requirement 1',
        projectName: 'P1',
        reference: 'This is a reference',
        requirementVersionId: 1,
        status: 'WORK_IN_PROGRESS',
        stepIndex: 0,
        unDirectlyVerified: false,
        verifiedBy: 'admin',
        verifyingCalledTestCaseIds: [12],
        coverageStepInfos: [{id: 11, index: 0}],
        verifyingTestCaseId: 3
      },
      {
        criticality: 'MAJOR',
        directlyVerified: true,
        name: 'Requirement 2',
        projectName: 'P1',
        reference: '',
        requirementVersionId: 2,
        status: 'APPROVED',
        stepIndex: 2,
        unDirectlyVerified: true,
        verifiedBy: 'admin',
        verifyingCalledTestCaseIds: [12],
        coverageStepInfos: [{id: 11, index: 2}],
        verifyingTestCaseId: 3
      }
    ],
    executionMode: 'MANUAL',
    lastExecutedOn: null,
    lastExecutedBy: 'admin',
    executionStatus: 'READY',
    automatedJobUrl: null,
    testAutomationServerKind: null,
    automatedExecutionResultUrl: null,
    automatedExecutionResultSummary: null,
    nbIssues: 0,
    iterationId: -1,
    kind: 'STANDARD',
    milestones: []
  };
}

function mockRefDataWithBugtracker() {
  return new ReferentialDataMockBuilder().withBugTrackers({
    name: 'bt'
  }).withProjects({bugTrackerBinding: {id: 1, bugTrackerId: 1, projectId: 1}, permissions: ADMIN_PERMISSIONS})
    .build();
}

function mockIssueGridResponse(dataRows: DataRow[]): GridResponse {
  return mockGridResponse('remoteId', dataRows);
}

function mockIssueDataRow(issueId: Identifier, remoteId: Identifier, stepOrder: string): DataRow {
  return mockDataRow({
    id: issueId.toString(),
    projectId: 1,
    data: {
      summary: 'summary',
      issueId: issueId.toString(),
      btProject: 'P1',
      stepOrder: stepOrder,
      priority: 'minor',
      executionOrder: 2,
      url: 'http://192.168.1.50/mantis/view.php?id=' + remoteId,
      remoteId: remoteId.toString(),
      executionId: 1,
      executionName: 'NX - Test Case 1',
      suiteNames: '',
      assignee: 'admin',
      status: 'assigned'
    },
    allowMoves: true,
  });
}
