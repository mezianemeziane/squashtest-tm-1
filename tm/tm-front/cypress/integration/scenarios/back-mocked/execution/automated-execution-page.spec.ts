import {ExecutionModel, ExecutionStepModel} from '../../../model/execution/execution.model';
import {ExecutionPage} from '../../../page-objects/pages/execution/execution-page';
import {TestAutomationServerKind} from '../../../model/test-automation/test-automation-server.model';

describe('Execution page', function () {
  it('it should display correct capsules information and result summary for a TF automated Test', () => {
    const executionModel: ExecutionModel = getExecutionModel(TestAutomationServerKind.jenkins);
    const automatedExecutionPage = ExecutionPage.initTestAtPage(1, executionModel);
    automatedExecutionPage.checkExecutionMode('Automatique');
    automatedExecutionPage.checkExecutionStatus('Succès');
    automatedExecutionPage.checkJobUrl('http://192.168.0.56/jenkins/job/test1');
    automatedExecutionPage.checkJobResult('http://192.168.0.56/jenkins/job/test1/Squash_TA_HTML_Report');
    automatedExecutionPage.automatedExecutionResultSummaryField.checkTextContent('This is a TF execution summary');
  });

  it('it should not display job url and job result capsules and result summary for a Squash Autom Test', () => {
    const executionModel: ExecutionModel = getExecutionModel(TestAutomationServerKind.squashAutom);
    const automatedExecutionPage = ExecutionPage.initTestAtPage(1, executionModel);
    automatedExecutionPage.assertJobUrlCapsuleNotExists();
    automatedExecutionPage.assertJobResultCapsuleNotExists();
    automatedExecutionPage.automatedExecutionResultSummaryField.assertNotExist();
  });
});


function getExecutionModel(testAutomationServerKind: TestAutomationServerKind): ExecutionModel {
  return {
    id: 1,
    projectId: 1,
    executionOrder: 2,
    name: 'NX - Test Case 1',
    prerequisite: '',
    attachmentList: {
      id: 1, attachments: [
        {id: 1, addedOn: new Date('25 Mar 2020 10:12:57'), name: 'attachment-1', size: 56789},
        {id: 2, addedOn: new Date('17 Mar 2019 20:40:00'), name: 'attachment-2', size: 1234567},
      ]
    },
    customFieldValues: [],
    tcImportance: 'HIGH',
    tcNatLabel: 'test-case.nature.NAT_BUSINESS_TESTING',
    tcNatIconName: 'indeterminate_checkbox',
    tcStatus: 'WORK_IN_PROGRESS',
    datasetLabel: 'JDD-1',
    tcTypeLabel: 'default',
    tcTypeIconName: 'indeterminate_checkbox',
    tcDescription: 'this is a description',
    comment: 'this is a comment',
    denormalizedCustomFieldValues: [],
    executionStepViews: [
      {
        id: 1,
        order: 0,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel,
      {
        id: 2,
        order: 1,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel,
      {
        id: 3,
        order: 2,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel
    ],
    coverages: [],
    executionMode: 'AUTOMATED',
    lastExecutedOn: new Date(),
    lastExecutedBy: 'admin',
    executionStatus: 'SUCCESS',
    testAutomationServerKind: testAutomationServerKind,
    automatedJobUrl: 'http://192.168.0.56/jenkins/job/test1',
    automatedExecutionResultUrl: 'http://192.168.0.56/jenkins/job/test1/Squash_TA_HTML_Report',
    automatedExecutionResultSummary: 'This is a TF execution summary',
    nbIssues: 0,
    iterationId: -1,
    kind: 'STANDARD',
    milestones: []
  };
}



