import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {TestCaseThroughRequirementSearchPage} from '../../../../page-objects/pages/campaign-workspace/search/test-case-through-requirement-search.page';
import {createEntityReferentialData} from '../../../../utils/referential/create-entity-referential.const';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';


function assertRedirectionToCampaignWorkspaceDone() {
  cy.url().should('equal', `${Cypress.config().baseUrl}/campaign-workspace`);
}

describe('Test Case through requirement search page for test-suite test-plan', () => {
  beforeEach(() => {
    //cy.server();
    cy.viewport(1200, 720);
  });

  describe('Test Case search table', () => {
    const testCase = {
      id: '1',
      type: 'TestCase',
      projectId: 1,
      data: {
        'name': 'Test 1',
        'id': 1,
        'reference': 'ref1',
        'projectName': 'project 1',
        'attachments': 0,
        'items': 0,
        'steps': 0,
        'nature': 14,
        'type': 16,
        'automatable': 'Y',
        'status': 'WORK_IN_PROGRESS',
        'importance': 'LOW',
        'createdBy': 'admin',
        'lastModifiedBy': 'admin',
        'tcMilestoneLocked': 0,
        'reqMilestoneLocked': 0
      }
    } as unknown as DataRow;

    it('should show links buttons and activate according to user selection', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [testCase]
      };
      const throughRequirementSearchPage = TestCaseThroughRequirementSearchPage.initTestAtPage(
        'test-suite', '1', createEntityReferentialData, gridResponse);
      const gridElement = throughRequirementSearchPage.grid;

      new NavBarElement().toggle();
      throughRequirementSearchPage.foldFilterPanel();
      throughRequirementSearchPage.assertLinkSelectionButtonExist();
      throughRequirementSearchPage.assertLinkSelectionButtonIsNotActive();
      throughRequirementSearchPage.assertLinkAllButtonExist();
      throughRequirementSearchPage.assertLinkAllButtonIsActive();
      throughRequirementSearchPage.assertNavigateBackButtonExist();
      throughRequirementSearchPage.assertNavigateBackButtonIsActive();
      gridElement.selectRow('1', '#', 'leftViewport');
      throughRequirementSearchPage.assertLinkSelectionButtonIsActive();
      gridElement.toggleRow('1', '#', 'leftViewport');
      throughRequirementSearchPage.assertLinkSelectionButtonIsNotActive();
    });

    it('should add a test case to a test-suite test plan', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [testCase]
      };
      const throughRequirementSearchPage = TestCaseThroughRequirementSearchPage.initTestAtPage(
        'test-suite', '1', createEntityReferentialData, gridResponse);
      const gridElement = throughRequirementSearchPage.grid;

      new NavBarElement().toggle();
      gridElement.selectRow('1', '#', 'leftViewport');

      const campaignTreeMock = buildCampaignTreeMock();
      throughRequirementSearchPage.linkSelection();
      campaignTreeMock.wait();
      assertRedirectionToCampaignWorkspaceDone();
    });

    it('should add all test cases to a test-suite test plan', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [testCase]
      };
      const throughRequirementSearchPage = TestCaseThroughRequirementSearchPage.initTestAtPage(
        'test-suite', '1', createEntityReferentialData, gridResponse);
      const gridElement = throughRequirementSearchPage.grid;

      new NavBarElement().toggle();
      const campaignTreeMock = buildCampaignTreeMock();
      throughRequirementSearchPage.linkAll();
      campaignTreeMock.wait();
      assertRedirectionToCampaignWorkspaceDone();
    });
  });
});

function buildCampaignTreeMock() {
  const campaignTree: GridResponse = {count: 0, dataRows: []};
  return new HttpMockBuilder(`/campaign-tree`).post().responseBody(campaignTree).build();
}
