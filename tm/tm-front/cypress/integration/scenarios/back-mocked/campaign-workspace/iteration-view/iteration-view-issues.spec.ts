import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {ReferentialDataMockBuilder} from '../../../../utils/referential/referential-data-builder';
import {CampaignWorkspacePage} from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {IterationViewPage} from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import {IterationModel} from '../../../../model/campaign/iteration-model';
import {getEmptyIterationStatisticsBundle, mockIterationModel} from '../../../../data-mock/iteration.data-mock';
import {AuthenticationProtocol} from '../../../../model/bugtracker/bug-tracker.model';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';

const referentialData = new ReferentialDataMockBuilder().withProjects({
  name: 'Project_Issues',
  bugTrackerBinding: {id: 1, bugTrackerId: 1, projectId: 3}
}).withBugTrackers({
  name: 'bugtracker',
  authProtocol: AuthenticationProtocol.BASIC_AUTH
}).build();


describe('Iteration View - Issues', () => {
  it('should display connection page if user not connected to bugtracker', () => {
    const iterationViewPage = navigateToIteration();
    const issuePage = iterationViewPage.showIssuesWithoutBindingToBugTracker();
    const connectionDialog = issuePage.openConnectionDialog();
    connectionDialog.fillUserName('admin');
    connectionDialog.fillPassword('admin');
    connectionDialog.connection();
  });

  it('should display table issues', () => {
    const modelResponse = {
      'entityType': 'campaign',
      'bugTrackerStatus': 'AUTHENTICATED',
      'projectName': '["LELprojet","test","LELprojetclassique","LELclassique","Projet_Test_Arnaud"]',
      'projectId': 328,
      'delete': '',
      'oslc': false
    };
    const campaignViewPage = navigateToIteration();
    const gridResponse = {
      dataRows: [{
        id: 1,
        data: {
          'summary': 'Anomalie',
          'url': 'http://mybutracker.com',
          'remoteId': 'TA-1',
          'priority': '1',
          'status': 'Error',
          'assignee': 'Admin',
          'executionId': '2',
          'executionOrder': '1',
          'executionName': 'Mon cas de test',
          'suiteNames': '',
          'btProject': 'Projet',
        }
      } as unknown as DataRow]
    } as GridResponse;
    const issuePage = campaignViewPage.showIssuesIfBindedToBugTracker(modelResponse, gridResponse);
    const grid = issuePage.issueGrid;

    grid.assertRowCount(1);
    grid.getRow(1).cell('summary').textRenderer().assertContainText('Anomalie');
  });

  function navigateToIteration(items: any[] = []): IterationViewPage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'CampaignLibrary-1',
        children: [],
        data: {'NAME': 'Project1'},
      } as unknown as DataRow]
    };

    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes, referentialData);
    const libraryChildren = [
      {
        id: 'CampaignLibrary-1',
        children: ['Campaign-3'],
        data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'Campaign-3',
        children: [],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        data: {'NAME': 'campaign3', 'CHILD_COUNT': 1}
      } as unknown as DataRow];

    const campaignChildren = [
      {
        id: 'Campaign-3',
        children: ['Iteration-1'],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        state: DataRowOpenState.open,
        data: {'NAME': 'campaign3', 'CHILD_COUNT': 1, 'MILESTONE_STATUS': 'IN_PROGRESS'}
      } as unknown as DataRow,
      {
        id: 'Iteration-1',
        children: [],
        projectId: 1,
        parentRowId: 'Campaign-3',
        data: {'NAME': 'iteration-1', 'CHILD_COUNT': 0, 'MILESTONE_STATUS': 'IN_PROGRESS'}
      } as unknown as DataRow];
    campaignWorkspacePage.tree.openNode('CampaignLibrary-1', libraryChildren);
    campaignWorkspacePage.tree.openNode('Campaign-3', campaignChildren);
    const model: IterationModel = mockIterationModel({
      name: 'iteration-1',
      itpi: items,
      users: [
        {id: 1, login: 'raowl', firstName: 'Ra', lastName: 'Oul'},
        {id: 2, login: 'jawny', firstName: 'Joe', lastName: 'Ni'}
      ]
    });
    new HttpMockBuilder(`iteration-view/${model.id}/statistics`)
      .responseBody(getEmptyIterationStatisticsBundle())
      .build();
    return campaignWorkspacePage.tree.selectNode<IterationViewPage>('Iteration-1', model);
  }
});
