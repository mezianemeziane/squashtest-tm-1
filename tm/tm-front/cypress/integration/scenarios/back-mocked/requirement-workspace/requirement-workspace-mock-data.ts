import {RequirementStatistics} from '../../../model/requirements/requirement-statistics.model';
import {
  ChartColumnType,
  ChartDataType,
  ChartOperation,
  ChartScopeType,
  ChartType
} from '../../../model/custom-report/chart-definition.model';
import {CustomDashboardModel} from '../../../model/custom-report/custom-dashboard.model';

export function getStatistics(): RequirementStatistics {
  return {
    boundTestCasesStatistics: {
      zeroTestCases: 3,
      manyTestCases: 4,
      oneTestCase: 3
    },
    statusesStatistics: {
      approved: 5,
      workInProgress: 4,
      obsolete: 0,
      underReview: 1
    },
    criticalityStatistics: {
      critical: 2,
      major: 3,
      minor: 1,
      undefined: 4
    },
    boundDescriptionStatistics: {
      hasNoDescription: 1,
      hasDescription: 9
    },
    coverageStatistics: {
      critical: 2,
      totalCritical: 2,
      major: 2,
      totalMajor: 3,
      minor: 0,
      totalMinor: 1,
      undefined: 1,
      totalUndefined: 4
    },
    validationStatistics: {
      conclusiveUndefined: 2,
      conclusiveMinor: 5,
      conclusiveMajor: 2,
      conclusiveCritical: 8,

      inconclusiveUndefined: 4,
      inconclusiveMajor: 1,
      inconclusiveMinor: 2,
      inconclusiveCritical: 3,

      undefinedUndefined: 3,
      undefinedMajor: 1,
      undefinedMinor: 2,
      undefinedCritical: 0,
    },
    generatedOn: new Date(),
    selectedIds: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  };
}

export function getFavoriteDashboard(): CustomDashboardModel {
  return {
    id: 2,
    projectId: 1,
    customReportLibraryNodeId: 18,
    name: 'Favorite Dashboard',
    createdBy: 'cypress',
    chartBindings: [
      {
        id: 2,
        chartDefinitionId: 2,
        dashboardId: 2,
        chartInstance: {
          id: 2,
          customReportLibraryNodeId: null,
          projectId: 1,
          name: 'New Chart',
          type: ChartType.PIE,
          measures: [{
            cufId: null,
            label: '',
            column: {
              columnType: ChartColumnType.ATTRIBUTE,
              label: 'TEST_CASE_ID',
              specializedEntityType: {entityType: 'TEST_CASE', entityRole: null},
              dataType: ChartDataType.NUMERIC
            },
            operation: ChartOperation.COUNT
          }],
          axis: [{
            cufId: null,
            label: '',
            column: {
              columnType: ChartColumnType.ATTRIBUTE,
              label: 'TEST_CASE_REFERENCE',
              specializedEntityType: {entityType: 'TEST_CASE', entityRole: null},
              dataType: ChartDataType.STRING
            },
            operation: ChartOperation.NONE
          }],
          filters: [],
          abscissa: [['']],
          series: {'': [2]},
          projectScope: [],
          scope: [],
          scopeType: ChartScopeType.DEFAULT
        },
        row: 1,
        col: 1,
        sizeX: 2,
        sizeY: 2
      }
    ],
    reportBindings: [],
  };
}
