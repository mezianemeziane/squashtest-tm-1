import {RequirementVersionModel} from '../../../../model/requirements/requirement-version.model';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {RequirementVersionViewPage} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view.page';
import {RequirementWorkspacePage} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {defaultReferentialData} from '../../../../utils/referential/default-referential-data.const';
import {RequirementViewPage} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
// tslint:disable-next-line:max-line-length
import {RequirementVersionViewRatePage} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view-rate.page';
import {RequirementVersionStatsBundle} from '../../../../model/requirements/requirement-version-stats-bundle.model';
import {RequirementVersionStats} from '../../../../model/requirements/requirement-version-stats.model';

describe('Requirement View rate', function () {
  it('should display simple rate', () => {
    const page = navigateToRequirementView();
    const ratePanel = page.clickAnchorLink('rate') as RequirementVersionViewRatePage;
    ratePanel.assertExist();

    const totalStatsElement = ratePanel.getTotalStatsElement();

    const redactionRateElement = totalStatsElement.getRedactionRateElement();
    redactionRateElement.assertExist();
    redactionRateElement.assertElementHaveTitle('Couverture');
    redactionRateElement.assertPercentageValue('40');
    redactionRateElement.assertRateValue('2/5');

    const verificationRateElement = totalStatsElement.getVerificationRateElement();
    verificationRateElement.assertExist();
    verificationRateElement.assertElementHaveTitle('Vérification');
    verificationRateElement.assertPercentageValue('50');
    verificationRateElement.assertRateValue('1/2');

    const validationRateElement = totalStatsElement.getValidationRateElement();
    validationRateElement.assertExist();
    validationRateElement.assertElementHaveTitle('Validation');
    validationRateElement.assertPercentageValue('50');
    validationRateElement.assertRateValue('1/2');
  });

  it('should display stats with child', () => {
    const requirementStatsWithChild = {
      children: buildRequirementStats(1, 2, 0, 0, 1, 0),
      total: buildRequirementStats(6, 4, 2, 1, 3, 1),
      currentVersion: buildRequirementStats(5, 2, 2, 1, 2, 1),
      haveChildren: true,
    } as RequirementVersionStatsBundle;

    const page = navigateToRequirementView(requirementStatsWithChild);
    const ratePanel = page.clickAnchorLink('rate') as RequirementVersionViewRatePage;

    const totalStatsElement = ratePanel.getTotalStatsElement();

    const redactionRateElement = totalStatsElement.getRedactionRateElement();
    redactionRateElement.assertPercentageValue('50');
    redactionRateElement.assertRateValue('3/6');
    redactionRateElement.assertHaveWarningColor();

    const verificationRateElement = totalStatsElement.getVerificationRateElement();
    verificationRateElement.assertPercentageValue('50');
    verificationRateElement.assertRateValue('1/2');
    verificationRateElement.assertHaveWarningColor();

    const validationRateElement = totalStatsElement.getValidationRateElement();
    validationRateElement.assertPercentageValue('25');
    validationRateElement.assertRateValue('1/4');
    validationRateElement.assertHaveDangerColor();

    const currentVersionStats = ratePanel.getCurrentVersionStatsElement();

    const currentVersionRedactionRateElement = currentVersionStats.getRedactionRateElement();
    currentVersionRedactionRateElement.assertElementHaveTitle('Cette exigence');
    currentVersionRedactionRateElement.assertPercentageValue('40');
    currentVersionRedactionRateElement.assertRateValue('2/5');
    currentVersionRedactionRateElement.assertHaveDangerColor();

    const currentVerificationRateElement = currentVersionStats.getVerificationRateElement();
    currentVerificationRateElement.assertElementHaveTitle('Cette exigence');
    currentVerificationRateElement.assertPercentageValue('50');
    currentVerificationRateElement.assertRateValue('1/2');
    currentVerificationRateElement.assertHaveWarningColor();


    const currentValidationRateElement = currentVersionStats.getValidationRateElement();
    currentValidationRateElement.assertElementHaveTitle('Cette exigence');
    currentValidationRateElement.assertPercentageValue('50');
    currentValidationRateElement.assertRateValue('1/2');
    currentValidationRateElement.assertHaveWarningColor();

    const childStats = ratePanel.getChildStatsElement();

    const childRedactionRateElement = childStats.getRedactionRateElement();
    childRedactionRateElement.assertElementHaveTitle('Ses filles');
    childRedactionRateElement.assertPercentageValue('100');
    childRedactionRateElement.assertRateValue('1/1');
    childRedactionRateElement.assertHaveValidColor();

    const childVerificationRateElement = childStats.getVerificationRateElement();
    childVerificationRateElement.assertElementHaveTitle('Ses filles');
    childVerificationRateElement.assertPercentageValue('0');
    childVerificationRateElement.assertRateValue('0/0');
    childVerificationRateElement.assertHaveUndefinedColor();

    const childValidationRateElement = childStats.getValidationRateElement();
    childValidationRateElement.assertElementHaveTitle('Ses filles');
    childValidationRateElement.assertPercentageValue('0');
    childValidationRateElement.assertRateValue('0/2');
    childValidationRateElement.assertHaveDangerColor();
  });

  const requirementStats = {
    children: buildRequirementStats(0, 0, 0, 0, 0, 0),
    total: buildRequirementStats(5, 2, 2, 1, 2, 1),
    currentVersion: buildRequirementStats(5, 2, 2, 1, 2, 1),
    haveChildren: false,
  } as RequirementVersionStatsBundle;

  const reqVersionModel = {
    id: 3,
    projectId: 1,
    name: 'Build Cupola',
    reference: 'M4',
    attachmentList: {id: 1, attachments: []},
    customFieldValues: [],
    category: 2,
    createdBy: 'admin',
    createdOn: new Date('2020-09-30 10:30'),
    criticality: 'MAJOR',
    description: '',
    lastModifiedBy: '',
    lastModifiedOn: null,
    milestones: [],
    requirementId: 3,
    status: 'UNDER_REVIEW',
    versionNumber: 1,
    bindableMilestones: [],
    verifyingTestCases: [],
    requirementVersionLinks: []
  } as RequirementVersionModel;

  const initialNodes: GridResponse = {
    count: 4,
    dataRows: [
      {
        id: 'RequirementLibrary-1',
        children: ['Requirement-3'],
        data: {'NAME': 'International Space Station', 'CHILD_COUNT': '1'},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'Requirement-3',
        children: [],
        parentRowId: 'RequirementLibrary-1',
        state: DataRowOpenState.leaf,
        data: {
          'RLN_ID': 3,
          'CHILD_COUNT': 0,
          'NAME': 'M4 - Build Cupola',
          CRITICALITY: 'CRITICAL',
          REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
          HAS_DESCRIPTION: true,
          REQ_CATEGORY_ICON: 'briefcase',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
          REQ_CATEGORY_TYPE: 'SYS',
          COVERAGE_COUNT: 0,
          IS_SYNCHRONIZED: false
        }
      } as unknown as DataRow,
    ]
  };

  function buildRequirementStats(allTestCaseCount: number, executedTestCase: number, plannedTestCase: number,
                                 verifiedTestCase: number, redactedTestCase: number, validatedTestCases: number): RequirementVersionStats {
    return {
      allTestCaseCount,
      executedTestCase,
      plannedTestCase,
      validatedTestCases,
      redactedTestCase,
      verifiedTestCase
    };

  }

  function navigateToRequirementView(requirementVersionStatsBundle: RequirementVersionStatsBundle = requirementStats)
    : RequirementVersionViewPage {
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, defaultReferentialData);
    requirementWorkspacePage.navBar.toggle();
    const reqModelWithStats = {
      ...reqVersionModel,
      requirementStats: requirementVersionStatsBundle
    };
    const requirementViewPage = requirementWorkspacePage.tree.selectNode('Requirement-3', reqModelWithStats) as RequirementViewPage;
    const requirementVersionViewPage = requirementViewPage.currentVersion;
    requirementVersionViewPage.toggleTree();
    return requirementVersionViewPage;
  }
});
