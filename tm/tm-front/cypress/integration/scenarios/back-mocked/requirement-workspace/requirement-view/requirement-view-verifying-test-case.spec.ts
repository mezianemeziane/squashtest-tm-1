import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {RequirementWorkspacePage} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {defaultReferentialData} from '../../../../utils/referential/default-referential-data.const';
import {RequirementVersionModel} from '../../../../model/requirements/requirement-version.model';
import {RequirementViewPage} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import {RequirementVersionViewPage} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view.page';
import {ChangeVerifyingTestCaseOperationReport} from '../../../../model/change-coverage-operation-report';
import {VerifyingTestCase} from '../../../../model/requirements/verifying-test-case';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';

describe('Requirement View verifying test case', function () {
  it('Should display data in table', () => {
    const page = navigateToRequirementView();
    const table = page.verifyingTestCaseTable;
    table.assertRowCount(2);
    const row = table.getRow(1);
    row.assertExist();
    row.cell('projectName').textRenderer().assertContainText('Project 1');
    row.cell('reference').textRenderer().assertContainText('REF.001');
    row.cell('name').linkRenderer().assertContainText('TestCase 1');
    row.cell('milestoneLabels').assertExist();
    row.cell('importance').iconRenderer().assertContainIcon('anticon-sqtm-core-test-case:double_down');
    row.cell('status').iconRenderer().assertContainIcon('anticon-sqtm-core-test-case:status');

    const secondRow = table.getRow(2);
    secondRow.assertExist();
    secondRow.cell('projectName').textRenderer().assertContainText('Project 42');
    secondRow.cell('reference').textRenderer().assertContainText('');
    secondRow.cell('name').linkRenderer().assertContainText('Mon Cas de test');
    secondRow.cell('milestoneLabels').assertExist();
    secondRow.cell('importance').iconRenderer().assertContainIcon('anticon-sqtm-core-test-case:double_up');
    secondRow.cell('status').iconRenderer().assertContainIcon('anticon-sqtm-core-test-case:status');

  });

  it('Should add test case to requirement version', () => {
    const page = navigateToRequirementView();
    const testcaseResponse = {
      dataRows: [
        {
          id: 'TestCaseLibrary-1',
          children: ['TestCase-3'],
          data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
          state: DataRowOpenState.open
        } as unknown as DataRow,
        {
          id: 'TestCase-3',
          children: [],
          data: {
            'NAME': 'a nice test',
            'CHILD_COUNT': 0,
            'TC_STATUS': 'APPROVED',
            'TC_KIND': 'STANDARD',
            'IMPORTANCE': 'HIGH'
          },
          parentRowId: 'TestCaseLibrary-1',
        } as unknown as DataRow
      ]
    } as GridResponse;

    const testcaseDrawer = page.openTestcaseDrawer(testcaseResponse);
    testcaseDrawer.beginDragAndDrop('TestCase-3');
    const otherTestCases: VerifyingTestCase[] = [
      {
        id: 1,
        name: 'TestCase 1',
        importance: 'LOW',
        milestoneLabels: '',
        milestoneMaxDate: null,
        milestoneMinDate: null,
        projectName: 'Project 1',
        reference: 'REF.001',
        status: 'APPROVED'
      },
      {
        id: 2,
        name: 'Mon Cas de test',
        importance: 'VERY_HIGH',
        milestoneLabels: 'Jalon',
        milestoneMaxDate: new Date('2020-10-18'),
        milestoneMinDate: new Date('2020-10-12'),
        projectName: 'Project 42',
        reference: '',
        status: 'TO_BE_UPDATED'
      },
      {
        id: 3,
        name: 'a nice test',
        importance: 'HIGH',
        milestoneLabels: 'Jalon',
        milestoneMaxDate: new Date('2020-10-18'),
        milestoneMinDate: new Date('2020-10-12'),
        projectName: 'Project 42',
        reference: '',
        status: 'APPROVED'
      }
    ];
    const verifyingTestCaseOperationReport: ChangeVerifyingTestCaseOperationReport = {
      verifyingTestCases: otherTestCases,
      summary: {
        notLinkableRejections: false,
        noVerifiableVersionRejections: false,
        alreadyVerifiedRejections: false
      }
    };
    page.dropTestCaseIntoRequirement(3, verifyingTestCaseOperationReport, {dataRows: []});
    page.closeDrawer();
    const table = page.verifyingTestCaseTable;
    const newTestCase = table.getRow(3);
    newTestCase.cell('name').linkRenderer().assertContainText('a nice test');
  });

  it('should remove a testcase row in table', () => {
    const page = navigateToRequirementView();
    const table = page.verifyingTestCaseTable;
    const testcasesDialogElement = page.showDeleteConfirmTestCaseDialog(3, 2);
    testcasesDialogElement.assertExist();
    testcasesDialogElement.deleteForSuccess({
      verifyingTestCases: [
        {
          id: 1,
          name: 'TestCase 1',
          importance: 'LOW',
          milestoneLabels: '',
          milestoneMaxDate: null,
          milestoneMinDate: null,
          projectName: 'Project 1',
          reference: 'REF.001',
          status: 'APPROVED'
        }
      ]
    }, {dataRows: []});
    testcasesDialogElement.assertNotExist();
    table.assertRowCount(1);
  });

  it('should remove many test case rows in table', () => {
    const page = navigateToRequirementView();
    const table = page.verifyingTestCaseTable;
    let testcasesDialogElement = page.showDeleteConfirmTestCasesDialog(3, [1, 2]);
    testcasesDialogElement.assertNotExist();
    table.selectRows([1, 2], '#', 'leftViewport');
    testcasesDialogElement = page.showDeleteConfirmTestCasesDialog(3, [1, 2]);
    testcasesDialogElement.assertExist();
    testcasesDialogElement.deleteForSuccess({
      verifyingTestCases: []
    }, {dataRows: []});
    testcasesDialogElement.assertNotExist();
    table.assertRowCount(0);
  });

  it('should navigate to search test-case for coverages', () => {
    const page = navigateToRequirementView();
    new NavBarElement().toggle();
    page.toggleTree();
    const testCaseForCoverageSearchPage = page.navigateToSearchTestCaseForCoverage();
    testCaseForCoverageSearchPage.assertExist();
    testCaseForCoverageSearchPage.assertLinkSelectionButtonExist();
    testCaseForCoverageSearchPage.assertLinkAllButtonExist();
  });

  const req3Model: RequirementVersionModel = {
    id: 3,
    projectId: 1,
    name: 'Build Cupola',
    reference: 'M4',
    attachmentList: {id: 1, attachments: []},
    customFieldValues: [],
    category: 2,
    createdBy: 'admin',
    createdOn: new Date('2020-09-30 10:30'),
    criticality: 'MAJOR',
    description: '',
    lastModifiedBy: '',
    lastModifiedOn: null,
    milestones: [],
    requirementId: 3,
    status: 'UNDER_REVIEW',
    versionNumber: 1,
    bindableMilestones: [
      {
        id: 1,
        endDate: new Date('2020-09-30 10:30'),
        label: 'Milestone',
        description: '',
        ownerFistName: '',
        ownerLastName: '',
        ownerLogin: '',
        range: 'GLOBAL',
        status: 'IN_PROGRESS',
        lastModifiedOn: new Date('2020-09-30 10:30'),
        lastModifiedBy: 'admin',
        createdOn: new Date('2020-09-30 10:30'),
        createdBy: 'admin'
      }
    ],
    verifyingTestCases: [
      {
        id: 1,
        name: 'TestCase 1',
        importance: 'LOW',
        milestoneLabels: '',
        milestoneMaxDate: null,
        milestoneMinDate: null,
        projectName: 'Project 1',
        reference: 'REF.001',
        status: 'APPROVED'
      },
      {
        id: 2,
        name: 'Mon Cas de test',
        importance: 'VERY_HIGH',
        milestoneLabels: 'Jalon',
        milestoneMaxDate: new Date('2020-10-18'),
        milestoneMinDate: new Date('2020-10-12'),
        projectName: 'Project 42',
        reference: '',
        status: 'TO_BE_UPDATED'
      }
    ],
    requirementVersionLinks: [],
    requirementStats: {
      children: {
        allTestCaseCount: 0,
        executedTestCase: 0,
        plannedTestCase: 0,
        verifiedTestCase: 0,
        redactedTestCase: 0,
        validatedTestCases: 0

      },
      total: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1

      },
      currentVersion: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1

      },
      haveChildren: false,
    },
    remoteReqPerimeterStatus: null
  };

  const initialNodes: GridResponse = {
    count: 2,
    dataRows: [
      {
        id: 'RequirementLibrary-1',
        children: ['Requirement-3'],
        data: {'NAME': 'International Space Station', 'CHILD_COUNT': '1'},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'Requirement-3',
        children: [],
        parentRowId: 'RequirementLibrary-1',
        state: DataRowOpenState.leaf,
        data: {
          'RLN_ID': 3,
          'CHILD_COUNT': 0,
          'NAME': 'M4 - Build Cupola',
          CRITICALITY: 'CRITICAL',
          REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
          HAS_DESCRIPTION: true,
          REQ_CATEGORY_ICON: 'briefcase',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
          REQ_CATEGORY_TYPE: 'SYS',
          COVERAGE_COUNT: 0,
          IS_SYNCHRONIZED: false
        }
      } as unknown as DataRow
    ]
  };

  function navigateToRequirementView(): RequirementVersionViewPage {
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, defaultReferentialData);
    requirementWorkspacePage.navBar.toggle();
    const requirementViewPage = requirementWorkspacePage.tree.selectNode('Requirement-3', req3Model) as RequirementViewPage;
    const requirementVersionViewPage = requirementViewPage.currentVersion;
    requirementVersionViewPage.toggleTree();
    return requirementVersionViewPage;
  }
});
