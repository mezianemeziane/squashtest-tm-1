// tslint:disable-next-line:max-line-length
import {RequirementVersionViewModificationHistoryPage} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view-modification-history.page';
import {RequirementWorkspacePage} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {defaultReferentialData} from '../../../../utils/referential/default-referential-data.const';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {RequirementViewPage} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import {RequirementVersionModel} from '../../../../model/requirements/requirement-version.model';
import {RequirementVersionViewPage} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view.page';

describe('Requirement version modification history', () => {
  it('Should navigate to modification history page and display data in table of an native requirement', () => {
    const requirementVersionViewPage = navigateToRequirementView('Requirement-3', nativeReqModel);

    const history: GridResponse = {
      dataRows: [
        {
          id: 1,
          data: {
            date: '2020-10-20',
            eventId: 1,
            newValue: 'WORK_IN_PROGRESS',
            event: 'status',
            oldValue: 'UNDER_REVIEW',
            user: 'admin'
          }
        } as unknown as DataRow,
        {
          id: 2,
          data: {
            date: '2020-11-21',
            eventId: 2,
            newValue: 'CRITICAL',
            event: 'criticality',
            oldValue: 'MAJOR',
            user: 'admin'
          }
        } as unknown as DataRow,
        {
          id: 3,
          data: {
            date: '2020-11-21',
            eventId: 3,
            newValue: '<p><strong>Hello</strong> world</p>',
            event: 'description',
            oldValue: '',
            user: 'admin'
          }
        } as unknown as DataRow,
      ]
    };

    const historyPage = requirementVersionViewPage.clickAnchorLink('history', history) as RequirementVersionViewModificationHistoryPage;
    const table = historyPage.grid;
    table.assertColumnCount(5);
    table.assertRowCount(3);

    const firstRow = table.getRow(1);
    firstRow.assertExist();
    firstRow.cell('date').textRenderer().assertContainText('20/10/2020');
    firstRow.cell('user').textRenderer().assertContainText('admin');
    // assuming the web browser language is currently French
    firstRow.cell('event').textRenderer().assertContainText('Modification du statut');
    firstRow.cell('oldValue').textRenderer().assertContainText('À approuver');
    firstRow.cell('newValue').textRenderer().assertContainText('En cours de rédaction');

    const secondRow = table.getRow(2);
    secondRow.assertExist();
    secondRow.cell('date').textRenderer().assertContainText('21/11/2020');
    secondRow.cell('user').textRenderer().assertContainText('admin');
    secondRow.cell('event').textRenderer().assertContainText('Modification de la criticité');
    secondRow.cell('oldValue').textRenderer().assertContainText('Majeure');
    secondRow.cell('newValue').textRenderer().assertContainText('Critique');

    const thirdRow = table.getRow(3);
    thirdRow.assertExist();
    thirdRow.cell('date').textRenderer().assertContainText('21/11/2020');
    thirdRow.cell('user').textRenderer().assertContainText('admin');
    thirdRow.cell('event').textRenderer().assertContainText('Modification de la description');
    thirdRow.cell('newValue').textRenderer().assertContainText('');
    const linkCellRenderer = thirdRow.cell('oldValue').linkRenderer();
    linkCellRenderer.assertContainText('Voir les modifications');

    const dialogElement = historyPage.showDescriptionChangeDialog(linkCellRenderer);
    dialogElement.assertExist();
    dialogElement.assertDialogOldValueLabel('Valeur d\'origine');
    dialogElement.assertDialogNewValueLabel('Nouvelle valeur');
    dialogElement.assertDialogOldValue('');
    dialogElement.assertDialogNewValue('<p><strong>Hello</strong> world</p>');

  });

  it('Should navigate to modification history page and display data in table of a sync requirement', () => {
    const requirementVersionViewPage = navigateToRequirementView('Requirement-10', syncReqModel);

    const history: GridResponse = {
      dataRows: [
        {
          id: 11,
          data: {
            date: '2020-11-19',
            eventId: 11,
            newValue: null,
            'syncReqCreationSource': 'http://192.168.1.57:8080/browse/PDQ-1',
            'syncReqUpdateSource': null,
            event: null,
            oldValue: null,
            user: 'jira.xsquash'
          }
        } as unknown as DataRow,
        {
          id: 12,
          data: {
            date: '2020-11-19',
            eventId: 12,
            newValue: null,
            'syncReqCreationSource': null,
            'syncReqUpdateSource': 'http://192.168.1.57:8080/browse/PDQ-1',
            event: null,
            oldValue: null,
            user: 'jira.xsquash'
          }
        } as unknown as DataRow
      ]
    };

    const historyPage = requirementVersionViewPage.clickAnchorLink('history', history) as RequirementVersionViewModificationHistoryPage;
    const table = historyPage.grid;
    table.assertColumnCount(5);
    table.assertRowCount(2);

    const firstRow = table.getRow(11);
    firstRow.assertExist();
    firstRow.cell('date').textRenderer().assertContainText('19/11/2020');
    firstRow.cell('user').textRenderer().assertContainText('jira.xsquash');
    // assuming the web browser language is currently French
    firstRow.cell('event').textRenderer().assertContainText('Création par synchronisation');
    firstRow.cell('newValue').textRenderer().assertContainText('');
    const linkCellRenderer1 = firstRow.cell('oldValue').linkRenderer();
    linkCellRenderer1.assertContainText('Source');

    const secondRow = table.getRow(12);
    secondRow.assertExist();
    secondRow.cell('date').textRenderer().assertContainText('19/11/2020');
    secondRow.cell('user').textRenderer().assertContainText('jira.xsquash');
    secondRow.cell('event').textRenderer().assertContainText('Modification par synchronisation');
    secondRow.cell('newValue').textRenderer().assertContainText('');
    const linkCellRenderer2 = secondRow.cell('oldValue').linkRenderer();
    linkCellRenderer2.assertContainText('Source');
    // verify html content
    linkCellRenderer2.findCellLink().should('have.attr', 'href')
      .and('equal', 'http://192.168.1.57:8080/browse/PDQ-1');
  });

  const initialNodes: GridResponse = {
    count: 4,
    dataRows: [
      {
        id: 'RequirementLibrary-1',
        children: [
          'Requirement-3',
          'Requirement-4',
          'Requirement-5',
          'RequirementFolder-108'
        ],
        data: {'NAME': 'International Space Station', 'CHILD_COUNT': '4'},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'Requirement-3',
        children: [],
        parentRowId: 'RequirementLibrary-1',
        state: DataRowOpenState.leaf,
        data: {
          'RLN_ID': 3,
          'CHILD_COUNT': 0,
          'NAME': 'M4 - Build Cupola',
          CRITICALITY: 'CRITICAL',
          REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
          HAS_DESCRIPTION: true,
          REQ_CATEGORY_ICON: 'briefcase',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
          REQ_CATEGORY_TYPE: 'SYS',
          COVERAGE_COUNT: 0,
          IS_SYNCHRONIZED: false,
          REMOTE_REQ_PERIMETER_STATUS: null,
          REMOTE_SYNCHRONISATION_ID: null
        }
      } as unknown as DataRow,
      {
        id: 'Requirement-4',
        children: [],
        parentRowId: 'RequirementLibrary-1',
        state: DataRowOpenState.leaf,
        data: {
          'RLN_ID': 4,
          'CHILD_COUNT': 0,
          'NAME': 'Repair Cupola',
          CRITICALITY: 'CRITICAL',
          REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
          HAS_DESCRIPTION: false,
          REQ_CATEGORY_ICON: 'briefcase',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
          REQ_CATEGORY_TYPE: 'SYS',
          COVERAGE_COUNT: 0,
          IS_SYNCHRONIZED: false,
          REMOTE_REQ_PERIMETER_STATUS: null,
          REMOTE_SYNCHRONISATION_ID: null
        }
      } as unknown as DataRow,
      {
        id: 'Requirement-5',
        children: [],
        parentRowId: 'RequirementLibrary-1',
        state: DataRowOpenState.leaf,
        data: {
          'RLN_ID': 5,
          'CHILD_COUNT': 0,
          'NAME': 'Kill Cupola',
          CRITICALITY: 'CRITICAL',
          REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
          HAS_DESCRIPTION: true,
          REQ_CATEGORY_ICON: 'briefcase',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
          REQ_CATEGORY_TYPE: 'SYS',
          COVERAGE_COUNT: 0,
          IS_SYNCHRONIZED: false,
          REMOTE_REQ_PERIMETER_STATUS: null,
          REMOTE_SYNCHRONISATION_ID: null
        }
      } as unknown as DataRow,
      {
        id: 'Requirement-10',
        children: [],
        parentRowId: 'RequirementFolder-108',
        state: DataRowOpenState.leaf,
        data: {
          'RLN_ID': 10,
          'CHILD_COUNT': 0,
          'NAME': 'PDQ-1 - Superman',
          CRITICALITY: 'UNDEFINED',
          REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
          HAS_DESCRIPTION: false,
          REQ_CATEGORY_ICON: 'indeterminate_checkbox_empty',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_UNDEFINED',
          REQ_CATEGORY_TYPE: 'SYS',
          COVERAGE_COUNT: 0,
          IS_SYNCHRONIZED: true,
          REMOTE_REQ_PERIMETER_STATUS: 'IN_CURRENT_PERIMETER',
          REMOTE_SYNCHRONISATION_ID: 1
        }
      } as unknown as DataRow,
      {
        id: 'RequirementFolder-108',
        children: ['Requirement-10'],
        parentRowId: 'RequirementLibrary-1',
        state: DataRowOpenState.open,
        data: {
          'RLN_ID': 108,
          'CHILD_COUNT': 1,
          'NAME': 'Avengers',
          'LAST_SYNC_STATUS': 'SUCCESS',
          IS_SYNCHRONIZED: true,
          'REMOTE_SYNCHRONISATION_NAME': 'syn2',
          HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ: false
        }
      } as unknown as DataRow,
    ]
  };

  const nativeReqModel: RequirementVersionModel = {
    id: 3,
    projectId: 1,
    name: 'Build Cupola',
    reference: 'M4',
    attachmentList: {id: 1, attachments: []},
    customFieldValues: [],
    category: 2,
    createdBy: 'admin',
    createdOn: new Date('2020-09-30 10:30'),
    criticality: 'MAJOR',
    description: '<p><strong>Hello</strong> world</p>',
    lastModifiedBy: '',
    lastModifiedOn: null,
    milestones: [],
    requirementId: 3,
    status: 'UNDER_REVIEW',
    versionNumber: 1,
    bindableMilestones: [],
    verifyingTestCases: [],
    requirementVersionLinks: [],
    requirementStats: {
      children: {
        allTestCaseCount: 0,
        executedTestCase: 0,
        plannedTestCase: 0,
        verifiedTestCase: 0,
        redactedTestCase: 0,
        validatedTestCases: 0

      },
      total: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1

      },
      currentVersion: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1

      },
      haveChildren: false,
    },
    remoteReqPerimeterStatus: 'IN_CURRENT_PERIMETER'
  };

  const syncReqModel: RequirementVersionModel = {
    id: 10,
    projectId: 1,
    name: 'Superman',
    reference: 'PDQ-1',
    attachmentList: {id: 1, attachments: []},
    customFieldValues: [],
    category: 6,
    createdBy: 'jira.xsquash',
    createdOn: new Date('2020-11-19 11:00'),
    criticality: 'UNDEFINED',
    description: '',
    lastModifiedBy: 'jira.xsquash',
    lastModifiedOn: new Date('2020-11-19 11:30'),
    milestones: [],
    requirementId: 10,
    status: 'WORK_IN_PROGRESS',
    versionNumber: 1,
    bindableMilestones: [],
    verifyingTestCases: [],
    requirementVersionLinks: [],
    requirementStats: {
      children: {
        allTestCaseCount: 0,
        executedTestCase: 0,
        plannedTestCase: 0,
        verifiedTestCase: 0,
        redactedTestCase: 0,
        validatedTestCases: 0

      },
      total: {
        allTestCaseCount: 0,
        executedTestCase: 0,
        plannedTestCase: 0,
        verifiedTestCase: 0,
        redactedTestCase: 0,
        validatedTestCases: 0

      },
      currentVersion: {
        allTestCaseCount: 0,
        executedTestCase: 0,
        plannedTestCase: 0,
        verifiedTestCase: 0,
        redactedTestCase: 0,
        validatedTestCases: 0

      },
      haveChildren: false,
    },
    remoteReqPerimeterStatus: 'IN_CURRENT_PERIMETER'
  };

  function navigateToRequirementView(reqName: string, reqModel: RequirementVersionModel): RequirementVersionViewPage {
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, defaultReferentialData);
    requirementWorkspacePage.navBar.toggle();
    const requirementViewPage = requirementWorkspacePage.tree.selectNode(reqName, reqModel) as RequirementViewPage;
    const requirementVersionViewPage = requirementViewPage.currentVersion;
    requirementVersionViewPage.toggleTree();
    return requirementVersionViewPage;
  }
});
