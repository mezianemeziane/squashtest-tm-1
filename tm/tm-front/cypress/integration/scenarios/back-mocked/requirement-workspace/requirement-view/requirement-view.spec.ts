import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
// tslint:disable-next-line:max-line-length
import {RequirementWorkspacePage} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {RequirementVersionModel} from '../../../../model/requirements/requirement-version.model';
import {RequirementViewPage} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {defaultReferentialData} from '../../../../utils/referential/default-referential-data.const';
// tslint:disable-next-line:max-line-length
import {RequirementVersionViewInformationPage} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view-information.page';
// tslint:disable-next-line:max-line-length
import {RequirementMultiSelectionPage} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-multi-selection.page';


function getRequirementLibraryChildNodes() {
  return [
    {
      id: 'RequirementLibrary-1',
      children: ['RequirementFolder-1', 'Requirement-3', 'RequirementFolder-2'],
      data: {'NAME': 'International Space Station', 'CHILD_COUNT': '3'},
      state: DataRowOpenState.open
    } as unknown as DataRow,
    {
      id: 'RequirementFolder-1',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      data: {'NAME': 'Structural Requirements'},
      state: DataRowOpenState.closed
    } as unknown as DataRow,
    {
      id: 'Requirement-3',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        'RLN_ID': 3,
        'CHILD_COUNT': 0,
        'NAME': 'M4 - Build Cupola',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false
      }
    } as unknown as DataRow,
    {
      id: 'RequirementFolder-2',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      data: {'NAME': 'Functional Requirements'},
      state: DataRowOpenState.closed
    } as unknown as DataRow
  ];
}

function updateRowData(beforeUpdateDataRow: DataRow, dataProperty: string, value: string): DataRow {
  const data = {...beforeUpdateDataRow.data};
  data[dataProperty] = value;
  return {
    ...beforeUpdateDataRow,
    data
  }
    ;
}

describe('Requirement View Display', function () {

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [
      {
        id: 'RequirementLibrary-1',
        children: [],
        data: {'NAME': 'International Space Station', 'CHILD_COUNT': '3'}
      } as unknown as DataRow,
      {
        id: 'RequirementLibrary-2',
        children: [],
        data: {'NAME': 'STS - Shuttle', 'CHILD_COUNT': '3'}
      } as unknown as DataRow
    ]
  };

  const req3Model: RequirementVersionModel = {
    id: 3,
    projectId: 1,
    name: 'Build Cupola',
    reference: 'M4',
    attachmentList: {id: 1, attachments: []},
    customFieldValues: [],
    category: 2,
    createdBy: 'admin',
    createdOn: new Date('2020-09-30 10:30'),
    criticality: 'MAJOR',
    description: '',
    lastModifiedBy: '',
    lastModifiedOn: null,
    milestones: [],
    requirementId: 3,
    status: 'UNDER_REVIEW',
    versionNumber: 1,
    bindableMilestones: [
      {
        id: 1,
        endDate: new Date('2020-09-30 10:30'),
        label: 'Milestone',
        description: '',
        ownerFistName: '',
        ownerLastName: '',
        ownerLogin: '',
        range: 'GLOBAL',
        status: 'IN_PROGRESS',
        createdBy: 'admin',
        createdOn: new Date('2020-09-30 10:30'),
        lastModifiedBy: 'admin',
        lastModifiedOn: new Date('2020-09-30 10:30'),
      }
    ],
    verifyingTestCases: [],
    requirementVersionLinks: [],
    requirementStats: {
      children: {
        allTestCaseCount: 0,
        executedTestCase: 0,
        plannedTestCase: 0,
        verifiedTestCase: 0,
        redactedTestCase: 0,
        validatedTestCases: 0

      },
      total: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1

      },
      currentVersion: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1

      },
      haveChildren: false,
    },
    remoteReqPerimeterStatus: null
  };

  it('should display a requirement', () => {

    const requirementPage = initAtRequirementViewPage();
    const currentVersion = requirementPage.currentVersion;
    currentVersion.assertExist();
    currentVersion.assertNameContains('Build Cupola');
    currentVersion.assertReferenceContains('M4');
  });

  it('should change information of requirement version', () => {
    let initialReqRowForTest = initialReqRow;
    const requirementPage = initAtRequirementViewPage();
    const currentVersion = requirementPage.currentVersion;
    const informationPage = currentVersion.clickAnchorLink('information') as RequirementVersionViewInformationPage;

    initialReqRowForTest = updateRowData(initialReqRowForTest, 'NAME', 'Kill Cupola');
    informationPage.rename('Kill Cupola', {dataRows: [initialReqRowForTest]});
    currentVersion.assertNameContains('Kill');
    informationPage.changeCategory('Métier');
    informationPage.changeCriticality('Mineure');
    informationPage.bindMilestone(1, []);
    const milestoneTagElement = informationPage.milestoneTagElement;
    milestoneTagElement.checkMilestones('Milestone');
    informationPage.changeDescription('Ma nouvelle description');
    informationPage.descriptionElement.checkTextContent('Ma nouvelle description');

    initialReqRowForTest = updateRowData(initialReqRowForTest, 'REQUIREMENT_STATUS', 'APPROVED');
    informationPage.changeStatus('Approuvée', {dataRows: [initialReqRowForTest]});

  });

  it('should create a new version', () => {
    const requirementViewPage = initAtRequirementViewPage();
  });

  function initAtRequirementViewPage(): RequirementViewPage {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, defaultReferentialData);
    new NavBarElement().toggle();
    const tree = requirementWorkspacePage.tree;
    tree.assertNodeExist(firstNode.id);
    tree.assertNodeTextContains(firstNode.id, firstNode.data['NAME']);
    tree.openNode(firstNode.id, getRequirementLibraryChildNodes());
    tree.assertNodeIsOpen(firstNode.id);
    const requirementPage: RequirementViewPage = tree.selectNode('Requirement-3', {...req3Model});
    requirementPage.assertExist();
    return requirementPage;
  }

});


const initialReqRow: DataRow = {
  'id': 'Requirement-3',
  'children': [],
  'state': 'leaf',
  'data': {
    'RLN_ID': 3,
    'NAME': 'M4 - Build Cupola',
    'HAS_DESCRIPTION': true,
    'REFERENCE': 'M4',
    'CRITICALITY': 'CRITICAL',
    'REQUIREMENT_STATUS': 'WORK_IN_PROGRESS',
    'REQ_CATEGORY_ICON': 'briefcase',
    'REQ_CATEGORY_LABEL': 'requirement.category.CAT_BUSINESS',
    'REQ_CATEGORY_TYPE': 'SYS',
    'CHILD_COUNT': 0,
    'COVERAGE_COUNT': 0,
    'IS_SYNCHRONIZED': false
  }
} as unknown as DataRow;
