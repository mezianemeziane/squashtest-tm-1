import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder
} from '../../../../utils/referential/referential-data-builder';
import {GroupedMultiListElement} from '../../../../page-objects/elements/filters/grouped-multi-list.element';
import {InputType} from '../../../../model/customfield/customfield.model';
import {BindableEntity} from '../../../../model/bindable-entity.model';
import {RequirementSearchPage} from '../../../../page-objects/pages/requirement-workspace/search/requirement-search-page';
import {EditableDateFieldElement} from '../../../../page-objects/elements/forms/editable-date-field.element';
import {UserView} from '../../../../model/user/user-view';

const HISTORICAL_GROUP_LABEL = 'Historique';
const CREATED_BY_LABEL = 'Créée par';
const CREATED_ON_LABEL = 'Créée le';
const CUSTOM_FIELD_GROUP_LABEL = 'Champs personnalisés';
const CUSTOM_FIELD_TAG_OR_OPERATION = 'Contient au moins une valeur';
const CUSTOM_FIELD_TAG_AND_OPERATION = 'Contient toutes les valeurs';
const ATTACH_COUNT = 'Nombre de pièces jointes';
const MODIFIED_BY_LABEL = 'Modifiée par';
const MODIFIED_ON_LABEL = 'Modifiée le';
const MILESTONE_NAME = 'Nom du jalon';
const MILESTONE_STATUS = 'Statut du jalon';
const MILESTONE_END_DATE = 'Date d\'échéance';

const OPERATION_LABELS = {
  EQUALS: 'Égal',
  GREATER: 'Supérieur',
  GREATER_EQUALS: 'Supérieur ou égal',
  BETWEEN: 'Entre'
};

function referentialDataWithCustomInfoLists() {
  return new ReferentialDataMockBuilder()
    .withProjects({
        name: 'Project 1',
        permissions: ALL_PROJECT_PERMISSIONS,
      },
      {
        name: 'Project 2',
        permissions: ALL_PROJECT_PERMISSIONS,
      },
      {
        name: 'Project 3',
        permissions: ALL_PROJECT_PERMISSIONS,
      })
    .withInfoLists(
      {
        label: 'Space Shuttles',
        items: [
          {label: 'Atlantys'},
          {label: 'Endeavour'},
          {label: 'Discovery'},
          {label: 'Columbia'},
          {label: 'Challenger'},
        ],
        boundToProject: [
          {projectIndex: 0, role: 'requirementCategory'}
        ]
      },
      {
        label: 'Rockets',
        items: [
          {label: 'Saturn 5'},
          {label: 'Proton'},
          {label: 'Ariane 6'},
        ],
        boundToProject: [
          {projectIndex: 1, role: 'requirementCategory'}
        ]
      },
    )
    .withCustomFields(
      {
        code: 'MISSION_TAG',
        inputType: InputType.TAG,
        label: 'Objectif de mission',
        name: 'Objectif de mission',
        optional: true,
        options: [
          {
            label: 'ISS',
          },
          {
            label: 'Hubble',
          },
          {
            label: 'Scientific experiences',
          }
        ],
        bindings: [{bindableEntities: [BindableEntity.REQUIREMENT_VERSION], projectIndexes: [0, 2]}]
      },
      {
        code: 'ENGINE_CODE',
        inputType: InputType.PLAIN_TEXT,
        label: 'Moteur du lanceur',
        name: 'Moteur principal du lanceur',
        optional: true,
        bindings: [{bindableEntities: [BindableEntity.REQUIREMENT_VERSION], projectIndexes: [2]}]
      },
      {
        code: 'Numéro de mission',
        inputType: InputType.NUMERIC,
        label: 'Numéro de mission',
        name: 'Numéro de mission',
        optional: true,
        bindings: [{bindableEntities: [BindableEntity.REQUIREMENT_VERSION], projectIndexes: [0]}]
      },
      {
        code: 'Date de mission',
        inputType: InputType.DATE_PICKER,
        label: 'Date de mission',
        name: 'Date de mission',
        optional: true,
        bindings: [{bindableEntities: [BindableEntity.REQUIREMENT_VERSION], projectIndexes: [0]}]
      },
      {
        code: 'Astronautes',
        inputType: InputType.DROPDOWN_LIST,
        label: 'Astronautes',
        name: 'Astronautes',
        optional: true,
        bindings: [{bindableEntities: [BindableEntity.REQUIREMENT_VERSION], projectIndexes: [0]}],
        options: [{
          label: 'Gaspard'
        }, {
          label: 'Jacob'
        }]
      },
      {
        code: 'Révision',
        inputType: InputType.CHECKBOX,
        label: 'Révision',
        name: 'Révision',
        optional: true,
        bindings: [{bindableEntities: [BindableEntity.REQUIREMENT_VERSION], projectIndexes: [0]}],
      },
    ).withMilestones({
        label: 'Milestone 1',
        status: 'IN_PROGRESS',
        endDate: new Date('2020-05-19'),
        boundProjectIndexes: [0, 1]
      },
      {
        label: 'Milestone 2',
        status: 'IN_PROGRESS',
        endDate: new Date('2020-05-19'),
        boundProjectIndexes: [0, 1]
      },
      {
        label: 'Milestone 3',
        status: 'PLANNED',
        endDate: new Date('2020-05-19'),
        boundProjectIndexes: [0]
      },
      {
        label: 'Milestone 4',
        status: 'FINISHED',
        endDate: new Date('2020-05-19'),
        boundProjectIndexes: [0]
      }
    )
    .build();
}

function checkSpaceShuttles(multiListCriteria: GroupedMultiListElement) {
  multiListCriteria.assertGroupExist('Space Shuttles');
  multiListCriteria.assertGroupContain('Space Shuttles', [
    'Atlantys',
    'Endeavour',
    'Discovery',
    'Columbia',
    'Challenger',
  ]);
}

function checkRockets(multiListCriteria: GroupedMultiListElement) {
  multiListCriteria.assertGroupExist('Rockets');
  multiListCriteria.assertGroupContain('Rockets', [
    'Saturn 5',
    'Proton',
    'Ariane 6',
  ]);
}


function checkDefaultInfoList(multiListCriteria: GroupedMultiListElement) {
  multiListCriteria.assertGroupExist('Liste par défaut');
  multiListCriteria.assertGroupContain('Liste par défaut', [
    'Fonctionnelle',
    'Non fonctionnelle',
    'Métier',
    'Cas d\'utilisation',
    'Exigence de test',
    'Non définie',
    'Ergonomique',
    'Performance',
    'Technique',
    'User story',
    'Sécurité',
  ]);
}

function checkNotFilteredList(multiListCriteria: GroupedMultiListElement) {
  checkSpaceShuttles(multiListCriteria);
  checkRockets(multiListCriteria);
  checkDefaultInfoList(multiListCriteria);
}


describe('Requirement Search', function () {

  beforeEach(() => {
    cy.viewport(1200, 720);
  });

  it('should display requirement search page', () => {
    const requirementSearchPage = RequirementSearchPage.initTestAtPage();
    requirementSearchPage.grid.filterPanel.assertPerimeterIsActive();
    requirementSearchPage.grid.filterPanel.assertPerimeterHasValue('project 1');
    requirementSearchPage.grid.filterPanel.assertAddCriteriaLinkIsPresent();
    requirementSearchPage.grid.filterPanel.assertCriteriaIsActive('Versions');
    requirementSearchPage.grid.filterPanel.assertCriteriaHasValue('Versions', 'Toutes les versions');
  });

  it('should show criteria list', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const testCaseResearchPage = RequirementSearchPage.initTestAtPage(referentialData);
    const criteriaList = testCaseResearchPage.grid.filterPanel.openCriteriaList();
    criteriaList.assertGroupExist('Informations');
    criteriaList.assertGroupContain('Informations', [
      'Nom',
      'Référence',
      'ID',
      'Description',
    ]);
    criteriaList.assertGroupExist(HISTORICAL_GROUP_LABEL);
    criteriaList.assertGroupContain(HISTORICAL_GROUP_LABEL, [
      // CREATED_ON_LABEL,
      CREATED_BY_LABEL,
      // MODIFIED_ON_LABEL,
      MODIFIED_BY_LABEL
    ]);
    // criteriaList.assertGroupExist('Attributs');
    // criteriaList.assertGroupContain('Attributs', [
    //   'Criticité',
    //   'Catégorie',
    //   'Statut',
    // ]);
    // criteriaList.assertGroupExist('Jalons');
    // criteriaList.assertGroupContain('Jalons', [
    //   MILESTONE_NAME,
    //   MILESTONE_STATUS,
    //   MILESTONE_END_DATE
    // ]);
    // criteriaList.assertGroupExist('Contenu');
    // criteriaList.assertGroupContain('Contenu', [
    //   'Nombre de pièce jointes',
    //   'Présence d\'une description',
    // ]);
    //
    // criteriaList.assertGroupExist('Associations');
    // criteriaList.assertGroupContain('Associations', [
    //   'Nombre de cas de test',
    // ]);


    //
    //
    // criteriaList.assertGroupExist(CUSTOM_FIELD_GROUP_LABEL);
    // criteriaList.assertGroupContain(CUSTOM_FIELD_GROUP_LABEL, [
    //   'Objectif de mission',
    //   'Moteur du lanceur',
    //   'Numéro de mission',
    // ]);
    // criteriaList.assertNoItemIsSelected();
  });

  /**
   * No need to retest the whole multiList component. The component used here is the same as test case nature,
   * so the testing is done only one time in test-case-search.spec
   */
  it('should show category options', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const requirementSearchPage = RequirementSearchPage.initTestAtPage(referentialData);
    const multiListCriteria = requirementSearchPage.grid.filterPanel.selectMultiListCriteria('Catégorie');
    checkNotFilteredList(multiListCriteria);
  });

  it('should add name criteria and research', () => {
    const requirementSearchPage = RequirementSearchPage.initTestAtPage();
    const textFilterWidgetElement = requirementSearchPage.grid.filterPanel.selectTextCriteria('Nom');
    textFilterWidgetElement.cancel();
    textFilterWidgetElement.assertNotExist();
    requirementSearchPage.grid.filterPanel.inactivateCriteria('Nom');
    requirementSearchPage.grid.filterPanel.fillTextCriteria('Nom', 'STS');
    requirementSearchPage.grid.filterPanel.assertCriteriaIsActive('Nom');
    // for now research do a toLowerCase on values
    requirementSearchPage.grid.filterPanel.assertCriteriaHasValue('Nom', 'sts');
    requirementSearchPage.grid.filterPanel.openExistingCriteria('Nom');
  });

  it('should add text cuf criteria and research', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const requirementSearchPage = RequirementSearchPage.initTestAtPage(referentialData);
    requirementSearchPage.grid.filterPanel.fillTextCriteria('Moteur du lanceur', 'J5');
    requirementSearchPage.grid.filterPanel.assertCriteriaIsActive('Moteur du lanceur');
    // for now research do a toLowerCase on values
    requirementSearchPage.grid.filterPanel.assertCriteriaHasValue('Moteur du lanceur', 'j5');
    const criteriaList = requirementSearchPage.grid.filterPanel.openCriteriaList();
    criteriaList.assertItemIsSelected('Moteur du lanceur');
  });

  it('should add attachment count criteria and research', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const requirementSearchPage = RequirementSearchPage.initTestAtPage(referentialData);
    const numericCriteria = requirementSearchPage.grid.filterPanel.selectNumericCriteria(ATTACH_COUNT);
    numericCriteria.assertOperationChosen(OPERATION_LABELS.EQUALS);
    numericCriteria.changeOperation(OPERATION_LABELS.GREATER);
    numericCriteria.fillInputMin('1');
    numericCriteria.update();
    requirementSearchPage.grid.filterPanel.assertCriteriaHasValue(ATTACH_COUNT, `${OPERATION_LABELS.GREATER} 1`);
    requirementSearchPage.grid.filterPanel.openExistingCriteria(ATTACH_COUNT);
    numericCriteria.changeOperation(OPERATION_LABELS.BETWEEN);
    numericCriteria.fillInputMin('1');
    numericCriteria.fillInputMax('3');
    numericCriteria.update();
    requirementSearchPage.grid.filterPanel.assertCriteriaHasValue(ATTACH_COUNT, `${OPERATION_LABELS.BETWEEN} 1 et 3`);
  });

  it('should show milestone criteria', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const requirementSearchPage = RequirementSearchPage.initTestAtPage(referentialData);
    const multiList = requirementSearchPage.grid.filterPanel.selectMultiListCriteria(MILESTONE_NAME);
    requirementSearchPage.grid.filterPanel.assertCriteriaIsActive(MILESTONE_NAME);
    multiList.assertGroupContain('ungrouped-items', ['Milestone 1', 'Milestone 2', 'Milestone 3', 'Milestone 4']);
    multiList.close();
    const projectScope = requirementSearchPage.grid.filterPanel.openProjectScopeSelector();
    projectScope.toggleOneItem('Project 1');
    projectScope.close();
    requirementSearchPage.grid.filterPanel.openExistingCriteria(MILESTONE_NAME);
    multiList.assertGroupContain('ungrouped-items', ['Milestone 1', 'Milestone 2']);
  });

  it('should show milestone status criteria', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const requirementSearchPage = RequirementSearchPage.initTestAtPage(referentialData);
    const multiList = requirementSearchPage.grid.filterPanel.selectMultiListCriteria(MILESTONE_STATUS);
    multiList.assertGroupContain('ungrouped-items', ['En cours', 'Terminé', 'Verrouillé']);
    multiList.toggleOneItem('Terminé');
    multiList.toggleOneItem('Verrouillé');
    multiList.close();
    requirementSearchPage.grid.filterPanel.assertCriteriaHasValue(MILESTONE_STATUS, 'Terminé, Verrouillé');
  });

  it('should add createdOn criteria and research', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const requirementSearchPage = RequirementSearchPage.initTestAtPage(referentialData);
    const dateCriteria = requirementSearchPage.grid.filterPanel.selectDateCriteria(CREATED_ON_LABEL);
    dateCriteria.assertOperationChosen(OPERATION_LABELS.EQUALS);
    dateCriteria.changeOperation(OPERATION_LABELS.GREATER);
    dateCriteria.fillTodayDate();
    dateCriteria.update();
    const localeToday = EditableDateFieldElement.dateToDisplayString(new Date(Date.now()));
    requirementSearchPage.grid.filterPanel.assertCriteriaHasValue(CREATED_ON_LABEL, `${OPERATION_LABELS.GREATER} ${localeToday}`);
    requirementSearchPage.grid.filterPanel.openExistingCriteria(CREATED_ON_LABEL);
    dateCriteria.changeOperation(OPERATION_LABELS.BETWEEN);
    dateCriteria.assertRangeComponentExist();
  });

  it('should add createdBy criteria and research', () => {
    const usersWhoCreatedRequirements: UserView[] = [
      {login: 'n-armstrong', firstName: 'Neil', lastName: 'Armstrong', id: 1},
      {login: 'b-aldrin', firstName: 'Buzz', lastName: 'Aldrin', id: 2},
      {login: 'm-collins', firstName: 'Michael', lastName: 'Collins', id: 3},
    ];

    const usersWhoModifiedRequirements = [
      {login: 'n-armstrong', firstName: 'Neil', lastName: 'Armstrong', id: 1},
      {login: 'b-aldrin', firstName: 'Buzz', lastName: 'Aldrin', id: 2},
    ];

    const referentialData = referentialDataWithCustomInfoLists();
    const requirementSearchPage = RequirementSearchPage.initTestAtPage(
      referentialData,
      undefined,
      {usersWhoModifiedRequirements, usersWhoCreatedRequirements});
    const createdByList = requirementSearchPage.grid.filterPanel.selectMultiListCriteria(CREATED_BY_LABEL);
    createdByList.assertGroupContain('ungrouped-items', [
      'Neil Armstrong (n-armstrong)',
      'Buzz Aldrin (b-aldrin)',
      'Michael Collins (m-collins)',
    ]);
    createdByList.toggleOneItem('Neil Armstrong (n-armstrong)');
    createdByList.close();
    requirementSearchPage.grid.filterPanel.assertCriteriaHasValue(CREATED_BY_LABEL, 'n-armstrong');
    const modifiedByList = requirementSearchPage.grid.filterPanel.selectMultiListCriteria(MODIFIED_BY_LABEL);
    modifiedByList.assertGroupContain('ungrouped-items', [
      'Neil Armstrong (n-armstrong)',
      'Buzz Aldrin (b-aldrin)',
    ]);
    modifiedByList.toggleOneItem('Buzz Aldrin (b-aldrin)');
    modifiedByList.close();
    requirementSearchPage.grid.filterPanel.assertCriteriaHasValue(MODIFIED_BY_LABEL, 'b-aldrin');

  });

  it('should add ID list criteria and search', () => {
    const referentialData = referentialDataWithCustomInfoLists();
    const requirementSearchPage = RequirementSearchPage.initTestAtPage(referentialData);
    const idListCriteria = requirementSearchPage.grid.filterPanel.selectNumericSetCriteria(`Liste d'ID`);
    idListCriteria.assertExistWithOutOperationSelector();
    idListCriteria.fillInput('4, 1, ,  12, abcd,,');
    idListCriteria.update();
    requirementSearchPage.grid.filterPanel.assertCriteriaHasValue(`Liste d'ID`, `1, 4, 12`);
  });

  // it('should restore filters from url query param', () => {
  //   const referentialData = referentialDataWithCustomInfoLists();
  //   const filters: SimpleFilter[] = [
  //     {
  //       id: 'status',
  //       operation: FilterOperation.IN,
  //       value: {
  //         kind: 'multiple-discrete-value',
  //         value: [{
  //           id: 'WORK_IN_PROGRESS',
  //           label: 'En cours de rédaction'
  //         }]
  //       }
  //     },
  //     {
  //       id: 'name',
  //       operation: FilterOperation.LIKE,
  //       value: {
  //         kind: 'single-string-value',
  //         value: 'Atlantys'
  //       }
  //     }
  //   ];
  //   const queryString = `filters=${JSON.stringify(filters)}`;
  //   const testCaseResearchPage = TestCaseResearchPage.initTestAtPage(referentialData, undefined, undefined, queryString);
  //   testCaseResearchPage.grid.filterPanel.assertCriteriaIsActive('Statut');
  //   testCaseResearchPage.grid.filterPanel.assertCriteriaHasValue('Statut', 'En cours de rédaction');
  //   testCaseResearchPage.grid.filterPanel.assertCriteriaIsActive('Nom');
  //   testCaseResearchPage.grid.filterPanel.assertCriteriaHasValue('Nom', 'Atlantys');
  // });
  //


});

