import {ReferentialDataMockBuilder} from '../../../../utils/referential/referential-data-builder';
import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {RequirementWorkspacePage} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {MilestoneInformationPanel} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-milestone-view.page';
// tslint:disable-next-line:max-line-length
import {RequirementStatisticPanelElement} from '../../../../page-objects/pages/requirement-workspace/panels/requirement-statistic-panel.element';
import {MilestoneRequirementDashboardModel} from '../../../../model/requirements/milestone-requirement-dashboard.model';


const statistics = {
  boundTestCasesStatistics: {
    zeroTestCases: 2,
    oneTestCase: 0,
    manyTestCases: 0
  },
  statusesStatistics: {
    workInProgress: 1,
    underReview: 1,
    approved: 0,
    obsolete: 0
  },
  criticalityStatistics: {
    undefined: 0,
    minor: 0,
    major: 1,
    critical: 1
  },
  boundDescriptionStatistics: {
    hasDescription: 2,
    hasNoDescription: 0
  },
  coverageStatistics: {
    undefined: 0,
    minor: 0,
    major: 0,
    critical: 0,
    totalUndefined: 0,
    totalMinor: 0,
    totalMajor: 1,
    totalCritical: 1
  },
  validationStatistics: {
    conclusiveUndefined: 0,
    conclusiveMinor: 0,
    conclusiveMajor: 0,
    conclusiveCritical: 0,
    inconclusiveUndefined: 0,
    inconclusiveMajor: 0,
    inconclusiveMinor: 0,
    inconclusiveCritical: 0,
    undefinedUndefined: 0,
    undefinedMajor: 0,
    undefinedMinor: 0,
    undefinedCritical: 0
  },
  selectedIds: [347237, 345972]
};

const endDate = new Date(2020, 6, 28);

const ORBITAL_RDV = 'orbital rendez-vous';

function buildReferentialData() {
  const referentialDataMock = new ReferentialDataMockBuilder()
    .withProjects(
      {name: 'Apollo', label: 'Apollo'}, {name: 'Gemini', label: 'Gemini'}
    ).withMilestones(
      {
        label: ORBITAL_RDV,
        description: '',
        endDate,
        status: 'IN_PROGRESS',
        boundProjectIndexes: [0, 1],
        range: 'GLOBAL',
        ownerFistName: 'admin',
        ownerLastName: 'admin',
        ownerLogin: 'admin',
      },
      {
        label: 'saturn 5 tests',
        description: '',
        endDate: new Date(),
        status: 'IN_PROGRESS',
        boundProjectIndexes: [1],
        range: 'GLOBAL',
        ownerFistName: 'admin',
        ownerLastName: 'admin',
        ownerLogin: 'admin',
      }
    )
    .withUser({functionalTester: true})
    .build();
  referentialDataMock.globalConfiguration.milestoneFeatureEnabled = true;
  return referentialDataMock;
}

const MILESTONE_LABEL_CRITERIA = 'Nom du jalon';

describe('Requirement Milestone Dashboard View', () => {
  it('should change milestone in menu above tree', () => {
    const requirementWorkspacePage: RequirementWorkspacePage = navigateToRequirementWorkspace();
    const milestoneSelector = requirementWorkspacePage.navBar.openMilestoneSelector();
    milestoneSelector.selectMilestone(ORBITAL_RDV);
    milestoneSelector.confirm();
    requirementWorkspacePage.treeMenu.assertMilestoneButtonIsVisible();
    const rqMilestoneSelector = requirementWorkspacePage.treeMenu.showMilestoneSelector();
    rqMilestoneSelector.assertMilestoneIsSelected(ORBITAL_RDV);
    rqMilestoneSelector.selectMilestone('saturn 5 tests');
    rqMilestoneSelector.confirm();
    requirementWorkspacePage.treeMenu.showMilestoneSelector();
    rqMilestoneSelector.assertMilestoneIsSelected('saturn 5 tests');
    rqMilestoneSelector.cancel();
    requirementWorkspacePage.navBar.assertMilestoneModeIsActive();
    requirementWorkspacePage.navBar.openMilestoneSelector();
    milestoneSelector.assertMilestoneIsSelected('saturn 5 tests');
  });

  it('should display requirement milestone dashboard', () => {
    const requirementWorkspacePage = navigateToRequirementWorkspace();
    requirementWorkspacePage.assertExist();
    const milestoneSelector = requirementWorkspacePage.navBar.openMilestoneSelector();
    milestoneSelector.selectMilestone(ORBITAL_RDV);
    milestoneSelector.confirm();
    const milestonePage = requirementWorkspacePage.treeMenu
      .showRequirementMilestoneDashboard({statistics: statistics} as unknown as MilestoneRequirementDashboardModel);
    milestonePage.assertNameEquals(ORBITAL_RDV);
    const informationPanel = milestonePage.clickAnchorLink<MilestoneInformationPanel>('information');
    informationPanel.assertStatusEquals('En cours');
    informationPanel.assertEndDateEquals('28/07/2020');
    const dashboardPanel = milestonePage.clickAnchorLink<RequirementStatisticPanelElement>('dashboard');
    dashboardPanel.assertTitleExist('Tableau de bord');
    dashboardPanel.orphanRequirementChart.assertChartExist();
    dashboardPanel.orphanRequirementChart.assertHasTitle('Couverture par les cas de test');
    dashboardPanel.statusChart.assertChartExist();
    dashboardPanel.statusChart.assertHasTitle('Statut');
    dashboardPanel.criticalityChart.assertChartExist();
    dashboardPanel.criticalityChart.assertHasTitle('Criticité');
    dashboardPanel.descriptionChart.assertChartExist();
    dashboardPanel.descriptionChart.assertHasTitle('Description');
    dashboardPanel.coverageByCriticalityChart.assertChartExist();
    // dashboardPanel.coverageByCriticalityChart.assertHasTitle('Couverture par criticité');
    dashboardPanel.validationByCriticalityChart.assertChartExist();
    // dashboardPanel.validationByCriticalityChart.assertHasTitle('Validation des Cdt par criticité');
    dashboardPanel.assertFooterContains('Total des exigences : 2');
  });
});

function navigateToRequirementWorkspace(): RequirementWorkspacePage {
  const referentialDataMock = buildReferentialData();

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'RequirementLibrary-1',
      children: [],
      data: {'NAME': 'Project 1', 'MILESTONES': [1, 2]}
    } as unknown as DataRow]
  };
  return RequirementWorkspacePage.initTestAtPage(initialNodes, referentialDataMock);
}
