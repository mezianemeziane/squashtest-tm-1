import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {RequirementWorkspacePage} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';

describe('Requirement Workspace Tree - Xsquash4jira synchronized requirements', function () {

  const initialNodes: GridResponse = {
    count: 3,
    dataRows: [
      {
        id: 'RequirementLibrary-1',
        children: [
          'RequirementFolder-101',
          'RequirementFolder-102',
          'RequirementFolder-103',
        ],
        data: {'NAME': 'Xsquash4jira', 'CHILD_COUNT': '3'},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'RequirementFolder-101',
        children: ['Requirement-1'],
        parentRowId: 'RequirementLibrary-1',
        state: DataRowOpenState.open,
        data: {
          RLN_ID: 101,
          CHILD_COUNT: 1,
          NAME: 'Sync1',
          LAST_SYNC_STATUS: 'SUCCESS',
          IS_SYNCHRONIZED: true,
          REMOTE_SYNCHRONISATION_NAME: 'sync1',
          HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ: false
        }
      } as unknown as DataRow,
      {
        id: 'RequirementFolder-102',
        children: ['Requirement-2', 'Requirement-3'],
        parentRowId: 'RequirementLibrary-1',
        state: DataRowOpenState.open,
        data: {
          RLN_ID: 102,
          CHILD_COUNT: 2,
          NAME: 'Sync2',
          LAST_SYNC_STATUS: 'SUCCESS',
          IS_SYNCHRONIZED: true,
          REMOTE_SYNCHRONISATION_NAME: 'sync2',
          HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ: true
        }
      } as unknown as DataRow,
      {
        id: 'RequirementFolder-103',
        children: ['Requirement-4'],
        parentRowId: 'RequirementLibrary-1',
        state: DataRowOpenState.open,
        data: {
          RLN_ID: 103,
          CHILD_COUNT: 1,
          NAME: 'Sync3',
          LAST_SYNC_STATUS: 'FAILURE',
          IS_SYNCHRONIZED: true,
          REMOTE_SYNCHRONISATION_NAME: 'sync3',
          HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ: false
        }
      } as unknown as DataRow,
      {
        id: 'Requirement-1',
        children: [],
        parentRowId: 'RequirementFolder-101',
        state: DataRowOpenState.leaf,
        data: {
          'RLN_ID': 1,
          'CHILD_COUNT': 0,
          'NAME': 'Req1',
          CRITICALITY: 'UNDEFINED',
          REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
          HAS_DESCRIPTION: false,
          REQ_CATEGORY_ICON: 'indeterminate_checkbox_empty',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_UNDEFINED',
          REQ_CATEGORY_TYPE: 'SYS',
          COVERAGE_COUNT: 0,
          IS_SYNCHRONIZED: true,
          REMOTE_REQ_PERIMETER_STATUS: 'IN_CURRENT_PERIMETER',
          REMOTE_SYNCHRONISATION_ID: 1
        }
      } as unknown as DataRow,
      {
        id: 'Requirement-2',
        children: [],
        parentRowId: 'RequirementFolder-102',
        state: DataRowOpenState.leaf,
        data: {
          'RLN_ID': 2,
          'CHILD_COUNT': 0,
          'NAME': 'Req2',
          CRITICALITY: 'UNDEFINED',
          REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
          HAS_DESCRIPTION: false,
          REQ_CATEGORY_ICON: 'indeterminate_checkbox_empty',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_UNDEFINED',
          REQ_CATEGORY_TYPE: 'SYS',
          COVERAGE_COUNT: 0,
          IS_SYNCHRONIZED: true,
          REMOTE_REQ_PERIMETER_STATUS: 'OUT_OF_CURRENT_PERIMETER',
          REMOTE_SYNCHRONISATION_ID: 2
        }
      } as unknown as DataRow,
      {
        id: 'Requirement-3',
        children: [],
        parentRowId: 'RequirementFolder-102',
        state: DataRowOpenState.leaf,
        data: {
          'RLN_ID': 3,
          'CHILD_COUNT': 0,
          'NAME': 'Req3',
          CRITICALITY: 'UNDEFINED',
          REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
          HAS_DESCRIPTION: false,
          REQ_CATEGORY_ICON: 'indeterminate_checkbox_empty',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_UNDEFINED',
          REQ_CATEGORY_TYPE: 'SYS',
          COVERAGE_COUNT: 0,
          IS_SYNCHRONIZED: true,
          REMOTE_REQ_PERIMETER_STATUS: 'NOT_FOUND',
          REMOTE_SYNCHRONISATION_ID: 2
        }
      } as unknown as DataRow,
      {
        id: 'Requirement-4',
        children: [],
        parentRowId: 'RequirementFolder-103',
        state: DataRowOpenState.leaf,
        data: {
          'RLN_ID': 4,
          'CHILD_COUNT': 0,
          'NAME': 'Req4',
          CRITICALITY: 'UNDEFINED',
          REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
          HAS_DESCRIPTION: false,
          REQ_CATEGORY_ICON: 'indeterminate_checkbox_empty',
          REQ_CATEGORY_LABEL: 'requirement.category.CAT_UNDEFINED',
          REQ_CATEGORY_TYPE: 'SYS',
          COVERAGE_COUNT: 0,
          IS_SYNCHRONIZED: true,
          REMOTE_REQ_PERIMETER_STATUS: 'UNKNOWN',
          REMOTE_SYNCHRONISATION_ID: 3
        }
      } as unknown as DataRow,
    ]
  };

  it('should display Requirement folder\'s synchronization status icon with correct color', () => {
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes);
    const tree = requirementWorkspacePage.tree;
    tree.assertSynchronizationStatusIconHasCorrectCssClass('RequirementFolder-101', 'remote-sync-success');
    tree.assertSynchronizationStatusIconHasCorrectCssClass('RequirementFolder-102', 'remote-sync-partial');
    tree.assertSynchronizationStatusIconHasCorrectCssClass('RequirementFolder-103', 'remote-sync-failed');
  });

  it('should display remote requirement perimeter status icon with correct color', () => {
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes);
    const tree = requirementWorkspacePage.tree;
    tree.assertRemoteReqPerimeterStatusIconHasCorrectCssClass('Requirement-1', 'remote-req-in-perimeter');
    tree.assertRemoteReqPerimeterStatusIconHasCorrectCssClass('Requirement-2', 'remote-req-out-of-perimeter');
    tree.assertRemoteReqPerimeterStatusIconHasCorrectCssClass('Requirement-3', 'remote-req-deleted');
  });
});
