import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {RequirementWorkspacePage} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';

describe('Requirement Workspace Tree Delete', function () {

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'RequirementLibrary-1',
      projectId: 1,
      children: [],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'}
    } as unknown as DataRow,
      {
        id: 'RequirementLibrary-2',
        projectId: 2,
        children: [],
        data: {'NAME': 'Project2', 'CHILD_COUNT': '1'}
      } as unknown as DataRow]
  };

  const libraryRefreshAtOpen = [
    {
      id: 'RequirementLibrary-1',
      projectId: 1,
      children: ['RequirementFolder-1', 'Requirement-3', 'RequirementFolder-2'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'},
      state: DataRowOpenState.open
    } as unknown as DataRow,
    {
      id: 'RequirementFolder-1',
      children: [],
      projectId: 1,
      parentRowId: 'RequirementLibrary-1',
      data: {'NAME': 'folder1'}
    } as unknown as DataRow,
    {
      id: 'Requirement-3',
      children: [],
      projectId: 1,
      parentRowId: 'RequirementLibrary-1',
      data: {
        'NAME': 'a nice requirement',
        'HAS_DESCRIPTION': true,
        'REQUIREMENT_STATUS': 'WORK_IN_PROGRESS',
        'CRITICALITY': 'MAJOR'
      }
    } as unknown as DataRow,
    {
      id: 'RequirementFolder-2',
      children: [],
      projectId: 1,
      parentRowId: 'RequirementLibrary-1',
      data: {'NAME': 'folder2'}
    } as unknown as DataRow
  ];

  it('should activate or deactivate delete button according to user selection', () => {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes);
    const tree = requirementWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    requirementWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    // the component should never render with empty model, but we don't care, we are just testing the menu
    tree.selectNode('RequirementFolder-1', {});
    requirementWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    tree.selectNode('Requirement-3', {});
    requirementWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    tree.selectNode('RequirementLibrary-1', {});
    requirementWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
  });

  it('should show server warnings when deleting', () => {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes);
    const tree = requirementWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    requirementWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    // the component should never render with empty model, but we don't care, we are just testing the menu
    tree.selectNode('RequirementFolder-1', {});
    requirementWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    const confirmDialog = requirementWorkspacePage.treeMenu.initDeletion('requirement-tree', [1], ['warning_milestone']);
    confirmDialog.checkWarningMessages(['warning_milestone']);
  });

  it('should delete node and remove it from tree', () => {
    const libRefreshed = {
      id: 'RequirementLibrary-1',
      projectId: 1,
      children: ['RequirementFolder-1', 'Requirement-3', 'RequirementFolder-2'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'},
      state: DataRowOpenState.open
    } as unknown as DataRow;
    const reqFolder1 = {
      id: 'RequirementFolder-1',
      children: [],
      data: {'NAME': 'folder1'},
      projectId: 1,
      parentRowId: 'RequirementLibrary-1',
    } as unknown as DataRow;
    const req = {
      id: 'Requirement-3',
      children: [],
      projectId: 1,
      parentRowId: 'RequirementLibrary-1',
      data: {
        'NAME': 'a nice requirement',
        'HAS_DESCRIPTION': true,
        'REQUIREMENT_STATUS': 'WORK_IN_PROGRESS',
        'CRITICALITY': 'MAJOR'
      }
    } as unknown as DataRow;
    const reqFolder2 = {
      id: 'RequirementFolder-2',
      children: [],
      data: {'NAME': 'folder2'},
      projectId: 1,
      parentRowId: 'RequirementLibrary-1',
    } as unknown as DataRow;
    const childNodes = [
      libRefreshed,
      reqFolder1,
      req,
      reqFolder2
    ];
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes);
    const tree = requirementWorkspacePage.tree;
    tree.openNode(firstNode.id, childNodes);
    requirementWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    // the component should never render with empty model, but we don't care, we are just testing the menu
    tree.selectNode('RequirementFolder-1', {});
    requirementWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    const confirmDialog = requirementWorkspacePage.treeMenu.initDeletion('requirement-tree', [1]);
    const libRefreshedAfterDelete = {
      id: 'RequirementLibrary-1',
      projectId: 1,
      children: ['Requirement-3', 'RequirementFolder-2'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '2'},
      state: DataRowOpenState.open
    } as unknown as DataRow;
    const refreshedContent = [
      libRefreshedAfterDelete, req, reqFolder2
    ];
    const selectedParentMock = new HttpMockBuilder('/requirement-library-view/1').responseBody({}).build();
    confirmDialog.deleteNodes([1], ['RequirementLibrary-1'], refreshedContent);
    selectedParentMock.wait();
    requirementWorkspacePage.tree.assertNodeNotExist('RequirementFolder-1');
    requirementWorkspacePage.tree.assertNodeExist('RequirementFolder-2');
    requirementWorkspacePage.tree.assertNodeExist('Requirement-3');
  });

});
