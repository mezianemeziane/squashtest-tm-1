/// <reference types="cypress" />

import {LoginPage} from '../../../page-objects/pages/login/login-page';
import {ReferentialData} from '../../../model/referential-data.model';
import {HomeWorkspacePage} from '../../../page-objects/pages/home-workspace/home-workspace.page';
import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';

describe('Authentication', function () {
  it('should login authorized user', () => {
    const loginPage = LoginPage.navigateTo();
    loginPage.assertExist();
    const homeWorkspacePage = loginPage.login('admin', 'admin');
    homeWorkspacePage.assertExist();
  });

  it('should show error message if login fail', () => {
    const loginPage = LoginPage.navigateTo();
    loginPage.assertExist();
    loginPage.loginFail('noop', 'noop');
    loginPage.assertLoginFailedWarningIsVisible();
  });

  it('should show login message if exist', () => {
    const loginMessage = '<p>Welcome to Squash TM 2.0</p>';
    const loginPage = LoginPage.navigateTo({
      loginMessage: loginMessage,
      isH2: false,
      squashVersion: '2.0.0.RELEASE'
    });
    loginPage.assertExist();
    loginPage.assertLoginMessageContains(loginMessage);
  });

  it('should hide login message block if no login message exist', () => {
    const loginPage = LoginPage.navigateTo({
      loginMessage: '',
      isH2: false,
      squashVersion: '2.0.0.RELEASE'
    });
    loginPage.assertExist();
    loginPage.assertLoginMessageIsNotVisible();
  });

  it('should show h2 warning if required', () => {
    const loginPage = LoginPage.navigateTo({
      loginMessage: '',
      isH2: true,
      squashVersion: '2.0.0.RELEASE'
    });
    loginPage.assertExist();
    loginPage.assertH2warningIsVisible();
  });

  it('should logout', () => {
    HomeWorkspacePage.initTestAtPage();
    const mock = new HttpMockBuilder('logout').build();
    NavBarElement.logout();
    mock.wait();
  });

});

