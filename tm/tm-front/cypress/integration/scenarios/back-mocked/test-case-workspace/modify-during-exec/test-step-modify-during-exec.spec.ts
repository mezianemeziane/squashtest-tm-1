import {TestStepModifyDuringExecPage} from '../../../../page-objects/pages/test-case-workspace/detailed-step-view/test-step-modify-during-exec.page';
import {
  ActionStepExecViewModel,
  ModifDuringExecModel
} from '../../../../model/modif-during-exec/modif-during-exec.model';

describe('Test Case Modify during exec', function () {
  it('should display modify step', () => {
    const modifDuringExecModel: Partial<ModifDuringExecModel> = {
      executionStepActionTestStepPairs: [{
        executionStepId: 1,
        testStepId: 1
      }]
    };
    const actionStepExecViewModel: Partial<ActionStepExecViewModel> = {};
    const page = TestStepModifyDuringExecPage.initTestAtPage(modifDuringExecModel, actionStepExecViewModel);
    page.assertExist();
    page.assertTestCaseNameContains('ref - tc-1');
    page.assertCalledTestCaseNameNotVisible();
  });

  it('should display modify step for called test case step', () => {
    const modifDuringExecModel: Partial<ModifDuringExecModel> = {
      executionStepActionTestStepPairs: [{
        executionStepId: 1,
        testStepId: 1
      }]
    };
    const actionStepExecViewModel: Partial<ActionStepExecViewModel> = {
      actionStepTestCaseId: 2,
      actionStepTestCaseName: 'called name',
      actionStepTestCaseReference: 'CREF',
    };
    const page = TestStepModifyDuringExecPage.initTestAtPage(modifDuringExecModel, actionStepExecViewModel);
    page.assertExist();
    page.assertTestCaseNameContains('ref - tc-1');
    page.assertCalledTestCaseNameContains('Appel de CREF - called name');
  });
});
