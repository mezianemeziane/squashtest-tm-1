import {TestCaseViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {createEntityReferentialData} from '../../../../utils/referential/create-entity-referential.const';
import {TestCaseModel} from '../../../../model/test-case/test-case.model';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {TestCaseViewExecutionsPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-executions.page';
import {mockTestCaseModel} from '../../../../data-mock/test-case.data-mock';

const initialTestCaseRow: DataRow = {
  id: 'TestCase-3',
  children: [],
  projectId: 1,
  parentRowId: 'TestCaseLibrary-1',
  data: {
    'NAME': 'TestCase3', 'CHILD_COUNT': 0, 'TC_STATUS': 'APPROVED', 'TC_KIND': 'STANDARD', 'IMPORTANCE': 'LOW'
  }
} as unknown as DataRow;
describe('Test Case View - Executions', function () {
  it('should display test case execution', () => {

    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();

    const executions: GridResponse = {
      dataRows: [{
        id: 1,
        data: {
          id: 1,
          campaignName: 'Campaign',
          datasetsName: '',
          executionMode: 'MANUAL',
          executionOrder: 0,
          executionStatus: 'SUCCESS',
          iterationName: 'Itération',
          lastExecutedBy: 'admin',
          lastExecutedOn: '2020-06-20',
          nbIssues: 0,
          projectName: 'Project',
          testSuiteName: ''
        }
      } as unknown as DataRow]
    };

    const executionPage = page.clickAnchorLink('executions', executions) as TestCaseViewExecutionsPage;
    const grid = executionPage.grid;
    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row.cell('projectName').textRenderer().assertContainText('Project');
    row.cell('iterationName').textRenderer().assertContainText('Itération');

  });

  function navigateToTestCase(): TestCaseViewPage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'TestCaseLibrary-1',
        children: [],
        data: {'NAME': 'Project1'}
      } as unknown as DataRow]
    };
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes, createEntityReferentialData);
    const libraryChildren = [
      {
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3'],
        data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      initialTestCaseRow];

    testCaseWorkspacePage.tree.openNode('TestCaseLibrary-1', libraryChildren);
    const model: TestCaseModel = mockTestCaseModel({
      id: 3,
      projectId: 1,
      name: 'TestCase3',
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: []
      },
      reference: '',
      description: '',
      uuid: '',
      type: 20,
      testSteps: [],
      status: 'WORK_IN_PROGRESS',
      prerequisite: '',
      parameters: [],
      nbIssues: 0,
      nature: 12,
      milestones: [],
      lastModifiedOn: new Date('2020-03-09 10:30'),
      lastModifiedBy: 'admin',
      kind: 'STANDARD',
      importanceAuto: false,
      importance: 'LOW',
      executions: [],
      datasets: [],
      datasetParamValues: [],
      createdOn: new Date('2020-03-09 10:30'),
      createdBy: 'admin',
      coverages: [],
      automationRequest: null,
      automatable: 'M',
      calledTestCases: [],
      lastExecutionStatus: 'SUCCESS',
      script: ''
    });
    return testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
  }
});
