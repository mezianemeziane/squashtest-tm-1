import {TestCaseViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {TestCaseModel} from '../../../../model/test-case/test-case.model';
import {AutomationRequest} from '../../../../model/test-case/automation-request-model';
import {ReferentialData} from '../../../../model/referential-data.model';
import {TestCaseAutomatableKeys} from '../../../../model/level-enums/level-enum';
import {TestCaseViewAutomationPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-automation.page';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder
} from '../../../../utils/referential/referential-data-builder';
import {
  TestAutomationServer,
  TestAutomationServerKind
} from '../../../../model/test-automation/test-automation-server.model';

describe('Test Case View - Automation', () => {
  it('should update when changing automatable status', () => {
    const testCaseModel = getTestCaseModel(false, 'M');
    const referentialData = getReferentialData(false);
    const automationPanel = navigateToAutomationPanel(referentialData, testCaseModel);

    // Initially, only the 'automatable' field is visible
    automationPanel.priority.assertNotExist();
    automationPanel.uuid.assertNotExist();
    automationPanel.transmittedOn.assertNotExist();

    const response: AutomationRequest = {
      id: 107,
      priority: null,
      requestStatus: 'WORK_IN_PROGRESS',
      transmittedOn: null,
      extender: null
    };

    // When switching to 'Y', fields are visible
    automationPanel.setAutomatable('Y', response);
    automationPanel.priority.checkPlaceholder();
    automationPanel.uuid.checkContent(testCaseModel.uuid);
    automationPanel.transmittedOn.assertNotExist();

    // When switching to 'N', only the 'automatable' field is visible
    automationPanel.setAutomatable('N', response);
    automationPanel.priority.assertNotExist();
    automationPanel.uuid.assertNotExist();
    automationPanel.transmittedOn.assertNotExist();
  });

  it('should show remote info if project allows it', () => {
    const testCaseModel = getTestCaseModel(false, 'Y');
    const referentialData = getReferentialData(true);
    const automationPanel = navigateToAutomationPanel(referentialData, testCaseModel);
    const automationRequest = testCaseModel.automationRequest;

    // then
    automationPanel.priority.checkContent('2');
    automationPanel.uuid.checkContent('8c83e7cd-6073-4624-8787-93482055f904');
    automationPanel.transmittedOn.checkContent('23/03/2020 09:37');
    automationPanel.remoteStatus.checkContent(automationRequest.extender.remoteStatus);
    automationPanel.remoteUrl.checkContent(automationRequest.extender.remoteIssueKey);
    automationPanel.remoteAssignedTo.checkContent(automationRequest.extender.remoteAssignedTo);
    automationPanel.automated.checkContent('non');
  });

  it('should update when transmitted', () => {
    const testCaseModel = getTestCaseModel(true, 'M');
    const referentialData = getReferentialData(true);
    const automationPanel = navigateToAutomationPanel(referentialData, testCaseModel);

    const automationRequest: AutomationRequest = {
      id: 107,
      priority: null,
      requestStatus: 'WORK_IN_PROGRESS',
      transmittedOn: null,
      extender: null
    };

    automationPanel.setAutomatable('Y', automationRequest);

    const updatedModel: TestCaseModel = {
      ...testCaseModel,
      automatable: 'Y',
      automationRequest: {
        id: 106,
        priority: 120,
        requestStatus: 'TRANSMITTED',
        transmittedOn: new Date('2020-03-23T08:37:33.000+0000'),
        extender: {
          id: 18,
          serverId: 33,
          remoteStatus: 'À faire',
          remoteIssueKey: 'TEST-59',
          remoteRequestUrl: 'http://plugin01.atlassian.net/browse/TEST-59',
          remoteAssignedTo: 'Jean Gabin',
          // tslint:disable-next-line:max-line-length
          sentValueForSync: '{"referenceTestCase":"","nameTestCase":"TC transmission 6","urlTestCase":"http://localhost:8081/squash/test-cases/382613/info","pathSharingServerTestCase":"","scriptTaTestCase":"N/A","prioriteTestCase":"null"}',
          synchronizableIssueStatus: 'TO_SYNCHRONIZE',
          lastSyncDateSquash: null
        },
      },
    };

    automationPanel.transmit([updatedModel]);

    // then
    automationPanel.priority.checkContent('120');
    automationPanel.transmittedOn.checkContent('23/03/2020 09:37');
    automationPanel.remoteUrl.checkContent(updatedModel.automationRequest.extender.remoteIssueKey);
    automationPanel.remoteAssignedTo.checkContent(updatedModel.automationRequest.extender.remoteAssignedTo);
    automationPanel.remoteStatus.checkContent(updatedModel.automationRequest.extender.remoteStatus);
  });

  it('should display message if there is synchronization error', () => {
    const referentialData = getReferentialData(true);
    const baseTestCaseModel = getTestCaseModel(false, 'Y');
    const deletedTestCaseModel = {
      ...baseTestCaseModel,
      automationRequest: {
        ...baseTestCaseModel.automationRequest,
        extender : {
          ...baseTestCaseModel.automationRequest.extender,
          synchronizableIssueStatus: 'DELETED',
        },
      },
    };
    const nonCompliantConfigurationTestCaseModel = {
      ...baseTestCaseModel,
      automationRequest: {
        ...baseTestCaseModel.automationRequest,
        extender : {
          ...baseTestCaseModel.automationRequest.extender,
          synchronizableIssueStatus: 'NON_COMPLIANT',
        },
      },
    };

    const nonCompliantAutomationPanel = navigateToAutomationPanel(referentialData, nonCompliantConfigurationTestCaseModel);
    nonCompliantAutomationPanel.syncErrorMessage.checkContent('Le ticket n\'est pas conforme à la configuration');

    const deletedAutomationPanel = navigateToAutomationPanel(referentialData, deletedTestCaseModel);
    deletedAutomationPanel.syncErrorMessage.checkContent('Le ticket a été supprimé');
  });
});

function navigateToAutomationPanel(referentialData: ReferentialData, testCaseModel: TestCaseModel): TestCaseViewAutomationPage {
  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'TestCaseLibrary-1',
      children: [],
      data: {'NAME': 'Project1'}
    } as unknown as DataRow]
  };

  const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes, referentialData);
  const libraryChildren = [
    {
      id: 'TestCaseLibrary-1',
      children: ['TestCase-3'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
      state: DataRowOpenState.open
    } as unknown as DataRow,
    {
      id: 'TestCase-3',
      children: [],
      projectId: 1,
      data: {
        'NAME': 'TestCase3',
        'CHILD_COUNT': 0,
        'TC_STATUS': 'APPROVED',
        'TC_KIND': 'STANDARD',
        'IMPORTANCE': 'LOW'
      },
      parentRowId: 'TestCaseLibrary-1',
    } as unknown as DataRow];

  testCaseWorkspacePage.tree.openNode('TestCaseLibrary-1', libraryChildren);

  const page = testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', testCaseModel);
  return page.clickAnchorLink('automation') as TestCaseViewAutomationPage;
}

function getReferentialData(setRemoteWorkflow: boolean): ReferentialData {
  const refData = {...baseReferentialData};
  refData.automationServers = [{
    id: 1,
    kind: TestAutomationServerKind.jenkins
  } as unknown as TestAutomationServer];
  refData.projects[0].automationWorkflowType = setRemoteWorkflow ? 'REMOTE_WORKFLOW' : 'NATIVE';
  refData.projects[0].taServerId = setRemoteWorkflow ? 1 : null;
  return refData;
}

function getTestCaseModel(scripted: boolean, automatable: TestCaseAutomatableKeys): TestCaseModel {
  const automationRequest: AutomationRequest = automatable === 'Y' ?
    {
      id: 97,
      priority: 2,
      requestStatus: 'READY',
      transmittedOn: new Date('2020-03-23T08:37:33.000+0000'),
      extender: {
        id: 10,
        serverId: 33,
        remoteStatus: 'À faire',
        remoteIssueKey: 'TEST-51',
        remoteRequestUrl: 'http://plugin01.atlassian.net/browse/TEST-51',
        remoteAssignedTo: 'N/A',
        // tslint:disable-next-line:max-line-length
        sentValueForSync: '{"referenceTestCase":"1.01","nameTestCase":"Nouveau cas de test","urlTestCase":"http://localhost:8081/squash/test-cases/382580/info","pathSharingServerTestCase":"","scriptTaTestCase":"N/A","prioriteTestCase":"null"}',
        synchronizableIssueStatus: 'TO_SYNCHRONIZE',
        lastSyncDateSquash: null
      }
    } : null;

  return {
    id: 3,
    projectId: 1,
    name: 'TestCase3',
    customFieldValues: [],
    attachmentList: {
      id: 1,
      attachments: []
    },
    reference: '',
    description: '',
    uuid: '8c83e7cd-6073-4624-8787-93482055f904',
    type: 20,
    testSteps: [],
    status: 'WORK_IN_PROGRESS',
    prerequisite: '',
    parameters: [],
    nbIssues: 0,
    nature: 12,
    milestones: [],
    lastModifiedOn: new Date('2020-03-09 10:30'),
    lastModifiedBy: 'admin',
    kind: scripted ? 'GHERKIN' : 'STANDARD',
    importanceAuto: true,
    importance: 'LOW',
    executions: [],
    datasets: [],
    datasetParamValues: [],
    createdOn: new Date('2020-03-09 10:30'),
    createdBy: 'admin',
    coverages: [],
    calledTestCases: [],
    automationRequest,
    automatable,
    lastExecutionStatus: 'SUCCESS',
    script: '',
    actionWordLibraryActive: null
  };
}

const baseReferentialData: ReferentialData = new ReferentialDataMockBuilder()
  .withProjects({
    allowAutomationWorkflow: true,
    taServerId: 1,
    testCaseNatureId: 2,
    testCaseTypeId: 3,
    permissions: ALL_PROJECT_PERMISSIONS,
  })
  .build();
