import {TestCaseViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {TestCaseModel} from '../../../../model/test-case/test-case.model';
import {createEntityReferentialData} from '../../../../utils/referential/create-entity-referential.const';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {mockTestCaseModel} from '../../../../data-mock/test-case.data-mock';

describe('Test Case View - Called test cases', function () {
  it('should display table', () => {
    const page = navigateToTestCase();
    const calledTCTable = page.calledTestCaseTable;
    new NavBarElement().toggle();
    page.toggleTree();

    const firstRow = calledTCTable.getRow(6);
    firstRow.cell('projectName').textRenderer().assertContainText('Project 1');
    firstRow.cell('reference').textRenderer().assertContainText('Ref.006');
    firstRow.cell('name').linkRenderer().assertContainText('Test Case 6');
    firstRow.cell('datasetName').textRenderer().assertContainText('Dataset 1');
    firstRow.cell('stepOrder').textRenderer().assertContainText('1');


    const secondRow = calledTCTable.getRow(7);
    secondRow.cell('projectName').textRenderer().assertContainText('Custom Project');
    secondRow.cell('reference').textRenderer().assertContainText('Ref.007');
    secondRow.cell('name').linkRenderer().assertContainText('Test Case 7');
    secondRow.cell('datasetName').textRenderer().assertContainText('');
    secondRow.cell('stepOrder').textRenderer().assertContainText('2');

  });

  function navigateToTestCase(): TestCaseViewPage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'TestCaseLibrary-1',
        children: [],
        data: {'NAME': 'Project1'}
      } as unknown as DataRow]
    };
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes, createEntityReferentialData);
    const libraryChildren = [
      {
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3'],
        data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'TestCase-3',
        children: [],
        projectId: 1,
        data: {
          'NAME': 'TestCase3',
          'CHILD_COUNT': 0,
          'TC_STATUS': 'APPROVED',
          'TC_KIND': 'STANDARD',
          'IMPORTANCE': 'LOW'
        },
        parentRowId: 'TestCaseLibrary-1',
      } as unknown as DataRow];

    testCaseWorkspacePage.tree.openNode('TestCaseLibrary-1', libraryChildren);
    const model: TestCaseModel = mockTestCaseModel({
      id: 3,
      projectId: 1,
      name: 'TestCase3',
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: []
      },
      reference: '',
      description: '',
      uuid: '',
      type: 20,
      testSteps: [],
      status: 'WORK_IN_PROGRESS',
      prerequisite: '',
      parameters: [],
      nbIssues: 0,
      nature: 12,
      milestones: [],
      lastModifiedOn: new Date('2020-03-09 10:30'),
      lastModifiedBy: 'admin',
      kind: 'STANDARD',
      importanceAuto: false,
      importance: 'LOW',
      executions: [],
      datasets: [],
      datasetParamValues: [],
      createdOn: new Date('2020-03-09 10:30'),
      createdBy: 'admin',
      coverages: [],
      automationRequest: null,
      automatable: 'M',
      calledTestCases: [{
        id: 6,
        projectName: 'Project 1',
        datasetName: 'Dataset 1',
        name: 'Test Case 6',
        reference: 'Ref.006',
        stepOrder: 0
      },
        {
          id: 7,
          projectName: 'Custom Project',
          datasetName: '',
          name: 'Test Case 7',
          reference: 'Ref.007',
          stepOrder: 1
        }],
      lastExecutionStatus: 'SUCCESS',
      script: '',
    });
    return testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
  }
});
