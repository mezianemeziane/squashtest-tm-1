import {EditableDateFieldElement} from '../../../../page-objects/elements/forms/editable-date-field.element';
import {RemoveMilestoneDialogElement} from '../../../../page-objects/pages/administration-workspace/dialogs/remove-milestone-dialog.element';
import {AdminWorkspaceMilestonesPage} from '../../../../page-objects/pages/administration-workspace/admin-workspace-milestones.page';
import {assertAccessDenied} from '../../../../utils/assert-access-denied.utils';
import {MilestoneAdminView} from '../../../../model/milestone/milestone.model';
import {selectByDataTestToolbarButtonId} from '../../../../utils/basic-selectors';
import {ConfirmChangeMilestoneOwnerDialog} from '../../../../page-objects/pages/administration-workspace/milestone-view/dialogs/confirm-change-milestone-owner.dialog';
import {MilestoneViewPage} from '../../../../page-objects/pages/administration-workspace/milestone-view/milestone-view.page';
import {ReferentialDataMockBuilder} from '../../../../utils/referential/referential-data-builder';
import {User} from '../../../../model/user/user.model';
import {NavBarAdminElement} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import {BindableProject} from '../../../../page-objects/pages/administration-workspace/milestone-view/dialogs/bind-projects-to-milestone.dialog';
import {makeMilestoneViewData, makeProjectViewData} from '../../../../data-mock/administration-views.data-mock';
import {ProjectViewDetailPage} from '../../../../page-objects/pages/administration-workspace/project-view/project-view-detail.page';
import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {mockDataRow, mockGridResponse} from '../../../../data-mock/grid.data-mock';

describe('Administration workspace milestones', function () {
  const todayDate = new Date();

  const initialMilestones: GridResponse = {
    count: 2,
    dataRows: [
      {
        id: '1',
        children: [],
        data: {
          milestoneId: 1,
          label: 'Milestone1',
          status: 'IN_PROGRESS',
          endDate: todayDate,
          projectCount: '2',
          description: 'One description',
          range: 'GLOBAL',
          ownerFistName: 'oneOwner',
          ownerLastName: 'oneOwner',
          ownerLogin: 'oneOwner',
          userSortColumn: 'oneOwner',
          createdOn: todayDate,
          createdBy: 'oneUser',
        }
      } as unknown as DataRow,
      {
        id: '2',
        children: [],
        data: {
          milestoneId: 2,
          label: 'Milestone2',
          status: 'FINISHED',
          endDate: todayDate,
          projectCount: '3',
          description: 'Another description',
          range: 'GLOBAL',
          ownerFistName: 'anotherOwner',
          ownerLastName: 'anotherOwner',
          ownerLogin: 'anotherOwner',
          userSortColumn: 'anotherOwner',
          createdOn: todayDate,
          createdBy: 'anotherUser',
        }
      } as unknown as DataRow,
    ]
  };

  const addMilestoneResponse: GridResponse = {
    count: 3,
    dataRows: [
      ...initialMilestones.dataRows,
      {
        id: '3',
        children: [],
        data: {
          milestoneId: 3,
          label: 'Milestone3',
          status: 'FINISHED',
          endDate: todayDate,
          projectCount: '2',
          description: '',
          range: 'GLOBAL',
          ownerFistName: 'anotherOwner',
          ownerLastName: 'anotherOwner',
          ownerLogin: 'anotherOwner',
          userSortColumn: 'anotherOwner',
          createdOn: todayDate,
          createdBy: 'anotherUser',
        }
      } as unknown as DataRow,
    ]
  };

  it('should forbid access to non-admin users', () => {
    assertAccessDenied('administration-workspace/projects');
  });

  it('should display milestones grid', () => {
    const page = AdminWorkspaceMilestonesPage.initTestAtPageMilestones(initialMilestones);
    const grid = page.grid;

    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row.cell('label').textRenderer().assertContainText('Milestone1');
    row.cell('status').textRenderer().assertContainText('En cours');
    row.cell('endDate').textRenderer().assertContainText(EditableDateFieldElement.dateToDisplayString(todayDate));
    row.cell('projectCount').textRenderer().assertContainText('2');
    grid.scrollPosition('right', 'mainViewport');
    row.cell('description').textRenderer().assertContainText('One description');
    row.cell('range').textRenderer().assertContainText('Globale');
    row.cell('userSortColumn').textRenderer().assertContainText('Administrateur');
    row.cell('createdOn').textRenderer().assertContainText(EditableDateFieldElement.dateToDisplayString(todayDate));
    row.cell('createdBy').textRenderer().assertContainText('oneUser');
  });

  it('should add milestone', () => {
    const page = AdminWorkspaceMilestonesPage.initTestAtPageMilestones(initialMilestones);
    const dialog = page.openCreateMilestone();
    dialog.assertExist();
    dialog.fillName('milestone3');
    dialog.setStatus('Planifié');
    dialog.fillDescription('Description 3');
    dialog.setDeadlineDateToToday();
    dialog.addMilestone(3, addMilestoneResponse, false);
    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(2);
    grid.assertRowExist(3);
  });

  it('should add another milestone', () => {
    const page = AdminWorkspaceMilestonesPage.initTestAtPageMilestones(initialMilestones);
    const dialog = page.openCreateMilestone();
    dialog.assertExist();
    dialog.fillName('milestone3');
    dialog.setStatus('Planifié');
    dialog.fillDescription('Description 3');
    dialog.setDeadlineDateToToday();
    dialog.addMilestone(3, addMilestoneResponse, true);

    dialog.assertExist();
    dialog.checkIfFormIsEmpty();

    dialog.cancel();
    dialog.assertNotExist();

    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(2);
    grid.assertRowExist(3);
  });

  it('should allow milestones removal', () => {
    const page = AdminWorkspaceMilestonesPage.initTestAtPageMilestones(initialMilestones);
    const grid = page.grid;

    // Show dialog
    grid.scrollPosition('right', 'mainViewport');
    grid.getRow(2).cell('delete').iconRenderer().click();

    // Confirm
    const dialog = new RemoveMilestoneDialogElement([2]);
    dialog.deleteForSuccess({
      count: 1,
      dataRows: [initialMilestones.dataRows[0]]
    });
  });

  it('should allow multiple milestones removal', () => {
    const page = AdminWorkspaceMilestonesPage.initTestAtPageMilestones(initialMilestones);
    page.grid.selectRows( [1, 2], '#', 'leftViewport');

    // Show dialog
    cy.get(selectByDataTestToolbarButtonId('delete-button')).click();

    // Confirm
    const dialog = new RemoveMilestoneDialogElement([1, 2]);
    dialog.deleteForSuccess({
      count: 0,
      dataRows: []
    });
  });
});

describe('Administration workspace - Milestone view information panel', function () {
  it('should set milestone name', () => {
    const milestonePage = showMilestoneView();
    milestonePage.entityNameField.setAndConfirmValue('NEW');
    milestonePage.entityNameField.checkContent('NEW');
  });

  it('should set milestone end date', () => {
    const milestonePage = showMilestoneView();
    milestonePage.informationPanel.endDateField.setToTodayAndConfirm();
  });

  it('should set milestone status', () => {
    const milestonePage = showMilestoneView();
    milestonePage.informationPanel.statusField.setAndConfirmValueWithServerResponse('Terminé', {
      ...getInitialModel(),
      status: 'FINISHED',
    });
  });

  it('should set milestone range', () => {
    const milestonePage = showMilestoneView();

    milestonePage.informationPanel.rangeField.setAndConfirmValueWithServerResponse('Global', {
      ...getInitialModel(),
      range: 'GLOBAL',
    });

    milestonePage.informationPanel.rangeField.checkSelectedOption('Global');
    milestonePage.informationPanel.assertOwnerIsNotEditable();
  });

  it('should set milestone description', () => {
    const milestonePage = showMilestoneView();
    milestonePage.informationPanel.descriptionField.setAndConfirmValue('blabla');
  });

  it('should set milestone owner', () => {
    const milestonePage = showMilestoneView();
    milestonePage.informationPanel.setOwner('jane.doe (jane doe)', {
      ...getInitialModel(),
      ownerLogin: 'jane.doe',
      ownerFistName: 'jane',
      ownerLastName: 'doe',
    });
    milestonePage.informationPanel.checkOwner('jane.doe (jane doe)');
  });

  it('should forbid modification for non-owner project manager', () => {
    const milestonePage = showMilestoneViewForProjectManager('mlucien', false);
    milestonePage.informationPanel.endDateField.assertIsNotEditable();
    milestonePage.informationPanel.statusField.assertIsNotEditable();
    milestonePage.informationPanel.rangeField.assertIsNotEditable();
    milestonePage.informationPanel.descriptionField.assertIsNotEditable();
    milestonePage.informationPanel.assertOwnerIsNotEditable();
  });

  it('should forbid all modification for owner project manager except range', () => {
    const milestonePage = showMilestoneViewForProjectManager('admin', true);
    milestonePage.informationPanel.endDateField.assertIsEditable();
    milestonePage.informationPanel.statusField.assertIsEditable();
    milestonePage.informationPanel.descriptionField.assertIsEditable();
    milestonePage.informationPanel.assertOwnerIsEditable();

    milestonePage.informationPanel.rangeField.assertIsNotEditable();
  });

  it('should show a warning when project manager changes owner', () => {
    const milestonePage = showMilestoneViewForProjectManager('admin', true);
    milestonePage.informationPanel.setOwnerNoMock('jane.doe (jane doe)');

    const dialog = new ConfirmChangeMilestoneOwnerDialog();
    dialog.assertMessage();
    dialog.clickOnCancelButton();
  });
});

function showMilestoneViewForProjectManager(username: string, canEditMilestone: boolean): MilestoneViewPage {
  const initialMilestones = getInitialMilestones();
  const refData = new ReferentialDataMockBuilder().withUser({
    admin: false,
    projectManager: true,
    userId: 1,
    username,
    automationProgrammer: false,
    functionalTester: false
  }).build();

  const milestoneView = getInitialModel();
  milestoneView.canEdit = canEditMilestone;

  const page = AdminWorkspaceMilestonesPage.initTestAtPageMilestones(initialMilestones, refData);
  return page.selectMilestoneByLabel(initialMilestones.dataRows[0].data.label,
    milestoneView, getPossibleOwners());
}

describe('Administration Workspace - Milestones - Projects', function () {
  it('should bind projects to milestone', () => {
    const milestonePage = showMilestoneView();
    milestonePage.assertExist();
    milestonePage.projectsPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    milestonePage.foldGrid();

    const bindableProjects: BindableProject[] =
      [
        {
          id: -3,
          name: 'Omega_project',
          label: 'Omega-3',
          template: false
        },
        {
          id: -4,
          name: 'Gamma_project',
          label: 'Gamma-4',
          template: false
        }
      ];

    const bindProjectsToMilestoneDialog = milestonePage.projectsPanel.clickOnBindProjectsButton(bindableProjects);

    bindProjectsToMilestoneDialog.selectProjects('Omega_project', 'Gamma_project');
    bindProjectsToMilestoneDialog.confirm([]);
  });


  it('should unbind one project', () => {
    const milestonePage = showMilestoneView();
    milestonePage.assertExist();
    milestonePage.projectsPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    milestonePage.foldGrid();

    milestonePage.projectsPanel.unbindOne('Alpha Project');
  });

  it('should unbind multiple projects', () => {
    const milestonePage = showMilestoneView();
    milestonePage.assertExist();
    milestonePage.projectsPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    milestonePage.foldGrid();

    milestonePage.projectsPanel.unbindMultiple(['Alpha Project', 'Beta Project']);
  });

  it('should navigate to project detail page and back', () => {
    const initialMilestones = getInitialMilestones();
    const workspacePage = AdminWorkspaceMilestonesPage.initTestAtPageMilestones(initialMilestones);
    const milestonePage = workspacePage.selectMilestoneByLabel(initialMilestones.dataRows[0].data.label,
      getInitialModel(), getPossibleOwners());

    milestonePage.projectsPanel.showProjectDetail('Alpha Project', makeProjectViewData());
    const projectViewDetailPage = new ProjectViewDetailPage();
    projectViewDetailPage.assertExist();

    projectViewDetailPage.clickBackButton();
    workspacePage.assertExist();
  });
});


function showMilestoneView(): MilestoneViewPage {
  const initialMilestones = getInitialMilestones();
  const page = AdminWorkspaceMilestonesPage.initTestAtPageMilestones(initialMilestones);
  return page.selectMilestoneByLabel(initialMilestones.dataRows[0].data.label,
    getInitialModel(), getPossibleOwners());
}

function getPossibleOwners(): User[] {
  return [
    {id: 1, login: 'jdoe', firstName: 'j', lastName: 'doe'} as User,
    {id: 2, login: 'jane.doe', firstName: 'jane', lastName: 'doe'} as User,
    {id: 99, login: 'admin', firstName: 'admin', lastName: 'admin'} as User,
    {id: 97, login: 'oneOwner', firstName: 'oneOwner', lastName: 'oneOwner'} as User,
  ];
}


function getInitialModel(): MilestoneAdminView {
  return makeMilestoneViewData({
    id: 1,
    label: 'Milestone1',
    description: '',
    status: 'IN_PROGRESS',
    endDate: new Date(),
    range: 'RESTRICTED',
    ownerFistName: 'admin',
    ownerLastName: 'admin',
    ownerLogin: 'admin',
    createdOn: new Date(),
    createdBy: 'admin',
    lastModifiedOn: new Date(),
    lastModifiedBy: 'admin',
    canEdit: true,
    boundProjectsInformation: [
      {
        projectId: -1,
        projectName: 'Alpha Project',
        template: false,
        milestoneBoundToOneObjectOfProject: true,
        boundToMilestone: true
      },
      {
        projectId: -2,
        projectName: 'Beta Project',
        template: false,
        milestoneBoundToOneObjectOfProject: true,
        boundToMilestone: true
      }
    ],
  });
}

function getInitialMilestones(): GridResponse {
  const todayDate = new Date();

  return mockGridResponse(null,
    [
      mockDataRow({
        id: '1',
        data: {
          milestoneId: 1,
          label: 'Milestone1',
          status: 'IN_PROGRESS',
          endDate: todayDate,
          projectCount: '2',
          description: 'One description',
          range: 'GLOBAL',
          ownerFistName: 'admin',
          ownerLastName: 'admin',
          ownerLogin: 'admin',
          createdOn: todayDate,
          createdBy: 'admin',
        }
      }),
      mockDataRow({
        id: '2',
        data: {
          milestoneId: 2,
          label: 'Milestone2',
          status: 'FINISHED',
          endDate: todayDate,
          projectCount: '3',
          description: 'Another description',
          range: 'GLOBAL',
          ownerFistName: 'anotherOwner',
          ownerLastName: 'anotherOwner',
          ownerLogin: 'anotherOwner',
          createdOn: todayDate,
          createdBy: 'anotherUser',
        }
      }),
    ]);
}
