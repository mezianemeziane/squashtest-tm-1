import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {RemoveBugtrackerDialogElement} from '../../../../page-objects/pages/administration-workspace/dialogs/remove-bugtracker-dialog.element';
import {CannotRemoveBugtrackerAlert} from '../../../../page-objects/pages/administration-workspace/dialogs/cannot-remove-bugtracker-alert.element';
import {AdminWorkspaceBugtrackersPage} from '../../../../page-objects/pages/administration-workspace/admin-workspace-bugtrackers.page';
import {CannotRemoveBugtrackersAlert} from '../../../../page-objects/pages/administration-workspace/dialogs/cannot-remove-bugtrackers-alert.element';
import {assertAccessDenied} from '../../../../utils/assert-access-denied.utils';
import {selectByDataTestToolbarButtonId} from '../../../../utils/basic-selectors';
import {mockDataRow, mockGridResponse} from '../../../../data-mock/grid.data-mock';
import {mockFieldValidationError} from '../../../../data-mock/http-errors.data-mock';
import {BugTrackerViewPage} from '../../../../page-objects/pages/administration-workspace/bug-tracker-view/bug-tracker-view.page';
import {AdminBugTrackerState} from '../../../../model/bugtracker/bug-tracker-view.model';
import {AuthenticationPolicy, AuthenticationProtocol} from '../../../../model/bugtracker/bug-tracker.model';

describe('Administration Workspace - Bugtrackers', function () {
  it('should forbid access to non-admin users', () => {
    assertAccessDenied('administration-workspace/projects');
  });

  it('should display bugtrackers grid', () => {
    const initialNodes = mockBTGridResponse(getInitialDataRows());
    const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);
    const grid = page.grid;

    const firstRow = grid.getRow(1);
    firstRow.cell('name').textRenderer().assertContainText('BT01');
    firstRow.cell('kind').textRenderer().assertContainText('jira.rest');
    firstRow.cell('url').linkRenderer().assertContainText('http://localhost:1392');
    grid.getRow(2).assertExist();
    grid.getRow(3).assertExist();
  });

  it('should add bugtrackers', () => {
    const initialNodes = mockBTGridResponse(getInitialDataRows());
    const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);

    const addDialog = page.openCreateBugtracker();
    addDialog.assertExist();
    addDialog.fillName('BT05');
    addDialog.selectKind('Mantis');
    addDialog.fillUrl('http://localhost:9090');

    addDialog.addWithOptions({
      addAnother: false,
      createResponse: {id: 4},
      gridResponse: mockBTGridResponse([
        ...getInitialDataRows(),
        mockDataRow({data: {serverId: 4}}),
      ]),
    });

    page.grid.assertRowExist(4);

    // Add another
    const addAnotherDialog = page.openCreateBugtracker();
    addAnotherDialog.assertExist();
    addAnotherDialog.fillName('b');
    addAnotherDialog.selectKind('Mantis');
    addAnotherDialog.fillUrl('h');

    addAnotherDialog.addWithOptions({
      addAnother: true,
      createResponse: {id: 5},
      gridResponse: mockBTGridResponse([]), // already tested above
    });

    addAnotherDialog.assertExist();
    addAnotherDialog.checkIfFormIsEmpty();
    addAnotherDialog.cancel();
    addAnotherDialog.assertNotExist();
  });

  it('should validate creation form', () => {
    const initialNodes = mockBTGridResponse(getInitialDataRows());
    const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);

    const dialog = page.openCreateBugtracker();
    dialog.assertExist();

    // should forbid to add a bugtracker if name already exists
    const nameHttpError = mockFieldValidationError('name', 'sqtm-core.error.generic.name-already-in-use');
    dialog.fillName('b');
    dialog.selectKind('Mantis');
    dialog.fillUrl('h');
    dialog.addWithServerSideFailure(nameHttpError);
    dialog.assertExist();
    dialog.checkIfNameAlreadyInUseErrorMessageIsDisplayed();

    // should forbid to add a bugtracker with an empty name
    dialog.clearForm();
    dialog.selectKind('Mantis');
    dialog.fillUrl('u');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a bugtracker with an empty url
    dialog.clearForm();
    dialog.fillName('b');
    dialog.selectKind('Mantis');
    dialog.fillUrl('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a bugtracker with a malformed url
    const urlHttpError = mockFieldValidationError('url', 'sqtm-core.exception.wrong-url');
    dialog.clearForm();
    dialog.fillName('BT05');
    dialog.selectKind('Mantis');
    dialog.fillUrl('localhost');
    dialog.addWithServerSideFailure(urlHttpError);
    dialog.assertExist();
    dialog.checkIfMalformedUrlErrorMessageIsDisplayed();
  });

  it('should allow removal of bugtracker from the grid', () => {
    const initialNodes = mockBTGridResponse(getInitialDataRows());
    const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);

    const firstRow = page.grid.getRow(1);
    firstRow.cell('delete').assertExist();
    firstRow.cell('delete').iconRenderer().click();
    const dialog = new RemoveBugtrackerDialogElement([1]);
    dialog.assertMessageSingular();
    dialog.deleteForSuccess({
      count: 2,
      dataRows: [initialNodes.dataRows[1], initialNodes.dataRows[2]]
    });
  });

  it('should forbid removal of bugtracker used by synchronizations', () => {
    const initialNodes = mockBTGridResponse(getInitialDataRows());
    const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);

    const firstRow = page.grid.getRow(3);
    firstRow.cell('delete').assertExist();
    firstRow.cell('delete').iconRenderer().click();
    const alert = new CannotRemoveBugtrackerAlert();
    alert.assertMessage();
    alert.close();
  });

  it('should allow multiple bugtrackers removal', () => {
    const initialNodes = mockBTGridResponse(getInitialDataRows());
    const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);

    // Select elements to delete
    page.grid.selectRows([1, 2], '#', 'leftViewport');

    // Show dialog
    cy.get(selectByDataTestToolbarButtonId('delete-button')).click();

    // Confirm
    const dialog = new RemoveBugtrackerDialogElement([1, 2]);
    dialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[2]]
    });
  });

  it('should forbid multiple bugtracker removal used by synchronizations', () => {
    const initialNodes = mockBTGridResponse(getInitialDataRows());
    const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);

    // Select elements to delete
    page.grid.selectRows([1, 2, 3], '#', 'leftViewport');

    // Show dialog
    cy.get(selectByDataTestToolbarButtonId('delete-button')).click();

    const alert = new CannotRemoveBugtrackersAlert();
    alert.assertMessage();
    alert.close();
  });

  function getInitialDataRows(): DataRow[] {
    return [
      mockDataRow({
        data: {
          serverId: 1,
          name: 'BT01',
          kind: 'jira.rest',
          url: 'http://localhost:1392',
          synchronisationCount: 0,
        }
      }),
      mockDataRow({
        data: {
          serverId: 2,
          name: 'BT02',
          kind: 'jira.rest',
          url: 'http://localhost:1393',
          synchronisationCount: 0,
        }
      }),
      mockDataRow({
        data: {
          serverId: 3,
          name: 'BT03',
          kind: 'jira.cloud',
          url: 'http://plugin01.atlassian.com/',
          synchronisationCount: 3,
        }
      }),
    ];
  }

  function mockBTGridResponse(dataRows: DataRow[]): GridResponse {
    return mockGridResponse('serverId', dataRows);
  }
});

describe('Administration Workspace - Bug tracker - Information panel', function () {
  it('should set bugtracker name', () => {
    const page = showBugTrackerView();
    page.entityNameField.setAndConfirmValue('NEW');
    page.entityNameField.checkContent('NEW');
  });

  it('should set bugtracker kind', () => {
    const page = showBugTrackerView();
    page.informationPanel.kindField.setAndConfirmValue('jira.server');
    page.informationPanel.kindField.checkSelectedOption('jira.server');
  });

  it('should set bugtracker url', () => {
    const page = showBugTrackerView();
    page.informationPanel.urlField.setAndConfirmValue('http://blabla.blabla');
    page.informationPanel.urlField.checkContent('http://blabla.blabla');
  });
});

describe('Administration Workspace - Bug tracker - Auth policy panel', function () {
  it('should send credential form', () => {
    const page = showBugTrackerView();

    page.authPolicyPanel.assertSendButtonDisabled();
    page.authPolicyPanel.usernameField.fill('username');
    page.authPolicyPanel.passwordField.fill('password');
    page.authPolicyPanel.assertSendButtonEnabled();

    page.authPolicyPanel.sendCredentialsForm();
    page.authPolicyPanel.assertSaveSuccessMessageVisible();
  });

  it('should warn user about server errors', () => {
    const page = showBugTrackerView();

    page.authPolicyPanel.assertSendButtonDisabled();
    page.authPolicyPanel.usernameField.fill('username');
    page.authPolicyPanel.passwordField.fill('password');
    page.authPolicyPanel.assertSendButtonEnabled();

    const errorMessage = '3RR0R';
    page.authPolicyPanel.sendCredentialsFormWithServerError(errorMessage);
    page.authPolicyPanel.assertServerErrorMessageVisible(errorMessage);
  });
});

describe('Administration Workspace - Bug tracker - Auth protocol panel', function () {
  it('should set bugtracker auth protocol', () => {
    const page = showBugTrackerView();
    page.authProtocolPanel.protocolField.checkSelectedOption('basic authentication');
    page.authProtocolPanel.assertOAuthFormNotVisible();
    page.authProtocolPanel.protocolField.setAndConfirmValue('OAuth 1A');
    page.authProtocolPanel.assertOAuthFormVisible();
  });

  it('should send OAuth conf form', () => {
    const page = showBugTrackerView();
    page.authProtocolPanel.protocolField.setAndConfirmValue('OAuth 1A');

    page.authProtocolPanel.assertSaveButtonDisabled();
    page.authProtocolPanel.assertStatusMessageNotVisible();

    page.authProtocolPanel.consumerKeyField.fill('some-key');
    page.authProtocolPanel.secretField.fill('some-secret');
    page.authProtocolPanel.assertSaveButtonEnabled();
    page.authProtocolPanel.assertUnsavedChangesMessageVisible();

    page.authProtocolPanel.sendOAuthForm();
    page.authProtocolPanel.assertSaveSuccessMessageVisible();
  });

  it('should warn user about server errors', () => {
    const page = showBugTrackerView();
    page.authProtocolPanel.protocolField.setAndConfirmValue('OAuth 1A');
    page.authProtocolPanel.consumerKeyField.fill('some-key');
    page.authProtocolPanel.secretField.fill('some-secret');
    page.authProtocolPanel.sendOAuthFormWithServerError();
    page.authProtocolPanel.assertServerErrorMessageVisible();
  });
});

function showBugTrackerView(): BugTrackerViewPage {
  const initialNodes = getInitialNodes();
  const page = AdminWorkspaceBugtrackersPage.initTestAtPageBugtrackers(initialNodes);
  return page.selectBugTrackerByName([initialNodes.dataRows[0].data.name],
    initialNodes.dataRows[0].data as AdminBugTrackerState);
}

function getInitialNodes(): GridResponse {
  const bugtracker = getInitialModel();

  return {
    count: 1,
    dataRows: [
      {
        id: bugtracker.id.toString(), children: [], data: bugtracker, allowMoves: true, allowedChildren: [], type: 'Generic'
      } as unknown as DataRow,
    ],
  };
}

function getInitialModel(): AdminBugTrackerState {
  return {
    id: 1,
    authProtocol: AuthenticationProtocol.BASIC_AUTH,
    authPolicy: AuthenticationPolicy.USER,
    kind: 'jira.cloud',
    supportedAuthenticationProtocols: [AuthenticationProtocol.BASIC_AUTH, AuthenticationProtocol.OAUTH_1A],
    url: 'http://some.url',
    name: 'BT1',
    iframeFriendly: true,
    bugTrackerKinds: ['jira.cloud', 'jira.server', 'mantis'],
  };
}
