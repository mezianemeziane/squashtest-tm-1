import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {AdminWorkspaceInfoListsPage} from '../../../../page-objects/pages/administration-workspace/admin-workspace-info-lists.page';
import {InfoListViewPage} from '../../../../page-objects/pages/administration-workspace/info-list-view/info-list-view.page';
import {AdminInfoList, AdminInfoListItem} from '../../../../model/infolist/adminInfoList.model';
import {NavBarAdminElement} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';

describe('Administration Workspace - Info lists', function () {
  const initialNodes: GridResponse = {
    count: 2,
    dataRows: [
      {
        id: '1',
        children: [],
        data: {
          infoListId: 1,
          label: 'List 1',
          description: 'description',
          code: 'LIST1',
          defaultValue: 'undefined',
          projectCount: 2,
        }
      } as unknown as DataRow,
      {
        id: '2',
        children: [],
        data: {
          infoListId: 2,
          label: 'List 2',
          description: 'description',
          code: 'LIST2',
          defaultValue: 'undefined',
          projectCount: 0,
        }
      } as unknown as DataRow,
    ]
  };

  it('should display info lists grid', () => {
    const page = AdminWorkspaceInfoListsPage.initTestAtPageInfoLists(initialNodes);
    const grid = page.grid;

    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row.cell('label').textRenderer().assertContainText('List 1');
    row.cell('description').textRenderer().assertContainText('description');
    row.cell('code').textRenderer().assertContainText('LIST1');
    row.cell('defaultValue').textRenderer().assertContainText('undefined');

    grid.assertRowExist(2);
    const rowList2 = grid.getRow(2);
    rowList2.cell('label').textRenderer().assertContainText('List 2');
    rowList2.cell('description').textRenderer().assertContainText('description');
    rowList2.cell('code').textRenderer().assertContainText('LIST2');
    rowList2.cell('defaultValue').textRenderer().assertContainText('undefined');
  });

  it('should create info list', () => {
    const page = AdminWorkspaceInfoListsPage.initTestAtPageInfoLists(initialNodes);

    const dialog = page.openCreateInfoList();
    dialog.assertExist();

    dialog.labelField.fill('info list 1');
    dialog.codeField.fill('IL_01');
    dialog.descriptionField.fill('description');
    cy.clickVoid();
    dialog.addInfoListOption('option 1', 'code1');
    dialog.infoListOptionsGrid.assertRowCount(1);
    dialog.addWithOptions({
      addAnother: false,
      createResponse: {id: 3},
      gridResponse: {
        count: initialNodes.count + 1,
        dataRows: [
          ...initialNodes.dataRows,
          {
            id: '3',
            children: [],
            data: {
              infoListId: 3,
              label: 'info list 1',
              code: 'IL_01',
              description: 'description',
              defaultValue: 'option 1',
              projectCount: 0,
            }
          } as unknown as DataRow,
        ]
      }
    });
  });

  it('should validate creation form', () => {
    const page = AdminWorkspaceInfoListsPage.initTestAtPageInfoLists(initialNodes);

    const dialog = page.openCreateInfoList();
    dialog.assertExist();
    dialog.labelField.fill('il');

    // should forbid to create info list with invalid code pattern
    dialog.codeField.fill('IL-01!');
    dialog.addInfoListOption('option 1', 'code1');
    dialog.infoListOptionsGrid.assertRowCount(1);
    dialog.clickOnAddButton();
    dialog.checkIfInvalidCodePatternErrorIsDisplayed();

    // should forbid to create info list without default value
    dialog.codeField.fill('IL_01');
    dialog.removeOptionWithLabel('option 1');
    dialog.clickOnAddButton();
    dialog.checkIfOptionDefaultValueRequiredErrorIsDisplayed();

    // should forbid to create info list if option name already exists
    dialog.addInfoListOption('option 1', 'code1');
    dialog.fillInfoListOptionLabel('option 1');
    dialog.fillInfoListOptionCode('code2');
    dialog.clickOnAddOptionButton();
    dialog.checkIfOptionNameAlreadyExistErrorIsDisplayed();

    // should forbid to create info list if option code already exists
    dialog.fillInfoListOptionLabel('option 2');
    dialog.fillInfoListOptionCode('code1');
    dialog.clickOnAddOptionButton();
    dialog.checkIfOptionCodeAlreadyExistErrorIsDisplayed();

    // should forbid to create info list if option code has invalid pattern
    dialog.fillInfoListOptionLabel('option 1');
    dialog.fillInfoListOptionCode('code1!!!!');
    dialog.clickOnAddOptionButton();
    dialog.checkIfInvalidCodePatternErrorIsDisplayed();
  });

  it('should delete an info list from the grid', () => {
    const page = AdminWorkspaceInfoListsPage.initTestAtPageInfoLists(initialNodes);
    page.deleteRowWithMatchingCellContent('label', ' List 1', true, {
      count: 1,
      dataRows: [
        initialNodes.dataRows[1]
      ]
    });

    page.grid.assertRowCount(1);
  });


  it('should delete multiple rows', () => {
    const page = AdminWorkspaceInfoListsPage.initTestAtPageInfoLists(initialNodes);
    page.selectInfoListsByLabel(['List 1', 'List 2']);
    page.clickOnMultipleDeleteButton(true, {
      count: 0,
      dataRows: []
    });
    page.grid.assertRowCount(0);
  });
});

describe('Administration Workspace - Info lists - Information panel', function () {
  it('should set infolist name', () => {
    const infoListPage = showInfoListView();
    infoListPage.assertExist();

    infoListPage.entityNameField.setAndConfirmValue('NEW');
    infoListPage.entityNameField.checkContent('NEW');
  });

  it('should set infolist code', () => {
    const infoListPage = showInfoListView();
    infoListPage.assertExist();

    infoListPage.informationPanel.codeField.setAndConfirmValue('NEW');
    infoListPage.informationPanel.codeField.checkContent('NEW');
  });

  it('should set infolist description', () => {
    const infoListPage = showInfoListView();
    infoListPage.assertExist();

    infoListPage.informationPanel.descriptionField.setAndConfirmValue('NEW');
    infoListPage.informationPanel.descriptionField.checkTextContent('NEW');
  });
});

describe('Administration Workspace - Info lists - Items panel', function () {
  it('should show info list items grid', () => {
    const page = showInfoListView();
    page.itemsPanel.grid.assertRowCount(3);
  });

  it('should forbid removing the default item', () => {
    // given
    const page = showInfoListView();
    const itemsPanel = page.itemsPanel;
    const first = 'label1';
    const second = 'label2';

    itemsPanel.assertDeleteIconIsHidden(first);
    itemsPanel.assertDeleteIconIsVisible(second);

    // when
    itemsPanel.setDefaultItem(second);

    // then
    itemsPanel.assertDeleteIconIsVisible(first);
    itemsPanel.assertDeleteIconIsHidden(second);
  });

  it('should add a new item', () => {
    // given
    const initialModel = getInitialInfoListModel();
    const page = showInfoListView();
    const addDialog = page.itemsPanel.openAddItemDialog();

    addDialog.iconField.assertHasNoIcon();
    const desiredIcon = 'sqtm-core-infolist-item\\:handshake';
    addDialog.iconField.selectIcon(desiredIcon);
    addDialog.iconField.assertHasIcon(desiredIcon);

    addDialog.nameField.fill('label4');
    addDialog.codeField.fill('code4');

    addDialog.addItem({
      ...initialModel,
      items: [
        ...initialModel.items,
        {...getInfoListItem(4, 3), iconName: 'handshake'},
      ]
    });

    page.itemsPanel.grid.assertRowCount(4);
  });
});


function showInfoListView(): InfoListViewPage {
  const initialNodes = getInitialNodes();
  const page = AdminWorkspaceInfoListsPage.initTestAtPageInfoLists(initialNodes);
  const viewPage = page.selectInfoListsByLabel([initialNodes.dataRows[0].data.label],
    initialNodes.dataRows[0].data as AdminInfoList);

  new NavBarAdminElement().toggle();
  viewPage.foldGrid();

  return viewPage;
}

function getInitialNodes(): GridResponse {
  const infoList = getInitialInfoListModel();

  return {
    count: 1,
    dataRows: [
      {
        id: infoList.id.toString(), children: [], data: infoList, allowMoves: true, allowedChildren: [], type: 'Generic'
      } as unknown as DataRow,
    ],
  };
}

function getInitialInfoListModel(): AdminInfoList {
  return {
    id: 1,
    code: 'list1',
    label: 'InfoList1',
    createdBy: 'cypress',
    createdOn: new Date(),
    description: '',
    items: [
      {...getInfoListItem(1, 0), isDefault: true},
      getInfoListItem(2, 1),
      getInfoListItem(3, 2),
    ],
    lastModifiedBy: null,
    lastModifiedOn: null,
  };
}

function getInfoListItem(id: number, index: number): AdminInfoListItem {
  return {
    id,
    label: 'label' + id,
    code: 'code' + id,
    system: false,
    itemIndex: index,
    isDefault: false,
    iconName: '',
    colour: '',
    friendlyLabel: '',
    uri: ''
  };
}
