import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {CreateExportViewPage} from '../../../page-objects/pages/custom-report-workspace/create-export-view.page';
import {ALL_PROJECT_PERMISSIONS, ReferentialDataMockBuilder} from '../../../utils/referential/referential-data-builder';
import {BindableEntity} from '../../../model/bindable-entity.model';
import {InputType} from '../../../model/customfield/customfield.model';
import {DataRow, DataRowOpenState, GridResponse} from '../../../model/grids/data-row.type';
import {mockGridResponse} from '../../../data-mock/grid.data-mock';
import {CustomExportEntityType} from '../../../model/custom-report/custom-export-columns.model';
import {CustomExportModel} from '../../../model/custom-report/custom-export.model';


describe('Create Custom Export View', function () {
  it('should create and display a custom export', () => {
    const workbenchPage = navigateToCustomExportWorkbench(true);
    new NavBarElement().toggle();
    workbenchPage.assertExist();

    workbenchPage.changeName('New');

    const scopeSelector = workbenchPage.openScopeSelector(mockCampaignGridResponse());
    scopeSelector.selectNode('Campaign 1');
    scopeSelector.confirm();

    workbenchPage.dragAndDropAttribute('Description');
    workbenchPage.dragAndDropAttribute('My CUF2');

    const customExportViewPage = workbenchPage.confirm(3, mockCustomLibraryGridResponse(), mockCustomExportModel());
    customExportViewPage.assertExist();
  });

  it('should show milestone columns', () => {
    const workbenchPage = navigateToCustomExportWorkbench(true);
    new NavBarElement().toggle();
    workbenchPage.assertExist();
    workbenchPage.assertMilestoneColumnsExist();
  });

  it('should not show milestone columns', () => {
    const workbenchPage = navigateToCustomExportWorkbench(false);
    new NavBarElement().toggle();
    workbenchPage.assertExist();
    workbenchPage.assertMilestoneColumnsNotExist();
  });
});

function navigateToCustomExportWorkbench(enableMilestoneMode: boolean): CreateExportViewPage {
  return CreateExportViewPage.initTestAtPage(new ReferentialDataMockBuilder()
    .withProjects({
      name: 'P',
      permissions: ALL_PROJECT_PERMISSIONS,
    })
    .withCustomFields({
        name: 'My CUF',
        code: 'MyCUF',
        inputType: InputType.PLAIN_TEXT,
        label: 'My CUF',
        optional: false,
        bindings: [{
          bindableEntities: [BindableEntity.CAMPAIGN],
          projectIndexes: [0],
        }],
      },
      {
        name: 'My CUF2',
        code: 'MyCUF2',
        inputType: InputType.PLAIN_TEXT,
        label: 'My CUF2',
        optional: false,
        bindings: [{
          bindableEntities: [BindableEntity.CAMPAIGN],
          projectIndexes: [0],
        }],
      })
    .withGlobalConfiguration({
      milestoneFeatureEnabled: enableMilestoneMode,
      uploadFileExtensionWhitelist: [],
      uploadFileSizeLimit: 0,
    })
    .build());
}

function mockCampaignGridResponse(): GridResponse {
  return {
    count: 3,
    dataRows: [{
      id: 'CampaignLibrary-1',
      children: ['CampaignFolder-2'],
      projectId: 1,
      state: DataRowOpenState.open,
      data: {'NAME': 'Project1', 'CHILD_COUNT': 1}
    } as unknown as DataRow,
      {
        id: 'CampaignFolder-2',
        children: ['Campaign-3'],
        parentRowId: 'CampaignLibrary-1',
        projectId: 1,
        state: DataRowOpenState.open,
        data: {'NAME': 'Folder 1', 'CHILD_COUNT': 1}
      } as unknown as DataRow,
      {
        id: 'Campaign-3',
        children: [],
        projectId: 1,
        parentRowId: 'CampaignFolder-2',
        data: {'NAME': 'Campaign 1', 'CHILD_COUNT': 0}
      } as unknown as DataRow
    ]
  };
}

function mockCustomExportModel(): CustomExportModel {
  return {
    name: 'New',
    columns: [{
      columnName: 'CAMPAIGN_DESCRIPTION',
      cufId: null,
      entityType: CustomExportEntityType.CAMPAIGN,
    },
      {
        columnName: 'CAMPAIGN_CUF',
        cufId: 1,
        entityType: CustomExportEntityType.CAMPAIGN,
      }],
    id: 3,
    projectId: 1,
    customReportLibraryNodeId: 3,
    scopeNodeId: 1,
    scopeNodeName: 'Camp'
  };
}

function mockCustomLibraryGridResponse(): GridResponse {
  return mockGridResponse('id', [
    {
      id: 'CustomReportCustomExport-3',
      children: [],
      state: DataRowOpenState.leaf,
      data: {
        'CRLN_ID': 3,
        'CHILD_COUNT': 0,
        'NAME': 'Financial Breakdown',
      }
    } as unknown as DataRow,
  ]);
}
