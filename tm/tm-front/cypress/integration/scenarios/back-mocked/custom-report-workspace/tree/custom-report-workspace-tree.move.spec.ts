import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {CustomReportWorkspacePage} from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';


describe('Custom Report Workspace Tree Move', function () {

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'CustomReportLibrary-1',
      projectId: 1,
      children: [],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'},
    } as unknown as DataRow,
      {
        id: 'CustomReportLibrary-2',
        projectId: 2,
        children: [],
        data: {'NAME': 'Project2', 'CHILD_COUNT': '0'},
      } as unknown as DataRow]
  };

  const crLib1 = {
    id: 'CustomReportLibrary-1',
    projectId: 1,
    children: ['CustomReportFolder-1', 'ChartDefinition-3', 'CustomReportFolder-2'],
    data: {'NAME': 'Project1', 'CHILD_COUNT': '3'},
    state: DataRowOpenState.open
  } as unknown as DataRow;

  const crFolder1 = {
    id: 'CustomReportFolder-1',
    children: [],
    projectId: 1,
    parentRowId: 'CustomReportLibrary-1',
    data: {'NAME': 'Folder1'},
  } as unknown as DataRow;

  const c3 = {
    id: 'ChartDefinition-3',
    children: [],
    parentRowId: 'CustomReportLibrary-1',
    state: DataRowOpenState.leaf,
    projectId: 1,
    data: {'NAME': 'A nice chart'}
  } as unknown as DataRow;

  const crFolder2 = {
    id: 'CustomReportFolder-2',
    children: [],
    projectId: 1,
    parentRowId: 'CustomReportLibrary-1',
    data: {'NAME': 'Folder2'},
  } as unknown as DataRow;

  const libraryRefreshAtOpen = [
    crLib1,
    crFolder1,
    c3,
    crFolder2
  ];

  it('should drop into folder', () => {
    const firstNode = initialNodes.dataRows[0];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    const tree = customReportWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.beginDragAndDrop(crFolder1.id);
    tree.assertNodeNotExist(crFolder1.id);
    tree.dragOverCenter(crFolder2.id);
    tree.assertContainerIsDndTarget(crFolder2.id);
    const refreshedRows = [
      {...crLib1, children: [c3.id, crFolder2.id]},
      {...crFolder1, parentRowId: crFolder2.id},
      {...c3},
      {...crFolder2, children: [crFolder1.id], state: DataRowOpenState.open},
    ];
    tree.drop(crFolder2.id, 'CustomReportLibrary-1,CustomReportFolder-2', refreshedRows);
    tree.assertRowHasParent(crFolder1.id, crFolder2.id);
  });



  it('should show rows when dragging multiple rows', () => {
    const firstNode = initialNodes.dataRows[0];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    const tree = customReportWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode(crFolder2.id);
    tree.addNodeToSelection(crFolder1.id, {});
    tree.beginDragAndDrop(crFolder2.id);
    tree.assertNodeNotExist(crFolder2.id);
    tree.assertNodeNotExist(crFolder1.id);
    tree.assertDndPlaceholderContains(crFolder2.id, 'Folder2');
    tree.assertDndPlaceholderContains(crFolder1.id, 'Folder1');
  });
});
