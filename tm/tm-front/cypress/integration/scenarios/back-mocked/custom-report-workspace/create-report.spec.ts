import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  anotherRequirementEditableReport,
  CreateReportViewPage,
  executionAdvanceReport,
  requirementEditableReport, variousEditableReport
} from '../../../page-objects/pages/custom-report-workspace/create-report-view.page';
import {ReportDefinitionViewModel, ReportInputType} from '../../../model/custom-report/report-definition.model';
import {
  MultiRadioWidget,
  TreePickerOption
} from '../../../page-objects/pages/custom-report-workspace/report-widget/report-widget';
import {ReferentialDataMockBuilder} from '../../../utils/referential/referential-data-builder';
import {getSimpleRequirementLibraryChildNodes} from '../../../data-mock/requirement-tree.mock';
import {InputType} from '../../../model/customfield/customfield.model';
import {BindableEntity} from '../../../model/bindable-entity.model';
import {getSimpleCustomReportLibraryChildNodes} from '../../../data-mock/custom-report.data-mock';
import {
  ReportDefinitionViewPage,
  ReportInformationPanelElement
} from '../../../page-objects/pages/custom-report-workspace/report-definition-view.page';

function buildReferentialData() {
  const referentialDataMock = new ReferentialDataMockBuilder()
    .withProjects(
      {name: 'Apollo', label: 'Apollo'},
      {name: 'Gemini', label: 'Gemini'},
    ).withMilestones(
      {
        label: 'saturn 5 tests',
        description: '',
        endDate: new Date(),
        status: 'IN_PROGRESS',
        boundProjectIndexes: [0, 1],
        range: 'GLOBAL',
        ownerFistName: 'admin',
        ownerLastName: 'admin',
        ownerLogin: 'admin',
      },
      {
        label: 'orbital rendez-vous',
        description: '',
        endDate: new Date(2020, 6, 28),
        status: 'IN_PROGRESS',
        boundProjectIndexes: [0, 1],
        range: 'GLOBAL',
        ownerFistName: 'admin',
        ownerLastName: 'admin',
        ownerLogin: 'admin',
      })
    .withUser({functionalTester: true})
    // NOT WORKING... DON'T KNOW WHY...
    .withCustomFields({
      label: 'tags2',
      code: 'code2',
      inputType: InputType.TAG,
      name: 'tags2',
      bindings: [{bindableEntities: [BindableEntity.REQUIREMENT_VERSION], projectIndexes: [0, 1]}],
      optional: false,
      options: [{label: 'hello', code: 'code'}]
    }, {
      label: 'tags',
      code: 'code',
      inputType: InputType.TAG,
      name: 'tags',
      bindings: [{bindableEntities: [BindableEntity.REQUIREMENT_VERSION], projectIndexes: [1]}],
      optional: false,
      options: [{label: 'hello', code: 'code'}]
    })
    .build();
  referentialDataMock.globalConfiguration.milestoneFeatureEnabled = true;
  return referentialDataMock;
}

describe('Create Report View', function () {
  it('should display create report form', () => {
    const createReportViewPage = navigateToReportWorkbench();
    new NavBarElement().toggle();
    createReportViewPage.assertExist();
    createReportViewPage.assertPreparationPhaseReportListExist();
    createReportViewPage.assertExecutionPhaseReportListExist();
    createReportViewPage.assertPreparationPhaseReportExist(requirementEditableReport.id, requirementEditableReport.label);
    createReportViewPage.assertPreparationPhaseReportExist(anotherRequirementEditableReport.id, anotherRequirementEditableReport.label);
    createReportViewPage.assertExecutionPhaseReportExist(executionAdvanceReport.id, executionAdvanceReport.label);
    createReportViewPage.assertVariousReportExist(variousEditableReport.id, variousEditableReport.label);
    createReportViewPage.assertNoReportIsSelected();
    createReportViewPage.selectReport(requirementEditableReport.id);
    createReportViewPage.assertReportIsSelected(requirementEditableReport.id);
    createReportViewPage.assertReportHasTitle(requirementEditableReport.label);
    const perimeterSelector =
      createReportViewPage.findCriteriaWidget('requirementsSelectionMode', ReportInputType.RADIO_BUTTONS_GROUP) as MultiRadioWidget;
    perimeterSelector.assertCriteriaHasTitle('Périmètre du rapport');
    perimeterSelector.assertOptionIsSelected('projectIds');
    perimeterSelector.assertOptionIsNotSelected('requirementsIds');
    perimeterSelector.assertOptionIsNotSelected('milestones');
    perimeterSelector.assertOptionIsNotSelected('tags');
    const projectPickerOption = perimeterSelector.createFormProjectWidget();
    projectPickerOption.assertExist();
    projectPickerOption.assertSelectedProjectsContains('Apollo');
    const projectPickerDialog = projectPickerOption.openProjectSelector();
    projectPickerDialog.assertExist();
    projectPickerDialog.assertProjectsAreSelected([1]);
    projectPickerDialog.toggleProject(1);
    projectPickerDialog.confirm();
    projectPickerOption.assertNoProjectAreSelected();
    createReportViewPage.clickOnDownload();
    perimeterSelector.assertRequiredErrorMessageExist();
    projectPickerOption.openProjectSelector();
    projectPickerDialog.toggleProject(1);
    projectPickerDialog.confirm();
    projectPickerOption.assertSelectedProjectsContains('Apollo');
    perimeterSelector.assertRequiredErrorMessageNotExist();
  });

  it('should select perimeter by custom tree selection', () => {
    const createReportViewPage = navigateToReportWorkbench();
    new NavBarElement().toggle();
    createReportViewPage.assertExist();
    createReportViewPage.selectReport(requirementEditableReport.id);
    createReportViewPage.assertReportIsSelected(requirementEditableReport.id);
    createReportViewPage.assertReportHasTitle(requirementEditableReport.label);
    createReportViewPage.toggleReportSelector();
    const perimeterSelector =
      createReportViewPage.findCriteriaWidget('requirementsSelectionMode', ReportInputType.RADIO_BUTTONS_GROUP) as MultiRadioWidget;
    perimeterSelector.changeSelectOption('requirementsIds');
    perimeterSelector.assertOptionIsSelected('requirementsIds');
    perimeterSelector.assertOptionIsNotSelected('projectIds');

    const treePickerOption =
      perimeterSelector.createFormTreePicker('requirementsIds', 'requirement') as TreePickerOption;
    createReportViewPage.clickOnDownload();
    perimeterSelector.assertRequiredErrorMessageExist();
    treePickerOption.assertExist();
    treePickerOption.assertNoNodesAreSelected();
    treePickerOption.openTreeNodeSelector({dataRows: getSimpleRequirementLibraryChildNodes()});
    // libraries should be filtered and thus no selection occurred
    treePickerOption.pickNodes('RequirementLibrary-1');
    treePickerOption.confirm();
    treePickerOption.assertNoNodesAreSelected();
    treePickerOption.openTreeNodeSelector({dataRows: getSimpleRequirementLibraryChildNodes()});
    treePickerOption.pickNodes('Requirement-3', 'RequirementFolder-1');
    treePickerOption.confirm();
    treePickerOption.assertHowManyNodesAreSelected(2);
    treePickerOption.openTreeNodeSelector({dataRows: getSimpleRequirementLibraryChildNodes()});
    treePickerOption.assertNodesAreSelected('Requirement-3', 'RequirementFolder-1');
    treePickerOption.pickNodes('RequirementFolder-2');
    treePickerOption.cancel();
    treePickerOption.assertHowManyNodesAreSelected(2);
    treePickerOption.openTreeNodeSelector({dataRows: getSimpleRequirementLibraryChildNodes()});
    treePickerOption.assertNodesAreSelected('Requirement-3', 'RequirementFolder-1');
    treePickerOption.pickNodes('RequirementFolder-2');
    treePickerOption.confirm();
    treePickerOption.assertHowManyNodesAreSelected(1);
  });

  it('should select perimeter by milestone', () => {
    const createReportViewPage = navigateToReportWorkbench();
    new NavBarElement().toggle();
    createReportViewPage.assertExist();
    createReportViewPage.selectReport(requirementEditableReport.id);
    createReportViewPage.assertReportIsSelected(requirementEditableReport.id);
    createReportViewPage.assertReportHasTitle(requirementEditableReport.label);
    createReportViewPage.toggleReportSelector();
    const perimeterSelector =
      createReportViewPage.findCriteriaWidget('requirementsSelectionMode', ReportInputType.RADIO_BUTTONS_GROUP) as MultiRadioWidget;
    perimeterSelector.changeSelectOption('milestones');
    perimeterSelector.assertOptionIsSelected('milestones');
    perimeterSelector.assertOptionIsNotSelected('projectIds');
    perimeterSelector.assertOptionIsNotSelected('requirementsIds');

    // Testing milestone picker
    const milestonePickerOption = perimeterSelector.createFormMilestonePicker();
    milestonePickerOption.assertNoMilestonesAreSelected();
    createReportViewPage.clickOnDownload();
    perimeterSelector.assertRequiredErrorMessageExist();
    const pickerDialogElement = milestonePickerOption.openMilestoneSelector();
    pickerDialogElement.selectMilestone(1);
    pickerDialogElement.confirm();
    milestonePickerOption.assertSelectedMilestoneContains('saturn 5 tests');
  });

  it('should save chart', () => {
    const createReportViewPage = navigateToReportWorkbench();
    new NavBarElement().toggle();
    createReportViewPage.assertExist();
    createReportViewPage.selectReport(requirementEditableReport.id);
    createReportViewPage.assertReportIsSelected(requirementEditableReport.id);
    // guard on ckeditor to prevent error on too fast navigation... Cypress is so fast that ckeditor is not initialized when navigating
    // and thus ckeditor.js emit an error because it's still running init phase.
    createReportViewPage.descriptionField.assertIsReady();
    createReportViewPage.simulateDuplicateNodeFailure('1');
    createReportViewPage.assertDuplicateNodeNameErrorIsVisible();
    const reportDefinitionViewModel: Partial<ReportDefinitionViewModel> = {
      customReportLibraryNodeId: 4,
      reportLabel: 'Cahier des exigences (format éditable)',
      summary: 'un rapport',
      name: 'Reg Report',
      pluginNamespace: 'report.books.requirements.requirements.report.label',
      attributes: {
        'Projets': [
          '1 Projet 1'
        ],
        'Option d\'impression': [
          'Imprimer seulement la dernière version d\'exigence'
        ]
      }
    };
    const customReportWorkspacePage =
      createReportViewPage.saveReport('1', 4, getSimpleCustomReportLibraryChildNodes(), reportDefinitionViewModel);
    customReportWorkspacePage.tree.assertNodeIsSelected('ReportDefinition-4');
    const reportDefinitionViewPage = new ReportDefinitionViewPage(4);
    const reportInformationPanelElement = reportDefinitionViewPage.clickAnchorLink('information') as ReportInformationPanelElement;
    reportInformationPanelElement.assertSummaryContains('un rapport');
    reportInformationPanelElement.assertAttributesContains('Projets', '1 Projet 1');
    reportInformationPanelElement.assertAttributesContains('Option d\'impression', 'Imprimer seulement la dernière version d\'exigence');
  });

});

function navigateToReportWorkbench() {
  return CreateReportViewPage.initTestAtPage(buildReferentialData(), undefined, '1');
}


