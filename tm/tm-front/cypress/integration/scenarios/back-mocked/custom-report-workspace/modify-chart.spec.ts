import {
  basicWorkbenchData,
} from '../../../page-objects/pages/custom-report-workspace/create-chart-view.page';
import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  ChartColumnType,
  ChartDataType,
  ChartDefinitionModel, ChartOperation,
  ChartType
} from '../../../model/custom-report/chart-definition.model';
import {getSimpleChartDefinition} from '../../../data-mock/custom-chart.mocks';
import {ModifyChartViewPage} from '../../../page-objects/pages/custom-report-workspace/modify-chart-view-page';
import {EntityType} from '../../../model/entity.model';
import {ChartDefinitionViewPage} from '../../../page-objects/pages/custom-report-workspace/chart-definition-view.page';
import {getSimpleCustomReportLibraryChildNodes} from '../../../data-mock/custom-report.data-mock';


describe('Modify Chart View', function () {
  it('should modify an existing chart', () => {
    const model = getSimpleChartDefinition(ChartType.PIE);
    const updatedModel: Partial<ChartDefinitionModel> = {...model};
    updatedModel.axis = [
      {
        cufId: null,
        label: '',
        column: {
          columnType: ChartColumnType.ATTRIBUTE,
          label: 'REQUIREMENT_CRITICALITY',
          specializedType: {
            entityType: EntityType.REQUIREMENT,
            entityRole: null
          },
          dataType: ChartDataType.LEVEL_ENUM
        },
        operation: ChartOperation.NONE
      }
    ];
    updatedModel.abscissa = [['MAJOR']];
    updatedModel.series = {'': [1]};

    const modifyChartViewPage = navigateToChartWorkbench(model);
    new NavBarElement().toggle();
    modifyChartViewPage.assertExist();
    modifyChartViewPage.assertChartIsRendered();
    modifyChartViewPage.axisSelector.openAttributeSelector();
    modifyChartViewPage.axisSelector.addAttributeAndRenderPreview(4, '1', updatedModel);
    const customReportWorkspacePage = modifyChartViewPage.saveChartModifications('3',
      getSimpleCustomReportLibraryChildNodes(), updatedModel);
    customReportWorkspacePage.tree.assertNodeIsSelected('ChartDefinition-3');
    const chartDefinitionViewPage = new ChartDefinitionViewPage(3);
    chartDefinitionViewPage.checkAxisInfo('Exigences | Criticité - Agrégation');

  });

  function navigateToChartWorkbench(model: Partial<ChartDefinitionModel>): ModifyChartViewPage {
    const chartWorkbenchData = basicWorkbenchData;
    chartWorkbenchData.chartDefinition = model;
    return ModifyChartViewPage.initTestAtPage(model, undefined, chartWorkbenchData, '3');
  }
});



