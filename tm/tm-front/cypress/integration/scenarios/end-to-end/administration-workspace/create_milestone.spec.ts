import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {AdminWorkspaceMilestonesPage} from '../../../page-objects/pages/administration-workspace/admin-workspace-milestones.page';

describe('Create Milestone', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
  });

  it('should check error Messages ', () => {
    cy.task('cleanDatabase');
    cy.logInAs('admin', 'admin');

    const adminWorkspaceMilestonesPage = NavBarElement.navigateToAdministration<AdminWorkspaceMilestonesPage>('milestones');
   // const createMilestoneDialog = adminWorkspaceMilestonesPage.openCreateMilestone();

    // createMilestoneDialog.clickOnAddButton();
    // createMilestoneDialog.checkIfRequiredErrorMessageIsDisplayed();
    // createMilestoneDialog.fillName('JALON');
    // createMilestoneDialog.clickOnAddButton();
    // createMilestoneDialog.checkIfRequiredErrorMessageIsDisplayed();

  });

  it.skip('should create Milestone ', () => {
    cy.task('cleanDatabase');
    cy.logInAs('admin', 'admin');

    const adminWorkspaceMilestonesPage = NavBarElement.navigateToAdministration<AdminWorkspaceMilestonesPage>('milestones');
    const createMilestoneDialog = adminWorkspaceMilestonesPage.openCreateMilestone();

    createMilestoneDialog.createMilestone('Mon JALON');

  });



  it('should delete milestone', () => {
    //  cy.logInAs('admin', 'admin');
    //  cy.viewport(1600, 900);
    // const adminWorkspaceUsersPage = <AdminWorkspaceUsersPage> NavBarElement.navigateToAdministration('users');
    // adminWorkspaceUsersPage.deleteUserByLogin('NewUser', true);
    // adminWorkspaceUsersPage.selectUsersByLogin(['2User']);
    // adminWorkspaceUsersPage.clickOnMultipleDeleteButton(false);
    // adminWorkspaceUsersPage.clickOnMultipleDeleteButton(true);

  });

});
