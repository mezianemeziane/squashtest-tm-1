import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {AdminWorkspaceProjectsPage} from '../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import {ProjectViewPage} from '../../../page-objects/pages/administration-workspace/project-view/project-view.page';
import {ProjectE2eCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';

describe('Modify Project', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
    cy.task('cleanDatabase');
    ProjectE2eCommands.createProject('project-1');
  });

  it('should check project information', () => {
    cy.logInAs('admin', 'admin');
    const adminWorkspaceProjectsPage = NavBarElement.navigateToAdministration<AdminWorkspaceProjectsPage>('projects');
    adminWorkspaceProjectsPage.grid.findRowId('name', 'project-1').then(projectId => {
      adminWorkspaceProjectsPage.grid.selectRow(projectId);
      const projectViewPage = new ProjectViewPage();
      projectViewPage.checkData('entity-name', 'project-1');
      projectViewPage.checkData('project-label', 'project-1');
      projectViewPage.checkDescriptionData('project-description', 'description' );
    });
  });

  it('should modify project information', () => {
    cy.logInAs('admin', 'admin');

    const adminWorkspaceProjectsPage = NavBarElement.navigateToAdministration<AdminWorkspaceProjectsPage>('projects');
      adminWorkspaceProjectsPage.grid.findRowId('name', 'project-1').then(projectId => {
        adminWorkspaceProjectsPage.grid.selectRow(projectId);
        const projectView = new ProjectViewPage();
      projectView.checkModificationDate(true);

      projectView.modifyData('entity-name', 'projectNEW');
      projectView.modifyData('project-description', 'NEW description');
      projectView.modifyData('project-label', 'NEW label');

      cy.reload();
    });
    adminWorkspaceProjectsPage.grid.findRowId('name', 'projectNEW').then(projectId => {
      adminWorkspaceProjectsPage.grid.selectRow(projectId);
      const projectView = new ProjectViewPage();
      projectView.checkModificationDate(false, true);
    });
  });

});
