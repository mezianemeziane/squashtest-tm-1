import {ProjectE2eCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';

describe('Create Project', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
  });

  it('should create project', () => {
    cy.task('cleanDatabase');
    ProjectE2eCommands.createProject('project-1');
    cy.logInAs('admin', 'admin');
    cy.goHome();
    ProjectE2eCommands.verifyProject('project-1');
  });
});
