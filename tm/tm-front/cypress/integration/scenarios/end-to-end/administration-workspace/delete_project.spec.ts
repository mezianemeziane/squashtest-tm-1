import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {AdminWorkspaceProjectsPage} from '../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import {ProjectViewPage} from '../../../page-objects/pages/administration-workspace/project-view/project-view.page';
import {ProjectE2eCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';

describe('Delete Project', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
    cy.task('cleanDatabase');
    ProjectE2eCommands.createProjects(['project-1', 'project-2']);
  });

  it('should delete project', () => {
    cy.logInAs('admin', 'admin');
    const adminWorkspaceProjectsPage = NavBarElement.navigateToAdministration<AdminWorkspaceProjectsPage>('projects');
    adminWorkspaceProjectsPage.grid.findRowId('name', 'project-1').then(projectId => {
      adminWorkspaceProjectsPage.grid.selectRow(projectId);
      const projectView = new ProjectViewPage();
      const removeProjectDialog = projectView.clickOnDeleteProjectMenuItem();
      removeProjectDialog.deleteForSuccess();
      cy.goHome();
      ProjectE2eCommands.verifyProject('project-2');
    });
  });
});
