import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {CreateCampaignDialog} from '../../../page-objects/pages/campaign-workspace/dialogs/create-campaign-dialog.element';
import {CampaignViewPage} from '../../../page-objects/pages/campaign-workspace/campaign/campaign-view.page';
import {CreateIterationDialog} from '../../../page-objects/pages/campaign-workspace/dialogs/create-iteration-dialog.element';
import {IterationViewPage} from '../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import {TestCaseWorkspacePage} from '../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {IterationTestPlanPage} from '../../../page-objects/pages/campaign-workspace/iteration/iteration-test-plan.page';
import {ProjectE2eCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';

// TODO RE WRITE THIS TEST

describe.skip('Create execution', function () {
  const TEST1_NAME = 'TEST1';
  const TEST2_NAME = 'TEST2';

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
  });

  it('should create a project', () => {
    cy.task('cleanDatabase');
    cy.logInAs('admin', 'admin');
    ProjectE2eCommands.createProject('project-1');
  });

  it('should create a campaign', () => {
    cy.logInAs('admin', 'admin');

    const campaignWorkspace = NavBarElement.navigateToCampaignWorkspace();
    campaignWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      campaignWorkspace.tree.selectNode(projectId);

      const createCampaignDialog: CreateCampaignDialog = campaignWorkspace.openCreateCampaign();

      createCampaignDialog.fillDescription('CAMPAIGN DESCRIPTION');
      createCampaignDialog.fillReference('CAMP-REF');
      createCampaignDialog.addWithClientSideFailure();
      createCampaignDialog.checkIfRequiredErrorMessageIsDisplayed();

      createCampaignDialog.fillName('CAMPAIGN1');
      createCampaignDialog.clickOnAddAnotherButton();
      createCampaignDialog.fillName('CAMPAIGN1');

      createCampaignDialog.addWithServerSideFailure();
      createCampaignDialog.checkIfDuplicateNameErrorMessageIsDisplayed();
      createCampaignDialog.cancel();

      campaignWorkspace.openCreateCampaign();
      createCampaignDialog.fillName('CAMPAIGN2');
      createCampaignDialog.addWithOptions({
        addAnother: false,
        parentRowIsClosed: false,
        parentRowRef: '',
      });
      campaignWorkspace.tree.findRowId('NAME', 'CAMPAIGN1').then(campaignId1 => {
        const campaign1viewPage =  campaignWorkspace.tree.selectNode(campaignId1) as CampaignViewPage;
        campaign1viewPage.assertNameContains('CAMPAIGN1');
        campaign1viewPage.checkDescription('CAMPAIGN DESCRIPTION');
      });
    });
  });

  it('should create an iteration', () => {
    cy.logInAs('admin', 'admin');
    const campaignWorkspace = NavBarElement.navigateToCampaignWorkspace();
    campaignWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      campaignWorkspace.tree.openNode(projectId);
      campaignWorkspace.tree.findRowId('NAME', 'CAMPAIGN1').then(campaignId1 => {
        campaignWorkspace.tree.selectNode(campaignId1);

        const createIterationDialog: CreateIterationDialog = campaignWorkspace.openCreateIteration();

        createIterationDialog.fillDescription('ITERATION DESCRIPTION');
        createIterationDialog.fillReference('IT-REF');
        createIterationDialog.addWithClientSideFailure();
        createIterationDialog.checkIfRequiredErrorMessageIsDisplayed();

        createIterationDialog.fillName('ITERATION1');
        createIterationDialog.addWithOptions({ parentRowIsClosed: false, addAnother: true });
        createIterationDialog.fillName('ITERATION1');
        createIterationDialog.addWithServerSideFailure();
        createIterationDialog.checkIfDuplicateNameErrorMessageIsDisplayed();
        createIterationDialog.cancel();

        campaignWorkspace.tree.selectNode(campaignId1);
        const createIterationDialog2: CreateIterationDialog = campaignWorkspace.openCreateIteration();
        createIterationDialog2.fillName('ITERATION2');
        createIterationDialog2.addWithOptions({
          parentRowIsClosed: false
        });

        campaignWorkspace.tree.findRowId('NAME', 'CAMPAIGN2').then(campaignId2 => {
          campaignWorkspace.tree.selectNode(campaignId2);

          campaignWorkspace.tree.findRowId('NAME', 'ITERATION1').then(iterationId => {
            const iteration1viewPage: IterationViewPage = campaignWorkspace.tree.selectNode(iterationId);
            iteration1viewPage.assertNameContains('ITERATION1');
          });
        });
      });
    });
  });

  it('should create test case', () => {
    cy.logInAs('admin', 'admin');

    let testCaseWorkspacePage: TestCaseWorkspacePage;
    testCaseWorkspacePage = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspacePage.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspacePage.tree.selectNode(projectId);
      const createTestCaseDialog = testCaseWorkspacePage.treeMenu.openCreateTestCase();
      createTestCaseDialog.fillFieldsWithoutCuf(TEST1_NAME, 'REF1', 'DESCRIPTION1');
      createTestCaseDialog.clickOnAddAnotherButton();
      createTestCaseDialog.fillFieldsWithoutCuf(TEST2_NAME, 'REF2', 'DESCRIPTION2');
      createTestCaseDialog.clickOnAddButton();
    });
  });

  it('should associate a test case to an iteration execution plan', () => {
    cy.logInAs('admin', 'admin');
    cy.viewport(1600, 900);
    const campaignWorkspace = NavBarElement.navigateToCampaignWorkspace();
    campaignWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      campaignWorkspace.tree.openNode(projectId);
    });
    campaignWorkspace.tree.findRowId('NAME', 'CAMPAIGN1').then(campaignId1 => {
      campaignWorkspace.tree.openNode(campaignId1);
    });

    campaignWorkspace.tree.findRowId('NAME', 'ITERATION1').then(iterationId => {
      const iteration1viewPage =  campaignWorkspace.tree.selectNode(iterationId) as IterationViewPage;

      let iterationTestPlanPage: IterationTestPlanPage;
      iterationTestPlanPage = iteration1viewPage.clickAnchorLink('plan-exec') as IterationTestPlanPage;
      const drawerTreeElement = iterationTestPlanPage.openTestCaseDrawer();


      drawerTreeElement.findRowId('NAME', 'project-1').then(projectId => {
        cy.log('ID4 :' + projectId);
        drawerTreeElement.openNode(projectId);
        drawerTreeElement.findRowId('NAME', TEST2_NAME).then(testCaseId2 => {
          drawerTreeElement.findRowId('NAME', TEST1_NAME).then(testCaseId1 => {
            drawerTreeElement.selectRows([testCaseId2, testCaseId1], 'NAME');
            drawerTreeElement.beginDragAndDrop(testCaseId2);
            iterationTestPlanPage.dropIntoTestPlanNoMock();
            iterationTestPlanPage.closeTestCaseDrawer();
          });
        });
      });
    });
  });
});
