import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {AdminWorkspaceCustomFieldsPage} from '../../../page-objects/pages/administration-workspace/admin-workspace-custom-fields.page';
import {ProjectE2eCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';
import {AdminWorkspaceProjectsPage} from '../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import {ProjectViewPage} from '../../../page-objects/pages/administration-workspace/project-view/project-view.page';
// tslint:disable-next-line:max-line-length
import {CustomFieldsPanelElement} from '../../../page-objects/pages/administration-workspace/project-view/panels/project-custom-fields-panel.element';
import {AdminWorkspaceUsersPage} from '../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import {UserViewPage} from '../../../page-objects/pages/administration-workspace/user-view/user-view.page';


describe('Create Project with CUF - prerequisite', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
    cy.task('cleanDatabase');
    ProjectE2eCommands.createProject('ProjetAutom');
  });

  it('should create CUF', () => {
    cy.logInAs('admin', 'admin');

    const adminWorkspaceCUFPage = NavBarElement.navigateToAdministration<AdminWorkspaceCustomFieldsPage>('entities-customization');

    let createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('ProjetAutom_DATE', 'ProjetAutom_DATE', 'ProjetAutom_DATE', 'Date' );

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog
      .createNonOptionalCUF('ProjetAutom_TS_o', 'ProjetAutom_TS_o', 'ProjetAutom_TS_o', 'Texte simple', 'ProjetAutom_TS_o');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('ProjetAutom_cac', 'ProjetAutom_cac', 'ProjetAutom_cac', 'Case à cocher', 'Vrai');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('ProjetAutom_TAG', 'ProjetAutom_TAG', 'ProjetAutom_TAG', 'Tag', 'ProjetAutom_TAG');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createNonOptionalCUF('ProjetAutom_NUM', 'ProjetAutom_NUM', 'ProjetAutom_NUM', 'Numérique', 50);

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    // TODO ADD DEFAULT VALUE + MAKE NON OPTIONAL
    createCustomFieldDialog.createOptionalCUF('projetAutom_TR_o', 'projetAutom_TR_o', 'projetAutom_TR_o', 'Texte riche');

  });
});


describe('Associate CUF - prerequisite', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
  });

  interface Datatype {
    entity: string;
    cuf: string;
    project: string;
  }

  const dataSets: Datatype[] =
    [{entity: 'Cas de test', cuf: 'ProjetAutom_cac', project: 'ProjetAutom'},
      {entity: 'Cas de test', cuf: 'ProjetAutom_DATE', project: 'ProjetAutom'},
      {entity: 'Cas de test', cuf: 'projetAutom_TR_o', project: 'ProjetAutom'},
      {entity: 'Pas de test', cuf: 'ProjetAutom_TS_o', project: 'ProjetAutom'},
      {entity: 'Pas de test', cuf: 'ProjetAutom_TAG', project: 'ProjetAutom'},
      {entity: 'Pas de test', cuf: 'ProjetAutom_NUM', project: 'ProjetAutom'},
    ];


  dataSets.forEach((data, index) => runTest(data, index));

  function runTest(data: Datatype, index: number) {
    it(`Dataset ${index} - For entity : ${data.entity}, cuf: ${data.cuf} - to project : ${data.project}`,
      () => {
        cy.logInAs('admin', 'admin');
        const adminWorkspaceProjectsPage =
          NavBarElement.navigateToAdministration<AdminWorkspaceProjectsPage>('projects');
        adminWorkspaceProjectsPage.grid.findRowId('name', `${data.project}`).then(projectId => {
          adminWorkspaceProjectsPage.grid.selectRow(projectId, 'name');
          const projectView = new ProjectViewPage();
          const customFieldsPanelElement = projectView.clickAnchorLink('custom-fields') as CustomFieldsPanelElement;
          const bindCustomFieldToProjectDialog = customFieldsPanelElement.openCustomFieldBindingDialog();
          bindCustomFieldToProjectDialog.selectEntity(`${data.entity}`);
          bindCustomFieldToProjectDialog.selectCuf(`${data.cuf}`);
          bindCustomFieldToProjectDialog.confirm();
        });
      });
  }
});

describe('Manage users', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
  });

  it('create user magsi', () => {
    cy.logInAs('admin', 'admin');
    let adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>('users');
    const createUserDialog = adminWorkspaceUsersPage.openCreateUser();
    createUserDialog.createUser('magsi', 'magsi', 'magsi', 'email@email.com', 'Utilisateur', 'password', 'password');
    cy.goHome();
    adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>('users');
    adminWorkspaceUsersPage.grid.findRowId('login', 'magsi').then(userId => {
      adminWorkspaceUsersPage.grid.selectRow(userId);
      const userViewPage = new UserViewPage();
      const addUserAuthorisationsDialog = userViewPage.authorisationsPanel.clickOnAddPermissionButton();
      addUserAuthorisationsDialog.selectProjects('ProjetAutom');
      addUserAuthorisationsDialog.selectProfile('Chef de projet');
      addUserAuthorisationsDialog.confirm();
    });
  });
});

// describe('Check users permissions', function () {
//
//   beforeEach(function () {
//     Cypress.Cookies.debug(true);
//     //cy.server();
//   });
//   interface Datatype {
//     login: string;
//     password: string;
//     importPossible: boolean;
//   }
//   const dataSets: Datatype[] =
//     [ {login: 'admin', password: 'admin', importPossible: true},
//       {login: 'Chef de projet', password: 'Chef de projet', importPossible: true},
//       {login: 'Designer de tests', password: 'Designer de tests', importPossible: false},
//       { login: 'Testeur référent', password: 'Testeur référent', importPossible: false},
//
//       {login: 'Testeur avancé', password: 'Testeur avancé', importPossible: false},
//       {login: 'Testeur', password: 'Testeur', importPossible: false},
//       {login: 'Invité', password: 'Invité', importPossible: false},
//       { login: 'Valideur', password: 'Valideur', importPossible: false}
//     ];
//
//
//   dataSets.forEach((data, index) => runTest(data, index));
//
//   function runTest(data: Datatype, index: number) {
//     it(`Dataset ${index} - For profile : ${data.login}, should allow test case import: ${data.importPossible}`,
//       () => {
//         cy.logInAs(data.login, data.password);
//         const testCaseWorkspacePage = NavBarElement.navigateToTestCaseWorkspace();
//         testCaseWorkspacePage.tree.findRowId('NAME', 'ProjetAutom').then(projectId => {
//           testCaseWorkspacePage.tree.selectNode(projectId);
//           if (data.importPossible) {
//             const importTestCaseDialog = testCaseWorkspacePage.treeMenu.openImportTestCaseDialog();
//           } else {
//             const importExportButton = testCaseWorkspacePage.treeMenu.importExportButton();
//             const menu = importExportButton.showMenu();
//             const menuItem = menu.item(TestCaseMenuItemIds.IMPORT);
//             menuItem.assertDisabled();
//           }
//         });
//       });
//
//   }
// });

describe.skip('Import test cases', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
  });

  it('should import test case', () => {

    cy.logInAs('admin', 'admin');
    const testCaseWorkspacePage = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspacePage.tree.findRowId('NAME', 'ProjetAutom').then(projectId => {
      testCaseWorkspacePage.tree.selectNode(projectId);
      const importDialog = testCaseWorkspacePage.treeMenu.openImportTestCaseDialog();
      importDialog.checkTitle('Importer des cas de test');
      importDialog.chooseFormat('Zip');
      importDialog.chooseFormat('Excel');
      // tslint:disable-next-line:max-line-length
      importDialog.checkImportMessage('Cet import permet de créer une arborescence de cas de test multiprojets à partir d\'un unique fichier Excel. Il gère les appels de cas de test, les pas de test variabilisés et les associations aux exigences.');
      importDialog.checkDownloadTemplateLink();
      importDialog.checkFileSelectorLinkExists();
      importDialog.chooseEncodage('UTF-8');
      importDialog.chooseEncodage('Windows');
      importDialog.checkImportTestCaseDialogButtons();
      importDialog.chooseImportFile('IMPORT_ProjetAutom.xls', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      importDialog.clickImport();
      importDialog.checkConfirmationMessage('IMPORT_ProjetAutom.xls');
      // importDialog.confirmXlsImport();
       cy.get('[data-test-dialog-button-id="confirm"]').click();
    });
  });
});
