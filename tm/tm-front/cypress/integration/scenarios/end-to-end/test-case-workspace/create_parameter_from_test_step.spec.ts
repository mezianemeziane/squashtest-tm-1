import {TestStepsViewPage} from '../../../page-objects/pages/test-case-workspace/test-case/test-steps-view.page';
import {ActionStepElement} from '../../../page-objects/elements/test-steps/action-step.element';
import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {TestCaseViewPage} from '../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {TestCaseViewParametersPage} from '../../../page-objects/pages/test-case-workspace/test-case/test-case-view-parameters.page';
import {ProjectE2eCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';
/// <reference types="../support" />
/// <reference types="cypress" />
// @ts-check

describe('Add parameter from a test step', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
    cy.task('cleanDatabase');
    ProjectE2eCommands.createProject('project-1');
  });

  it('should create test case and add step with param', () => {

    cy.viewport(1200, 720);

    cy.logInAs('admin', 'admin');
    cy.goHome();
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);
      const createTestCaseDialog = testCaseWorkspace.treeMenu.openCreateTestCase();
      createTestCaseDialog.checkCreateTestCaseDialogButtons();

      createTestCaseDialog.fillFieldsWithoutCuf('TEST1', 'REF1', 'DESCRIPTION1');
      createTestCaseDialog.addWithOptions({ addAnother: false, parentRowIsClosed: false });
      testCaseWorkspace.tree.selectNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
        const testCase1ViewPage = testCaseWorkspace.tree.selectNode(testCaseId1) as TestCaseViewPage;
        testCase1ViewPage.checkNameReferenceAndDescription('TEST1', 'REF1', 'DESCRIPTION1');

        const testCase1testStepPage = testCase1ViewPage.clickAnchorLink('steps') as TestStepsViewPage;

        const actionStep1: ActionStepElement = testCase1testStepPage.getActionStepByIndex(0);
        actionStep1.fillEmptyStep('FIRST ACTION', 'action');
        actionStep1.fillEmptyStep('FIRST RESULT', 'result');
        actionStep1.clickAddAnother();

        const actionStep2: ActionStepElement = testCase1testStepPage.getActionStepByIndex(1);
        // {} is used to escape { characters
        actionStep2.fillEmptyStep('${{}param{}}', 'action');
        actionStep2.fillEmptyStep('2nd RESULT', 'result');
        actionStep2.clickAdd();

        const testCaseViewParametersPage = testCase1ViewPage.clickAnchorLink('parameters') as TestCaseViewParametersPage;
        testCaseViewParametersPage.checkExistingParam('param');
      });
    });
  });
});
