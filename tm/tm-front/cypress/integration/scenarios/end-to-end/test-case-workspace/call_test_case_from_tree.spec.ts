import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {TestCaseViewPage} from '../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {ProjectE2eCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';
import {TestStepsViewPage} from '../../../page-objects/pages/test-case-workspace/test-case/test-steps-view.page';
import {ActionStepElement} from '../../../page-objects/elements/test-steps/action-step.element';
import {toEntityRowReference} from '../../../page-objects/elements/grid/grid.element';
// tslint:disable-next-line:max-line-length
import {TestCaseViewCalledTestCasesPage} from '../../../page-objects/pages/test-case-workspace/test-case/test-case-view-called-test-cases.page';

describe('Create Test cases - prerequisite', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
    cy.task('cleanDatabase');
    ProjectE2eCommands.createProject('project-1');
  });

  it('should create 2 test cases', () => {


    cy.logInAs('admin', 'admin');

    cy.goHome();
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);

      const createTestCaseDialog = testCaseWorkspace.treeMenu.openCreateTestCase();
      createTestCaseDialog.checkCreateTestCaseDialogButtons();

      createTestCaseDialog.fillFieldsWithoutCuf('TEST1', 'REF1', 'DESCRIPTION1');
      createTestCaseDialog.clickOnAddAnotherButton();
      createTestCaseDialog.checkIfFormIsEmpty(false);

      createTestCaseDialog.fillFieldsWithoutCuf('TEST2', 'REF2', 'DESCRIPTION2');
      createTestCaseDialog.clickOnAddButton();
      testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
        const testCase1ViewPage = testCaseWorkspace.tree.selectNode(testCaseId1) as TestCaseViewPage;
        const testCase1testStepPage = testCase1ViewPage.clickAnchorLink('steps') as TestStepsViewPage;

        const actionStep1: ActionStepElement = testCase1testStepPage.getActionStepByIndex(0);
        actionStep1.fillEmptyStep('FIRST ACTION', 'action');
        actionStep1.fillEmptyStep('FIRST RESULT', 'result');
        actionStep1.clickAddAnother();

        const actionStep2: ActionStepElement = testCase1testStepPage.getActionStepByIndex(1);
        actionStep2.fillEmptyStep('2nd ACTION', 'action');
        actionStep2.fillEmptyStep('2nd RESULT', 'result');
        actionStep2.clickAdd();

      });
    });
  });

});
describe('Call test case from tree', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
  });
  it('should call a test case from tree', () => {
    cy.logInAs('admin', 'admin');

    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
        const testCase1ViewPage = testCaseWorkspace.tree.selectNode(testCaseId1) as TestCaseViewPage;
        const testCase1testStepPage = testCase1ViewPage.clickAnchorLink('steps') as TestStepsViewPage;
        testCaseWorkspace.tree.findRowId('NAME', 'TEST2').then(testCaseId2 => {
          testCaseWorkspace.tree.beginDragAndDrop(testCaseId2);
          testCase1testStepPage.enterIntoTestCase();
          const entityReference = toEntityRowReference(testCaseId1);
          const id1 = entityReference.id;
          testCase1testStepPage.dropCalledTestCaseE2E(id1);
          const actionStep3: ActionStepElement = testCase1testStepPage.getActionStepByIndex(2);
          actionStep3.checkCalledTestCaseStep('TEST2');
        });
      });
    });
  });

  it('should check called test table', () => {
    cy.logInAs('admin', 'admin');

    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'TEST2').then(testCaseId2 => {
        const testCase1ViewPage = testCaseWorkspace.tree.selectNode(testCaseId2) as TestCaseViewPage;
        const testCaseViewCalledTestCasesPage = testCase1ViewPage.clickAnchorLink('called-test-cases') as TestCaseViewCalledTestCasesPage;
        testCaseViewCalledTestCasesPage.checkExistingCallingTest('TEST1');
      });
    });
  });

  it('should call a test case from tree - cyclic calling', () => {
    cy.logInAs('admin', 'admin');

    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
        const testCase1ViewPage = testCaseWorkspace.tree.selectNode(testCaseId1) as TestCaseViewPage;
        const testCase1testStepPage = testCase1ViewPage.clickAnchorLink('steps') as TestStepsViewPage;
        testCaseWorkspace.tree.beginDragAndDrop(testCaseId1);
        testCase1testStepPage.enterIntoTestCase();
        const entityReference = toEntityRowReference(testCaseId1);
        const id1 = entityReference.id;
        testCase1testStepPage.dropCalledTestCaseE2E(id1);
        cy.get('sqtm-core-alert-dialog')
          .find('span[data-test-dialog-message]')
          .should('have.text', 'Cette action est impossible car un cycle a été détecté dans la séquence d\'appel des cas de test.');
        cy.get('button[data-test-dialog-button-id="close"]')
          .click();
      });
    });
  });
});
