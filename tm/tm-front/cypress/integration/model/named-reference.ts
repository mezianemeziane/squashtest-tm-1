export interface NamedReference {
  id: number;
  name: string;
}
