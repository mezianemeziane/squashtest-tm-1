import {CustomDashboardModel} from '../custom-report/custom-dashboard.model';
import {RequirementStatistics} from './requirement-statistics.model';


export class MilestoneRequirementDashboardModel {
  statistics: RequirementStatistics;
  dashboard: CustomDashboardModel;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}
