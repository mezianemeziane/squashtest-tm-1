import {EntityModel} from '../entity.model';
import {Milestone} from '../milestone/milestone.model';
import {VerifyingTestCase} from './verifying-test-case';
import {RequirementVersionLink} from './requirement-version.link';
import {RequirementVersionStatsBundle} from './requirement-version-stats-bundle.model';


export interface RequirementVersionModel extends EntityModel {
  name: string;
  reference: string;
  versionNumber: number;
  category: number;
  criticality: string;
  status: string;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  milestones: Milestone[];
  description: string;
  requirementId: number;
  bindableMilestones: Milestone[];
  verifyingTestCases: VerifyingTestCase[];
  requirementVersionLinks: RequirementVersionLink[];
  requirementStats: RequirementVersionStatsBundle;
  remoteReqPerimeterStatus: RemoteRequirementPerimeterStatus;
}

export type RemoteRequirementPerimeterStatus =
  'UNKNOWN' |
  'IN_CURRENT_PERIMETER' |
  'OUT_OF_CURRENT_PERIMETER'|
  'NOT_FOUND';

