export interface AdminInfoListItem {
  id: number;
  uri: string;
  code: string;
  colour: string;
  label: string;
  friendlyLabel: string;
  iconName: string;
  itemIndex: number;
  isDefault: boolean;
  system: boolean;
}

export interface AdminInfoList {
  id: number;
  code: string;
  label: string;
  description: string;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  items: AdminInfoListItem[];
}

