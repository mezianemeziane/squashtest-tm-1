export class InfoListItem {
  id: number;
  uri: string;
  code: string;
  label: string;
  friendlyLabel: string;
  iconName: string;
  itemIndex: number;
  default: boolean;
  system: boolean;
  denormalized: boolean;
}
