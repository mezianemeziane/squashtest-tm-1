import {Milestone} from './milestone.model';


export interface MilestoneAdminProjectView {

  milestone: Milestone;
  milestoneBoundToOneObjectOfProject: boolean;

}
