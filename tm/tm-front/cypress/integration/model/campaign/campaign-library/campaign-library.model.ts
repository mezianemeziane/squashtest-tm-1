import {EntityModel} from '../../entity.model';

export interface CampaignLibraryModel extends EntityModel {
  name: string;
  description: string;
}
