import {Identifier} from '../grids/data-row.type';

export interface RemoteIssue {
  id: string;
  summary: string;
  description: string;
  comment: string;
  project: RemoteProject;
  state: RemoteStatus;
  assignee: RemoteUser;
  priority: RemotePriority;
  category: RemoteCategory;
  version: RemoteVersion;
  bugtracker: string;
  hasBlankId: boolean;

  getNewKey(): string;
}

export interface RemoteAttribute {
  id: Identifier;
  name: string;
}

export type RemoteProject = RemoteAttribute;
type RemoteStatus = RemoteAttribute;
type RemoteUser = RemoteAttribute;
type RemotePriority = RemoteAttribute;
type RemoteCategory = RemoteAttribute;
type RemoteVersion = RemoteAttribute;

// BT Issues (e.g. Mantis)

export interface BTIssue extends RemoteIssue {
  project: BTProject;
  reporter: RemoteUser;
  createdOn: Date;
  status: RemoteStatus;
}

export interface BTProject extends RemoteProject {
  id: Identifier;
  name: string;
  priorities: RemotePriority[];
  versions: RemoteVersion[];
  users: RemoteUser[];
  categories: RemoteCategory[];
  defaultIssuePriority: RemotePriority;
}

export function isBTIssue(remoteIssue: RemoteIssue): remoteIssue is BTIssue {
  if (remoteIssue == null || typeof remoteIssue !== 'object') {
    return false;
  }

  return remoteIssue.hasOwnProperty('reporter');
}

// Advanced Issues (e.g. Jira)

export interface AdvancedIssue extends RemoteIssue {
  fieldValues: { [fieldId: string]: FieldValue };
  project: AdvancedProject;
  id: string;
  btName: string;
  currentScheme: string;
}

export interface AdvancedProject extends RemoteProject {
  id: string;
  name: string;
  schemes: FieldsMap;
}

export interface FieldsMap {
  [schemeName: string]: Field[];
}

export interface Field {
  id: string;
  label: string;
  possibleValues: FieldValue[];
  rendering: ValueRendering;
}

export interface FieldValue {
  id: string;
  typename: string;
  scalar: string;
  composite: FieldValue[];
  custom: unknown;
  name: string;
}

export interface ValueRendering {
  operations: string[];
  inputType: ValueRenderingInputType;
  required: boolean;
}

export interface ValueRenderingInputType {
  name: string;
  original: string;
  dataType: string;
  fieldSchemeSelector: boolean;
  configuration: any;
}

export function isAdvancedIssue(remoteIssue: RemoteIssue): remoteIssue is AdvancedIssue {
  if (remoteIssue == null || typeof remoteIssue !== 'object') {
    return false;
  }

  return Boolean(remoteIssue['currentScheme']);
}

// OSLC issues (e.g. RTC bugtracker plugin)

export interface OslcIssue extends RemoteIssue {
  fieldValues: { [fieldId: string]: FieldValue };
  project: AdvancedProject;
  id: string;
  btName: string;
  url: string;
  currentScheme: string;
  selectDialog: string;
  createDialog: string;
}

export function isOslcIssue(remoteIssue: RemoteIssue): remoteIssue is OslcIssue {
  if (remoteIssue == null || typeof remoteIssue !== 'object') {
    return false;
  }

  return remoteIssue.hasOwnProperty('createDialog')
    && remoteIssue.hasOwnProperty('selectDialog');
}
