import {BindableEntity} from '../bindable-entity.model';

export enum ReportInputType {
  TEXT = 'TEXT',
  PASSWORD = 'PASSWORD',
  LABEL = 'LABEL',
  DATE = 'DATE',
  DROPDOWN_LIST = 'DROPDOWN_LIST',
  RADIO_BUTTONS_GROUP = 'RADIO_BUTTONS_GROUP',
  CHECKBOX = 'CHECKBOX',
  CHECKBOXES_GROUP = 'CHECKBOXES_GROUP',
  TREE_PICKER = 'TREE_PICKER',
  PROJECT_PICKER = 'PROJECT_PICKER',
  MILESTONE_PICKER = 'MILESTONE_PICKER',
  TAG_PICKER = 'TAG_PICKER',
  INPUTS_GROUP = 'INPUTS_GROUP'
}

export interface ReportInput {
  name: string;
  label: string;
  type: ReportInputType;
  required: boolean;
}

export interface ReportOption {
  name: string;
  label: string;
  value: string;
  defaultSelected: boolean;
  givesAccessTo: string;
  composite: boolean;
  contentType: ReportInputType;
  bindableEntity?: BindableEntity;
  nodeTyp?: NodeType;
}

export interface ReportOptionGroup extends ReportInput {
  type: ReportInputType.CHECKBOXES_GROUP | ReportInputType.RADIO_BUTTONS_GROUP;
  options: ReportOption[];
}


export interface ReportDefinitionModel {
  id: number;
  name: string;
  description: string;
  summary: string;
  pluginNamespace: string;
  parameters: any;
}

export interface Report {
  id: string;
  label: string;
  description: string;
  category: StandardReportCategory;
  inputs: ReportInput[];
}

export interface ReportWorkbenchData {
  availableReports: Report[];
  reportDefinition: ReportDefinitionModel;
  projectId: number;
  containerId?: number;
}

export enum StandardReportCategory {
  EXECUTION_PHASE = 'EXECUTION_PHASE',
  PREPARATION_PHASE = 'PREPARATION_PHASE',
  VARIOUS = 'VARIOUS'
}

export interface ReportDefinitionViewModel extends ReportDefinitionModel {
  customReportLibraryNodeId: number;
  projectId: number;
  attributes: { [K in string]: string[] };
  reportLabel?: string;
  missingPlugin: boolean;
}

export enum NodeType {
  CAMPAIGN = 'CAMPAIGN',
  TEST_CASE = 'TEST_CASE',
  REQUIREMENT = 'REQUIREMENT'
}
