import {EntityReference} from '../entity.model';

export enum ChartType {
  PIE = 'PIE',
  BAR = 'BAR',
  CUMULATIVE = 'CUMULATIVE',
  TREND = 'TREND',
  COMPARATIVE = 'COMPARATIVE'
}

export enum ChartScopeType {
  DEFAULT = 'DEFAULT',
  PROJECTS = 'PROJECTS',
  CUSTOM = 'CUSTOM'
}


export enum ChartOperation {
  NONE = 'NONE',
  GREATER = 'GREATER',
  GREATER_EQUAL = 'GREATER_EQUAL',
  LOWER = 'LOWER',
  LOWER_EQUAL = 'LOWER_EQUAL',
  BETWEEN = 'BETWEEN',
  EQUALS = 'EQUALS',
  IN = 'IN',
  LIKE = 'LIKE',
  MATCHES = 'MATCHES',
  MIN = 'MIN',
  MAX = 'MAX',
  AVG = 'AVG',
  SUM = 'SUM',
  COUNT = 'COUNT',
  IS_NULL = 'IS_NULL',
  NOT_NULL = 'NOT_NULL',
  NOT_EQUALS = 'NOT_EQUALS',
  BY_DAY = 'BY_DAY',
  BY_WEEK = 'BY_WEEK',
  BY_MONTH = 'BY_MONTH',
  BY_YEAR = 'BY_YEAR',
  FULLTEXT = 'FULLTEXT',
}

export enum ChartColumnType {
  ENTITY = 'ENTITY',
  CUF = 'CUF',
  ATTRIBUTE = 'ATTRIBUTE',
  CALCULATED = 'CALCULATED',
}

export enum ChartDataType {

  // @formatter:off
  NUMERIC = 'NUMERIC',
  STRING = 'STRING',
  DATE = 'DATE',
  DATE_AS_STRING = 'DATE_AS_STRING',
  EXISTENCE = 'EXISTENCE',
  BOOLEAN = 'BOOLEAN',
  BOOLEAN_AS_STRING = 'BOOLEAN_AS_STRING',
  LEVEL_ENUM = 'LEVEL_ENUM',
  REQUIREMENT_STATUS = 'REQUIREMENT_STATUS',
  EXECUTION_STATUS = 'EXECUTION_STATUS',
  LIST = 'LIST',
  INFO_LIST_ITEM = 'INFO_LIST_ITEM',
  TAG = 'TAG',
  ENUM = 'ENUM',

  // type ENTITY means that columns of that datatype represent the entity itself rather than one of its attributes.
  ENTITY = 'ENTITY',
  // @formatter:on

  TEXT = 'TEXT'
}

export interface ChartColumnPrototype {
  columnType: ChartColumnType;
  label: string;
  specializedType: any;
  dataType: ChartDataType;
}


export interface MeasureColumn {
  cufId?: number;
  label: string;
  column: ChartColumnPrototype;
  operation: ChartOperation;
}

// for now AxisColumn seems to be like Measure columns.
export type AxisColumn = MeasureColumn;

export interface ChartFilter {
  id: number;
  cufId?: number;
  values: string[];
  column: ChartColumnPrototype;
  operation: ChartOperation;
}

export interface ChartDefinitionModel {
  id: number;
  name: string;
  createdOn: Date;
  createdBy: string;
  lastModifiedOn: Date;
  lastModifiedBy: string;
  customReportLibraryNodeId: number;
  projectId: number;
  type: ChartType;
  measures: MeasureColumn[];
  axis: AxisColumn[];
  filters: ChartFilter[];
  abscissa: any[][];
  scopeType: ChartScopeType;
  scope: EntityReference[];
  // ids of projects in perimeter. Don't why it's string type, however it's like that...
  projectScope: string[];
  series: { [K in string]: number[] };
}

export interface ChartBuildingBlocks {
  columnPrototypes: { [K in CustomChartEntityType]: ChartColumnPrototype[] };
  columnRoles: { [K in string]: any[] };
  dataTypes: { [K in ChartDataType]: ChartOperation[] };
}

export interface ChartWorkbenchData {
  chartBuildingBlocks: ChartBuildingBlocks;
  chartDefinition: Partial<ChartDefinitionModel>;
  projectId: number;
  containerId: number;
}

export enum CustomChartEntityType {
  TEST_CASE = 'TEST_CASE',
  REQUIREMENT = 'REQUIREMENT',
  REQUIREMENT_VERSION = 'REQUIREMENT_VERSION',
  CAMPAIGN = 'CAMPAIGN',
  ITERATION = 'ITERATION',
  TEST_SUITE = 'TEST_SUITE',
  ITEM_TEST_PLAN = 'ITEM_TEST_PLAN',
  EXECUTION = 'EXECUTION'
}
