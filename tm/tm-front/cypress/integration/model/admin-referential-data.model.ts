import {AuthenticatedUser} from './user/authenticated-user.model';
import {GlobalConfiguration, LicenseInformation} from './referential-data.model';
import {CustomField} from './customfield/customfield.model';
import {TestAutomationServerKind} from './test-automation/test-automation-server.model';
import {TemplateConfigurablePlugin} from './plugin/template-configurable-plugin.model';

export class AdminReferentialData {
  user: AuthenticatedUser;
  globalConfiguration: GlobalConfiguration;
  licenseInformation: LicenseInformation;
  customFields: CustomField[];
  availableTestAutomationServerKinds: TestAutomationServerKind[];
  canManageLocalPassword: boolean;
  templateConfigurablePlugins: TemplateConfigurablePlugin[];
}
