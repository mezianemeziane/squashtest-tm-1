/**
 * Class representing the ReferentialData fetched from server.
 */
import {Project} from './project/project.model';
import {InfoList} from './infolist/infolist.model';
import {AuthenticatedUser} from './user/authenticated-user.model';
import {CustomField} from './customfield/customfield.model';
import {BugTracker} from './bugtracker/bug-tracker.model';
import {Milestone} from './milestone/milestone.model';
import {TestAutomationServer, TestAutomationServerKind} from './test-automation/test-automation-server.model';
import {RequirementVersionLinkType} from './requirements/requirement-version-link-type.model';
import {AutomatedTestTechnology} from './automation/automated-test-technology.model';
import {WorkspaceWizard} from './workspace-wizard/workspace-wizard.model';
import {ScmServer} from './scm-server/scm-server.model';
import {TemplateConfigurablePlugin} from './plugin/template-configurable-plugin.model';


export class ReferentialData {
  projects: Project[];
  infoLists: InfoList[];
  filteredProjectIds: number[];
  projectFilterStatus: boolean;
  user: AuthenticatedUser;
  customFields: CustomField[];
  globalConfiguration: GlobalConfiguration;
  bugTrackers: BugTracker[];
  milestones: Milestone[];
  automationServers: TestAutomationServer[];
  milestoneFilterState: MilestoneFilterState;
  requirementVersionLinkTypes: RequirementVersionLinkType[];
  workspacePlugins: WorkspacePlugin[];
  workspaceWizards: WorkspaceWizard[];
  licenseInformation: LicenseInformation;
  automatedTestTechnologies: AutomatedTestTechnology[];
  availableTestAutomationServerKinds: TestAutomationServerKind[];
  scmServers: ScmServer[];
  templateConfigurablePlugins: TemplateConfigurablePlugin[];
}

export class GlobalConfiguration {
  milestoneFeatureEnabled: boolean;
  uploadFileExtensionWhitelist: string[];
  uploadFileSizeLimit: number;
}

export class LicenseInformation {
  activatedUserExcess: string;
  pluginLicenseExpiration: string;
}

export interface MilestoneFilterState {
  milestoneModeEnabled: boolean;
  selectedMilestoneId: number;
}

export interface WorkspacePlugin {
  id: string;
  workspaceId: string;
  name: string;
  iconName: string;
  tooltip: string;
  theme: string;
  url: string;
}
