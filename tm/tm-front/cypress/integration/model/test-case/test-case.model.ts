import {AutomationRequest} from './automation-request-model';
import {Parameter} from './parameter.model';
import {Dataset} from './dataset.model';
import {RequirementVersionCoverage} from './requirement-version-coverage-model';
import {DatasetParamValue} from './dataset-param-value';
import {TestStepModel} from './test-step.model';
import {Execution} from './execution.model';
import {Milestone} from '../milestone/milestone.model';
import {ExecutionStatusKeys, TestCaseImportanceKeys} from '../level-enums/level-enum';
import {CalledTestCase} from './called-test-case.model';
import {EntityModel} from '../entity.model';

export interface TestCaseModel extends EntityModel {
  name: string;
  reference: string;
  importance: TestCaseImportanceKeys;
  description: string;
  status: string;
  nature: number;
  type: number;
  importanceAuto: boolean;
  automatable: string;
  prerequisite: string;
  testSteps: TestStepModel[];
  milestones: Milestone[];
  automationRequest: AutomationRequest;
  parameters: Parameter[];
  datasets: Dataset[];
  datasetParamValues: DatasetParamValue[];
  coverages: RequirementVersionCoverage[];
  uuid: string;
  kind: TestCaseKind;
  executions: Execution[];
  nbIssues: number;
  calledTestCases: CalledTestCase[];
  lastModifiedOn: Date;
  lastModifiedBy: string;
  createdBy: string;
  createdOn: Date;
  lastExecutionStatus: ExecutionStatusKeys;
  script: string;
  actionWordLibraryActive: boolean;
  automatedTestTechnology?: number;
  automatedTestReference?: string;
  scmRepositoryId?: number;
  configuredRemoteFinalStatus?: string;
}

export type TestCaseKind = 'STANDARD' | 'GHERKIN' | 'KEYWORD';
