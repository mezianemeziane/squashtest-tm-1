
export interface TemplateConfigurablePlugin {
  id: string;
  type: string;
  name: string;
}
