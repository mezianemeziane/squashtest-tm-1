export class CustomFieldOption {

  cfId: number;
  label: string;
  position: number;
  colour: string;
  code: string;
}
