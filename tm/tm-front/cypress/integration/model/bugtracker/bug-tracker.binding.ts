export interface BugTrackerBinding {
  id: number;
  projectId: number;
  bugTrackerId: number;
}
