// custom field values from server

import {InputType} from '../customfield/customfield.model';

export interface DenormalizedCustomFieldValueModel {
  id: number;
  denormalizedFieldHolderId: number;
  value: string;
  label: string;
  fieldType: string;
  inputType: InputType;
}
