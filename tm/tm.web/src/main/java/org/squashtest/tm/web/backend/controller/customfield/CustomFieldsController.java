/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.customfield;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.customfield.CustomFieldOption;
import org.squashtest.tm.exception.DomainException;
import org.squashtest.tm.exception.RequiredFieldException;
import org.squashtest.tm.service.customfield.CustomFieldManagerService;
import org.squashtest.tm.service.display.custom.field.CustomFieldDisplayService;
import org.squashtest.tm.service.internal.display.dto.CustomFieldDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.web.backend.controller.form.model.CustomFieldFormModel;
import javax.validation.Valid;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/backend/custom-fields")
public class CustomFieldsController {

	private static final String CUSTOM_FIELDS_URL = "/{customFieldIds}";

	CustomFieldDisplayService customFieldDisplayService;
	CustomFieldManagerService customFieldManagerService;

	public CustomFieldsController(CustomFieldDisplayService customFieldDisplayService,
								  CustomFieldManagerService customFieldManagerService) {
		this.customFieldDisplayService = customFieldDisplayService;
		this.customFieldManagerService = customFieldManagerService;
	}

	@ResponseBody
	@PostMapping
	public GridResponse getAllCustomFields(@RequestBody GridRequest request) {
		return customFieldDisplayService.findAll(request);
	}

	@ResponseBody
	@GetMapping
	public Map<String, List<CustomFieldDto>> getAllNamedReferences() {
		return Collections.singletonMap("customFields", customFieldDisplayService.findAllWithPossibleValues());
	}

	@RequestMapping(value = "/new", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public Map<String, Object> createCustomField(@RequestBody CustomFieldFormModel customFieldFormModel) {
		Map<String, Object> response = new HashMap<>();
		CustomField customField = customFieldFormModel.getCustomField();
		customFieldManagerService.persist(customField);
		response.put("id", customField.getId());
		return response;
	}

	@RequestMapping(value = "/{cufId}/label", method = RequestMethod.POST)
	@ResponseBody
	public void changeLabel(@PathVariable long cufId, @RequestBody CustomFieldsController.CustomFieldPatch patch) {
		customFieldManagerService.changeLabel(cufId, patch.getLabel());
	}

	@RequestMapping(value = "/{cufId}/name", method = RequestMethod.POST)
	@ResponseBody
	public void changeName(@PathVariable long cufId, @RequestBody CustomFieldsController.CustomFieldPatch patch) {
		customFieldManagerService.changeName(cufId, patch.getName());
	}

	@RequestMapping(value = "/{cufId}/code", method = RequestMethod.POST)
	@ResponseBody
	public void changeCode(@PathVariable long cufId, @RequestBody CustomFieldsController.CustomFieldPatch patch) {
		customFieldManagerService.changeCode(cufId, patch.getCode());
	}

	@RequestMapping(value = "/{cufId}/optional", method = RequestMethod.POST)
	@ResponseBody
	public void changeOptional(@PathVariable long cufId, @RequestBody CustomFieldsController.CustomFieldPatch patch) {
		customFieldManagerService.changeOptional(cufId, patch.isOptional());
	}

	@RequestMapping(value = "/{cufId}/default-value", method = RequestMethod.POST)
	@ResponseBody
	public void changeDefaultValue(@PathVariable long cufId, @RequestBody CustomFieldsController.CustomFieldPatch patch) {
		customFieldManagerService.changeDefaultValue(cufId, patch.getDefaultValue());
	}

	@RequestMapping(value = "/{cufId}/numeric-default-value", method = RequestMethod.POST)
	@ResponseBody
	public void changeNumericDefaultValue(@PathVariable long cufId, @RequestBody CustomFieldsController.CustomFieldPatch patch) {
		Double value = patch.getNumericDefaultValue();
		customFieldManagerService.changeDefaultValue(cufId, value == null ? null : value.toString());
	}

	@RequestMapping(value = "/{cufId}/large-default-value", method = RequestMethod.POST)
	@ResponseBody
	public void changeLargeDefaultValue(@PathVariable long cufId, @RequestBody CustomFieldsController.CustomFieldPatch patch) {
		customFieldManagerService.changeDefaultValue(cufId, patch.getLargeDefaultValue());
	}

	@ResponseBody
	@RequestMapping(value = CUSTOM_FIELDS_URL, method = RequestMethod.DELETE)
	public void deleteCustomField(@PathVariable("customFieldIds") List<Long> customFieldIds) {
		customFieldManagerService.deleteCustomField(customFieldIds);
	}

	@RequestMapping(value = "/{cufId}/options/new", method = RequestMethod.POST)
	@ResponseBody
	public CustomFieldDto addOption(@PathVariable long cufId,
						  @Valid @RequestBody CustomFieldOption option) {
		try {
			customFieldManagerService.addOption(cufId, option);
		} catch (DomainException e) {
			e.setObjectName("new-cuf-option");
			throw e;
		}
		return this.customFieldDisplayService.getCustomFieldView(cufId);
	}

	@RequestMapping(value = "/{cufId}/options/remove", method = RequestMethod.POST)
	@ResponseBody
	public void removeOption(@PathVariable long cufId, @RequestBody Map<String, List<String>> request) {

		customFieldManagerService.removeOptions(cufId, request.get("optionLabels"));
	}

	@RequestMapping(value = "/{cufId}/options/label", method = RequestMethod.POST)
	@ResponseBody
	public void changeOptionLabel(@PathVariable long cufId,
								  @RequestBody CustomFieldsController.CustomFieldOptionPatch patch) {
		if (patch.getNewLabel() == null || patch.getNewLabel().trim().isEmpty()) {
			throw new RequiredFieldException("label");
		}

		try {
			customFieldManagerService.changeOptionLabel(cufId, patch.getCurrentLabel(), patch.getNewLabel());
		} catch (DomainException e) {
			e.setObjectName("rename-cuf-option");
			throw e;
		}
	}

	@RequestMapping(value = "/{cufId}/options/code", method = RequestMethod.POST)
	@ResponseBody
	public void changeOptionCode(@PathVariable long cufId,
								  @RequestBody CustomFieldsController.CustomFieldOptionPatch patch) {

		if (patch.getCode() == null || patch.getCode().trim().isEmpty()) {
			throw new RequiredFieldException("code");
		}

		try {
			customFieldManagerService.changeOptionCode(cufId, patch.getCurrentLabel(), patch.getCode());
		} catch (DomainException e) {
			e.setObjectName("change-cuf-option-code");
			throw e;
		}
	}

	@RequestMapping(value = "/{cufId}/options/color", method = RequestMethod.POST)
	@ResponseBody
	public void changeOptionColor(@PathVariable long cufId,
								 @RequestBody CustomFieldsController.CustomFieldOptionPatch patch) {
		try {
			customFieldManagerService.changeOptionColour(cufId, patch.getCurrentLabel(), patch.getColor());
		} catch (DomainException e) {
			e.setObjectName("change-cuf-option-colour");
			throw e;
		}
	}

	@RequestMapping(value = "/{cufId}/options/positions", method = RequestMethod.POST)
	@ResponseBody
	public CustomFieldDto changeOptionsPositions(@PathVariable long cufId, @RequestBody ReorderOptionsRequestBody requestBody) {
		customFieldManagerService.changeOptionsPositions(cufId, requestBody.position, requestBody.getLabels());
		return customFieldDisplayService.getCustomFieldView(cufId);
	}

	static class CustomFieldPatch {
		private String name;
		private String label;
		private String code;
		private boolean optional;
		private String defaultValue;
		private Double numericDefaultValue;
		private String largeDefaultValue;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getLabel() { return label; }

		public void setLabel(String label) { this.label = label; }

		public String getDefaultValue() { return defaultValue; }

		public void setDefaultValue(String defaultValue) { this.defaultValue = defaultValue; }

		public String getCode() { return code; }

		public void setCode(String code) { this.code = code; }

		public boolean isOptional() { return optional; }

		public void setOptional(boolean optional) {	this.optional = optional; }

		public Double getNumericDefaultValue() { return numericDefaultValue; }

		public void setNumericDefaultValue(Double numericDefaultValue) { this.numericDefaultValue = numericDefaultValue; }

		public String getLargeDefaultValue() { return largeDefaultValue; }

		public void setLargeDefaultValue(String largeDefaultValue) { this.largeDefaultValue = largeDefaultValue; }
	}

	static class CustomFieldOptionPatch {
		private String currentLabel;
		private String newLabel;
		private String code;
		private String color;

		public String getCurrentLabel() { return currentLabel; }

		public void setCurrentLabel(String currentLabel) { this.currentLabel = currentLabel; }

		public String getNewLabel() { return newLabel; }

		public void setNewLabel(String newLabel) { this.newLabel = newLabel; }

		public String getCode() { return code; }

		public void setCode(String code) { this.code = code;}

		public String getColor() { return color; }

		public void setColor(String color) { this.color = color; }
	}

	static class ReorderOptionsRequestBody {
		private List<String> labels;
		private Integer position;

		public Integer getPosition() {
			return position;
		}

		public void setPosition(Integer position) {
			this.position = position;
		}

		public List<String> getLabels() {
			return labels;
		}

		public void setLabels(List<String> labels) {
			this.labels = labels;
		}
	}
}
