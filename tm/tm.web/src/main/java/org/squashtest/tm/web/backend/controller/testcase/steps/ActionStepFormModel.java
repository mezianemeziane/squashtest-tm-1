/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase.steps;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.service.internal.dto.RawValueModel;
import org.squashtest.tm.service.internal.dto.RawValueModel.RawValueModelMap;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

// XSS OK
public class ActionStepFormModel {
	private String action = "";

	private String expectedResult = "";

	private int index;

	private RawValueModelMap customFields = new RawValueModelMap();

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getExpectedResult() {
		return expectedResult;
	}

	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public RawValueModelMap getCustomFields() {
		return customFields;
	}

	public void setCustomFields(RawValueModelMap customFields) {
		this.customFields = customFields;
	}

	public ActionTestStep getActionTestStep() {
		ActionTestStep newStep = new ActionTestStep();
		newStep.setAction(HTMLCleanupUtils.cleanHtml(action));
		newStep.setExpectedResult(HTMLCleanupUtils.cleanHtml(expectedResult));
		return newStep;
	}

	@JsonIgnore
	public Map<Long, RawValue> getCufs() {
		Map<Long, RawValue> cufs = new HashMap<>(customFields.size());
		for (Map.Entry<Long, RawValueModel> entry : customFields.entrySet()) {
			cufs.put(entry.getKey(), entry.getValue().toRawValue());
		}
		return cufs;
	}

}
