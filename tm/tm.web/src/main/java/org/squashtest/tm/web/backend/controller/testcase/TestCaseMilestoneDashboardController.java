/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.users.PartyPreference;
import org.squashtest.tm.domain.users.preferences.CorePartyPreference;
import org.squashtest.tm.service.customreport.CustomReportDashboardService;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportDashboard;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.statistics.testcase.TestCaseStatisticsBundle;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;
import org.squashtest.tm.service.user.PartyPreferenceService;
import org.squashtest.tm.web.backend.model.builder.JsonCustomReportDashboardBuilder;

import javax.inject.Named;
import javax.inject.Provider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/backend/test-case-milestone-dashboard")
public class TestCaseMilestoneDashboardController {

	private final TestCaseLibraryNavigationService testCaseLibraryNavigationService;
	private final ActiveMilestoneHolder activeMilestoneHolder;
	private final CustomReportLibraryNodeService customReportLibraryNodeService;
	@Named("customReport.dashboardBuilder")
	private final Provider<JsonCustomReportDashboardBuilder> builderProvider;
	private final CustomReportDashboardService customReportDashboardService;
	private final PartyPreferenceService partyPreferenceService;

	public TestCaseMilestoneDashboardController(
		TestCaseLibraryNavigationService testCaseLibraryNavigationService,
		ActiveMilestoneHolder activeMilestoneHolder,
		CustomReportLibraryNodeService customReportLibraryNodeService,
		Provider<JsonCustomReportDashboardBuilder> builderProvider,
		CustomReportDashboardService customReportDashboardService,
		PartyPreferenceService partyPreferenceService) {
		this.testCaseLibraryNavigationService = testCaseLibraryNavigationService;
		this.activeMilestoneHolder = activeMilestoneHolder;
		this.customReportLibraryNodeService = customReportLibraryNodeService;
		this.builderProvider = builderProvider;
		this.customReportDashboardService = customReportDashboardService;
		this.partyPreferenceService = partyPreferenceService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public TestCaseMilestoneDashboard getDashboardByMilestone(Locale locale) {
		TestCaseMilestoneDashboard testCaseMilestoneDashboard = new TestCaseMilestoneDashboard();
		boolean canShowDashboardInWorkspace = customReportDashboardService.canShowDashboardInWorkspace(Workspace.TEST_CASE);
		boolean shouldShowFavoriteDashboardInWorkspace = customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.TEST_CASE);
		testCaseMilestoneDashboard.setCanShowFavoriteDashboard(canShowDashboardInWorkspace);
		testCaseMilestoneDashboard.setShouldShowFavoriteDashboard(shouldShowFavoriteDashboardInWorkspace);
		if (shouldShowFavoriteDashboardInWorkspace) {
			if (canShowDashboardInWorkspace) {
				PartyPreference preference = partyPreferenceService
					.findPreferenceForCurrentUser(CorePartyPreference.FAVORITE_DASHBOARD_TEST_CASE.getPreferenceKey());
				Long dashboardId = Long.valueOf(preference.getPreferenceValue());
				CustomReportDashboard dashboard = customReportLibraryNodeService.findCustomReportDashboardById(dashboardId);
				JsonCustomReportDashboard jsonDashboard = builderProvider.get()
					.build(dashboardId, dashboard, locale, Collections.emptyList(), true, Workspace.REQUIREMENT);
				testCaseMilestoneDashboard.setDashboard(jsonDashboard);
				testCaseMilestoneDashboard.setFavoriteDashboardId(dashboardId);
			}
		} else {
			Milestone activeMilestone = activeMilestoneHolder.getActiveMilestone().orElse(null);
			List<Long> nodeIds = testCaseLibraryNavigationService.findAllTestCasesLibraryNodeForMilestone(activeMilestone);

			testCaseMilestoneDashboard.setStatistics(testCaseLibraryNavigationService.getStatisticsForSelection(new ArrayList<Long>(), nodeIds));
		}

		return testCaseMilestoneDashboard;
	}

	static class TestCaseMilestoneDashboard {
		private TestCaseStatisticsBundle statistics;
		private JsonCustomReportDashboard dashboard;
		private boolean shouldShowFavoriteDashboard;
		private boolean canShowFavoriteDashboard;
		private Long favoriteDashboardId;

		public TestCaseStatisticsBundle getStatistics() {
			return statistics;
		}

		public void setStatistics(TestCaseStatisticsBundle statistics) {
			this.statistics = statistics;
		}

		public JsonCustomReportDashboard getDashboard() {
			return dashboard;
		}

		public void setDashboard(JsonCustomReportDashboard dashboard) {
			this.dashboard = dashboard;
		}

		public boolean isShouldShowFavoriteDashboard() {
			return shouldShowFavoriteDashboard;
		}

		public void setShouldShowFavoriteDashboard(boolean shouldShowFavoriteDashboard) {
			this.shouldShowFavoriteDashboard = shouldShowFavoriteDashboard;
		}

		public boolean isCanShowFavoriteDashboard() {
			return canShowFavoriteDashboard;
		}

		public void setCanShowFavoriteDashboard(boolean canShowFavoriteDashboard) {
			this.canShowFavoriteDashboard = canShowFavoriteDashboard;
		}

		public Long getFavoriteDashboardId() {
			return favoriteDashboardId;
		}

		public void setFavoriteDashboardId(Long favoriteDashboardId) {
			this.favoriteDashboardId = favoriteDashboardId;
		}
	}
}
