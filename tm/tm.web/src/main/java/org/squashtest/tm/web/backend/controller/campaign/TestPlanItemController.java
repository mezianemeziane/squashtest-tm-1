/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.campaign.CampaignTestPlanManagerService;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;

import java.util.List;

@RestController
@RequestMapping("backend/test-plan-item")
public class TestPlanItemController {

	private final IterationTestPlanManagerService iterationTestPlanManagerService;

	private final CampaignTestPlanManagerService campaignTestPlanManagerService;

	public TestPlanItemController(IterationTestPlanManagerService iterationTestPlanManagerService,
								  CampaignTestPlanManagerService campaignTestPlanManagerService) {
		this.iterationTestPlanManagerService = iterationTestPlanManagerService;
		this.campaignTestPlanManagerService = campaignTestPlanManagerService;
	}

	@ResponseBody
	@PostMapping(value = "{testPlanItemId}/dataset")
	public Long setDatasetToIterationTestPlanItem(@PathVariable long testPlanItemId, @RequestBody IterationTestPlanManagerController.IterationTestPlanItemPatch patch) {
		iterationTestPlanManagerService.changeDataset(testPlanItemId, patch.getDatasetId());
		return patch.getDatasetId();
	}

	@ResponseBody
	@PostMapping(value = "{testPlanItemId}/dataset-to-ctpi")
	public Long setDatasetToIterationTestPlanItem(@PathVariable long testPlanItemId, @RequestBody CampaignTestPlanManagerController.CampaignTestPlanItemPatch patch) {
		campaignTestPlanManagerService.changeDataset(testPlanItemId, patch.getDatasetId());
		return patch.getDatasetId();
	}

	@ResponseBody
	@PostMapping(value = "/{testPlanItemsIds}/assign-user")
	public Long assignUserToIterationTestPlanItem(@PathVariable List<Long> testPlanItemsIds, @RequestBody IterationTestPlanManagerController.IterationTestPlanItemPatch patch) {
		iterationTestPlanManagerService.assignUserToTestPlanItems(testPlanItemsIds, patch.getAssignee());
		return patch.getAssignee();
	}

	@ResponseBody
	@PostMapping(value = "/{testPlanItemsIds}/execution-status")
	public void updateIterationTestPlanItemsStatus(@PathVariable List<Long> testPlanItemsIds, @RequestBody IterationTestPlanManagerController.IterationTestPlanItemPatch patch) {
		iterationTestPlanManagerService.forceExecutionStatus(testPlanItemsIds, patch.getExecutionStatus());
	}

	@ResponseBody
	@PostMapping(value = "/{testPlanItemsIds}/assign-user-to-ctpi")
	public Long assignUserToCampaignTestPlanItem(@PathVariable List<Long> testPlanItemsIds, @RequestBody CampaignTestPlanManagerController.CampaignTestPlanItemPatch patch) {
		campaignTestPlanManagerService.assignUserToTestPlanItems(testPlanItemsIds, patch.getAssignee());
		return patch.getAssignee();
	}
}
