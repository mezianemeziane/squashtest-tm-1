/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.referential;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.display.referential.ReferentialDataProvider;
import org.squashtest.tm.service.internal.display.referential.AdminReferentialData;
import org.squashtest.tm.service.internal.display.referential.ReferentialData;
import org.squashtest.tm.service.internal.display.referential.WorkspacePluginDto;
import org.squashtest.tm.service.internal.display.referential.WorkspaceWizardDto;
import org.squashtest.tm.service.workspace.WorkspacePluginManager;
import org.squashtest.tm.web.backend.manager.wizard.WorkspaceWizardManager;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("backend/referential")
public class ReferentialDataController {

    private final ReferentialDataProvider referentialDataProvider;
    private final WorkspacePluginManager workspacePluginManager;
    private final WorkspaceWizardManager workspaceWizardManager;

    public ReferentialDataController(ReferentialDataProvider referentialDataProvider,
                                     WorkspacePluginManager workspacePluginManager,
                                     WorkspaceWizardManager workspaceWizardManager) {
        this.referentialDataProvider = referentialDataProvider;
        this.workspacePluginManager = workspacePluginManager;
        this.workspaceWizardManager = workspaceWizardManager;
    }

    @GetMapping
    public ReferentialData get() {
        ReferentialData referentialData = referentialDataProvider.findReferentialData();
        appendWorkspacePlugins(referentialData);
        appendWorkspaceWizards(referentialData);

        return referentialData;
    }

    private void appendWorkspacePlugins(ReferentialData referentialData) {
        List<WorkspacePluginDto> plugins = this.workspacePluginManager.getAllAuthorized()
                .stream()
                .map(WorkspacePluginDto::fromWorkspacePlugin)
                .collect(Collectors.toList());

        referentialData.setWorkspacePlugins(plugins);
    }

    private void appendWorkspaceWizards(ReferentialData referentialData) {
        List<WorkspaceWizardDto> workspaceWizards = workspaceWizardManager.findAll()
                .stream()
                .map(WorkspaceWizardDto::fromWorkspaceWizard)
                .collect(Collectors.toList());

        referentialData.setWorkspaceWizards(workspaceWizards);
    }

    @GetMapping(path = "/admin")
    public AdminReferentialData getAdminReferentialData() {
        return this.referentialDataProvider.findAdminReferentialData();
    }
}
