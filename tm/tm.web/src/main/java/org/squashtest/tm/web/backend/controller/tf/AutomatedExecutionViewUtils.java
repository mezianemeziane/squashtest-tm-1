/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.tf;

import org.squashtest.tm.core.foundation.lang.MathsUtils;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteWithSquashAutomAutomatedITPIs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class AutomatedExecutionViewUtils {

	private AutomatedExecutionViewUtils() {
		super();
	}

	public static AutomatedSuiteOverview buildExecInfo(AutomatedSuiteWithSquashAutomAutomatedITPIs suite) {
		Collection<AutomatedExecutionExtender> executions = suite.getSuite().getExecutionExtenders();
		Collection<AutomatedExecutionExtender> filteredExecutions = executions.stream().filter(exec -> exec.getAutomatedTest() != null).collect(Collectors.toList());
		Collection<IterationTestPlanItem> items = suite.getSquashAutomAutomatedItems();
		List<ExecutionAutoView> executionsViews = new ArrayList<>(filteredExecutions.size());
		List<ItemAutoView> itemsViews = new ArrayList<>(items.size());
		int totalExec = filteredExecutions.size();
		int totalTerminated = 0;
		for (AutomatedExecutionExtender autoExec : filteredExecutions) {
			Execution execution = autoExec.getExecution();
			if (execution.getExecutionStatus().isTerminatedStatus()) {
				totalTerminated++;
			}
			ExecutionAutoView execView = convertAutoExecToExecOverview(autoExec);
			executionsViews.add(execView);
		}
		for (IterationTestPlanItem item : items) {
			ItemAutoView itemView = translateItemInView(item);
			itemsViews.add(itemView);
		}
		int percentage = percentProgression(totalTerminated, totalExec);
		return new AutomatedSuiteOverview(percentage, suite.getSuite().getId(), executionsViews, itemsViews);

	}

	public static AutomatedSuiteOverview buildExecInfo(AutomatedSuite suite) {
		Collection<AutomatedExecutionExtender> executions = suite.getExecutionExtenders();
		Collection<AutomatedExecutionExtender> filteredExecutions = executions.stream().filter(exec -> exec.getAutomatedTest() != null).collect(Collectors.toList());
		List<ExecutionAutoView> executionsViews = new ArrayList<>(filteredExecutions.size());
		int totalExec = filteredExecutions.size();
		int totalTerminated = 0;
		for (AutomatedExecutionExtender autoExec : filteredExecutions) {
			Execution execution = autoExec.getExecution();
			if (execution.getExecutionStatus().isTerminatedStatus()) {
				totalTerminated++;
			}
			ExecutionAutoView execView = convertAutoExecToExecOverview(autoExec);
			executionsViews.add(execView);
		}
		int percentage = percentProgression(totalTerminated, totalExec);
		return new AutomatedSuiteOverview(percentage, suite.getId(), executionsViews);

	}

	private static int percentProgression(int totalTerminated, int totalExec) {
		if(totalExec == 0) {
			return 100;
		}

		return MathsUtils.percent(totalTerminated, totalExec);
	}

	public static ExecutionAutoView convertAutoExecToExecOverview(AutomatedExecutionExtender autoExec) {
		ExecutionAutoView execView = new ExecutionAutoView(
			autoExec.getExecution().getId(),
			autoExec.getExecution().getName(),
			autoExec.getExecution().getExecutionStatus(),
			autoExec.getNodeName(),
			autoExec.getAutomatedProject().getLabel());
		return execView;
	}

	public static ItemAutoView translateItemInView(IterationTestPlanItem autoItem) {
		ItemAutoView itemAutoView = new ItemAutoView();

		itemAutoView.id = autoItem.getId();
		itemAutoView.name = autoItem.getReferencedTestCase().getName();
		itemAutoView.automatedServerName = autoItem.getReferencedTestCase().getProject().getTestAutomationServer().getName();

		return itemAutoView;
	}

	public static class AutomatedSuiteOverview {
		private String suiteId;
		private List<ExecutionAutoView> executions;
		private List<ItemAutoView> items;
		private int percentage = 0;

		public AutomatedSuiteOverview(int percentage, String suiteId, List<ExecutionAutoView> executions, List<ItemAutoView> items) {
			this.suiteId = suiteId;
			this.executions = executions;
			this.items = items;
			this.percentage = percentage;

		}

		public AutomatedSuiteOverview(int percentage, String suiteId, List<ExecutionAutoView> executions) {
			this.suiteId = suiteId;
			this.executions = executions;
			this.items = Collections.EMPTY_LIST;
			this.percentage = percentage;

		}

		public String getSuiteId() {
			return suiteId;
		}

		public void setSuiteId(String suiteId) {
			this.suiteId = suiteId;
		}

		public List<ExecutionAutoView> getExecutions() {
			return executions;
		}

		public void setExecutions(List<ExecutionAutoView> executions) {
			this.executions = executions;
		}

		public int getPercentage() {
			return percentage;
		}

		public void setPercentage(int percentage) {
			this.percentage = percentage;
		}

		public List<ItemAutoView> getItems() {
			return items;
		}

		public void setItems(List<ItemAutoView> items) {
			this.items = items;
		}
	}

	public final static class ExecutionAutoView {

		private Long id;
		private String name;
		private ExecutionStatus status;
		private String node;
		private String automatedProject;

		public ExecutionAutoView(Long id, String name, ExecutionStatus status, String node, String automatedProject) {
			this.id = id;
			this.name = name;
			this.status = status;
			this.node = node;
			this.automatedProject = automatedProject;
		}

		public Long getId() {
			return id;
		}


		public String getName() {
			return name;
		}


		public ExecutionStatus getStatus() {
			return status;
		}

		/**
		 * @return the node
		 */
		public String getNode() {
			return node;
		}

		/**
		 * @return the automatedProject
		 */
		public String getAutomatedProject() {
			return automatedProject;
		}

	}

	public final static class ItemAutoView {

		private Long id;
		private String name;
		private String automatedServerName;

		private ItemAutoView() {
			super();
		}

		public Long getId() {
			return id;
		}


		public String getName() {
			return name;
		}

		public String getAutomatedServerName() {
			return automatedServerName;
		}
	}

}
