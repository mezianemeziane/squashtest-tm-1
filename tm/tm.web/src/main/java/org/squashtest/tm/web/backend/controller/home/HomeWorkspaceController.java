/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.home;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.internal.display.home.HomeWorkspaceData;
import org.squashtest.tm.service.internal.display.home.HomeWorkspaceDisplayService;
import org.squashtest.tm.web.backend.model.builder.JsonCustomReportDashboardBuilder;

import javax.inject.Named;
import javax.inject.Provider;
import java.util.Locale;

@RestController
@RequestMapping("backend/home-workspace")
public class HomeWorkspaceController {

	private final CustomReportLibraryNodeService customReportLibraryNodeService;

	private final Provider<JsonCustomReportDashboardBuilder> builderProvider;

	private final HomeWorkspaceDisplayService homeWorkspaceDisplayService;

	public HomeWorkspaceController(HomeWorkspaceDisplayService homeWorkspaceDisplayService,
								   @Named("customReport.dashboardBuilder") Provider<JsonCustomReportDashboardBuilder> builderProvider,
								   CustomReportLibraryNodeService customReportLibraryNodeService) {
		this.homeWorkspaceDisplayService = homeWorkspaceDisplayService;
		this.builderProvider = builderProvider;
		this.customReportLibraryNodeService = customReportLibraryNodeService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public HomeWorkspaceData loadHomeWorkspace(Locale locale) {
		HomeWorkspaceData homeWorkspaceData = this.homeWorkspaceDisplayService.getWorkspacePayload();
		Long dashboardId = homeWorkspaceData.getFavoriteDashboardId();
		if (dashboardId != null) {

			CustomReportDashboard dashboard = customReportLibraryNodeService.findCustomReportDashboardById(homeWorkspaceData.getFavoriteDashboardId());
			homeWorkspaceData.setDashboard(builderProvider.get().build(dashboardId, dashboard, locale));
		}
		return homeWorkspaceData;
	}
}
