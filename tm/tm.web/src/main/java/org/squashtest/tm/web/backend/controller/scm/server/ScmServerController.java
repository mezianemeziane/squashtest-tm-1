/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.scm.server;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.csp.core.bugtracker.net.AuthenticationException;
import org.squashtest.tm.core.foundation.exception.InvalidUrlException;
import org.squashtest.tm.core.foundation.lang.UrlUtils;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.domain.scm.ScmServer;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.exception.WrongUrlException;
import org.squashtest.tm.service.display.scm.server.ScmServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.ScmServerDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.scmserver.ScmConnectorRegistry;
import org.squashtest.tm.service.scmserver.ScmRepositoryManagerService;
import org.squashtest.tm.service.scmserver.ScmServerManagerService;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.thirdpartyserver.ThirdPartyServerCredentialsService;
import org.squashtest.tm.web.backend.controller.form.model.ScmServerFormModel;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/backend/scm-servers")
public class ScmServerController {

	private ScmServerManagerService scmServerManagerService;

	private ScmServerDisplayService scmServerDisplayService;

	private ScmConnectorRegistry scmConnectorRegistry;

	private ScmRepositoryManagerService scmRepositoryManager;


	private ThirdPartyServerCredentialsService thirdPartyServerCredentialsService;


	@Inject
	ScmServerController(ScmServerDisplayService scmServerDisplayService,
						ScmServerManagerService scmServerManagerService,
						ScmConnectorRegistry scmConnectorRegistry,
						ScmRepositoryManagerService scmRepositoryManager,
						ThirdPartyServerCredentialsService thirdPartyServerCredentialsService) {
		this.scmServerDisplayService = scmServerDisplayService;
		this.scmServerManagerService = scmServerManagerService;
		this.scmConnectorRegistry = scmConnectorRegistry;
		this.scmRepositoryManager = scmRepositoryManager;
		this.thirdPartyServerCredentialsService = thirdPartyServerCredentialsService;
	}

	@ResponseBody
	@GetMapping
	public Map<String, List<ScmServerDto>> getAllServersAndRepositories() {
		return Collections.singletonMap("scmServers", scmServerDisplayService.getAllServersAndRepositories());
	}

	@ResponseBody
	@PostMapping
	public GridResponse getAllScmServers(@RequestBody GridRequest request) {
		return scmServerDisplayService.findAll(request);
	}

	@ResponseBody
	@PostMapping(value="/new")
	public Map<String,Object> addScmServer(@Valid @RequestBody ScmServerFormModel scmServerFormModel) {
		Map<String, Object> tempReturn = new HashMap<>();
		try {
			ScmServer scmServer = scmServerFormModel.getScmServer();
			UrlUtils.toUrl(scmServerFormModel.getUrl());
			scmServerManagerService.createNewScmServer(scmServer);
			tempReturn.put("id", scmServer.getId());

		} catch (InvalidUrlException iue) {
			throw new WrongUrlException("url", iue);
		}

		return tempReturn;
	}

	@ResponseBody
	@GetMapping(value="/get-scm-server-kinds")
	public Map<String, Set<String>> getScmServerKinds() {
		Map<String, Set<String>> response = new HashMap<>();

		response.put("scmServerKinds",  scmConnectorRegistry.getRegisteredScmKinds());

		return response;
	}

	@RequestMapping(value = "/{scmServerIds}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteScmServers(@PathVariable List<Long> scmServerIds) {
		scmServerManagerService.deleteScmServers(scmServerIds);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{scmServerId}/repositories")
	@ResponseBody
	public Map<String, List<NamedReference>> getScmRepositories(@PathVariable long scmServerId) {
		List<ScmRepository> repositories = scmRepositoryManager.findByScmServerOrderByPath(scmServerId);

		List<NamedReference> namedReferences = repositories.stream()
			.map(scmRepository -> new NamedReference(scmRepository.getId(), scmRepository.getName()))
			.collect(Collectors.toList());

		return Collections.singletonMap("repositories", namedReferences);
	}

	@RequestMapping(value = "/{scmServerId}/name", method = RequestMethod.POST)
	@ResponseBody
	public void updateName(@PathVariable long scmServerId, @RequestBody ScmServerPatch patch) {
		scmServerManagerService.updateName(scmServerId, patch.getName());
	}

	@RequestMapping(value = "/{scmServerId}/url", method = RequestMethod.POST)
	@ResponseBody
	public void updateUrl(@PathVariable long scmServerId, @RequestBody ScmServerPatch patch) {
		checkUrl(patch.getUrl());
		scmServerManagerService.updateUrl(scmServerId, patch.getUrl());
	}

	@RequestMapping(value = "/{scmServerId}/committer-mail", method = RequestMethod.POST)
	@ResponseBody
	public void updateCommitterMail(@PathVariable long scmServerId, @RequestBody ScmServerPatch patch) {
		scmServerManagerService.updateCommitterMail(scmServerId, patch.getCommitterMail());
	}

	@RequestMapping(value = "/{scmServerId}/credentials", method = RequestMethod.POST)
	@ResponseBody
	public void storeCredentials(@PathVariable long scmServerId, @RequestBody ManageableCredentials credentials){
		thirdPartyServerCredentialsService.storeCredentials(scmServerId, credentials);
	}

	@PostMapping(value = "{scmServerId}/auth-protocol")
	@ResponseBody
	public void changeAuthProtocol(@PathVariable long scmServerId,
								   @RequestBody ScmServerPatch patch) {
		try {
			AuthenticationProtocol protocol = Enum.valueOf(AuthenticationProtocol.class, patch.getAuthProtocol());
			thirdPartyServerCredentialsService.changeAuthenticationProtocol(scmServerId, protocol);
		} catch (IllegalArgumentException e) {
			throw new AuthenticationException("Unknown authentication protocol " + patch.getAuthProtocol(), e);
		}
	}

	private void checkUrl(String url) {
		try {
			UrlUtils.toUrl(url);
		} catch (InvalidUrlException iue) {
			throw new WrongUrlException("url", iue);
		}
	}

	public static class ScmServerPatch {
		String name;
		String url;
		String committerMail;
		private String authProtocol;

		public String getName() { return name; }

		public void setName(String name) { this.name = name; }

		public String getUrl() { return url; }

		public void setUrl(String url) { this.url = url; }

		public String getCommitterMail() { return committerMail; }

		public void setCommitterMail(String committerMail) { this.committerMail = committerMail; }

		public String getAuthProtocol() {
			return authProtocol;
		}

		public void setAuthProtocol(String authProtocol) {
			this.authProtocol = authProtocol;
		}
	}
}
