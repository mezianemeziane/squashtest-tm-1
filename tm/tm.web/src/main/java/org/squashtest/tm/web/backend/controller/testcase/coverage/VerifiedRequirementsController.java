/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase.coverage;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.squashtest.tm.exception.requirement.VerifiedRequirementException;
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService;
import org.squashtest.tm.service.internal.display.dto.RequirementVersionCoverageDto;
import org.squashtest.tm.service.requirement.VerifiedRequirementsManagerService;
import org.squashtest.tm.web.backend.helper.VerifiedRequirementActionSummaryBuilder;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Controller for managing coverages in test cases and test steps.
 *
 * @author Julien Thebault
 */
@Controller
public class VerifiedRequirementsController {
	private static final String REQUIREMENTS_IDS = "requirementsIds[]";

	private final VerifiedRequirementsManagerService verifiedRequirementsManagerService;

	private final TestCaseDisplayService testCaseDisplayService;

	public VerifiedRequirementsController(
		VerifiedRequirementsManagerService verifiedRequirementsManagerService,
		TestCaseDisplayService testCaseDisplayService
	) {
		this.verifiedRequirementsManagerService = verifiedRequirementsManagerService;
		this.testCaseDisplayService = testCaseDisplayService;
	}

	@ResponseBody
	@RequestMapping(value = "backend/test-cases/{testCaseId}/verified-requirements", method = RequestMethod.POST)
	public ChangeCoverageOperationReport addVerifiedRequirementsToTestCase(@RequestBody RequirementIdsForm requirementIdsForm,
																		   @PathVariable long testCaseId) {
		Collection<VerifiedRequirementException> rejections =
			verifiedRequirementsManagerService
				.addVerifiedRequirementsToTestCase(requirementIdsForm.requirementIds, testCaseId);
		Map<String, Object> summary = buildSummary(rejections);
		List<RequirementVersionCoverageDto> coverages = this.testCaseDisplayService.findCoverages(testCaseId);
		ChangeCoverageOperationReport report = new ChangeCoverageOperationReport(coverages);
		report.setSummary(summary);
		return report;
	}

	@ResponseBody
	@RequestMapping(value = "backend/test-cases/{testCaseId}/steps/{testStepId}/verified-requirements", method = RequestMethod.POST)
	public ChangeCoverageOperationReport addVerifiedRequirementsToTestStep(@RequestBody RequirementIdsForm requirementIdsForm,
																		   @PathVariable long testCaseId,
																		   @PathVariable long testStepId) {

		Collection<VerifiedRequirementException> rejections = verifiedRequirementsManagerService
			.addVerifiedRequirementsToTestStep(requirementIdsForm.requirementIds, testStepId);
		List<RequirementVersionCoverageDto> coverages = this.testCaseDisplayService.findCoverages(testCaseId);
		Map<String, Object> summary = buildSummary(rejections);
		ChangeCoverageOperationReport report = new ChangeCoverageOperationReport(coverages);
		report.setSummary(summary);
		return report;
	}

	@ResponseBody
	@RequestMapping(value = "/backend/test-steps/{testStepId}/verified-requirement-versions/{requirementVersionId}", method = RequestMethod.POST)
	public Map<String, Object> addVerifiedRequirementToTestStep(@PathVariable long requirementVersionId,
																@PathVariable long testStepId) {
		Collection<VerifiedRequirementException> rejections = verifiedRequirementsManagerService
			.addVerifiedRequirementVersionToTestStep(requirementVersionId, testStepId);

		return buildSummary(rejections);

	}

	private Map<String, Object> buildSummary(Collection<VerifiedRequirementException> rejections) {
		return VerifiedRequirementActionSummaryBuilder.buildAddActionSummary(rejections);
	}

	@ResponseBody
	@RequestMapping(value = "backend/test-cases/{testCaseId}/verified-requirement-versions/{requirementVersionsIds}", method = RequestMethod.DELETE)
	public ChangeCoverageOperationReport removeVerifiedRequirementVersionsFromTestCase(@PathVariable List<Long> requirementVersionsIds,
																					   @PathVariable long testCaseId) {
		verifiedRequirementsManagerService.removeVerifiedRequirementVersionsFromTestCase(requirementVersionsIds,
			testCaseId);
		List<RequirementVersionCoverageDto> coverages = this.testCaseDisplayService.findCoverages(testCaseId);
		return new ChangeCoverageOperationReport(coverages);
	}

	@ResponseBody
	@RequestMapping(value = "backend/test-cases/{testCaseId}/steps/{testStepId}/verified-requirement-versions/{requirementVersionsIds}", method = RequestMethod.DELETE)
	public ChangeCoverageOperationReport removeVerifiedRequirementVersionsFromTestStep(@PathVariable List<Long> requirementVersionsIds,
																					   @PathVariable long testStepId,
																					   @PathVariable long testCaseId) {
		verifiedRequirementsManagerService.removeVerifiedRequirementVersionsFromTestStep(requirementVersionsIds,
			testStepId);
		List<RequirementVersionCoverageDto> coverages = this.testCaseDisplayService.findCoverages(testCaseId);
		return new ChangeCoverageOperationReport(coverages);
	}

	static class RequirementIdsForm {
		private List<Long> requirementIds;

		public List<Long> getRequirementIds() {
			return requirementIds;
		}

		public void setRequirementIds(List<Long> requirementIds) {
			this.requirementIds = requirementIds;
		}
	}

	static class ChangeCoverageOperationReport {
		private List<RequirementVersionCoverageDto> coverages;
		private Map<String, Object> summary;
		private Map<Long, VerifiedRequirementException> exceptions;

		public ChangeCoverageOperationReport(List<RequirementVersionCoverageDto> coverages) {
			this.coverages = coverages;
		}

		public List<RequirementVersionCoverageDto> getCoverages() {
			return coverages;
		}

		public Map<String, Object> getSummary() {
			return summary;
		}

		public void setSummary(Map<String, Object> summary) {
			this.summary = summary;
		}
	}

}
