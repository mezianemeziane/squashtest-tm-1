/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.project;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.service.project.ProjectTemplateManagerService;
import org.squashtest.tm.web.backend.model.json.JsonTemplateFromProject;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/backend/project-templates")
public class ProjectTemplateController {

	private ProjectTemplateManagerService projectTemplateManagerService;

	@Inject
	ProjectTemplateController(ProjectTemplateManagerService projectTemplateManagerService) {
		this.projectTemplateManagerService = projectTemplateManagerService;
	}

	@ResponseBody
	@ResponseStatus(value = HttpStatus.CREATED)
	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public Map<String, Object> createTemplateFromProject(@Valid @RequestBody JsonTemplateFromProject jsonTemplateFromProject) {
		Map<String, Object> response = new HashMap<>();

		try {
			ProjectTemplate projectTemplate = projectTemplateManagerService.addTemplateFromProject(
				jsonTemplateFromProject.getProjectTemplate(),
				jsonTemplateFromProject.getTemplateId(),
				jsonTemplateFromProject.getParams()
			);

			response.put("id", projectTemplate.getId());
		} catch (NameAlreadyInUseException ex) {
			ex.setObjectName("add-template-from-project");
			throw ex;
		}

		return response;
	}
}
