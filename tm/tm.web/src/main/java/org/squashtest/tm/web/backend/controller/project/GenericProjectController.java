/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.project;

import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.api.wizard.WorkspaceWizard;
import org.squashtest.tm.api.workspace.WorkspaceType;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.service.display.project.ProjectDisplayService;
import org.squashtest.tm.service.internal.display.dto.PartyProjectPermissionDto;
import org.squashtest.tm.service.internal.display.dto.ProjectDto;
import org.squashtest.tm.service.internal.display.dto.TestAutomationProjectDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.project.CustomGenericProjectManager;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.templateplugin.TemplateConfigurablePluginBindingService;
import org.squashtest.tm.service.testautomation.TestAutomationProjectManagerService;
import org.squashtest.tm.web.backend.manager.wizard.WorkspaceWizardManager;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/backend/generic-projects")
public class GenericProjectController {

	private ProjectDisplayService projectDisplayService;

	private final CustomGenericProjectManager projectManagerService;

	private GenericProjectManagerService projectManager;

	private TaskExecutor taskExecutor;

	private TestAutomationProjectManagerService testAutomationProjectService;

	private WorkspaceWizardManager pluginManager;

	private final TemplateConfigurablePluginBindingService templateConfigurablePluginBindingService;

	@Inject
	public GenericProjectController(ProjectDisplayService projectDisplayService,
									GenericProjectManagerService projectManager,
									CustomGenericProjectManager projectManagerService,
									TaskExecutor taskExecutor,
									TestAutomationProjectManagerService testAutomationProjectService,
									WorkspaceWizardManager pluginManager, TemplateConfigurablePluginBindingService templateConfigurablePluginBindingService) {
		this.projectDisplayService = projectDisplayService;
		this.projectManager = projectManager;
		this.projectManagerService = projectManagerService;
		this.taskExecutor = taskExecutor;
		this.testAutomationProjectService = testAutomationProjectService;
		this.pluginManager = pluginManager;

		this.templateConfigurablePluginBindingService = templateConfigurablePluginBindingService;
	}

	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public GridResponse getProjects(@RequestBody GridRequest request) {
		return projectDisplayService.findAll(request);
	}

	@ResponseBody
	@RequestMapping(value = "/templates", method = RequestMethod.GET)
	public Map<String, Object> getTemplates() {
		Map<String, Object> response = new HashMap<>();
		response.put("templates", projectDisplayService.getTemplateNamedReferences());
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/{projectId}/permissions/{partyIds}/group", method = RequestMethod.POST)
	public Map<String, List<PartyProjectPermissionDto>> addNewPartyPermissions(@PathVariable long projectId,
																			   @PathVariable List<Long> partyIds,
																			   @RequestBody Map<String, String> request) {
		projectManagerService.addNewPermissionToProject(partyIds, projectId, request.get("group"));
		return Collections.singletonMap("partyProjectPermissions", projectDisplayService.getPartyProjectPermissions(projectId));
	}

	@ResponseBody
	@RequestMapping(value = "/{projectId}/permissions/{partyIds}", method = RequestMethod.DELETE)
	public void removePartyPermissions(@PathVariable long projectId,
									   @PathVariable List<Long> partyIds) {
		projectManagerService.removeProjectPermission(partyIds, projectId);
	}


	@RequestMapping(value = "/{projectId}/bugtracker", method = RequestMethod.POST)
	@ResponseBody
	public void changeBugtracker(@PathVariable long projectId, @RequestBody Map<String,Long> request) {
		if (request.get("bugtrackerId") != null) {
			projectManager.changeBugTracker(projectId, request.get("bugtrackerId"));
		} else {
			projectManager.removeBugTracker(projectId);
		}
	}

	@RequestMapping(value = "/{projectId}/bugtracker/projectNames", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object>  getBugtrackerProjectNames(@PathVariable long projectId) {
		GenericProject project = projectManager.findById(projectId);
		Map<String, Object> response = new HashMap<>();
		if (project.isBugtrackerConnected()) {
			response.put("projectNames", project.getBugtrackerBinding().getProjectNames());

		}
		return response;
	}

	@RequestMapping(value = "/{projectId}/bugtracker/projectNames", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> changeBugtrackerProjectName(@RequestBody List<String> projectBugTrackerNames,
													@PathVariable long projectId) {
		Map<String, Object> response = new HashMap<>();
		projectManager.changeBugTrackerProjectName(projectId, projectBugTrackerNames);
		response.put("projectNames", projectBugTrackerNames);
		return response;
	}

	@RequestMapping(value = "/{projectId}/name", method = RequestMethod.POST)
	@ResponseBody
	public void changeName(@PathVariable long projectId, @RequestBody GenericProjectController.ProjectPatch patch) {
		projectManager.changeName(projectId, patch.getName());
	}

	@RequestMapping(value = "/{projectId}/description", method = RequestMethod.POST)
	@ResponseBody
	public void changeDescription(@PathVariable long projectId, @RequestBody GenericProjectController.ProjectPatch patch) {
		projectManager.changeDescription(projectId, patch.getDescription());
	}

	@RequestMapping(value = "/{projectId}/label", method = RequestMethod.POST)
	@ResponseBody
	public void changeLabel(@PathVariable long projectId, @RequestBody GenericProjectController.ProjectPatch patch) {
		projectManager.changeLabel(projectId, patch.getLabel());
	}

	@PostMapping(value = "/{projectId}/linked-template-id")
	@ResponseBody
	public Map<String, Object> associateTemplate(@PathVariable long projectId,
												 @RequestBody GenericProjectController.ProjectPatch patch) {
		final Long templateId = patch.getLinkedTemplateId();

		final Map<String, Object> response = new HashMap<>();

		if (templateId == null) {
			projectManager.disassociateFromTemplate(projectId);
			response.put("templateName", null);
		} else {
			ProjectDto linkedTemplate = projectDisplayService.getProjectView(templateId);
			projectManager.associateToTemplate(projectId, templateId, patch.getBoundTemplatePlugins());
			response.put("templateName", linkedTemplate.getName());
		}

		return response;
	}

	@PostMapping(value="/{projectId}/automation-workflow-type")
	@ResponseBody
	public void changeAutomationWorkflowType(@PathVariable long projectId,
											 @RequestBody GenericProjectController.ProjectPatch patch) {
		projectManager.changeAutomationWorkflow(projectId, patch.getAutomationWorkflowType());
	}

	@PostMapping(value="/{projectId}/bdd-implementation-technology")
	@ResponseBody
	public void changeBddImplementationTechnology(@PathVariable long projectId,
												  @RequestBody GenericProjectController.ProjectPatch patch) {
		projectManager.changeBddImplTechnology(projectId, patch.getBddImplementationTechnology());
	}

	@PostMapping(value="/{projectId}/bdd-script-language")
	@ResponseBody
	public void changeBddScriptLanguage(@PathVariable long projectId,
										@RequestBody GenericProjectController.ProjectPatch patch) {
		projectManager.changeBddScriptLanguage(projectId, patch.getBddScriptLanguage());
	}

	@PostMapping(value="/{projectId}/scm-repository-id")
	@ResponseBody
	public void changeScmServer(@PathVariable long projectId,
								@RequestBody GenericProjectController.ProjectPatch patch) {
		final Long repositoryId = patch.getScmRepositoryId();

		if (repositoryId == null) {
			projectManager.unbindScmRepository(projectId);
		} else {
			projectManager.bindScmRepository(projectId, repositoryId);
		}
	}

	@PostMapping(value="/{projectId}/use-tree-structure-in-scm-repo")
	@ResponseBody
	public void changeUseTreeStructureInScmRepo(@PathVariable long projectId,
												@RequestBody GenericProjectController.ProjectPatch patch) {
		projectManager.changeUseTreeStructureInScmRepo(projectId, patch.isUseTreeStructureInScmRepo());
	}

	@PostMapping(value="/{projectId}/ta-server-id")
	@ResponseBody
	public void changeTestAutomationServer(@PathVariable long projectId,
										   @RequestBody GenericProjectController.ProjectPatch patch) {
		projectManager.bindTestAutomationServer(projectId, patch.getTaServerId());
	}

	@PostMapping(value="/{projectId}/automated-suites-lifetime")
	@ResponseBody
	public void changeAutomatedSuitesLifetime(@PathVariable long projectId,
											  @RequestBody GenericProjectController.ProjectPatch patch) {
		projectManager.changeAutomatedSuitesLifetime(projectId, patch.getAutomatedSuitesLifetime());
	}

	/**
	 * Binds test automation projects (a.k.a jobs) to a TM project.
	 *
	 * @param projectId the TM project ID to bound jobs to
	 * @param request the request body MUST contain a "taProject" field which contains an array of TestAutomationProjects to bind
	 * @return the updated list of bound TA project for this TM project
	 */
	@PostMapping(value = "/{projectId}/test-automation-projects/new")
	@ResponseBody
	public Map<String, List<TestAutomationProjectDto>> addTestAutomationProject(@PathVariable long projectId,
																				@RequestBody Map<String, TestAutomationProjectDto[]> request) {

		List<TestAutomationProject> taProjects = Arrays.stream(request.get("taProjects")).map(projectDto -> {
			TestAutomationProject taProject = new TestAutomationProject();
			taProject.setJobName(projectDto.getRemoteName());
			taProject.setCanRunGherkin(projectDto.isCanRunBdd());
			taProject.setLabel(projectDto.getLabel());
			return taProject;
		}).collect(Collectors.toList());

		projectManager.bindTestAutomationProjects(projectId, taProjects);
		return Collections.singletonMap("taProjects", testAutomationProjectService.findAllByTMProject(projectId));
	}

	@RequestMapping(value="/{projectId}/allow-tc-modif-during-exec")
	@ResponseBody
	public void changeAllowTcModifDuringExec(@PathVariable long projectId,
											 @RequestBody GenericProjectController.ProjectPatch patch) {
		projectManager.changeAllowTcModifDuringExec(projectId, patch.isAllowTcModifDuringExec());
	}

	// This method enables and disables an optional execution status which is not already used within project
	@RequestMapping(value ="/{projectId}/change-execution-status/{executionStatus}", method = RequestMethod.POST)
	@ResponseBody
	public void changeExecutionStatusOnProject(@PathVariable long projectId,
											   @PathVariable String executionStatus,
											   @RequestBody Map<String,Boolean> patch) {
		if (patch.get(executionStatus.toUpperCase())) {
			projectManager.enableExecutionStatus(projectId, ExecutionStatus.valueOf(executionStatus));
		} else {
			projectManager.disableExecutionStatus(projectId, ExecutionStatus.valueOf(executionStatus));
		}
	}

	// If the optional status is already used withing project, this method disables this optional status and replaces all its
	// values by a new one selected by user.
	@RequestMapping(value ="/{projectId}/disable-and-replace-execution-status-within-project/{sourceExecutionStatus}", method = RequestMethod.POST)
	@ResponseBody
	public void disableAndReplaceStatusWithinProject(@PathVariable long projectId,
													 @PathVariable String sourceExecutionStatus,
													 @RequestBody Map<String, String> targetExecutionStatus) {
		projectManager.disableExecutionStatus(projectId, ExecutionStatus.valueOf(sourceExecutionStatus));

		ExecutionStatus source = ExecutionStatus.valueOf(sourceExecutionStatus);
		ExecutionStatus target = ExecutionStatus.valueOf(targetExecutionStatus.get("targetExecutionStatus"));
		Runnable replacer = new AsynchronousReplaceExecutionStatus(projectId, source, target);
		taskExecutor.execute(replacer);
	}

	@RequestMapping(value ="/{projectId}/get-enabled-execution-status/{executionStatus}", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getEnabledExecutionStatus(@PathVariable long projectId,
														 @PathVariable String executionStatus) {
		Set<ExecutionStatus> statuses = projectManager.enabledExecutionStatuses(projectId);
		ExecutionStatus status = ExecutionStatus.valueOf(executionStatus);
		statuses.remove(status);
		List<Map<String, Object>> options = new ArrayList<>();
		for (ExecutionStatus st : statuses) {
			Map<String, Object> statusOption = new HashMap<>();
			statusOption.put("label", st.getI18nKey());
			statusOption.put("id",  st.name());
			options.add(statusOption);
		}
		return Collections.singletonMap("statuses", options);
	}

	// ************************* plugins administration ***********************

	@RequestMapping(value = "/{projectId}/plugins/{pluginId}", method = RequestMethod.POST)
	@ResponseBody
	public void enablePlugin(@PathVariable long projectId, @PathVariable String pluginId) {
		WorkspaceWizard wizard = pluginManager.findById(pluginId);
		projectManager.enablePluginForWorkspace(projectId, wizard.getDisplayWorkspace(), pluginId, wizard.getPluginType());

	}

	@RequestMapping(value = "/{projectId}/plugins/{pluginId}", method = RequestMethod.DELETE, params = {"saveConf"})
	@ResponseBody
	@Transactional
	// TODO PCK: this is a lot of work, it may be better suited in a service. pluginManager reside in web layer though
	public void disablePlugin(@PathVariable long projectId, @PathVariable String pluginId, @RequestParam("saveConf") Boolean saveConf) {
		WorkspaceWizard plugin = pluginManager.findById(pluginId);
		List<WorkspaceType> workspaceTypes;
		WorkspaceType configuringWorkspace = plugin.getConfiguringWorkspace();
		WorkspaceType displayWorkspace = plugin.getDisplayWorkspace();

		if (!configuringWorkspace.equals(displayWorkspace)) {
			workspaceTypes = Arrays.asList(configuringWorkspace, displayWorkspace);
		} else {
			workspaceTypes = Collections.singletonList(displayWorkspace);
		}

		if (saveConf.equals(true)) {
			projectManager.disablePluginAndKeepConfiguration(projectId, workspaceTypes, plugin.getId());
			projectManager.disableSynchronisations(projectId, pluginId);
		} else {
			// TODO: remove ref to plugin
			if (pluginId.contains("workflow.automjira")) {
				projectManager.deleteAllRemoteAutomationRequestExtenders(projectId);
			}

			boolean isProjectBound = templateConfigurablePluginBindingService.isProjectConfigurationBoundToTemplate(projectId);
			boolean isProjectTemplate = projectManagerService.isProjectTemplate(projectId);

			if (isProjectTemplate) {
				projectManager.disablePluginAndRemoveConfiguration(projectId, workspaceTypes, pluginId);
				projectManager.synchronizeBoundPluginConfigurations(projectId, pluginId);
			} else if (isProjectBound) {
				projectManager.disablePluginAndKeepConfiguration(projectId, workspaceTypes, pluginId);
				projectManager.removeSynchronisations(projectId, pluginId);
			} else {
				projectManager.disablePluginAndRemoveConfiguration(projectId, workspaceTypes, pluginId);
				projectManager.removeSynchronisations(projectId, pluginId);
			}
		}
	}

	private class AsynchronousReplaceExecutionStatus implements Runnable {

		private Long projectId;
		private ExecutionStatus sourceExecutionStatus;
		private ExecutionStatus targetExecutionStatus;

		public AsynchronousReplaceExecutionStatus(Long projectId, ExecutionStatus sourceExecutionStatus,
												  ExecutionStatus targetExecutionStatus) {
			super();
			this.projectId = projectId;
			this.sourceExecutionStatus = sourceExecutionStatus;
			this.targetExecutionStatus = targetExecutionStatus;
		}

		@Override
		public void run() {
			projectManager.replaceExecutionStepStatus(projectId, sourceExecutionStatus, targetExecutionStatus);
		}
	}

	public static class ProjectPatch {
		private String name;
		private String description;
		private String label;
		private Long linkedTemplateId;
		private String automationWorkflowType;
		private Long scmRepositoryId;
		private boolean useTreeStructureInScmRepo;
		private Long taServerId;
		private boolean allowTcModifDuringExec;
		private Map<String,Boolean> allowedStatuses;
		private String automatedSuitesLifetime;
		private String bddImplementationTechnology;
		private String bddScriptLanguage;
		private List<String> boundTemplatePlugins;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public Long getLinkedTemplateId() {
			return linkedTemplateId;
		}

		public void setLinkedTemplateId(Long linkedTemplateId) {
			this.linkedTemplateId = linkedTemplateId;
		}

		public String getAutomationWorkflowType() {
			return automationWorkflowType;
		}

		public void setAutomationWorkflowType(String automationWorkflowType) { this.automationWorkflowType = automationWorkflowType; }

		public Long getScmRepositoryId() {
			return scmRepositoryId;
		}

		public void setScmRepositoryId(Long scmRepositoryId) {
			this.scmRepositoryId = scmRepositoryId;
		}

		public boolean isUseTreeStructureInScmRepo() {
			return useTreeStructureInScmRepo;
		}

		public void setUseTreeStructureInScmRepo(boolean useTreeStructureInScmRepo) { this.useTreeStructureInScmRepo = useTreeStructureInScmRepo; }

		public Long getTaServerId() {
			return taServerId;
		}

		public void setTaServerId(Long taServerId) {
			this.taServerId = taServerId;
		}

		public boolean isAllowTcModifDuringExec() {
			return allowTcModifDuringExec;
		}

		public void setAllowTcModifDuringExec(boolean allowTcModifDuringExec) {	this.allowTcModifDuringExec = allowTcModifDuringExec; }

		public Map<String, Boolean> getAllowedStatuses() {
			return allowedStatuses;
		}

		public void setAllowedStatuses(Map<String, Boolean> allowedStatuses) {
			this.allowedStatuses = allowedStatuses;
		}

		public String getAutomatedSuitesLifetime() {
			return automatedSuitesLifetime;
		}

		public void setAutomatedSuitesLifetime(String automatedSuitesLifetime) {
			this.automatedSuitesLifetime = automatedSuitesLifetime;
		}

		public String getBddImplementationTechnology() {
			return bddImplementationTechnology;
		}

		public void setBddImplementationTechnology(String bddImplementationTechnology) {
			this.bddImplementationTechnology = bddImplementationTechnology;
		}

		public String getBddScriptLanguage() {
			return bddScriptLanguage;
		}

		public void setBddScriptLanguage(String bddScriptLanguage) {
			this.bddScriptLanguage = bddScriptLanguage;
		}

		public List<String> getBoundTemplatePlugins() {
			return boundTemplatePlugins;
		}

		public void setBoundTemplatePlugins(List<String> boundTemplatePlugins) {
			this.boundTemplatePlugins = boundTemplatePlugins;
		}
	}
}
