/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.infolist;

import org.springframework.web.bind.annotation.*;
import org.squashtest.tm.service.display.infolist.InfoListDisplayService;
import org.squashtest.tm.service.internal.display.dto.InfoListAdminViewDto;


import javax.inject.Inject;

@RestController
@RequestMapping("/backend/info-list-view")
public class InfoListViewController {

	private InfoListDisplayService infoListDisplayService;

	@Inject
	InfoListViewController(InfoListDisplayService infoListDisplayService) {
		this.infoListDisplayService = infoListDisplayService;
	}

	@RequestMapping(value = "/{infoListId}", method = RequestMethod.GET)
	@ResponseBody
	public InfoListAdminViewDto getInfoListView(@PathVariable long infoListId) {
		return infoListDisplayService.getInfoListView(infoListId);
	}
}
