/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.bugtracker;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.csp.core.bugtracker.domain.BugTracker;
import org.squashtest.tm.service.bugtracker.BugTrackerFinderService;
import org.squashtest.tm.service.bugtracker.BugTrackerModificationService;
import org.squashtest.tm.service.display.bugtracker.BugTrackerDisplayService;
import org.squashtest.tm.service.internal.display.dto.BugTrackerViewDto;
import org.squashtest.tm.service.internal.display.dto.CredentialsDto;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.servers.ServerAuthConfiguration;
import org.squashtest.tm.service.servers.StoredCredentialsManager;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/backend/bugtracker-view")
public class BugTrackerViewController {

    private final BugTrackerDisplayService bugTrackerDisplayService;
    private final BugTrackerFinderService bugTrackerFinder;
    private final StoredCredentialsManager credentialsManager;
    private final BugTrackerModificationService bugTrackerModificationService;
    private final BugTrackerFinderService bugTrackerFinderService;

    BugTrackerViewController(BugTrackerDisplayService bugTrackerDisplayService,
                             BugTrackerFinderService bugTrackerFinder,
                             StoredCredentialsManager credentialsManager,
                             BugTrackerModificationService bugTrackerModificationService,
                             BugTrackerFinderService bugTrackerFinderService) {
        this.bugTrackerDisplayService = bugTrackerDisplayService;
        this.bugTrackerFinder = bugTrackerFinder;
        this.credentialsManager = credentialsManager;
        this.bugTrackerModificationService = bugTrackerModificationService;
        this.bugTrackerFinderService = bugTrackerFinderService;
    }

    @GetMapping("/{bugTrackerId}")
    @ResponseBody
    BugTrackerViewDto getBugTrackerView(@PathVariable Long bugTrackerId) {
        BugTrackerViewDto dto = this.bugTrackerDisplayService.getBugTrackerView(bugTrackerId);
        dto.setBugTrackerKinds(findBugTrackerKinds());

        ManageableCredentials credentials = credentialsManager.findAppLevelCredentials(bugTrackerId);
        dto.setCredentials(CredentialsDto.from(credentials));

        ServerAuthConfiguration configuration = credentialsManager.findServerAuthConfiguration(bugTrackerId);
        dto.setAuthConfiguration(configuration);

        List<String> supportedProtocols = getSupportedAuthenticationProtocols(bugTrackerId);
        dto.setSupportedAuthenticationProtocols(supportedProtocols);

        return dto;
    }

    private List<String> getSupportedAuthenticationProtocols(Long bugTrackerId) {
        // Trying to get supported protocols may throw a UnknownConnectorKindException if the plugin supporting the
        // bug tracker kind is not present.
        BugTracker bugTracker = bugTrackerFinderService.findById(bugTrackerId);
        return Arrays.stream(bugTrackerModificationService.getSupportedProtocols(bugTracker))
                .map(Enum::name).collect(Collectors.toList());
    }

    private Set<String> findBugTrackerKinds() {
        return bugTrackerFinder.findBugTrackerKinds()
                .stream()
                .map(HTMLCleanupUtils::cleanHtml)
                .collect(Collectors.toSet());
    }
}
