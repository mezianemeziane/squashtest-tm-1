/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.bugtracker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.csp.core.bugtracker.core.BugTrackerManagerException;
import org.squashtest.csp.core.bugtracker.core.BugTrackerNoCredentialsException;
import org.squashtest.csp.core.bugtracker.domain.BTIssue;
import org.squashtest.csp.core.bugtracker.domain.BugTracker;
import org.squashtest.csp.core.bugtracker.spi.BugTrackerInterfaceDescriptor;
import org.squashtest.tm.bugtracker.advanceddomain.AdvancedIssue;
import org.squashtest.tm.bugtracker.advanceddomain.DelegateCommand;
import org.squashtest.tm.bugtracker.definition.Attachment;
import org.squashtest.tm.bugtracker.definition.RemoteIssue;
import org.squashtest.tm.core.foundation.collection.DefaultPagingAndSorting;
import org.squashtest.tm.core.foundation.collection.PagedCollectionHolder;
import org.squashtest.tm.core.foundation.collection.PagingAndSorting;
import org.squashtest.tm.core.foundation.collection.PagingBackedPagedCollectionHolder;
import org.squashtest.tm.core.foundation.collection.SortOrder;
import org.squashtest.tm.core.foundation.collection.SpringPaginationUtils;
import org.squashtest.tm.core.foundation.exception.NullArgumentException;
import org.squashtest.tm.domain.bugtracker.IssueDetector;
import org.squashtest.tm.domain.bugtracker.IssueOwnership;
import org.squashtest.tm.domain.bugtracker.RemoteIssueDecorator;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.servers.AuthenticationStatus;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.users.PartyPreference;
import org.squashtest.tm.domain.users.preferences.CorePartyPreference;
import org.squashtest.tm.exception.bugtracker.BugTrackerManagerActionException;
import org.squashtest.tm.service.bugtracker.BugTrackerManagerService;
import org.squashtest.tm.service.bugtracker.BugTrackersLocalService;
import org.squashtest.tm.service.bugtracker.BugTrackersService;
import org.squashtest.tm.service.bugtracker.RequirementVersionIssueOwnership;
import org.squashtest.tm.service.campaign.CampaignFinder;
import org.squashtest.tm.service.campaign.CampaignLibraryNavigationService;
import org.squashtest.tm.service.campaign.IterationFinder;
import org.squashtest.tm.service.campaign.TestSuiteFinder;
import org.squashtest.tm.service.display.issue.IssueEntityType;
import org.squashtest.tm.service.execution.ExecutionFinder;
import org.squashtest.tm.service.internal.bugtracker.BugTrackerConnectorFactory;
import org.squashtest.tm.service.internal.configuration.CallbackUrlProvider;
import org.squashtest.tm.service.internal.display.bugtracker.BugTrackerServiceHelper;
import org.squashtest.tm.service.internal.display.dto.IssuesPanelModel;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.GridSort;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.requirement.RequirementVersionManagerService;
import org.squashtest.tm.service.testautomation.spi.BadConfiguration;
import org.squashtest.tm.service.testcase.TestCaseFinder;
import org.squashtest.tm.service.user.PartyPreferenceService;
import org.squashtest.tm.web.backend.controller.attachment.UploadedData;
import org.squashtest.tm.web.backend.controller.attachment.UploadedDataPropertyEditorSupport;
import org.squashtest.tm.web.backend.controller.thirdpartyserver.ThirdPartyServersAuthenticationController;
import org.squashtest.tm.web.backend.helper.JsonHelper;
import org.squashtest.tm.web.i18n.InternationalizationHelper;
import oslcdomain.OslcIssue;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static java.util.stream.Collectors.toList;

/**
 * This controller is responsible for a lot of things :
 *  - Fetch known issues for each issue bindable entities
 *  - Post new remote issues (execution and execution steps) and detach existing remote issues
 *  - Forward attachments and commands to remote issues
 *
 * Notes :
 * - as of 1.13 the questions regarding authentications has been moved to
 * {@link ThirdPartyServersAuthenticationController}
 * - as of 2.0, bugtrackers administration was moved to {@link BugTrackerModificationController}
 *
 * @author bsiri, aboittiaux, pckerneis
 */
@Controller
@RequestMapping("/backend/issues")
public class RemoteIssuesController {

	private static final String SORTING_DEFAULT_ATTRIBUTE = "Issue.remoteIssueId"; // here is the real fix for #5683 ;)

	private static final Logger LOGGER = LoggerFactory.getLogger(RemoteIssuesController.class);

	private final BugTrackerConnectorFactory btFactory;
	private final BugTrackersLocalService bugTrackersLocalService;
	private final RequirementVersionManagerService requirementVersionManager;
	private final CampaignFinder campaignFinder;
	private final IterationFinder iterationFinder;
	private final TestSuiteFinder testSuiteFinder;
	private final ExecutionFinder executionFinder;
	private final BugTrackerManagerService bugTrackerManagerService;
	private final InternationalizationHelper messageSource;
	private final CampaignLibraryNavigationService clnService;
	private final TestCaseFinder testCaseFinder;
	private final BugTrackerServiceHelper bugTrackerServiceHelper;
	private final BugTrackersService bugTrackersService;
	private final PartyPreferenceService partyPreferenceService;
	private final GenericProjectManagerService projectManager;
	private final CallbackUrlProvider callbackUrlProvider;

	RemoteIssuesController(BugTrackerConnectorFactory btFactory,
						   BugTrackersLocalService bugTrackersLocalService,
						   RequirementVersionManagerService requirementVersionManager,
						   CampaignFinder campaignFinder,
						   IterationFinder iterationFinder,
						   TestSuiteFinder testSuiteFinder,
						   ExecutionFinder executionFinder,
						   BugTrackerManagerService bugTrackerManagerService,
						   InternationalizationHelper messageSource,
						   CampaignLibraryNavigationService clnService,
						   TestCaseFinder testCaseFinder,
						   BugTrackerServiceHelper bugTrackerServiceHelper,
						   BugTrackersService bugTrackersService,
						   PartyPreferenceService partyPreferenceService,
						   GenericProjectManagerService projectManager,
						   CallbackUrlProvider callbackUrlProvider) {
		this.btFactory = btFactory;
		this.bugTrackersLocalService = bugTrackersLocalService;
		this.requirementVersionManager = requirementVersionManager;
		this.campaignFinder = campaignFinder;
		this.iterationFinder = iterationFinder;
		this.testSuiteFinder = testSuiteFinder;
		this.executionFinder = executionFinder;
		this.bugTrackerManagerService = bugTrackerManagerService;
		this.messageSource = messageSource;
		this.clnService = clnService;
		this.testCaseFinder = testCaseFinder;
		this.bugTrackerServiceHelper = bugTrackerServiceHelper;
		this.bugTrackersService = bugTrackersService;
		this.partyPreferenceService = partyPreferenceService;
		this.projectManager = projectManager;
		this.callbackUrlProvider = callbackUrlProvider;
	}

	static final String EXECUTION_STEP_TYPE = "execution-step";
	static final String EXECUTION_TYPE = "execution";
	static final String ITERATION_TYPE = "iteration";
	static final String CAMPAIGN_TYPE = "campaign";
	static final String TEST_SUITE_TYPE = "test-suite";
	static final String TEST_CASE_TYPE = "test-case";
	static final String CAMPAIGN_FOLDER_TYPE = "campaign-folder";
	static final String REQUIREMENT_VERSION_TYPE = "requirement-version";

	private static final String BUGTRACKER_ID = "bugTrackerId";

	private static final String POSTING_NEW_ISSUE_MESSAGE = "BugTrackerController: posting a new issue for execution-step ";

	@InitBinder
	public void initBinder(ServletRequestDataBinder binder) throws ServletException {
		binder.registerCustomEditor(UploadedData.class, new UploadedDataPropertyEditorSupport());
	}

	@RequestMapping(value = "projects/{projectId}/bugtracker", method = RequestMethod.GET)
	@ResponseBody
	BugTrackerInfo getBugtrackerInfo(@PathVariable long projectId) {
		GenericProject project = projectManager.findById(projectId);
		BugTrackerInfo response = new BugTrackerInfo();
		if (project.isBugtrackerConnected()) {
			response.setProjectNames(project.getBugtrackerBinding().getProjectNames());
			response.setKind(project.getBugtrackerBinding().getBugtracker().getKind());
		}
		return response;
	}

	public static class BugTrackerInfo {
		List<String> projectNames;
		String kind;

		public List<String> getProjectNames() {
			return projectNames;
		}

		public void setProjectNames(List<String> projectNames) {
			this.projectNames = projectNames;
		}

		public String getKind() {
			return kind;
		}

		public void setKind(String kind) {
			this.kind = kind;
		}
	}

	/*
	 * ***********************************************************************************************************
	 * ExecutionStep
	 * ***********************************************************************************************************
	 */

	@GetMapping(EXECUTION_STEP_TYPE + "/{stepId}")
	@ResponseBody
	public IssuesPanelModel getExecStepIssuePanel(@PathVariable Long stepId, Locale locale) {
		ExecutionStep step = executionFinder.findExecutionStepById(stepId);
		return makeIssuePanel(EXECUTION_STEP_TYPE, locale, step.getProject());
	}

	@PostMapping(EXECUTION_STEP_TYPE + "/{stepId}/known-issues")
	@ResponseBody
	public GridResponse getExecStepKnownIssuesData(@PathVariable Long stepId, @RequestBody GridRequest gridRequest) {
		PagingAndSorting sorter = new IssueDisplaySorting(gridRequest);
		return getKnownIssuesData(IssueEntityType.EXECUTION_STEP_TYPE, stepId, sorter);
	}

	@GetMapping(EXECUTION_STEP_TYPE + "/{stepId}/new-issue")
	@ResponseBody
	public RemoteIssue getExecStepReportStub(@PathVariable Long stepId, Locale locale, HttpServletRequest request,
											 @RequestParam("project-name") String projectName) {
		ExecutionStep step = executionFinder.findExecutionStepById(stepId);
		String executionUrl;
		try {
			String squashPublicUrl = callbackUrlProvider.getCallbackUrl().toExternalForm();
			executionUrl = BugTrackerControllerHelper.buildExecutionUrlWithSquashPublicUrl(squashPublicUrl, step.getExecution());
		} catch (BadConfiguration badConfigurationException) {
			executionUrl = BugTrackerControllerHelper.buildExecutionUrlFromRequest(request, step.getExecution());
		}
		return makeReportIssueModel(step, locale, executionUrl, projectName);
	}

	@PostMapping(EXECUTION_STEP_TYPE + "/{stepId}/new-issue")
	@ResponseBody
	public Map<String, String> postExecStepIssueReport(@PathVariable Long stepId, @RequestBody BTIssue jsonIssue) {
		LOGGER.trace(POSTING_NEW_ISSUE_MESSAGE + stepId);
		IssueDetector entity = executionFinder.findExecutionStepById(stepId);

		if (jsonIssue.hasBlankId()) {
			return processIssue(jsonIssue, entity);
		} else {
			return attachIssue(jsonIssue, entity);
		}
	}

	@PostMapping(EXECUTION_STEP_TYPE + "/{stepId}/new-advanced-issue")
	@ResponseBody
	public Map<String, String> postExecStepAdvancedIssueReport(@PathVariable Long stepId, @RequestBody AdvancedIssue jsonIssue) {
		LOGGER.trace(POSTING_NEW_ISSUE_MESSAGE + stepId);
		IssueDetector entity = executionFinder.findExecutionStepById(stepId);

		if (jsonIssue.hasBlankId()) {
			return processIssue(jsonIssue, entity);
		} else {
			return attachIssue(jsonIssue, entity);
		}
	}

	@PostMapping(EXECUTION_STEP_TYPE + "/{stepId}/new-oslc-issue")
	@ResponseBody
	public void postExecStepIssueReport(@PathVariable Long stepId, @RequestBody NewOslcIssueRequestBody requestBody) {
		LOGGER.trace(POSTING_NEW_ISSUE_MESSAGE + stepId);

		IssueDetector entity = executionFinder.findExecutionStepById(stepId);

		OslcIssue issue = new OslcIssue();
		issue.setId(requestBody.getIssueId());
		attachIssue(issue, entity);
	}

	/*
	 * ***********************************************************************************************************
	 * Execution
	 * ***********************************************************************************************************
	 */

	@GetMapping(EXECUTION_TYPE + "/{execId}")
	@ResponseBody
	public IssuesPanelModel getExecIssuePanel(@PathVariable Long execId, Locale locale) {
		Execution execution = executionFinder.findById(execId);
		return makeIssuePanel(EXECUTION_TYPE, locale, execution.getProject());
	}

	@PostMapping(EXECUTION_TYPE + "/{execId}/known-issues")
	@ResponseBody
	public GridResponse getExecKnownIssuesData(@PathVariable Long execId, @RequestBody GridRequest gridRequest) {
		PagingAndSorting sorter = new IssueDisplaySorting(gridRequest);
		return getKnownIssuesData(IssueEntityType.EXECUTION_TYPE, execId, sorter);
	}

	@PostMapping(EXECUTION_TYPE + "/{execId}/all-known-issues")
	@ResponseBody
	public GridResponse getExecKnownIssuesDataWithoutPaging(@PathVariable Long execId) {
		PagingAndSorting sorter = new DefaultPagingAndSorting("", true);
		return getKnownIssuesData(IssueEntityType.EXECUTION_TYPE, execId, sorter);
	}

	@GetMapping(EXECUTION_TYPE + "/{execId}/new-issue")
	@ResponseBody
	public RemoteIssue getExecReportStub(@PathVariable Long execId, Locale locale, HttpServletRequest request,
										 @RequestParam("project-name") String projectName) {
		Execution execution = executionFinder.findById(execId);
		String executionUrl;
		try {
			String squashPublicUrl = callbackUrlProvider.getCallbackUrl().toExternalForm();
			executionUrl = BugTrackerControllerHelper.buildExecutionUrlWithSquashPublicUrl(squashPublicUrl, execution);
		} catch (BadConfiguration badConfigurationException) {
			executionUrl = BugTrackerControllerHelper.buildExecutionUrlFromRequest(request, execution);
		}
		return makeReportIssueModel(execution, locale, executionUrl, projectName);
	}

	@PostMapping(EXECUTION_TYPE + "/{execId}/new-issue")
	@ResponseBody
	public Object postExecIssueReport(@PathVariable Long execId, @RequestBody BTIssue jsonIssue) {
		LOGGER.trace(POSTING_NEW_ISSUE_MESSAGE + execId);

		Execution entity = executionFinder.findById(execId);

		if (jsonIssue.hasBlankId()) {
			return processIssue(jsonIssue, entity);
		} else {
			return attachIssue(jsonIssue, entity);
		}
	}

	@PostMapping(EXECUTION_TYPE + "/{execId}/new-advanced-issue")
	@ResponseBody
	public Object postExecAdvancedIssueReport(@PathVariable Long execId, @RequestBody AdvancedIssue jsonIssue) {
		LOGGER.trace(POSTING_NEW_ISSUE_MESSAGE + execId);

		Execution entity = executionFinder.findById(execId);

		if (jsonIssue.hasBlankId()) {
			return processIssue(jsonIssue, entity);
		} else {
			return attachIssue(jsonIssue, entity);
		}
	}

	@PostMapping(EXECUTION_TYPE + "/{execId}/new-oslc-issue")
	@ResponseBody
	public void postExecIssueReport(@PathVariable Long execId, @RequestBody NewOslcIssueRequestBody requestBody) {
		LOGGER.trace("BugTrackerController: posting a new issue for execution " + execId);

		Execution entity = executionFinder.findById(execId);

		OslcIssue issue = new OslcIssue();
		issue.setId(requestBody.getIssueId());
		attachIssue(issue, entity);
	}

	/*
	 * ***********************************************************************************************************
	 * RequirementVersion
	 * ***********************************************************************************************************
	 */

	@GetMapping(REQUIREMENT_VERSION_TYPE + "/{rvId}")
	@ResponseBody
	public IssuesPanelModel getRequirementWorkspaceIssuePanel(@PathVariable Long rvId, Locale locale) {
		RequirementVersion requirementVersion = requirementVersionManager.findById(rvId);
		return makeIssuePanel(REQUIREMENT_VERSION_TYPE, locale, requirementVersion.getProject());
	}

	@PostMapping(REQUIREMENT_VERSION_TYPE + "/{rvId}/known-issues/all")
	@ResponseBody
	public GridResponse getRequirementVersionKnownIssuesData(@PathVariable Long rvId, @RequestBody GridRequest gridRequest) {
		PagingAndSorting sorter = new IssueDisplaySorting(gridRequest);
		return getKnownIssuesDataForRequirementVersion(rvId, "all", sorter);
	}

	@PostMapping(REQUIREMENT_VERSION_TYPE + "/{rvId}/known-issues/current-version")
	@ResponseBody
	public GridResponse getRequirementVersionKnownIssuesDataCurrentVersion(@PathVariable Long rvId, @RequestBody GridRequest gridRequest) {
		PagingAndSorting sorter = new IssueDisplaySorting(gridRequest);
		return getKnownIssuesDataForRequirementVersion(rvId, "info", sorter);
	}

	/*
	 * ***********************************************************************************************************
	 * TestCase
	 * ***********************************************************************************************************
	 */

	@GetMapping(TEST_CASE_TYPE + "/{tcId}")
	@ResponseBody
	public IssuesPanelModel getTestCaseIssuePanel(@PathVariable Long tcId, Locale locale) {
		TestCase testCase = testCaseFinder.findById(tcId);
		return makeIssuePanel(TEST_CASE_TYPE, locale, testCase.getProject());
	}

	@PostMapping(TEST_CASE_TYPE + "/{tcId}/known-issues")
	@ResponseBody
	public GridResponse getTestCaseKnownIssuesData(@PathVariable Long tcId, @RequestBody GridRequest gridRequest) {
		PagingAndSorting sorter = new IssueDisplaySorting(gridRequest);
		return getKnownIssuesData(IssueEntityType.TEST_CASE_TYPE, tcId, sorter);
	}

	/*
	 * ***********************************************************************************************************
	 * Iteration
	 * ***********************************************************************************************************
	 */

	@GetMapping(ITERATION_TYPE + "/{iterId}")
	@ResponseBody
	public IssuesPanelModel getIterationIssuePanel(@PathVariable Long iterId, Locale locale) {
		Iteration iteration = iterationFinder.findById(iterId);
		return makeIssuePanel(ITERATION_TYPE, locale, iteration.getProject());
	}

	@PostMapping(ITERATION_TYPE + "/{iterId}/known-issues")
	@ResponseBody
	public GridResponse getIterationKnownIssuesData(@PathVariable Long iterId, @RequestBody GridRequest gridRequest) {
		PagingAndSorting sorter = new IssueDisplaySorting(gridRequest);
		return getKnownIssuesData(IssueEntityType.ITERATION_TYPE, iterId, sorter);
	}

	/*
	 * ***********************************************************************************************************
	 * Campaign
	 * ***********************************************************************************************************
	 */

	@GetMapping(CAMPAIGN_TYPE + "/{campId}")
	@ResponseBody
	public IssuesPanelModel getCampaignIssuePanel(@PathVariable Long campId, Locale locale) {
		Campaign campaign = campaignFinder.findById(campId);
		return makeIssuePanel(CAMPAIGN_TYPE, locale, campaign.getProject());
	}

	@PostMapping(CAMPAIGN_TYPE + "/{campId}/known-issues")
	@ResponseBody
	public GridResponse getCampaignKnownIssuesData(@PathVariable Long campId, @RequestBody GridRequest gridRequest) {
		PagingAndSorting sorter = new IssueDisplaySorting(gridRequest);
		return getKnownIssuesData(IssueEntityType.CAMPAIGN_TYPE, campId, sorter);
	}

	/*
	 * ***********************************************************************************************************
	 * TestSuite level section
	 * ***********************************************************************************************************
	 */

	@GetMapping(TEST_SUITE_TYPE + "/{testSuiteId}")
	@ResponseBody
	public IssuesPanelModel getTestSuiteIssuePanel(@PathVariable Long testSuiteId, Locale locale) {

		TestSuite testSuite = testSuiteFinder.findById(testSuiteId);
		return makeIssuePanel(TEST_SUITE_TYPE, locale, testSuite.getProject());
	}

	@PostMapping(TEST_SUITE_TYPE + "/{testSuiteId}/known-issues")
	@ResponseBody
	public GridResponse getTestSuiteKnownIssuesData(@PathVariable Long testSuiteId, @RequestBody GridRequest gridRequest) {
		PagingAndSorting sorter = new IssueDisplaySorting(gridRequest);
		return getKnownIssuesData(IssueEntityType.TEST_SUITE_TYPE, testSuiteId, sorter);
	}

	/*
	 * *********************************************************************************************************
	 * CampaignFolder
	 * *********************************************************************************************************
	 */

	@GetMapping(CAMPAIGN_FOLDER_TYPE + "/{campaignFolderId}")
	@ResponseBody
	public IssuesPanelModel getCampaignFolderIssuePanel(@PathVariable Long campaignFolderId, Locale locale) {
		CampaignFolder campaignFolder = clnService.findFolder(campaignFolderId);
		return makeIssuePanel(CAMPAIGN_FOLDER_TYPE, locale, campaignFolder.getProject());
	}

	@PostMapping(CAMPAIGN_FOLDER_TYPE + "/{campaignFolderId}/known-issues")
	@ResponseBody
	public GridResponse getCampaignFolderKnownIssuesData(@PathVariable Long campaignFolderId, @RequestBody GridRequest gridRequest) {
		PagingAndSorting sorter = new IssueDisplaySorting(gridRequest);
		return getKnownIssuesData(IssueEntityType.CAMPAIGN_FOLDER_TYPE, campaignFolderId, sorter);
	}

	/*
	 * *********************************************************************************************************
	 * Issues management
	 * *********************************************************************************************************
	 */

	@GetMapping("/find-issue/{remoteKey}")
	@ResponseBody
	public RemoteIssue findIssue(@PathVariable String remoteKey, @RequestParam(BUGTRACKER_ID) long bugTrackerId) {
		BugTracker bugTracker = bugTrackerManagerService.findById(bugTrackerId);
		return bugTrackersLocalService.getIssue(remoteKey, bugTracker);
	}

	private Map<String, String> processIssue(RemoteIssue issue, IssueDetector entity) {
		final RemoteIssue postedIssue = bugTrackersLocalService.createIssue(entity, issue);
		final URL issueUrl = bugTrackersLocalService.getIssueUrl(postedIssue.getId(), entity.getBugTracker());

		Map<String, String> result = new HashMap<>();
		result.put("url", issueUrl.toString());
		result.put("issueId", postedIssue.getId());

		List<String> remoteReqIds = bugTrackersLocalService.findAllRemoteReqIdByServerUrlVerifiedByATestCase(entity.getBugTracker().getUrl(), entity.getReferencedTestCase().getId());
		if (!remoteReqIds.isEmpty()) {
			bugTrackersLocalService.linkIssueToRemoteRequirements(postedIssue.getId(), remoteReqIds, entity.getBugTracker());
		}

		return result;
	}

	private Map<String, String> attachIssue(final RemoteIssue issue, IssueDetector entity) {
		bugTrackersLocalService.attachIssue(entity, issue.getId());
		final URL issueUrl = bugTrackersLocalService.getIssueUrl(issue.getId(), entity.getBugTracker());

		Map<String, String> result = new HashMap<>();
		result.put("url", issueUrl.toString());
		result.put("issueId", issue.getId());

		List<String> remoteReqIds = bugTrackersLocalService.findAllRemoteReqIdByServerUrlVerifiedByATestCase(entity.getBugTracker().getUrl(), entity.getReferencedTestCase().getId());
		if (!remoteReqIds.isEmpty()) {
			bugTrackersLocalService.linkIssueToRemoteRequirements(issue.getId(), remoteReqIds, entity.getBugTracker());
		}
		return result;
	}

	@DeleteMapping("/{issueIds}")
	@ResponseBody
	public void detachIssues(@PathVariable List<Long> issueIds) {
		bugTrackersLocalService.detachIssues(issueIds);
	}

	@PostMapping("/{btName}/{remoteIssueId}/attachments")
	@ResponseBody
	public void forwardAttachmentsToIssue(@PathVariable String btName,
										  @PathVariable String remoteIssueId,
										  @RequestParam("attachment[]") List<UploadedData> uploads) {
		List<Attachment> issueAttachments = new ArrayList<>(uploads.size());

		for (UploadedData upload : uploads) {
			Attachment newAttachment = new Attachment(upload.getName(), upload.getSizeInBytes(), upload.getStream());
			issueAttachments.add(newAttachment);
		}

		bugTrackersLocalService.forwardAttachments(remoteIssueId, btName, issueAttachments);

		// now ensure that the input streams are closed
		for (Attachment attachment : issueAttachments) {
			try {
				attachment.getStreamContent().close();
			} catch (IOException ex) {
				LOGGER.warn("issue attachments : could not close stream for " + attachment.getName()
					+ ", this is non fatal anyway", ex);
			}
		}
	}

	@PostMapping("{bugtrackerName}/command")
	@ResponseBody
	public Object forwardDelegateCommand(@PathVariable String bugtrackerName, @RequestBody DelegateCommand command) {
		return bugTrackersLocalService.forwardDelegateCommand(command, bugtrackerName);
	}

	/* ********* generates a json model for an issue ******* */

	private RemoteIssue makeReportIssueModel(Execution exec, Locale locale, String executionUrl, String projectName) {
		String defaultDescription = BugTrackerControllerHelper.getDefaultDescription(exec, locale, messageSource,
			executionUrl);
		return makeReportIssueModel(exec, defaultDescription, projectName);
	}

	private RemoteIssue makeReportIssueModel(ExecutionStep step, Locale locale, String executionUrl,
											 String projectName) {
		String defaultDescription = BugTrackerControllerHelper.getDefaultDescription(step, locale, messageSource,
			executionUrl);
		return makeReportIssueModel(step, defaultDescription, locale, projectName);
	}

	private RemoteIssue makeReportIssueModel(ExecutionStep step, String defaultDescription,
											 Locale locale, String projectName) {
		RemoteIssue emptyIssue = makeReportIssueModel(step, defaultDescription, projectName);
		String comment = BugTrackerControllerHelper.getAdditionalInformation(step, locale, messageSource);
		emptyIssue.setComment(comment);
		return emptyIssue;
	}

	private RemoteIssue makeReportIssueModel(IssueDetector entity, String defaultDescription, String projectName) {
		RemoteIssue emptyIssue = bugTrackersLocalService.createReportIssueTemplate(projectName, entity.getBugTracker());
		emptyIssue.setDescription(defaultDescription);
		return emptyIssue;
	}

	private IssuesPanelModel makeIssuePanel(String type, Locale locale, Project project) {
		if (project.isBugtrackerConnected()) {
			BugTracker bugtracker = project.findBugTracker();
			AuthenticationStatus status = checkStatus(bugtracker, project.getId());
			BugTrackerInterfaceDescriptor descriptor = bugTrackersLocalService.getInterfaceDescriptor(bugtracker);
			descriptor.setLocale(locale);
			String projectNames = JsonHelper.serialize(project.getBugtrackerBinding().getProjectNames()
				.stream()
				.map(HTMLCleanupUtils::cleanAndUnescapeHTML)
				.collect(toList()));
			IssuesPanelModel issuesPanelModel = new IssuesPanelModel();
			issuesPanelModel.setProjectId(project.getId());
			issuesPanelModel.setBugTrackerStatus(status);
			issuesPanelModel.setDelete("");
			issuesPanelModel.setEntityType(type);
			issuesPanelModel.setProjectName(projectNames);
			issuesPanelModel.setOslc(btFactory.isOslcConnector(bugtracker.getKind()));

			PartyPreference modePreference = partyPreferenceService.findPreferenceForCurrentUser(CorePartyPreference.BUGTRACKER_MODE.getPreferenceKey());

			if (modePreference != null) {
				issuesPanelModel.setBugTrackerMode(modePreference.getPreferenceValue());
			}

			return issuesPanelModel;
		} else {
			return new IssuesPanelModel();
		}
	}

	/* ******************************* private methods ********************************************** */

	private GridResponse getKnownIssuesDataForRequirementVersion(Long id, String panelSource, PagingAndSorting paging) {
		PagedCollectionHolder<List<RequirementVersionIssueOwnership<RemoteIssueDecorator>>> filteredCollection;
		try {
			filteredCollection = bugTrackersLocalService.findSortedIssueOwnershipForRequirmentVersion(id, panelSource, paging);
		} catch (BugTrackerNoCredentialsException | NullArgumentException exception) {
			filteredCollection = makeEmptyIssueDecoratorCollectionHolderForRequirement(IssueEntityType.REQUIREMENT_VERSION_TYPE.name(), id, exception, paging);
		}

		return bugTrackerServiceHelper.createModelBuilderForRequirementVersion().buildGridResponse(filteredCollection, IssueEntityType.REQUIREMENT_VERSION_TYPE.name());
	}

	private GridResponse getKnownIssuesData(IssueEntityType entityType, Long id, PagingAndSorting paging) {
		PagedCollectionHolder<List<IssueOwnership<RemoteIssueDecorator>>> filteredCollection;

		try {
			switch (entityType) {
				case TEST_CASE_TYPE:
					filteredCollection = bugTrackersLocalService.findSortedIssueOwnershipForTestCase(id, paging);
					break;
				case CAMPAIGN_FOLDER_TYPE:
					filteredCollection = bugTrackersLocalService.findSortedIssueOwnershipForCampaignFolder(id, paging);
					break;
				case CAMPAIGN_TYPE:
					filteredCollection = bugTrackersLocalService.findSortedIssueOwnershipsForCampaign(id, paging);
					break;
				case ITERATION_TYPE:
					filteredCollection = bugTrackersLocalService.findSortedIssueOwnershipForIteration(id, paging);
					break;
				case TEST_SUITE_TYPE:
					filteredCollection = bugTrackersLocalService.findSortedIssueOwnershipsForTestSuite(id, paging);
					break;
				case EXECUTION_TYPE:
					filteredCollection = bugTrackersLocalService.findSortedIssueOwnershipsforExecution(id, paging);
					break;
				case EXECUTION_STEP_TYPE:
					filteredCollection = bugTrackersLocalService.findSortedIssueOwnerShipsForExecutionStep(id, paging);
					break;
				default:
					String error = "BugTrackerController : cannot fetch issues for unknown entity type '" + entityType + "'";
					if (LOGGER.isErrorEnabled()) {
						LOGGER.error(error);
					}
					throw new IllegalArgumentException(error);
			}
		}
		// no credentials exception are okay, the rest is to be treated as usual
		catch (BugTrackerNoCredentialsException | NullArgumentException exception) {
			filteredCollection = makeEmptyIssueDecoratorCollectionHolder(entityType.name(), id, exception, paging);
		}

		return bugTrackerServiceHelper.createModelBuilderFor(entityType).buildGridResponse(filteredCollection, entityType.name());
	}

	private AuthenticationStatus checkStatus(BugTracker bugTracker, long projectId) {
		AuthenticationStatus status = bugTrackersLocalService.checkBugTrackerStatus(projectId);

		if (status == AuthenticationStatus.AUTHENTICATED) {
			try {
				Credentials credentials = bugTrackersService.getCredentials(bugTracker);
				bugTrackersService.testCredentials(bugTracker, credentials);
			} catch (BugTrackerManagerException ex) {
				throw new BugTrackerManagerActionException(ex);
			}
		}

		return status;
	}

	private static final class IssueDisplaySorting implements PagingAndSorting {

		private final GridRequest request;

		private IssueDisplaySorting(final GridRequest request) {
			this.request = request;
		}

		@Override
		public int getFirstItemIndex() {
			return request.getPage();
		}

		@Override
		public String getSortedAttribute() {
			String attribute = "";
			if (!request.getSort().isEmpty()) {
				attribute = SORTING_DEFAULT_ATTRIBUTE;
			}
			return attribute;
		}

		@Override
		public int getPageSize() {
			return request.getSize();
		}

		@Override
		public boolean shouldDisplayAll() {
			return getPageSize() < 0;
		}

		/**
		 * @see org.squashtest.tm.core.foundation.collection.Sorting#getSortOrder()
		 */
		@Override
		public SortOrder getSortOrder() {
			GridSort.SortDirection direction = request.getSort().get(0).getDirection();
			if (direction.equals(GridSort.SortDirection.ASC)) {
				return SortOrder.ASCENDING;
			} else {
				return SortOrder.DESCENDING;
			}
		}

		@Override
		public Pageable toPageable() {
			return SpringPaginationUtils.toPageable(this);
		}

	}

	private PagedCollectionHolder<List<RequirementVersionIssueOwnership<RemoteIssueDecorator>>> makeEmptyIssueDecoratorCollectionHolderForRequirement(
		String entityName, Long entityId, Exception cause, PagingAndSorting paging) {
		LOGGER.trace("BugTrackerController : fetching known issues for  " + HtmlUtils.htmlEscape(entityName) + " " + entityId
			+ " failed, exception : ", cause);
		List<RequirementVersionIssueOwnership<RemoteIssueDecorator>> emptyList = new LinkedList<>();
		return new PagingBackedPagedCollectionHolder<>(paging, 0, emptyList);
	}

	private PagedCollectionHolder<List<IssueOwnership<RemoteIssueDecorator>>> makeEmptyIssueDecoratorCollectionHolder(
		String entityName, Long entityId, Exception cause, PagingAndSorting paging) {
		LOGGER.trace("BugTrackerController : fetching known issues for  " + HtmlUtils.htmlEscape(entityName) + " " + entityId
			+ " failed, exception : ", cause);
		List<IssueOwnership<RemoteIssueDecorator>> emptyList = new LinkedList<>();
		return new PagingBackedPagedCollectionHolder<>(paging, 0, emptyList);
	}

	static class NewOslcIssueRequestBody {
		String issueId;

		public String getIssueId() {
			return issueId;
		}

		public void setIssueId(String issueId) {
			this.issueId = issueId;
		}
	}
}
