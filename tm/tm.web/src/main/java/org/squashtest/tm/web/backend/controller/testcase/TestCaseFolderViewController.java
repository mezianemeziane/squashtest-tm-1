/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase;

import com.google.common.collect.Lists;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService;
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseFolderDto;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportDashboard;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;
import org.squashtest.tm.web.backend.model.builder.JsonCustomReportDashboardBuilder;

import javax.inject.Named;
import javax.inject.Provider;
import java.util.Collections;
import java.util.Locale;


@RestController
@RequestMapping("/backend/test-case-folder-view")
public class TestCaseFolderViewController {

	private TestCaseDisplayService testCaseDisplayService;
	private final CustomReportLibraryNodeService customReportLibraryNodeService;
	@Named("customReport.dashboardBuilder")
	private final Provider<JsonCustomReportDashboardBuilder> builderProvider;
	private final TestCaseLibraryNavigationService testCaseLibraryNavigationService;

	public TestCaseFolderViewController(
		TestCaseDisplayService testCaseDisplayService,
		CustomReportLibraryNodeService customReportLibraryNodeService,
		Provider<JsonCustomReportDashboardBuilder> builderProvider,
		TestCaseLibraryNavigationService testCaseLibraryNavigationService
	) {
		this.testCaseDisplayService = testCaseDisplayService;
		this.builderProvider = builderProvider;
		this.customReportLibraryNodeService = customReportLibraryNodeService;
		this.testCaseLibraryNavigationService = testCaseLibraryNavigationService;
	}

	@RequestMapping(value = "/{testCaseFolderId}", method = RequestMethod.GET)
	public TestCaseFolderDto getTestCaseView(@PathVariable long testCaseFolderId, Locale locale) {
		TestCaseFolderDto dto = testCaseDisplayService.getTestCaseFolderView(testCaseFolderId);

		if (dto.isShouldShowFavoriteDashboard()) {
			if (dto.isCanShowFavoriteDashboard()) {
				EntityReference library = new EntityReference(EntityType.TEST_CASE_FOLDER, testCaseFolderId);
				CustomReportDashboard dashboard = customReportLibraryNodeService.findCustomReportDashboardById(dto.getFavoriteDashboardId());
				JsonCustomReportDashboard jsonDashboard = builderProvider.get()
					.build(dto.getFavoriteDashboardId(), dashboard, locale, Collections.singletonList(library), false, Workspace.TEST_CASE);
				dto.setDashboard(jsonDashboard);
			}
		} else {
			dto.setStatistics(testCaseLibraryNavigationService.getStatisticsForSelection(Collections.emptyList(), Lists.newArrayList(testCaseFolderId)));
		}

		return dto;
	}

}
