/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.config;

import java.util.EnumSet;

/**
 * Contains a list of all SPA root page urls. It will be used to :
 * - Forward all request matching to these url to predefined index url. So user can access directly to deep link into client app
 * - Bypass spring security auth on these urls to allow initial serving of the app.
 *
 * DO NOT PUT controllers or api urls here. No urls beginning by /backend or /api !!!
 */
public enum AngularAppPageUrls {

	BASE_PAGE("/"),
	LOGIN_PAGE("/login/**"),
	LOGOUT_PAGE("/logout/**"),
	ADMINISTRATION_WORKSPACE("/administration-workspace/**"),
	SEARCH("/search/**"),
	HOME_WORKSPACE("/home-workspace/**"),
	REQUIREMENT_WORKSPACE("/requirement-workspace/**"),
	TEST_CASE_WORKSPACE("/test-case-workspace/**"),
	DETAILED_TEST_STEP("/detailed-test-step/**"),
	CAMPAIGN_WORKSPACE("/campaign-workspace/**"),
	EXECUTION("/execution/**"),
	// Required to allow proper display of application with legacy urls
	EXECUTION_LEGACY_LINK("/executions/**"),
	EXECUTION_RUNNER("/execution-runner/**"),
	MODIF_DURING_EXEC("/modif-during-exec/**"),
	CUSTOM_REPORT_WORKSPACE("/custom-report-workspace/**"),
	AUTOMATION_WORKSPACE("/automation-workspace/**"),
	REQUIREMENT_MULTI_VERSIONS("/requirement/**"),
	USER_ACCOUNT("/user-account/**"),
	OAUTH_PAGES("/oauth/**");

	private static final String defaultIndexPage = "/index";
	public final String urlPattern;
	public final String viewName;

	AngularAppPageUrls(String urlPattern) {
		this(urlPattern, defaultIndexPage);
	}

	AngularAppPageUrls(String urlPattern, String viewName) {
		this.urlPattern = urlPattern;
		this.viewName = viewName;
	}

	public static String[] getAllUrlsPatterns() {
		return EnumSet.allOf(AngularAppPageUrls.class).stream()
			.map(angularAppPageUrls -> angularAppPageUrls.urlPattern)
			.toArray(String[]::new);
	}
}
