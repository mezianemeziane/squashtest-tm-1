/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.campaign.CreatedTestPlanItems;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.campaign.TestSuiteTestPlanManagerService;
import org.squashtest.tm.service.display.campaign.TestSuiteDisplayService;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.repository.display.IssueDisplayDao;
import org.squashtest.tm.web.backend.controller.campaign.IterationTestPlanManagerController.ItpiMassEditPatch;

import java.util.List;

@RestController
@RequestMapping("backend/test-suite")
public class TestSuiteTestPlanManagerController {

	private final TestSuiteTestPlanManagerService testSuiteTestPlanManagerService;
	private final IterationTestPlanManagerService iterationTestPlanManagerService;
	private final TestSuiteDisplayService testSuiteDisplayService;
	private final IssueDisplayDao issueDisplayDao;

	public TestSuiteTestPlanManagerController(TestSuiteTestPlanManagerService testSuiteTestPlanManagerService,
											  IterationTestPlanManagerService iterationTestPlanManagerService,
											  TestSuiteDisplayService testSuiteDisplayService,
											  IssueDisplayDao issueDisplayDao) {
		this.testSuiteTestPlanManagerService = testSuiteTestPlanManagerService;
		this.iterationTestPlanManagerService = iterationTestPlanManagerService;
		this.testSuiteDisplayService = testSuiteDisplayService;
		this.issueDisplayDao = issueDisplayDao;
	}

	@RequestMapping(value = "{testSuiteId}/test-plan", method = RequestMethod.POST)
	public GridResponse findIterationTestPlan(@PathVariable Long testSuiteId, @RequestBody GridRequest gridRequest) {
		return this.testSuiteDisplayService.findTestPlan(testSuiteId, gridRequest);
	}

	@ResponseBody
	@RequestMapping(value = "/{testSuiteId}/test-plan-items", method = RequestMethod.POST)
	public CreatedTestPlanItems addTestCasesToTestSuite(
		@RequestBody TestCaseIdForm testCaseIdForm,
		@PathVariable long testSuiteId) {
		return testSuiteTestPlanManagerService.addTestCasesToIterationAndTestSuite(testCaseIdForm.getTestCaseIds(), testSuiteId);
	}

	@ResponseBody
	@RequestMapping(value = "/{testSuiteId}/test-plan-items/detach/{testPlanItemsIds}", method = RequestMethod.DELETE)
	public DeletedTestPlanItemReport detachTestCaseFromTestSuite(@PathVariable Long testSuiteId,
											@PathVariable List<Long> testPlanItemsIds) {
		testSuiteTestPlanManagerService.detachTestPlanFromTestSuite(testPlanItemsIds, testSuiteId);
		DeletedTestPlanItemReport report = new DeletedTestPlanItemReport();
		report.setNbIssues(issueDisplayDao.countIssuesByTestSuiteId(testSuiteId));
		return report;
	}

	@ResponseBody
	@RequestMapping(value = "/{testSuiteId}/test-plan-items/remove/{testPlanItemsIds}", method = RequestMethod.DELETE)
	public DeletedTestPlanItemReport removeTestCaseFromTestSuiteAndIteration(@PathVariable Long testSuiteId,
														@PathVariable List<Long> testPlanItemsIds) {
		testSuiteTestPlanManagerService.detachTestPlanFromTestSuiteAndRemoveFromIteration(testPlanItemsIds, testSuiteId);
		DeletedTestPlanItemReport report = new DeletedTestPlanItemReport();
		report.setNbIssues(issueDisplayDao.countIssuesByTestSuiteId(testSuiteId));
		return report;
	}

	@ResponseBody
	@RequestMapping(value = "{testSuiteId}/test-plan/execution/{executionIds}", method = RequestMethod.DELETE)
	public DeletedExecutionFromTestPlanItemReport removeExecutionsFromTestPlanItem(@PathVariable("executionIds") List<Long> executionIds, @PathVariable long testSuiteId) {
		Long iterationId = 	testSuiteDisplayService.findIterationIdByTestsuiteId(testSuiteId);
		iterationTestPlanManagerService.removeExecutionsFromTestPlanItem(executionIds, iterationId);
		DeletedExecutionFromTestPlanItemReport report = new DeletedExecutionFromTestPlanItemReport();
		report.setNbIssues(issueDisplayDao.countIssuesByTestSuiteId(testSuiteId));
		return report;
	}

	@ResponseBody
	@RequestMapping(value = "/test-plan/{itemIds}/mass-update")
	public void massUpdate(@PathVariable("itemIds") List<Long> itemIds,
						   @RequestBody ItpiMassEditPatch patch){
		if (patch.getExecutionStatus() != null) {
			iterationTestPlanManagerService.forceExecutionStatus(itemIds, patch.getExecutionStatus());
		}
		if (patch.isChangeAssignee()) {
			if (patch.getAssignee() == null) {
				iterationTestPlanManagerService.removeTestPlanItemsAssignments(itemIds);
			} else {
				iterationTestPlanManagerService.assignUserToTestPlanItems(itemIds, patch.getAssignee());
			}
		}
	}



	/**
	 * Handles internal drag and drop into grid
	 *
	 * @param testSuiteId : the test suite owning the moving test plan items
	 * @param itemIds the ids of the items we are trying to move
	 * @param newIndex  the new position of the first of them
	 */
	@ResponseBody
	@RequestMapping(value = "/{testSuiteId}/test-plan/{itemIds}/position/{newIndex}", method = RequestMethod.POST)
	public void moveTestPlanItems(@PathVariable long testSuiteId,
								  @PathVariable List<Long> itemIds,
								  @PathVariable int newIndex) {
		 testSuiteTestPlanManagerService.changeTestPlanPosition(testSuiteId, newIndex, itemIds);
	}

	static class DeletedTestPlanItemReport {
		private long nbIssues;

		public long getNbIssues() {
			return nbIssues;
		}

		public void setNbIssues(long nbIssues) {
			this.nbIssues = nbIssues;
		}
	}

	static class DeletedExecutionFromTestPlanItemReport {
		private long nbIssues;

		public long getNbIssues() {
			return nbIssues;
		}

		public void setNbIssues(long nbIssues) {
			this.nbIssues = nbIssues;
		}
	}
}
