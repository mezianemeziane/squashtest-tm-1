/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.campaign.CampaignTestPlanManagerService;
import org.squashtest.tm.service.display.campaign.CampaignDisplayService;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

import java.util.List;
import java.util.Map;

/**
 * @author qtran - created on 02/02/2021
 */
@RestController
@RequestMapping("backend/campaign")
public class CampaignTestPlanManagerController {

	private final CampaignTestPlanManagerService campaignTestPlanManagerService;

	private final CampaignDisplayService campaignDisplayService;

	public CampaignTestPlanManagerController(CampaignTestPlanManagerService campaignTestPlanManagerService,
											 CampaignDisplayService campaignDisplayService) {
		this.campaignTestPlanManagerService = campaignTestPlanManagerService;
		this.campaignDisplayService = campaignDisplayService;
	}

	@ResponseBody
	@PostMapping(value = "/{campaignId}/test-plan")
	public GridResponse findCampaignTestPlan(@PathVariable Long campaignId, @RequestBody GridRequest gridRequest) {
		return campaignDisplayService.findTestPlan(campaignId, gridRequest);
	}

	/**
	 * Handles internal drag and drop into grid
	 *
	 * @param campaignId : the campaign owning the moving test plan items
	 * @param itemIds the ids of the items to be moved
	 * @param newIndex  the new position of the first of them
	 */
	@PostMapping(value = "/{campaignId}/test-plan/{itemIds}/position/{newIndex}")
	@ResponseBody
	public void moveTestPlanItems(@PathVariable long campaignId,
								  @PathVariable List<Long> itemIds,
								  @PathVariable int newIndex) {
		campaignTestPlanManagerService.moveTestPlanItems(campaignId, newIndex, itemIds);
	}

	@ResponseBody
	@RequestMapping(value = "/{campaignId}/test-plan-items", method = RequestMethod.POST)
	public Map<String,Object> addTestCasesToCampaign(@RequestBody TestCaseIdForm testCaseIdForm,
													 @PathVariable long campaignId) {
		return campaignTestPlanManagerService.addTestCasesToCampaignTestPlan(testCaseIdForm.getTestCaseIds(), campaignId);
	}

	@ResponseBody
	@DeleteMapping(value = "/{campaignId}/test-plan/{testPlanItemsIds}")
	public void removeTestPlanItemsFromIteration(@PathVariable("testPlanItemsIds") List<Long> testPlanItemsIds,
												 @PathVariable long campaignId) {
		campaignTestPlanManagerService.removeTestPlanItems(campaignId, testPlanItemsIds);
	}

	@ResponseBody
	@PostMapping(value = "/test-plan/{itemIds}/mass-update")
	public void massUpdate(@PathVariable("itemIds") List<Long> itemIds,
						   @RequestBody CtpiMassEditPatch patch) {
		if (patch.isChangeAssignee()) {
			if (patch.getAssignee() == null) {
				campaignTestPlanManagerService.removeTestPlanItemsAssignments(itemIds);
			} else {
				campaignTestPlanManagerService.assignUserToTestPlanItems(itemIds, patch.getAssignee());
			}
		}
	}

	static class CampaignTestPlanItemPatch {
		private Long assignee;
		private Long datasetId;

		public Long getAssignee() {
			return assignee;
		}

		public void setAssignee(Long assignee) {
			this.assignee = assignee;
		}

		public Long getDatasetId() {
			return datasetId;
		}

		public void setDatasetId(Long datasetId) {
			this.datasetId = datasetId;
		}
	}

	static class CtpiMassEditPatch extends CampaignTestPlanItemPatch {
		private boolean changeAssignee;

		public boolean isChangeAssignee() {
			return changeAssignee;
		}

		public void setChangeAssignee(boolean changeAssignee) {
			this.changeAssignee = changeAssignee;
		}
	}
}
