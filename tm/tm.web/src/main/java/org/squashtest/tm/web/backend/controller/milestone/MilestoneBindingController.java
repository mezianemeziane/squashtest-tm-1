/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.milestone;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.milestone.MilestoneRange;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.exception.milestone.MilestoneLabelAlreadyExistsException;
import org.squashtest.tm.service.display.milestone.MilestoneDisplayService;
import org.squashtest.tm.service.internal.display.dto.*;
import org.squashtest.tm.service.internal.project.ProjectHelper;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.service.milestone.CustomMilestoneManager;
import org.squashtest.tm.service.milestone.MilestoneBindingManagerService;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.web.backend.controller.form.model.MilestoneFormModel;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.*;

import static org.squashtest.tm.api.security.acls.Roles.ROLE_ADMIN;

@Controller
@RequestMapping("/backend/milestone-binding")
public class MilestoneBindingController {

	private CustomMilestoneManager customMilestoneManager;
	private MilestoneBindingManagerService milestoneBindingManagerService;
	private PermissionEvaluationService permissionEvaluationService;
	private ProjectFinder projectFinder;
	private MilestoneDisplayService milestoneDisplayService;

	@Inject
	MilestoneBindingController(CustomMilestoneManager customMilestoneManager,
							   MilestoneBindingManagerService milestoneBindingManagerService,
							   PermissionEvaluationService permissionEvaluationService,
							   ProjectFinder projectFinder,
							   MilestoneDisplayService milestoneDisplayService) {
		this.customMilestoneManager = customMilestoneManager;
		this.milestoneBindingManagerService = milestoneBindingManagerService;
		this.permissionEvaluationService = permissionEvaluationService;
		this.projectFinder = projectFinder;
		this.milestoneDisplayService = milestoneDisplayService;
	}


	@RequestMapping(value = "/project/{projectId}/bind-milestones/{milestoneIds}", method = RequestMethod.POST)
	@ResponseBody
	public void bindMilestonesToProject(@PathVariable Long projectId, @PathVariable List<Long> milestoneIds) {
		milestoneBindingManagerService.bindMilestonesToProject(milestoneIds, projectId);
	}

	@RequestMapping(value = "/project/{projectId}/bind-milestones-to-project-and-objects/{milestoneIds}", method = RequestMethod.POST)
	@ResponseBody
	public void bindMilestonesToProjectAndObjects(@PathVariable Long projectId, @PathVariable List<Long> milestoneIds) {
		milestoneBindingManagerService.bindMilestonesToProjectAndBindObject(projectId, milestoneIds);
	}

	@RequestMapping(value = "/project/{projectId}/unbind-milestones/{milestoneIds}", method = RequestMethod.DELETE)
	@ResponseBody
	public void unbindMilestonesFromProject(@PathVariable long projectId, @PathVariable List<Long> milestoneIds) {
		milestoneBindingManagerService.unbindMilestonesFromProject(milestoneIds, projectId);
	}

	@RequestMapping(value = "/{milestoneId}/bind-projects/{projectIds}", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, List<ProjectInfoForMilestoneAdminViewDto>>bindProjectsToMilestone(@PathVariable Long milestoneId, @PathVariable List<Long> projectIds) {
		milestoneBindingManagerService.bindProjectsToMilestone(projectIds, milestoneId);
		MilestoneAdminViewDto milestone = milestoneDisplayService.getMilestoneView(milestoneId);
		return Collections.singletonMap("boundProjectsInformation", milestone.getBoundProjectsInformation());
	}

	@RequestMapping(value = "/{milestoneId}/unbind-projects/{projectIds}", method = RequestMethod.DELETE)
	@ResponseBody
	public void unbindProjectsFromMilestone(@PathVariable Long milestoneId, @PathVariable List<Long> projectIds) {
		milestoneBindingManagerService.unbindProjectsFromMilestone(projectIds, milestoneId);
	}

	@RequestMapping(value = "/{milestoneId}/unbind-projects-and-keep-in-perimeter/{projectIds}", method = RequestMethod.DELETE)
	@ResponseBody
	public void unbindProjectFromMilestoneKeepInPerimeter(@PathVariable("milestoneId") Long milestoneId,
														  @PathVariable("projectIds") List<Long> projectIds) {
		milestoneBindingManagerService.unbindProjectsFromMilestoneKeepInPerimeter(projectIds, milestoneId);
	}

	@RequestMapping(value = "/{milestoneId}/bindable-projects", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<BindableProjectToMilestoneDto>> getBindableProjectsForMilestone(@PathVariable long milestoneId) {
		Collection<GenericProject> projects = milestoneBindingManagerService.getAllBindableProjectForMilestone(milestoneId);

		List<BindableProjectToMilestoneDto> bindableProjects = new ArrayList<>();
		projects.forEach(project -> {
			BindableProjectToMilestoneDto bindableProjectDto = new BindableProjectToMilestoneDto();
			bindableProjectDto.setId(project.getId());
			bindableProjectDto.setName(project.getName());
			bindableProjectDto.setLabel(project.getLabel());
			bindableProjectDto.setTemplate(ProjectHelper.isTemplate(project));
			bindableProjects.add(bindableProjectDto);
		});

		return Collections.singletonMap("bindableProjects", bindableProjects);
	}

	@RequestMapping(value = "/project/{projectId}/create-milestone-and-bind-to-project", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> createAndBindMilestoneToProject(@PathVariable Long projectId, @Valid @RequestBody MilestoneFormModel milestoneFormModel) {
		Map<String, Object> response = new HashMap<>();
		Milestone milestone = createMilestone(milestoneFormModel);
		MilestoneProjectViewDto milestoneProjectViewDto = prepareServerResponseAfterCreatingMilestone(milestone);
		milestoneProjectViewDto.setMilestoneBoundToOneObjectOfProject(false);
		response.put("milestone", milestoneProjectViewDto);
		milestoneBindingManagerService.bindMilestonesToProject(Collections.singletonList(milestone.getId()), projectId);

		return response;
	}


	@RequestMapping(value = "/project/{projectId}/create-milestone-and-bind-to-project-and-objects", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> createAndBindMilestoneToProjectAndObjects(@PathVariable Long projectId, @Valid @RequestBody MilestoneFormModel milestoneFormModel) {
		Map<String, Object> response = new HashMap<>();
		Milestone milestone = createMilestone(milestoneFormModel);
		MilestoneProjectViewDto milestoneProjectViewDto = prepareServerResponseAfterCreatingMilestone(milestone);
		milestoneProjectViewDto.setMilestoneBoundToOneObjectOfProject(true);
		response.put("milestone", milestoneProjectViewDto);
		milestoneBindingManagerService.bindMilestonesToProjectAndBindObject(projectId, Collections.singletonList(milestone.getId()));

		return response;
	}

	private Milestone createMilestone(MilestoneFormModel milestoneFormModel) {
		Milestone milestone = milestoneFormModel.getMilestone();
		setRange(milestone);
		setPerimeter(milestone);
		try {
			customMilestoneManager.addMilestone(milestone);
		} catch (MilestoneLabelAlreadyExistsException ex) {
			throw new NameAlreadyInUseException("Milestone", milestoneFormModel.getLabel(), "label");
		}

		return milestone;
	}

	private MilestoneProjectViewDto prepareServerResponseAfterCreatingMilestone(Milestone milestone) {
		MilestoneProjectViewDto response = new MilestoneProjectViewDto();
		MilestoneDto milestoneDto = new MilestoneDto();
		milestoneDto.setId(milestone.getId());
		milestoneDto.setLabel(milestone.getLabel());
		String sanitizedDescription = HTMLCleanupUtils.htmlToText(milestone.getDescription());
		milestoneDto.setDescription(sanitizedDescription);
		milestoneDto.setEndDate(milestone.getEndDate());
		milestoneDto.setOwnerFirstName(milestone.getOwner().getFirstName());
		milestoneDto.setOwnerLastName(milestone.getOwner().getLastName());
		milestoneDto.setOwnerLogin(milestone.getOwner().getLogin());
		milestoneDto.setStatus(milestone.getStatus().name());
		milestoneDto.setRange(milestone.getRange().name());
		response.setMilestone(milestoneDto);
		return response;
	}

	private void setRange(Milestone milestone) {
		if (permissionEvaluationService.hasRole(ROLE_ADMIN)) {
			milestone.setRange(MilestoneRange.GLOBAL);
		} else {
			milestone.setRange(MilestoneRange.RESTRICTED);
		}
	}

	private void setPerimeter(Milestone milestone) {
		if (!permissionEvaluationService.hasRole(ROLE_ADMIN)) {
			List<GenericProject> projects = projectFinder.findAllICanManage();
			milestone.addProjectsToPerimeter(projects);
		}
	}


}
