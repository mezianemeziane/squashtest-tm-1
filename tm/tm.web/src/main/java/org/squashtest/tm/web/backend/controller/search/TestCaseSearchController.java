/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.search;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import org.squashtest.tm.service.display.search.*;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

import java.util.List;

@RestController
@RequestMapping("backend/search/test-case")
public class TestCaseSearchController {

	private final TestCaseSearchService testCaseSearchService;

	private final TestCaseSearchGridDisplayService testCaseResearchGridService;

	private final TestCaseSearchInputDataProvider inputDataProvider;

	private final TestCaseThroughRequirementSearchService testCaseThroughRequirementSearchService;

	public TestCaseSearchController(
		TestCaseSearchService testCaseSearchService,
		TestCaseSearchGridDisplayService testCaseResearchGridService,
		TestCaseSearchInputDataProvider inputDataProvider,
		TestCaseThroughRequirementSearchService testCaseThroughRequirementSearchService
	) {
		this.testCaseSearchService = testCaseSearchService;
		this.testCaseResearchGridService = testCaseResearchGridService;
		this.inputDataProvider = inputDataProvider;
		this.testCaseThroughRequirementSearchService = testCaseThroughRequirementSearchService;
	}

	@GetMapping
	public TestCaseSearchInputData getTestCaseResearchInputData() {
		return inputDataProvider.provideTestCaseResearchInputData();
	}

	@PostMapping
	public GridResponse searchTestCase(@RequestBody GridRequest gridRequest) {
		ResearchResult result = testCaseSearchService.search(gridRequest);
		return testCaseResearchGridService.fetchResearchRows(result);
	}

	@PostMapping(value = "/by-requirement")
	public GridResponse searchTestCaseByRequirement(@RequestBody GridRequest gridRequest) {
		ResearchResult tcResult = testCaseThroughRequirementSearchService.search(gridRequest);
		return testCaseResearchGridService.fetchResearchRows(tcResult);
	}

}
