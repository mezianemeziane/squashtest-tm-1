/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.project;

import com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.api.plugin.EntityReference;
import org.squashtest.tm.api.plugin.EntityType;
import org.squashtest.tm.api.plugin.PluginValidationException;
import org.squashtest.tm.api.template.TemplateConfigurablePlugin;
import org.squashtest.tm.api.wizard.WorkspaceWizard;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.service.display.project.ProjectDisplayService;
import org.squashtest.tm.service.internal.display.dto.BindMilestoneToProjectDialogData;
import org.squashtest.tm.service.internal.display.dto.ProjectPluginDto;
import org.squashtest.tm.service.internal.display.dto.ProjectViewDto;
import org.squashtest.tm.service.internal.display.dto.TestAutomationProjectDto;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.web.backend.manager.wizard.WorkspaceWizardManager;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
@RequestMapping("/backend/project-view")
public class ProjectViewController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectViewController.class);
    private static final String STATUS_ERROR = "ERROR";
    private static final String STATUS_OK = "OK";

    private final ProjectDisplayService projectViewDisplayService;
    private final GenericProjectManagerService projectManager;
    private final WorkspaceWizardManager pluginManager;

    @Autowired(required = false)
    private final Collection<TemplateConfigurablePlugin> templatePlugins = Collections.emptyList();

    @Inject
    ProjectViewController(ProjectDisplayService projectViewDisplayService,
						  GenericProjectManagerService projectManager,
						  WorkspaceWizardManager pluginManager) {
        this.projectViewDisplayService = projectViewDisplayService;
        this.projectManager = projectManager;
        this.pluginManager = pluginManager;
	}

    @GetMapping("/{projectId}")
    public ProjectViewDto getProjectView(@PathVariable long projectId) {
        ProjectViewDto dto = projectViewDisplayService.getProjectView(projectId);
        dto.setAvailablePlugins(getAvailablePlugins(projectId, dto.isTemplate()));
        return dto;
    }

    @GetMapping("/{projectId}/available-milestones")
    public BindMilestoneToProjectDialogData getAvailableMilestones(@PathVariable long projectId) {
        return projectViewDisplayService.findAvailableMilestones(projectId);
    }

    @GetMapping("/{projectId}/unbound-parties")
    public UnboundPartiesResponse getUnboundParties(@PathVariable long projectId) {
        List<Party> partyList = projectManager.findPartyWithoutPermissionByProject(projectId);

        List<UnboundParty> users = new ArrayList<>();
        List<UnboundParty> teams = new ArrayList<>();

        for (Party p : partyList) {
            final boolean isUser = User.class.isAssignableFrom(p.getClass());
            final Long id = p.getId();
            final String label = HTMLCleanupUtils.cleanAndUnescapeHTML(p.getName());
            final UnboundParty unboundParty = new UnboundParty(id, label);

            if (isUser) {
                users.add(unboundParty);
            } else {
                teams.add(unboundParty);
            }
        }

        return new UnboundPartiesResponse(users, teams);
    }

    static class UnboundPartiesResponse {
        private List<UnboundParty> users;
        private List<UnboundParty> teams;

        public UnboundPartiesResponse(List<UnboundParty> users, List<UnboundParty> teams) {
            this.users = users;
            this.teams = teams;
        }

		public List<UnboundParty> getUsers() {
			return users;
		}

		public void setUsers(List<UnboundParty> users) {
			this.users = users;
		}

		public List<UnboundParty> getTeams() {
			return teams;
		}

		public void setTeams(List<UnboundParty> teams) {
			this.teams = teams;
		}
	}

    static class UnboundParty {
        private Long id;
        private String label;

        public UnboundParty(Long id, String label) {
            this.id = id;
            this.label = label;
        }

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}
	}

    @GetMapping("/{projectId}/available-ta-projects")
    @ResponseBody
    public Map<String, List<TestAutomationProjectDto>> getAvailableTAProjects(@PathVariable long projectId) {
        final List<TestAutomationProjectDto> dtos = projectManager.findAllAvailableTaProjects(projectId).stream()
                .map((TestAutomationProject taProject) ->
					TestAutomationProjectDto.fromRemoteTestAutomationProject(taProject, projectId))
                .collect(Collectors.toList());
        return Collections.singletonMap("taProjects", dtos);
    }

    private List<ProjectPluginDto> getAvailablePlugins(long projectId, boolean projectIsTemplate) {
        Collection<WorkspaceWizard> plugins = pluginManager.findAll();
        return toPluginDtos(projectId, plugins, projectIsTemplate);
    }

    private List<ProjectPluginDto> toPluginDtos(long projectId, Collection<WorkspaceWizard> plugins, boolean isTemplate) {
        return IntStream.range(0, plugins.size())
                .mapToObj(index -> toPluginDto(projectId, Iterables.get(plugins, index), index + 1, isTemplate))
                .collect(Collectors.toList());
    }

    private ProjectPluginDto toPluginDto(Long projectId,
                                         WorkspaceWizard plugin,
                                         int pluginIndex,
                                         boolean isTemplate) {
        EntityReference context = new EntityReference(EntityType.PROJECT, projectId);

        ProjectPluginDto projectPluginDto = new ProjectPluginDto(plugin);
        projectPluginDto.setIndex(pluginIndex);
        projectPluginDto.setEnabled(pluginManager.isActivePlugin(plugin, projectId));
        projectPluginDto.setHasConf(pluginManager.hasConfiguration(plugin, projectId));
        projectPluginDto.setPluginType(plugin.getPluginType());

        updateConfigUrl(plugin, isTemplate, context, projectPluginDto);
        updatePluginStatus(plugin, context, projectPluginDto);

        return projectPluginDto;
    }

    private void updateConfigUrl(WorkspaceWizard plugin, boolean isTemplate, EntityReference context, ProjectPluginDto projectPluginDto) {
        if (isTemplate) {
            Optional<TemplateConfigurablePlugin> templatePlugin = findTemplatePlugin(plugin.getId());
            templatePlugin.ifPresent(templateConfigurablePlugin -> projectPluginDto.setConfigUrl(templateConfigurablePlugin.getTemplateConfigurationPath(context)));
        } else {
            projectPluginDto.setConfigUrl(plugin.getConfigurationPath(context));
        }
    }

    private Optional<TemplateConfigurablePlugin> findTemplatePlugin(String pluginId) {
        return templatePlugins.stream()
                .filter(tp -> tp.getId().equals(pluginId))
                .findFirst();
    }

    private void updatePluginStatus(WorkspaceWizard plugin, EntityReference context, ProjectPluginDto projectPluginDto) {
        try {
            plugin.validate(context);
            projectPluginDto.setStatus(STATUS_OK);
        } catch (PluginValidationException damnit) {
            projectPluginDto.setStatus(STATUS_ERROR);
            LOGGER.debug("Plugin validation failed for Plugin " + plugin.getName(), damnit);
        }
    }
}
