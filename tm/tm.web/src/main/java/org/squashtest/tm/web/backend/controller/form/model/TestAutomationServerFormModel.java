/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.form.model;

import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;

public class TestAutomationServerFormModel {
	private String name;
	private String description;
	private String baseUrl;
	private String kind;
	private boolean manualSlaveSelection;

	public TestAutomationServer getTestAutomationServer() {
		TestAutomationServerKind serverKind = TestAutomationServerKind.valueOf(kind);
		TestAutomationServer server = new TestAutomationServer(serverKind);
		server.setUrl(getBaseUrl());
		server.setName(getName());
		server.setDescription(getDescription());
		server.setManualSlaveSelection(isManualSlaveSelection());
		return server;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isManualSlaveSelection() {
		return manualSlaveSelection;
	}

	public void setManualSlaveSelection(boolean manualSlaveSelection) {
		this.manualSlaveSelection = manualSlaveSelection;
	}
}
