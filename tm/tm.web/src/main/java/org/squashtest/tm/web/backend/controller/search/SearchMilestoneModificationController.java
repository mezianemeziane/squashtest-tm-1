/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.search;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.service.requirement.RequirementVersionManagerService;
import org.squashtest.tm.service.testcase.TestCaseModificationService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("backend/search/milestones")
public class SearchMilestoneModificationController {

	private TestCaseModificationService testCaseModificationService;
	private RequirementVersionManagerService requirementVersionManagerService;

	public SearchMilestoneModificationController(TestCaseModificationService testCaseModificationService,
												 RequirementVersionManagerService requirementVersionManagerService) {
		this.testCaseModificationService = testCaseModificationService;
		this.requirementVersionManagerService = requirementVersionManagerService;
	}

	@RequestMapping(value = "test-case/{testCaseIds}", method = RequestMethod.GET)
	@ResponseBody
	public MilestoneMassEdit getMilestoneMassEditForTc(@PathVariable List<Long> testCaseIds) {

		MilestoneMassEdit data = new MilestoneMassEdit();
		data.setCheckedIds(testCaseModificationService.findBindedMilestonesIdForMassModif(testCaseIds));
		Collection<Milestone> milestones = testCaseModificationService.findAssociableMilestonesForMassModif(testCaseIds);
		data.setMilestoneIds(milestones.stream().map(Milestone::getId).collect(Collectors.toCollection(ArrayList::new)));
		data.setSamePerimeter(testCaseModificationService.haveSamePerimeter(testCaseIds));
		return data;
	}

	@RequestMapping(value = "test-case", method = RequestMethod.POST)
	@ResponseBody
	public void bindMilestonesToTcs(@RequestBody MilestonesPatch milestonesPatch) {

		Collection<Long> bindedBefore = testCaseModificationService.findBindedMilestonesIdForMassModif(milestonesPatch.getBindableObjectIds());
		bindedBefore.removeAll(milestonesPatch.getMilestoneIds());

		for (Long testCaseId : milestonesPatch.getBindableObjectIds()) {
			testCaseModificationService.bindMilestones(testCaseId, milestonesPatch.getMilestoneIds());
			testCaseModificationService.unbindMilestones(testCaseId, bindedBefore);
		}
	}

	@RequestMapping(value = "requirement/{requirementVersionIds}", method = RequestMethod.GET)
	@ResponseBody
	public MilestoneMassEdit getMilestoneMassEditForRequirement(@PathVariable List<Long> requirementVersionIds) {

		MilestoneMassEdit data = new MilestoneMassEdit();
		data.setCheckedIds(requirementVersionManagerService.findBindedMilestonesIdForMassModif(requirementVersionIds));
		Collection<Milestone> milestones = requirementVersionManagerService.findAssociableMilestonesForMassModif(requirementVersionIds);
		data.setMilestoneIds(milestones.stream().map(Milestone::getId).collect(Collectors.toCollection(ArrayList::new)));
		data.setSamePerimeter(requirementVersionManagerService.haveSamePerimeter(requirementVersionIds));
		return data;
	}

	@RequestMapping(value = "requirement", method = RequestMethod.POST)
	@ResponseBody
	public boolean bindMilestonesToReqVersions(@RequestBody MilestonesPatch milestonesPatch) {

		List<Long> reqVersionIds = milestonesPatch.getBindableObjectIds();
		List<Long> milestoneIds = milestonesPatch.getMilestoneIds();

		Collection<Long> bindedBefore = requirementVersionManagerService.findBindedMilestonesIdForMassModif(reqVersionIds);
		bindedBefore.removeAll(milestoneIds);

		boolean isOneVersionAlreadyBound = !milestoneIds.isEmpty()
			&& requirementVersionManagerService.isOneMilestoneAlreadyBindToAnotherRequirementVersion(reqVersionIds, milestoneIds);

		for (Long reqVersionId : reqVersionIds) {
			requirementVersionManagerService.bindMilestones(reqVersionId, milestoneIds);
			requirementVersionManagerService.unbindMilestones(reqVersionId, bindedBefore);
		}

		return isOneVersionAlreadyBound;
	}

	private static class MilestoneMassEdit {
		private Collection<Long> checkedIds = new ArrayList<>();
		private Collection<Long> milestoneIds = new ArrayList<>();
		private boolean samePerimeter;

		public Collection<Long> getCheckedIds() {
			return checkedIds;
		}

		public void setCheckedIds(Collection<Long> checkedIds) {
			this.checkedIds = checkedIds;
		}

		public Collection<Long> getMilestoneIds() {
			return milestoneIds;
		}

		public void setMilestoneIds(Collection<Long> milestoneIds) {
			this.milestoneIds = milestoneIds;
		}

		public boolean isSamePerimeter() {
			return samePerimeter;
		}

		public void setSamePerimeter(boolean samePerimeter) {
			this.samePerimeter = samePerimeter;
		}
	}

	private static class MilestonesPatch {
		private List<Long> milestoneIds = new ArrayList<>();
		private List<Long> bindableObjectIds = new ArrayList<>();

		public List<Long> getMilestoneIds() {
			return milestoneIds;
		}

		public void setMilestoneIds(List<Long> milestoneIds) {
			this.milestoneIds = milestoneIds;
		}

		public List<Long> getBindableObjectIds() {
			return bindableObjectIds;
		}

		public void setBindableObjectIds(List<Long> bindableObjectIds) {
			this.bindableObjectIds = bindableObjectIds;
		}
	}
}
