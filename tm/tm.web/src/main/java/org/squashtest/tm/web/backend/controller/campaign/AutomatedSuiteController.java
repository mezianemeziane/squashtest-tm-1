/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.service.display.campaign.AutomatedSuiteDisplayService;
import org.squashtest.tm.service.internal.display.dto.campaign.AutomatedSuiteExecutionReportDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

@Controller
@RequestMapping("backend/automated-suite/{automatedSuiteId}")
public class AutomatedSuiteController {

	private AutomatedSuiteDisplayService automatedSuiteDisplayService;

	public AutomatedSuiteController(AutomatedSuiteDisplayService automatedSuiteDisplayService) {
		this.automatedSuiteDisplayService = automatedSuiteDisplayService;
	}

	@ResponseBody
	@PostMapping(value = "/executions")
	public GridResponse findExecutionByAutomatedSuite(@PathVariable String automatedSuiteId, @RequestBody GridRequest request) {
		return automatedSuiteDisplayService.findExecutionByAutomatedSuiteID(automatedSuiteId, request);
	}

	@ResponseBody
	@GetMapping(value = "/report-urls")
	public AutomatedSuiteExecutionReportDto findReportUrlsByAutomatedSuite(@PathVariable String automatedSuiteId) {
		return automatedSuiteDisplayService.findReportUrlsByAutomatedSuite(automatedSuiteId);
	}
}
