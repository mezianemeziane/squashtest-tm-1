/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.squashtest.tm.domain.campaign.IterationStatus;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.service.campaign.TestSuiteModificationService;
import org.squashtest.tm.service.display.campaign.TestSuiteDisplayService;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

@RestController
@RequestMapping("backend/test-suite/{testSuiteId}")
public class TestSuiteModificationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestSuiteModificationController.class);

	private TestSuiteModificationService testSuiteModificationService;

	public TestSuiteModificationController(TestSuiteModificationService testSuiteModificationService) {
		this.testSuiteModificationService = testSuiteModificationService;
	}

	@ResponseBody
	@PostMapping(value = "/name")
	public void rename(@PathVariable Long testSuiteId, @RequestBody TestSuitePatch patch) {
		testSuiteModificationService.rename(testSuiteId, patch.getName());
	}

	@ResponseBody
	@PostMapping(value = "/description")
	public void changeDescription(@PathVariable Long testSuiteId, @RequestBody TestSuitePatch patch) {
		testSuiteModificationService.changeDescription(testSuiteId, patch.getDescription());
	}

	@ResponseBody
	@PostMapping(value = "/execution-status")
	public void changeExecutionStatus(@PathVariable Long testSuiteId, @RequestBody TestSuitePatch patch) {
		testSuiteModificationService.changeExecutionStatus(testSuiteId, ExecutionStatus.valueOf(patch.getExecutionStatus()));
	}



	static class TestSuitePatch {
		private String name;
		private String description;
		private String executionStatus;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getExecutionStatus() {
			return executionStatus;
		}

		public void setExecutionStatus(String executionStatus) {
			this.executionStatus = executionStatus;
		}
	}
}
