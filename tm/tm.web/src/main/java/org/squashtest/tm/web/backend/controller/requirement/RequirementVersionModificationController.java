/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.requirement;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.requirement.RequirementCriticality;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.service.display.requirements.RequirementDisplayService;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.requirement.RequirementVersionManagerService;

import java.util.List;

@Controller
@RequestMapping("backend/requirement-version/{requirementVersionId}")
public class RequirementVersionModificationController {

	private final RequirementVersionManagerService requirementVersionManager;

	private final RequirementDisplayService requirementDisplayService;

	public RequirementVersionModificationController(RequirementVersionManagerService requirementVersionManager,
													RequirementDisplayService requirementDisplayService) {
		this.requirementVersionManager = requirementVersionManager;
		this.requirementDisplayService = requirementDisplayService;
	}

	@ResponseBody
	@PostMapping(value = "/history")
	public GridResponse getHistory(@PathVariable Long requirementVersionId, @RequestBody GridRequest requestParam) {
		return requirementDisplayService.findCurrentVersionModificationHistoryByRequirementVersionId(requirementVersionId, requestParam);
	}

	@ResponseBody
	@PostMapping(value = "/name")
	public void rename(@PathVariable Long requirementVersionId, @RequestBody RequirementVersionPatch patch) {
		requirementVersionManager.rename(requirementVersionId, patch.getName());
	}

	@ResponseBody
	@PostMapping(value = "/reference")
	public void changeReference(@PathVariable Long requirementVersionId, @RequestBody RequirementVersionPatch patch) {
		requirementVersionManager.changeReference(requirementVersionId, patch.getReference());
	}

	@ResponseBody
	@PostMapping(value = "/description")
	public void changeDescription(@PathVariable Long requirementVersionId, @RequestBody RequirementVersionPatch patch) {
		requirementVersionManager.changeDescription(requirementVersionId, patch.getDescription());
	}

	@ResponseBody
	@PostMapping(value = "/category")
	public void changeCategory(@PathVariable Long requirementVersionId, @RequestBody RequirementVersionPatch patch) {
		requirementVersionManager.changeCategory(requirementVersionId, patch.getCategory());
	}

	@ResponseBody
	@PostMapping(value = "/criticality")
	public void changeCriticality(@PathVariable Long requirementVersionId, @RequestBody RequirementVersionPatch patch) {
		requirementVersionManager.changeCriticality(requirementVersionId, RequirementCriticality.valueOf(patch.getCriticality()));
	}

	@ResponseBody
	@PostMapping(value = "/status")
	public void changeStatus(@PathVariable Long requirementVersionId, @RequestBody RequirementVersionPatch patch) {
		requirementVersionManager.changeStatus(requirementVersionId, RequirementStatus.valueOf(patch.getStatus()));
	}

	@RequestMapping(value = "/milestones/{milestoneIds}", method = RequestMethod.POST)
	@ResponseBody
	public List<MilestoneDto> bindMilestones(@PathVariable long requirementVersionId, @PathVariable("milestoneIds") List<Long> milestoneIds) {
		requirementVersionManager.bindMilestones(requirementVersionId, milestoneIds);
		return requirementDisplayService.findBindableMilestones(requirementVersionId);
	}


	@ResponseBody
	@RequestMapping(value = "/milestones/{milestoneIds}", method = RequestMethod.DELETE)
	public List<MilestoneDto> unbindMilestones(@PathVariable long requirementVersionId, @PathVariable List<Long> milestoneIds) {
		requirementVersionManager.unbindMilestones(requirementVersionId, milestoneIds);
		return requirementDisplayService.findBindableMilestones(requirementVersionId);
	}

	static class RequirementVersionPatch {
		private String name;
		private String reference;
		private String description;
		private Long category;
		private String status;
		private String criticality;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getReference() {
			return reference;
		}

		public void setReference(String reference) {
			this.reference = reference;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public Long getCategory() {
			return category;
		}

		public void setCategory(Long category) {
			this.category = category;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getCriticality() {
			return criticality;
		}

		public void setCriticality(String criticality) {
			this.criticality = criticality;
		}
	}
}
