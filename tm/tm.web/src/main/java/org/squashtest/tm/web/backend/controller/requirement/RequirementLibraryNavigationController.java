/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.requirement;

import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.NodeWorkspace;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.requirement.*;
import org.squashtest.tm.exception.library.RightsUnsuficientsForOperationException;
import org.squashtest.tm.service.display.workspace.tree.MultipleHierarchyTreeBrowser;
import org.squashtest.tm.service.display.workspace.tree.TreeNodeCollectorService;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.library.LibraryNavigationService;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.requirement.RequirementLibraryNavigationService;
import org.squashtest.tm.service.requirement.RequirementStatisticsService;
import org.squashtest.tm.web.backend.controller.form.model.*;
import org.squashtest.tm.web.backend.controller.navigation.AbstractLibraryNavigationController;

import java.util.*;

@SuppressWarnings("rawtypes")
@RestController
@RequestMapping(path = "backend/requirement-tree")
public class RequirementLibraryNavigationController extends AbstractLibraryNavigationController<RequirementLibrary, RequirementFolder, RequirementLibraryNode> {

	private static final String ADD_REQUIREMENT_FOLDER = "add-requirement-folder";
	private static final String ADD_REQUIREMENT = "add-requirement";
	private final RequirementLibraryNavigationService requirementLibraryNavigationService;
	private final TreeNodeCollectorService treeNodeCollectorService;
	private final ActiveMilestoneHolder activeMilestoneHolder;
	private final RequirementStatisticsService requirementStatisticsService;

	public RequirementLibraryNavigationController(MultipleHierarchyTreeBrowser treeBrowser,
												  RequirementLibraryNavigationService requirementLibraryNavigationService,
												  TreeNodeCollectorService treeNodeCollectorService,
												  MessageSource messageSource,
												  ActiveMilestoneHolder activeMilestoneHolder,
												  RequirementStatisticsService requirementStatisticsService) {
		super(treeBrowser, messageSource);
		this.requirementLibraryNavigationService = requirementLibraryNavigationService;
		this.treeNodeCollectorService = treeNodeCollectorService;
		this.activeMilestoneHolder = activeMilestoneHolder;
		this.requirementStatisticsService = requirementStatisticsService;
	}


	@ResponseBody
	@RequestMapping(value = "/{destinationId}/content/paste", method = RequestMethod.POST)
	public void copyNodes(@RequestBody() NodeList nodeList, @PathVariable("destinationId") String destinationId) {
		NodeReference nodeReference = NodeReference.fromNodeId(destinationId);
		List<Long> copiedNodeIds = nodeList.getIds();

		try {
			switch (nodeReference.getNodeType()) {
				case REQUIREMENT_FOLDER:
					requirementLibraryNavigationService.copyNodesToFolder(nodeReference.getId(), copiedNodeIds.toArray(new Long[0]));
					break;
				case REQUIREMENT_LIBRARY:
					requirementLibraryNavigationService.copyNodesToLibrary(nodeReference.getId(), copiedNodeIds.toArray(new Long[0]));
					break;
				case REQUIREMENT:
					requirementLibraryNavigationService.copyNodesToRequirement(nodeReference.getId(), copiedNodeIds.toArray(new Long[0]));
					break;
				default:
					throw new IllegalArgumentException("copy nodes : specified destination type doesn't exists : "
						+ nodeReference.getNodeType());
			}
		} catch (AccessDeniedException ade) {
			throw new RightsUnsuficientsForOperationException(ade);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/{destinationRef}/content/move", method = RequestMethod.POST)
	public void moveNodes(@RequestBody() NodeList nodeList, @PathVariable("destinationRef") String destinationRef) {
		NodeReference nodeReference = NodeReference.fromNodeId(destinationRef);
		Long destinationId = nodeReference.getId();
		NodeType destinationType = nodeReference.getNodeType();
		Long[] movedNodeIds = nodeList.getIds().toArray(new Long[0]);

		try {
			switch (destinationType) {
				case REQUIREMENT_LIBRARY:
					requirementLibraryNavigationService.moveNodesToLibrary(destinationId, movedNodeIds);
					break;
				case REQUIREMENT_FOLDER:
					requirementLibraryNavigationService.moveNodesToFolder(destinationId, movedNodeIds);
					break;
				case REQUIREMENT:
					requirementLibraryNavigationService.moveNodesToRequirement(destinationId, movedNodeIds);
					break;
				default:
					throw new IllegalArgumentException("move nodes : specified destination type doesn't exists : "
						+ destinationType);
			}
		} catch (AccessDeniedException ade) {
			throw new RightsUnsuficientsForOperationException(ade);
		}

	}

	@ResponseBody
	@RequestMapping(value = "/{destinationRef}/content/move/{position}", method = RequestMethod.POST)
	public void moveNodesAtPosition(@RequestBody() NodeList nodeList, @PathVariable("destinationRef") String destinationRef, @PathVariable("position") int position) {
		NodeReference nodeReference = NodeReference.fromNodeId(destinationRef);
		Long destinationId = nodeReference.getId();
		NodeType destinationType = nodeReference.getNodeType();
		Long[] movedNodeIds = nodeList.getIds().toArray(new Long[0]);

		try {
			switch (destinationType) {
				case REQUIREMENT_LIBRARY:
					requirementLibraryNavigationService.moveNodesToLibrary(destinationId, movedNodeIds, position);
					break;
				case REQUIREMENT_FOLDER:
					requirementLibraryNavigationService.moveNodesToFolder(destinationId, movedNodeIds, position);
					break;
				case REQUIREMENT:
					requirementLibraryNavigationService.moveNodesToRequirement(destinationId, movedNodeIds, position);
					break;
				default:
					throw new IllegalArgumentException("move nodes : specified destination type doesn't exists : "
						+ destinationType);
			}
		} catch (AccessDeniedException ade) {
			throw new RightsUnsuficientsForOperationException(ade);
		}
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "new-folder", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public DataRow addNewFolder(@RequestBody RequirementFolderFormModel folderModel) throws BindException {

		validateRequirementFolderFormModel(folderModel);

		RequirementFolder requirementFolder = folderModel.getRequirementFolder();

		EntityReference parentEntityReference = getParentNodeReference(folderModel);
		createAndPersistRequirementFolder(requirementFolder, parentEntityReference, folderModel.getCufs());
		return treeNodeCollectorService.collectNode(NodeType.REQUIREMENT_FOLDER, requirementFolder);
	}

	private void createAndPersistRequirementFolder(RequirementFolder requirementFolder, EntityReference parentEntityReference, Map<Long, RawValue> cufs) {
		switch (parentEntityReference.getType()) {
			case REQUIREMENT_LIBRARY:
				requirementLibraryNavigationService.addFolderToLibrary(parentEntityReference.getId(), requirementFolder, cufs);
				break;
			case REQUIREMENT_FOLDER:
				requirementLibraryNavigationService.addFolderToFolder(parentEntityReference.getId(), requirementFolder, cufs);
				break;
			default:
				throw new IllegalArgumentException("This entity type is not handled " + parentEntityReference.toNodeId());
		}
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "new-requirement", method = RequestMethod.POST)
	public DataRow addNewRequirement(@RequestBody RequirementFormModel requirementModel) throws BindException {
		validateRequirementFormModel(requirementModel);
		EntityReference parentEntityReference = getParentNodeReference(requirementModel);
		Requirement req = createAndPersistRequirement(parentEntityReference, requirementModel);
		return treeNodeCollectorService.collectNode(NodeType.REQUIREMENT, req);
	}

	private EntityReference getParentNodeReference(EntityFormModel entityFormModel) {
		String serializedParentEntityReference = entityFormModel.getParentEntityReference();
		return EntityReference.fromNodeId(serializedParentEntityReference);
	}

	private Requirement createAndPersistRequirement(EntityReference parentEntityReference, RequirementFormModel requirementModel) {
		NewRequirementVersionDto newRequirementVersionDto = requirementModel.toDTO();
		Requirement req;

		switch (parentEntityReference.getType()) {
			case REQUIREMENT_LIBRARY:
				req = requirementLibraryNavigationService.addRequirementToRequirementLibrary(parentEntityReference.getId(), newRequirementVersionDto, activeMilestoneAsList());
				break;
			case REQUIREMENT_FOLDER:
				req = requirementLibraryNavigationService.addRequirementToRequirementFolder(parentEntityReference.getId(), newRequirementVersionDto, activeMilestoneAsList());
				break;
			case REQUIREMENT:
				req = requirementLibraryNavigationService.addRequirementToRequirement(parentEntityReference.getId(), newRequirementVersionDto, activeMilestoneAsList());
				break;
			default:
				throw new IllegalArgumentException("This entity type is not handled " + parentEntityReference.toNodeId());
		}
		return req;
	}

	@ResponseBody
	@RequestMapping(value = "/validation-statistics",
		method = RequestMethod.POST)
	public IdList getValidationRequirementIds(@RequestBody ValidationStatisticRequest validationStatisticRequest) {
		Collection<Long> ids = requirementStatisticsService.gatherRequirementIdsFromValidation(
			validationStatisticRequest.selectedIds,
			RequirementCriticality.valueOf(validationStatisticRequest.criticality),
			validationStatisticRequest.validation);
		return new IdList(ids);
	}

	private void validateRequirementFormModel(RequirementFormModel requirementModel) throws BindException {
		BindingResult validation = new BeanPropertyBindingResult(requirementModel, ADD_REQUIREMENT);
		RequirementFormModel.RequirementFormModelValidator validator = new RequirementFormModel.RequirementFormModelValidator(messageSource);
		validator.validate(requirementModel, validation);

		if (validation.hasErrors()) {
			throw new BindException(validation);
		}
	}

	private List<Long> activeMilestoneAsList() {
		List<Long> milestoneIds = new ArrayList<>();
		Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();
		activeMilestone.ifPresent(milestone -> milestoneIds.add(milestone.getId()));
		return milestoneIds;

	}

	@Override
	protected LibraryNavigationService<RequirementLibrary, RequirementFolder, RequirementLibraryNode> getLibraryNavigationService() {
		return this.requirementLibraryNavigationService;
	}

	@Override
	protected NodeWorkspace getWorkspace() {
		return NodeWorkspace.REQUIREMENT;
	}

	private void validateRequirementFolderFormModel(RequirementFolderFormModel folderModel) throws BindException {
		BindingResult validation = new BeanPropertyBindingResult(folderModel, ADD_REQUIREMENT_FOLDER);
		EntityFormModelValidator entityFormModelValidator = new EntityFormModelValidator();
		entityFormModelValidator.validate(folderModel, validation);

		if (validation.hasErrors()) {
			throw new BindException(validation);
		}
	}

	static class ValidationStatisticRequest {
		private List<Long> selectedIds;
		private String criticality;
		private Collection<String> validation;

		public List<Long> getSelectedIds() {
			return selectedIds;
		}

		public void setSelectedIds(List<Long> selectedIds) {
			this.selectedIds = selectedIds;
		}

		public String getCriticality() {
			return criticality;
		}

		public void setCriticality(String criticality) {
			this.criticality = criticality;
		}

		public Collection<String> getValidation() {
			return validation;
		}

		public void setValidation(Collection<String> validation) {
			this.validation = validation;
		}
	}
}
