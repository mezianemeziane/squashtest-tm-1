/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.squashtest.tm.service.internal.display.dto.AutomatedTestDto;
import org.squashtest.tm.service.internal.display.grid.TreeGridResponse;
import org.squashtest.tm.service.testautomation.model.TestAutomationProjectContent;
import org.squashtest.tm.service.testcase.TestCaseModificationService;
import org.squashtest.tm.web.backend.model.testautomation.TATestFlatTree;
import org.squashtest.tm.web.backend.model.testautomation.TATestNode;
import org.squashtest.tm.web.backend.model.testautomation.TATestNodeListBuilder;

import java.util.Collection;

@Controller
@RequestMapping("backend/test-case/{testCaseId}/test-automation")
public class TestCaseAutomationController {

	private TestCaseModificationService testCaseModificationService;

	public TestCaseAutomationController(TestCaseModificationService testCaseModificationService) {
		this.testCaseModificationService = testCaseModificationService;
	}

	@ResponseBody
	@GetMapping(value = "/tests")
	public TreeGridResponse findAssignableAutomatedTests(@PathVariable Long testCaseId) {
		Collection<TestAutomationProjectContent> list = testCaseModificationService.findAssignableAutomationTests(testCaseId);
		Collection<TATestNode> nodes = new TATestNodeListBuilder().build(list);
		return new TATestFlatTree().flatTree(nodes);
	}

	@PostMapping(value = "/tests")
	@ResponseBody
	public AutomatedTestDto bindAutomatedTest(@PathVariable Long testCaseId, @RequestBody AutomatedTestPatch automatedTestPatch) {
		return testCaseModificationService.bindAutomatedTestToTC(testCaseId, automatedTestPatch.path);
	}

	static class AutomatedTestPatch {
		String path;

		public String getPath() {
			return path;
		}

		public void setPath(String path) {
			this.path = path;
		}
	}
}
