/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.resource.PathResourceResolver;

import java.io.IOException;

public class SquashResourceResolver extends PathResourceResolver {

	private static final Logger LOGGER = LoggerFactory.getLogger(SquashResourceResolver.class);
	private String baseApiPath = "backend";

	@Override
	protected Resource getResource(String resourcePath, Resource location) throws IOException {

		LOGGER.debug("Try to resolve resource for resource : {} ", resourcePath);

		// Backend request are forwarded to controllers, not static resources
		if (resourcePath.startsWith(baseApiPath) ||
			resourcePath.startsWith("/error") ||
			resourcePath.startsWith("error") ||
			resourcePath.startsWith(baseApiPath.substring(1))) {
			LOGGER.debug("{} seems to be a call to data controllers, returning null.", resourcePath);
			return null;
		}

		// If the requested url is a plugin, we serve the plugin app by serving it's index.html.
		// A plugin is an angular app built with the option --deploy-url=/<plugin-id>/ and packaged in the jar of the plugin
		// Ie for a plugin called x-squash. The angular app must be packaged into /META-INF/resources/x-squash of the x-squash.version.jar
		if (resourcePath.startsWith("/plugin/") || resourcePath.startsWith("plugin/")) {
			LOGGER.debug("{} seems to be a call to plugin. Try to resolve plugin app.", resourcePath);
			String[] parts = resourcePath.split("/");
			Resource pluginRootPage = null;
			if (parts.length > 1) {
				LOGGER.debug("Requested plugin root page is {}", parts[1]);
				pluginRootPage = new ClassPathResource("/META-INF/resources/" + parts[1] + "/index.html");
				LOGGER.debug("Resolved plugin, root page : {}. class path resource is : {}.", parts[1], pluginRootPage);
			}
			return pluginRootPage != null && pluginRootPage.exists() && pluginRootPage.isReadable() ? pluginRootPage : null;
		}

		//default to application root : the index.html of tm-front
		return location.exists() && location.isReadable() ? location : null;
	}
}
