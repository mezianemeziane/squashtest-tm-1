/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.scm.server;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.squashtest.tm.core.scm.api.exception.ScmException;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.exception.scmserver.ScmPluginException;
import org.squashtest.tm.service.display.scm.server.ScmServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.ScmRepositoryDto;
import org.squashtest.tm.service.scmserver.ScmRepositoryManagerService;
import org.squashtest.tm.web.backend.controller.form.model.ScmRepositoryFormModel;


import javax.inject.Inject;
import java.io.IOException;
import java.util.*;


@Controller
@RequestMapping("/backend/scm-repositories")
public class ScmRepositoryManagementController {

	ScmRepositoryManagerService scmRepositoryManager;
	ScmServerDisplayService scmServerDisplayService;

	@Inject
	ScmRepositoryManagementController(ScmRepositoryManagerService scmRepositoryManager,
									  ScmServerDisplayService scmServerDisplayService) {
		this.scmRepositoryManager = scmRepositoryManager;
		this.scmServerDisplayService = scmServerDisplayService;
	}

	@RequestMapping(value = "/{scmServerId}/new", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, List<ScmRepositoryDto>> createNewScmRepository(@PathVariable long scmServerId, @RequestBody ScmRepositoryFormModel scmRepositoryFormModel) throws IOException {
		try {
			scmRepositoryManager.createNewScmRepository(
				scmServerId,
				scmRepositoryFormModel.getScmRepository(),
				scmRepositoryFormModel.getCloneRepository()
			);

		} catch (ScmException ex) {
			throw new ScmPluginException(ex.getField(), ex);
		}

		List<ScmRepositoryDto> repositories = scmServerDisplayService.getScmServerView(scmServerId).getRepositories();
		return Collections.singletonMap("repositories", repositories);
	}

	@RequestMapping(value = "/{scmServerId}/{scmRepositoryId}/branch", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, List<ScmRepositoryDto>> updateBranch(@PathVariable long scmServerId, @PathVariable long scmRepositoryId, @RequestBody ScmRepositoryPatch patch) throws IOException {
		try {
			scmRepositoryManager.updateBranch(scmRepositoryId, patch.getBranch());
		} catch (ScmException ex) {
			throw new ScmPluginException(ex.getField(), ex);
		}

		List<ScmRepositoryDto> repositories = scmServerDisplayService.getScmServerView(scmServerId).getRepositories();
		return Collections.singletonMap("repositories", repositories);
	}

	@RequestMapping(value = "/{scmRepositoriesIds}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteScmRepositories(@PathVariable List<Long> scmRepositoriesIds) {
		scmRepositoryManager.deleteScmRepositories(scmRepositoriesIds);
	}

	@RequestMapping(value = "check-bound-to-project/{scmRepositoryIds}", method = RequestMethod.GET)
	@ResponseBody
	public boolean isOneRepositoryBoundToProject(@PathVariable Collection<Long> scmRepositoryIds) {
		return scmRepositoryManager.isOneRepositoryBoundToProjectOrTestCase(scmRepositoryIds);
	}

	public static class ScmRepositoryPatch {
		private String branch;

		public String getBranch() {	return branch; }

		public void setBranch(String branch) { this.branch = branch; }
	}

}
