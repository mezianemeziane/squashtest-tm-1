/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.exceptionresolver.model;

public final class FieldValidationErrorModel {

	public final String objectName; // NOSONAR Field is immutable
	public final String fieldName; // NOSONAR Field is immutable
	private String fieldValue;
	public final String i18nKey; // NOSONAR Field is immutable
	public Object[] messageArgs; //NOSONAR Field is immutable

	public FieldValidationErrorModel(String objectName, String fieldName, String i18nKey) {
		super();
		this.objectName = objectName;
		this.fieldName = fieldName;
		this.fieldValue = null;
		this.i18nKey = i18nKey;
	}

	public FieldValidationErrorModel(String objectName, String fieldName, String i18nKey, Object[] messageArgs) {
		this(objectName, fieldName, i18nKey);
		this.messageArgs = messageArgs;
	}

	public FieldValidationErrorModel(String objectName, String fieldName, String i18nKey, Object[] messageArgs, String fieldValue) {
		this(objectName, fieldName, i18nKey, messageArgs);
		this.fieldValue = fieldValue;
	}

	public FieldValidationErrorModel(String objectName, String fieldName, String i18nKey, Object[] messageArgs, Object fieldValue) {
		this(objectName, fieldName, i18nKey, messageArgs);
		if(fieldValue != null){
			this.fieldValue = fieldValue.toString();
		}
	}

	public String getFieldValue() {
		return fieldValue;
	}
}
