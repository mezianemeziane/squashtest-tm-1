/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.bugtracker;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseKind;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

// XSS OK - bflessel
@Component
public final class BugTrackerControllerHelper {
	private BugTrackerControllerHelper() {

	}

	/**
	 * Will build a String that shows all steps before the bugged step + the bugged step itself.<br/>
	 * The String for a Step will differ whether the referenced TestCase is a Scripted TestCase or a Standard one.
	 * @param buggedStep the bugged step where the issue will be declared
	 * @param locale the locale used
	 * @param messageSource the messages
	 * @return the build String
	 */
	public static String getAdditionalInformation(
		ExecutionStep buggedStep,
		Locale locale,
		MessageSource messageSource) {

		Execution execution = buggedStep.getExecution();
		TestCaseKind testCaseKind = TestCaseKind.fromTestCase(execution.getReferencedTestCase());

		List<ExecutionStep> executionSteps = execution.getSteps();
		int totalStepNumber = executionSteps.size();
		long buggedStepId = buggedStep.getId();

		switch (testCaseKind) {
			case STANDARD:
				return getAdditionalInformationForStandardTestCase(
					executionSteps, buggedStepId, totalStepNumber, locale, messageSource);
			case GHERKIN:
				return getAdditionalInformationForScriptedTestCase(
					executionSteps, buggedStepId, totalStepNumber, locale, messageSource);
			case KEYWORD:
				return getAdditionalInformationForKeywordTestCase(
					executionSteps, buggedStepId, totalStepNumber, locale, messageSource);
			default:
				throw new IllegalArgumentException("The Kind " + testCaseKind + " for a Test Case does not exist.");
		}
	}

	/**
	 * For a Standard TestCase,
	 * build the String that shows all ExecutionSteps before the bugged step + the bugged step itself.<br/>
	 * The string will look like this : <br/>
	 * <br>
	 * <em>
	 * 	=============================================<br>
	 *  |    Step 1/N<br>
	 *  =============================================<br>
	 * 	--- Action ---<br>
	 * 	action description<br>
	 * 	<br>
	 * 	--- Expected Result ----<br>
	 * 	expected result description<br>
	 * 	<br>
	 * 	<br>
	 * 	=============================================<br>
	 * 	|    Step 2/N<br>
	 * 	=============================================<br>
	 * 	...<br>
	 * 	<br></em>
	 *
	 * @param executionSteps the list of Steps in the Execution
	 * @param buggedStepId the id of the bugged Step
	 * @param totalStepNumber the total number of Steps
	 * @param locale the locale
	 * @param messageSource the messageSource
	 * @return the built String as described
	 */
	private static String getAdditionalInformationForStandardTestCase(
		List<ExecutionStep> executionSteps,
		long buggedStepId,
		int totalStepNumber,
		Locale locale,
		MessageSource messageSource) {

		StringBuilder builder = new StringBuilder();
		for (ExecutionStep step : executionSteps) {
			appendStepTitle(locale, messageSource, totalStepNumber, builder, step);

			String actionText = HTMLCleanupUtils.htmlToText(step.getAction());
			String expectedResult = HTMLCleanupUtils.htmlToText(step.getExpectedResult());

			builder.append(messageSource.getMessage("issue.default.additionalInformation.action", null, locale));
			builder.append(actionText);
			builder.append(messageSource.getMessage("issue.default.additionalInformation.expectedResult", null, locale));
			builder.append(expectedResult);
			builder.append("\n\n\n");
			if (step.getId().equals(buggedStepId)) {
				break;
			}
		}
		return builder.toString();
	}

	/**
	 * For a Scripted TestCase,
	 * build the String that shows all ExecutionSteps before the bugged step + the bugged step itself.<br/>
	 * The String will look like this : <br/>
	 * <br/>
	 * <em>
	 * 	=============================================<br>
	 *  |    Step 1/N<br>
	 *  =============================================<br>
	 *  --- Script ---<br/>
	 *  script text<br/>
	 *  <br/>
	 *  <br/>
	 * 	=============================================<br>
	 * 	|    Step 2/N<br>
	 * 	=============================================<br>
	 *  --- Script ---<br/>
	 *  script text<br/>
	 * 	...<br>
	 * <br/>
	 * </em>
	 *
	 * @param executionSteps the list of Steps in the Execution
	 * @param buggedStepId the id of the bugged Step
	 * @param totalStepNumber the total number of Steps
	 * @param locale the locale
	 * @param messageSource the messageSource
	 * @return the built String as described
	 */
	private static String getAdditionalInformationForScriptedTestCase(
		List<ExecutionStep> executionSteps,
		long buggedStepId,
		int totalStepNumber,
		Locale locale,
		MessageSource messageSource) {

		StringBuilder builder = new StringBuilder();
		for (ExecutionStep step : executionSteps) {
			appendStepTitle(locale, messageSource, totalStepNumber, builder, step);

			String scriptTextWithCarriageReturns = step.getAction().replaceAll("</br>", "<br/>");
			String scriptText = HTMLCleanupUtils.htmlToText(scriptTextWithCarriageReturns);
			builder.append(messageSource.getMessage("issue.default.additionalInformation.script", null, locale));
			builder.append(scriptText);
			builder.append("\n\n\n");
			if (step.getId().equals(buggedStepId)) {
				break;
			}
		}
		return builder.toString();
	}

	/**
	 * For a keyword TestCase, build the String that shows all ExecutionSteps before the bugged step + the bugged step
	 * itself. For each step, only the action is shown.
	 *
	 * @param executionSteps the list of Steps in the Execution
	 * @param buggedStepId the id of the bugged Step
	 * @param totalStepNumber the total number of Steps
	 * @param locale the locale
	 * @param messageSource the messageSource
	 * @return the built String as described
	 */
	private static String getAdditionalInformationForKeywordTestCase(List<ExecutionStep> executionSteps, long buggedStepId, int totalStepNumber, Locale locale, MessageSource messageSource) {
		StringBuilder builder = new StringBuilder();
		for (ExecutionStep step : executionSteps) {
			appendStepTitle(locale, messageSource, totalStepNumber, builder, step);

			String content = HTMLCleanupUtils.htmlToText(step.getAction());
			builder.append(content);
			builder.append("\n\n\n");
			if (step.getId().equals(buggedStepId)) {
				break;
			}
		}
		return builder.toString();
	}

	private static void appendStepTitle(Locale locale, MessageSource messageSource, int totalStepNumber,
										StringBuilder builder, ExecutionStep step) {
		builder.append("=============================================\n|    ");
		builder.append(messageSource.getMessage("issue.default.additionalInformation.step", null, locale));
		builder.append(" ");
		builder.append(step.getExecutionStepOrder() + 1);
		builder.append("/");
		builder.append(totalStepNumber);
		builder.append("\n=============================================\n");
	}

	/**
	 * Will build a default description String that will look like this : <br/>
	 * <br/>
	 * <em># Test Case : [Reference] test case name <br/>
	 * # Execution : execution link <br/>
	 * <br/>
	 * # Issue description :<br/></em>
	 *
	 * @param execution
	 *            an execution where the issue will be declared
	 * @return the description string
	 */
	public static String getDefaultDescription(Execution execution, Locale locale, MessageSource messageSource,
											   String executionUrl) {
		StringBuilder description = new StringBuilder();
		appendTestCaseDesc(execution.getReferencedTestCase(), description, locale, messageSource);
		appendExecutionDesc(description, locale, messageSource, executionUrl);
		appendDescHeader(description, locale, messageSource);
		return description.toString();
	}

	/**
	 * Will build a default description String that will look like this : <br/>
	 * <br/>
	 * <em># Test Case : [Reference] test case name <br/>
	 * # Execution : execution link <br/>
	 * # Concerned Step : step n�/total step nb<br/>
	 * <br/>
	 * # Issue description :<br/></em>
	 *
	 * @param step
	 *            an execution step where the issue will be declared
	 * @return the string built as described
	 */
	public static String getDefaultDescription(ExecutionStep step, Locale locale, MessageSource messageSource,
											   String executionUrl) {
		StringBuilder description = new StringBuilder();
		appendTestCaseDesc(step.getExecution().getReferencedTestCase(), description, locale, messageSource);
		appendExecutionDesc(description, locale, messageSource, executionUrl);
		appendStepDesc(step, description, locale, messageSource);
		appendDescHeader(description, locale, messageSource);
		return description.toString();
	}

	/**
	 * build the url of the execution
	 *
	 * @return <b>"http://</b>serverName<b>:</b>serverPort/contextPath<b>/executions/</b>executionId<b>/info"</b>
	 */
	public static String buildExecutionUrlFromRequest(HttpServletRequest request, Execution execution) {
		StringBuilder requestUrl = new StringBuilder(request.getScheme());
		// formatter:off
		requestUrl.append("://")
			.append(request.getServerName())
			.append(':')
			.append(request.getServerPort())
			.append(request.getContextPath())
			.append("/execution/")
			.append(execution.getId());
		// formatter:on
		return HTMLCleanupUtils.cleanHtml(requestUrl.toString());
	}

	public static String buildExecutionUrlWithSquashPublicUrl(String squashPublicUrl, Execution execution) {
		StringBuilder requestUrl = new StringBuilder(squashPublicUrl)
			.append("/execution/")
			.append(execution.getId());
		return HTMLCleanupUtils.cleanHtml(requestUrl.toString());
	}

	private static void appendDescHeader(StringBuilder description, Locale locale, MessageSource messageSource) {
		description.append("\n# ");
		description.append(messageSource.getMessage("issue.default.description.description", null, locale));
		description.append(" :\n");
	}

	private static void appendStepDesc(ExecutionStep step, StringBuilder description, Locale locale,
									   MessageSource messageSource) {
		description.append("# ");
		description.append(messageSource.getMessage("issue.default.description.concernedStep", null, locale));
		description.append(": ");
		description.append(step.getExecutionStepOrder() + 1);
		description.append("/");
		description.append(step.getExecution().getSteps().size());
		description.append("\n");
	}

	private static void appendExecutionDesc(StringBuilder description, Locale locale, MessageSource messageSource,
											String executionUrl) {
		description.append("# ");
		description.append(messageSource.getMessage("issue.default.description.execution", null, locale));
		description.append(": ");
		description.append(executionUrl);
		description.append("\n");
	}

	private static void appendTestCaseDesc(TestCase testCase, StringBuilder description, Locale locale,
										   MessageSource messageSource) {
		if (testCase != null) {
			description.append("# ");
			description.append(messageSource.getMessage("issue.default.description.testCase", null, locale));
			description.append(": [");
			description.append(HTMLCleanupUtils.cleanAndUnescapeHTML(testCase.getReference()));
			description.append("] ");
			description.append(HTMLCleanupUtils.cleanAndUnescapeHTML(testCase.getName()));
			description.append("\n");
		}
	}

}
