/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.report;

import org.squashtest.tm.api.report.form.*;
import org.squashtest.tm.api.report.form.composite.MilestonePickerOption;
import org.squashtest.tm.api.report.form.composite.ProjectPickerOption;
import org.squashtest.tm.api.report.form.composite.TagPickerOption;
import org.squashtest.tm.api.report.form.composite.TreePickerOption;
import org.squashtest.tm.web.backend.controller.report.OptionInputJson.TagPickerOptionJson;
import org.squashtest.tm.web.backend.controller.report.OptionInputJson.TreePickerOptionJson;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class OptionsGroupJson extends BasicInputJson {

	private List<OptionInputJson> options = Collections.emptyList();

	public OptionsGroupJson(OptionsGroup input) {
		super(input);
		this.options = input.getOptions().stream()
			.map(OptionsGroupJson::fromOptionInput)
			.collect(Collectors.toList());
	}

	public List<OptionInputJson> getOptions() {
		return options;
	}

	public static OptionInputJson fromOptionInput(OptionInput optionInput){
		OptionInputJson[] optionInputJsons = new OptionInputJson[1];

		OptionInputVisitor optionInputVisitor = new OptionInputVisitor() {

			@Override
			public void visit(ContainerOption<?> containerOption) {
				OptionInputJson optionInputJson = new OptionInputJson(containerOption);
				optionInputJson.setComposite(true);
				optionInputJson.setContentType(containerOption.getContent().getType());
				optionInputJsons[0] = optionInputJson;
			}

			@Override
			public void visit(MilestonePickerOption milestonePickerOption) {
				OptionInputJson optionInputJson = new OptionInputJson(milestonePickerOption);
				optionInputJson.setComposite(true);
				optionInputJson.setContentType(milestonePickerOption.getContent().getType());
				optionInputJsons[0] = optionInputJson;
			}

			@Override
			public void visit(ProjectPickerOption projectPickerOption) {
				OptionInputJson optionInputJson = new OptionInputJson(projectPickerOption);
				optionInputJson.setComposite(true);
				optionInputJson.setContentType(projectPickerOption.getContent().getType());
				optionInputJsons[0] = optionInputJson;
			}

			@Override
			public void visit(TagPickerOption tagPickerOption) {
				OptionInputJson optionInputJson = new TagPickerOptionJson(tagPickerOption);
				optionInputJson.setComposite(true);
				optionInputJson.setContentType(tagPickerOption.getContent().getType());
				optionInputJsons[0] = optionInputJson;
			}

			@Override
			public void visit(TreePickerOption treePickerOption) {
				OptionInputJson optionInputJson = new TreePickerOptionJson(treePickerOption);
				optionInputJson.setComposite(true);
				optionInputJson.setContentType(treePickerOption.getContent().getType());
				optionInputJsons[0] = optionInputJson;
			}

			@Override
			public void visit(CheckboxInput checkboxInput) {
				OptionInputJson optionInputJson = new OptionInputJson(optionInput);
				optionInputJson.setComposite(true);
				optionInputJson.setContentType(InputType.CHECKBOX);
				optionInputJsons[0] = optionInputJson;
			}

			@Override
			public void visit(OptionInput optionInput) {
				OptionInputJson optionInputJson = new OptionInputJson(optionInput);
				optionInputJson.setComposite(true);
				// defaulting to checkbox
				optionInputJson.setContentType(InputType.CHECKBOX);
				optionInputJsons[0] = optionInputJson;
			}
		};

		optionInput.accept(optionInputVisitor);

		return optionInputJsons[0];
	}
}
