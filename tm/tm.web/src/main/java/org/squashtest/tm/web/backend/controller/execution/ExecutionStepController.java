/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.execution;

import org.springframework.web.bind.annotation.*;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.service.display.execution.ExecutionDisplayService;
import org.squashtest.tm.service.execution.ExecutionProcessingService;
import org.squashtest.tm.service.internal.display.dto.execution.ExecutionStepView;
import org.squashtest.tm.service.internal.display.dto.execution.ExecutionView;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController()
@RequestMapping("/backend/execution-step/{stepId}")
public class ExecutionStepController {

	private final ExecutionProcessingService executionProcessingService;
	private final ExecutionDisplayService executionDisplayService;

	public ExecutionStepController(ExecutionProcessingService executionProcessingService,
								   ExecutionDisplayService executionDisplayService) {
		this.executionProcessingService = executionProcessingService;
		this.executionDisplayService = executionDisplayService;
	}

	@PostMapping(value = "/status")
	@ResponseBody
	public Map<String, Object> updateExecutionStatus(@RequestBody ExecutionStepPatch patch, @PathVariable long stepId, @RequestParam("executionId") Long executionId) {
		ExecutionStatus status = ExecutionStatus.valueOf(patch.executionStatus);
		executionProcessingService.changeExecutionStepStatus(stepId, status);
		ExecutionView execution = executionDisplayService.findOne(executionId);
		Optional<ExecutionStepView> executionStep = execution.getExecutionStepViews().stream().filter(step -> step.getId() == stepId).findAny();
		Map<String, Object> response = new HashMap<>();
		response.put("executionStatus", execution.getExecutionStatus());
		response.put("lastExecutedOn", execution.getLastExecutedOn());
		response.put("lastExecutedBy", execution.getLastExecutedBy());
		executionStep.ifPresent(step -> {
			response.put("stepLastExecutedOn", step.getLastExecutedOn());
			response.put("stepLastExecutedBy", step.getLastExecutedBy());
		});

		return response;
	}

	@RequestMapping("/comment")
	@ResponseBody
	public void changeComment(@RequestBody ExecutionStepPatch patch, @PathVariable long stepId) {
		executionProcessingService.setExecutionStepComment(stepId, patch.comment);
	}

	static class ExecutionStepPatch {

		private String executionStatus;
		private String comment;

		public String getExecutionStatus() {
			return executionStatus;
		}

		public void setExecutionStatus(String executionStatus) {
			this.executionStatus = executionStatus;
		}

		public String getComment() {
			return comment;
		}

		public void setComment(String comment) {
			this.comment = comment;
		}
	}

}
