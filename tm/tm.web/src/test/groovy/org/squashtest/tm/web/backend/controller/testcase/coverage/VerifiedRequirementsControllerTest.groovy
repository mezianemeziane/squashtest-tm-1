/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase.coverage


import org.squashtest.tm.service.display.testcase.TestCaseDisplayService
import org.squashtest.tm.service.requirement.VerifiedRequirementsManagerService
import org.squashtest.tm.web.backend.controller.NodeBuildingSpecification

class VerifiedRequirementsControllerTest extends NodeBuildingSpecification {
	VerifiedRequirementsManagerService verifiedRequirementsManagerService = Mock()
	TestCaseDisplayService testCaseDisplayService = Mock()
	VerifiedRequirementsController controller = new VerifiedRequirementsController(
		verifiedRequirementsManagerService,
		testCaseDisplayService
	)

	def "should add requirements to verified requirements to test case"() {
		when:
		def idsForm = new VerifiedRequirementsController.RequirementIdsForm()
		idsForm.requirementIds = [5, 15]
		controller.addVerifiedRequirementsToTestCase(idsForm, 10)

		then:
		1 * verifiedRequirementsManagerService.addVerifiedRequirementsToTestCase([5, 15], 10) >> []
	}

	def "should add requirements to verified requirements of test step"() {
		when:
		def idsForm = new VerifiedRequirementsController.RequirementIdsForm()
		idsForm.requirementIds = [5, 15]
		controller.addVerifiedRequirementsToTestStep(idsForm,1, 10)

		then:
		1 * verifiedRequirementsManagerService.addVerifiedRequirementsToTestStep([5, 15], 10) >> []
	}

	def "should add requirements to verified requirement of test step"() {
		when:
		def idsForm = new VerifiedRequirementsController.RequirementIdsForm()
		idsForm.requirementIds = [5]
		controller.addVerifiedRequirementsToTestStep(idsForm,1, 10)

		then:
		1 * verifiedRequirementsManagerService.addVerifiedRequirementsToTestStep([5L], 10L) >> []
	}

	def "should remove requirements from verified requirements of test case"() {
		when:
		controller.removeVerifiedRequirementVersionsFromTestCase([5, 15], 10)

		then:
		1 * verifiedRequirementsManagerService.removeVerifiedRequirementVersionsFromTestCase([5, 15], 10)
	}

	def "should remove requirements from verified requirements of test step"() {
		when:
		controller.removeVerifiedRequirementVersionsFromTestStep([5, 15], 10,1)

		then:
		1 * verifiedRequirementsManagerService.removeVerifiedRequirementVersionsFromTestStep([5, 15], 10)
	}
}
